global class Phase1ContractAlignment implements Database.Batchable<sObject>, Schedulable{
    
    global List<String> staticValues = new List<String>();

    global Phase1ContractAlignment(List<String> val){
        this.staticValues = val;
    }
    
    global void execute(SchedulableContext sc) {
      Phase1ContractAlignment a = new Phase1ContractAlignment(this.staticValues); 
      Database.executeBatch(a, 15);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){  
		String activated = 'Activated';
        return Database.getQueryLocator('SELECT Id, AccountId, SBQQ__Opportunity__c, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__StartDate__c, ' +
                                        'SBQQ__Quote__r.SBQQ__ExpirationDate__c, SBQQ__Quote__r.SBQQ__PriceBook__c, ' +
                                        'SBQQ__Quote__r.Billing_Profile__c FROM Contract WHERE Phase1Source__c = TRUE AND SBQQ__Quote__r.SBQQ__Status__c !=\''+ ConstantsUtil.Quote_StageName_Rejected +'\' and StatusCode =:activated AND Cluster_Id__c in: staticValues '+
                                         'AND Id NOT IN (SELECT Contract__c FROM Order WHERE SBQQ__Contracted__c= TRUE AND Type = \''+ ConstantsUtil.ORDER_TYPE_NEW +'\' AND Custom_Type__c = \''+ ConstantsUtil.ORDER_TYPE_ACTIVATION +'\' AND Status = \''+ ConstantsUtil.ORDER_STATUS_ACTIVATED +'\')');
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        
        Set<Id> contractIds = new Set<Id>();
        Map<Id, Contract> contractMap = new Map<Id, Contract>();
        for(Contract contract : scope){
            contractIds.add(contract.Id);
            contractMap.put(contract.Id, contract);
        }
        
        if(!contractIds.isEmpty() && !contractMap.isEmpty()){
            List<SBQQ__Subscription__c> subs = [SELECT  Id, 
                                                        SBQQ__RequiredById__c, 
                                                        SBQQ__Product__c,
                                                        SBQQ__Contract__c,
                                                        SBQQ__QuoteLine__c,
                                                        SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c,
                                                        SBQQ__QuoteLine__r.SBQQ__Quantity__c,
                                                        SBQQ__QuoteLine__r.SBQQ__ListPrice__c,
                                                        SBQQ__QuoteLine__r.SBQQ__StartDate__c,
														SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c,
                                                        SBQQ__QuoteLine__r.Subscription_Term__c,
                                                        SBQQ__QuoteLine__r.Place__c,
                                                        SBQQ__QuoteLine__r.Total__c,
                                                        SBQQ__QuoteLine__r.Package_Total__c
                                                  FROM  SBQQ__Subscription__c 
                                                  WHERE SBQQ__Contract__c IN :contractIds];
            
            Map<Id, List<SBQQ__Subscription__c>> contractToSubs = new Map<Id, List<SBQQ__Subscription__c>>();
            for(SBQQ__Subscription__c currentSub : subs){
                if(!contractToSubs.containsKey(currentSub.SBQQ__Contract__c)){
                    contractToSubs.put(currentSub.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{currentSub});
                }
                else{
                    contractToSubs.get(currentSub.SBQQ__Contract__c).add(currentSub);
                }
                currentSub.RequiredBy__c = currentSub.SBQQ__RequiredById__c;
                currentSub.Subscription_Term__c = currentSub.SBQQ__QuoteLine__r.Subscription_Term__c;
                currentSub.Total__c = currentSub.SBQQ__QuoteLine__r.Total__c;
            }
            SubscriptionTriggerHandler.disableTrigger = true;
            update subs;
            SubscriptionTriggerHandler.disableTrigger = false;
            
            List<Order> ordersToInsert = new List<Order>();
            for(Id currentContractId : contractToSubs.keySet()){
                Order order = new Order();
                order.Custom_Type__c = 'Activation';
                order.Type = 'New';
                order.Status = 'Draft';
                order.EffectiveDate = contractMap.get(currentContractId).SBQQ__Quote__r.SBQQ__StartDate__c;
                order.EndDate = contractMap.get(currentContractId).SBQQ__Quote__r.SBQQ__ExpirationDate__c;
                order.AccountId = contractMap.get(currentContractId).AccountId;
                order.Pricebook2Id = '01s1r000003jhYBAAY';//contractMap.get(currentContractId).SBQQ__Quote__r.SBQQ__PriceBook__c;
                order.SBQQ__Quote__c = contractMap.get(currentContractId).SBQQ__Quote__c;
                order.OpportunityId = contractMap.get(currentContractId).SBQQ__Opportunity__c;
                order.Billing_Profile__c = contractMap.get(currentContractId).SBQQ__Quote__r.Billing_Profile__c;
                order.Contract__c = currentContractId;
                order.ContractId = currentContractId;
                order.SBQQ__ContractingMethod__c = 'By Subscription End Date';
                ordersToInsert.add(order);
            }
            
            if(!ordersToInsert.isEmpty()){
                OrderTriggerHandler.disableTrigger = true;
                Database.insert(ordersToInsert, false);
                OrderTriggerHandler.disableTrigger = false;
            }
            
            Map<Id, Id> contractToOrder = new Map<Id, Id>();
            for(Order currentOrder : ordersToInsert) {
                if(currentOrder.Id != null){
                    contractToOrder.put(currentOrder.Contract__c, currentOrder.Id);
                }
            }
            
            List<OrderItem> fatherOrderItems = new List<OrderItem>();
            List<OrderItem> childrenOrderItems = new List<OrderItem>();
            
            for(Id currentContractId : contractToSubs.keySet()){
                for(SBQQ__Subscription__c currentSub : contractToSubs.get(currentContractId)){
                    if(contractToOrder.containsKey(currentSub.SBQQ__Contract__c)){
						if(String.isBlank(currentSub.SBQQ__RequiredById__c)){
							OrderItem fatherOrderItem = new OrderItem();
							fatherOrderItem.OrderId = contractToOrder.get(currentSub.SBQQ__Contract__c);
							fatherOrderItem.SBQQ__Subscription__c = currentSub.Id;
							fatherOrderItem.SBQQ__Status__c = 'Draft';
							fatherOrderItem.Product2Id = currentSub.SBQQ__Product__c;
							fatherOrderItem.PricebookEntryId = currentSub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c;
							fatherOrderItem.Quantity = currentSub.SBQQ__QuoteLine__r.SBQQ__Quantity__c;
							fatherOrderItem.UnitPrice = currentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
							fatherOrderItem.ServiceDate = currentSub.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c;
							System.debug('SBQQ__StartDate__c '+currentSub.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c+ ' Subscription_Term__c '+currentSub.SBQQ__QuoteLine__r.Subscription_Term__c);
							fatherOrderItem.End_Date__c = currentSub.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c.addMonths(Integer.valueOf(currentSub.SBQQ__QuoteLine__r.Subscription_Term__c)).addDays(-1);
							fatherOrderItem.Place__c = currentSub.SBQQ__QuoteLine__r.Place__c;
							fatherOrderItem.SBQQ__ContractingMethod__c = 'Inherit';
							fatherOrderItem.SBQQ__QuoteLine__c = currentSub.SBQQ__QuoteLine__c;
							fatherOrderItem.Subscription_Term__c = currentSub.SBQQ__QuoteLine__r.Subscription_Term__c;
							fatherOrderItem.Total__c = currentSub.SBQQ__QuoteLine__r.Total__c;
							fatherOrderItem.Package_Total__c = currentSub.SBQQ__QuoteLine__r.Package_Total__c;
							fatherOrderItems.add(fatherOrderItem);
						}
					}
                }
            }
            
            if(!fatherOrderItems.isEmpty()){
                OrderProductTriggerHandler.disableTrigger = true;
                insert fatherOrderItems;
                OrderProductTriggerHandler.disableTrigger = false;
            }
            
            Map<Id, Id> subscriptionToOrderItem = new Map<Id, Id>();
            
			for(OrderItem currentOrderItem : fatherOrderItems) subscriptionToOrderItem.put(currentOrderItem.SBQQ__Subscription__c, currentOrderItem.Id);
            
            for(Id currentContractId : contractToSubs.keySet()){
                for(SBQQ__Subscription__c currentSub : contractToSubs.get(currentContractId)){
					if(contractToOrder.containsKey(currentSub.SBQQ__Contract__c)){
						if(!String.isBlank(currentSub.SBQQ__RequiredById__c)){
							OrderItem childOrderItem = new OrderItem();
							childOrderItem.OrderId = contractToOrder.get(currentSub.SBQQ__Contract__c);
							childOrderItem.SBQQ__Subscription__c = currentSub.Id;
							childOrderItem.SBQQ__Status__c = 'Draft';
							childOrderItem.Product2Id = currentSub.SBQQ__Product__c;
							childOrderItem.PricebookEntryId = currentSub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c;
							childOrderItem.Quantity = currentSub.SBQQ__QuoteLine__r.SBQQ__Quantity__c;
							childOrderItem.UnitPrice = currentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
							childOrderItem.ServiceDate = currentSub.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c;
							childOrderItem.End_Date__c = currentSub.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c.addMonths(Integer.valueOf(currentSub.SBQQ__QuoteLine__r.Subscription_Term__c)).addDays(-1);
							childOrderItem.Place__c = currentSub.SBQQ__QuoteLine__r.Place__c;
							childOrderItem.SBQQ__ContractingMethod__c = 'Inherit';
							childOrderItem.SBQQ__QuoteLine__c = currentSub.SBQQ__QuoteLine__c;
							childOrderItem.SBQQ__RequiredBy__c = subscriptionToOrderItem.get(currentSub.SBQQ__RequiredById__c);
							childOrderItem.Subscription_Term__c = currentSub.SBQQ__QuoteLine__r.Subscription_Term__c;
							childOrderItem.Total__c = currentSub.SBQQ__QuoteLine__r.Total__c;
							childOrderItem.Package_Total__c = currentSub.SBQQ__QuoteLine__r.Package_Total__c;
							childrenOrderItems.add(childOrderItem);
						}
					}
                }
            }
            
            if(!childrenOrderItems.isEmpty()){
                OrderProductTriggerHandler.disableTrigger = true;
                insert childrenOrderItems;
                OrderProductTriggerHandler.disableTrigger = false;
            }
            
            List<OrderItem> orderItems = new List<OrderItem>();
            orderItems.addAll(fatherOrderItems);
            orderItems.addAll(childrenOrderItems);
            
            SBQQ.TriggerControl.disable(); 
			List<Order> ordersToUpdate = new List<Order>();
            for(Order currentOrder : ordersToInsert){
                if(currentOrder.Id != null){
                    currentOrder.Status = 'Activated';
					currentOrder.SBQQ__Contracted__c = true;
					ordersToUpdate.add(currentOrder);
                }
            }
            if(!ordersToUpdate.isEmpty()) update ordersToUpdate;
            for(OrderItem currentOI : orderItems){
                currentOI.SBQQ__Status__c = 'Activated';
                currentOI.SBQQ__Contracted__c = true;
            }
            update orderItems;
      
			Set<Id> orderItemIds = new Set<Id>();
			for(OrderItem oi : orderItems){
			orderItemIds.add(oi.Id);
			}
            
            Map<Id, SBQQ__Subscription__c> subsToUpdate = new Map<Id, SBQQ__Subscription__c>();
            List<OrderItem> orderItemsToUpdate = [SELECT Id, SBQQ__Subscription__c, SBQQ__Subscription__r.SBQQ__OrderProduct__c FROM OrderItem WHERE SBQQ__Subscription__r.SBQQ__Contract__r.Phase1Source__c = TRUE AND Id IN:orderItemIds ];
            for(OrderItem oi : orderItemsToUpdate){
                oi.SBQQ__Subscription__r.SBQQ__OrderProduct__c = oi.Id;
				oi.SBQQ__Subscription__r.Fixed__c = true;
                subsToUpdate.put(oi.SBQQ__Subscription__c, oi.SBQQ__Subscription__r);
            }
            if(!subsToUpdate.isEmpty()){
                update subsToUpdate.values();
            }
            SBQQ.TriggerControl.enable();
        }
    }
    
    global void finish(Database.BatchableContext info){}
    
}