/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Alessandro L. Marotta <amarotta@deloitte.it>
 * Date        : 19-12-2019
 * Sprint      :  
 * Work item   : 
 * Testclass   : Test_SharingUtility
 * Package     : 
 * Description : Utility for handling apex sharing rules
 * Changelog   : 
 */
public class SharingUtility {
    /*
   public static Boolean shareQuotesManually(Map<Id, SBQQ__Quote__c> newQuotesMap, Map<Id, SBQQ__Quote__c> oldQuotesMap, String accessLevel)
    {
        Boolean result = false;
        Set<Id> salesRepIds = new Set<Id>();
        
        Map<Id, UserTerritory2Association> u2as;
        
        Map<Id, Id> u2asByUserId = new Map<Id, Id>();
        
        Set<String> territoryDeveloperNames = new Set<String>();
        Map<Id, Group> groupsMap;
        Map<String, Map<String,Id>> groupsByTypeAndDeveloperNameMap = new Map<String, Map<String,Id>>();
        
        Map<Id, SBQQ__Quote__share> quoteSharesMap;
        Map<Id, Map<Id,Id>> qsByParentIdAndUserOrGroupIdMap;
        
        List<SBQQ__Quote__share> quoteSharingRulesToInsert = new List<SBQQ__Quote__share>();
        List<SBQQ__Quote__share> quoteSharingRulesToDelete = new List<SBQQ__Quote__share>();
        
        if(newQuotesMap != NULL && newQuotesMap.size() > 0)
        {
            for(SBQQ__Quote__c quote : newQuotesMap.values())
            {
                if(quote.SBQQ__SalesRep__c != NULL)
                    	salesRepIds.add(quote.SBQQ__SalesRep__c);
                
                if(Trigger.isUpdate && Trigger.isAfter)
                {
                    if(oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c != NULL 
                       && quote.SBQQ__SalesRep__c != oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c)
                    {
                        salesRepIds.add(oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c);
                    }
                }
            }
            
            if(salesRepIds.size() > 0)
            {
            	u2as = SEL_UserTerritory2Association.getUserTerritory2AssocationsByUserId(salesRepIds);
                
            	for(UserTerritory2Association u2a : u2as.values())
                {
                    if(!u2asByUserId.containsKey(u2a.UserId))
                    {
                        u2asByUserId.put(u2a.UserId, u2a.Id);
                    }
                    
                    if(u2a.Territory2.ParentTerritory2Id != NULL
                       && u2a.Territory2.ParentTerritory2.ParentTerritory2Id != NULL)
                    {
                    	territoryDeveloperNames.add(u2a.Territory2.ParentTerritory2.DeveloperName);
                    }
                }
                
                if(territoryDeveloperNames != NULL && territoryDeveloperNames.size() > 0)
                {
                	groupsMap = SEL_Group.getGroupsByDeveloperName(territoryDeveloperNames);
                    if(groupsMap != NULL && groupsMap.size() > 0)
                    {
                        for(Group g : groupsMap.values())
                        {
                            if(!groupsByTypeAndDeveloperNameMap.containsKey(g.DeveloperName))
                            {
                                groupsByTypeAndDeveloperNameMap.put(g.DeveloperName, new Map<String,Id>());
                            }
                            
                            if(!groupsByTypeAndDeveloperNameMap.get(g.DeveloperName).containsKey(g.Type))
                            {
                                groupsByTypeAndDeveloperNameMap.get(g.DeveloperName).put(g.Type, g.Id);
                            }
                        }
                    }
                }
            }
            
            if(Trigger.isInsert && Trigger.isAfter)
            {
                for(SBQQ__Quote__c quote : newQuotesMap.values())
                {
                    if(quote.SBQQ__SalesRep__c != NULL
                       && u2asByUserId.containsKey(quote.SBQQ__SalesRep__c))
                    {
                        Id u2aId = u2asByUserId.get(quote.SBQQ__SalesRep__c);
                        
                        if(u2as.get(u2aId).Territory2.ParentTerritory2Id != NULL
                           && u2as.get(u2aId).Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                           && groupsByTypeAndDeveloperNameMap.containsKey(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName)
                           && groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).containsKey('Territory'))
                        {
                            SBQQ__Quote__share qshare = new SBQQ__Quote__share();
                            qshare.ParentId = quote.Id;
                            qshare.UserOrGroupId = groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).get('Territory');
                            qshare.AccessLevel = accessLevel;
                            qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
                            quoteSharingRulesToInsert.add(qshare);
                        }
                    }
                }
            }else if(Trigger.isUpdate && Trigger.isAfter)
            {
                Set<Id> groupIds = new Set<Id>();
                if(groupsMap != NULL && groupsMap.size() > 0)
                	groupIds = groupsMap.keySet();
                
                quoteSharesMap = new Map<Id, SBQQ__Quote__share>([SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted 
                                                      FROM SBQQ__Quote__share 
                                                      WHERE ParentId IN :newQuotesMap.keySet() 
                                                      AND UserOrGroupId IN :groupIds]);
                
                qsByParentIdAndUserOrGroupIdMap = new Map<Id, Map<Id, Id>>();
                
                if(quoteSharesMap != NULL && quoteSharesMap.size() > 0)
                {
                    for(SBQQ__Quote__share qs : quoteSharesMap.values())
                    {
                        if(!qsByParentIdAndUserOrGroupIdMap.containsKey(qs.ParentId))
                        {
                            qsByParentIdAndUserOrGroupIdMap.put(qs.ParentId, new Map<Id,Id>());
                        }
                        
                        if(!qsByParentIdAndUserOrGroupIdMap.get(qs.ParentId).containsKey(qs.UserOrGroupId))
                        {
                            qsByParentIdAndUserOrGroupIdMap.get(qs.ParentId).put(qs.UserOrGroupId, qs.Id);
                        }
                    }
                }
                
                for(SBQQ__Quote__c quote : newQuotesMap.values())
                {
                    if(quote.SBQQ__SalesRep__c != NULL)
                    {
                        if(u2asByUserId.containsKey(quote.SBQQ__SalesRep__c))
                        {
                            if(oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c == NULL)
                            {
                                //Add Apex Sharing Territory
                                Id u2aId = u2asByUserId.get(quote.SBQQ__SalesRep__c);
                                
                                if(u2as.get(u2aId).Territory2.ParentTerritory2Id != NULL
                                   && u2as.get(u2aId).Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                                   && groupsByTypeAndDeveloperNameMap.containsKey(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName)
                                   && groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).containsKey('Territory'))
                                {
                                    SBQQ__Quote__share qshare = new SBQQ__Quote__share();
                                    qshare.ParentId = quote.Id;
                                    qshare.UserOrGroupId = groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).get('Territory');
                                    qshare.AccessLevel = accessLevel;
                                    qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
                                    quoteSharingRulesToInsert.add(qshare);
                                }
                                
                            }else if(u2asByUserId.containsKey(oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c))
                            {
                                //Check if the new sales rep's Territory is the same as the old sales rep's Territory, if not remove previous ApexSharing and Add the new onex
                                UserTerritory2Association oldU2A;
                                UserTerritory2Association newU2A;
                                Id oldU2AId = u2asByUserId.get(oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c);
                                Id newU2AId = u2asByUserId.get(newQuotesMap.get(quote.Id).SBQQ__SalesRep__c);
                                if(u2as.containsKey(oldU2AId))
                                	 oldU2A = u2as.get(oldU2AId);
                                
                                if(u2as.containsKey(newU2AId))
                                    newU2A = u2as.get(newU2AId);
                                
                                if((oldU2A != NULL && newU2A != NULL) 
                                   && newU2A.Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                                   && oldU2A.Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                                   && newU2A.Territory2.ParentTerritory2Id != oldU2A.Territory2.ParentTerritory2Id)
                                {
                                    String newTerritoryDeveloperName = newU2A.Territory2.ParentTerritory2.DeveloperName;
                                    
                                    if(qsByParentIdAndUserOrGroupIdMap.containsKey(quote.Id))
                                    {
                                        if(qsByParentIdAndUserOrGroupIdMap.get(quote.Id) != NULL && qsByParentIdAndUserOrGroupIdMap.get(quote.Id).size() > 0)
                                        {
                                            Boolean create = true;
                                            
                                            for(Id qsId : qsByParentIdAndUserOrGroupIdMap.get(quote.Id).values())
                                            {
                                                if(quoteSharesMap.containsKey(qsId))
                                                {
                                                    SBQQ__Quote__share qs = quoteSharesMap.get(qsId);
                                                    if(groupsMap.containsKey(qs.UserOrGroupId))
                                                    {
                                                        Group g = groupsMap.get(qs.UserOrGroupId);
                                                        if(!g.DeveloperName.equalsIgnoreCase(newTerritoryDeveloperName))
                                                        {
                                                            quoteSharingRulesToDelete.add(qs);
                                                        }else
                                                        {
                                                            create = false;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if(create)
                                            {
                                                Id u2aId = u2asByUserId.get(quote.SBQQ__SalesRep__c);
                                
                                                if(u2as.get(u2aId).Territory2.ParentTerritory2Id != NULL
                                                   && u2as.get(u2aId).Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                                                   && groupsByTypeAndDeveloperNameMap.containsKey(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName)
                                                   && groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).containsKey('Territory'))
                                                {
                                                    SBQQ__Quote__share qshare = new SBQQ__Quote__share();
                                                    qshare.ParentId = quote.Id;
                                                    qshare.UserOrGroupId = groupsByTypeAndDeveloperNameMap.get(u2as.get(u2aId).Territory2.ParentTerritory2.DeveloperName).get('Territory');
                                                    qshare.AccessLevel = accessLevel;
                                                    qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
                                                    quoteSharingRulesToInsert.add(qshare);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else
                        {
                            //Remove Apex Sharing Territory
                            if(qsByParentIdAndUserOrGroupIdMap != NULL && qsByParentIdAndUserOrGroupIdMap.size() > 0)
                            {
                                for(Id qsId : qsByParentIdAndUserOrGroupIdMap.get(quote.Id).values())
                                {
                                    if(quoteSharesMap.containsKey(qsId))
                                    {
                                        quoteSharingRulesToDelete.add(quoteSharesMap.get(qsId));
                                    }
                                }
                            }
                        }
                        
                    }else
                    {
                        if(oldQuotesMap.containsKey(quote.Id) && oldQuotesMap.get(quote.Id).SBQQ__SalesRep__c != NULL)
                        {
                            if(qsByParentIdAndUserOrGroupIdMap != NULL && qsByParentIdAndUserOrGroupIdMap.size() > 0)
                            {
                                //Remove Apex Sharing Territory
                                for(Id qsId : qsByParentIdAndUserOrGroupIdMap.get(quote.Id).values())
                                {
                                    if(quoteSharesMap.containsKey(qsId))
                                    {
                                        quoteSharingRulesToDelete.add(quoteSharesMap.get(qsId));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            Boolean partialResult = true;
            
            if(quoteSharingRulesToDelete != NULL && quoteSharingRulesToDelete.size() > 0)
            {
                try{
                    Database.DeleteResult[] drs = Database.delete(quoteSharingRulesToDelete);
                    
                    for(Database.DeleteResult dr: drs)
                    {
                        if(!dr.isSuccess())
                        {
                            Database.Error err = dr.getErrors()[0];
                            if(err.getStatusCode() != StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                               !err.getMessage().contains('AccessLevel')){
                                   partialResult = false;
                               }
                        }
                    }
                    
                }catch(Exception e)
                {
                    System.debug('Exception thrown: ' + e.getMessage());
                }
            }
            
            if(quoteSharingRulesToInsert != NULL && quoteSharingRulesToInsert.size() > 0)
            {
                try{
                    Database.SaveResult[] srs = Database.insert(quoteSharingRulesToInsert);
                    
                    for(Database.SaveResult sr: srs)
                    {
                        if(!sr.isSuccess())
                        {
                            Database.Error err = sr.getErrors()[0];
                            if(err.getStatusCode() != StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                               !err.getMessage().contains('AccessLevel')){
                                   partialResult = false;
                               }
                        }
                    }
                    
                    
                    
                }catch(Exception e)
                {
                    System.debug('Exception thrown: ' + e.getMessage());
                }
            }
            
            result = partialResult;
        }
        
        return result;
    } */

    public static Boolean shareLeadManually(Id recordId, Id userOrGroupId, String accessLevel){
        
        
        Boolean result = false;
        
        LeadShare leadSharingRule = new LeadShare();
        // Set the ID of record being shared.
        leadSharingRule.LeadId = recordId;
        
        // Set the ID of user or group being granted access.
        leadSharingRule.UserOrGroupId = userOrGroupId;
        
        // Set the access level.
        leadSharingRule.LeadAccessLevel = accessLevel;
        
        // Set rowCause to 'manual' for manual sharing.
        // This line can be omitted as 'manual' is the default value for sharing objects.
        //jobShr.RowCause = Schema.Lead__Share.RowCause.Manual;
        
        // Insert the sharing record and capture the save result. 
        // The false parameter allows for partial processing if multiple records passed 
        // into the operation.
        Database.SaveResult sr = Database.insert(leadSharingRule,false);
        
        // Process the save results.
        System.debug('Save result isSuccess: '+sr.isSuccess());
        if(sr.isSuccess()){
            // Indicates success
            result = true;
        } else {
            // Get first save result error.
            Database.Error err = sr.getErrors()[0];
            
            // Check if the error is related to trival access level.
            // Access level must be more permissive than the object's default.
            // These sharing records are not required and thus an insert exception is acceptable. 
            if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
               err.getMessage().contains('AccessLevel')){
                   // Indicates success.
                   result = true;
               }else{
                   // Indicates failure.
                   result = false;
               }
        }
        
        System.debug('RESULT OF MANUAL SHARING: '+result);
        return result;
    }
    
}