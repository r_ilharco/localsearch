/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Order Management - This class evaluates orders based on type, status and integration type.
* A product-specific JSON is generated for integrated orders and sent to backends; a case is created 
* for manual orders.
*
* The main method of this class is invoked by Order_Management Process Builder.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2019-10-07
* @systemLayer    Invocation
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Vincenzo Laudato
* 2020-06-19      SPIII-1250 - Termination/Cancellation order are sent as soon as they are evaluated 
*                 and no more delayed and relegated to a batch.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella
* 2020-06-14      SPIII-3214 -GAD Product - Upgrade - Missing Termination Order in case of Manual
					Product.  row 106 chaged (else if)
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class OrderUtility {
	
    public static Map<Id,Date> orderItemToNewServiceDate {get;set;}
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method retrieves needed information to evaluate orders and starts the process of order
    * management.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    recordIds    List of Ids of orders that have to be evaluated
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @InvocableMethod(label='Activate Contract' description='Activate Contract')
    public static void activateContracts(List<Id> recordIds){
        
        List<Order> orderList = SEL_Order.getOrdersByIds(recordIds);
        
        if(!orderList.isEmpty()) changeOrderStatus(orderList, false);   
    }

    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method retrieves needed information to evaluate orders and starts the process of order
    * management.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    recordIds                 List of orders records that have to be evaluated
    * @input param    upDownAfterTermination    Boolean that defines if the upgrade-termination order
                                                has been already fulfilled.
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void changeOrderStatus(List<Order> orders, Boolean upDownAfterTermination){
		
        List<Error_Log__c> errorLogsToInsert = new List<Error_Log__c>();
        
        Set<String> queryFields = ConstantsUtil.QUERY_FIELDS;

        Set<Id> ordersId = new Set<Id>();
        
        List<OrderItem> orderItems = new List<OrderItem>();
        Map<Id, Set<OrderItem>> orderToOrderItems = new Map<Id, Set<OrderItem>>();
        Map<Id, Order_Management__mdt> orderToOrderMgmtMdt = new Map<Id, Order_Management__mdt>();
        
        SRV_OrderUtil.UpgradeUtilityWrapper upgradeUtilityWrapper = new SRV_OrderUtil.UpgradeUtilityWrapper();
        SRV_OrderUtil.ActivationOrderIdsWrapper activationOrderIdsWrapper = new SRV_OrderUtil.ActivationOrderIdsWrapper();
        SRV_OrderUtil.IntegratedOrdersWrapper integratedOrdersWrapper = new SRV_OrderUtil.IntegratedOrdersWrapper();

        List<Order> integratedOrders = new List<Order>();
        Set<Id> orderToCreateCaseIds = new Set<Id>();
        
        List<Order> upgradeTerminationOrders = new List<Order>();
        Set<Id> contractIds = new Set<Id>();

        List<OrderItem> activationOIToUpdate = new List<OrderItem>();
        List<OrderItem> amendmentOIToUpdate = new List<OrderItem>();
        List<OrderItem> termCancOIToUpdate = new List<OrderItem>();

        for(Order currentOrder : orders) ordersId.add(currentOrder.Id);
		
        try{
            orderItems = SEL_OrderItem.getOrderItemsByOrderIds(ordersId, queryFields);
            for(OrderItem oi : orderItems) SRV_OrderUtil.generateSetOIValueMap(orderToOrderItems, oi.OrderId, oi);
        }
        catch(Exception e){
            errorLogsToInsert.add(LogUtility.generateErrorLog('OrderUtility', 'changeOrderStatus', e, null, null, e.getStackTraceString()));
            return;
        }

        for(Order currentOrder : orders){
			            
            Boolean isActivationCT = ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(currentOrder.Custom_Type__c);
            Boolean isTerminationCT = ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(currentOrder.Custom_Type__c);
            Boolean isUpgradePT = ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(currentOrder.Type);

            Boolean isUpDown = isUpgradePT && isActivationCT;
			
            try{
                List<Order_Management__mdt> orderManagementMetadata = SRV_OrderUtil.getOrderManagementMdt(orderToOrderItems.get(currentOrder.Id));
                
                if(!orderManagementMetadata.isEmpty()){
                    if(SRV_OrderUtil.checkForManualProducts(orderManagementMetadata[0])){
                        if(isUpDown && !upDownAfterTermination) SRV_OrderUtil.evaluateUpgradeOrder(currentOrder, orderToOrderItems.get(currentOrder.Id), upgradeUtilityWrapper);
                        else orderToCreateCaseIds.add(currentOrder.Id);
                    }
                    else if(isUpDown && !upDownAfterTermination) SRV_OrderUtil.evaluateUpgradeOrder(currentOrder, orderToOrderItems.get(currentOrder.Id), upgradeUtilityWrapper);
                    else{
                        integratedOrders.add(currentOrder);
                        orderToOrderMgmtMdt.put(currentOrder.Id, orderManagementMetadata[0]);
                    }
                    
                    if(isTerminationCT && isUpgradePT){
                        contractIds.add(currentOrder.Contract__c);
                        upgradeTerminationOrders.add(currentOrder);
                    }
                }
                else{
                    String exceptionMessage = Label.OrderUtility_OrderMgmtMissing.replace('{orderId}', currentOrder.Id).replace('{orderStatus}', currentOrder.Status);
                    throw new CustomException(exceptionMessage);
                }
            }
            catch(CustomException ce){
                errorLogsToInsert.add(LogUtility.generateErrorLog('OrderUtility', 'changeOrderStatus', ce, null, currentOrder.Id, null));
            }
            catch(Exception e){
                errorLogsToInsert.add(LogUtility.generateErrorLog('OrderUtility', 'changeOrderStatus', e, null, currentOrder.Id, null));
            	return;
            }
        }

        if(!integratedOrders.isEmpty()){
           	Map<Id, Set<OrderItem>> orderIdToOrderItemsFinal = SRV_OrderUtil.getOrderItemsToSend(integratedOrders, orderToOrderItems, integratedOrdersWrapper, activationOrderIdsWrapper, upDownAfterTermination);
            Map<Id, OrderItem> finalOrderItemMap = new Map<Id, OrderItem>();
            for(Id key : orderIdToOrderItemsFinal.keySet()){
                for(OrderItem value : orderIdToOrderItemsFinal.get(key)){
                    finalOrderItemMap.put(value.Id, value);
                }
            }
            SRV_OrderUtil.sendIntegratedOrders(integratedOrders, orderIdToOrderItemsFinal, orderToOrderItems, orderToOrderMgmtMdt, integratedOrdersWrapper, activationOrderIdsWrapper, finalOrderItemMap);
        }
		
        if(!integratedOrdersWrapper.ordersToUpdate.isEmpty()) update integratedOrdersWrapper.ordersToUpdate;
        if(!integratedOrdersWrapper.orderItemsToUpdate.isEmpty()) update integratedOrdersWrapper.orderItemsToUpdate;
        
        if(!upgradeUtilityWrapper.orderIdToContractId.isEmpty() && !upgradeUtilityWrapper.idToOrder.isEmpty()){
            
            Savepoint checkpoint;
			if(!Test.isRunningTest()) checkpoint = Database.setSavepoint();
            
            try{
                if(!upgradeUtilityWrapper.subsIdToUpdate.isEmpty() && !upgradeUtilityWrapper.subIdToOrderItem.isEmpty()){
                    List<SBQQ__Subscription__c> subsToUpdate = SEL_Subscription.getSubsById(upgradeUtilityWrapper.subsIdToUpdate);
                    if(!subsToUpdate.isEmpty()){
                        SRV_OrderUtil.terminateSubscriptionsAndContract(subsToUpdate, upgradeUtilityWrapper);
                    }
                }
                else throw new CustomException('Check Subscription_To_Terminate__c field on Termination OrderItems');
                SRV_OrderUtil.terminationOrderCreation(upgradeUtilityWrapper, orderToOrderItems);
            }
            catch(CustomException ce){
                if(!Test.isRunningTest()) Database.rollback(checkpoint);
                Set<String> orderIdsErrorMgmt = (Set<String>)JSON.deserialize(JSON.serialize(upgradeUtilityWrapper.idToOrder.keySet()), Set<String>.class);
                String orderIds = String.join(new List<String>(orderIdsErrorMgmt), ',');
                LogUtility.generateErrorLog('OrderUtility', 'changeOrderStatus', ce, null, orderIds, null);
            }
            catch(Exception e){
                if(!Test.isRunningTest()) Database.rollback(checkpoint);
                Set<String> orderIdsErrorMgmt = (Set<String>)JSON.deserialize(JSON.serialize(upgradeUtilityWrapper.idToOrder.keySet()), Set<String>.class);
                String orderIds = String.join(new List<String>(orderIdsErrorMgmt), ',');
                LogUtility.generateErrorLog('OrderUtility', 'changeOrderStatus', e, null, orderIds, null);
            }
        }
		
        if(!contractIds.isEmpty() && !upgradeTerminationOrders.isEmpty()){
            List<Case> caseToInsert = SRV_OrderUtil.createSambaCase(upgradeTerminationOrders, contractIds);
            if(!caseToInsert.isEmpty()) insert caseToInsert;
        }
        
        if(!orderToCreateCaseIds.isEmpty()) OrderCaseUtility.generateCase(orderToCreateCaseIds);
        if(!errorLogsToInsert.isEmpty()) insert errorLogsToInsert;
    }

    public class CustomException extends Exception {}
}