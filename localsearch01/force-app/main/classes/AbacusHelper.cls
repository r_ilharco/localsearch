public class AbacusHelper {
	
    static string abaLoginEndpoint = ConstantsUtil.BILLING_ESB_ABACUS_ENDPOINT + '/login';
    static string abaLogoutEndpoint = ConstantsUtil.BILLING_ESB_ABACUS_ENDPOINT + '/logout';

    private static integer[] esrMod10Sngl;
    
    @testvisible
    private static integer[] EsrMod10 {
        get {
            if(esrMod10Sngl == null) {
                esrMod10Sngl = new integer[10];
                for(integer ix = 0, slen = ConstantsUtil.BILLING_ESR_MOD10.length(); ix < slen; ix++) {
                    esrMod10Sngl[ix] = integer.valueOf(ConstantsUtil.BILLING_ESR_MOD10.mid(ix, 1));
                }
            }
            return esrMod10Sngl;
        }
    }
        
    public static string abacusLoginToken() {
        
        string soapNS = ConstantsUtil.XMLNS_SOAP;
        string xsi = ConstantsUtil.XMLNS_SCHEMA_INSTANCE;
        string abaTypesNS = ConstantsUtil.XMLNS_ABA_CONNECT_TYPES;
        Billing_Setting__mdt setting = BillingHelper.setting;
        
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'SOAP-ENV');
        envelope.setNamespace('xsi', xsi);
        Dom.XmlNode body = envelope.addChildElement('Body', soapNS, 'SOAP-ENV');       
        Dom.XmlNode userlogin = body.addChildElement('Login', abaTypesNS, 'ns1').addChildElement('UserLogin', abaTypesNS, 'ns1');
        userlogin.addChildElement('UserName', abaTypesNS, 'ns1').addTextNode(setting.Abacus_User__c);
        userlogin.addChildElement('Password', abaTypesNS, 'ns1').addTextNode(setting.Abacus_Password__c);
        userlogin.addChildElement('Mandant', abaTypesNS, 'ns1').addTextNode(setting.Abacus_Mandant__c);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(abaLoginEndpoint);
        req.setHeader('Content-Type', 'text/xml');    
        req.setHeader('SOAPAction', 'login');
        req.setBodyDocument(doc);
        Http http = new Http();
        HttpResponse res = http.send(req);

        if(res.getStatusCode() != 200) {
            throw new AbacusHelperException('HTTP Error:' + res.getStatusCode() + '-' + res.getStatus() + ':' + res.getBody(), ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
        }
        Dom.Document resDoc = getDocumentFromMultipartMime(res);
        envelope = resDoc.getRootElement();
        Dom.XmlNode xmlBody = envelope.getChildElement('Body', soapNS);
        Dom.XmlNode xmlFault = xmlBody.getChildElement('Fault', soapNS);
		AbacusHelperException fault = checkFaults(xmlBody);
        if(fault != null) {
            throw fault;
        }
        Dom.XmlNode loginResponse = xmlBody.getChildElement('LoginResponse', abaTypesNS);
        Dom.XmlNode loginToken = loginResponse.getChildElement('LoginToken', abaTypesNS);
        return loginToken.getText();
    }
	
    public static void abacusLogout(string abaToken) {
        string soapNS = ConstantsUtil.XMLNS_SOAP;
        string xsi = ConstantsUtil.XMLNS_SCHEMA_INSTANCE;
        string abaTypesNS = ConstantsUtil.XMLNS_ABA_CONNECT_TYPES;
        
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'SOAP-ENV');
        envelope.setNamespace('xsi', xsi);
        envelope
            .addChildElement('Body', soapNS, 'SOAP-ENV')
        	.addChildElement('Logout', abaTypesNS, 'ns1')
            .addChildElement('LoginToken', abaTypesNS, 'ns1')
            .addTextNode(abaToken);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(abaLogoutEndpoint);
        req.setHeader('Content-Type', 'text/xml');    
        req.setHeader('SOAPAction', 'logout');
        req.setBodyDocument(doc);
        Http http = new Http();
        HttpResponse res = http.send(req);

        if(res.getStatusCode() != 200) {
            throw new AbacusHelperException('HTTP Error:' + res.getStatusCode() + '-' + res.getStatus() + ':' + res.getBody(), ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
        }
        Dom.Document resDoc = getDocumentFromMultipartMime(res);
        envelope = resDoc.getRootElement();
        Dom.XmlNode xmlBody = envelope.getChildElement('Body', soapNS);
		AbacusHelperException fault = checkFaults(xmlBody);
        if(fault != null) {
            throw fault;
        }
    }
    
    // Strips the XML document form the whole message body.
    // Abacus may return XML as a multipart mime response, instead of a single response with only the XML document.
    public static Dom.Document getDocumentFromMultipartMime(HttpResponse res) {
        // content is returned as multipart mime. Must return only xml body of first part
        try {
            List<string> ctypeParts = res.getHeader('Content-Type').split(';');
            if(ctypeParts[0] == 'multipart/related') {
                string boundary;
                for(string ctypePart : ctypeParts) {
                    List<string> keyValue = ctypePart.trim().split('=');
                    if(keyValue[0] == 'boundary') {
                        boundary = '--' + keyValue[1].substringBetween('"', '"');
                        break;
                    }
                }
                string mimePart = res.getBody().substringBetween(boundary, boundary + '--');
                Dom.Document resDoc = new Dom.Document();
                resDoc.load(mimePart.split('\\r?\\n\\r?\\n')[1]);
                return resDoc;
            } else {
                return res.getBodyDocument();
            }
        } catch (Exception ex) {
            throw new AbacusHelperException(ex, 'Error parsing response', ErrorHandler.ErrorCode.E_UNKNOWN);
        }
    }

    public static AbacusHelperException checkFaults(Dom.XmlNode xmlBody) {
        string soapNS = ConstantsUtil.XMLNS_SOAP;
        string xsi = ConstantsUtil.XMLNS_SCHEMA_INSTANCE;
        string abaTypesNS = ConstantsUtil.XMLNS_ABA_CONNECT_TYPES;
        
        Dom.XmlNode xmlFault = xmlBody.getChildElement('Fault', soapNS);
        if(xmlFault != null) {
            // Fault response (faultcode, faultstring, detail)
            Dom.XmlNode abaFault = xmlFault.getChildElement('detail', null).getChildElement('AbaConnectFault', abaTypesNS);
            // AbaFault (Code, Message, Cause (Opt))
            if(abaFault != null) {
            	return new AbacusHelperException(FaultType.ABACUS,  
                    getElementText(abaFault, 'Message', abaTypesNS),
                	getElementText(abaFault, 'Code', abaTypesNS),
                	getElementText(abaFault, 'Cause', abaTypesNS),
                    ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT
                );
            } else {
            	return new AbacusHelperException(
                    FaultType.SOAP,
                    getElementText(xmlFault, 'faultcode', null),
                    getElementText(xmlFault, 'faultstring', null),
                    getElementText(xmlFault, 'detail', null),
                    ErrorHandler.ErrorCode.E_SOAP_FAULT
                );                
            }
        }
        return null;
    }
    
    public static string toISO8601String(Date value) {
        if(value == null) {
            return null;
        } 
        return value.year() + '-' + string.valueOf(value.month()).leftPad(2, '0') + '-' + string.valueOf(value.day()).leftPad(2, '0');
    }
    
    public static Date validTaxMonth(Date dt) {
        if(dt == null) return null;
        Date today = Date.today();
        if(dt.monthsBetween(today) > 0) {
            return today;
        }
        return dt;
    }

    public static string convertTaxCode(string sfTaxCode) {
        if(sfTaxCode == '30' || sfTaxCode == '31' || sfTaxCode == '32') {
            return sfTaxCode;
        }
        if(sfTaxCode == ConstantsUtil.BILLING_TAX_CODE_LOW) {
            return '32';
        }
        return '31';
    }
    
    // Used to generate the ESR value plus final check digit. Algorithm (Modulo 10) at http://www.hosang.ch/modulo10.aspx
    public static string getESRValue(string customerNumber, string invoiceNumber) {
        string fullString = ConstantsUtil.BILLING_ESR_PREFIX + customerNumber.leftPad(10, '0') + invoiceNumber.leftPad(10, '0');
        integer esr = 0;
        for(integer i=0; i < fullString.length(); i++) {
            esr =  EsrMod10[Math.mod(esr + integer.valueOf(fullString.mid(i, 1)), 10)];
            if(esr == null){
                esr = 0;
            }
        }
        esr = Math.mod(10 - esr, 10);
        return fullstring + esr;
    }
    
    public static string getAbaCustomer(Invoice__c invoice) {
        if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK && invoice.Process_Mode__c == ConstantsUtil.INVOICE_PROCESS_MODE_SWISSLISTMIG) {
            if(invoice.Open_Amount__c == 0) {
                return '199999996';
            }
            return '199999997';
        } else {
            return '199999998';
        }  
    }
    
    @testvisible
    private static string getElementText(Dom.XmlNode parent, string element, string namespace) {
        if(parent == null) {
            return '';
        }
        Dom.XmlNode node = parent.getChildElement(element, namespace);
        if(node == null) {
            return '';
        }
        return node.getText();
    }
    
    public enum FaultType { SOAP, ABACUS }
    
    public class AbacusHelperException extends ErrorHandler.GenericException {
        public FaultType FaultType {get; set;}
        public string FaultDetail {get; set;}
        public string FaultMessage {get; set;}
        public string FaultCode {get; set;}
        
        public AbacusHelperException(Exception initException, string message, ErrorHandler.ErrorCode errorCode) {
            super(initException, message, errorCode);
        }

        public AbacusHelperException(string message, ErrorHandler.ErrorCode errorCode) {
            super(message, errorCode);
        }
        
        public AbacusHelperException(FaultType faultType, string message, string faultCode, string detail, ErrorHandler.ErrorCode errorCode) {
            super(FaultType.Name() + ' Fault: ' + errorCode + ' - ' + message + ':' + detail, errorCode);
            this.FaultMessage = message;
            this.FaultType = faultType;
            this.FaultDetail = detail;
            this.FaultCode = faultCode;
        }        
    }    
}