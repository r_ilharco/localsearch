public class ValidateAddressResponseWrapper {
    
    public String result{get;set;}
    public String source{get;set;}
    public AttributesObj attributes{get;set;}
    public ErrorObj error{get;set;}
    public String request_date {get; set;}
    public String response_date {get; set;}
    public Map<String, String> params {get; set;}
    
    public ValidateAddressResponseWrapper(){
        params = new Map<String,String>();
        attributes = new AttributesObj();
        error = new ErrorObj();       
    }
    
    public class AttributesObj{
        public String company_name{get;set;}
        public String start_vat_obligation{get;set;}
        public String hrid{get;set;}
        public String vatid{get;set;}
        public String legal_form{get;set;}
        public String uid{get;set;}
        public String mobile{get;set;}
        public String fax{get;set;}
        public String phone{get;set;}
        public AddressObj address{get;set;}       
    }
    
    public class AddressObj{
        public String street{get;set;}
        public String location{get;set;}
        public Integer zip {get; set;}
        public String street_number{get;set;}
        public String country{get; set;}
    }
    
    public class ErrorObj{
        
        public ErrorObj(){
            error_code = '';
        	error_description = '';
		}
        
        public ErrorObj(ErrorObj errObj)
        {
            if(errObj != NULL)
            {
                error_code = errObj.error_code;
                error_description = errObj.error_description;
			}else
            {
                error_code = '';
        		error_description = '';
            }
		}
        
        public String error_code{get;set;}
        public String error_description{get;set;}
    }
}