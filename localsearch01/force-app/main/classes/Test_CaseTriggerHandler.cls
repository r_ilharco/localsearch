@IsTest public class Test_CaseTriggerHandler {

    public static testmethod void CaseTriggerHandlerTest1() {

        List<Case> cList = new List<Case>();
        List<Case> cList2 = new List<Case>();
        List<Case> cList3 = new List<Case>();
        List<Case> cList4 = new List<Case>();

        Account customerAcc = new Account(Name='customerAccount');
        customerAcc.Type = 'Customer';
        customerAcc.PreferredLanguage__c= 'German';
        customerAcc.Status__c = 'New';
        customerAcc.BillingCity = 'Zürich';
        customerAcc.BillingCountry = 'Switzerland';
        customerAcc.BillingPostalCode = '8005';
        customerAcc.BillingStreet = 'Förrlibuckstrasse, 62';
        customerAcc.Phone = '+39999999';
        customerAcc.Email__c = 'oggi@ciao.com.net';
        customerAcc.Customer_Number__c = '370980914380193402183823752875028734832';
        insert customerAcc;

        Contact secondCont = new Contact(LastName='secondContact');
        secondCont.MailingCity = 'Zürich';
        secondCont.MailingCountry = 'Switzerland';
        secondCont.MailingPostalCode = '8005';
        secondCont.MailingStreet = 'Förrlibuckstrasse, 62';
        secondCont.AccountId = customerAcc.Id;
        secondCont.Phone = '+35555555';
        secondCont.Email = 'albero@bello.com.net';
        insert secondCont;

        Contact primaryCont = new Contact(LastName='primaryContact');
        primaryCont.MailingCity = 'Zürich';
        primaryCont.MailingCountry = 'Switzerland';
        primaryCont.MailingPostalCode = '8005';
        primaryCont.MailingStreet = 'Förrlibuckstrasse, 62';
        primaryCont.AccountId = customerAcc.Id;
        primaryCont.Primary__c = true;
        insert primaryCont;

        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;

        User u = new user();
        u.LastName = 'mylastName';
        u.Email = 'testclass@test.class';
        u.Alias = 'tclass';
        u.Username = 'testclass@test.class';
        u.CommunityNickname = 'testclass@test.class';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u; 

        test.startTest();
            Case case1 = new Case();
            case1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Customer Data').getRecordTypeId();
            case1.Status = 'New';
            case1.AccountId = customerAcc.Id;
            case1.Origin = 'Phone';
            case1.Topic__c = 'Change Customer Data';
            //case1.Sub_Topic__c = 'Quality-Check';
            case1.Language__c = 'German';
            cList.add(case1);

            Case case2 = new Case();
            case2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Other').getRecordTypeId();
            case2.Status = 'New';
            case2.ContactId = primaryCont.Id;
            case2.Origin = 'Email';
            case2.Language__c = 'German';
            cList.add(case2);

            insert cList;     

            Case case3 = new Case();
            case3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Customer Data').getRecordTypeId();
            case3.Status = 'New';
            case3.AccountId = customerAcc.Id;
            case3.Origin = 'Phone';
            case3.Topic__c = 'Change Customer Data';
            //case3.Sub_Topic__c = 'Quality-Check';
            case3.Language__c = 'French';
            cList2.add(case3);

            Case case4 = new Case();
            case4.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Other').getRecordTypeId();
            case4.Status = 'New';
            case4.SuppliedEmail = 'albero@bello.com.net';
            case4.Origin = 'Email';
            case4.Subject = 'GE03I';
            case4.Sub_Topic__c = 'Contract Copy';
            cList2.add(case4);

            insert cList2;

            Case case5 = new Case();
            case5.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case5.Status = 'New';
            case5.SuppliedEmail = 'oggi@ciao.com.net';
            case5.Origin = 'Web';
            case5.Topic__c = 'Contract Information and Changes';
            case5.Sub_Topic__c = 'Replacement';
            case5.Language__c = 'French';
            cList3.add(case5);

            Case case6 = new Case();
            case6.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case6.Status = 'New';
            case6.SuppliedPhone = '+39999999';
            case6.Origin = 'Web';
            case6.Topic__c = 'Contract Information and Changes';
            case6.Sub_Topic__c = 'Replacement';
            case6.Language__c = 'French';
            cList3.add(case6);

            Case case7 = new Case();
            case7.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case7.Status = 'New';
            case7.SuppliedEmail = 'albero@bello.com.net';
            case7.Origin = 'Web';
            case7.Topic__c = 'Finance';
            case7.Language__c = 'French';
            cList3.add(case7);

            Case case8 = new Case();
            case8.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case8.Status = 'New';
            case8.SuppliedPhone = '+35555555';
            case8.Origin = 'Web';
            case8.Topic__c = 'Finance';
            case8.Case_Level__c= 'Customer_Care';
            case8.Language__c = 'French';
            cList3.add(case8);

            Case case9 = new Case();
            case9.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Support').getRecordTypeId();
            case9.Status = 'New';
            case9.SuppliedEmail = 'testclass@test.class';
            case9.Origin = 'Web';
            case9.Topic__c = 'Other';
            case9.Language__c = 'French';
            cList3.add(case9);

            Case case10 = new Case();
            case10.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case10.Status = 'New';
            case10.Customer_Number__c = '370980914380193402183823752875028734832';
            case10.Origin = 'Web';
            case10.Topic__c = 'Finance';
            case10.Language__c = 'French';
            cList3.add(case10);
        
        	Case case11 = new Case();
            case11.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case11.Status = 'New';
            case11.SuppliedEmail = 'testclass@test.class';
            case11.Origin = 'Manual';
            case11.Topic__c = 'Finance';
            case11.Language__c = 'French';
            case11.Run_Assignment__c = true;
            cList3.add(case11);

            insert cList3;

            case1.Case_Level__c= 'Complaint_Management';
            case1.Forwarded__c= 'Yes';
        	case1.Status = 'Pending';
        	case1.Pending_Reason__c = 'pending customer';

            case2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case2.Topic__c = 'Contract Information and Changes';
            case2.Sub_Topic__c = 'Contract Copy';

            case8.Case_Level__c= '';
            
        	case3.Status = 'Closed';
        	case3.Closure_Reason__c = 'Solved';

            cList4.add(case1);
            cList4.add(case2);
            cList4.add(case8);
        	cList4.add(case3);

            update cList4; 

        test.stopTest();
            
        }
}