/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Mara Mazzarella <mamazzarella@deloitte.it>
 * Date		   : 07-10-2019
 * Sprint      : 10
 * Work item   : 
 * Testclass   : Test_OrderTrigger
 * Package     : 
 * Description : Trigger Helper on Order
 * Changelog   : 
 */

public class OrderTriggerHelper {
    
    public static void updateSignatureAndXactlyFields(List<Order> newItems) {
        
        Set<Id> contractsToQuery = new Set<Id>();
        Set<Id> quoteIds = new Set<Id>();
        
        List<Order> autoRenewalOrders = new List<Order>();
        List<Order> allOtherOrders = new List<Order>();
        List<Order> cancellationTerminationOrders = new List<Order>();
        
        Map<Id, SBQQ__Quote__c> quotesMap = new Map<Id, SBQQ__Quote__c>();
        Map<Id, Contract> contractsMap = new Map<Id, Contract>();
        
        Map<Id, Contract> contractsToUpdate = new Map<Id, Contract>();
        
        for(Order o : newItems){
            
            Boolean isAutoRenewal = ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL.equalsIgnoreCase(o.Type);
            Boolean isCancellationType = ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(o.Type);
            Boolean isTerminationType = ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(o.Type);
            
            Boolean isNew = ConstantsUtil.ORDER_TYPE_NEW.equalsIgnoreCase(o.Type);
            Boolean isUpgrade = ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(o.Type);
            Boolean isReplacement = ConstantsUtil.ORDER_TYPE_REPLACEMENT.equalsIgnoreCase(o.Type);
            Boolean isAmendment = ConstantsUtil.ORDER_TYPE_AMENDMENT.equalsIgnoreCase(o.Type);

            if(String.isNotBlank(o.Contract__c)){
                if(isAutoRenewal){
                    autoRenewalOrders.add(o);
                    contractsToQuery.add(o.Contract__c);
                }
                else if(isCancellationType || isTerminationType){
                    cancellationTerminationOrders.add(o);
                    contractsToQuery.add(o.Contract__c);
                }
            }
            if(String.isNotBlank(o.SBQQ__Quote__c)){
                if(isNew || isAmendment || isUpgrade || isReplacement){
                    quoteIds.add(o.SBQQ__Quote__c);
                    allOtherOrders.add(o);
                }
                if(String.isNotBlank(o.Contract__c)) contractsToQuery.add(o.Contract__c);
            }
        }
        
        if(!quoteIds?.isEmpty()) quotesMap = SEL_Quote.getQuoteById(quoteIds);
        if(!contractsToQuery?.isEmpty()) contractsMap = SEL_Contract.getContractsByIds(contractsToQuery);
        
        if(!autoRenewalOrders?.isEmpty()){
            for(Order o : autoRenewalOrders){
                o.OwnerAtAutorenewal__c = contractsMap?.get(o.Contract__c)?.Account.OwnerId;
                o.OwnerOnSignature__c = contractsMap?.get(o.Contract__c)?.OwnerOnSignature__c;
                o.CompanySignedBy__c = contractsMap?.get(o.Contract__c)?.CompanySignedId;
                o.CompanySignedDate__c = contractsMap?.get(o.Contract__c)?.CompanySignedDate;
                o.OriginalSeller__c = contractsMap?.get(o.Contract__c)?.CompanySignedId;
                Contract c = new Contract();
                c.Id = o.Contract__c;
                c.OwnerAtAutorenewal__c = contractsMap?.get(o.Contract__c)?.Account.OwnerId;
                contractsToUpdate.put(c.Id, c);
            }
        }
        if(!cancellationTerminationOrders?.isEmpty()){
            for(Order o : cancellationTerminationOrders){
                o.CompanySignedBy__c = quotesMap?.get(o.SBQQ__Quote__c)?.SBQQ__SalesRep__c;
                o.CompanySignedDate__c = quotesMap?.get(o.SBQQ__Quote__c)?.Contract_Signed_Date__c;
                o.OwnerAtAutorenewal__c = contractsMap?.get(o.Contract__c)?.OwnerAtAutorenewal__c;
                o.OwnerOnSignature__c = contractsMap?.get(o.Contract__c)?.OwnerOnSignature__c;
                o.OriginalSeller__c = quotesMap?.get(o.SBQQ__Quote__c)?.SBQQ__SalesRep__c;
            }
        }
        if(!allOtherOrders.isEmpty()){
            for(Order o : allOtherOrders){
                
                Boolean isActivation = ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(o.Custom_Type__c);
                Boolean isCancellation = ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(o.Custom_Type__c);
            	Boolean isTermination = ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(o.Custom_Type__c);
                
                Boolean isUpgrade = ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(o.Type);
                Boolean isReplacement = ConstantsUtil.ORDER_TYPE_REPLACEMENT.equalsIgnoreCase(o.Type);
                
                o.CompanySignedBy__c = quotesMap?.get(o.SBQQ__Quote__c)?.SBQQ__SalesRep__c;
                o.CompanySignedDate__c = quotesMap?.get(o.SBQQ__Quote__c)?.Contract_Signed_Date__c;
                o.OwnerOnSignature__c = quotesMap?.get(o.SBQQ__Quote__c)?.AccountOwner__c;
                if(isActivation) o.OriginalSeller__c = quotesMap?.get(o.SBQQ__Quote__c)?.SBQQ__SalesRep__c;
                if(isCancellation || isTermination) o.OriginalSeller__c = contractsMap?.get(o.Contract__c)?.CompanySignedId;
                if(isActivation && (isUpgrade || isReplacement)){
                    o.AccountOwner__c = quotesMap?.get(o.SBQQ__Quote__c)?.AccountOwner__c;
                    o.RegionalDirector__c = quotesMap?.get(o.SBQQ__Quote__c)?.RegionalDirector__c;
                    o.SalesManagerDeputy__c = quotesMap?.get(o.SBQQ__Quote__c)?.SalesManagerDeputy__c;
                    o.SalesManager__c = quotesMap?.get(o.SBQQ__Quote__c)?.SalesManager__c;
                }
            }
        }
        
        if(!contractsToUpdate.isEmpty()){
            ContractTriggerHandler.disableTrigger = true;
            update contractsToUpdate.values();
            ContractTriggerHandler.disableTrigger = false;
        }
    }
        
    // SPIII-4486 *Implicit Renewals, Termination & Cancellation Orders type only. Other Order types get account owner territory hierarchy information from Quote via twin fields.
    public static void updateTerritoryInformation(List<Order> newItems){
        
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Account> accountsMap;
        Set<Id> userIdsForTerritoryInformationSet = new Set<Id>();
        SRV_UserTerritory2Association.UserTerritoryInformation utInformation;
        
        for(Order o : newItems){
            if(o.type == ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL || o.type == ConstantsUtil.ORDER_TYPE_TERMINATION || o.type == ConstantsUtil.ORDER_TYPE_CANCELLATION){
                accountIds.add(o.AccountId);
            }
        }
        
        if(accountIds != NULL && !accountIds.isEmpty()) accountsMap = SEL_Account.getAccountsById(accountIds);
        
        if(accountsMap != NULL && !accountsMap.isEmpty()){
            for(Order o : newItems){
                Id accountOwnerId = NULL;                
                if(o.AccountId != NULL && accountsMap.containsKey(o.AccountId)){
                    if(accountsMap.get(o.AccountId).OwnerId != NULL){
                        o.AccountOwner__c = accountsMap.get(o.AccountId).OwnerId;
                        accountOwnerId = accountsMap.get(o.AccountId).OwnerId;
                        userIdsForTerritoryInformationSet.add(o.AccountOwner__c);
                    }
                }
            }
        }        
        if(userIdsForTerritoryInformationSet != NULL && !userIdsForTerritoryInformationSet.isEmpty()){
            utInformation = SRV_UserTerritory2Association.getUserTerritoryHierarchyByUserIds(userIdsForTerritoryInformationSet);
            if(utInformation != NULL && utInformation.ut2asByUserIdMap != NULL && utInformation.ut2asByUserIdMap.size() > 0){
                for(Order o : newItems){
                    Id territoryUser = NULL;
                    if(o.AccountOwner__c != NULL){
                        territoryUser = o.AccountOwner__c;
                        if(territoryUser != NULL && utInformation.ut2asByUserIdMap.containsKey(territoryUser)){
                            UserTerritory2Association ut2a = utInformation.ut2aMap.get(utInformation.ut2asByUserIdMap.get(territoryUser));
                            if(utInformation.ut2aByTerritoryIdAndRoleMap != NULL){
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2.ParentTerritory2Id) 
                                   && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                                   && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI)))
                                    o.RegionalDirector__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI)).UserId;
                                system.debug('====> ' +o.RegionalDirector__c);
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id) 
                                   && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)
                                   && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA)))
                                    o.SalesManager__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA)).UserId;
                                system.debug('====> ' +o.SalesManager__c);
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id) 
                                   && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD)
                                   && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD)))
                                    o.SalesManagerDeputy__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD)).UserId;
                                system.debug('====> ' + o.SalesManagerDeputy__c);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public static List<Order> checkIfTerminationOrderClosed(Map<Id, Order> newItems, Map<Id,Order > oldItems) {
        Set<Id> orderToCheck = new Set<Id>();
    	for(Id orderId : newItems.keySet()){
            if(newItems.get(orderId).Status != oldItems.get(orderId).Status 
              && newItems.get(orderId).Manual_Activation_Roll__c > 0
              && ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(newItems.get(orderId).Status)
              && ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(newItems.get(orderId).Custom_Type__c) 
              && (ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(newItems.get(orderId).Type) ||
                  ConstantsUtil.ORDER_TYPE_DOWNGRADE.equalsIgnoreCase(newItems.get(orderId).Type) )){
                orderToCheck.add(orderId);
            }
        }
        if(!orderToCheck.isEmpty()){
            System.debug('checkIfTerminationOrderClosed');
            List<Order> childs = SEL_Order.getChildOrder(orderToCheck,ConstantsUtil.LIST_ORDER_STATUS,ConstantsUtil.LIST_ORDER_TYPE,ConstantsUtil.ORDER_TYPE_TERMINATION);
            System.debug('childs '+childs);
            if(!childs.isEmpty()){
                Set<Id> errorIds = new Set<Id>();
                for(Order o: childs){
                    errorIds.add(o.Parent_Order__c);
                }
                for(Order o : newItems.values()){
                    if(errorIds.contains(o.id)){
                        if(!Test.isRunningTest()) o.addError(Label.Termination_Order_Before);
                    }
                }
         	}        
         }
        return newItems.values();
    } 

    //OAVERSANO 20191011 Enhancement SF2-344 -- START
    public static void updateNewCustomerFlag(List<Order> newItems) {
        Set<Id> accountToCheck = new Set<Id>();
    	for(Order ord : newItems){
            if(String.isNotBlank(ord.AccountId))
                accountToCheck.add(ord.AccountId);
        }
        if(!accountToCheck.isEmpty()){
            System.debug('- - updateNewCustomerFlag - -');
            System.debug('accountToCheck: '+accountToCheck);
            Map<Id, Account> accountNewCustomerValue = new Map<Id, Account>([SELECT Id, New_Customer__c FROM Account WHERE Id IN:accountToCheck]);
            System.debug('accountNewCustomerValue: '+accountNewCustomerValue);
            if(!accountNewCustomerValue.isEmpty()){
                for(Order o : newItems){
                    if(o.AccountId != null){
                        if(accountNewCustomerValue.containsKey(o.AccountId)){
                            o.New_Customer__c = accountNewCustomerValue.get(o.AccountId).New_Customer__c;
                        } 
                    }
                }
         	}        
         }
    }
    //OAVERSANO 20191011 Enhancement SF2-344 -- END
    
    //07/12/2019 - SF2-651 - gcasola - Create immediate Task when Local Business is created - task for opty owner, to remind the product set up on AMA/Customer Center
    public static void executeTasks(Map<Id, Order> ordersMap){

        Map<Id, Order> validOrderMap = new Map<Id, Order>();

        for(Order order : ordersMap.values()){
            if(!ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL.equalsIgnoreCase(order.Type)){
                validOrderMap.put(order.Id, order);                
            }
        }

        if(!validOrderMap.isEmpty()){
            List<Task> tasksToInsert = new List<Task>();

            Map<Id, Order> ordersWithProductSetUPTask = new Map<Id, Order>(
                [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.OwnerId,
                    (SELECT Id, OrderId, ServiceDate, Product2.Name 
                    FROM OrderItems 
                    WHERE Product2.Tasks__c includes(:ConstantsUtil.PRODUCT_SET_UP_REMINDER_TASK)) 
                FROM Order 
                WHERE Id IN (SELECT OrderId FROM OrderItem WHERE Product2.Tasks__c includes(:ConstantsUtil.PRODUCT_SET_UP_REMINDER_TASK) AND OrderId IN :validOrderMap.keySet()) 
                AND Id IN :validOrderMap.keySet()
                ]
            );
            
            if(ordersWithProductSetUPTask != NULL && ordersWithProductSetUPTask.size() > 0){
                for(Id orderId : ordersWithProductSetUPTask.keySet()){
                    if(ordersWithProductSetUPTask.get(orderId).OrderItems != NULL && ordersWithProductSetUPTask.get(orderId).OrderItems.size() > 0){
                        for(OrderItem oi : ordersWithProductSetUPTask.get(orderId).OrderItems){
                            Task t = new Task();
                            t.Subject = Label.productSetUPConfigurationReminder_subject_txt + ' - ' + oi.Product2.Name;
                            t.WhatId = orderId;
                            t.OwnerId = ordersWithProductSetUPTask.get(orderId).SBQQ__Quote__r.SBQQ__Opportunity2__r.OwnerId;
                            t.ActivityDate = oi.ServiceDate;
                            tasksToInsert.add(t);
                        }
                    }
                }
                if(tasksToInsert.size() > 0)
                    insert tasksToInsert;
            }
        }
    }
    
    //Part of Item SF2-482 FM
    /*public static void updateSubIfRejected( Map<Id,Order> orderToWorkOn) {
        
        List<OrderItem> allOrderItems = new List<OrderItem> ();
        Set<String> fieldsToQuery = new Set<String> {'Id','SBQQ__Subscription__c'};
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        
        allOrderItems = SEL_OrderItem.getOrderItemsByOrderIds(orderToWorkOn.KeySet(), fieldsToQuery);
        
        if(!allOrderItems.isEmpty()){
            
            for(OrderItem anOI : allOrderItems){
                
				SBQQ__Subscription__c aSub = new SBQQ__Subscription__c (Id = anOI.SBQQ__Subscription__c, 
                                                                        In_Termination__c = false,
                                                                        SBQQ__TerminatedDate__c = null,
                                                                        Termination_Date__c = null,
                                                                        Termination_Reason__c = ''
                                                                      );                
                subsToUpdate.add(aSub);
            }
            
        }
        
        if(!subsToUpdate.isEmpty()){
            update subsToUpdate;
        }
        
    }
    
 	public static void renewalContractManagement(Map<Id, Order> newItems, Map<Id, Order> oldItems){
        system.debug('here for implicit renewal');
        Set<Id> contractIds = new Set<Id>();
        for(Order o : newItems.values()){
            if(o.Type == ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL && o.Status == ConstantsUtil.ORDER_STATUS_ACTIVATED 
            	&&  o.Status != oldItems.get(o.Id).Status){
                system.debug('here for implicit renewal -> OKAY');
                contractIds.add(o.ContractId);
            }
        }
        List<String> contractFields = new List<String>(  Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().keySet() );
        String queryOnContracts =' SELECT SBQQ__Quote__r.SBQQ__SalesRep__c,' +String.join( contractFields, ',' ) +' FROM Contract WHERE Id IN :contractIds';
        List<Contract> contracts = Database.query(queryOnContracts); 
        List<String> subscriptionFields = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet() );
        String queryOnSubs =' SELECT SBQQ__QuoteLine__r.Base_term_Renewal_term__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, ' +String.join( subscriptionFields, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c IN :contractIds ORDER BY SBQQ__RequiredById__c';
        List<SBQQ__Subscription__c> subscriptions = Database.query(queryOnSubs); 
        Map<Id, List<SBQQ__Subscription__c>> contractAndSubsMap = new Map<Id, List<SBQQ__Subscription__c>>();
        for(SBQQ__Subscription__c sub : subscriptions){
            if(!contractAndSubsMap.containsKey(sub.SBQQ__Contract__c)){
                contractAndSubsMap.put(sub.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{sub});
            }else{
                contractAndSubsMap.get(sub.SBQQ__Contract__c).add(sub);
            }
        }
        List<Contract> contractsToAdd = new List<Contract>();
        for(Contract c : contracts){
            system.debug('here for implicit renewal -> Contracts size > 0');
            Contract clonedContract = new Contract();
            clonedContract = c.clone(false, false, false, false);
            clonedContract.Status = ConstantsUtil.CONTRACT_STATUS_DRAFT;
            clonedContract.Contract__c = c.Id;
            contractsToAdd.add(clonedContract);
        }
        if(contractsToAdd.size() > 0){
            insert contractsToAdd;
        }         
        List<Contract> newContracts = [SELECT Id, Contract__c FROM Contract WHERE Contract__c IN: contractIds];
        List<SBQQ__Subscription__c> subscriptionsToAdd = new List<SBQQ__Subscription__c>();
        for(Contract newContract : newContracts){
            for(SBQQ__Subscription__c s : contractAndSubsMap.get(newContract.Contract__c)){
                SBQQ__Subscription__c clonedSub = new SBQQ__Subscription__c();
                clonedSub = s.clone(false, false, false, false);
                clonedSub.SBQQ__Contract__c = newContract.Id;
                clonedSub.SBQQ__RequiredById__c = null; //needs to be forced: if we don't, the new child subs will have a lookup on the old master one 
                subscriptionsToAdd.add(clonedSub);
            }
        }
        if(subscriptionsToAdd.size() > 0){
            insert subscriptionsToAdd;
        } 
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        for(Id contractId : contractIds){
            for(SBQQ__Subscription__c currentSub : contractAndSubsMap.get(contractId)){
                if(Decimal.valueOf(currentSub.Subscription_Term__c) > currentSub.SBQQ__QuoteLine__r.Base_term_Renewal_term__c){
                    currentSub.Subscription_Term__c = String.valueOf(currentSub.SBQQ__QuoteLine__r.Base_term_Renewal_term__c);
                    currentSub.Total__c = currentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
                    subscriptionsToUpdate.add(currentSub);
                }
            }
        }
		update subscriptionsToUpdate;        
    }*/

    //Vincenzo Laudato SF2-375 - Delete reservation if a order is cancelled
    public static void deleteAllocation(List<Order> orders, Map<Id,Order> newOrders, Map<Id,Order> oldOrders){
        List<Order> validOrder = new List<Order>();
        for(Order currentOrder : orders){
            if(ConstantsUtil.ORDER_STATUS_CANCELLED.equalsIgnoreCase(newOrders.get(currentOrder.Id).Status) &&
               !ConstantsUtil.ORDER_STATUS_CANCELLED.equalsIgnoreCase(oldOrders.get(currentOrder.Id).Status)){
				validOrder.add(currentOrder);                
            }
        }
        if(!validOrder.isEmpty()){
            Set<Id> orderIds = new Set<Id>();
            for(Order currentOrder : validOrder){
                orderIds.add(currentOrder.Id);              
            }
            if(!orderIds.isEmpty()){
                Set<String> allocationIdToDelete = new Set<String>();
                Map<String, Id> allocationIdToOrderId = new Map<String, Id>();
                Map<String, List<OrderItem>> orderToOrderItems = new Map<String, List<OrderItem>>();
                
                List<OrderItem> allOrderItems = SEL_OrderItem.getOrderItemsByOrderIds(orderIds);
                for(OrderItem currentOrderItem : allOrderItems){
                    if(orderIds.contains(currentOrderItem.OrderId)){
                        if(currentOrderItem.Ad_Context_Allocation__c!=null){
                            allocationIdToDelete.add(currentOrderItem.Ad_Context_Allocation__c);
                            allocationIdToOrderId.put(currentOrderItem.Ad_Context_Allocation__c, currentOrderItem.OrderId);
                        }
                    }
                }
                
				if(!allocationIdToDelete.isEmpty()){
                    for(String currentId : allocationIdToDelete){
                        Map<String,String> parameters = new Map<String,String>();
                        parameters.put('ad_context_allocation', currentId);
                        String correlationId = generateCorrelationId(currentId);
                        deleteAllocationCallout(parameters, correlationId, allocationIdToOrderId);
                    }
                }
            }           
        }     
    }
    
    @future(callout=true)
    public static void deleteAllocationCallout(Map<String,String> parameters, String correlationId, Map<String, Id> allocationIdToOrderId){
        
        String endpoint = '';
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.AD_CONTEXT_ALLOCATION);
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        
        Profile p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(p.Id);
        
        Decimal cached_millisecs;
        
        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        Boolean isNewToken = false;
        if(c2.Token__c == null || c2.Token__c == '' ||((cached_millisecs/1000).round()>=(Integer)c2.Expires__c)){
            isNewToken=True;
            token = UtilityToken.refreshToken();
        } 
        else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        
        if(!parameters.isEmpty()){
            endpoint = c2.Endpoint__c + methodName;
            endpoint+='/'+parameters.get('ad_context_allocation');
        }
        
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        //request.setHeader('X-LS-Tracing-BPName', '');
		//request.setHeader('X-LS-Tracing-Initiator', '');
        //request.setHeader('X-LS-Tracing-CallingApp', '');
        //request.setHeader('X-LS-Tracing-BOIdName', '');
        //request.setHeader('X-LS-Tracing-BOIdValue', '');
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('DELETE');
        request.setTimeout(60000);
        
        HttpResponse response;
        try{
            response = http.send(request);
            insert LogUtility.saveCasaReservationLog(methodName+'/'+parameters.get('ad_context_allocation')+' DELETE', response, null, correlationId, allocationIdToOrderId.get(parameters.get('ad_context_allocation')), null);
        }
        catch(Exception e){
            insert LogUtility.saveCasaReservationLog(methodName+'/'+parameters.get('ad_context_allocation')+' DELETE', response, null, correlationId, allocationIdToOrderId.get(parameters.get('ad_context_allocation')), e);
        }
    }
    
    public static String generateCorrelationId(String recordId){
        String hashString = recordId + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        return EncodingUtil.convertToHex(hash);
    }
    
    public static void setStartDate(List<Order> newItems) {
        for(Order o : newItems){
            if (o.ContractSAMBA_Key__c == null || (!ConstantsUtil.ORDER_TYPE_NEW.equalsIgnoreCase(o.Type) && o.ContractSAMBA_Key__c != null) ){
                o.ContractSAMBA_Key__c =null;
                if( (ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(o.Type) || ConstantsUtil.ORDER_TYPE_DOWNGRADE.equalsIgnoreCase(o.Type)) && ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(o.Custom_Type__c)){
                    o.EffectiveDate = Date.today()+1;
                }
                else if(!ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL.equalsIgnoreCase(o.Type)){
                    o.EffectiveDate = Date.today();
                }
            }    
        }
    } 
    
	public static void  setStartDate_Renovero(List<Order> newItems) {
        set<Id> ordersId = new set<Id>();
        for(Order o : newItems){
            ordersId.add(o.OpportunityId); 
        }
        Map<ID, Opportunity> allOpportunity = new Map<ID, Opportunity>([Select Id,CreatedDate, ExternalOPTY__c  From Opportunity where Id In :ordersId]);

        for(Order o : newItems){
            if(allOpportunity.containsKey(o.OpportunityId)){
              date dt = date.valueOf(allOpportunity.get(o.OpportunityId).CreatedDate); 
              o.Renovero_ID__c = allOpportunity.get(o.OpportunityId).ExternalOPTY__c;
              o.EffectiveDate = date.newinstance(dT.year(), dT.month(), dT.day());  
            }            
        }
    }     
    
   
    /*
    public static void copyAttachmentsFromQuote(List<Order> newItems) {
        Map<Id,Set<Id>> quoteToOrderMap = new Map<Id,Set<Id>>();
        for(Order currentOrder : newItems){
            if(String.isNotBlank(currentOrder.SBQQ__Quote__c) && !currentOrder.Migrated__c){
                if(!quoteToOrderMap.containsKey(currentOrder.SBQQ__Quote__c)){
                    quoteToOrderMap.put(currentOrder.SBQQ__Quote__c, new Set<Id>{currentOrder.Id});
                }
                else quoteToOrderMap.get(currentOrder.SBQQ__Quote__c).add(currentOrder.Id);
                //quoteToOrderMap.put(currentOrder.SBQQ__Quote__c, currentOrder.Id);
            }
        }
        if(!quoteToOrderMap.isEmpty()){
            List<ContentDocumentLink> attachments = new List<ContentDocumentLink>();
            List<ContentDocumentLink> attachmentsToInsert = new List<ContentDocumentLink>();
            Set<Id> quotesId = quoteToOrderMap.keySet();
            
            List<String> fieldNames = new List<String>(  Schema.getGlobalDescribe().get('ContentDocumentLink').getDescribe().fields.getMap().keySet() );
            String query =' SELECT ' +String.join( fieldNames, ',' ) +' FROM ContentDocumentLink WHERE LinkedEntityId IN :quotesId';
            attachments = Database.query(query);
            if(!attachments.isEmpty()){
                for(ContentDocumentLink currentAttachment: attachments){
                    for(Id currentOrderId : quoteToOrderMap.get(currentAttachment.LinkedEntityId)){
                        ContentDocumentLink clonedAttachment = currentAttachment.clone();
                        clonedAttachment.LinkedEntityId = currentOrderId;
                        attachmentsToInsert.add(clonedAttachment);
                    }
                }
            }
            if(!attachmentsToInsert.isEmpty()){
                insert attachmentsToInsert;
            }
        }
    }*/

    public static void cancelOrderItems(List<Order> orders, Map<Id,Order> newOrders, Map<Id,Order> oldOrders){
        Set<Id> validOrderIds = new Set<Id>();
        for(Order currentOrder : orders){
            Boolean newStatusCancelled = ConstantsUtil.ORDER_STATUS_CANCELLED.equalsIgnoreCase(newOrders.get(currentOrder.Id).Status);
            Boolean oldStatusNotCancelled = !ConstantsUtil.ORDER_STATUS_CANCELLED.equalsIgnoreCase(oldOrders.get(currentOrder.Id).Status);
            Boolean newStatusRejected = ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(newOrders.get(currentOrder.Id).Status);
            Boolean oldStatusNotRejected = !ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(oldOrders.get(currentOrder.Id).Status);
            if( (newStatusCancelled && oldStatusNotCancelled) || (newStatusRejected && oldStatusNotRejected)){
				validOrderIds.add(currentOrder.Id);                
            }
        }
        if(!validOrderIds.isEmpty()){
            List<OrderItem> orderItemsToCancel = [SELECT Id, Order.Status, SBQQ__Status__c FROM OrderItem WHERE OrderId IN :validOrderIds];
            if(!orderItemsToCancel.isEmpty()){
                for(OrderItem currentOrderItem : orderItemsToCancel){
                    if(ConstantsUtil.ORDER_STATUS_CANCELLED.equalsIgnoreCase(currentOrderItem.Order.Status))
                        currentOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_CANCELLED;
                    else if(ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(currentOrderItem.Order.Status))
                        currentOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_REJECTED;
                }
                update orderItemsToCancel;
            }
        }
    }
}