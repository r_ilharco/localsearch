/**
 * Author      : Gennaro Casola <gcasola@deloitte.it>
 * Date        : 05-21-2019
 * Sprint      : 1
 * Work item   : US_61 - blocking down selling on a quote for the same place
 * Testclass   :
 * Package     : 
 * Description : Selector Class for SBQQ__QuoteLine__c
 * Changelog   : 
 *              #1 - Adjustment for PlaceRequiredId = FALSE / MyWebSite Products - gcasola 07-08-2019
 */

public with sharing class SEL_SBQQQuoteLine {
    
    public static List<SBQQ__QuoteLine__c> getQLinesByPlaceProductAndStatus(Set<Id> placeIds){
        Set<String> opportunityStages = new Set<String>{ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON, ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST};
        return [SELECT  Id,
                		Account__c,
                        Place__c,
                        Product_Group__c,
                        SBQQ__Quote__r.Name
                FROM    SBQQ__QuoteLine__c
                WHERE   Place__c IN :placeIds
                AND     (SBQQ__Quote__r.SBQQ__Status__c = :ConstantsUtil.QUOTE_STATUS_PRESENTED OR (SBQQ__Quote__r.SBQQ__Status__c = :ConstantsUtil.Quote_StageName_Accepted AND SBQQ__Quote__r.SBQQ__Opportunity2__r.StageName NOT IN :opportunityStages))];
    }

    public static List<SBQQ__QuoteLine__c> getQLinesByProductAccountAndStatus(Set<Id> accountIds){
        Set<String> opportunityStages = new Set<String>{ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON, ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST};
        return [SELECT  Id,
                        Account__c,
                        CategoryID__c,
                        LocationID__c,
                        Language__c,
                        SBQQ__Quote__r.Name,
                		Product_Group__c
                FROM    SBQQ__QuoteLine__c
                WHERE   Account__c IN :accountIds   
                AND     SBQQ__Product__r.Product_Group__c IN (:ConstantsUtil.PRODUCT_GROUP_LOCALBANNER, :ConstantsUtil.PRODUCT_GROUP_SEARCHBANNER)
                AND     (SBQQ__Quote__r.SBQQ__Status__c = :ConstantsUtil.QUOTE_STATUS_PRESENTED OR (SBQQ__Quote__r.SBQQ__Status__c = :ConstantsUtil.Quote_StageName_Accepted AND SBQQ__Quote__r.SBQQ__Opportunity2__r.StageName NOT IN :opportunityStages))];
    }
    
    public static List<SBQQ__QuoteLine__c> categoryLocationCheckQuoteLinesRetrieve(Set<Id> quotesId, Set<Id> quoteLinesId){
        return [SELECT Id,
                        Account__c,
                        CategoryID__c,
                        LocationID__c,
                        Language__c,
                        SBQQ__Quote__c,
                        SBQQ__Number__c,
                        SBQQ__StartDate__c,
                		Product_Group__c
                FROM    SBQQ__QuoteLine__c 
                WHERE   SBQQ__Quote__c IN :quotesId 
                AND     Id NOT IN :quoteLinesId 
                AND     SBQQ__Product__r.Product_Group__c IN (:ConstantsUtil.PRODUCT_GROUP_LOCALBANNER, :ConstantsUtil.PRODUCT_GROUP_SEARCHBANNER)];
    }

    /**
     * Gennaro Casola | Sprint 2 | Date 05-23-2019
     * Description
     * Get Quote Lines by Id specified in Set<Id> ids
     *
    */
    
    public static List<SBQQ__QuoteLine__c> retrieveQLinesByQuoteIds(Set<Id> quoteIds){ 
        return [SELECT  Id, Name,SBQQ__RequiredBy__r.Termination_Date__c, SBQQ__Product__r.Product_Group__c, Termination_Date__c,SBQQ__Quote__r.SBQQ__Opportunity2__c, SBQQ__Quote__r.SBQQ__Status__c,SBQQ__Product__r.SBQQ__SubscriptionType__c, SBQQ__PricebookEntryId__c,SBQQ__RequiredBy__r.SBQQ__Product__c,
                        SBQQ__Product__c, SBQQ__Product__r.One_time_Fee__c, SBQQ__Product__r.SBQQ__Component__c,SBQQ__SubscriptionType__c,SBQQ__Quote__r.SBQQ__Account__c,SBQQ__ProductOption__c,
                        SBQQ__Bundled__c,SBQQ__Quantity__c, SBQQ__EffectiveQuantity__c, SBQQ__ListPrice__c,SBQQ__ProductSubscriptionType__c,SBQQ__StartDate__c,SBQQ__BillingFrequency__c,
                        SBQQ__RequiredBy__r.Subscription_Term__c, SBQQ__RequiredBy__r.Base_term_Renewal_term__c,SBQQ__SubscriptionPricing__c,Integration_List_Price__c,Samba_Migration__c,SBQQ__OptionLevel__c,
                        SBQQ__Product__r.SBQQ__SubscriptionPricing__c,SBQQ__TaxCode__c, SBQQ__Product__r.SBQQ__TaxCode__c,SBQQ__Product__r.PlaceIDRequired__c,Migration_Net_Price__c,SBQQ__DynamicOptionId__c,
                        SBQQ__DefaultSubscriptionTerm__c, SBQQ__Product__r.SBQQ__SubscriptionTerm__c,RequiredByProductCode__c,SBQQ__SubscriptionBase__c,SBQQ__Product__r.SBQQ__SubscriptionBase__c,
                        SBQQ__Discount__c,SBQQ__AdditionalDiscountAmount__c,SBQQ__Quote__r.SBQQ__CustomerDiscount__c,Total__c,SBQQ__QuoteLine__c.Place__c,End_Date__c,Category__c,CategoryID__c,
                        SBQQ__RequiredBy__c, Subscription_Term__c, Base_term_Renewal_term__c,Package_Total__c,SBQQ__Quote__c,SBQQ__RequiredBy__r.SBQQ__SubscriptionTerm__c,Location__c,LocationID__c,
                        SambaIsStartConfirmed__c, sambaIsBilled__c, SBQQ__SubscriptionTerm__c, SBQQ__Quote__r.SBQQ__PrimaryContact__c,SBQQ__ProductCode__c, SBQQ__NetPrice__c, contract_ref_id__c,
                        SBQQ__EffectiveStartDate__c, Quote_Type__c, SBQQ__Product__r.Name,SBQQ__Number__c,SBQQ__RequiredBy__r.SBQQ__StartDate__c,SBQQ__RequiredBy__r.End_Date__c,SBQQ__RequiredBy__r.SBQQ__EndDate__c,
                        Status__c, Url__c, Url2__c, Url3__c, Url4__c, Ad_Context_Allocation__c, Campaign_Id__c, SBQQ__Product__r.ASAP_Allowed__c, SBQQ__Product__r.Min_Start_Date__c, SBQQ__Product__r.Max_Start_Date__c, Edition__c
                FROM    SBQQ__QuoteLine__c 
                WHERE   SBQQ__Quote__c in: quoteIds ORDER BY SBQQ__RequiredBy__c NULLS First];
    }  
    
    public static Map<Id, SBQQ__QuoteLine__c> getQuoteLinesById(Set<Id> ids)
    {
        return new Map<Id, SBQQ__QuoteLine__c>(
                [
                    SELECT Id, Place__c, SBQQ__Quote__r.SBQQ__Type__c, SBQQ__Product__c,
                    SBQQ__ProductName__c,
                    SBQQ__Product__r.Priority__c, Subscription_Term__c, SBQQ__RequiredBy__c,
                    SBQQ__RequiredBy__r.Subscription_To_Terminate__c,
                    SBQQ__RequiredBy__r.Subscription_To_Terminate__r.SBQQ__ProductName__c,
                    SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
                    SBQQ__ProductSubscriptionType__c,
                    Master__c,
                    MasterCode__c,
                    End_Date__c,
                    Downselling_Disable__c,
                    Upselling_Disable__c,
                    SBQQ__StartDate__c,
                    SBQQ__UpgradedSubscription__r.End_Date__c,
                    SBQQ__BillingFrequency__c
                    FROM SBQQ__QuoteLine__c 
                    WHERE Id IN :ids
                ]
        );
    }

    /**
     * Gennaro Casola | Sprint 2 | Date 05-23-2019
     * Description
     *
     */
    public static List<SBQQ__QuoteLine__c> getQLinesByPlaceAndQuoteStatusNotIn(Set<Id> quoteLineIds, Set<Id> placeIds, List<String> quoteStatus)
    {             
        
        return [SELECT Id, Name, Place__c, SBQQ__Quote__r.SBQQ__Ordered__c, SBQQ__Quote__r.SBQQ__Status__c,
                                                SBQQ__Quote__c, SBQQ__Product__c, SBQQ__Product__r.Priority__c,
                                                SBQQ__Product__r.Product_Group__c
                                                FROM SBQQ__QuoteLine__c 
                                                WHERE SBQQ__Quote__r.SBQQ__Status__c IN :quoteStatus 
                                                AND Place__c  IN : placeIds 
                                                AND Id NOT IN :quoteLineIds];
    }
    
    //Adjustment for PlaceRequiredId = FALSE / MyWebSite Products - gcasola 07-08-2019
    public static Map<Id, SBQQ__QuoteLine__c> getQLinesWithoutPlaceAndProductOptionByQuoteId(Set<Id> quoteIds)
    {
        return new Map<Id, SBQQ__QuoteLine__c>([
            SELECT Id, SBQQ__Quote__c
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__ProductOption__c = NULL 
            AND Place__c = NULL
            AND SBQQ__Product__r.PlaceIDRequired__c = 'Yes'
            AND SBQQ__Quote__c IN :quoteIds
        ]);
    }
    
    public static Map<Id, SBQQ__QuoteLine__c> getQLinesByQuoteId(Set<Id> quoteIds)
    {
        return new Map<Id, SBQQ__QuoteLine__c>([
            SELECT Id, SBQQ__Quote__c, SBQQ__Product__c,SBQQ__ProductCode__c, Subscription_To_Terminate__c, 
            SBQQ__RequiredBy__r.Subscription_To_Terminate__r.Product_Code__c,
            SBQQ__RequiredBy__r.SBQQ__ProductCode__c,SBQQ__ProductName__c,
            Place__c, SBQQ__Product__r.PlaceIDRequired__c, SBQQ__Product__r.ProductCode, SBQQ__Product__r.ExternalKey_ProductCode__c,
            SBQQ__RequiredBy__c, SBQQ__ProductOption__c, SBQQ__RequiredBy__r.Place__c,SBQQ__RequiredBy__r.Subscription_To_Terminate__c,
            SBQQ__Product__r.Name, SBQQ__Product__r.Product_Group__c, SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c,
            Subscription_Term__c, SBQQ__Product__r.Configuration_Model__c, SBQQ__ListPrice__c, SBQQ__BillingFrequency__c, Total__c,
            SBQQ__AdditionalDiscountAmount__c, SBQQ__Discount__c, Base_term_Renewal_term__c, Product_Group__c, SBQQ__Quantity__c,
            SBQQ__EffectiveQuantity__c, Place__r.PlaceID__c, Edition__r.Local_Guide_Number__c, SBQQ__Product__r.DocumentsFolder__c
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__Quote__c IN :quoteIds
            ]);
    }
    
    public static Map<Id, SBQQ__QuoteLine__c> getQLinesWithPlaceSubscriptionByQuoteId(Set<Id> quoteIds)
    {
        return new Map<Id, SBQQ__QuoteLine__c>([
            SELECT Id, SBQQ__Quote__c, SBQQ__Product__c
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__Quote__c IN :quoteIds
            AND (
                (Place__c != '' AND SBQQ__Product__r.PlaceIDRequired__c = 'Yes') 
                OR 
                (Place__c = '' AND SBQQ__Product__r.PlaceIDRequired__c = 'No')
                OR SBQQ__Product__r.PlaceIDRequired__c = 'Optional'
            )                                    
            AND SBQQ__ProductOption__c = NULL
            AND Subscription_To_Terminate__c = NULL
            ]);
    }   
    
        /**
     * Gennaro Casola | Sprint 2 | Date 05-23-2019
     * Description
     *
     */
    
    public static List<SBQQ__QuoteLine__c> getQLinesByPlaceAndQuoteStatus( Set<Id> placeIds, List<String> quoteStatus)
    {             
        
        return [SELECT Id, Name, Place__c, SBQQ__Quote__r.SBQQ__Ordered__c, 
                                                SBQQ__Quote__c, SBQQ__Product__c
                                                FROM SBQQ__QuoteLine__c 
                                                WHERE SBQQ__Quote__r.SBQQ__Status__c IN :quoteStatus 
                                                AND Place__c  IN : placeIds ];

    }
    
    //START - FIXED ISSUE ON THE GENERATION OF CHILDREN SUBSCRIPTIONS ON FIXED TERM CONTRACTS - 07-04-2019 - gcasola
    public static Map<Id, SBQQ__QuoteLine__c> getChildrenQLByParentIdAndNotIn(Set<Id> ids, Set<Id> excludedIds)
    {
        return new Map<Id, SBQQ__QuoteLine__c>(
                [
                    SELECT Id, Subscription_Term__c, SBQQ__RequiredBy__c, SBQQ__ProductSubscriptionType__c
                    FROM SBQQ__QuoteLine__c 
                    WHERE SBQQ__RequiredBy__c IN :ids
                    AND Id NOT IN :excludedIds
                ]
        );
    }
    //END - FIXED ISSUE ON THE GENERATION OF CHILDREN SUBSCRIPTIONS ON FIXED TERM CONTRACTS - 07-04-2019 - gcasola
   public static Map<Id, SBQQ__QuoteLine__c> getSortedAllQLinesByQuoteId(Set<Id> quoteIds){
        return new Map<Id, SBQQ__QuoteLine__c>([SELECT  Id,
                        Subscription_To_Terminate__c,
                        SBQQ__RequiredBy__c,
                        SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c,
                        Place__c,
                		Sort_Order__c
                FROM    SBQQ__QuoteLine__c
                WHERE   SBQQ__Quote__c in :quoteIds
                ORDER BY DocRowNumber__c NULLS LAST, SBQQ__RequiredBy__c, SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c nulls last,SBQQ__ProductOption__r.SBQQ__Number__c
               ]);
    }
    
    public static List<SBQQ__QuoteLine__c> getParentQLinesByQuoteId(Id quoteId){
        return [SELECT  Id,
                        Subscription_To_Terminate__c,
                		SBQQ__StartDate__c,
                		Quote_Type__c
                FROM    SBQQ__QuoteLine__c
                WHERE   SBQQ__Quote__c = :quoteId
                AND     SBQQ__RequiredBy__c = null];
    }
    
    public static List<SBQQ__QuoteLine__c> getParentQLinesByQuoteIds(Set<Id> quoteIds){
        return [SELECT  Id,
                        Subscription_To_Terminate__c,
                        SBQQ__Quote__c
                FROM    SBQQ__QuoteLine__c
                WHERE   SBQQ__Quote__c IN :quoteIds
                AND     SBQQ__RequiredBy__c = null];
    }

    
     /**
     * Mara Mazzarella | Sprint 6 | Date 07-29-2019
     *
     */
     public static List<SBQQ__QuoteLine__c> getActiveQuoteLineByAccountId(Id accountId){
        return [SELECT Id, Location__c, Category__c
                FROM SBQQ__QuoteLine__c 
                WHERE SBQQ__Quote__r.SBQQ__Account__c =: accountId
                AND SBQQ__Quote__r.SBQQ__Status__c ='Accepted'];
    }

    public static Map<Id, SBQQ__QuoteLine__c> getParentQLinesByQuoteIdAndIdNotIn(Set<Id> quoteIds, Set<Id> quoteLineIds){
        return new Map<Id, SBQQ__QuoteLine__c>([
            SELECT Id, SBQQ__Quote__c, SBQQ__Product__c, Place__c, SBQQ__Product__r.PlaceIDRequired__c, SBQQ__Product__r.ProductCode,
            SBQQ__RequiredBy__c, SBQQ__ProductOption__c, SBQQ__RequiredBy__r.Place__c, SBQQ__Product__r.Name, SBQQ__Product__r.Product_Group__c,
            Subscription_Term__c, SBQQ__Product__r.Configuration_Model__c, SBQQ__ListPrice__c, SBQQ__BillingFrequency__c, Total__c,
            SBQQ__AdditionalDiscountAmount__c, SBQQ__Discount__c, Base_term_Renewal_term__c, Product_Group__c
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__Quote__c IN :quoteIds
            AND Id NOT IN :quoteLineIds
            AND SBQQ__RequiredBy__c = NULL
            ]);
    }
    
    public static Map<Id, SBQQ__QuoteLine__c> getParentQLinesByQuoteId(Set<Id> quoteIds){
        return new Map<Id, SBQQ__QuoteLine__c>([
            SELECT Id, Name, SBQQ__Quote__c, SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__Product__c, Place__c, Place__r.Name,Place__r.PlaceID__c, SBQQ__Product__r.PlaceIDRequired__c, SBQQ__Product__r.ProductCode,
            SBQQ__RequiredBy__c, SBQQ__ProductOption__c, SBQQ__RequiredBy__r.Place__c, SBQQ__Product__r.Name, SBQQ__Product__r.Product_Group__c,
            Subscription_Term__c, SBQQ__Product__r.Configuration_Model__c, SBQQ__ListPrice__c, SBQQ__BillingFrequency__c, Total__c,
            SBQQ__AdditionalDiscountAmount__c, SBQQ__Discount__c, Base_term_Renewal_term__c, Product_Group__c
            FROM SBQQ__QuoteLine__c
            WHERE SBQQ__Quote__c IN :quoteIds
            AND SBQQ__RequiredBy__c = NULL
            ]);
    }
    
    public static List<SBQQ__QuoteLine__c> getDuplicateLocationbyStatusAndAccount(Set<Id> accIds, Set<Id> newIds, List<String> status){
        System.debug('newIds '+newIds);
        System.debug('accIds '+accIds);
        return [SELECT Location__c,
                SBQQ__Quote__r.SBQQ__Account__c
                FROM  SBQQ__QuoteLine__c 
                WHERE Location__c != null
                AND   SBQQ__Quote__r.SBQQ__Account__c =: accIds
                AND  Id NOT in : newIds
                AND   SBQQ__Quote__r.SBQQ__Status__c in: status
                ORDER BY SBQQ__Quote__r.SBQQ__Account__c ];
    }
    
    public static List<SBQQ__QuoteLine__c> getDuplicateCategorybyStatusAndAccount(Set<Id> accIds, Set<Id> newIds, List<String> status){
        System.debug('newIds '+newIds);
        System.debug('accIds '+accIds);
        return [SELECT Category__c,
                SBQQ__Quote__r.SBQQ__Account__c
                FROM  SBQQ__QuoteLine__c 
                WHERE Category__c != null 
                AND   SBQQ__Quote__r.SBQQ__Account__c =: accIds
                AND  Id NOT in : newIds
                AND   SBQQ__Quote__r.SBQQ__Status__c in: status
                ORDER BY SBQQ__Quote__r.SBQQ__Account__c ];
    } 
    
    public static List<SBQQ__QuoteLine__c> getQuoteLinebyProductCodeAndAccount(Set<Id> accIds, String productCode, List<String> status){
        System.debug('accIds '+accIds);
        return [SELECT SBQQ__ProductCode__c,
                SBQQ__Quote__r.SBQQ__Account__c
                FROM  SBQQ__QuoteLine__c 
                WHERE SBQQ__ProductCode__c =: productCode
                AND   SBQQ__Quote__r.SBQQ__Account__c =: accIds
                AND   SBQQ__Quote__r.SBQQ__Status__c in: status
                ORDER BY SBQQ__Quote__r.SBQQ__Account__c ];
    } 
    
    //Vincenzo Laudato SF2-375
    public static List<SBQQ__QuoteLine__c> getAdContextAllocatedQL(Set<Id> quoteIds){
        return [SELECT  Id,
                        Ad_Context_Allocation__c,
                        Quote__c
                FROM    SBQQ__QuoteLine__c 
                WHERE   Ad_Context_Allocation__c != null
                AND     Quote__c IN :quoteIds
                ];
    } 

    public static List<SBQQ__QuoteLine__c> getQLinesByIds(List<String> qlIds){
        return [SELECT  Id, 
                        Name, 
                        SBQQ__Quote__c, 
                        SBQQ__Quote__r.SBQQ__Account__c, 
                        SBQQ__Quote__r.SBQQ__Primary__c,
                        SBQQ__Quote__r.SBQQ__Opportunity2__c,
                        SBQQ__Quote__r.External_Id__c, 
                        SBQQ__ProductCode__c, 
                        SBQQ__ProductName__c, 
                        SBQQ__Quantity__c,
                        Place__c, 
                        Place__r.PlaceID__c, 
                        Status__c, 
                        SBQQ__RequiredBy__c, 
                        SBQQ__StartDate__c,
                        TrialStartDate__c, 
                        TrialEndDate__c, 
                        TrialStatus__c,
                        SBQQ__Quote__r.Trial_CallId__c
                FROM    SBQQ__QuoteLine__c 
                WHERE   Id IN :qlIds];
    }

    public static List<SBQQ__QuoteLine__c> getQuoteLinesByQuoteId(Set<Id> quoteIds, Set<String> queryFields){
        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
        List<String> queryFieldsList = new List<String>(queryFields);
        System.debug('queryFieldsList' +queryFieldsList);
        String query =' SELECT ' +String.join(queryFieldsList, ',') +' FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c IN :quoteIds';
        quoteLines = Database.query(query);
        return quoteLines;
    }
    
    public static List<SBQQ__QuoteLine__c> getQuoteLinesById(Set<String> qLIds, Set<String> queryFields){
        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
        List<String> queryFieldsList = new List<String>(queryFields);
        System.debug('queryFieldsList' +queryFieldsList);
        String query =' SELECT ' +String.join(queryFieldsList, ',') +' FROM SBQQ__QuoteLine__c WHERE Id IN :qLIds';
        quoteLines = Database.query(query);
        return quoteLines;
    }
    
    public static Map<Id, SBQQ__QuoteLine__c> getParentQuoteLinesByOpprtunityIds (Set<Id> opportunityIds){
        
        return new Map<Id, SBQQ__QuoteLine__c>([SELECT  Id, 
                                                        SBQQ__Product__c, 
                                                        Subscription_Term__c,
                                                		End_Date__c,
                                                        SBQQ__Product__r.ASAP_Allowed__c,
                                                        SBQQ__Product__r.Min_Start_Date__c,
                                                        SBQQ__Product__r.Max_Start_Date__c,
                                                        SBQQ__Product__r.Name,
                                                		SBQQ__Product__r.Product_Group__c,
                                                        SBQQ__Quote__r.SBQQ__Opportunity2__c,
                                                		SBQQ__Quote__r.SBQQ__Type__c,
                                                        SBQQ__StartDate__c,
                                                		Place__c
                                                FROM    SBQQ__QuoteLine__c
                                                WHERE   SBQQ__Quote__c IN (SELECT   Id 
                                                                           FROM     SBQQ__Quote__c 
                                                                           WHERE    SBQQ__Status__c = :ConstantsUtil.Quote_Status_Accepted 
                                                                           AND      SBQQ__Opportunity2__c IN :opportunityIds)
                                                AND     SBQQ__RequiredBy__c = NULL]);
    }
    
}