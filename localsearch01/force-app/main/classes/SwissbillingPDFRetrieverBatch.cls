public class SwissbillingPDFRetrieverBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    public List<sObject> start(Database.BatchableContext context) {
        List<Invoice__c> toFetch = new List<Invoice__c>();
        // TODO: Add a time window limit of 24hs to reduce persistent error requests.
        
        //Vincenzo Laudato 2019-06-10 *** START
        List<Invoice__c> invoices = [SELECT Id, Name, Correlation_Id__c, Invoice_Code__c from Invoice__c WHERE Status__c =: ConstantsUtil.INVOICE_STATUS_COLLECTED LIMIT 5000];
        
        if(!invoices.isEmpty()){
            Map<Id, Invoice__c> invoicesMap = new Map<Id, Invoice__c>(invoices);
            Set<Id> keySet = invoicesMap.KeySet();
            
            if(keySet.size() > 0) {
                List<ContentDocumentLink> docs = [select LinkedEntityId, ContentDocumentId, ContentDocument.Title from ContentDocumentLink where LinkedEntityId IN :keySet];
                for(ContentDocumentLink doc : docs) {
                    if(string.format(ConstantsUtil.INVOICE_PDF_NAME, new List<String> {invoicesMap.get(doc.LinkedEntityId).Name} ) == doc.ContentDocument.Title) { 
                        invoicesMap.remove(doc.LinkedEntityId);  
                        continue;  
                    }
                }
                toFetch.AddAll(invoicesMap.values());
            }
        }
        //END
        return toFetch;
    }
    
    public void execute(Database.BatchableContext context, List<sObject> data) {
        List<Invoice__c> invoices = (List<Invoice__c>)data;
        //for(Invoice__c invoice : invoices) {
        BillingResults.attachPdfDocument(invoices);
        //}
    }
    
    public void finish(Database.BatchableContext context) {}
}