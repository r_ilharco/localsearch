public class Result{
    @AuraEnabled public String name {get; set;}
    @AuraEnabled public String fields {get; set;}
    @AuraEnabled public Map<String,List<SelectOptionLightning>> mapValuesOption {get; set;}
    @AuraEnabled public Map<String,List<SelectOptionLightning>> mapValuesString {get; set;}
    @AuraEnabled public String buttons {get; set;}
    @AuraEnabled public List<Option> options {get; set;}
    @AuraEnabled public List<SelectOptionLightning> subterms {get; set;}
    @AuraEnabled public List<SelectOptionLightning> billingFrequency {get; set;}
    @AuraEnabled public String mandatoryFields {get;set;}
    
    public Result(){
        mapValuesOption = new Map<String,List<SelectOptionLightning>>();
        mapValuesString = new Map<String,List<SelectOptionLightning>>();
        options = new List<Option>();
        subterms = new List<SelectOptionLightning>();
        billingFrequency = new List<SelectOptionLightning>();
    }
}