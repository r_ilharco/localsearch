/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> 
 * Date		   : 
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public with sharing class SwissbillingWrappers {
    SalesInvoiceSummary invoiceSummary {get;set;}

    public SwissbillingWrappers(SalesInvoiceSummary invoiceSummary) {
        //this.invoiceSummary = new SalesInvoiceSummary();
        this.invoiceSummary = invoiceSummary;
    }

    public class SalesInvoiceSummary{
        public String sales_invoice_id {get;set;}
        public String client_id {get;set;}
        public String client_name {get;set;}
        public Decimal total_amount_incl_tax {get;set;}
        public String payment_status {get;set;}
        public Decimal open_amount {get;set;}

        public SalesInvoiceSummary(String salesInvoiceId, String clientId, String clientName, Decimal totalAmountInclTax, 
                                    String paymentStatus, Decimal openAmount){
            this.sales_invoice_id = salesInvoiceId;
            this.client_id = clientId;
            this.client_name = clientName;
            this.total_amount_incl_tax = totalAmountInclTax;
            this.payment_status = paymentStatus;
            this.open_amount = openAmount;
        }
    }
    public class StatusInvoiceSummary{
        public List<FeedbackInvoicesObj> FeedbackInvoices {get;set;}
    }

    public class FeedbackInvoicesObj{
        public String Status {get;set;} 
        public String RequestTimestamp {get;set;} 
        public String ResponseTimestamp {get;set;} 
        public String CorrelationId {get;set;} 
        public String DocNumber {get;set;} 
        public DetailsObj Details {get;set;} 
    }
    public class DetailsObj {
        public String ErrorDescription {get;set;} 
        public String ErrorCode {get;set;} 
    }

    public static StatusInvoiceSummary deserializeJson(String jsonBody) {
        system.debug('json: ' + jsonBody);
        return (StatusInvoiceSummary)JSON.deserialize(jsonBody, StatusInvoiceSummary.class);
    }

    public class invoicesToSend{
        public List<Map<String, Object>> invoices;
        public invoicesToSend(){
            invoices = new List<Map<String, Object>>();
        }
    }
}