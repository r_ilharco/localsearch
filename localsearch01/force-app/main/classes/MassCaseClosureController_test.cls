@isTest
public class MassCaseClosureController_test {
		
    
    @testSetup
    static void setup() {
        Test_DataFactory.insertFutureUsers();
		AccountTriggerHandler.disableTrigger = true;
        List<Account> accs = Test_DataFactory.generateAccounts('Test', 1, false);
        insert accs;
        AccountTriggerHandler.disableTrigger = false;
        
        Contact primaryCont = new Contact(LastName='primaryContact');
        primaryCont.MailingCity = 'Zürich';
        primaryCont.MailingCountry = 'Switzerland';
        primaryCont.MailingPostalCode = '8005';
        primaryCont.MailingStreet = 'Förrlibuckstrasse, 62';
        primaryCont.AccountId = accs[0].Id;
        primaryCont.Primary__c = true;
        insert primaryCont;
       
         Case case1 = new Case();
            case1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Customer Data').getRecordTypeId();
            case1.Status = 'New';
        	case1.AccountId = accs[0].Id;
            case1.Origin = 'Phone';
            case1.Topic__c = 'Change Customer Data';
            case1.ContactId = primaryCont.Id;
            case1.Language__c = 'German';
       insert case1;
    }
    
    @isTest 
    static void MassCaseClosureController_PartialSuccess(){
        List<Case> casesList =  [SELECT Id FROM Case];
        List<String> casesIds = new List<String>();
        for(Case cs : casesList){
            casesIds.add(cs.Id);
        }
        
        Test.StartTest(); 

            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(casesList);
            MassCaseClosureController  obj = new MassCaseClosureController(sc);	
       		MassCaseClosureController.getPicklistOptions('Case', 'Closure_Reason__c');
        	/*MassCaseClosureController.result res = new MassCaseClosureController.result();
        	res.isSuccess = MassCaseClosureController.closeCaseshandler(casesIds, 'Closed', 'Solved').isSuccess;*/
			system.assertEquals(false,MassCaseClosureController.closeCaseshandler(casesIds, 'Closed', 'Solved').isSuccess);
        
		Test.StopTest();
    }
    
    @isTest 
    static void MassCaseClosureController_Success(){
        List<Account> accs = [Select Id from Account];

        Case case1 = new Case();
            case1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Product Fulfillment').getRecordTypeId();
            case1.Status = 'New';
        	case1.AccountId = accs[0].Id;
            case1.Origin = 'Phone';
            case1.Topic__c = 'Product Activation';
            case1.Language__c = 'German';
       insert case1;
        
        //List<Case> casesList = new List<Case>{case1};
            List<String> casesIds = new List<String>{case1.id};
        
        Test.StartTest(); 
			system.assertEquals(true,MassCaseClosureController.closeCaseshandler(casesIds, 'Closed', 'Solved').isSuccess);
        
		Test.StopTest();
    }
    
        @isTest 
    static void MassCaseClosureController_SuccessAndPartialSuccess(){
        List<Account> accs = [Select Id from Account];

        Case case1 = new Case();
            case1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Product Fulfillment').getRecordTypeId();
            case1.Status = 'New';
        	case1.AccountId = accs[0].Id;
            case1.Origin = 'Phone';
            case1.Topic__c = 'Product Activation';
            case1.Language__c = 'German';
       insert case1;
        
  
        List<String> casesIds = new List<String>();
        for(Case cs : [SELECT Id FROM Case]){
            casesIds.add(cs.Id);
        }
        
        Test.StartTest(); 
			system.assertEquals(true,MassCaseClosureController.closeCaseshandler(casesIds, 'Closed', 'Solved').isSuccess);
        
		Test.StopTest();
    }
    
    @isTest
    static void MassCaseClosureController_UserAccess(){
        Mass_Case_Closure__c mcs = new Mass_Case_Closure__c();
        mcs.Digital_Agency__c = true;
        mcs.System_Administrator__c =true;
        mcs.Telesales_Agent__c = true; 
        mcs.Sales__c = true;
        mcs.Product_Manager__c = true;

        insert mcs;
        User salesDMc = [Select Id,Profile.Name from User where Code__c = 'DMC' and IsActive = true Limit 1];
        User telesales = [Select Id, Profile.Name from User where Profile.Name = 'Telesales Agent' and IsActive = true Limit 1];
        system.debug('telesales ::'+telesales.Profile.Name);
        User digitalAgency = [Select Id,Profile.Name from User where Code__c = 'FCT' and IsActive = true Limit 1];
        Test.StartTest(); 
        system.runAs(salesDMc){
            
          system.assertEquals(MassCaseClosureController.isAllowed(),'');
        }
        system.runAs(telesales){
           system.assertEquals(MassCaseClosureController.isAllowed(),'');
        }
        system.runAs(digitalAgency){
            system.assertEquals(MassCaseClosureController.isAllowed(),'');
        }
       		system.assertEquals(MassCaseClosureController.isAllowed(),'');
			
        
		Test.StopTest();
    }
}