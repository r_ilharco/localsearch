@RestResource(urlMapping='/cases/contracts/terminations/*')
global with sharing class CaseHandlerService{

    @HttpPost
    global static void createCase(String description) {

        RestResponse res = RestContext.response;
        
        String correlationId;

        //get queue to assign
        Group queue = [SELECT id,DeveloperName FROM Group WHERE DeveloperName =: Label.queue1 limit 1];
        
        //create the new case.
        Case c;
        try {

            c = new Case(
                Subject = Label.Contract_termination_subject,
                Status = Label.Case_Status1, //Status New
                Origin = Label.Case_origin1, //Origin Email
                Priority = Label.Case_priority1, //Case priority
                Description = description, 
                Type = Label.Case_type1, // Contract termination
                Sub_Type__c	= Label.Case_subtype1, // Upselling on SAMBA
                OwnerId = queue.id,
				RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CSC Case').getRecordTypeId()
                );
            
            insert c;

            c = [select id, CaseNumber FROM case where id =: c.id];

        } catch(Exception e) {
            // log the error and response
            ErrorHandler.logError('Case', 'Contract Termination', e, ErrorHandler.ErrorCode.E_UNKNOWN, e.getMessage(), 'Contract termination SAMBA', correlationId);
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
                
        }

        //return the case number
        res.responseBody = Blob.valueOf(JSON.serialize(c.CaseNumber));
        res.statusCode = 200;
    }
    
     
}