@isTest
private class Test_ContractInBoundApi{
static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    @testSetup
    private static void setup(){
        insertBypassFlowNames();
        test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
        system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
        insert psas;
        }
        TestSetupUtilClass.createContracts(1);
        Contract c = [SELECT ID FROM Contract LIMIT 1];
        Place__c place = test_dataFactory.createPlaces('TestPlace',1)[0];
        insert place;
        PermissionSetAssignment PSA = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        string UserId = PSA.AssigneeId;
        user u = [select id from user where id=: UserId][0];
        system.runas(u){
        SBQQ__Subscription__c sub = test_datafactory.createSubscriptionsWithPlace('draft', place.id, c.id, 1)[0];
        insert sub;
        }
        
        
    }
    
	@isTest
    private static void testGetApi_200(){  
        try{
            Contract c = [SELECT ContractNumber FROM Contract LIMIT 1];
        	RestRequest req = new RestRequest();
            req.params.put('ContractNumber', c.ContractNumber); 
		    RestResponse res = new RestResponse();
        
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
                ContractInBoundApi.getContractAndSubs();
            Test.stopTest();
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }

@isTest
    private static void testCampaign(){  
        try{
            campaignstatusinbound.InboundDataWrapper inb = new campaignstatusinbound.InboundDataWrapper();
inb.campaigncode = 'TestCa';
System.assertEquals(inb.campaigncode,'TestCa');
inb.campaignRemoteId = 'testcrid';
system.assertequals(inb.campaignremoteId,'testcrid');
inb.customerId = 'Test_id';
system.assertEquals(inb.customerID, 'Test_id');
inb.customerremoteId = 'test_remote_id';
system.assertEquals(inb.customerremoteId,'test_remote_id');
inb.statusCode = 'test_status_code';
system.assertEquals(inb.statuscode,'test_status_code');
inb.statusmessage = 'test_status_message';
system.assertEquals(inb.statusmessage, 'test_status_message');
campaignStatusInbound.detail det = new campaignstatusInbound.detail();
det.key = 'key';
system.assertEquals(det.key,'key');
det.value = 'value';
system.assertEquals(det.value,'value');
            Contract c = [SELECT ContractNumber FROM Contract LIMIT 1];
        	RestRequest req = new RestRequest();
            req.params.put(inb.campaigncode,'TestCa'); 
		    RestResponse res = new RestResponse();
        
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
                campaignstatusinbound.activateorder();
            Test.stopTest();
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }

@istest
    private static void test_getset(){
campaignstatusinbound.InboundDataWrapper inb = new campaignstatusinbound.InboundDataWrapper();
inb.campaigncode = 'TestCa';
System.assertEquals(inb.campaigncode,'TestCa');
inb.campaignRemoteId = 'testcrid';
system.assertequals(inb.campaignremoteId,'testcrid');
inb.customerId = 'Test_id';
system.assertEquals(inb.customerID, 'Test_id');
inb.customerremoteId = 'test_remote_id';
system.assertEquals(inb.customerremoteId,'test_remote_id');
inb.statusCode = 'test_status_code';
system.assertEquals(inb.statuscode,'test_status_code');
inb.statusmessage = 'test_status_message';
system.assertEquals(inb.statusmessage, 'test_status_message');
campaignStatusInbound.detail det = new campaignstatusInbound.detail();
det.key = 'key';
system.assertEquals(det.key,'key');
det.value = 'value';
system.assertEquals(det.value,'value');
}
    
@isTest
    private static void testGetApi_500(){  
        try{
            Contract c = [SELECT ContractNumber FROM Contract LIMIT 1];
        	RestRequest req = new RestRequest();
            req.params.put('ContractNumber', ''); 
		    RestResponse res = new RestResponse();
        
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
                ContractInBoundApi.getContractAndSubs();
            Test.stopTest();
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }
}