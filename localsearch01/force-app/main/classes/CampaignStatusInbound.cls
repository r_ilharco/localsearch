@RestResource(urlMapping='/campaign-status')
global without sharing class CampaignStatusInbound {
	
    @HttpPost
    global static void activateOrder(){
        RestResponse res = RestContext.response;
        RestRequest request = RestContext.request;
        
        try{
            String query;
            InboundDataWrapper inputBody = (InboundDataWrapper)JSON.deserialize(request.requestBody.toString(), InboundDataWrapper.class);
            if(inputBody != null){
                if(String.isNotBlank(inputBody.campaignCode)){
                    String campaignCode = inputBody.campaignCode;  
                    if(String.isNotBlank(inputBody.statusCode)){
                        if(inputBody.statusCode.equalsIgnoreCase('ONL')){
                            query = 'SELECT Id, OrderId FROM OrderItem WHERE Campaign_Id__c = :campaignCode';
                        }
                        else if(inputBody.statusCode.equalsIgnoreCase('CAN')){
                            query = 'SELECT Id FROM SBQQ__Subscription__c WHERE Campaign_Id__c = :campaignCode';
                        }
                        else throw new BadRequestException('Invalid value for statusCode: '+ inputBody.statusCode);
                    }
                    else throw new BadRequestException('Invalid value for statusCode: BLANK');
                }
                else throw new BadRequestException('Invalid value for campaignCode: BLANK');
                
                if(String.isNotBlank(query)){
                    
                    List<sObject> retrievedObj = Database.query(query);
                    if(!retrievedObj.isEmpty()){
                        Id recordId = retrievedObj[0].Id;
                        Schema.SObjectType objectType = recordId.getSobjectType();
                        
                        if(Schema.OrderItem.SObjectType == objectType){
                            OrderItem myCampFather = (OrderItem)retrievedObj[0];
                            SRV_CampaignStatusInbound.activateOrder(myCampFather);
                        }
                        else if(Schema.SBQQ__Subscription__c.SObjectType == objectType){
                            SBQQ__Subscription__c myCampFather = (SBQQ__Subscription__c)retrievedObj[0];
                            SRV_CampaignStatusInbound.terminateOrder(myCampFather);
                        }
                    }
                    else{
                        throw new CustomException('Order/Contract cannot be found.');
                    }
                }
                
            }
            else throw new BadRequestException('NULL Request Body');
        }
        catch(BadRequestException badReqEx){
            //LogUtility.saveCampaignStatusLog(request.requestBody.toString());
            res.responseBody = Blob.valueOf('Bad Request - '+badReqEx.getMessage());
            res.statusCode = 400;
        }
        catch(CustomException ce){
			//LogUtility.saveCampaignStatusLog(request.requestBody.toString());
            res.responseBody = Blob.valueOf(ce.getMessage());
            res.statusCode = 500;
        }
        catch(Exception e){
			//LogUtility.saveCampaignStatusLog(request.requestBody.toString());
            res.statusCode = 500;
        }
    }
    
    public class InboundDataWrapper{
        public String campaignCode {get;set;}
        public String campaignRemoteId {get;set;}
        public String customerId {get;set;}
        public String customerRemoteId {get;set;}
        public String statusCode {get;set;}
        public String statusMessage {get;set;}
        public List<Detail> details {get;set;}
    }
    
    public class Detail{
        public String key {get;set;}
        public String value {get;set;}
    }
    
    public class BadRequestException extends Exception{}
    public class CustomException extends Exception{}
}