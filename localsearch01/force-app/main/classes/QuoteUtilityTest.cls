@isTest
private class QuoteUtilityTest {
    
    @testSetup static void setup() {
        
        Account testAcc = Test_DataFactory.generateAccounts('Account Name Test',1,false)[0];
        insert testAcc;
        Contact testCont = Test_DataFactory.generateContactsForAccount(testAcc.Id, 'Last Name Test', 1)[0];
        insert testCont;
        Opportunity testOpp = Test_DataFactory.generateOpportunity('Test Opp', testAcc.Id);
        insert testOpp;
        Billing_Profile__c testBilling = Test_DataFactory.generateBillingProfiles(testAcc.id, 'TestName', '', 1)[0];            
        insert testBilling;
        SBQQ__Quote__c testQuote= Test_DataFactory.generateQuote(testOpp.Id, testAcc.Id, testCont.Id, testBilling.Id);
        insert testQuote;
        
    }
    
      @isTest static void compareOwnerTest(){
        try{
            SBQQ__Quote__c testQuote = [SELECT id, name, OwnerId FROM SBQQ__Quote__c LIMIT 1];     
            Id profileId = userinfo.getProfileId();
            
            System.debug('Profile id: '+profileId);
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;               
            Boolean checkOwner = QuoteUtility.compareOwner(testQuote.Id, profileName);
            system.assert(checkOwner);

          	Boolean checkOwnerWithExc = QuoteUtility.compareOwner(testQuote.OwnerId, profileName);
            system.assert(checkOwnerWithExc);
          	
        } catch (Exception ex){
            system.assert(!String.isBlank(ex.getMessage()));
        }        
    }
    
            
      @isTest static void getOpportunityDataTest(){
        try{
        SBQQ__Quote__c testQuote = [SELECT Id, SBQQ__Opportunity2__c, Billing_Profile__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__Quote__c opp = QuoteUtility.getOpportunityData(testQuote.Id);
        System.assertEquals(testQuote.Id, opp.Id);
        QuoteUtility.getOpportunityData(null);
        } catch (QueryException ex){
            system.assert(!String.isBlank(ex.getMessage()));
        }        
    }
    
    @isTest static void getStageTest(){
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> stageValues = fieldResult.getPicklistValues();
        List<String> options = new List<String>();
        for (Schema.PicklistEntry stage: stageValues) {
            if(stage.getValue() == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON || stage.getValue() == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST){
                options.add(stage.getValue());
            }
        }
        Map<String,String> stages = QuoteUtility.getStage();
        system.assertEquals(stages.get(options[0]), options[0]);
    }
    
     @isTest static void getCloseReasonTest(){
		Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_reason__c.getDescribe();
        List<Schema.PicklistEntry> lossReason = fieldResult.getPicklistValues();
        Map<String, String> reasonmp = QuoteUtility.getCloseReason();
         String reason = lossReason[0].getValue();
        system.assertEquals(reasonmp.get(reason), lossReason[0].getValue()) ;
     }
  
    @isTest static void closeOppTestSuccess(){
        
        Opportunity opp = [Select ID, Name, StageName, Opportunity_reason__c From Opportunity where Name = 'Test Opp' LIMIT 1];
       	String oppStageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        
        Test.startTest();
        try{
            QuoteUtility.closeOpp(opp.Id, oppStageName, null);
            Opportunity oppUpdated = [SELECT ID, Name, StageName, Opportunity_reason__c From Opportunity where Name = 'Test Opp' LIMIT 1];
            system.assertEquals(oppStageName, oppUpdated.StageName);
             QuoteUtility.closeOpp(null, null , null);
        }catch (DmlException ex){
            system.assert(!String.isBlank(ex.getMessage())); 
        }
        catch (Exception exc){
            system.assert(!String.isBlank(exc.getMessage()));
        }
        Test.stopTest();

    }
}