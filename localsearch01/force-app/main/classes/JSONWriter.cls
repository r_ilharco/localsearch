/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Service class for HierachicalQuoteLinesController.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2019-01-04
* @systemLayer    Service

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-07-20      Added Start Date and ASAP on Quote Lines related list
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class JSONWriter {

    /**
	* ───────────────────────────────────────────────────────────────────────────────────────────────┐
	* Build quoteline hierarchy map as JSON Structure with Field Api Names changed by the keyPairs map
	* ────────────────────────────────────────────────────────────────────────────────────────────────
	* @input param		hierarchyMap		A one-level depth Hierarchy Map composed by 
	* 										SBQQ__QuoteLine__c Objects
	* @input param		keyPairs			Pairs of Keys to convert a Field API Name in an arbitrary 
	* 										JSON property name
	* @input param		translatedValues	Value translated in user language
	* @return   		String				JSON Structure with Field Api Names changed by the 
	* 										keyPairs map	
	* ───────────────────────────────────────────────────────────────────────────────────────────────┘
	*/
    public static String getJSONbyHierarchyMap(Map<SBQQ__QuoteLine__c, List<SBQQ__QuoteLine__c>> hierarchyMap, Map<String,String> keyPairs, Map<Id, Map<String, String>> translatedValues){
        
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartArray();
        
        for(SBQQ__QuoteLine__c ql : hierarchyMap.keySet()) printObjectToJSON(gen, ql, keyPairs, hierarchyMap.get(ql), translatedValues);
        
        gen.writeEndArray();

        return gen.getAsString();
    }

    private static void printObjectToJSON(JSONGenerator gen, SBQQ__QuoteLine__c ql, Map<String,String> keyPairs, List<SBQQ__QuoteLine__c> children, Map<Id, Map<String, String>> translatedValues){
        gen.writeStartObject();
        gen.writeStringField('name', String.valueOf(ql.get('Name')));
        if(ql.Place__r.PlaceId__c != null)
        gen.writeStringField('PlaceID__c', ql.Place__r.PlaceId__c);
        gen.writeNumberField('Subscription_Term__c', Integer.valueOf(ql.Subscription_Term__c));
        gen.writeNumberField('SBQQ__ListPrice__c', ql.SBQQ__ListPrice__c);
        if(ql.SBQQ__TotalDiscountRate__c != null){
            Decimal discount = ql.SBQQ__TotalDiscountRate__c;
            Decimal roundedDiscount = (Decimal)Math.round(discount)/100;
            gen.writeNumberField('SBQQ__TotalDiscountRate__c', roundedDiscount);
        }
        gen.writeBooleanField('showLBx', Boolean.valueOf(ql.get('Is_LBx__c')));
        gen.writeDateField('SBQQ__StartDate__c', Date.valueOf(ql.get('SBQQ__StartDate__c')));
        gen.writeBooleanField('ASAP_Activation__c', Boolean.valueOf(ql.get('ASAP_Activation__c')));
        printObject(gen, ql, KeyPairs, translatedValues);

        if(children!=null){
            String keyvalue = '_children';
            gen.writeFieldName(keyvalue);
            gen.writeStartArray();
            
            for(SBQQ__QuoteLine__c qlchild : children){
                gen.writeStartObject();
                gen.writeStringField('name', String.valueOf(qlchild.get('Name')));
                gen.writeBooleanField('showLBx', Boolean.valueOf(qlchild.get('Is_LBx__c')));
                gen.writeNumberField('Subscription_Term__c', Integer.valueOf(qlchild.Subscription_Term__c));
                gen.writeNumberField('SBQQ__ListPrice__c', qlchild.SBQQ__ListPrice__c);
                if(qlchild.SBQQ__TotalDiscountRate__c != null){
                    Decimal discount = qlchild.SBQQ__TotalDiscountRate__c;
                    Decimal roundedDiscount = (Decimal)Math.round(discount)/100;
                    gen.writeNumberField('SBQQ__TotalDiscountRate__c', roundedDiscount);
                }
                
                gen.writeDateField('SBQQ__StartDate__c', Date.valueOf(qlchild.get('SBQQ__StartDate__c')));
        		gen.writeBooleanField('ASAP_Activation__c', Boolean.valueOf(ql.get('ASAP_Activation__c')));
                
                printObject(gen, qlchild, KeyPairs, translatedValues);
                gen.writeEndObject();
            }

            gen.writeEndArray();

        }

        gen.writeEndObject();
    }

    private static void printObject(JSONGenerator gen, SBQQ__QuoteLine__c ql, Map<String, String> KeyPairs, Map<Id, Map<String, String>> translatedValues){
        
        for(String key : keyPairs.keySet()){
            String value = null;
            if(translatedValues.containsKey(ql.Id) && translatedValues.get(ql.Id).containsKey(key)){
                value = translatedValues.get(ql.Id).get(key);
            }
            
            //System.debug('[JSONWriter.printObjectToJSON] (key, value): ' + '(' + keyPairs.get(key) + ',' + String.valueOf(ql.get(key)) + ')' );
            gen.writeStringField(String.valueOf(keyPairs.get(key)), (ql.get(key)==null)?'':String.valueOf(((value!=NULL)?value:ql.get(key))));
        }
    }
}