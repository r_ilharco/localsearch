global class ContractSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        //Activate Contract Batch

        //ContractActivation_Batch job = new ContractActivastion_Batch();
        //Database.executeBatch(job,50); //First error: Too many callouts: 101


        Date dt_from = Date.valueOf(System.Today()) - Integer.valueOf(Label.Contract_job_Prior_days);
        Date dt_to = Date.valueOf(System.Today());    
        String status = ConstantsUtil.CONTRACT_STATUS_DRAFT;        

        List<Contract> listContract=[SELECT id FROM Contract WHERE ( Status = :status AND StartDate >= :dt_from AND StartDate <= :dt_to) LIMIT 50];
        ContractActivationQueueJob contrQue = new ContractActivationQueueJob(listContract);
        System.enqueueJob(contrQue);

        //Activate Contract Batch        
        status=ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION;
        //Terminate Contract
        List<Contract> listContract_term=[SELECT id FROM Contract WHERE ( Status = :status AND TerminateDate__c >= :dt_from AND TerminateDate__c <= :dt_to) LIMIT 50];
        ContractTerminationQueueJob contrQue_term = new ContractTerminationQueueJob(listContract_term);
        System.enqueueJob(contrQue_term);      
        
        //Terminate Subscription        
        status=ConstantsUtil.CONTRACT_STATUS_ACTIVE;      
        String status1=ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION;            
      
        List<Contract> listContract_subs=[SELECT id FROM Contract WHERE ( (Status = :status OR Status = :status1) AND Terminate_Subs_Date__c >= :dt_from AND Terminate_Subs_Date__c <= :dt_to) LIMIT 50];
        ContractTerminateSubscripQueueJob contrQue_subs = new ContractTerminateSubscripQueueJob(listContract_subs);
        System.enqueueJob(contrQue_subs);

        status=ConstantsUtil.CONTRACT_STATUS_IN_CANCELLATION;
        //Cancellation Contract
        List<Contract> listContract_can=[SELECT id FROM Contract WHERE (Status = :status AND Cancel_Date__c >= :dt_from AND Cancel_Date__c <= :dt_to) LIMIT 50];
        ContractCancellationQueueJob contrQue_can = new ContractCancellationQueueJob(listContract_can);
        System.enqueueJob(contrQue_can);            
        
        //Amend opp to Subscription Activate
		String StageName='Closed Won';        
        String sStatus=ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        
		Set<ID> cids = new Set<ID>();
        //select SBQQ__AmendedContract__r.SBQQ__Quote__c,SBQQ__AmendedContract__c from Opportunity where SBQQ__AmendedContract__c !=null and StageName='Closed Won'         
        List<Opportunity> lcids=[select SBQQ__AmendedContract__c from Opportunity where SBQQ__AmendedContract__c !=null and StageName=:StageName AND DAY_ONLY(CreatedDate) >= :dt_from AND DAY_ONLY(CreatedDate) <= :dt_to];
        System.debug(lcids);

        For(Opportunity o: lcids)
            {
		        integer count = [SELECT count() FROM SBQQ__Subscription__c where SBQQ__Contract__c=:o.SBQQ__AmendedContract__c and Subsctiption_Status__c !=:sStatus];
				//System.debug(o.SBQQ__AmendedContract__c+'***'+o.SBQQ__AmendedContract__r.SBQQ__Quote__c+' '+ count);
                if(count > 0){
	                cids.add(o.SBQQ__AmendedContract__c); // Get AccountId which subscription not Active
                }
            }
        //System.debug(cids);
        
         if(cids.size() >0){
	        ContractSubsActivateQueueJob contrQue_amnd = new ContractSubsActivateQueueJob(cids);
    	    System.enqueueJob(contrQue_amnd);         
        }
    
    // //Trial Activation
    //     status=ConstantsUtil.QUOTELINE_STATUS_TRIAL_REQUESTED;
    //     List<SBQQ__QuoteLine__c> listQuoteLine=[SELECT Id, Name, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Account__c, SBQQ__Quote__r.SBQQ__Primary__c, SBQQ__Quote__r.SBQQ__Opportunity2__c, SBQQ__ProductCode__c, SBQQ__ProductName__c, SBQQ__Quantity__c, Place__c, Place__r.PlaceID__c, Status__c, SBQQ__RequiredBy__c, SBQQ__StartDate__c, TrialStartDate__c, TrialEndDate__c FROM SBQQ__QuoteLine__c 
    //                 WHERE (TrialStatus__c  = :status AND TrialStartDate__c >= :dt_from AND TrialStartDate__c <=:dt_to and Place__c !=null ) limit 110];
    //     TrialActivationQueueJob listQuoteLine_Q = new TrialActivationQueueJob(listQuoteLine);
    //     System.enqueueJob(listQuoteLine_Q);            
           
    }
}