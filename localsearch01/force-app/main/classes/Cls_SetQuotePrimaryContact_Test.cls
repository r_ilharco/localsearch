@isTest
public class Cls_SetQuotePrimaryContact_Test {
	
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
    }



    @TestSetup
    public static void setup(){
        insertBypassFlowNames();

        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        AccountTriggerHandler.disableTrigger = true;
        Test_Billing.createAccount();
        
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
            c.Primary__c = true;
        }
        
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        Test_Billing.createOpp(accounts[0].id);
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        insert contacts;
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c,Filtered_Primary_Contact__c,SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        system.debug('quote: ' + q);
        Test_Billing.generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
		q.SBQQ__Status__c = ConstantsUtil.Quote_Status_Accepted;
        q.Filtered_Primary_Contact__c = null ;
        q.SBQQ__PrimaryContact__c = null ;
        update q;
        QuoteLineTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        AccountTriggerHandler.disableTrigger = false;
    }

    public static void generateQuoteLine(String quoteId, String productId, Date startDate, String requiredById, String billingFreq){
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__StartDate__c = startDate;
        quoteLine.CategoryID__c = 'testcategoryid';
        quoteLine.Category__c = 'Category Name';
        quoteLine.LocationID__c = 'testlocationid';
        quoteLine.Location__c = 'Location Name';
        quoteLine.SBQQ__Quantity__c= 1;
        quoteLine.SBQQ__ListPrice__c = 1290;
        quoteLine.Total__c = 490;
        quoteLine.Ad_Context_Allocation__c = 'testadcontextallocationid';
        quoteLine.Campaign_Id__c = 'testcampaignid';
        quoteLine.Contract_Ref_Id__c = 'testcontractrefid';
        quoteLine.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
        quoteLine.SBQQ__RequiredBy__c = requiredById;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.SBQQ__BillingFrequency__c =  billingFreq;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.Subscription_Term__c = '12';
        insert quoteLine;
    }

    @isTest
    public static void test_Cls_SetQuotePrimaryContact_Test(){  


        Test.startTest();
        
		Cls_SetQuotePrimaryContact primary = new Cls_SetQuotePrimaryContact(100); 
        Database.executeBatch(primary, 100); 
        Cls_SetQuotePrimaryContact.scheduleMe();
		Cls_SetQuotePrimaryContact.scheduleMe('Cls_SetQuotePrimaryContact01','0 0 0-6 ? * * *',40000);
        Test.stopTest();
        
        system.assertEquals( [SELECT Id FROM Contact LIMIT 1].Id, [SELECT Id,Filtered_Primary_Contact__c FROM SBQQ__Quote__c LIMIT 1].Filtered_Primary_Contact__c); 
            
       
    }
}