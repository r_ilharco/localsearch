/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-27-2019
 * Sprint      : 2
 * Work item   : US_61 - blocking down selling on a quote for the same place
 * Testclass   :
 * Package     : 
 * Description : Selector Class for SBQQ__Quote__c
 * Changelog   : SF2-345, Sprint 9, Wave E - Update of Quote and Contract Template - gcasola - 09-17-2019
 */

public with sharing class SEL_Quote {

    public static Map<Id, SBQQ__Quote__c> getQuoteById(Set<Id> ids)
    {
        return new Map<Id, SBQQ__Quote__c>([SELECT	Id
                                            		,Name
                                            		,AccountOwner__c
                                            		,SBQQ__StartDate__c
                                            		,Contract_Signed_Date__c
                                            		,SBQQ__MasterContract__c
                                            		,SBQQ__Status__c
                                            		,SBQQ__Type__c
                                            		,Upgrade_Downgrade_Contract__c
                                            		,SBQQ__ExpirationDate__c
                                            		,New_EW__c
                                            		,Old_EW__c
                                            		,SBQQ__PrimaryContact__c
                                                    ,Account_Customer_Number__c
                                            		,RegionalDirector__c
                                            		,SalesManagerDeputy__c
                                            		,SalesManager__c
                                            		,SBQQ__CustomerDiscount__c
                                            		,SBQQ__Opportunity2__c
                                            		,SBQQ__Opportunity2__r.SBQQ__AmendedContract__c
                                            		,SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.SBQQ__Quote__c
                                            		,Billing_Profile__c
                                            		,Billing_Profile__r.Customer__r.Name
                                            		,Billing_Profile__r.Customer__r.UID__C
                                            		,Billing_Profile__r.Billing_Street__c
                                            		,Billing_Profile__r.Billing_Postal_Code__c
                                            		,Billing_Profile__r.Billing_City__c
                                            		,Billing_Profile__r.Mail_address__c
                                            		,Billing_Profile__r.Phone__c
													,SBQQ__Account__c
                                            		,SBQQ__Account__r.OwnerId
													,SBQQ__Account__r.Email__c
                                            		,SBQQ__Account__r.Name
                                            		,SBQQ__Account__r.BillingCity
                                            		,SBQQ__Account__r.BillingStreet
                                            		,SBQQ__Account__r.BillingPostalCode
                                            		,SBQQ__Account__r.UID__c
                                            		,SBQQ__Account__r.Phone
                                            		,SBQQ__Account__r.SkipAddressValidation__c
                                            		,SBQQ__SalesRep__c
                                            		,SBQQ__SalesRep__r.Email
                                            		,Filtered_Primary_Contact__c
                                            		,Filtered_Primary_Contact__r.Email
                                            		,Filtered_Primary_Contact__r.Language__c
                                            		,SBQQ__PrimaryContact__r.Email
                                            		,SBQQ__PrimaryContact__r.Language__c
                                            FROM 	SBQQ__Quote__c 
                                            WHERE 	Id IN :ids]);
    }
    
    public static Map<Id, SBQQ__Quote__c> getQuoteWithoutPDFDocument(Set<Id> ids)
    {
        return new Map<Id, SBQQ__Quote__c>([
            SELECT Id 
            FROM SBQQ__Quote__c 
            WHERE Id NOT IN (SELECT SBQQ__Quote__c FROM SBQQ__QuoteDocument__c)
            AND Id IN :ids
        ]);
    }
    
       //Vincenzo Laudato - SF-210
    public static SBQQ__Quote__c getQuoteByCallId(String callId){
        List<SBQQ__Quote__c> quotes = [ SELECT  Id 
                                        FROM    SBQQ__Quote__c 
                                        WHERE   Trial_CallId__c =: callId 
                                        LIMIT 1];
        
        if(!quotes.isEmpty()){
            return quotes[0];
        }
        else{
            return null;
        }
    }
    
    
    public static SBQQ__Quote__c getQuoteById(Id quoteId){
        return [SELECT 
                		Id
                		,Name
                		,SBQQ__Status__c
                		,SBQQ__Opportunity2__r.StageName
                		,SBQQ__Opportunity2__r.SBQQ__Ordered__c
                		,SBQQ__Opportunity2__r.Name
                		,SBQQ__Type__c
                		,SBQQ__SalesRep__c
                		,SBQQ__SalesRep__r.Email
                		,SBQQ__SalesRep__r.Name
                		,Approved_By_Process__c
                		,Filtered_Primary_Contact__c
                		,SBQQ__Opportunity2__r.SBQQ__AmendedContract__c
                		,Filtered_Primary_Contact__r.Salutation
                		,Filtered_Primary_Contact__r.Email
                		,Filtered_Primary_Contact__r.FirstName
                		,Filtered_Primary_Contact__r.LastName
                FROM 	SBQQ__Quote__c
                WHERE 	Id = :quoteId
               	LIMIT 	1];
    }


    public static Map<Id, SBQQ__Quote__c> getQuotebyIdAndTypeAndStatus(Set<Id> ids, List<String> status, List<String> types)
    {
        return new Map<Id, SBQQ__Quote__c>([select  id, 
                                        SBQQ__Account__c, 
                                        SBQQ__Type__c 
                                from 	SBQQ__Quote__c 
                                where 	id in: ids
                               	and 	SBQQ__Type__c not in :types
                                and     SBQQ__Status__c in: status]);
    }

    public static SBQQ__Quote__c getQuoteByTrialCallId(String externalId){
        List<SBQQ__Quote__c> quotes = [ SELECT  Id 
                                        FROM    SBQQ__Quote__c 
                                        WHERE   Trial_CallId__c =: externalId 
                                        LIMIT 1];
        
        if(!quotes.isEmpty()){
            return quotes[0];
        }
        else{
            return null;
        }

    }

    public static Map<Id, SBQQ__Quote__c> getQuotesByOpportunityIdsAndStatuses(Set<Id> opportunityIds, Set<String> statuses){
        return new Map<Id, SBQQ__Quote__c>([SELECT  id, 
                                        SBQQ__Status__c, 
                                        SBQQ__Opportunity2__c 
                                FROM 	SBQQ__Quote__c 
                                WHERE 	SBQQ__Opportunity2__c IN :opportunityIds
                                AND     SBQQ__Status__c IN :statuses]);
    }
}