@isTest
public class Test_UserManagementRESTService {
    @testSetup
    static void setupData()
    {
        Test_DataFactory.insertFutureUsers();
    }
    
    @isTest
    public static void test_deactivateUser_OK()
    {
        RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
        res.statusCode = 200;
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	UserManagementRESTService.deactivateUser('managerFederationId',false);
        Test.stopTest();
		        
    }
    
    @isTest
    public static void test_deactivateUser_alreadyDeactivated()
    {
        User u = [SELECT Id, IsActive FROM User WHERE Username = 'managertester@test.com' LIMIT 1];
        RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
        res.statusCode = 200;
        
        u.IsActive = false;
        update u;
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	UserManagementRESTService.deactivateUser('managerFederationId',false);
        Test.stopTest();
		        
    }
    
    @isTest
    public static void test_deactivateUser_CannotBeActivated()
    {
        User u = [SELECT Id, IsActive FROM User WHERE Username = 'managertester@test.com' LIMIT 1];
        RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
        res.statusCode = 200;
        
        u.IsActive = false;
        update u;
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	UserManagementRESTService.deactivateUser('managerFederationId',true);
        Test.stopTest();
		        
    }
    
}