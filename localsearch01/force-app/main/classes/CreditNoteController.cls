public class CreditNoteController {
    
    @AuraEnabled        
    public static Map<String, object> getDefaultCreditNoteValues(string invoiceId) {
        Map<String, object> result = new Map<String, Object>();
        
        try{
                       
            List<Invoice_Order__c> invoiceOrderList = [SELECT Invoice__c, Invoice__r.Invoice_Code__c, Invoice__r.Process_Mode__c, Invoice__r.Status__c,
                                                       Invoice__r.Amount__c, Invoice__r.Customer__c, Invoice__r.Billing_Profile__c, Invoice__r.Billing_Profile__r.Channels__c,
                                                       Contract__c, Contract__r.TerminateDate__c, Invoice__r.Tax_Mode__c
                                                       FROM Invoice_Order__c 
                                                       WHERE Invoice__c =: invoiceId LIMIT 1];
            
            if(invoiceOrderList.size() > 0){
                
                String invoiceCodeValue = 'FAK';                         
                if(invoiceOrderList[0].Invoice__r.Invoice_Code__c == invoiceCodeValue){
                    
                    String processModeValue = 'ModeSwissList';
                    if(invoiceOrderList[0].Invoice__r.Process_Mode__c != processModeValue){
                        
                        String statusValueDraft = 'Draft';
                        String statusValueInCollection = 'In Collection';
                        String statusValueCancelled = 'Cancelled';
                        //if(invoiceOrderList[0].Invoice__r.Status__c != statusValueDraft && invoiceOrderList[0].Invoice__r.Status__c != statusValueInCollection){
                        //vlaudato
                        if(invoiceOrderList[0].Invoice__r.Status__c != statusValueCancelled){
                            if(invoiceOrderList[0].Invoice__r.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                                result.put('Was_Exempt__c', ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_YES);
                            }else{
                                result.put('Was_Exempt__c', ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_NO);
                            }
                            result.put('Manual__c', true);
                            system.debug('Manual__c: ' + true);
                            
                            result.put('Contract__c', invoiceOrderList[0].Contract__c);
                            system.debug('Contract__c: ' + invoiceOrderList[0].Contract__c);
                            
                            result.put('Account__c', invoiceOrderList[0].Invoice__r.Customer__c);
                            system.debug('Account__c: ' + invoiceOrderList[0].Invoice__r.Customer__c);
                            
                            result.put('Execution_Date__c', Date.today());//invoiceOrderList[0].Contract__r.TerminateDate__c);
                            system.debug('Execution_Date__c: ' + Date.today());
                            /*if(invoiceOrderList[0].Contract__r.TerminateDate__c != null){
                                result.put('Period_From__c', invoiceOrderList[0].Contract__r.TerminateDate__c);
                                system.debug('Period_From__c: ' + invoiceOrderList[0].Contract__r.TerminateDate__c);
                            }else{
                                result.put('Period_From__c', Date.today());
                                system.debug('Period_From__c: ' + Date.today());
                            }*/
                            
                            
                            
                            //result.put('Period_To__c', invoiceOrderList[0].Contract__r.TerminateDate__c);
                            //system.debug('Period_To__c: ' + invoiceOrderList[0].Contract__r.TerminateDate__c);
                            
                            ExistingCreditNoteInfo existingCreditNotes = getExistingCreditNoteInfo(invoiceOrderList[0].Invoice__c);
                                
                            Integer totalNumber = 0;
                            Decimal totalAmount = 0;
                            if(existingCreditNotes != null && existingCreditNotes.totalNumber > 0){
                                totalNumber = existingCreditNotes.totalNumber;
                                totalAmount = existingCreditNotes.totalAmount;                                
                            }
                            
                            Decimal maxPossibleAmount = invoiceOrderList[0].Invoice__r.Amount__c - totalAmount;
                            result.put('Max_Possible_Amount', maxPossibleAmount);    
                            system.debug('Max_Possible_Amount: ' + maxPossibleAmount);
                            
                            result.put('Value__c', maxPossibleAmount);    
                            system.debug('Value__c: ' + maxPossibleAmount);
                            
                            //result.put('Value__c', invoiceOrderList[0].Invoice__r.Amount__c);    
                            //system.debug('Value__c: ' + invoiceOrderList[0].Invoice__r.Amount__c); 
                                                                                   
                            result.put('Reimbursed_Invoice__c', invoiceOrderList[0].Invoice__c);
                            system.debug('Reimbursed_Invoice__c: ' + invoiceOrderList[0].Invoice__c);
                            
                            system.debug('invoice : ' + invoiceOrderList[0].Invoice__r);
                            system.debug('bp: ' + invoiceOrderList[0].Invoice__r.Billing_Profile__r);
                            
                            Billing_Profile__c billingProfile = invoiceOrderList[0].Invoice__r.Billing_Profile__r;                                 
                            if(billingProfile != null){
                                result.put('Billing_Profile__c', invoiceOrderList[0].Invoice__r.Billing_Profile__c);
                                system.debug('billingProfile: ' + invoiceOrderList[0].Invoice__r.Billing_Profile__c);
                                
                                system.debug('all billing channel: ' + billingProfile.Channels__c);
                                
                                if(billingProfile.Channels__c != null){
                                    string channel = billingProfile.Channels__c.substringBefore(';');
                                    result.put('Billing_Channel__c', channel);                                   
                                    system.debug('1st billing channel: ' + channel);
                                }
                                else{
                                    result.put('Billing_Channel__c', '');
                                }
                            }
                            
                            Id contractId = invoiceOrderList[0].Contract__c;
                            /*List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, Name, SBQQ__ContractNumber__c, SBQQ__ProductName__c, SBQQ__EndDate__c, 
                                                                            Next_Invoice_Date__c, SBQQ__Contract__r.SBQQ__Evergreen__c
                                                                            FROM SBQQ__Subscription__c 
                                                                            WHERE SBQQ__Contract__c =: contractId AND Place__c <> NULL LIMIT 1];*/
                            List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, Name, SBQQ__ContractNumber__c, SBQQ__ProductName__c, SBQQ__EndDate__c, 
                                                                            Next_Invoice_Date__c, SBQQ__Contract__r.SBQQ__Evergreen__c, SBQQ__StartDate__c
                                                                            FROM SBQQ__Subscription__c 
                                                                            WHERE SBQQ__Contract__c =: contractId AND SBQQ__Product__r.One_time_Fee__c != true
                                                                            LIMIT 1];
                            
                            List<Invoice_Item__c> invoiceItems = [SELECT Id, Accrual_From__c
                                                                 FROM Invoice_Item__c
                                                                 WHERE Subscription__c = :subscriptionList[0].Id AND Invoice_Order__r.Invoice__c = :invoiceId
                                                                 LIMIT 1];
                            
                            if(subscriptionList.size() > 0){
                                String title = subscriptionList[0].SBQQ__ContractNumber__c + ' ' + subscriptionList[0].SBQQ__ProductName__c;                                
                                if(totalNumber > 0){
                                    title = title + ' N' + (totalNumber+1);
                                }
                                
                                result.put('Name', title);
                            	system.debug('Name: ' + title);
                                
                                result.put('Subscription__c', subscriptionList[0].Id);
                                system.debug('Subscription__c: ' + subscriptionList[0].Id);   
                                
                                result.put('Period_To__c', subscriptionList[0].Next_Invoice_Date__c.addDays(-1));
                                system.debug('Period_To__c: ' + subscriptionList[0].Next_Invoice_Date__c.addDays(-1));
                                
                                result.put('Period_From__c', invoiceItems[0].Accrual_From__c);
                                system.debug('Period_From__c: ' + invoiceItems[0].Accrual_From__c);
                                
                                //list<CurrencyType> c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and IsCorporate=TRUE];
                                System.debug('curr: ' + UserInfo.getDefaultCurrency());
                                result.put('Currency', UserInfo.getDefaultCurrency());
                            }                      
                        }
                        else{
                            //vlaudato
                            //result.put('Error_Message', ConstantsUtil.INVOICE_STATUS_DRAFT_OR_IN_COLLECTION);
                            //system.debug('Error_Message: ' + ConstantsUtil.INVOICE_STATUS_DRAFT_OR_IN_COLLECTION);  
                            result.put('Error_Message', ConstantsUtil.INVOICE_STATUS_CANCELLED_ERROR);                     
                            return result;
                        }
                    }
                    else{
                        result.put('Error_Message', ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST);
                        system.debug('Error_Message: ' + ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST);                    
                        return result;
                    }               
                }
                else{
                    result.put('Error_Message', ConstantsUtil.INVOICE_CODE_FAK);
                    system.debug('Error_Message: ' + ConstantsUtil.INVOICE_CODE_FAK);              
                    return result;
                }                
            }
            
            Credit_Note__c c = new Credit_Note__c();
            c.Manual__c = true;
            
            // ldimartino, SF-398, START
            List<Invoice__c> invoiceList = [SELECT Tax_Mode__c
                                                       FROM Invoice__c 
                                                       WHERE Id =: invoiceId LIMIT 1];
            if(invoiceList.size() > 0){
                if(invoiceList[0].Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                    c.Was_Exempt__c = ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_YES;
                }else{
                    c.Was_Exempt__c = ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_NO;
                }
            }
            system.debug('was_Exempt: ' + c.Was_Exempt__c);
            // ldimartino, SF-398, END
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.CREDIT_NOTE_BP_NAME, ConstantsUtil.CREDIT_NOTE_BP_NAME, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, invoiceId+'');
            result.put('Error_Message', e.getMessage());
        }
        system.debug('result: ' + result);
        return result;
    }
    
    
    public static ExistingCreditNoteInfo getExistingCreditNoteInfo(Id invoiceId){
        ExistingCreditNoteInfo creditNotesInfo;
        try{            
            creditNotesInfo = new ExistingCreditNoteInfo();
            List<AggregateResult> creditNoteList = [SELECT sum(value__c) TotalAmount, count(value__c) TotalCreditNotes, Reimbursed_Invoice__c 
                                                   FROM Credit_Note__c 
                                                   GROUP BY Reimbursed_Invoice__c 
                                                   HAVING Reimbursed_Invoice__c =: invoiceId LIMIT 1];
                //[SELECT Name FROM Credit_Note__c WHERE Reimbursed_Invoice__c =: invoiceId];
            
            if(creditNoteList.size() > 0){
                creditNotesInfo.totalNumber = (Integer)creditNoteList[0].get('TotalCreditNotes');
                creditNotesInfo.totalAmount = (Decimal)creditNoteList[0].get('TotalAmount');
            }
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.CREDIT_NOTE_BP_NAME, ConstantsUtil.CREDIT_NOTE_BP_NAME, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, invoiceId+'');
        }
        
        return creditNotesInfo;
    }

    /*
    public static String getInvoiceCodePickListValue(String apiName){
        string pickValue = null;
        for( Schema.PicklistEntry entry : Invoice__c.Invoice_Code__c.getDescribe().getPicklistValues()) {
            if(entry.getValue() == apiName){
                pickValue = entry.getLabel();
                break;
            }        
        }
        
        system.debug('invoice pick value: ' + pickValue);
        return pickValue;
    }
    
    public static String getProcessModeCodePickListValue(String apiName){
        string pickValue = null;
        for( Schema.PicklistEntry entry : Invoice__c.Process_Mode__c.getDescribe().getPicklistValues()) {
            if(entry.getValue() == apiName){
                pickValue = entry.getLabel();
                break;
            }        
        }
        
        system.debug('ProcessMode pick value: ' + pickValue);
        return pickValue;
    }
    
    public static String getStatusPickListValue(String apiName){
        string pickValue = null;
        for( Schema.PicklistEntry entry : Invoice__c.Status__c.getDescribe().getPicklistValues()) {
            if(entry.getValue() == apiName){
                pickValue = entry.getLabel();
                break;
            }        
        }
        
        system.debug('Status pick value: ' + pickValue);
        return pickValue;       
    }
	*/
    //vlaudato
    /*@AuraEnabled
    public static void doCallout(Id invoiceId){
        Set<Id> ids = new Set<Id>{invoiceId};
        Database.executeBatch(new SwissbillingTransferJob(null,ids));        
    }*/
    
    public class ExistingCreditNoteInfo{
        Integer totalNumber{get; set;}
        Decimal totalAmount{get; set;}
        
        public ExistingCreditNoteInfo(){
            this.totalNumber = 0;
            this.totalAmount = 0;
        }
    }

}