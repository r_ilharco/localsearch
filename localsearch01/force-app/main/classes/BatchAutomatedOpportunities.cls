global class BatchAutomatedOpportunities implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    
    private boolean RunFirstTime = false;
    global BatchAutomatedOpportunities(){}
    global BatchAutomatedOpportunities(boolean runFirstTime){
        this.RunFirstTime = runFirstTime;
    }
    
    global database.querylocator start(Database.BatchableContext bc) {
        //Date today = Date.valueOf(System.Today());
        //List<SBQQ__Subscription__c> allSubscriptions = new  List<SBQQ__Subscription__c>();
        //List<SBQQ__Subscription__c> subscriptions = new  List<SBQQ__Subscription__c>();
        
        string query = 'Select Id,Total__c, Package_Total__c, RequiredBy__c, SBQQ__Contract__r.Account.OwnerId, SBQQ__Contract__r.SBQQ__RenewalOpportunity__c,SBQQ__Contract__r.AccountId,SBQQ__Contract__c,SBQQ__Contract__r.EndDate, SBQQ__ContractNumber__c,SBQQ__Product__r.Opportunity_Start_Date_DMC__c, SBQQ__Product__r.Opportunity_Start_Date_Telesales__c,  SBQQ__Product__r.Opportunity_Expiration_Date_DMC__c, SBQQ__Product__r.Opportunity_Expiration_Date_Telesales__c, SBQQ__Product__r.DMC_Campaign__c,SBQQ__Product__r.Telesales_Campaign__c from SBQQ__Subscription__c where SBQQ__Product__r.Create_automatic_renewal_opportunity__c = true and SBQQ__Contract__r.Status = \'' + ConstantsUtil.CONTRACT_STATUS_ACTIVE + '\' And In_Termination__c = false AND SBQQ__Contract__r.SBQQ__Evergreen__c = false order By SBQQ__Product__r.Opportunity_Start_Date_DMC__c DESC';
       /* allSubscriptions = Database.query(query);
        system.debug('allSubscriptions query::'+query);
        system.debug('allSubscriptions ::'+allSubscriptions.size());
        if(!allSubscriptions.isEmpty()){
            for(SBQQ__Subscription__c sub :allSubscriptions){
                if(RunFirstTime){
                    if(sub.SBQQ__Contract__r.EndDate > today && sub.SBQQ__Contract__r.EndDate <= (today + (Integer.valueOf(sub.SBQQ__Product__r.Opportunity_Start_Date_DMC__c)))){
                        subscriptions.add(sub);
                    }
                } else if(sub.SBQQ__Contract__r.EndDate == (today + (Integer.valueOf(sub.SBQQ__Product__r.Opportunity_Start_Date_DMC__c)))){
                    subscriptions.add(sub);
                }
            }
        }
        system.debug('subscriptions ::'+subscriptions.size());
        return subscriptions;*/

        return Database.getQueryLocator(query); 
        
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        Date today = Date.valueOf(System.Today());
        List<SBQQ__Subscription__c> subscriptions = new  List<SBQQ__Subscription__c>();
        if(!scope.isEmpty()){
            for(SBQQ__Subscription__c sub :scope){
                if(RunFirstTime){
                    if(sub.SBQQ__Contract__r.EndDate > today && sub.SBQQ__Contract__r.EndDate <= (today + (Integer.valueOf(sub.SBQQ__Product__r.Opportunity_Start_Date_DMC__c)))){
                        subscriptions.add(sub);
                    }
                } else if(sub.SBQQ__Contract__r.EndDate == (today + (Integer.valueOf(sub.SBQQ__Product__r.Opportunity_Start_Date_DMC__c)))){
                    subscriptions.add(sub);
                }
            }
        }
        system.debug('subscriptions :: '+subscriptions.size());
        Map<Id,List<SBQQ__Subscription__c>> contractSubsMap = new  Map<Id,List<SBQQ__Subscription__c>>();
        if(!subscriptions.isEmpty()){
            for(SBQQ__Subscription__c sub : subscriptions) {
                if(contractSubsMap.containsKey(sub.SBQQ__Contract__c)){
                    List<SBQQ__Subscription__c> subsList = contractSubsMap.get(sub.SBQQ__Contract__c);
                    subsList.add(sub);
                    contractSubsMap.put(sub.SBQQ__Contract__c,subsList);
                }else{
                    contractSubsMap.put(sub.SBQQ__Contract__c,new List<SBQQ__Subscription__c> {sub});
                }
                
            }
        }        
        if(!contractSubsMap.isEmpty()){
            Database.SaveResult[]  resultoppinsert =  Database.insert(createOpportunityLogic(contractSubsMap, this.RunFirstTime),false);
            for (Database.SaveResult sr_opp : resultoppinsert) {
                system.debug('sr_opp.isSuccess() :: '+sr_opp.isSuccess());
                if(sr_opp.isSuccess()) {
                    System.debug('Successfully Updated OPPORTUNITY: ' + sr_opp.getId());
                }else{
                    for(Database.Error err : sr_opp.getErrors()) { 
                        System.debug('Error: '+ err.getStatusCode() + ' *****' + err.getMessage());
                    }  
                }   
            }
        }
        
    }
    
    private static List<Opportunity> createOpportunityLogic(Map<Id,List<SBQQ__Subscription__c>> contractSubsMap, boolean runFirstTime){
        set<string> oppNames = new set<string>();
        List<Opportunity> opportunityList = new List<Opportunity>();
        List<Opportunity> opportunityToCreate= new List<Opportunity>();
        Date today = Date.valueOf(System.Today());
        
        Automated_Opportunity_Settings__c ao = Automated_Opportunity_Settings__c.getOrgDefaults();
        Id ownerId = (Id)ao.Owner_ID__c ;
        
        for(Id contractId : contractSubsMap.keySet()){
            SBQQ__Subscription__c subs = contractSubsMap.get(contractId)[0];
            if(runFirstTime && subs.SBQQ__Contract__r.SBQQ__RenewalOpportunity__c != null){ continue; }
            Boolean isTelesales = runFirstTime && (subs.SBQQ__Contract__r.EndDate <= (today + Integer.valueOf(subs.SBQQ__Product__r.Opportunity_Start_Date_Telesales__c)));
            Opportunity opp = new Opportunity();
            opp.AccountId = subs.SBQQ__Contract__r.AccountId;
            opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION;
            opp.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS; 
            opp.Amount = 0;
            for(SBQQ__Subscription__c sub : contractSubsMap.get(contractId)){
                if(sub.RequiredBy__c == null){
                    opp.Amount += sub.Package_Total__c != null ? sub.Package_Total__c : 0;
                    system.debug('opp.Amount ::::: > ' +opp.Amount);
                }
            }
            opp.Name = Label.Automated_opportunity+subs.SBQQ__ContractNumber__c;
            oppNames.add(opp.Name);
            if(isTelesales){
                opp.CloseDate = subs.SBQQ__Contract__r.EndDate;
                opp.Automated_Opportunity_Flow__c = ConstantsUtil.OPPORTUNITY_FLOW_Telesales;
                opp.OwnerId = ownerId;
                opp.Expiration_Date__c = subs.SBQQ__Contract__r.EndDate;
                opp.CampaignId = subs.SBQQ__Product__r.Telesales_Campaign__c;
                opp.SBQQ__RenewedContract__c = subs.SBQQ__Contract__c; // SPIII-5215
            } else {
                opp.CloseDate =  (subs.SBQQ__Contract__r.EndDate - (Integer.valueOf(subs.SBQQ__Product__r.Opportunity_Expiration_Date_DMC__c)));
                opp.Automated_Opportunity_Flow__c = ConstantsUtil.OPPORTUNITY_FLOW_DMC;
                opp.OwnerId = subs.SBQQ__Contract__r.Account.OwnerId;
                opp.Expiration_Date__c = (subs.SBQQ__Contract__r.EndDate - (Integer.valueOf(subs.SBQQ__Product__r.Opportunity_Expiration_Date_DMC__c)));
                opp.Telesales_opportunity_Expiration_Date__c = subs.SBQQ__Product__r.Opportunity_Expiration_Date_Telesales__c;
                opp.Telesales_Campaign_ID__c = subs.SBQQ__Product__r.Telesales_Campaign__c;
                opp.CampaignId = subs.SBQQ__Product__r.DMC_Campaign__c;
                opp.Contract_EndDate__c = subs.SBQQ__Contract__r.EndDate;
                opp.AO_Contract__c = subs.SBQQ__ContractNumber__c;
                opp.SBQQ__RenewedContract__c = subs.SBQQ__Contract__c; // SPIII-5215
            }
            
            system.debug('Opp ::: '+opp); 
            opportunityList.add(opp);
        }
        
        Map<string, integer> mapopps = new Map<string, integer>();
        for(AggregateResult ar : [select count(id) cnt, Automated_Opportunity_Flow__c, Name 
                                  from opportunity 
                                  where  StageName = :ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION
                                  and Type = :ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS
                                  and Automated_Opportunity_Flow__c in (:ConstantsUtil.OPPORTUNITY_FLOW_Telesales,:ConstantsUtil.OPPORTUNITY_FLOW_DMC)
                                  and name in :oppNames
                                  group by Name, Automated_Opportunity_Flow__c]){
                                      string key = ar.get('Name') + '-'+ar.get('Automated_Opportunity_Flow__c');
                                      mapopps.put(key, integer.valueof(ar.get('cnt')));
                                  }
        
        for(Opportunity o :opportunityList){
            string key = o.name+'-'+o.Automated_Opportunity_Flow__c;
            if(!mapopps.containsKey(key))
                opportunityToCreate.add(o);
        }
        system.debug('opportunityToCreate ::: '+opportunityToCreate.size());
        return opportunityToCreate;
    }
    
    
    global void finish(Database.BatchableContext bc){
        
        
    }  
    
    global void execute(SchedulableContext sc) {
        //  BatchAutomatedOpportunities automatedOpportunities = new BatchAutomatedOpportunities(); 
        database.executebatch(this, 200);
    }
    
    public static String sched = '0 0 1 * * ?';
    global static string scheduleMe() {
        string name = 'BatchAutomatedOpportunities';
        if (Test.isRunningTest() ) {
            name = name + system.now();
        }
        return scheduleMe(name, sched) ;      
    }
    
    global static string scheduleMe(string name,String schedule) {
        return System.schedule(name, schedule, new BatchAutomatedOpportunities());       
    }
    
}