@isTest
public class Test_OrderCaseUtility {
	@testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Order Management Insert Only,Order Management';
		insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;		
        
        Product2 product = generteFatherProduct();
        insert product;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;

        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
		insert new GlobalConfiguration_cs__c(skillCallContract__c = true);
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
    }
    @isTest
    static void test_OrderUtility_ManualActivation(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, 
                                               SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c,
                                               SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
		insert fatherOI;
        Test.startTest();
        OrderUtility.activateContracts(new List<Id>{order.Id});
        OrderCaseUtility.updateCase(new Set<Id>{order.Id});
        Test.stopTest();
    }
    
    public static Product2 generteFatherProduct(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}