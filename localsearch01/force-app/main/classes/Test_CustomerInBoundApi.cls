@isTest
private class Test_CustomerInBoundApi{
static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    @testSetup
    private static void setup(){
        insertBypassFlowNames();
        TestSetupUtilClass.createAccounts(1);
        Account a = [SELECT ID FROM account LIMIT 1];
        
        
        
    }
    
	@isTest
    private static void testGetCostumerApi_200(){  
        try{
            account a = [SELECT id,name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Phone,Email__c,GoldenRecordID__c,POBox__c,P_O_Box_City__c,P_O_Box_Zip_Postal_Code__c,PreferredLanguage__c,Status__c,Mobile_Phone__c,Fax,Website,UID__c,SalesChannel__c,ClientRating__c FROM account LIMIT 1];
            a.Customer_Number__c = '200123456';
        	RestRequest req = new RestRequest();
            req.params.put('CustomerNumber', a.Customer_Number__c); 
		    RestResponse res = new RestResponse();
        
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
                CustomerInBoundApi.getaccount();
                customerinboundapi.Customerinfo custominfo = new customerinboundapi.CustomerInfo(a);
            Test.stopTest();
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }


}