/**
* Author	  : Alessandro L. Marotta <amarotta@deloitte.it>
* Date	  : 17-10-2019
* Sprint      : Sprint 11
* Work item   : Feedback #95
* Testclass   : Test_CustomOrderActivationController
* Package     : 
* Description : Controller for Custom Order Activation component
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella
* 2020-11-20      SPIII-3636 - MyCash order activation fail - added check on service date in
				  activateOrder()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class CustomOrderActivationController {
    private static String fixedFieldStringForChild = 'Product2.Name';
    private static String fixedLabelStringForChild = 'Name';

    @AuraEnabled
    public static List<CustomOrderActivationWrapper> getOrderItems(Id orderId){

        Set<String> advProdGroup = new Set<String>{ 
                                                    ConstantsUtil.PRODUCT_GROUP_ADVONLINE,
                                                    ConstantsUtil.PRODUCT_GROUP_LOCALBANNER,
                                                    ConstantsUtil.PRODUCT_GROUP_SEARCHBANNER,
                                                    ConstantsUtil.PRODUCT_GROUP_LOCALMOBILEFIRST,
                                                    ConstantsUtil.PRODUCT_GROUP_LOCALTOPLISTING
                                                };
                
        List<OrderItem> orderItems = SEL_OrderItem.getParentOrderItemsByOrderId(orderId);
        String productGroupCode = orderItems[0].Product2.Product_Group__c;
        if(orderItems[0].Product2.Family.equalsIgnoreCase('Disused')) productGroupCode = orderItems[0].Product2.Family;
        
        String productGroupType = '';
        if(productGroupCode.contains('MyCampaign')) productGroupType = orderItems[0].Product2.ProductCode.contains('SEARCH') || orderItems[0].Product2.ProductCode.contains('VISUAL') ? 'search' : 'social';
        
        List<CustomOrderActivationWrapper> elToReturn = new List<CustomOrderActivationWrapper>();
        
        String orderStatus = orderItems[0].Order.Status;
        String queryStringOrdConfig = 'Select 	Current_Status__c,Manual_Activation__c,Manual_Amend__c,Manual_Cancellation__c,Manual_Termination__c,Next_Status__c, No_Manual_Step_Required__c,Product_Code__c,'+
            ' Product_Group__c,Send_to_Backend__c from Order_Management__mdt '+
            ' WHERE No_Manual_Step_Required__c = false '+
            ' AND Current_Status__c =:orderStatus ' +
            ' AND Product_Group__c =: productGroupCode ';

        if(String.isNotEmpty(productGroupType)) queryStringOrdConfig += ' AND Product_Code__c = :productGroupType';
        
        String orderType ;
        if(orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_NEW)) queryStringOrdConfig += ' AND Manual_Activation__c = true';
        if(orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION)) queryStringOrdConfig +=' AND Manual_Cancellation__c = true';
        if(orderItems[0].Order.Custom_Type__c.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION)) queryStringOrdConfig+=' AND Manual_Termination__c = true';
        if(orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT)) queryStringOrdConfig+=' AND Manual_Amend__c = true';
        
        List<Order_Management__mdt> orderManagementConfig = Database.query(queryStringOrdConfig);
        if(orderManagementConfig.size() >0){
            //Manage Amendment, Termination, Cancellation Order Type 
            if((orderItems[0].Order.Custom_Type__c.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION)||
                orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT)||
                orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION)) 
               && !advProdGroup.contains(productGroupCode)){
                   CustomOrderActivationWrapper ordElem = new CustomOrderActivationWrapper(); 
                   ordElem.status = ConstantsUtil.statusMap.get(orderItems[0].Order.Custom_Type__c.toUpperCase());
                   if(orderItems[0].Order.Type == ConstantsUtil.ORDER_TYPE_AMENDMENT) ordElem.status = ConstantsUtil.statusMap.get(orderItems[0].Order.Type.toUpperCase());
                   ordElem.isFullActivation = true;		
                   ordElem.isSecondActivation = false;
                   //ordElem.ordStatus =orderItems[0].Order.Status;
                   ordElem.hideChild = true;
                   elToReturn.add(ordElem);
               }
               else{
                   System.debug('productGroupCode '+productGroupCode);
                   Boolean isSecondActivation = orderManagementConfig[0].Current_Status__c == orderManagementConfig[0].Next_Status__c;
                   List<Order_Activation__mdt> orderActivationRecords = [SELECT MasterLabel, QualifiedApiName,
                                                                         Product_Code__c, Required_Fields_for_Order_Activation__c,
                                                                         Editable_Fields_for_Order_Activation__c,
                                                                         HideChild__c
                                                                         FROM   Order_Activation__mdt
                                                                         WHERE  Product_Group__c =:productGroupCode];
                   if(orderActivationRecords.size() > 0){
                       Order_Activation__mdt  mdt = orderActivationRecords[0];
                       Set<String> fieldsForMaster = new Set<String>();
                       if(String.isNotBlank(mdt.Required_Fields_for_Order_Activation__c) && (!orderItems[0].Order.Custom_Type__c.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION) && 
                                                                                             !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT) &&
                                                                                             !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION))){
                                                                                                 fieldsForMaster.addAll(mdt.Required_Fields_for_Order_Activation__c.split(';'));
                                                                                             }
                       Set<String> editableFieldsForMaster= new Set<String>();
                       if(mdt.Editable_Fields_for_Order_Activation__c != null && (!orderItems[0].Order.Custom_Type__c.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION) && 
                                                                                  !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT) &&
                                                                                  !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION))){
                                                                                      editableFieldsForMaster.addAll(mdt.Editable_Fields_for_Order_Activation__c.split(';'));	
                                                                                  }
                       Set<String> allFieldForQuery = new Set<String>();
                       allFieldForQuery.addAll(fieldsForMaster);
                       allFieldForQuery.addAll(editableFieldsForMaster);
                       Set<Id> allOrderItemForQuery = new Set<Id>();
                       for(OrderItem o: orderItems) allOrderItemForQuery.add(o.Id);
                       
                       List<String> additionalField = 'Product2.ProductCode;Product2.Product_Group__c; Order.Status;Order.Type;Order.Custom_Type__c;Product2.Name;Order.Manual_Activation_Roll__c'.split(';');
                       allFieldForQuery.addAll(additionalField);
                       
                       List<OrderItem> orderItemsMaster = new List<OrderItem>();
                       orderItemsMaster = SEL_OrderItem.dynamicGetOrderItemFromIds(allOrderItemForQuery, new List<String>(allFieldForQuery));
                       if(orderItemsMaster != null && orderItemsMaster.size()>0){
                           /********************FIELD TO SHOW - ORDER ITEM CHILD******************/
                           Set<String> fixedFieldForChild = new Set<String>(CustomOrderActivationController.fixedFieldStringForChild.split(';'));                    
                           /********************LABEL TO SHOW - ORDER ITEM CHILD******************/
                           Set<String> fixedLabelForChild = new Set<String>(CustomOrderActivationController.fixedLabelStringForChild.split(';')); 
                           /********************RETRIEVE THE ITEM RECORDS******************/ 
                           
                           map<String,List<OrderItem>> orderItemMasterToChildMap = new map<String,List<OrderItem>>();
                           /********************EXECUTE QUERY - ORDER ITEM CHILD******************/
                           additionalField = 'Id;SBQQ__RequiredBy__c;Activate_Later__c;Product2.One_time_Fee__c'.split(';');
                           additionalField.addAll(fixedFieldForChild);
                           List<OrderItem> orderItemsChild = SEL_OrderItem.getOrderItemsByParent(allOrderItemForQuery, additionalField ,isSecondActivation);
                           System.debug('orderItemsChild '+orderItemsChild.size());
                           /********************MAP ID ORDER ITEM MASTER -> LIST ORDER ITEM CHILD******************/
                           if((!orderItems[0].Order.Custom_Type__c.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION) && 
                               !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT) &&
                               !orderItems[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION))){
                                   for(OrderItem o : orderItemsChild){
                                       if(orderItemMasterToChildMap.get(o.SBQQ__RequiredBy__c)==null){
                                           orderItemMasterToChildMap.put(o.SBQQ__RequiredBy__c,new List<OrderItem>());
                                       }
                                       orderItemMasterToChildMap.get(o.SBQQ__RequiredBy__c).add(o);
                                   }
                               }
                           for(OrderItem o: orderItemsMaster){
                               CustomOrderActivationWrapper ordElem = new CustomOrderActivationWrapper();  
                               
                               List<CustomOrderActivationWrapper.childElementType> listElementChild = new List<CustomOrderActivationWrapper.childElementType>();
                               List<OrderItem> orderItemChildList = orderItemMasterToChildMap.get(o.Id);
                               if(fixedFieldForChild != null && fixedFieldForChild.size()>0 && orderItemChildList != null){
                                   for(OrderItem child : orderItemChildList){
                                       System.debug('child :'+child);
                                       CustomOrderActivationWrapper.childElementType childFieldtoShow =new CustomOrderActivationWrapper.childElementType();
                                       childFieldtoShow.childId = child.Id; 
                                       childFieldtoShow.activateLater = child.Activate_Later__c;
                                       childFieldtoShow.hideItem = !child.Product2.One_time_Fee__c;
                                       childFieldtoShow.items = CustomOrderActivationController.createListItems(child,new Set<String>(),fixedFieldForChild,fixedLabelForChild); 
                                       listElementChild.add(childFieldtoShow);
                                   }
                               } 
                               if((	fieldsForMaster!=null 		&& 
                                   fieldsForMaster.size()>0 		&&
                                   listElementChild!= null			&&
                                   listElementChild.size()>0)|| (orderItemChildList == null && !isSecondActivation)){
                                       ordElem.ordItem = o;
                                       if(!isSecondActivation){
                                           ordElem.ordItemElement = CustomOrderActivationController.createListItems(o,fieldsForMaster,editableFieldsForMaster); 
                                       }
                                       ordElem.status = orderManagementConfig[0].Next_Status__c;//to change move in constants
                                       ordElem.childElement = listElementChild;
                                       ordElem.hideChild = mdt.HideChild__c; 
                                       ordElem.isSecondActivation = isSecondActivation;
                                       ordElem.isFullActivation = false;
                                       elToReturn.add(ordElem);
                                   }
                           }
                       }
                   }
                   else{
                       CustomOrderActivationWrapper ordElem = new CustomOrderActivationWrapper(); 
                       ordElem.status = Label.OrderActivation_ErrorNoManualItem;
                       elToReturn.add(ordElem);
                   }
                   
               }
        }
        else{
            //NO CONFIGURATION FOR THIS ORDER IN THIS STATUS
            CustomOrderActivationWrapper ordElem = new CustomOrderActivationWrapper(); 
            ordElem.status = Label.OrderActivation_ErrorNoManualItem;
            elToReturn.add(ordElem);
        }
        System.debug('elToReturn: '+elToReturn);
        System.debug('elToReturn: '+elToReturn.size());
        return elToReturn;
    }

    @AuraEnabled
    public static CustomOrderActivationWrapper.activationResponse activateOrder(Id orderId,String orderItems){
        //Create a SavePoint in order to avoid partial Activation
        Savepoint sp = Database.setSavepoint();
        map<Id,date> mapServiceDate = new map<Id,date>();
        List<OrderItem> orderItemsForConfig = SEL_OrderItem.getParentOrderItemsByOrderId(orderId);
        String productGroupCode = orderItemsForConfig[0].Product2.Product_Group__c;
        //TODO CHECK Logic and add custom labels
        String productGroupType = '';
        if(productGroupCode.contains('MyCampaign')){
            productGroupType = orderItemsForConfig[0].Product2.ProductCode.contains('SEARCH') || orderItemsForConfig[0].Product2.ProductCode.contains('VISUAL') ? 'search' : 'social';
        }
        // retrieve the Order Management Records
        String orderStatus = orderItemsForConfig[0].Order.Status ;
        System.debug('orderStatus:' + orderStatus);
        String queryStringOrdConfig = 'Select 	Current_Status__c,Manual_Activation__c,Manual_Amend__c,Manual_Cancellation__c,Manual_Termination__c,Next_Status__c, No_Manual_Step_Required__c,Product_Code__c,'+
            ' Product_Group__c,Send_to_Backend__c from Order_Management__mdt '+
            ' WHERE No_Manual_Step_Required__c = false '+
            ' AND Current_Status__c =:orderStatus ' +
            ' AND Product_Group__c =: productGroupCode ' ;
        if(String.isNotEmpty(productGroupType)){
            queryStringOrdConfig += ' AND Product_Code__c = :productGroupType';
        }
        String orderType ;
        if(orderItemsForConfig[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_NEW)) queryStringOrdConfig += ' AND Manual_Activation__c = true';
        if(orderItemsForConfig[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_CANCELLATION)) queryStringOrdConfig +=' AND Manual_Cancellation__c = true';
        if(orderItemsForConfig[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION)) queryStringOrdConfig+=' AND Manual_Termination__c = true';
        if(orderItemsForConfig[0].Order.Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_AMENDMENT)) queryStringOrdConfig+=' AND Manual_Amend__c = true';
        
        List<Order_Management__mdt> orderManagementConfig = Database.query(queryStringOrdConfig);
        Order_Management__mdt mdt;
        if(orderManagementConfig.size()>0) mdt = orderManagementConfig[0];
        
        CustomOrderActivationWrapper.activationResponse result = new CustomOrderActivationWrapper.activationResponse();
        
        try{
            list<CustomOrderActivationWrapper> orderItemsList =  (List<CustomOrderActivationWrapper>)JSON.deserialize(orderItems, List<CustomOrderActivationWrapper>.class);           
            
            //********* Deserialize the child *********
            for(CustomOrderActivationWrapper orderItemMaster: orderItemsList){
                orderItemMaster.childElement = new  List<CustomOrderActivationWrapper.childElementType>();
                orderItemMaster.ordItemElement = new List<CustomOrderActivationWrapper.ItemElement>();
                if(orderItemMaster.serializedChildElement != null){
                    orderItemMaster.childElement = (List<CustomOrderActivationWrapper.childElementType>)JSON.deserialize(orderItemMaster.serializedChildElement, List<CustomOrderActivationWrapper.childElementType>.class); 
                }
                if(orderItemMaster.serializedordItemElement != null){
                    orderItemMaster.ordItemElement = (List<CustomOrderActivationWrapper.ItemElement>)JSON.deserialize(orderItemMaster.serializedordItemElement, List<CustomOrderActivationWrapper.ItemElement>.class); 
                }
            }
            list<OrderItem> orderItem2Update = new list<OrderItem>();
            boolean error = false;
            Order order2update;
            set<Id> ordItemChildIdSet = new set<Id>(); 
            list<SBQQ__Subscription__c> subscription2Update = new list<SBQQ__Subscription__c>();
            boolean rejected = true;
            for(CustomOrderActivationWrapper orderItemMaster: orderItemsList){
                if(!orderItemMaster.isSecondActivation){
                    
                    orderItem orMaster = new orderItem();	
                    orMaster.Id = orderItemMaster.ordItemId;			
                    orMaster.Activate_Later__c = false;					
                    orMaster.SBQQ__Status__c = ConstantsUtil.statusCodeMap.get(orderItemMaster.status)!=null? ConstantsUtil.statusCodeMap.get(orderItemMaster.status) : orderItemMaster.status;
                    if(!orderItemMaster.status.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_REJECTED)){
                        rejected = false;
                        for(CustomOrderActivationWrapper.ItemElement reqField : orderItemMaster.ordItemElement){
                            if(reqField.value == null){
                                error = true;
                                result.result = false;
                                result.message = Label.OrderActivation_genericErrorActivation+'. '+Label.OrderActivation_requiredFieldErrorActivation;
                                result.title = Label.OrderActivation_Error;
                            }
                            if(reqField.isEditable){
                                switch on reqField.fieldType.toUpperCase(){
                                    when 'BOOLEAN'{
                                        orMaster.put(reqField.fieldName,Boolean.valueOf(reqField.value));    
                                    }
                                    when 'DATE'{
                                        orMaster.put(reqField.fieldName,Date.valueOf(reqField.value));    
                                    }
                                    when else {
                                        orMaster.put(reqField.fieldName,reqField.value);                                        
                                    }
                                }                                
                            }
                            if(orMaster.get('ServiceDate')!=null){
                                mapServiceDate.put(orMaster.Id,Date.valueOf(orMaster.get('ServiceDate')));
                            }                            
                        }
                    }
                    if(!error){
                        for(CustomOrderActivationWrapper.childElementType child : orderItemMaster.childElement){
                            orderItem orChild = new orderItem();	
                            orChild.Id = child.childId;	
                            orChild.SBQQ__Status__c = ConstantsUtil.statusCodeMap.get(orderItemMaster.status)!=null? ConstantsUtil.statusCodeMap.get(orderItemMaster.status) : orderItemMaster.status;
                            orChild.Activate_Later__c = !orderItemMaster.status.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_REJECTED) ?  child.activateLater :false ;
                            if(orMaster.get('ServiceDate')!=null){
                                orChild.ServiceDate = Date.valueOf(orMaster.get('ServiceDate'));
                                mapServiceDate.put(child.childId,Date.valueOf(orMaster.get('ServiceDate')));
                            }
                            orderItem2Update.add(orChild);
                        }
                        orderItem2Update.add(orMaster);
                    }
                    else{
                        break;
                    }   
                }
                else{
                    for(CustomOrderActivationWrapper.childElementType child : orderItemMaster.childElement){
                        orderItem orChild = new orderItem();
                        orChild.Id = child.childId;
                        if(!child.activateLater){
                            orChild.SBQQ__Status__c =ConstantsUtil.statusCodeMap.get(orderItemMaster.status)!=null? ConstantsUtil.statusCodeMap.get(orderItemMaster.status) : orderItemMaster.status;
                            ordItemChildIdSet.add(child.childId);
                        }
                        orChild.Activate_Later__c = child.activateLater;
                        orderItem2Update.add(orChild);
                    }
                }
            }
            
            if(!orderItemsList[0].isSecondActivation){
                order2update = new Order();
                order2update.id = orderId;
                order2update.Status = rejected? ConstantsUtil.ORDER_STATUS_REJECTED :  (ConstantsUtil.statusCodeMap.get(orderItemsList[0].status)!=null? ConstantsUtil.statusCodeMap.get(orderItemsList[0].status) : orderItemsList[0].status);//mettere status
                    }
                        //Create the List of Subscription to update
            if(orderItem2Update.size()>0){
                subscription2Update = SEL_Subscription.getSubscriptionToUpdateFromOrderItem(ordItemChildIdSet);
            }
            for(SBQQ__Subscription__c subscriptionItem :subscription2Update){
                subscriptionItem.Activate_Later__c = false;
                subscriptionItem.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
            }
            
            
            if(!error){
                //Update Order
                if(mdt != null && mdt.Send_to_Backend__c){
                    OrderUtility.orderItemToNewServiceDate = mapServiceDate;
                    System.enqueueJob(new SRV_OrderManager.QueueableActivateContracts(new List<Id>{orderId}));
                    //OrderUtility.activateContracts(new List<Id>{orderId}); 
                }
                if(order2update != null ){
                    update order2update;
                }
                //Update List of OrderItemChild
                update orderItem2Update;
                if(order2update != null && !rejected ){
                    order2update.SBQQ__Contracted__c =  order2update.Status.equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED) ? true : false;
                    update order2update;
                }
                
                //Update List of Subscription
                if(subscription2Update.size()>0){
                    update subscription2Update;
                }
                //Update Case for manual order management, only when custom type is activation
                if(order2update != null){
                    OrderCaseUtility.updateCase( new Set<Id>{order2update.Id});
                }
                
                result.result = true;
                result.message =Label.OrderActivation_orderActivatedMessage;
                result.title = Label.OrderActivation_success;
            }
        }
        catch(Exception e){
            result.result = false;
            result.title = Label.OrderActivation_Error;
            result.message =Label.OrderActivation_genericErrorActivation;
            result.exceptionMessage=e.getMessage();
            System.debug('CustomOrderActivationController -- activateOrder -- exception : '+e.getMessage());
            Database.rollback(sp);
        }
        
        return result;
    }
    
    @AuraEnabled
    public static CustomOrderActivationWrapper.activationResponse activateAllOrder(Id orderId,String orderStatus){
        //Create a SavePoint in order to avoid partial Activation
        Savepoint sp = Database.setSavepoint();
        CustomOrderActivationWrapper.activationResponse result = new CustomOrderActivationWrapper.activationResponse();
        System.debug('orderId :'+orderId);
        List<Order> orderToHandle = new List<Order>();
        orderToHandle = [SELECT Id, Custom_Type__c, Type FROM Order WHERE Id =:orderId];
        try{
            //only order Item
            Order order2update = new Order();
            order2update.id = orderId;
            order2update.Status =ConstantsUtil.statusCodeMap.get(orderStatus);
            System.debug('orderStatus '+orderStatus);
            if(ConstantsUtil.statusCodeMap.get(orderStatus).equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED)
               && ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(orderToHandle[0].Custom_Type__c)){
                   order2update.SBQQ__Contracted__c = true; 
               }else{
                   List<OrderItem> orderItemToUpdate =  SEL_OrderItem.getOrderItemByOrderId((Id)orderId);
                   for(OrderItem ordItem :orderItemToUpdate){
                       ordItem.SBQQ__Status__c = ConstantsUtil.statusCodeMap.get(orderStatus) ;
                   }
                   update orderItemToUpdate;
                   System.debug('ORDER ITEM TO UPDATE = '+JSON.serializePretty(orderItemToUpdate));
               }
            System.debug('ORDER  TO UPDATE BEFORE = '+JSON.serializePretty(order2update));
            update order2update; 
            System.debug('ORDER  TO UPDATE AFTER = '+JSON.serializePretty(order2update));
            
            if( ConstantsUtil.statusCodeMap.get(orderStatus).equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED) && (ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(orderToHandle[0].Custom_Type__c) || ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderToHandle[0].Type))){
                ManualOrderUtil.cancellationTerminationContract(new Set<Id>{orderId});
            }
            else if(ConstantsUtil.statusCodeMap.get(orderStatus).equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED) && ConstantsUtil.ORDER_TYPE_AMENDMENT.equalsIgnoreCase(orderToHandle[0].Type)){
                OrderCaseUtility.updateCase( new Set<Id>{orderId});  
            }
            
            result.result = true;
            result.message =Label.OrderActivation_orderActivatedMessage;
            result.title = Label.OrderActivation_success;
            
        }catch(DMLException excp)
        {
            result.result = false;
            result.title = Label.OrderActivation_Error;
            result.message =Label.OrderActivation_genericErrorActivation + ' - '+excp.getdmlMessage(0);
            result.exceptionMessage=excp.getMessage();
            System.debug('CustomOrderActivationController'+excp); 
            System.debug('CustomOrderActivationController -- activateOrder -- exception : '+excp.getMessage()+' Line : '+excp.getLineNumber());
            Database.rollback(sp);                      
        } catch(Exception e){
            result.result = false;
            result.title = Label.OrderActivation_Error;
            result.message =Label.OrderActivation_genericErrorActivation;
            result.exceptionMessage=e.getMessage();
            System.debug('CustomOrderActivationController -- activateOrder -- exception : '+e.getMessage()+' Line : '+e.getLineNumber());
            Database.rollback(sp);
        }
        
        return result;
    }
    
    
    private static List<CustomOrderActivationWrapper.ItemElement> createListItems(OrderItem ordItem,
                                                                                  Set<String> fieldToShow,
                                                                                  Set<String> editableFields){
                                                                                      return createListItems( ordItem,fieldToShow,editableFields,null,null);
                                                                                  }
    private static List<CustomOrderActivationWrapper.ItemElement> createListItems(OrderItem ordItem,
                                                                                  Set<String> fieldToShow,
                                                                                  Set<String> fixedField,
                                                                                  Set<String> fixedLabel){
                                                                                      return createListItems( ordItem,fieldToShow,new Set<String>(),fixedField,fixedLabel);
                                                                                  }
    private static List<CustomOrderActivationWrapper.ItemElement> createListItems(OrderItem ordItem,
                                                                                  Set<String> fieldToShow,
                                                                                  Set<String> editableFields,
                                                                                  Set<String> fixedField,
                                                                                  Set<String> fixedLabel){
                                                                                      String objName = 'OrderItem';
                                                                                      Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
                                                                                      Map<String, Schema.SObjectField> M;
                                                                                      
                                                                                      
                                                                                      
                                                                                      List<CustomOrderActivationWrapper.ItemElement> Items = new List<CustomOrderActivationWrapper.ItemElement>();
                                                                                      if(fixedField!=null && fixedLabel!=null){
                                                                                          for(integer i = 0 ; i < fixedField.size(); i++){
                                                                                              
                                                                                              String fieldElem = (new list<string>(fixedField))[i];
                                                                                              String labelElem = (new list<string>(fixedLabel))[i];
                                                                                              CustomOrderActivationWrapper.ItemElement item = new CustomOrderActivationWrapper.ItemElement();
                                                                                              if(fieldElem.split('\\.').size()>1){
                                                                                                  list<String> relatedField = fieldElem.split('\\.');
                                                                                                  item.value = String.valueOf(ordItem.getSobject(relatedField[0]).get(relatedField[1]));
                                                                                              }
                                                                                              else{
                                                                                                  item.value = String.valueOf(ordItem.get(fieldElem));
                                                                                              }
                                                                                              item.fieldName= fieldElem;
                                                                                              item.fieldLabel = labelElem;
                                                                                              item.isEditable = false;
                                                                                              Items.add(item);
                                                                                          }
                                                                                      }
                                                                                      for(String field : fieldToShow){
                                                                                          CustomOrderActivationWrapper.ItemElement item = new CustomOrderActivationWrapper.ItemElement();
                                                                                          item.value = String.valueOf(ordItem.get(field));
                                                                                          item.fieldName= field;
                                                                                          item.fieldLabel = fieldMap.get(field).getDescribe().getLabel();
                                                                                          item.fieldType = String.valueOf( fieldMap.get(field).getDescribe().getType());
                                                                                          item.helpText = fieldMap.get(field).getDescribe().getInlineHelpText();
                                                                                          item.isEditable = editableFields.contains(field);
                                                                                          Items.add(item);
                                                                                      }
                                                                                      return Items;
                                                                                  }
    
    
}