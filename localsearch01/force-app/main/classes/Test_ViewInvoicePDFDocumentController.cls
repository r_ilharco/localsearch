/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-02-2019
 * Sprint      : 
 * Work item   : SF2-177 Invoices pdf have to be accessible
 * Testclass   : 
 * Package     : 
 * Description : Test Class for Controller for VF Page 'ViewInvoicePDFDocument' on QuickAction 'View Invoice PDF'
 * Changelog   : 
 */

@isTest
public class Test_ViewInvoicePDFDocumentController {
    
    @testSetup
    public static void setup()
    {
        Account acct = Test_DataFactory.createAccounts('AccountTest', 1)[0];
        insert acct;
        Invoice__c invoice = Test_DataFactory.createInvoices(acct, 'InvoiceTest', 1)[0];
        invoice.Correlation_Id__c = '96e8c195758dd0d0c90ff62cab222625';
        invoice.Invoice_Code__c ='GUT';
        insert invoice;
    }
    
    @isTest
    public static void test_getPdfDocument_Success()
    {
        Invoice__c invoice = [SELECT Id FROM Invoice__c][0];
        Test.setMock(HttpCalloutMock.class, new billingEndpointInvoiceDocuments_Success());
        String expectedResult = EncodingUtil.base64Encode(Blob.toPdf('PDFExample'));
        Test.startTest();
        ViewInvoicePDFDocumentController controller = new ViewInvoicePDFDocumentController(new ApexPages.StandardController(invoice));
        String result = controller.pdf;
        Test.stopTest();
    }
    
    @isTest
    public static void test_getPdfDocument_Success_Empty()
    {
        Invoice__c invoice = [SELECT Id FROM Invoice__c][0];
        Test.setMock(HttpCalloutMock.class, new billingEndpointInvoiceDocuments_Success_Empty());
        String expectedResult = EncodingUtil.base64Encode(Blob.toPdf('PDFExample'));
        
        Test.startTest();
        ViewInvoicePDFDocumentController controller = new ViewInvoicePDFDocumentController(new ApexPages.StandardController(invoice));
        String result = controller.pdf;
        Test.stopTest();
    }
    
      @isTest
    public static void test_correlationNull()
    {
        Invoice__c invoice = [SELECT Id FROM Invoice__c][0];
        invoice.Correlation_Id__c = null;
        update invoice;
        Test.setMock(HttpCalloutMock.class, new billingEndpointInvoiceDocuments_Success_Empty());
        String expectedResult = EncodingUtil.base64Encode(Blob.toPdf('PDFExample'));
        
        Test.startTest();
        ViewInvoicePDFDocumentController controller = new ViewInvoicePDFDocumentController(new ApexPages.StandardController(invoice));
        String result = controller.pdf;
        Test.stopTest();
    }
    
    @isTest
    public static void test_getPdfDocument_Error()
    {
        Invoice__c invoice = [SELECT Id FROM Invoice__c][0];
        Test.setMock(HttpCalloutMock.class, new billingEndpointInvoiceDocuments_Error());
        String expectedResult = EncodingUtil.base64Encode(Blob.toPdf('PDFExample'));
        
        Test.startTest();
        ViewInvoicePDFDocumentController controller = new ViewInvoicePDFDocumentController(new ApexPages.StandardController(invoice));
        String result = controller.pdf;
        Test.stopTest();
    }

    @isTest
    public static void test_getPdfDocument_FAK()
    {
        Invoice__c invoice = [SELECT Id FROM Invoice__c][0];
        invoice.Invoice_Code__c = 'FAK';
        update invoice;
        Test.setMock(HttpCalloutMock.class, new billingEndpointInvoiceDocuments_Success());
        String expectedResult = EncodingUtil.base64Encode(Blob.toPdf('PDFExample'));
        Test.startTest();
        ViewInvoicePDFDocumentController controller = new ViewInvoicePDFDocumentController(new ApexPages.StandardController(invoice));
        String result = controller.pdf;
        Test.stopTest();
    }
    
    public class billingEndpointInvoiceDocuments_Success implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
        	res.setHeader('Content-Type', 'application/pdf');
        	res.setBodyAsBlob(Blob.toPdf('PDFExample'));
        	res.setStatusCode(200);
        	return res;
        }
	}
    
    public class billingEndpointInvoiceDocuments_Success_Empty implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
        	res.setHeader('Content-Type', 'application/pdf');
            res.setBody('');
        	res.setStatusCode(200);
        	return res;
        }
	}
    
    public class billingEndpointInvoiceDocuments_Error implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
        	res.setHeader('Content-Type', 'application/pdf');
            res.setBody('');
        	res.setStatusCode(418);
        	return res;
        }
	}
}