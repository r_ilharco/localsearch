public with sharing class StratTrialController {
    public SBQQ__Quote__c theQuote {get;set;}
    public Id idQuote {get;set;}
    public Map<Id, Boolean> mapTrial {get;set;}
    public List<SBQQ__QuoteLine__c > listQuoteLines {get;set;}
    Map<Id, SBQQ__QuoteLine__c > mapQuoteLines = new Map<Id, SBQQ__QuoteLine__c >();
    public StratTrialController(){}
    public StratTrialController(ApexPages.StandardController stdController) {
        //theQuote = (SBQQ__Quote__c)stdController.getRecord();
        idQuote = ApexPages.CurrentPage().getparameters().get('id');
        theQuote = [SELECT Id, Name, SBQQ__Account__c, SBQQ__Account__r.Name, SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.Name, SBQQ__Primary__c FROM SBQQ__Quote__c WHERE Id=:idQuote];
        mapTrial = new Map<Id, Boolean>();
        listQuoteLines = new List<SBQQ__QuoteLine__c >();
        for(SBQQ__QuoteLine__c ql: [SELECT 
                                  Id, 
                                  Name, 
                                  SBQQ__Product__c ,
                                  SBQQ__ProductCode__c, 
                                  SBQQ__ProductName__c, 
                                  SBQQ__Quantity__c,
                                  SBQQ__RequiredBy__c,
                                  Place__c, Place_Name__c, 
                                  Status__c 
                              FROM SBQQ__QuoteLine__c 
                              WHERE SBQQ__Quote__c = :idQuote AND Status__c='Draft' AND SBQQ__Bundled__c = False]) //AND Place__c!=NULL 
        {
            listQuoteLines.add(ql);
            mapQuoteLines.put(ql.Id, ql);
            mapTrial.put(ql.Id, false);
        }
        System.debug('listQuoteLines');
    }
    
    public PageReference saveTrial() {
        System.debug('mapTrial '+mapTrial);
        Set<Id> setQLId = new Set<Id>(); 
        for(Id idQL: mapTrial.keySet()) {
            if(mapTrial.get(idQL)) {
                if(mapQuoteLines.get(idQL).Place__c==Null) {
                    ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Selected item not associated with any place.');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                setQLId.add(idQL);
            }
        }
        if(setQLId.isEmpty()) {
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'No item selected to start trial activation.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        List<SBQQ__QuoteLine__c> listQLinesToUpdate = new List<SBQQ__QuoteLine__c>();
        
        ObjParam oParam = new ObjParam();
        oParam.account_id = theQuote.SBQQ__Account__c;
        oParam.id = theQuote.Id;
        oParam.primary = theQuote.SBQQ__Primary__c ? 'true' : 'false';
        oParam.type = 'quote';
        oParam.technical_fields.put('correlation_id', theQuote.SBQQ__Opportunity2__c);
        for(SBQQ__QuoteLine__c ql: [SELECT Id, Name, SBQQ__ProductCode__c, SBQQ__ProductName__c, SBQQ__Quantity__c,
                                  Place__c, Place__r.PlaceID__c, Status__c, SBQQ__RequiredBy__c, SBQQ__StartDate__c, TrialStartDate__c   
                              FROM SBQQ__QuoteLine__c 
                              WHERE Id = :setQLId OR SBQQ__RequiredBy__c=:setQLId ])
        {
            if(ql.TrialStartDate__c==null) ql.TrialStartDate__c = Date.today();
            Map<String,String> mapElement = new Map<String,String>();
            mapElement.put('element_id', ql.Id);
            if(ql.Place__c!=null) mapElement.put('place_id', ql.Place__r.PlaceID__c);
            if(ql.SBQQ__RequiredBy__c!=null) mapElement.put('parent_element_id', ql.SBQQ__RequiredBy__c);
            mapElement.put('product_code', ql.SBQQ__ProductCode__c);
            mapElement.put('quantity', String.valueOf(ql.SBQQ__Quantity__c));
            mapElement.put('start_date', String.valueOf(ql.TrialStartDate__c ));
            
            oParam.elements.add(mapElement);
            
            ql.Status__c = 'Trial Requested';
            listQLinesToUpdate.add(ql);
        }
        //System.debug(oParam.elements.size());
        System.debug(JSON.serialize(oParam));
        
        String status = 'OK';
        //String status = sendRequest(oParam);
        
        if(status=='OK') {
            createTask(oParam);
            try {
                update listQLinesToUpdate;
            } catch(Exception e) {
                ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.WARNING,'Trial activation started successfully, but unable to update status.');
                ApexPages.addMessage(myMsg);
                return null;
            }
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.CONFIRM,'Trial Activation started successfully.');
            ApexPages.addMessage(myMsg);
            
            return null;
        } else {
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Attempt Failed. Unable to start trial activation.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
    
    public class ObjParam {
        String account_id;
        String id;
        String primary;
        String type;
        Map<String,String> technical_fields;
        List<Map<String,String>> elements;
        public ObjParam() {
            technical_fields = new Map<String,String>();
            elements = new List<Map<String,String>>();
        }
    }
/*{
    "account_id": "123456789",
    "elements": [{
        "element_id": "111",      
        "place_id": "OmLdt_vAE-AleL-7MvvT7gxxxxx",
        "product_code": "SLS001",
        "quantity": 2,
        "start_date": "2018-10-25"
    }, {
        "element_id": "112",
        "parent_element_id": "111",
        "product_code": "ORER001",
        "quantity": 3,
        "start_date": "2018-10-25"
    }, {
        "element_id": "113",      
        "parent_element_id": "111",           
        "product_code": "OREV001",      
        "quantity": 2,      
        "start_date": "2018-10-25"
    }],
    "id": "masselliSadiqueDavideEusuf",
    "primary": "true",
    "type": "quote",
    "technical_fields":
    {
        "correlation_id":"23232323"
    }
}*/
    
    public String getSecurityToken() {
        Httprequest request = new HttpRequest(); 
        Http http = new Http();
        request.setMethod('POST');
        request.setEndpoint('https://apicloud-test.localsearch.tech/ei/security/token');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'grant_type=client_credentials&';
        body += 'client_id=eg35g3uc2gt4tqz5yb4a76rn&';
        body += 'client_secret=748t5fXJGD';
        request.setBody(body);
        HttpResponse res = http.send(request);
        //System.debug(res);
        if(res.getStatusCode()==200){
            Map<String,Object> mapBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            return (String) mapBody.get('access_token');
        }
        return null;
    }
    
    private void createTask(ObjParam param) {
        Task nTask = new Task(
            Subject     ='Trial Activation', 
            OwnerId     = UserInfo.getUserId(), 
            ActivityDate = Date.today()+5, 
            WhatId      = param.Id,
            Status = 'Offen' // Open
        );
        insert nTask;
    }
    
    private String sendRequest(ObjParam param) {
        String token = this.getSecurityToken();
        Httprequest request = new HttpRequest(); 
        Http http = new Http();
        request.setMethod('POST');
        request.setEndpoint('https://apicloud-test.localsearch.tech/realsubscription');
        request.setHeader('Authorization', 'Bearer '+token);
        request.setHeader('Content-Type', 'application/json');
        
        String body = JSON.serialize(param);
        request.setBody(body);
        HttpResponse res = http.send(request);
        System.debug('res: '+res);
        System.debug('res.getStatusCode = '+res.getStatusCode());
        if(res.getStatusCode()==200){
            Map<String,Object> mapBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            return (String) mapBody.get('message');
        }
        return null;
    }
}