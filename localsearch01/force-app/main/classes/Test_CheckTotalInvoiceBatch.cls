@isTest
public class Test_CheckTotalInvoiceBatch {
   
    @TestSetup
    static void makeData(){
        Test_Billing.testSetup();
        Account account = new Account(Name = 'Test');
        insert account;

        Invoice__c invoice = new Invoice__c();
        invoice.Name = 'InvoiceTest';
        invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
        invoice.Customer__c = account.Id;
        invoice.Amount__c = 500;
        insert invoice;
        
        Invoice_Order__c invoiceOrder = new Invoice_Order__c();
        invoiceOrder.Invoice__c = invoice.Id;
        invoiceOrder.Contract__c = [SELECT Id FROM Contract LIMIT 1].Id;
        insert invoiceOrder;
        
        Invoice_Item__c invoiceItem = new Invoice_Item__c();
        invoiceItem.Invoice_Order__c = invoiceOrder.Id;
        invoiceItem.Subscription__c = [SELECT Id FROM SBQQ__Subscription__c LIMIT 1].Id;
        invoiceItem.Amount__c = 150;
        invoiceItem.Accrual_From__c = Date.today();
        insert invoiceItem;
        
    }

    @isTest
    private static void batchTest() {
        Test.startTest();
            Database.executeBatch(new CheckTotalInvoiceBatch());
        Test.stopTest();
        Invoice__c invoiceTest = [SELECT Invoice_wrongly_generated__c FROM Invoice__c LIMIT 1];
        System.assertEquals(invoiceTest.Invoice_wrongly_generated__c, true);

    }
}