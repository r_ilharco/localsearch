/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.      
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Package     : 
 * Description : Test class for ContractTriggerHandler & ContractTriggerHelper
 * Changelog   : 
 */

@isTest
public class Test_ContractTrigger {

    @testSetup
    public static void test_setupData(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
		
		quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
		
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insert(){
        List<String> fieldNamesOrder = new List<String>(  Schema.getGlobalDescribe().get('Order').getDescribe().fields.getMap().keySet());
        String queryOrder = 'SELECT ' +String.join( fieldNamesOrder, ',' ) +' FROM Order LIMIT 1';
        Order order = Database.query(queryOrder);
        
        Test.startTest();
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.Pricebook2Id = Test.getStandardPricebookId();
        insert contract;
        
        delete contract;
        undelete contract;
        Test.stopTest();
    }
    
    @isTest
    public static void test_update(){
        List<String> fieldNamesOrder = new List<String>(  Schema.getGlobalDescribe().get('Order').getDescribe().fields.getMap().keySet());
        String queryOrder = 'SELECT ' +String.join( fieldNamesOrder, ',' ) +' FROM Order LIMIT 1';
        Order order = Database.query(queryOrder);
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.Pricebook2Id = Test.getStandardPricebookId();
        insert contract;
        
        Test.startTest();
		contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
        update contract;
        Test.stopTest();
    }
    
    @isTest
    public static void test_creditNoteCheck(){
        
        SBQQ.TriggerControl.disable();
		ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c fatherQL = Database.query(queryQuoteLine);

        List<String> fieldNamesOrder = new List<String>(  Schema.getGlobalDescribe().get('Order').getDescribe().fields.getMap().keySet());
        String queryOrder = 'SELECT ' +String.join( fieldNamesOrder, ',' ) +' FROM Order LIMIT 1';
        Order order = Database.query(queryOrder);
        
        List<String> fieldNamesOrderItem = new List<String>(  Schema.getGlobalDescribe().get('OrderItem').getDescribe().fields.getMap().keySet());
        String queryOrderItem = 'SELECT ' +String.join( fieldNamesOrderItem, ',' ) +' FROM OrderItem LIMIT 1';
        OrderItem fatherOI = Database.query(queryOrderItem);
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.Pricebook2Id = Test.getStandardPricebookId();
        insert contract;
        
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        insert fatherSub;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        update fatherSub;
        
        Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
        upgradeOpportunity.CloseDate = Date.today().addDays(1);
		upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
		upgradeOpportunity.Upgrade__c = true;     
		upgradeOpportunity.Downgrade__c = false;
		insert upgradeOpportunity;
        
        String upgradeOpportunityId = upgradeOpportunity.Id;
        String queryUpgradeQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :upgradeOpportunityId LIMIT 1';
        SBQQ__Quote__c upgradeQuote = Database.query(queryUpgradeQuote);
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQLUpgrade = Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('SLS001').Id, Date.today(), null, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLS001').Id));
        fatherQLUpgrade.Subscription_To_Terminate__c = fatherSub.Id;
        insert fatherQLUpgrade;
        
        upgradeQuote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update upgradeQuote;

        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_UPGRADE);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        orderMap.put('SBQQ__Quote__c',upgradeQuote.Id);
        Order upgradeOrder = Test_DataFactory.generateOrder(orderMap);
        upgradeOrder.OpportunityId = upgradeOpportunity.Id;
        insert upgradeOrder;
        
        OrderItem upgradeOI = Test_DataFactory.generateFatherOrderItem(upgradeOrder.Id, fatherQLUpgrade);
        upgradeOI.Subscription_To_Terminate__c = fatherSub.Id;
        insert upgradeOI;
        
        ContractTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        Contract upgradeContract = Test_DataFactory.generateContractFromOrder(upgradeOrder, false);
        insert upgradeContract;
        Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        SubscriptionTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
    }
}