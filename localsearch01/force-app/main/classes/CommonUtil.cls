global class CommonUtil {
    public static String getAllFields(String sObjectName){
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().SObjectType.getDescribe().fields.getMap();
        List<String> accessiblefFields = new List<String>();
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefFields.add(field.getDescribe().getName());
        }
        String allFields='';
        for(String fieldName : accessiblefFields)
            allFields += fieldName+',';
        allFields = allFields.subString(0,allFields.length()-1);
        return allFields;
    }
}