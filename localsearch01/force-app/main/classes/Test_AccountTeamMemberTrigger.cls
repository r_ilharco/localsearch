@isTest
public class Test_AccountTeamMemberTrigger {
    
	@testSetup
    public static void test_setupData(){
        
		ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'User Management';
        insert byPassFlow;
    }
    
    @isTest
    public static void test_cannotDeleteDiffTerritory(){
        
        //UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        
        Territory2 firstLevelTerritory = [SELECT Id,name FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1];
        Territory2 firstLevelTerritory2 = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1 OFFSET 1];
        system.debug('firstLevelTerritory '+firstLevelTerritory);
        runTest(firstLevelTerritory.Id, firstLevelTerritory2.Id);
        
        //UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
    }
    
    
    @isTest
    public static void test_canDeleteSameTerritory(){
        
        Territory2 firstLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1];
        
        runTest(firstLevelTerritory.Id, firstLevelTerritory.Id);
        
    }
    
    @isTest
    public static void test_canDeleteSameTerritory2(){
        
        Territory2 firstLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1];
        Territory2 secondLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = :firstLevelTerritory.Id AND Territory2Model.State = 'Active' LIMIT 1];
        
        runTest(secondLevelTerritory.Id, firstLevelTerritory.Id);
    }
    
    public static void runTest(Id territoryOnAccount, Id territoryOnUser){
		
        UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        
        List<User> usersToInsert = new List<User>();
        
        Profile salesProfile = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SALESPROFILE_LABEL LIMIT 1];
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SYSADMINPROFILE_LABEL LIMIT 1];
        User adminUser = new User(ProfileId = adminProfile.Id,
                                  LastName = 'adminTestClassUser',
                                  Email = 'adminTestClassUser@test.com',
                                  Username = 'adminTestClassUser@test.com'+System.currentTimeMillis(),
                                  CompanyName = 'ADMINTESTCLASS',
                                  Title = 'adminTestClass',
                                  Alias = 'admTEST',
                                  TimeZoneSidKey='America/Los_Angeles',
                                  EmailEncodingKey = 'UTF-8',
                                  LanguageLocaleKey = 'en_US',
                                  LocaleSidKey = 'en_US'
                                 );
        usersToInsert.add(adminUser);
        UserRole sdiRole = [SELECT Id FROM UserRole WHERE Name= 'Regional Director Region 4'];
        system.debug('sdiRole id '+sdiRole.id);
        User SDIUser = new User(ProfileId = salesProfile.Id,
                                UserRoleId = sdiRole.Id,
                                LastName = 'smaTestClassUser',
                                Email = 'smaTestClassUser@test.com',
                                Username = 'smaTestClassUser@test.com'+System.currentTimeMillis(),
                                CompanyName = 'SMATESTCLASS',
                                Title = 'smaTestClass',
                                Alias = 'smaTEST',
                                TimeZoneSidKey='America/Los_Angeles',
                                EmailEncodingKey = 'UTF-8',
                                LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US',
                                Code__c = 'SRD'
                               );
        usersToInsert.add(SDIUser);        
        User DMCUser = new User(ProfileId = salesProfile.Id,
                              LastName = 'dmcTestClassUser',
                              Email = 'dmcTestClassUser@test.com',
                              Username = 'dmcTestClassUser@test.com'+System.currentTimeMillis(),
                              CompanyName = 'DMCTESTCLASS',
                              Title = 'dmcTestClass',
                              Alias = 'dmcTEST',
                              TimeZoneSidKey='America/Los_Angeles',
                              EmailEncodingKey = 'UTF-8',
                              LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US',
                              Code__c = 'DMC'
                             );
        usersToInsert.add(DMCUser);
        insert usersToInsert;
        
        insert Test_DataFactory.addPermissionSetLicenseSFCpqlicense(usersToInsert);
        
        UserTerritory2Association ut2a = new UserTerritory2Association();
        
        ut2a.Territory2Id = territoryOnUser;
        ut2a.UserId = SDIUser.Id;
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        insert ut2a;
        
        UserTerritory2Association temp = [SELECT Territory2Id FROM UserTerritory2Association WHERE UserId =: SDIUser.id ];
        Territory2 tempterr = [SELECT Name FROM Territory2 where id=:temp.Territory2Id ];
        system.debug('territory assigned to user '+tempterr.name);
        
        
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CPQUserCustomSB'];
		insert new PermissionSetAssignment(AssigneeId = SDIUser.id, PermissionSetId = ps.Id);
        
        Account account;
        AccountTeamMember teamMember;
        
        System.runAs(adminUser){
            account = Test_DataFactory.generateAccounts('Test Name Account', 1, false)[0];
            
            AccountTriggerHandler.disableTrigger = true;
            insert account;
            AccountTriggerHandler.disableTrigger = false;
            
            teamMember = new AccountTeamMember();
            teamMember.AccountAccessLevel = 'Edit';
            teamMember.AccountId = account.Id;
            teamMember.UserId = adminUser.Id;
            teamMember.OpportunityAccessLevel = 'None';
            teamMember.CaseAccessLevel = 'Read';
            teamMember.ContactAccessLevel = 'Read';
            insert teamMember;
            update teamMember;
			
            account.OwnerId = SDIUser.id;
            update account;            
            
            
            ObjectTerritory2Association ot2a = new ObjectTerritory2Association();
            ot2a.ObjectId = account.Id;
            ot2a.Territory2Id = territoryOnAccount;
            ot2a.AssociationCause = 'Territory2Manual';
            insert ot2a;
            
            ObjectTerritory2Association temp1 = [SELECT Territory2Id FROM ObjectTerritory2Association WHERE ObjectId =: account.id ];
            Territory2 tempterr1 = [SELECT Name FROM Territory2 where id=:temp1.Territory2Id ];
        	system.debug('territory assigned to account '+tempterr1.name);
            system.runas(SDIUser){
              	AccountTeamMember teamMember1 = new AccountTeamMember();
                teamMember1.AccountAccessLevel = 'Edit';
                teamMember1.AccountId = account.Id;
                teamMember1.UserId = adminUser.Id;
                teamMember1.OpportunityAccessLevel = 'None';
                teamMember1.CaseAccessLevel = 'Read';
                teamMember1.ContactAccessLevel = 'Read';            	
                insert teamMember1;
                delete teamMember1;
      		}
           	SDIUser.code__c = 'DMC';
          	update SDIUser;
            system.runas(SDIUser){
                AccountTeamMember teamMember2 = new AccountTeamMember();
                teamMember2.AccountAccessLevel = 'Edit';
                teamMember2.AccountId = account.Id;
                teamMember2.UserId = adminUser.Id;
                teamMember2.OpportunityAccessLevel = 'None';
                teamMember2.CaseAccessLevel = 'Read';
                teamMember2.ContactAccessLevel = 'Read';            	
                insert teamMember2;
                delete teamMember2;
            }
        }
        
		
        
        delete ut2a;
        
        UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
    }

}