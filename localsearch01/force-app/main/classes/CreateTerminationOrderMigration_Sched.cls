global  class CreateTerminationOrderMigration_Sched  implements Schedulable{
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(system.now().minute()+1);
        String year = string.valueOf(system.now().year());

        for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'CreateTerminationOrderMigration%']){
            if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                System.abortjob(ct.id);
            else 
                batchs.add(ct.CronJobDetail.name);
        }
        if(batchs.isEmpty()|| !batchs.contains('CreateTerminationOrderMigration0')){
            CreateTerminationOrderMigration b0 = new CreateTerminationOrderMigration(new List<String>{'0','1','a','A','b','B','2','3','e','E','f','F'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
            System.schedule('CreateTerminationOrderMigration0', strSchedule, b0);
        } 
        if(batchs.isEmpty() || !batchs.contains('CreateTerminationOrderMigration2')){
            CreateTerminationOrderMigration b2 = new CreateTerminationOrderMigration(new List<String>{'4','5','I','i','l','L','6','7','o','O','p','P'});
            String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('CreateTerminationOrderMigration2', strSchedule,b2);
        } 
        if(batchs.isEmpty() || !batchs.contains('CreateTerminationOrderMigration4')){
            CreateTerminationOrderMigration b4 = new CreateTerminationOrderMigration(new List<String>{'8','9','s','S','t','T','u','U','v','V','z','Z'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('CreateTerminationOrderMigration4', strSchedule,b4);
        }
        if(batchs.isEmpty() || !batchs.contains('CreateTerminationOrderMigration6')){
            CreateTerminationOrderMigration b6 = new CreateTerminationOrderMigration(new List<String>{'c','C','d','D','x','X','w','W','g','G','h','H','y','Y'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('CreateTerminationOrderMigration6', strSchedule,b6);
        }
        if(batchs.isEmpty() || !batchs.contains('CreateTerminationOrderMigration8')){
            CreateTerminationOrderMigration b8 = new CreateTerminationOrderMigration(new List<String>{'m','M','n','N','j','J','q','Q','r','R','k','K'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('CreateTerminationOrderMigration8', strSchedule,b8);
        }
    }
}