@isTest
public class ContractSubsActivateQueueJobTest {

    @isTest
    public static void testExecute(){
        try{
            Test.startTest();
            Set<ID> idSet = new Set<ID>();
            ContractSubsActivateQueueJob queueJob = new ContractSubsActivateQueueJob(idSet);
            queueJob.execute(null);
            Test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }
    
}