/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* TestClass for OpportunityTrigger (Handler and Helper) and SRV_Pricebook classes.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2019-12-02
* @systemLayer    Test
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_OpportunityTrigger {
    
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Quote Management,Quote Line Management,Send Owner Notification,Community User Creation';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
		OrderProductTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        QuoteDocumentTriggerHandler.disableTrigger = true;

                
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierDMCTest';
        String assertion = NULL;
        Profile salesProfile = [select id from Profile where Name ='Sales' limit 1];
        Boolean create = true;
        User currentUser = [select id, code__c from User where id = :UserInfo.getUserId()];
        currentUser.code__c = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        currentUser.Sales_Channel__c = 'FieldSales';
        UserTriggerHandler.disableTrigger=true;
        update currentUser;
        UserTriggerHandler.disableTrigger=false;
      /*  Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'username@test.it',
                'user.federationidentifier' => 'FederationIdentifierDMCTest',
                'user.code__c' => 'DMC',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1DMC',
                'user.lastname' => 'lastName1DMC',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'DMCt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'departmentNumberTest',
                'user.callcenter' => 'AltitudeSalesforceConnector',
                'user.profileid' => salesProfile.id
        };
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
		User dmcUser = suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);

        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SYSADMINPROFILE_LABEL LIMIT 1];
      User adminUser = new User(ProfileId = adminProfile.Id,
                                  LastName = 'TestClassAdminLast',
                                  Email = 'testclassadmin@user.com',
                                  Username = 'testclassadmin@user.com'+System.currentTimeMillis(),
                                  CompanyName = 'TESTADM',
                                  Title = 'ADMtest',
                                  Alias = 'ADMtest',
                                  TimeZoneSidKey='America/Los_Angeles',
                                  EmailEncodingKey = 'UTF-8',
                                  LanguageLocaleKey = 'en_US',
                                  LocaleSidKey = 'en_US'
                                 );
        
        UserTriggerHandler.disableTrigger = true;
       insert adminUser;
       insert dmcUser;
        UserTriggerHandler.disableTrigger = false;
        list<User> listpermissionUser = new list<User>();
        listpermissionUser.add(adminUser);*/
        //listpermissionUser.add(dmcUser);
      //  insert Test_DataFactory.addPermissionSetLicenseSFCpqlicense(listpermissionUser);
        
        Account account = Test_DataFactory.generateAccounts('Test Account Not Duplicated', 1, false)[0];
        account.UID__c = 'CHY-YYYYYY';
        account.Phone = '3334567899';
        account.Email__c = 'testSetupEmail@test.com';
        account.Customer_Number__c = '1111111111';
        account.GoldenRecordId__c = 'GoldenIdSetup';
        account.Active_DMC_Contracts__c = 0;
        insert account;
        
        
        List<Pricebook2> pricebooks = new List<Pricebook2>();
        Pricebook2 dmcPricebook = new Pricebook2(Name = 'DMC',
                                                 Description = 'DMC Pricebook',
                                                 IsActive = true,
                                                 ExternalId__c = ConstantsUtil.DMC_PRICEBOOK,
                                                 Sales_Channel__c = 'FieldSales'
                                                );
        pricebooks.add(dmcPricebook);
        /*Pricebook2 onePresencePricebook = new Pricebook2(Name = 'One Presence',
                                                         Description = 'One Presence Pricebook',
                                                         IsActive = true,
                                                         ExternalId__c = ConstantsUtil.ONEPRESENCE_PRICEBOOK,
                                                         Sales_Channel__c = 'FieldSales;Telesales'
                                                        );
        pricebooks.add(onePresencePricebook);*/
        insert pricebooks;
        
        Product2 localBANNER = generateFatherProduct_localBANNER();
        insert localBANNER;
        
        PricebookEntry localBANNEREntryStandard = new PricebookEntry(UnitPrice = 599,
                                                                     Product2Id = localBANNER.Id,
                                                                     Pricebook2Id = Test.getStandardPricebookId(),
                                                                     IsActive= true
                                                                    );
        insert localBANNEREntryStandard;
        PricebookEntry localBANNEREntryDMC = new PricebookEntry(UnitPrice = 599,
                                                                Product2Id = localBANNER.Id,
                                                                Pricebook2Id = dmcPricebook.Id,
                                                                IsActive= true
                                                               );
        insert localBANNEREntryDMC;

        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test Setup Contact', 1)[0];
        insert contact;
        
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test 1');
        insert place;

        
        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
		OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        QuoteDocumentTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_checkAccountDuplicates(){
        
        OpportunityTriggerHandler.disableTrigger = true;
		OrderTriggerHandler.disableTrigger = true;
		OrderProductTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        AccountTriggerHandler.disableTrigger = true;
        QuoteDocumentTriggerHandler.disableTrigger = true;
        
        List<Account> account = Test_DataFactory.generateAccounts('Test Name',2,false);
        account[0].Name = 'Duplicate Name Account';
        account[0].BillingStreet = '65 Forrlibuckstrasse';
        account[1].Name = 'Duplicate Name Account 2';
        account[0].LegalEntity__c = 'Foundation';
        account[1].LegalEntity__c = 'Association';
        insert account;
        
        Test.startTest();
        List<Account> accounts = [SELECT Id, isDuplicated__c FROM Account];

        Pricebook2 p2 = new Pricebook2();
        p2.Name = 'DMC';
        p2.Sales_Channel__c = 'FieldSales';
        p2.ExternalId__c = 'P2ExtId';
        insert p2;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Opportunity Name Test', account[0].Id);
        insert opportunity;
        account[1].isDuplicated__c = true;
        update account[1];
        Opportunity opportunity2 = Test_DataFactory.generateOpportunity('Opportunity Name Test 2', account[1].Id);
        insert opportunity2;        
        Test.stopTest();
        OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        QuoteDocumentTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_setPricebook(){
		
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id, Pricebook2.ExternalId__c FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            if(pbe.Pricebook2.ExternalId__c == ConstantsUtil.DMC_PRICEBOOK) product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products) productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        
      /* User adminUser = [select id from User where  Alias ='ADMtest' limit 1];
       User dmcUser = [select id from User where Alias ='DMCt1' limit 1];
        System.runAs(adminUser){
            AccountShare accShare= new AccountShare();
            accShare.AccountId = account.Id;
            accShare.AccountAccessLevel = 'Edit';
            accShare.OpportunityAccessLevel = 'Edit';
            accShare.UserOrGroupId = dmcUser.Id;
            accShare.RowCause = 'Manual';
            insert accShare;
            
            account.OwnerId = dmcUser.Id;
            update account;
        }
        */
        Test.startTest();
        
        SBQQ.TriggerControl.disable();
        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
		OrderTriggerHandler.disableTrigger = true;
        QuoteDocumentTriggerHandler.disableTrigger = true;

        
      //  System.runAs(dmcUser){
            Pricebook2 dmcPb = [SELECT Id FROM Pricebook2 WHERE ExternalId__c = :ConstantsUtil.DMC_PRICEBOOK];
            Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name 1', account.Id);
			opportunity.Pricebook2Id = dmcPb.Id;
            insert opportunity;
            
            String optyId = opportunity.Id;
            List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
            String queryQuotes = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :optyId LIMIT 1';
            SBQQ__Quote__c quote = Database.query(queryQuotes);
            SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBANNER001').Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id));
            fatherQuoteLine.URL__c = 'test.com';
            fatherQuoteLine.Ad_Context_Allocation__c = 'testAdAllocation';
            fatherQuoteLine.Category__c = 'testCategory';
            fatherQuoteLine.CategoryID__c = 'testCategoryId';
            fatherQuoteLine.Location__c = 'testLocation';
            fatherQuoteLine.LocationID__c = 'testLocationId';
            insert fatherQuoteLine;
            
            SBQQ__QuoteDocument__c quoteDocument = Test_DataFactory.generateQuoteDocument(quote.Id);
            quoteDocument.SBQQ__SignatureStatus__c  = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
            quoteDocument.SignatureDate__c = Date.today();
            insert quoteDocument;
            quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
            quote.SignatureDate__c = Date.today();
            quote.Contract_Signed_Date__c = Date.today();
            quote.SBQQ__Primary__c = true;
            update quote;
            
            Order activationOrder = createActivationOrder(account.Id, quote.Id, dmcPb.Id);
            insert activationOrder;
            OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(activationOrder.Id, fatherQuoteLine);
            insert fatherOrderItem;
            
            opportunity.SBQQ__PrimaryQuote__c = quote.Id;
            opportunity.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
            update opportunity;
            
            activationOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
            activationOrder.SBQQ__Contracted__c = true;
            update activationOrder;

            Contract contract = Test_DataFactory.generateContractFromOrder(activationOrder, false);
            insert contract;
            SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItem);
			fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
            fatherSub.Total__c = 100.0;
            insert fatherSub;
            
            fatherOrderItem.SBQQ__Subscription__c = fatherSub.Id;
            update fatherOrderItem;
            
            contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
            contract.SBQQ__Order__c = activationOrder.Id;
            update contract;
            
            fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            update fatherSub;
			
            Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
            upgradeOpportunity.CloseDate = Date.today().addDays(1);
            upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
            upgradeOpportunity.Upgrade__c = true;     
            upgradeOpportunity.Downgrade__c = false;
        
            insert upgradeOpportunity;
            
        //}
        SBQQ.TriggerControl.enable();
        QuoteTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        QuoteDocumentTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_setStartDate(){
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id, Pricebook2.ExternalId__c FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            if(pbe.Pricebook2.ExternalId__c == ConstantsUtil.DMC_PRICEBOOK) product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.ASAP_Allowed__c = true;
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        
        Pricebook2 dmcPb = [SELECT Id FROM Pricebook2 WHERE ExternalId__c = :ConstantsUtil.DMC_PRICEBOOK];
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name 1', account.Id);
        opportunity.Pricebook2Id = dmcPb.Id;
        insert opportunity;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuotes);
        QuoteLineTriggerHandler.disableTrigger = true;
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        QuoteDocumentTriggerHandler.disableTrigger = true;

        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBANNER001').Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id));
        fatherQuoteLine.URL__c = 'test.com';
        fatherQuoteLine.Ad_Context_Allocation__c = 'testAdAllocation';
        fatherQuoteLine.Category__c = 'testCategory';
        fatherQuoteLine.CategoryID__c = 'testCategoryId';
        fatherQuoteLine.Location__c = 'testLocation';
        fatherQuoteLine.LocationID__c = 'testLocationId';
        insert fatherQuoteLine;
        
        SBQQ__QuoteDocument__c quoteDocument = Test_DataFactory.generateQuoteDocument(quote.Id);
        quoteDocument.SBQQ__SignatureStatus__c  = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        quoteDocument.SignatureDate__c = Date.today();
        insert quoteDocument;
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        quote.SignatureDate__c = Date.today();
        quote.Contract_Signed_Date__c = Date.today();
        update quote;
        
        Order activationOrder = createActivationOrder(account.Id, quote.Id, dmcPb.Id);
        insert activationOrder;
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(activationOrder.Id, fatherQuoteLine);
        insert fatherOrderItem;
		
		Test.startTest();        
        opportunity.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        update opportunity;
        Test.stopTest();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        QuoteDocumentTriggerHandler.disableTrigger = false;
    }
    
    public static Product2 generateFatherProduct_localBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        product.Min_Start_Date__c = '0';
        product.Min_Start_Date__c = '365';
        product.ASAP_Allowed__c = false;
        return product;        
    }
    public static Product2 generateFatherProduct_onePRESENCE(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.PRODUCT2_PRODUCTFAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','OnePRESENCE');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONEPRESENCE001-AR');
        fieldApiNameToValueProduct.put('KSTCode__c','1234');
        fieldApiNameToValueProduct.put('KTRCode__c','123456789');
        fieldApiNameToValueProduct.put('KTRDesignation__c','One Presence');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','70');
        fieldApiNameToValueProduct.put('Product_Group__c','OnePRESENCE');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','111111');
        fieldApiNameToValueProduct.put('ProductCode','ONEPRESENCE_001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        product.Min_Start_Date__c = '0';
        product.Min_Start_Date__c = '365';
        product.ASAP_Allowed__c = false;
        return product;
    }
    public static Order createActivationOrder(String accountId, String quoteId, Id pricebookId){
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c', ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type', ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate', String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId', accountId);
        fieldApiNameToValue.put('SBQQ__Quote__c', quoteId);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.Pricebook2Id = pricebookId;
        return order;
    }
}