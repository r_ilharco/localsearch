@istest 
public class test_creditNoteController_v2 {

static ByPassFlow__c insertBypassFlowNames(){ 
            ByPassFlow__c byPassFlow = new ByPassFlow__c();
            byPassFlow.Name = Userinfo.getProfileId();
            byPassFlow.FlowNames__c = 'Opportunity Management,Community User Creation,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management,Update Contract Company Sign Info';
            insert byPassFlow;
            return byPassFlow;
    }
    @testsetup static void test_setup(){
    insertbypassflownames();
     test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
        system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
        insert psas;
        }
        Date tday = Date.today();
        tday = tday.addDays(10);
        Account testAccount = new Account(
            Name = 'TestAccount'
            ,BillingCity  = 'PT'
            ,BillingPostalCode = '8765'
            ,BillingCountry  = 'Italy'
            ,billingStreet = 'Street'
        ); 
        insert testAccount; 
        Contact testContact = new Contact(
            Email  = 'tes@test.com'
            , LastName = 'testcontact'
            , AccountId = testAccount.Id
            , MailingCity = testAccount.BillingCity
            ,MailingPostalCode = testAccount.BillingPostalCode
            ,MailingCountry  = testAccount.BillingCountry
             ); 

            billing_profile__c p = new Billing_Profile__c();
			p.Customer__c = testaccount.id;
			p.Name = 'test profile ';
			p.Billing_City__c = 'test billing city';
			p.Billing_Country__c = 'test billing country';
			p.Billing_Postal_Code__c = '1234';
			p.Billing_Contact__c = testcontact.Id;
			p.Billing_Language__c = 'Italian';
            p.Billing_Name__c = 'test billing name';
            p.Billing_Street__c  = 'test billing street';
			insert p;
         Opportunity testOpportunity = new Opportunity(
            Name = 'TestOpportunity'
            , AccountId = testAccount.Id
            , StageName = 'Qualification'
            , CloseDate = tday
        ); 
       SBQQ__Quote__c  testQuote2 = new SBQQ__Quote__c (
            SBQQ__Account__c  = testAccount.Id
            , SBQQ__Primary__c  = true
            , SBQQ__PrimaryContact__c  = testContact.Id
            , SBQQ__Opportunity2__c = testOpportunity.Id
            , trial_CallId__c = 'qtewibqleububfuvqbwecuçwbçeuvcqweucçehçhwvcuçehw'
        ); 
        Contract testContract = new Contract(
            Name = 'TestContract'
            , AccountId = testAccount.Id
            , Status = ConstantsUtil.CONTRACT_STATUS_DRAFT
            , SBQQ__Opportunity__c = testOpportunity.Id
            , SBQQ__Quote__c = testquote2.Id
            , StartDate = tday
            , Terminate_Subs_Date__c = tday
            , CallId__c = 'ewibqleububfuvqbwecuçwbçeuvcqweucçehçhwvcuçehwf'
            , sambaisbilled__c = true
        );
        
        insert testContract;
        List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',1);
        places[0].Account__c = testAccount.id;        
        insert places;
        Product2 p4 = new Product2(AutomaticRenew__c= false,
                                   Family= 'Entry Product',
                                   IsActive= true,
                                   One_time_fee__c = true,
                                   MultiplePlaceIDAllowed__c= false,
                                   Name= 'Drittplattformen Swiss List Starter',
                                   PlaceIDRequired__c= 'No',
                                   ProductCode= 'OPMBASICLOW001',
                                   SBQQ__Component__c= true,
                                   SBQQ__ConfigurationType__c= 'Disabled',
                                   SBQQ__DefaultQuantity__c= 1,
                                   SBQQ__NonDiscountable__c= true,
                                   SBQQ__Optional__c= false,
                                   SBQQ__OptionSelectionMethod__c= 'Click',
                                   SBQQ__PriceEditable__c= false,
                                   SBQQ__PricingMethodEditable__c= false,
                                   SBQQ__PricingMethod__c= 'List',
                                   SBQQ__QuantityEditable__c= false,
                                   SBQQ__SubscriptionBase__c= 'List',
                                   SBQQ__SubscriptionPricing__c= 'Fixed Price',
                                  Subscription_Term__c = '36',
                                   Base_term_Renewal_term__c = '12',
                              //     SBQQ__SubscriptionType__c= 'Evergreen',
                                   SBQQ__BillingFrequency__c='Annual',
                                   SBQQ__TaxCode__c='Standard');
        insert p4;
        permissionsetAssignment psa = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        user u = [select id from user where id=: psa.AssigneeId][0];
        List<SBQQ__Subscription__c> subs = Test_DataFactory.getSubscriptions(testAccount,places[0], testcontract, new List<Product2>{p4},1);
        system.runas(u){
         insert subs;
        }

        List<Invoice__c> invoices = Test_DataFactory.createInvoices(1,ConstantsUtil.INVOICE_STATUS_IN_COLLECTION,'	Standard0' );
        invoices[0].invoice_code__c = 'FAK';
        invoices[0].amount__c = 2000;
        invoices[0].customer__c = testaccount.id;
        invoices[0].tax_mode__c= ConstantsUtil.BILLING_TAX_MODE_EXEMPT;
        invoices[0].billing_profile__c = p.id;
        insert invoices;
        List<Invoice_Order__c> orders = Test_DataFactory.createInvoiceOrders(1, invoices[0].id);
        orders[0].contract__c = testcontract.id;
        insert orders;
        List<Invoice_item__c> items = Test_dataFactory.createInvoiceItems(1,orders[0].id);
        items[0].Amount__c = 1500;
        items[0].subscription__c = subs[0].id;
        insert items;
        credit_note__c creditnote = new credit_note__c();
        creditnote.Reimbursed_Invoice__c = invoices[0].id;
        creditnote.contract__c = testcontract.id;
        creditnote.Account__c = testaccount.id;
        creditnote.Value__c = 100;
        creditnote.Execution_Date__c = date.today();
        creditnote.Subscription__c = subs[0].id;
        insert creditnote;
}

    @istest static void test_getDefaultCreditNoteValues(){
    invoice_item__c item = [select id from invoice_item__c limit 1];
    string invoiceitemid = item.id;
    test.startTest();
    CreditNoteController_V2.getDefaultCreditNoteValues(invoiceitemid);
    test.stopTest();
}

}