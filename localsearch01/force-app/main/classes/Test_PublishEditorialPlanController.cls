@isTest
public class Test_PublishEditorialPlanController {
	@testSetup
    public static void testSetup(){
        test.startTest();
        Editorial_Plan__c ep = new Editorial_Plan__c();
        ep.Name = '2021';
        ep.Language__c = 'de';
        ep.Local_Guide_Main_Title__c = '2021';
        ep.Local_Guide_Number__c = 1234;
        ep.Plan_Closing_Date__c = Date.today().addDays(365); 
        ep.Plan_Start_Date__c = Date.today();
        ep.Planned_Publication_Date__c = Date.today().addDays(395);
        ep.Publication_Date__c = Date.today().addDays(405);
        ep.Publication__c = '2021';
		ep.External_Id__c = '1234-2021';
        insert ep;
        
        Editorial_Plan__c ep2 = new Editorial_Plan__c();
        ep2.Name = 'editorialTestName';
        ep2.Language__c = 'de';
        ep2.Local_Guide_Main_Title__c = 'editorialTestTitle';
        ep2.Local_Guide_Number__c = 2234;
        ep2.Plan_Closing_Date__c = Date.today().addDays(365); 
        ep2.Plan_Start_Date__c = Date.today();
        ep2.Planned_Publication_Date__c = Date.today().addDays(395);
        ep2.Publication_Date__c = Date.today().addDays(-1);
        ep2.Publication__c = '2021';
		ep2.External_Id__c = '2234-2021';
        insert ep2;
        
        Editorial_Plan__c ep3 = new Editorial_Plan__c();
        ep3.Name = 'editorialTestName';
        ep3.Language__c = 'de';
        ep3.Local_Guide_Main_Title__c = 'editorialTestTitle';
        ep3.Local_Guide_Number__c = 3234;
        ep3.Plan_Closing_Date__c = Date.today().addDays(365); 
        ep3.Plan_Start_Date__c = Date.today();
        ep3.Planned_Publication_Date__c = Date.today().addDays(395);
        ep3.Publication_Date__c = Date.today().addDays(390);
        ep3.Publication__c = '2021';
		ep3.External_Id__c = '3234-2021';
        insert ep3;
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;	
        SBQQ.TriggerControl.disable(); 
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(),null, null, null, 'Annual');
        fatherQL.Edition__c = ep.Id;
        insert fatherQL;
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.disable(); 
        
        Account account2 = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity2 = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote2 = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account2.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote2.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity2.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL2;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL2 = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL2);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        test.stopTest();
    }
    
    public static List<PricebookEntry> createPricebook2(Id pbId,List<Product2> prList){
        List<PricebookEntry> peList = new List<PricebookEntry> ();
        String extId = 'PE';
        for(Product2 pr :prList){
            PricebookEntry pe = new PricebookEntry();
            pe.Pricebook2Id = pbId;
            pe.Product2Id = pr.id;
            pe.UnitPrice = 50;
            pe.IsActive = true;
            pe.UseStandardPrice = false;
			peList.add(pe);
        }
		return peList;
	}
    
    public static testmethod void dateCheckAndUpdates(){
        Editorial_Plan__c ep = [SELECT Id FROM Editorial_Plan__c WHERE Local_Guide_Number__c = 1234 LIMIT 1];
        List<Order> os = [SELECT Id, Status FROM Order];
        for(Order o : os){
            o.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        }
        update os;
        List<OrderItem> ois = [SELECT Id, Edition__c FROM OrderItem];
        for(OrderItem oi : ois){
            oi.Edition__c = ep.Id;
        }
        update ois;
        PublishEditorialPlanController.dateCheckAndUpdates(ep.Id);
    }
    
    public static testmethod void dateCheckAndUpdatesFirstElse(){
        Editorial_Plan__c ep = [SELECT Id FROM Editorial_Plan__c WHERE Local_Guide_Number__c = 2234 LIMIT 1];
        PublishEditorialPlanController.dateCheckAndUpdates(ep.Id);
    }
    
    public static testmethod void dateCheckAndUpdatesSecondElse(){
        Editorial_Plan__c ep = [SELECT Id FROM Editorial_Plan__c WHERE Local_Guide_Number__c = 3234 LIMIT 1];
        PublishEditorialPlanController.dateCheckAndUpdates(ep.Id);
    }
    
    public static testmethod void batchTest(){
        SubscriptionTriggerHandler.disableTrigger = true;
        Account a = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        Test_Billing.generateContract(q.Id, a.id, opp.Id);
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        Test_Billing.generateSubs(c, qls);
        List<SBQQ__Subscription__c> queriedSubs = [SELECT Id, RequiredBy__c, Next_Invoice_Date__c FROM SBQQ__Subscription__c];
        List<SBQQ__Subscription__c> subsToUpdateList = new List<SBQQ__Subscription__c>();
        
        for(SBQQ__Subscription__c sub : queriedSubs){
            if(sub.RequiredBy__c == NULL){
                SBQQ__Subscription__c childSub = sub.clone();
                childSub.Next_Invoice_Date__c = NULL;
                childSub.RequiredBy__c = sub.Id;
                childSub.SBQQ__Quantity__c = 1;
                sub.Next_Invoice_Date__c = Date.today().addMonths(3);
                subsToUpdateList.add(sub);
                subsToUpdateList.add(childSub);
            }
        }
        
        if(subsToUpdateList != NULL && subsToUpdateList.size() > 0){
            upsert subsToUpdateList;
        }
        
        Editorial_Plan__c ep = [SELECT Id, Publication_Date__c FROM Editorial_Plan__c WHERE Local_Guide_Number__c = 1234 LIMIT 1];
        EditorialPlanInvoiceDateBatch editorialPlanBatch = new EditorialPlanInvoiceDateBatch(ep.Publication_Date__c, '2021', 1234);
        List<SBQQ__Subscription__c> subs = [SELECT Id, RequiredBy__c, RequiredBy__r.Next_Invoice_Date__c, Next_Invoice_Date__c, SBQQ__QuoteLine__r.Edition__r.Publication_Date__c, Number_of_Publications__c, SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c, SBQQ__QuoteLine__r.Edition__r.Name
                                            FROM SBQQ__Subscription__c];
		for(SBQQ__Subscription__c aSub : subs){
			aSub.Number_of_Publications__c = 3;
		}
        system.debug('subs: ' + JSON.serialize(subs));
        editorialPlanBatch.execute(null, subs);
        SubscriptionTriggerHandler.disableTrigger = false;

    }
    
    public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
}