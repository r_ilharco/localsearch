@isTest
public class UserTriggerHandlerTest {
    
    @isTest 
    public static void testCreateNewUser() {
        
        List<User> lstUser = new List<User>();
       
        for(Integer i = 0; i < 201; i++){
            String username = 'example' + i +'@example.com.prod';

            User u = new User(email='Example@example.com', 
                              LastName='lastName', 
                              FirstName = 'firstName',
                              Code__c='TLS',
                              EmailEncodingKey='ISO-8859-1',
                              TimeZoneSidKey='Europe/Berlin',
                              LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US',
                              Username = username, 
                              ProfileId = '00e1r0000027zBjAAI',
                              Alias = 'alias');
            lstUser.add(u);
        }
        
        Insert lstUser;        
        
        List<User> lstUserNew = [SELECT Id,Code__c FROM User WHERE Code__c = 'TLS'];
        
        List<id> lstUserId = new List<id>();
        for(User u : lstUserNew){
            lstUserId.add(u.id);
        }
        
        User u = lstUserNew.get(0);
        u.code__c = 'CSC';
        
        List<PermissionSetAssignment> lstPmset = [SELECT id FROM PermissionSetAssignment Where AssigneeId IN: lstUserId];
		
        update u;

    }

}