@isTest
public class ContractsOverviewControllerpage_Test {
 	
    static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Update Contract Company Sign Info';
            insert byPassTrigger;
    }
    

@testSetup
    public static void test_setupData(){
        insertBypassFlowNames();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ.TriggerControl.disable();
      
        
        Editorial_Plan__c ep = new Editorial_Plan__c();
        ep.Local_Guide_Number__c = 120;
        ep.Local_Guide_Main_Title__c = 'Local_Guide_Main_Title__c';
        ep.Local_Guide_Title_2__c = 'Local_Guide_Title_2__c';
        ep.Local_Guide_Title_3__c = 'Local_Guide_Title_3__c';
        insert ep;
        
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        productCodeToProduct.get('LBB001').SBQQ__NonDiscountable__c = false;
        productCodeToProduct.get('OPMBASICLOW001').SBQQ__NonDiscountable__c = false;
        productCodeToProduct.get('OPMBASICLOW001').PlaceIDRequired__c = 'No';
        productCodeToProduct.get('LBB001').ExternalKey_ProductCode__c = 'LGC-SPA';
        productCodeToProduct.get('LBB001').ProductCode = 'LGC-SPA';
        update productCodeToProduct.get('OPMBASICLOW001');
        update productCodeToProduct.get('LBB001');
        
        SBQQ__ProductOption__c po = new SBQQ__ProductOption__c();
        po.SBQQ__Number__c = 1;
        insert po;
        
        Account account = Test_DataFactory.generateAccounts('TestAccountName0001', 1, false)[0];
 
        insert account;
        system.debug('ACCOUNT ID :'+account.Id);
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        String opportunityId = opportunity.Id;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        quote.SBQQ__SalesRep__c = UserInfo.getUserId();
        quote.SBQQ__Opportunity2__c = opportunity.Id;
        update quote;
        
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id = :productCodeToProduct.get('LBB001').Id];
        List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',1);
        places[0].Account__c = account.id;  
        places[0].PlaceID__c = 'ZpP8fTVhKmMOLps8FiDAxQ';
        insert places;
        
        SBQQ__QuoteLine__c fatherQuoteLineWithPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        fatherQuoteLineWithPlace.TrialStartDate__c = date.today();
        fatherQuoteLineWithPlace.SBQQ__Product__c = products[0].id;
        fatherQuoteLineWithPlace.TrialEndDate__c = date.today().adddays(30);
        fatherQuoteLineWithPlace.place__c = places[0].id;
        fatherQuoteLineWithPlace.Package_Total__c = 2.50;
        fatherQuoteLineWithPlace.Edition__c = ep.Id;
        fatherQuoteLineWithPlace.Location__c = 'Location__c';
        fatherQuoteLineWithPlace.Category__c = 'Category__c';
        fatherQuoteLineWithPlace.Total__c = 2000;
        fatherQuoteLineWithPlace.DocRowNumber__c = 1;
        insert fatherQuoteLineWithPlace;
        fatherQuoteLineWithPlace.SBQQ__AdditionalDiscountAmount__c = 10;
        update fatherQuoteLineWithPlace;
        
        SBQQ__QuoteLine__c childQuoteLineWithPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        childQuoteLineWithPlace.SBQQ__RequiredBy__c = fatherQuoteLineWithPlace.id;
        childQuoteLineWithPlace.Package_Total__c = 2.50;
        childQuoteLineWithPlace.Total__c = 2000;
        childQuoteLineWithPlace.DocRowNumber__c = 2;
        childQuoteLineWithPlace.Subscription_Term__c = '12';
        insert childQuoteLineWithPlace;
        childQuoteLineWithPlace.SBQQ__Discount__c = 10;
        update childQuoteLineWithPlace;
        
        SBQQ__QuoteLine__c fatherQuoteLineWithoutPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OPMBASICLOW001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        fatherQuoteLineWithoutPlace.TrialStartDate__c = date.today();
        fatherQuoteLineWithoutPlace.SBQQ__Product__c = products[0].id;
        fatherQuoteLineWithoutPlace.TrialEndDate__c = date.today().adddays(30);
        fatherQuoteLineWithoutPlace.place__c = places[0].id;
        fatherQuoteLineWithoutPlace.Package_Total__c = 2.50;
        fatherQuoteLineWithoutPlace.SBQQ__ProductOption__c = NULL;
        fatherQuoteLineWithoutPlace.Total__c = 2000;
        fatherQuoteLineWithoutPlace.Subscription_Term__c = '36';
        fatherQuoteLineWithoutPlace.Place__c = NULL;
        fatherQuoteLineWithoutPlace.DocRowNumber__c = 3;
        insert fatherQuoteLineWithoutPlace;
        fatherQuoteLineWithoutPlace.SBQQ__AdditionalDiscountAmount__c = 10;
        update fatherQuoteLineWithoutPlace;
        
        SBQQ__QuoteLine__c childQuoteLineWithoutPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OPMBASICLOW001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        childQuoteLineWithoutPlace.SBQQ__RequiredBy__c = fatherQuoteLineWithoutPlace.id;
        childQuoteLineWithoutPlace.Package_Total__c = 2.50;
        childQuoteLineWithoutPlace.Total__c = 2000;
        childQuoteLineWithoutPlace.Place__c = NULL;
        childQuoteLineWithoutPlace.Subscription_Term__c = NULL;
        childQuoteLineWithoutPlace.DocRowNumber__c = 4;
        insert childQuoteLineWithoutPlace;
        childQuoteLineWithoutPlace.SBQQ__Discount__c = 10;
        update childQuoteLineWithoutPlace;
        
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        
        
        Order order2 = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order2.OpportunityId = opportunity.Id;
        insert order2;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order2.Id, fatherQuoteLineWithPlace);
        fatherOI.Edition__c =ep.id;
        insert fatherOI;
     	List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        childrenQLs.add(childQuoteLineWithPlace);
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order2.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
	
        Contract contract2 = Test_DataFactory.generateContractFromOrder(order2, false);
        insert contract2;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract2, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLineWithPlace.Subscription_Term__c;
        insert fatherSub;
        
         List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract2, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order2.Contract__c = contract2.Id;
        order2.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order2.SBQQ__Contracted__c = true;
        update order2;
        
        contract2.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract2.SBQQ__Order__c = order2.Id;
        update contract2;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        fatherSub.Place__c = fatherQuoteLineWithPlace.place__c;
        
        
        
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        

        Contract cntrct = Test_DataFactory.createContracts (account.Id, 1)[0];
        cntrct.SBQQ__Order__c = order.Id;
        insert cntrct;

        
        
        List<SBQQ__QuoteLine__c> ls_quoteline = new List<SBQQ__QuoteLine__c>();
        ls_quoteline.add(fatherQuoteLineWithoutPlace);
        
        List<SBQQ__Subscription__c> subs  =  generateSubs(cntrct,ls_quoteline);
		SubscriptionTriggerHandler.disableTrigger = true;
        insert subs;
        SubscriptionTriggerHandler.disableTrigger = false;        
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
		QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable();

    }
    
    
    
    

    @isTest
    public static void test_downloadPDF() {
        Test.startTest();
		ContractsOverviewControllerpage	tCoc = new ContractsOverviewControllerpage();
        tCoc.language = 'fr';
        system.debug('json :: '+json.serializePretty(prepareData()));
        tCoc.PDFData = Json.serialize(prepareData());
        tCoc.accountId =  [Select Id from account  Limit 1].Id;
        system.debug('ACCOUNT ID :'+tCoc.accountId);
        tCoc.downloadPDF();
        Test.stopTest();
    }
    
    public static List<SubscriptionsWrapper> prepareData(){
         SubscriptionsResponse res = new SubscriptionsResponse();
          Set<Id> productIds = new Set<Id>();
        string language ='fr';
        string accId = [Select Id from account Limit 1].Id;
        List<SBQQ__Subscription__c> listSubs = new List<SBQQ__Subscription__c>();
        
        String queryToPerform = 'SELECT Id,Name, SBQQ__ProductName__c, SBQQ__AdditionalDiscountAmount__c, Subsctiption_Status__c, SBQQ__RequiredById__c, '+
                              + 'SBQQ__Product__c , SBQQ__Product__r.Manual_Activation__c, SBQQ__Product__r.Production__c, Place__c , Place_Name__c, SBQQ__SubscriptionType__c, ' +
                              + 'SBQQ__Bundled__c,  SBQQ__EndDate__c,  End_Date__c, In_Termination__c, Termination_Date__c, SBQQ__StartDate__c, Total__c, Package_Total__c, '+
                              + 'Subscription_Term__c, SBQQ__Product__r.Base_term_Renewal_term__c, SBQQ__Product__r.Product_Group__c, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c, '+
            				  + 'SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Contract__r.SBQQ__Opportunity__r.Name, SBQQ__Contract__r.AccountId, SBQQ__Contract__r.Account.Customer_Number__c, '+
            				  + 'SBQQ__Contract__r.SBQQ__Quote__c, SBQQ__Contract__r.SBQQ__Quote__r.Name, SBQQ__Contract__r.Account.Name, SBQQ__Product__r.Period_of_notice__c, '+
                              + 'SBQQ__Product__r.One_time_Fee__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, SBQQ__Quantity__c, SBQQ__Contract__c, SBQQ__Contract__r.ContractNumber FROM SBQQ__Subscription__c ';
        
        String whereCause = 'WHERE SBQQ__Contract__r.AccountId = :accId ';
        
        String orderByCause = 'ORDER BY SBQQ__RequiredById__c ASC NULLS FIRST ';
        
        listSubs = Database.query(queryToPerform + whereCause + orderByCause);
        
         Map<Id, List<SBQQ__Subscription__c>> subHierarchy = new Map<Id, List<SBQQ__Subscription__c>>();
        
        for(SBQQ__Subscription__c s : listSubs){
            productIds.add(s.SBQQ__Product__c);
            if(String.isBlank(s.SBQQ__RequiredById__c)) subHierarchy.put(s.Id, new List<SBQQ__Subscription__c>());
            else{
                if(subHierarchy.containsKey(s.SBQQ__RequiredById__c)) subHierarchy.get(s.SBQQ__RequiredById__c).add(s);
                else subHierarchy.put(s.SBQQ__RequiredById__c, new List<SBQQ__Subscription__c>{s});
            }
        }
        
         List<SBQQ__Localization__c> localizations = SEL_Localization.getProductNameTranslations(productIds);
         Map<Id, Map<String, String>> productLanguageTranslation = new Map<Id, Map<String, String>>();
        
        for(SBQQ__Localization__c loc : localizations){
            
            if(!String.isBlank(loc.SBQQ__Text__c)){
                
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)) productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                else productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
            }
        }
        
              for(SBQQ__Subscription__c sub : listSubs){
            String productName = sub.SBQQ__ProductName__c;
            
            if(productLanguageTranslation.containsKey(sub.SBQQ__Product__c) && productLanguageTranslation.get(sub.SBQQ__Product__c).containsKey(language)){
                productName = productLanguageTranslation.get(sub.SBQQ__Product__c).get(language);
            }
            
            SubscriptionsWrapper wrap = new SubscriptionsWrapper();
            wrap.id = sub.id;
            wrap.name = sub.Name;
            wrap.placeId = sub.Place__c;
            wrap.placeName = sub.Place_Name__c;
            wrap.productId = sub.SBQQ__Product__c;
            wrap.productName = productName;
            wrap.status = sub.Subsctiption_Status__c;
            wrap.startDate = sub.SBQQ__StartDate__c;
            wrap.endDate = sub.End_Date__c;
            wrap.inTermination = boolean.valueOf(sub.In_Termination__c);
            wrap.terminationDate = sub.Termination_Date__c;
            wrap.total = sub.Total__c;
            wrap.subType = sub.SBQQ__SubscriptionType__c;
            wrap.SubscriptionTerm = sub.Subscription_Term__c;
            wrap.baseTermPrice = sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            if(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
                Decimal roundedDiscount = (Decimal)Math.round(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                wrap.discount = roundedDiscount;
            }
            if(String.isNotBlank(sub.SBQQ__Product__r.Base_term_Renewal_term__c)) wrap.BasetermRenewalTerm = Decimal.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c);
            wrap.showOptions = sub.SBQQ__Bundled__c || sub.Subsctiption_Status__c != ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
            wrap.contractId = sub.SBQQ__Contract__c;
            wrap.contractNumber = sub.SBQQ__Contract__r.ContractNumber;
            wrap.quantity = sub.SBQQ__Quantity__c;
            wrap.productGroup = sub.SBQQ__Product__r.Product_Group__c;
            wrap.accountId = sub.SBQQ__Contract__r.AccountId;
            wrap.customerNumber = sub.SBQQ__Contract__r.Account.Customer_Number__c;
            wrap.accountName = sub.SBQQ__Contract__r.Account.Name;
            wrap.opportunityId = sub.SBQQ__Contract__r.SBQQ__Opportunity__c;
            wrap.opportunityName = sub.SBQQ__Contract__r.SBQQ__Opportunity__r.Name;
            wrap.quoteId = sub.SBQQ__Contract__r.SBQQ__Quote__c;
            wrap.quoteNumber = sub.SBQQ__Contract__r.SBQQ__Quote__r.Name;
            if(String.isNotBlank(sub.SBQQ__Product__r.Period_of_notice__c)) wrap.periodsOfNotice = sub.SBQQ__Product__r.Period_of_notice__c;
            
            /*Decimal oneTimeFeeTotal = 0;
            if(!Test.isRunningTest() && String.isBlank(sub.SBQQ__RequiredById__c)){
                List<SBQQ__Subscription__c> childrenSubs = subHierarchy?.get(sub.Id);
                if(!childrenSubs?.isEmpty()){
                    for(SBQQ__Subscription__c childSub : childrenSubs){
                        if(childSub.SBQQ__Product__r.One_time_Fee__c) oneTimeFeeTotal += childSub.Total__c;
                    }
                }
               
                
                Decimal period = (Decimal)(Integer.valueOf(sub.Subscription_Term__c))/(Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c));
               	Decimal totalWithoutOTF = Integer.valueOf(sub.Subscription_Term__c) <= 12 ? sub.Package_Total__c - oneTimeFeeTotal : (Decimal)(sub.Package_Total__c - oneTimeFeeTotal)/period;

                wrap.bundleTotal = totalWithoutOTF + oneTimeFeeTotal;
            }
            
            if(sub.SBQQ__RequiredById__c == null) res.subscriptions.add(wrap);
            else{
                for(Subscriptionswrapper w : res.subscriptions){
                    if(w.id == sub.SBQQ__RequiredById__c){
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }
            */
            
            //the total will display the total master product value while bundle total will display the overall sum of master product and its options – not on annual base but overall
            if(sub.SBQQ__RequiredById__c == null) {
                wrap.bundleTotal = wrap.total;
               	res.subscriptions.add(wrap);
            } 
            else{
               for(Subscriptionswrapper w : res.subscriptions){
                    if(w.id == sub.SBQQ__RequiredById__c){
                        if(sub.Total__c == null) w.bundleTotal += 0 ; 
                        else w.bundleTotal += sub.Total__c;
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }
            
            
        }
        res.valid = true;
        return res.subscriptions;
    }
    
 public class SubscriptionsWrapper{
        @AuraEnabled public Id id {get; set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public Id placeId {get; set;}
        @AuraEnabled public String placeName {get; set;}
        @AuraEnabled public Id productId {get; set;}
        @AuraEnabled public String productName {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String subType {get; set;}
        @AuraEnabled public Boolean showOptions {get; set;}
        @AuraEnabled public Date startDate {get; set;}
        @AuraEnabled public Date endDate {get; set;}
        @AuraEnabled public boolean inTermination {get; set;}
        @AuraEnabled public Date terminationDate {get; set;}
        @AuraEnabled public List<SubscriptionsWrapper> childrens {get; set;}
        @AuraEnabled public Decimal discount {get; set;}
        @AuraEnabled public Decimal total {get; set;}
        @AuraEnabled public String SubscriptionTerm {get; set;}
        @AuraEnabled public Decimal BasetermRenewalTerm {get; set;}
        @AuraEnabled public Decimal baseTermPrice {get; set;}
        @AuraEnabled public Id contractId {get; set;}
        @AuraEnabled public String contractNumber {get; set;}
        @AuraEnabled public Decimal quantity {get; set;}
        @AuraEnabled public String productGroup {get; set;}
        @AuraEnabled public Id accountId {get; set;}
        @AuraEnabled public String customerNumber {get; set;}
        @AuraEnabled public String accountName {get; set;}
        @AuraEnabled public Id opportunityId {get; set;}
        @AuraEnabled public String opportunityName {get; set;}
        @AuraEnabled public Id quoteId {get; set;}
        @AuraEnabled public String quoteNumber {get; set;}
        @AuraEnabled public String periodsOfNotice {get; set;}
        @AuraEnabled public Decimal bundleTotal {get; set;}
        
        public SubscriptionsWrapper(){
            childrens = new List<SubscriptionsWrapper>();
        }
    }
    
    public class SubscriptionsResponse{
        @AuraEnabled public Boolean valid {get; set;}
        @AuraEnabled public Boolean isSizeZero {get; set;}
        @AuraEnabled public Decimal totalRevenueAmount {get;set;}
        @AuraEnabled public List<SubscriptionsWrapper> subscriptions {get; set;}
        public SubscriptionsResponse(){
            subscriptions = new List<SubscriptionsWrapper>();
            this.isSizeZero = false;
        }
    }
    
    public static  List<SBQQ__Subscription__c> generateSubs(Contract contr, List<SBQQ__QuoteLine__c> quoteLines){
       
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, SBQQ__BillingType__c='Advance',
                                                        SBQQ__ChargeType__c = 'Recurring', SBQQ__Discount__c=0.0, SBQQ__ListPrice__c=quoteLine.SBQQ__ListPrice__c, 
                                                        SBQQ__Number__c = quoteLine.SBQQ__Number__c, SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c = FALSE, 
                                                        SBQQ__Product__c = quoteLine.SBQQ__Product__c, SBQQ__Quantity__c = quoteLine.SBQQ__Quantity__c, SBQQ__QuoteLine__c = quoteLine.Id, 
                                                        Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.today(), SBQQ__SubscriptionEndDate__c=null,
                                                        SBQQ__Contract__c = contr.Id, SBQQ__Account__c = contr.AccountId, SBQQ__Bundled__c = FALSE, Status__c = 'Active', Total__c = 100.0));
        }
     
        
        return subscriptions;
       
    }
    
    
}