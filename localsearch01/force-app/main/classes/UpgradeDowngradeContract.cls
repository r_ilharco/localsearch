/**
 * Author	   : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date		   : 05-29-2019
 * Sprint      : Sprint 2
 * Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
 * 				 US_75 Customer asks for a contract amendment (downgrade) to replace a product with another one
 * Testclass   : UpgradeDowngradeContractTest
 * Package     : 
 * Description : Controller to manage the upgrade/ downgrade of contract
 * Changelog   : 
 * 				#1 - SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 07-02-2019 Fixing
 *              #2 - gcasola - System.NullPointerException Fix - 07-03-2019
 ──────────────────────────────────────────────────────────────────────────────────────────────────
* @TestClass      UpgradeDowngradeContractTest 
* @changes		  SPIII-2064 - Add a validation Check on Renewal, Amend and Upgrade button on Contract
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-29
 ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-2447 - *Temporary Solution for Pilot* - Error message for active Subs on the same Place for Upgrade
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-08-04
──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2021-02-16      SPIII-5426 - add logic to prevent user to terminate 
*				 if the subscription is already in termination
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class UpgradeDowngradeContract {
    
    @AuraEnabled
	public static String upgradeContract(String recordId){
        String result = '';
        Decimal offset;
        String priceBookId = null;
        Boolean canUpgrade = false;
        List<SBQQ__Quote__c> upgQuote = new List<SBQQ__Quote__c>();
        User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
        Set<Id> accountTeamMembers = new Set<Id>();
        Upgrade_Downgrade_Contract__mdt upgConfig = [SELECT Id,
                            DeveloperName,Closed_Date_Offset__c
                            FROM Upgrade_Downgrade_Contract__mdt
                            WHERE DeveloperName =: 'Upgrade_Setting' LIMIT 1];
        offset = upgConfig.Closed_Date_Offset__c;
            Contract cont = [SELECT  AccountId,
                             		 Account.OwnerId,
                                     EndDate, 
                                     Status,
									 SBQQ__Opportunity__r.Pricebook2Id,
                             		 SBQQ__Opportunity__r.Pricebook2.ExternalId__c,
                                     ContractNumber,
                                     SBQQ__Quote__r.SBQQ__PriceBook__c
                             FROM Contract
                             WHERE   Id = :recordId][0];
            for(AccountTeamMember member : [select id, UserId from AccountTeamMember where Accountid =: cont.AccountId ]){
                accountTeamMembers.add(member.UserId);
            }	

            if(cont.Account.OwnerId != currentUser.id 
               && currentUser.Profile.Name!= ConstantsUtil.SysAdmin
               && currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_DMC
               && !accountTeamMembers.contains(currentUser.ID)){
                   throw new AuraHandledException(Label.Upgrade_Downgrade_Amend_Validation);
               }

            if(ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(cont.Status)){
                // check if an other upgraded quote in draft status already exists 
                upgQuote = [select id from SBQQ__Quote__c where SBQQ__Type__c =: ConstantsUtil.QUOTE_TYPE_UPGRADE
                            and Upgrade_Downgrade_Contract__c =: recordId 
                            and SBQQ__Status__c =: ConstantsUtil.Quote_Status_Draft];
                if(upgQuote.size()>0){
                    return upgQuote[0].id;
                }
                priceBookId = null;
                //check if subscriptions active with PBE active also can be upgrade
               if(SRV_Contract.getSellableProductsByContract(cont,ConstantsUtil.QUOTE_TYPE_UPGRADE).size()>0){
                    upgQuote.add(getQuote(cont, Label.Upgrade_Contract_Name, TRUE, FALSE, recordId, offset, priceBookId));
                    return upgQuote[0].id;
                }
                else{
                    throw new AuraHandledException(Label.Upgrade_not_allowed);
                }
            }
            return null; 
    }
    
	public static SBQQ__Quote__c getQuote(Contract cont,  String label, Boolean Upg, Boolean Dwn, Id recordId, Decimal offset, String priceBookId)
    {
        try{
            Opportunity opp = new Opportunity();
            opp.AccountId = cont.AccountId;
            opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION;
            opp.CloseDate = Date.today()+(Integer)offset;
            opp.Name = label+ cont.ContractNumber ;
            opp.UpgradeDowngrade_Contract__c = recordId;
            opp.Upgrade__c = Upg;     
            opp.Downgrade__c = Dwn;
            opp.PriceBook2Id = priceBookId;
            insert opp;
            return [select  id,
                    		SBQQ__Opportunity2__c 
                    from    SBQQ__Quote__c 
                    where   SBQQ__Opportunity2__c =: opp.Id][0];
        }
        catch(Exception e){
            throw new AuraHandledException(e.getMessage());         
        }
    }
   
}