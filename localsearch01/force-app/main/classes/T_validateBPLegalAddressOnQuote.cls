@isTest
public class T_validateBPLegalAddressOnQuote {
    
    @isTest
    public static void Test_validateBPLegalAddressOnQuote(){
        Test.startTest();
            
        Test_AddressValidationMock addressValidationMock =  new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK);
        Test.setMock(HttpCalloutMock.class, addressValidationMock);
        
        List<Account> accounts = Test_DataFactory.generateAccounts('Account test name', 1, false);
        accounts[0].Customer_Number__c = null;
        insert accounts[0];
        
        List<Contact> contacts = Test_DataFactory.generateContactsForAccount(accounts[0].Id, 'Last name test', 1);
        
        List<Billing_Profile__c> billingProfilesList = Test_DataFactory.generateBillingProfiles(accounts[0].Id,'Test Billing Profile', null, 1);
        insert billingProfilesList;
        Opportunity oppo = Test_DataFactory.generateOpportunity('Opportunity test name', accounts[0].Id);
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(oppo.Id, accounts[0].Id, contacts[0].Id, billingProfilesList[0].Id);
       	insert quote;
        
        validateBPLegalAddressOnQuote.validateAccountAndBPonQuote(new List<ID>{quote.Id});
        
        Test.stopTest();
    }
}