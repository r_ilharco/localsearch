/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
* Service class incuding invocable methods for handling territory management for accounts
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Alessandro L. Marotta <amarotta@deloitte.it>
* @created		  10-01-2020
* @systemLayer    Utility 
* @TestClass 	  

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class SRV_TerritoryAccountManagement {

    
    @InvocableMethod
    public static void removeSales_invocable(List<Id> userIds){
     /*   List<Account> accs = new List<Account>();
        Set<Id> userIdsSet = new Set<Id>();
        for(Id uId : userIds){
            userIdsSet.add(uId);
        }
        Map<Id, User> usersToCheck = new Map<Id, User>([select id from User where id in :userIdsSet]);
        Set<User> usersToUpdate = new Set<User>();
        User singleUserToUpdate;
        List<Account> accountsToCheck = [select SalesUser__c, Assigned_Territory__c from Account
                                         where SalesUser__c in :userIdsSet and Status__c !=: ConstantsUtil.ACCOUNT_STATUS_CLOSED FOR UPDATE ];
        for(Account a : accountsToCheck){
            if(usersToCheck.containsKey(a.SalesUser__c)){
                singleUserToUpdate = usersToCheck.get(a.SalesUser__c);
                if(a.Area_Code__c.equalsIgnoreCase(singleUserToUpdate.Old_Area_Code__c)) {
                    //NOTE: Old Area Code on User gets cleared as action following this method, in the User Management process builder
                    a.SalesUser__c = null;
                    accs.add(a);
                }
            }
        }
        if(accs.size()>0){
        	update accs;
        }
        if(!usersToUpdate.isEmpty()){
            List<User> finalUsers = new List<User>();
            finalUsers.addAll(usersToUpdate);
            update finalUsers;
        }*/
    }
    
    
}