@isTest
public class TrialActivationUtil_Test implements HttpCalloutMock {
    
    public Integer type;
    
    TrialActivationUtil_Test(Integer typeHere){
        this.type=typeHere;
    }  
    
    public HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        
        if(this.type==1){           
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test", "message":""}');
            res.setStatusCode(200);
        }
        
        if(this.type==2){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test", "message":"OK"}');
            res.setStatusCode(200);
        }
        
        if(this.type==3){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test", "message":"OK"}');
            res.setStatusCode(201);
        }
        
        return res;
    }
    
    @isTest
    static void test_sendRequest(){
        try{
            Test.setMock(HttpCalloutMock.class, new TrialActivationUtil_Test(1));
            
            SBQQ__Quote__c quote = new SBQQ__Quote__c();       
            Product2 p = new Product2();
            
            SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(
                SBQQ__Quote__c=quote.Id, 
                SBQQ__Product__c=p.Id, 
                SBQQ__Quantity__c=1
            );
            
            TrialActivationUtil.AMARequestPayload objParam = new TrialActivationUtil.AMARequestPayload();       
            TrialActivationUtil.RequestElement re = new TrialActivationUtil.RequestElement(ql);
            objParam.elements.add(re);
            
            string access_token = TrialActivationUtil.getSecurityToken();
            HttpResponse resd = TrialActivationUtil.sendRequest(objParam,access_token);           
            List<SBQQ__QuoteLine__c> qlList = TrialActivationUtil.getQuoteLines(objParam);
            string qpp = TrialActivationUtil.getQuoteLinePrettyPrint('Test', ql);  
            string gj = objParam.getJson();
            string gpp = objParam.getPrettyPrint();  
            TrialActivationUtil.QuoteRelatedWrapper qrw = TrialActivationUtil.getQuoteLineItems(quote.Id);           
            SBQQ__QuoteLine__c ql2 = TrialActivationUtil_Test.getQuoteLine();
            string qIds = ql.Id + ',' + ql2.Id;
            String sta = TrialActivationUtil.saveTriaActivation(qIds);
            
            TrialActivationUtil.QuoteLineWrapper qlw = new TrialActivationUtil.QuoteLineWrapper(ql);
      
            
        }catch(Exception e){	                            
            //System.Assert(False,e.getMessage());
        }
        
        
    }
    
    @isTest
    public static void testTrialActivationType1() {
        Test.setMock(HttpCalloutMock.class, new TrialActivationUtil_Test(1));
        try{
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            TrialActivationManager.sendTrialRequest(listQLines);
            
            
            
        }catch(Exception e){	                            
            //System.Assert(False,e.getMessage());
        }
    }
    
    @isTest
    public static void testTrialActivationType2() {
        Test.setMock(HttpCalloutMock.class, new TrialActivationUtil_Test(2));       
        try{
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            TrialActivationManager.sendTrialRequest(listQLines);
        }catch(Exception e){	                            
            //System.Assert(False,e.getMessage());
        }
    }
    
    @isTest
    public static void testTrialActivationType3() {
        Test.setMock(HttpCalloutMock.class, new TrialActivationUtil_Test(3));
        try{
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            TrialActivationManager.sendTrialRequest(listQLines);
        }catch(Exception e){	                            
            //System.Assert(False,e.getMessage());
        }
    }
    
    
    public static SBQQ__QuoteLine__c getQuoteLine(){
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        Product2 p = new Product2();
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(
            SBQQ__Quote__c=quote.Id, 
            SBQQ__Product__c=p.Id, 
            SBQQ__Quantity__c =1, 
            Status__c=ConstantsUtil.QUOTELINE_STATUS_TRIAL_REQUESTED,
            TrialStartDate__c = Date.today()
        );
        
        return quoteLine;
    }
    
    
    @isTest 
    static void testQuotePrint(){
        try{
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh'; 
            acc.BillingCity = 'test city';
            acc.BillingCountry = 'test country';
            acc.BillingPostalCode = '123';
            acc.BillingState = 'test state';
            acc.PreferredLanguage__c = 'German';
            
            insert acc;
            
            Place__c place = new Place__c();
            place.Account__c = acc.Id;
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            
            insert place;
            
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            // opp.Pricebook2Id = pBook.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
            
            insert opp;
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            
            Billing_Profile__c billProfile = new Billing_Profile__c();       	
            billProfile.Customer__c	= acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c	= 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c	= 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';	
            billProfile.Channels__c	= 'test';
            billProfile.Name = 'Test Bill Prof Name';
            
            insert billProfile;
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__PrimaryContact__c = myContact.Id;
            quot.Billing_Profile__c = billProfile.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            quot.SBQQ__Primary__c = true;
            
            insert quot;
            //System.Debug('Quote '+ quot);
            
            
            Product2 prod1 = new Product2();
            prod1.Name = 'test product1';
            prod1.ProductCode = 'test1';
            prod1.IsActive=True;
            prod1.SBQQ__ConfigurationType__c='Allowed';
            prod1.SBQQ__ConfigurationEvent__c='Always';
            prod1.SBQQ__QuantityEditable__c=True;       
            prod1.SBQQ__NonDiscountable__c=False;
            //prod.SBQQ__SubscriptionType__c='Renewable'; 
            prod1.SBQQ__SubscriptionPricing__c='Fixed Price';
            prod1.SBQQ__SubscriptionTerm__c=12; 
            
            insert prod1;
            
            
            Product2 prod = new Product2();
            prod.Name = 'test product1';
            prod.ProductCode = 'test1';
            prod.IsActive=True;
            prod.SBQQ__ConfigurationType__c='Allowed';
            prod.SBQQ__ConfigurationEvent__c='Always';
            prod.SBQQ__QuantityEditable__c=True;       
            prod.SBQQ__NonDiscountable__c=False;
            //prod.SBQQ__SubscriptionType__c='Renewable'; 
            prod.SBQQ__SubscriptionPricing__c='Fixed Price';
            prod.SBQQ__SubscriptionTerm__c=12; 
            
            insert prod;
            
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            
            SBQQ__QuoteLine__c qline2 = new   SBQQ__QuoteLine__c();
            
            qline2.SBQQ__Quote__c = quot.Id;
            qline2.SBQQ__Product__c = prod1.Id;
            qline2.Place__c = place.Id;
            qline2.SBQQ__Quantity__c = 1;
            //qline2.TrialStartDate__c= date.today();
            qline2.TrialStatus__c= 'Trial';
            
            
            insert qline2;	
            listQLines.add(qline2);
            
            SBQQ__QuoteLine__c qline1 = new   SBQQ__QuoteLine__c();
            
            qline1.SBQQ__Quote__c = quot.Id;
            qline1.SBQQ__Product__c = prod.Id;
            qline1.Place__c = place.Id;
            qline1.SBQQ__Quantity__c = 1;
            qline1.TrialStartDate__c= date.today();
            qline1.TrialEndDate__c=date.today().AddDays(30);
            qline1.TrialStatus__c= 'Trial';
            
            //insert qline1;	
            listQLines.add(qline1);
            
            Id qqid= qline2.Id;
            List<Id> ii = new List<Id>();
            ii.add(qqid);
            
            //update qline2;
            //
            Test.setMock(HttpCalloutMock.class, new TrialActivationUtil_Test(2));
            test.startTest();
            TrialActivationUtil.getQuoteLinePrettyPrint('Okay', qline2);
            TrialActivationUtil.saveTriaActivation(qline2.Id);
            
           
            TrialActivationUtil.QuoteLineWrapper qq = new TrialActivationUtil.QuoteLineWrapper(qline1);
            
            TrialActivationUtil.saveTriaActivationBatch(ii);
            //TrialActivationUtil.TriaActivationQueueJob(listQLines);
            
            TrialActivationUtil.QuoteRelatedWrapper qrw =  new TrialActivationUtil.QuoteRelatedWrapper();
            TrialActivationUtil.getQuoteLineItems(qqid);
           

            test.stopTest();
            
            
        }
        catch(Exception e)
        {
            
            system.assert(false, e.getMessage());
        }
    }
    
    
    
}