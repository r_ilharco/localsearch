/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Account Team Member Trigger Helper
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-08-10
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class AccountTeamMemberTriggerHelper {
	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Only SDI of the Account (Account and SDI must be in the same territory) can delete a Team Member
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    oldItems    Trigger.Old context variable
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void checkPermissions(Map<Id, AccountTeamMember> oldItems){ 
        
        List<AccountTeamMember> teamMemberToAddError = new List<AccountTeamMember>();
        List<User> userRecord = [SELECT Id, Code__c, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        if(!userRecord.isEmpty()){
            
            Boolean isDMC = ConstantsUtil.DMC_CODE_USERCONFIGURATION_MDT.equalsIgnoreCase(userRecord[0].Code__c);
            Boolean isAdmin = ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(userRecord[0].Profile.Name);
            
            if(isDMC) cannotError(oldItems.values());
            else if(!isAdmin){
                
                Map<Id, List<AccountTeamMember>> accountToTeamMembers = new Map<Id, List<AccountTeamMember>>();
                
                for(AccountTeamMember currentMember : oldItems.values()){
                    if(!accountToTeamMembers.containsKey(currentMember.AccountId)) accountToTeamMembers.put(currentMember.AccountId, new List<AccountTeamMember>{currentMember});
                    else accountToTeamMembers.get(currentMember.AccountId).add(currentMember);
                }
                
                Set<Id> accountsInDifferentTerritory = accountsInDifferentTerritory(accountToTeamMembers, userRecord[0]);
                if(!accountsInDifferentTerritory.isEmpty()){
                    for(Id accountId : accountsInDifferentTerritory){
                        teamMemberToAddError.addAll(accountToTeamMembers.get(accountId));
                    }
                }
                if(!teamMemberToAddError.isEmpty()) cannotError(teamMemberToAddError);
            }
        }
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Add Error on delete of a Team Member if the user does not have permissions.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    teamMembersBeingDeleted    List of deleted Account Team Member to add error
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void cannotError(List<AccountTeamMember> teamMembersBeingDeleted){
        for(AccountTeamMember currentMember : teamMembersBeingDeleted){
            if(!Test.isRunningTest()){
                if(Trigger.isDelete)
                	currentMember.addError(Label.NoPermission_AccountTeamMemberDelete);
               	else if(Trigger.isUpdate)
                    currentMember.addError(Label.NoPermission_AccountTeamMemberUpdate);
                else if(Trigger.isInsert)
                    currentMember.addError(Label.NoPermission_AccountTeamMemberInsert);
			}
        }
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * If SMA/SMD/SRD are not in the same territory of the Account, they cannot delete Team Members.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input 	param    	accountToTeamMembers    	Account to deleted team members map
    * @input 	param    	userDeletingRecord    		Current User record
    * @return 	Set<Id>									Account ids for which the current user is not
    * 													in the same territory
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Set<Id> accountsInDifferentTerritory(Map<Id, List<AccountTeamMember>> accountToTeamMembers, User userDeletingRecord){
        
        Set<Id> accountIds = new Set<Id>();
        
        List<ObjectTerritory2Association> accountTerritories = [SELECT Id, Territory2Id, Territory2.ParentTerritory2Id, ObjectId FROM ObjectTerritory2Association WHERE ObjectId IN :accountToTeamMembers.keySet()];        
        List<UserTerritory2Association> userTerritories =  [SELECT Id, Territory2Id, Territory2.ParentTerritory2Id FROM UserTerritory2Association WHERE UserId = :userDeletingRecord.Id LIMIT 1];
        
        if(!userTerritories.isEmpty() && !accountTerritories.isEmpty()){
            
            String userTerritory = userTerritories[0].Territory2Id;
            String userParentTerritory = userTerritories[0].Territory2.ParentTerritory2Id;
            
            for(ObjectTerritory2Association currentAccountTerritory : accountTerritories){
                
                String accountTerritory = currentAccountTerritory.Territory2Id;
                String accountParentTerritory = currentAccountTerritory.Territory2.ParentTerritory2Id;
                
                Boolean isSameTerritory = false;
                
                if(String.isNotBlank(userParentTerritory) && String.isNotBlank(accountParentTerritory)){
                    if(userParentTerritory.equals(accountParentTerritory)) isSameTerritory = true;
                }
                else if(String.isNotBlank(userParentTerritory) && String.isNotBlank(accountTerritory)){
                    if(userParentTerritory.equals(accountTerritory)) isSameTerritory = true;
                }
                else if(String.isNotBlank(userTerritory) && String.isNotBlank(accountParentTerritory)){
                    if(userTerritory.equals(accountParentTerritory)) isSameTerritory = true;
                }
                else if(String.isNotBlank(userTerritory) && String.isNotBlank(accountTerritory)){
                    if(userTerritory.equals(accountTerritory)) isSameTerritory = true;
                }

                if(!isSameTerritory){
                    accountIds.add(currentAccountTerritory.ObjectId);
                }
			}
            
        }
        
        return accountIds;
    }

}