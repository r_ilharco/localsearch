@isTest
public class QuoteDocumentSendNotificationTest {
    
    @Testsetup  
    static void setup(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
       
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generateFatherProduct());


        insert productsToInsert;
        
        
        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Billing_Profile__c billingProfile = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :account.Id LIMIT 1];

        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        insert opportunity;
        
        SBQQ__Quote__c quote = [SELECT Id,Replacement__c,SBQQ__Opportunity2__c,SBQQ__Account__c FROM SBQQ__Quote__c Where SBQQ__Opportunity2__c = :opportunity.Id LIMIT 1];
       	
        
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;        
        
        QuoteTriggerHandler.disableTrigger = false;
        SBQQ__QuoteDocument__c quoteDocument1 = Test_DataFactory.generateQuoteDocument(quote.Id);
        insert quoteDocument1;
        
        QuoteTriggerHandler.disableTrigger = true;

        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;       
    }
    
    @isTest
    public static void SendNotificationTest()
    {                
			Map<id, SBQQ__QuoteDocument__c> lst = new Map<id, SBQQ__QuoteDocument__c>([select id from SBQQ__QuoteDocument__c]);
            Test.startTest();  
           
            QuoteDocumentSendNotification.processQuoteDocument(new List<id>(lst.keySet()));

            Test.stopTest(); 
    }
    
        public static Product2 generateFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        
        product.Create_automatic_renewal_opportunity__c = true;
        product.Opportunity_Start_Date_DMC__c = 365;
        product.Opportunity_Expiration_Date_DMC__c = 91;
        product.Opportunity_Expiration_Date_Telesales__c = 0;
        
        return product;        
    }
}