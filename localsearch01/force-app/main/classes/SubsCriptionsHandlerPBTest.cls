@isTest
public class SubscriptionsHandlerPBTest {
    
    @isTest
    static void testIsSubsCriptionsTerminated(){
        
        try{        
            User u = new user();
            u.LastName = 'Test Code';
            u.Email = 'iqbal.rocky@arollotech.com';
            u.Alias = 'Tcode';
            u.Username = 'iqbal.roc@arollo.com';
            u.CommunityNickname = 'test12';
            u.LocaleSidKey = 'en_US';
            u.TimeZoneSidKey = 'GMT';
            u.ProfileID = '00e1r0000027zBpAAI';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
            
            insert u;
            system.debug('User' +u);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
            
            insert acc;
            
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            
            insert place;
            System.debug('Place: '+ place);
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
            
            insert opp;
            System.Debug('opp '+ opp); 
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
            
            Billing_Profile__c billProfile = new Billing_Profile__c();
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'test';   
            billProfile.Name = 'Test Bill Prof Name';
            insert billProfile;
            
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            quot.Billing_Profile__c= billProfile.id;
            
            insert quot;
            System.Debug('Quote '+ quot);          
            
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Opportunity__c= opp.Id;
            myContract.SBQQ__Quote__c= quot.Id;
            myContract.OwnerId= u.Id;
            
            
            insert myContract;
            System.debug('Contract: '+ myContract); 
            
            // myContract.Status='Active';
            // update myContract;
            SBQQ__Subscription__c subs = new SBQQ__Subscription__c();
            subs.SBQQ__Account__c = acc.Id;
            subs.Place__c =place.Id;
            subs.SBQQ__Contract__c = myContract.Id;
            subs.SBQQ__Quantity__c = 2;
            subs.Status__c = 'Draft';
            subs.Termination_Generate_Credit_Note__c = true;
            
            test.startTest();
            insert subs;
            subs.Status__c='Terminated';
            
            update subs;
            System.debug('Subs: '+ subs); 
            
            SBQQ__Subscription__c subs1 = new SBQQ__Subscription__c();
            subs1.SBQQ__Account__c = acc.Id;
            //subs1.Place__c =place.Id;
            subs1.SBQQ__Contract__c = myContract.Id;
            subs1.SBQQ__Quantity__c = 2;
            subs1.Status__c = 'Draft';
            //subs.Termination_Generate_Credit_Note__c = true;
            
            insert subs1;
            subs1.Status__c='Failed';
            
            update subs1;
            System.debug('Subs: '+ subs1); 
            
            SBQQ__Subscription__c subs2 = new SBQQ__Subscription__c();
            subs2.SBQQ__Account__c = acc.Id;
            subs2.Place__c =place.Id;
            subs2.SBQQ__Contract__c = myContract.Id;
            subs2.SBQQ__Quantity__c = 2;
            subs2.Status__c = 'Draft';
            subs2.Termination_Generate_Credit_Note__c = true;
            
            insert subs2;
            subs2.Status__c='Production';
            
            update subs2;
            System.debug('Subs: '+ subs2); 
            test.stopTest();
        }
        catch(Exception e){
            system.assert(false, e.getMessage());
        }
    }
}