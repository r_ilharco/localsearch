/**
 * Author      : Gennaro Casola <gcasola@deloitte.it>
 * Date        : 04-10-2019
 * Sprint      : 1
 * Work item   : 25
 * Testclass   : Test_SBQQQuoteTrigger
 * Package     : 
 * Description : Trigger Handler on SBQQ__Quote__c
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-1883 - Digital Signature Expiration Management - Revoke "On-Going" Quote Documents on Quote Rejection
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-07-20
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Gennaro Casola
* 2020-08-12      SPIII-1599 - Process of Substitution
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class QuoteTriggerHandler implements ITriggerHandler{
    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        
        Map<String, List<SBQQ__Quote__c>> quoteMapByType = SRV_Quote.constructQuoteMapByType((List<SBQQ__Quote__c >) newItems);
        
        ATL_QuoteTriggerHelper.setDefaultRelations((List<SBQQ__Quote__c >) newItems,null);
        
        if(quoteMapByType.containsKey(ConstantsUtil.QUOTE_TYPE_RENEWAL)){
            ATL_QuoteTriggerHelper.setRenewalQuoteFields(quoteMapByType.get(ConstantsUtil.QUOTE_TYPE_RENEWAL));
        }
      	if(FeatureManagement.checkPermission(ConstantsUtil.PERMISSION_Renovero_Migration)){
            setRenoverosIds((List<SBQQ__Quote__c>)newItems);
        }
        /*if(quoteMapByType.containsKey(ConstantsUtil.QUOTE_TYPE_DOWNGRADE)){
            ATL_QuoteTriggerHelper.quoteStartDateCalculation(quoteMapByType.get(ConstantsUtil.QUOTE_TYPE_DOWNGRADE));
        }*/
        //ATL_QuoteTriggerHelper.updateOpp((List<SBQQ__Quote__c >) newItems, new Map<Id,SBQQ__Quote__c >()); 
        //Luca Proietti Nicolai SF2-464 -  Discount Rules Enhancement Dev
        //ATL_QuoteTriggerHelper.associateApproversForDiscount((List<SBQQ__Quote__c >) newItems);   
    }
    
    public void setRenoverosIds(List<SBQQ__Quote__c>newItems){
      
            set<Id> oppIds = new set<Id>();
        	
            for(SBQQ__Quote__c qt : newItems){
                oppIds.add(qt.SBQQ__Opportunity2__c); 
            }
            
            Map<ID, Opportunity > allOpportunity = new Map<ID, Opportunity>([Select Id,ExternalOPTY__c  From Opportunity where Id In :oppIds]);
            
            for(SBQQ__Quote__c qts : newItems){
                if(allOpportunity.containsKey(qts.SBQQ__Opportunity2__c)){
                     qts.Renovero_ID__c = allOpportunity.get(qts.SBQQ__Opportunity2__c).ExternalOPTY__c; 
					qts.ContractSAMBA_Key__c = allOpportunity.get(qts.SBQQ__Opportunity2__c).ExternalOPTY__c; 
                }
            
            }
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        Map<Id, SBQQ__Quote__c> quoteItems = (Map<Id, SBQQ__Quote__c>) newItems;
        Map<Id, SBQQ__Quote__c> oldQuoteItems = (Map<Id, SBQQ__Quote__c>) oldItems;
        Set<Id> quoteIds = new Set<Id>();
        Set<Id> errorIds = new Set<Id>();
        
        for(SBQQ__Quote__c aQuote : quoteItems.values()){
            if((ConstantsUtil.Quote_Status_Accepted).equalsIgnoreCase(aQuote.SBQQ__Status__c) 
               && !(oldQuoteItems.get(aQuote.Id).SBQQ__Status__c).equalsIgnoreCase(aQuote.SBQQ__Status__c)){
                quoteIds.add(aQuote.Id);
            }
        }
        if(!quoteIds.isEmpty()){
            errorIds = checkProductValidationOnAcceptance(quoteIds);
            for(Id qId : errorIds){
                if(quoteItems.containsKey(qId)){
                    quoteItems.get(qId).addError(Label.Quote_Accepted_Localina_Error);
                }
            }
        }
        ATL_QuoteTriggerHelper.checkSubscripionsPriorToReplacemnt((Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);        
        ATL_QuoteTriggerHelper.upgradeValidation((Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.quoteValidation((Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.SetToUpdateQuote((List<SBQQ__Quote__c>) newItems.values(), (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.checkQuantityOfVipPlace((List<SBQQ__Quote__c>) newItems.values(), (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.setDefaultRelations((List<SBQQ__Quote__c >) newItems.values(),  (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.placeValidityCheck((Map<Id, SBQQ__Quote__c>) oldItems, (Map<Id, SBQQ__Quote__c>) newItems);
		ATL_QuoteTriggerHelper.printProductTaskValidation((Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        //Vincenzo Laudato - Trial Deactivation
        ATL_QuoteTriggerHelper.deactivateTrial((List<SBQQ__Quote__c >) newItems.values(), (Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        
        //Luca Proietti Nicolai SF2-464 -  Discount Rules Enhancement Dev
        //ATL_QuoteTriggerHelper.checkDiscounts((Map<Id, SBQQ__Quote__c>) oldItems, (Map<Id, SBQQ__Quote__c>) newItems);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        ATL_QuoteTriggerHelper.quoteValidationForDeleting();
        //SF2-533 - Quote document + SF2-631 - Quote Template T&C - gcasola - 11-26-2019
        //ATL_QuoteTriggerHelper.deleteRelatedContents(oldItems);
    }
    public void AfterInsert(Map<Id, SObject> newItems) {
        ATL_QuoteTriggerHelper.ownerRecalculation((Map<Id, SBQQ__Quote__c>)newItems);
        ATL_QuoteTriggerHelper.setPrimaryQuoteOnRenewalOpty((List<SBQQ__Quote__c>)newItems.values());
        //ATL_QuoteTriggerHelper.updateOpp((List<SBQQ__Quote__c >) newItems.values(), new Map<Id,SBQQ__Quote__c >());
        //ATL_QuoteTriggerHelper.attachRelatedContents(newItems, null);
        //SharingUtility.shareQuotesManually((Map<Id, SBQQ__Quote__c>)newItems, null, 'Edit');
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        ATL_QuoteTriggerHelper.setContactOnQuoteLine((Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        ATL_QuoteTriggerHelper.deleteAllocation((List<SBQQ__Quote__c >) newItems.values(), (Map<Id, SBQQ__Quote__c>) newItems, (Map<Id, SBQQ__Quote__c>) oldItems);
        if(!ATL_QuoteTriggerHelper.staticQuoteLinesToUpdate.isEmpty()){
            try{
                update ATL_QuoteTriggerHelper.staticQuoteLinesToUpdate;
            }catch (Exception e) {
                System.debug('Exception in ATL_QuoteTriggerHelper.staticQuoteLinesToUpdate '+e.getMessage());
            }
        }
        ATL_QuoteTriggerHelper.setQuoteDocumentsInRevoked(newItems,oldItems);
        ATL_QuoteTriggerHelper.resetReplacedByProductOnLinkedSubscriptions(newItems,oldItems);
        //Luca Proietti Nicolai SF2-464 -  Discount Rules Enhancement Dev
        //ATL_QuoteTriggerHelper.nextApproverForDiscount((Map<Id, SBQQ__Quote__c>) oldItems, (Map<Id, SBQQ__Quote__c>) newItems);
        //SharingUtility.shareQuotesManually((Map<Id, SBQQ__Quote__c>)newItems, (Map<Id, SBQQ__Quote__c>) oldItems, 'Edit');
    }
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    private Set<Id> checkProductValidationOnAcceptance(Set<Id> quoteIds){
        
        Map<Id, SBQQ__QuoteLine__c> quoteLineMap = new Map<Id, SBQQ__QuoteLine__c>();
        quoteLineMap = QuoteLineSelector.getMapRecordsByQuoteIds(quoteIds);
        Set<Id> quoteToQL = new Set<Id>();
        
        for(SBQQ__QuoteLine__c aQuoteLine : quoteLineMap.values()){
            if((aQuoteLine.SBQQ__Product__r.ProductCode).contains('LOCALINA') &&
               (String.isBlank(aQuoteLine.SBQQ__Quote__r.SBQQ__Account__r.Phone) 
               || String.isBlank(aQuoteLine.SBQQ__Quote__r.SBQQ__Account__r.Email__c))){
                    quoteToQL.add(aQuoteLine.SBQQ__Quote__c);
            }
        }
        return quoteToQL;
    }
}