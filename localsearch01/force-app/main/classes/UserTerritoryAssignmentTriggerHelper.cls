/**
    * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
    * 
    *   The Auxilary Class for the UserTerritoryAssignmentTriggerHandler  
    *
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    * @author         Mazzarella Mara <mamazzarella@deloitte.it>
    * @created		  2019-10-06
    * @systemLayer    Trigger
    * @TestClass 	  UserTerritoryAssignmentTriggerTest
    
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    *				  
    * @changes
    * @modifiedby      Mazzarella Mara
    * 2019-10-06      Sprint 3 / W2-Territory Management - Check if user are already assigned to other territory
    *
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    *				  
    * @changes
    * @modifiedby      Gennaro Casola <gcasola@deloitte.it>
    * 2020-06-12      SFRO-389
    *
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    *				  
    * @changes
    * @modifiedby      Gennaro Casola <gcasola@deloitte.it>
    * 2020-06-24      [SPIII-1147] TERRITORY MODEL STRUCTURE WITH TWO LEVELS (REGION AND TERRITORIES)
    *	
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    *				  
    * @changes
    * @modifiedby      Gennaro Casola <gcasola@deloitte.it>
    * 2020-06-30      [SPIII-1149] TERRITORY DMC ASSOCIATION
    *				  
    * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    
    public class UserTerritoryAssignmentTriggerHelper {
        
        public static String processes = '>>>';
        
         /**
          * ───────────────────────────────────────────────────────────────────────────────────────────────┐
          * It updates the current assigned territory on the User Record.
          * ────────────────────────────────────────────────────────────────────────────────────────────────
          * Gennaro Casola <gcasola@deloitte.it>
          * 2020-06-24      [SPIII-1147] TERRITORY MODEL STRUCTURE WITH TWO LEVELS (REGION AND TERRITORIES)
          *  
          * ───────────────────────────────────────────────────────────────────────────────────────────────┘
          */
        
        public static void manageAssignedTerritoryOnUsers(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
            List<User> usersToUpdate = new List<User>();
            
            if(Trigger.operationType == System.TriggerOperation.AFTER_INSERT || Trigger.operationType == System.TriggerOperation.AFTER_UPDATE){
                Set<Id> territoryIds = new Set<Id>();
                Map<Id,Territory2> territoriesMap;
                
                for(UserTerritory2Association ut2a : ((Map<Id, UserTerritory2Association>)newItems).values())
                    territoryIds.add(ut2a.Territory2Id);
                
                territoriesMap = new Map<Id,Territory2>([SELECT Id, Name, DeveloperName FROM Territory2 WHERE Id IN :territoryIds 
                                                         AND Territory2.Territory2Model.State = 'Active']);
                
                for(UserTerritory2Association ut2a : ((Map<Id, UserTerritory2Association>)newItems).values())
                        usersToUpdate.add(new User(Id = ut2a.UserId, AssignedTerritory__c = territoriesMap.get(ut2a.Territory2Id).Name));
                
            }else if(Trigger.operationType == System.TriggerOperation.AFTER_DELETE){
                for(UserTerritory2Association ut2a : ((Map<Id, UserTerritory2Association>)oldItems).values())
                        usersToUpdate.add(new User(Id = ut2a.UserId, AssignedTerritory__c = NULL));
            }
            System.debug('usersToUpdate '+usersToUpdate);        
            if(usersToUpdate != NULL && usersToUpdate.size() > 0){
                UserTriggerHandler.disableTrigger = true;
                    update usersToUpdate;
                UserTriggerHandler.disableTrigger = false;
            }
        }
    
        /**
          * ───────────────────────────────────────────────────────────────────────────────────────────────┐
          * The DeletedUserAssociations__c is used to count the deleted associations for a certain Territory 
          * and It serves for the Batch that updates the Users(SRD, SMA, SMD) on the Accounts.
          * ────────────────────────────────────────────────────────────────────────────────────────────────
          * Gennaro Casola <gcasola@deloitte.it>
          * 2020-06-12      SFRO-389
          *  
          * ───────────────────────────────────────────────────────────────────────────────────────────────┘
          */
    
        public static void manageDeletedUserAssociations(Map<Id, SObject> oldItems){
            Map<Id,Territory2> territoriesToUpdateMap = new Map<Id,Territory2>();
            Set<Id> territoryIdsToUpdate = new Set<Id>();
            Set<String> rolesToBlankOnAccounts = new Set<String>{ConstantsUtil.ROLE_IN_TERRITORY_SDI, ConstantsUtil.ROLE_IN_TERRITORY_SMA, ConstantsUtil.ROLE_IN_TERRITORY_SMD};
            Map<Id, UserTerritory2Association> oldUT2AMap = (Map<Id, UserTerritory2Association>) oldItems;
            
            for(UserTerritory2Association ut2a : oldUT2AMap.values())
                if(rolesToBlankOnAccounts.contains(ut2a.RoleInTerritory2))
                	territoryIdsToUpdate.add(ut2a.Territory2Id);
            
            if(territoryIdsToUpdate != NULL && territoryIdsToUpdate.size() > 0){
                territoriesToUpdateMap = new Map<Id,Territory2>([SELECT Id, DeletedUserAssociations__c FROM Territory2 WHERE Id IN :territoryIdsToUpdate]);
            
                if(territoriesToUpdateMap != NULL && territoriesToUpdateMap.size() > 0){
                    for(Territory2 t : territoriesToUpdateMap.values()){
                        if(t.DeletedUserAssociations__c == NULL)
                            t.DeletedUserAssociations__c = 0;
                        
                        t.DeletedUserAssociations__c += 1;
                    }
                }
                
                update territoriesToUpdateMap.values();
            }
        }
    
        /**
          * ───────────────────────────────────────────────────────────────────────────────────────────────┐
          * It verifies that the Users are not being assigned to more than one Territory.
            (0,1) SRD for each Territory
            (0,1) SMA for each Territory
            (0,1) SMD for each Territory
            (0,N) DMC for each Territory
          * ────────────────────────────────────────────────────────────────────────────────────────────────
          * Gennaro Casola <gcasola@deloitte.it>
          * 2020-06-30      [SPIII-1149] TERRITORY DMC ASSOCIATION
          * ───────────────────────────────────────────────────────────────────────────────────────────────┘
          */
        
        public static void checkUniqueTerritoryforUser(List<UserTerritory2Association> listNew){
            Map<String, Integer> limitsByRolePerTerritoryMap = new Map<String, Integer>{
                ConstantsUtil.ROLE_IN_TERRITORY_SDI => ((Label.UserTerritory2Association_Limit_Territory_SRD.isNumeric())?Integer.valueOf(Label.UserTerritory2Association_Limit_Territory_SRD):1),
                ConstantsUtil.ROLE_IN_TERRITORY_SMA => ((Label.UserTerritory2Association_Limit_Territory_SMA.isNumeric())?Integer.valueOf(Label.UserTerritory2Association_Limit_Territory_SMA):1),
                ConstantsUtil.ROLE_IN_TERRITORY_SMD => ((Label.UserTerritory2Association_Limit_Territory_SMD.isNumeric())?Integer.valueOf(Label.UserTerritory2Association_Limit_Territory_SMD):1)            
            };
            Map<Id, Set<Id>> territoryIdsByUserIdMap = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> ut2aByUserIdMap = new Map<Id, Set<Id>>();
            Map<Id, Map<String, Set<Id>>> ut2aByTerritoryAndRoleMap = new Map<Id, Map<String, Set<Id>>>();
            Set<Id> userIds = new Set<Id>();
            Set<Id> territoryIds = new Set<Id>();
            Set<UserTerritory2Association> ut2aInErrorSet = new Set<UserTerritory2Association>();
            
            Map<Id, Set<UserTerritory2Association>> ut2aInScopeByUserIdMap = new Map<Id, Set<UserTerritory2Association>>();
            Map<Id, Map<String, Set<UserTerritory2Association>>> ut2aInScopeByTerritoryAndRoleMap = new Map<Id, Map<String, Set<UserTerritory2Association>>>();
            
            for(UserTerritory2Association ut2a : listNew){
                userIds.add(ut2a.UserId);
                territoryIds.add(ut2a.Territory2Id);
                
                if(!ut2aInScopeByUserIdMap.containsKey(ut2a.UserId))
                    ut2aInScopeByUserIdMap.put(ut2a.UserId, new Set<UserTerritory2Association>());
                
                 ut2aInScopeByUserIdMap.get(ut2a.UserId).add(ut2a);
                
                if(!ut2aInScopeByTerritoryAndRoleMap.containsKey(ut2a.Territory2Id))
                    ut2aInScopeByTerritoryAndRoleMap.put(ut2a.Territory2Id, new Map<String, Set<UserTerritory2Association>>());
                
                if(!ut2aInScopeByTerritoryAndRoleMap.get(ut2a.Territory2Id).containsKey(ut2a.RoleInTerritory2))
                    ut2aInScopeByTerritoryAndRoleMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2, new Set<UserTerritory2Association>());
    
                ut2aInScopeByTerritoryAndRoleMap.get(ut2a.Territory2Id).get(ut2a.RoleInTerritory2).add(ut2a);
            }
            
            if(ut2aInScopeByUserIdMap != NULL && ut2aInScopeByUserIdMap.size() > 0){
                for(Id userId : ut2aInScopeByUserIdMap.keySet()){
                    if(ut2aInScopeByUserIdMap.get(userId).size() > 1){
                        for(UserTerritory2Association ut2a : ut2aInScopeByUserIdMap.get(userId)){
                            ut2a.addError(Label.ut2a_errorMessage_NoMultipleAssociation);
                            ut2aInErrorSet.add(ut2a);
                        }
                    } 
                }
            }
            
            if(ut2aInScopeByTerritoryAndRoleMap != NULL && ut2aInScopeByTerritoryAndRoleMap.size() > 0){
                for(Id Territory2Id : ut2aInScopeByTerritoryAndRoleMap.keySet()){
                    if(ut2aInScopeByTerritoryAndRoleMap.get(Territory2Id).size() > 0){
                        for(String roleInTerritory2 : ut2aInScopeByTerritoryAndRoleMap.get(Territory2Id).keySet()){
                            if(limitsByRolePerTerritoryMap.containsKey(roleInTerritory2) && 
                               ut2aInScopeByTerritoryAndRoleMap.get(Territory2Id).get(roleInTerritory2).size() > limitsByRolePerTerritoryMap.get(roleInTerritory2)){
                                   for(UserTerritory2Association ut2a : ut2aInScopeByTerritoryAndRoleMap.get(Territory2Id).get(roleInTerritory2)){
                                       Integer limitRole = limitsByRolePerTerritoryMap.get(roleInTerritory2);
                                       ut2a.addError(String.format(Label.ut2a_errorMessage_AssociationsLimitByRoleReached, new List<Object>{limitRole, roleInTerritory2}));
                                       ut2aInErrorSet.add(ut2a);
                                   }
                               }
                        }
                    }
                }
            }
            
            Map<Id, UserTerritory2Association> ut2aMap = new Map<Id, UserTerritory2Association>([SELECT Id, UserId, User.Name, Territory2Id, Territory2.Name, IsActive, RoleInTerritory2 FROM UserTerritory2Association WHERE IsActive = true AND Territory2.Territory2Model.State = 'Active' AND (UserId IN :userIds OR Territory2Id IN :territoryIds)]);
            
            for(UserTerritory2Association ut2a : ut2aMap.values()){
                if(!ut2aByUserIdMap.containsKey(ut2a.UserId))
                    ut2aByUserIdMap.put(ut2a.UserId, new Set<Id>());
                
                ut2aByUserIdMap.get(ut2a.UserId).add(ut2a.Id);
                
                if(!ut2aByTerritoryAndRoleMap.containsKey(ut2a.Territory2Id))
                    ut2aByTerritoryAndRoleMap.put(ut2a.Territory2Id, new Map<String, Set<Id>>());
                
                if(!ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).containsKey(ut2a.RoleInTerritory2))
                    ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2, new Set<Id>());
                
                ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).get(ut2a.RoleInTerritory2).add(ut2a.Id);
            }
            
            for(UserTerritory2Association ut2a : listNew){
                if(!ut2aInErrorSet.contains(ut2a)){
                    if(ut2aByUserIdMap.containsKey(ut2a.UserId)){
                        Set<Id> ut2aSet = ut2aByUserIdMap.get(ut2a.UserId).clone();
                        
                        if(Trigger.operationType == System.TriggerOperation.BEFORE_UPDATE)
                            ut2aSet.remove(ut2a.Id);
                        
                        if(ut2aSet.size() > 0){
                            String errorMessage = ut2aMap.get(new List<Id>(ut2aSet).get(0)).User.Name + ' ' + Label.ut2a_errorMessage_UserAlreadyAssignedToTerritoryModel + ' ';
                            Boolean first = true;
                            for(Id ut2aId : ut2aByUserIdMap.get(ut2a.UserId)){
                                if(!first) errorMessage += ', ';
                                errorMessage += ut2aMap.get(ut2aId).Territory2.Name + ' (' +  ut2aMap.get(ut2aId).RoleInTerritory2 + ')';
                                if(first) first = false;
                            }
                            ut2a.addError(errorMessage);
                        }
                    }
                    
                    if(limitsByRolePerTerritoryMap.containsKey(ut2a.RoleInTerritory2) 
                       && ut2aByTerritoryAndRoleMap.containsKey(ut2a.Territory2Id) 
                       && ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).containsKey(ut2a.RoleInTerritory2)){
                           Set<Id> ut2aSet = ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).get(ut2a.RoleInTerritory2).clone();
                           
                           if(Trigger.operationType == System.TriggerOperation.BEFORE_UPDATE)
                               ut2aSet.remove(ut2a.Id);
                           
                           if((ut2aSet.size() + ut2aInScopeByTerritoryAndRoleMap.get(ut2a.Territory2Id).get(ut2a.roleInTerritory2).size()) > limitsByRolePerTerritoryMap.get(ut2a.RoleInTerritory2)){
                               String territoryName = ut2aMap.get(new List<Id>(ut2aByTerritoryAndRoleMap.get(ut2a.Territory2Id).get(ut2a.RoleInTerritory2)).get(0)).Territory2.Name;
                               ut2a.addError(String.format(Label.ut2a_errorMessage_TotalAssociationsLimitByRoleReached, new List<Object>{territoryName,limitsByRolePerTerritoryMap.get(ut2a.RoleInTerritory2),ut2a.RoleInTerritory2}));
                           }
                       }
                }
            }
        }
        
        public static void delegatedApproverManagement(Map<Id, UserTerritory2Association> newItems){
            Set<Id> territoryIds = new Set<Id>();
            Set<Id> firstLevelTerritories = new Set<Id>();
            Set<Id> userIds = new Set<Id>();
            Map<Id, Id> thirdToFirstTerrMap = new Map<Id,Id>();
            List<User> usersToUpdate = new List<User>();
            Map<Id, List<UserTerritory2Association>> terrToUtaMap = new Map<Id, List<UserTerritory2Association>>();
            List<UserTerritory2Association> utasToCheck = new List<UserTerritory2Association>();
            for(UserTerritory2Association uta : newItems.values()){
                if(String.isNotBlank(uta.UserId) && String.isNotBlank(uta.Territory2Id) && String.isNotBlank(uta.RoleInTerritory2)){
                    if(ConstantsUtil.ROLE_IN_TERRITORY_SMA.equalsIgnoreCase(uta.RoleInTerritory2) || ConstantsUtil.ROLE_IN_TERRITORY_SMD.equalsIgnoreCase(uta.RoleInTerritory2)){
                        territoryIds.add(uta.Territory2Id);
                    	userIds.add(uta.UserId);
                        utasToCheck.add(uta);
                    }
                }
            }
            system.debug('utasToCheck: ' + utasToCheck);
            system.debug('Users: ' + userIds + ', territories: ' + territoryIds);
            if(territoryIds.size() > 0){
                Map<Id, User> users = new Map<Id, User>([SELECT Id, DelegatedApproverId FROM User WHERE Id IN :userIds]);
                List<Territory2> territories = [SELECT Id, ParentTerritory2Id FROM Territory2 WHERE Id IN :territoryIds];
                for(Territory2 t : territories){
                    if(String.isNotBlank(t.ParentTerritory2Id)){
                        firstLevelTerritories.add(t.ParentTerritory2Id);
                        thirdToFirstTerrMap.put(t.Id, t.ParentTerritory2Id);
                    }
                }
                List<UserTerritory2Association> utas = [SELECT Id, Territory2Id, UserId, RoleInTerritory2 FROM UserTerritory2Association WHERE Territory2Id IN :territoryIds OR Territory2Id IN :firstLevelTerritories];
                system.debug('utas: ' + utas);
                for(UserTerritory2Association uta : utas){
                    if(!terrToUtaMap.containsKey(uta.Territory2Id)){
                        terrToUtaMap.put(uta.Territory2Id, new List<UserTerritory2Association>{uta});
                    }else{
                        terrToUtaMap.get(uta.Territory2Id).add(uta);
                    }
                }
                system.debug('terrToUtaMap: ' + terrToUtaMap);
                for(UserTerritory2Association u : utasToCheck){
                    system.debug('utaToCheck: ' + u);
                    if(ConstantsUtil.ROLE_IN_TERRITORY_SMA.equalsIgnoreCase(u.RoleInTerritory2)){//sma
                        system.debug('here for SMA');
                        for(UserTerritory2Association utaForSMADelegate : terrToUtaMap.get(thirdToFirstTerrMap.get(u.Territory2Id))){
                            system.debug('utaForSMADelegate: ' + utaForSMADelegate);
                            if(ConstantsUtil.ROLE_IN_TERRITORY_SDI.equalsIgnoreCase(utaForSMADelegate.RoleInTerritory2)){
                                User us = users.get(u.UserId);
                                us.DelegatedApproverId = utaForSMADelegate.UserId;
                                usersToUpdate.add(us);
                            }
                        }
                    }else if(ConstantsUtil.ROLE_IN_TERRITORY_SMD.equalsIgnoreCase(u.RoleInTerritory2)){//smd
                        system.debug('here for SMD');
                        for(UserTerritory2Association utaForSMDDelegate : terrToUtaMap.get(u.Territory2Id)){
                            system.debug('utaForSMDDelegate: ' + utaForSMDDelegate);
                            if(ConstantsUtil.ROLE_IN_TERRITORY_SMA.equalsIgnoreCase(utaForSMDDelegate.RoleInTerritory2)){
                                User us = users.get(u.UserId);
                                us.DelegatedApproverId = utaForSMDDelegate.UserId;
                                usersToUpdate.add(us);
                            }
                        }
                    }
                }
                system.debug('usersToUpdate: ' + usersToUpdate);
                if(usersToUpdate.size() > 0){
                    update usersToUpdate;
                }
            }
        }
        
        public static void delegatedApproverManagementForDelete(Map<Id, UserTerritory2Association> oldItems){
            List<User> usersToUpdate = new List<User>();
            Set<Id> userIds = new Set<Id>();
            for(UserTerritory2Association uta : oldItems.values()){
                system.debug('uta: ' + uta);
                if(!ConstantsUtil.ROLE_IN_TERRITORY_DMC.equalsIgnoreCase(uta.RoleInTerritory2)){
                    userIds.add(uta.UserId); 
                } 
            }
            system.debug('userIds: ' + userIds);
            if(userIds.size() > 0){
                List<User> users = [SELECT Id FROM User WHERE DelegatedApproverId IN :userIds];
                system.debug('users: ' + users);
                for(User u : users){
                    system.debug('u: ' + u);
                    u.DelegatedApproverId = NULL;
                    usersToUpdate.add(u);
                }
                if(usersToUpdate.size() > 0){
                    system.debug('usersToUpdate: ' + usersToUpdate);
                    update usersToUpdate;
                }
            }
        }
    }