@isTest
public class OpprtunityHandlerPBTest {
	
   @isTest
    public static void testOpportunityClosedWON(){
                     
        try{ 
			
                Account acc = createAccount();
                
                Place__c place = new Place__c();
                place.Account__c = acc.Id;
                place.Name = 'test place';
                place.LastName__c = 'test last';
                place.Company__c = 'test company';
                place.City__c = 'test city';
                place.Country__c = 'test coountry';
                place.PostalCode__c = '1234';
                
                insert place;
                System.debug('Place: '+ place);
                
                Contact myContact = new Contact();
                myContact.AccountId = acc.Id;
                myContact.Phone = '07412345';
                myContact.Email = 'test@test.com';
                myContact.LastName = 'tlame';
                myContact.MailingCity = 'test city';
                myContact.MailingPostalCode = '123';
                myContact.MailingCountry = 'test country';
                
                insert myContact;
                System.Debug('Contact: '+ myContact);
                
                Billing_Profile__c billProfile = new Billing_Profile__c();       	
                billProfile.Customer__c	= acc.Id;
                billProfile.Billing_Contact__c = myContact.Id;
                billProfile.Billing_Language__c = 'French';
                billProfile.Billing_City__c	= 'test';
                billProfile.Billing_Country__c = 'test';
                billProfile.Billing_Name__c	= 'test bill name';
                billProfile.Billing_Postal_Code__c = '12345';
                billProfile.Billing_Street__c = 'test 123 Secret Street';	
                billProfile.Channels__c	= 'test';
                billProfile.Name = 'Test Bill Prof Name';
                
                insert billProfile;
                System.Debug('Bill Profile: '+ billProfile);
                                              
                Pricebook2 pBook = createPricebook();
                
                Opportunity opp = new Opportunity();
                opp.Account = acc;
                opp.AccountId = acc.Id;
                opp.Pricebook2Id = pBook.Id;
                opp.StageName = 'Qualification';
                opp.CloseDate = Date.today().AddDays(89);
                opp.Name = 'Test Opportunity';
                    
                insert opp;
                System.Debug('opp '+ opp); 
                
                SBQQ__Quote__c quot = new SBQQ__Quote__c();
                quot.SBQQ__Account__c = acc.Id;
                quot.SBQQ__Opportunity2__c =opp.Id;
                quot.SBQQ__PrimaryContact__c = myContact.Id;
                quot.Billing_Profile__c = billProfile.Id;
                quot.SBQQ__Type__c = 'Quote';
                quot.SBQQ__Status__c = 'Draft';
                quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
                quot.SBQQ__Primary__c = true;
                
                insert quot;
                System.Debug('Quote '+ quot);
                
                Contract myContract = new Contract();
                myContract.AccountId = acc.Id;
                myContract.Status = 'Draft';
                myContract.StartDate = Date.today();
                myContract.TerminateDate__c = Date.today();
                
                insert myContract;
                System.debug('Contract: '+ myContract);   
                            
                                                
                Product2 prod = createProduct();
                
                SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
                quoteLine.SBQQ__Quote__c = quot.Id;
                quoteLine.SBQQ__Product__c = prod.Id;
                quoteLine.Place__c = place.Id;
                quoteLine.SBQQ__Quantity__c = 1;
                
                insert quoteLine;
                System.Debug('Quote Line '+ quoteLine);

                quot.SBQQ__Status__c = 'Accepted';
                update quot;
                System.Debug('Quote '+ quot);
                
                
                quot.SBQQ__Primary__c = true;
                update quot;
                
            test.startTest();
                opp.StageName = 'Closed-Won';
                //opp.IsWon = true;
                update opp;
                System.Debug('opp '+ opp); 
                test.stopTest();
             
            
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }
        
    }

    
     @isTest
    public static void testOpportunityClosedLost(){
                     
        try{ 

                Account acc1 = createAccount1();
                
                Place__c place1 = new Place__c();
                place1.Account__c = acc1.Id;
                place1.Name = 'test place1';
                place1.LastName__c = 'test last1';
                place1.Company__c = 'test company1';
                place1.City__c = 'test city1';
                place1.Country__c = 'test coountry1';
                place1.PostalCode__c = '12341';
                
                insert place1;
                System.debug('Place: '+ place1);
                
                Contact myContact1 = new Contact();
                myContact1.AccountId = acc1.Id;
                myContact1.Phone = '07412345';
                myContact1.Email = 'test@test1.com';
                myContact1.LastName = 'tlame1';
                myContact1.MailingCity = 'test city1';
                myContact1.MailingPostalCode = '1231';
                myContact1.MailingCountry = 'test country1';
                
                insert myContact1;
                System.Debug('Contact: '+ myContact1);
                
                Billing_Profile__c billProfile1 = new Billing_Profile__c();       	
                billProfile1.Customer__c	= acc1.Id;
                billProfile1.Billing_Contact__c = myContact1.Id;
                billProfile1.Billing_Language__c = 'French';
                billProfile1.Billing_City__c	= 'test';
                billProfile1.Billing_Country__c = 'test';
                billProfile1.Billing_Name__c	= 'test bill name';
                billProfile1.Billing_Postal_Code__c = '12345';
                billProfile1.Billing_Street__c = 'test 123 Secret Street';	
                billProfile1.Channels__c	= 'test';
                billProfile1.Name = 'Test Bill Prof Name';
                
                insert billProfile1;
                System.Debug('Bill Profile: '+ billProfile1);
                                              
                Pricebook2 pBook1 = createPricebook1();
                
                Opportunity opp1 = new Opportunity();
                opp1.Account = acc1;
                opp1.AccountId = acc1.Id;
                opp1.Pricebook2Id = pBook1.Id;
                opp1.StageName = 'Qualification';
                opp1.CloseDate = Date.today().AddDays(89);
                opp1.Name = 'Test Opportunity1';
                    
                insert opp1;
                System.Debug('opp '+ opp1); 
                
                SBQQ__Quote__c quot1 = new SBQQ__Quote__c();
                quot1.SBQQ__Account__c = acc1.Id;
                quot1.SBQQ__Opportunity2__c =opp1.Id;
                quot1.SBQQ__PrimaryContact__c = myContact1.Id;
                quot1.Billing_Profile__c = billProfile1.Id;
                quot1.SBQQ__Type__c = 'Quote';
                quot1.SBQQ__Status__c = 'Draft';
                quot1.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
                quot1.SBQQ__Primary__c = true;
                
                insert quot1;
                System.Debug('Quote '+ quot1);
                
                Contract myContract1 = new Contract();
                myContract1.AccountId = acc1.Id;
                myContract1.Status = 'Draft';
                myContract1.StartDate = Date.today();
                myContract1.TerminateDate__c = Date.today();
                
                insert myContract1;
                System.debug('Contract: '+ myContract1);                
              
                                                
                Product2 prod1 = createProduct1();
                
                SBQQ__QuoteLine__c quoteLine1 = new SBQQ__QuoteLine__c();
                quoteLine1.SBQQ__Quote__c = quot1.Id;
                quoteLine1.SBQQ__Product__c = prod1.Id;
                quoteLine1.Place__c = place1.Id;
                quoteLine1.SBQQ__Quantity__c = 1;
                
                insert quoteLine1;
                System.Debug('Quote Line '+ quoteLine1);
                

                quot1.SBQQ__Status__c = 'Accepted';
                update quot1;
                System.Debug('Quote '+ quot1);

                
                quot1.SBQQ__Primary__c = true;
                update quot1;
                
            test.startTest();
                opp1.StageName = 'Closed-Lost';
                //opp.IsWon = true;
                update opp1;
                System.Debug('opp '+ opp1); 
               test.stopTest(); 
              
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }
        
    }

    
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.GoldenRecordID__c = 'GRID';       
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh'; 
        acc.BillingCity = 'test city';
        acc.BillingCountry = 'test country';
        acc.BillingPostalCode = '123';
        acc.BillingState = 'test state';
        acc.PreferredLanguage__c = 'German';
        
        insert acc;
        System.Debug('Account: '+ acc);  
        return acc;
    }
    
    @isTest
    public static Pricebook2 createPricebook(){
        Pricebook2 pBook = new Pricebook2();
        pBook.Name = 'test p book';
        
        insert pBook;
        System.Debug('P. Book '+ pBook); 
        return pBook;
    }
    
    @isTest
    public static Product2 createProduct(){
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.ProductCode = 'test';
        prod.IsActive=True;
        prod.SBQQ__ConfigurationType__c='Allowed';
        prod.SBQQ__ConfigurationEvent__c='Always';
        prod.SBQQ__QuantityEditable__c=True;       
        prod.SBQQ__NonDiscountable__c=False;
        //prod.SBQQ__SubscriptionType__c='Renewable'; 
        prod.SBQQ__SubscriptionPricing__c='Fixed Price';
        prod.SBQQ__SubscriptionTerm__c=12; 
        
        insert prod;
        System.Debug('Product '+ prod); 
        return prod;
    }
   
    
     @isTest
    public static Account createAccount1(){
        Account acc1 = new Account();
        acc1.Name = 'test acc';
        acc1.GoldenRecordID__c = 'GRID';       
        acc1.POBox__c = '10';
        acc1.P_O_Box_Zip_Postal_Code__c ='101';
        acc1.P_O_Box_City__c ='dh'; 
        acc1.BillingCity = 'test city';
        acc1.BillingCountry = 'test country';
        acc1.BillingPostalCode = '123';
        acc1.BillingState = 'test state';
        acc1.PreferredLanguage__c = 'German';
        
        insert acc1;
        System.Debug('Account: '+ acc1);  
        return acc1;
    }
    
    @isTest
    public static Pricebook2 createPricebook1(){
        Pricebook2 pBook1 = new Pricebook2();
        pBook1.Name = 'test p book';
        
        insert pBook1;
        System.Debug('P. Book '+ pBook1); 
        return pBook1;
    }
    
    @isTest
    public static Product2 createProduct1(){
        Product2 prod1 = new Product2();
        prod1.Name = 'test product';
        prod1.ProductCode = 'test';
        prod1.IsActive=True;
        prod1.SBQQ__ConfigurationType__c='Allowed';
        prod1.SBQQ__ConfigurationEvent__c='Always';
        prod1.SBQQ__QuantityEditable__c=True;       
        prod1.SBQQ__NonDiscountable__c=False;
        //prod1.SBQQ__SubscriptionType__c='Renewable'; 
        prod1.SBQQ__SubscriptionPricing__c='Fixed Price';
        prod1.SBQQ__SubscriptionTerm__c=12; 
        
        insert prod1;
        System.Debug('Product '+ prod1); 
        return prod1;
    }
}