@isTest
public class GenerateQuoteDocumentPBTest {

    @isTest
    static void testGenerateQuoteDocument(){
        try{
         	Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
                       
            insert acc;
        
        	Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
                
            insert opp;
            System.Debug('opp '+ opp); 

  			Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
        
           
         	SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            
            insert quot;
            System.Debug('Quote '+ quot);
        
     
        	Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Opportunity__c= opp.Id;
        	myContract.SBQQ__Quote__c= quot.Id;
                
            test.startTest();
            insert myContract;
            System.debug('Contract: '+ myContract); 
          
        	myContract.Status='Active';
        	update myContract;
            test.stopTest();
        }
        catch(Exception e)
        {
            system.assert(false, e.getMessage());
        }
    }
    
}