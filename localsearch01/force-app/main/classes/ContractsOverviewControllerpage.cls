public class ContractsOverviewControllerpage{
    
    
    public SubscriptionsWrapper subswrapper;
    
    public string PDFData {get; set;} 
    public string accountId {get; set;} 
    public String logo {get; set;}
    public String LSFooterBannersLink {get; set;}
    public Account customer {get; set;}
    public Contact contact {get; set;}
    public string language {get; set;}
    public List<SubscriptionsWrapper> wrapper {get; set;}
    public Map<ID, Map<String, String>> productLanguageTranslation = new Map<ID, Map<String, String>>();
   	public List<SubscriptionsWrapper> orderedList_Place {get; set;}
    public List<SubscriptionsWrapper> orderedList {get; set;}    
    public set<Id> subsIds {get; set;}
    public List<Id> masterProductIdsByPlaceIdMapOrder {get; set;}
    public  set<Id> products {get; set;}
	public Map<Id, SBQQ__Subscription__c> subscriptions {get; set;}
    public List<Id> masterProductIdsWithPlace {get; set;}
    public Map<Id, String> editionInfoByMasterSubId {get; set;}
    public Map<Id, SBQQ__Subscription__c> masterSubMapByPlace {get; set;}    
    public Map<String, List<Id>> masterSubByProductCode {get; set;}
    public Map<Id, Integer> numberOfProductOptionsByMasterSubId {get; set;}    
	public Map<Id, Map<Id, SBQQ__Subscription__c>> childrenSubWithPlaceMap {get; set;}
    public Map<Id, Map<Id, SBQQ__Subscription__c>> childrenSubWithoutPlaceMap {get; set;}
	public Map<Id, List<Id>> masterProductIdsByPlaceIdMap {get; set;}    
    public set<Id> places {get; set;}
    public List<Id> masterProductIdsWithoutPlace {get; set;}    
    public Map<Id, SBQQ__Subscription__c> masterSubWithoutPlaceMap{get; set;}
    public String substitutionSubscriptionsBlock {get; set;}
    public Map<Id, Place__c> placeMap {get; set;}
    public Map<Id, Product2> productMap {get; set;} 
	
    public static  string CONTRACT_STATUS_PRODUCTION {get; set;} 
    public static  string CONTRACT_STATUS_ACTIVE {get; set;} 
    public static  string CONTRACT_STATUS_IN_CANCELLATION {get; set;} 
    public static  string CONTRACT_STATUS_CANCELLED {get; set;} 
    public static  string CONTRACT_STATUS_DRAFT {get; set;} 
    public static  string CONTRACT_STATUS_IN_TERMINATION {get; set;} 
    public static  string CONTRACT_STATUS_TERMINATED {get; set;} 
    public static  string CONTRACT_STATUS_EXPIRED {get; set;} 
    public static  string CONTRACT_STATUS_FAILED {get; set;}  
    public static  Map<String, Map<String, String>> statusLanguaMap {get; set;}  
    public Map<Id, String> productNameTranslated {get; set;}
    public Map<String, String> displayedNameMap {get; set;}
    
    public ContractsOverviewControllerpage() {
        System.debug('PDFData '+PDFData);

    } 
    
    public PageReference downloadPDF(){
        CONTRACT_STATUS_ACTIVE = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        CONTRACT_STATUS_CANCELLED = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
        CONTRACT_STATUS_DRAFT = ConstantsUtil.CONTRACT_STATUS_DRAFT;
        CONTRACT_STATUS_TERMINATED = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
        CONTRACT_STATUS_EXPIRED = ConstantsUtil.CONTRACT_STATUS_EXPIRED;
        CONTRACT_STATUS_PRODUCTION =ConstantsUtil.CONTRACT_STATUS_PRODUCTION;
        statusTranslations();
        Map<string, string> fileNameMap = new Map<string, string> {'en' => 'Contract Overview','de' => 'Vertragsübersicht','it' => 'Panoramica dei contratti','fr' => 'Aperçu des contrats'};
        System.debug('PDFData '+PDFData);
        System.debug('language:'+language);
        System.PageReference pageRef = new System.PageReference('/apex/ContractsOverviewPage');
        ContractsOverviewControllerpage();
        string fileName = fileNameMap.containsKey(language) ? fileNameMap.get(language) : 'Contract Overview';
        fileName+='_'+customer.Customer_Number__c;
        system.debug('CtrOverview_Header ::: '+Label.CtrOverview_Header);
        pageRef.getHeaders().put('content-disposition', 'attachment; filename='+ fileName+'.pdf');
        return pageRef;
    }
    
    public void statusTranslations (){
        statusLanguaMap = new Map<String, Map<String, String>> ();
        Map<String, String> activeMap = new Map<String, String>{'en' => CONTRACT_STATUS_ACTIVE,'de' => 'Aktiv','it' => 'Attivo','fr' => 'Actif'};
        statusLanguaMap.put(CONTRACT_STATUS_ACTIVE, activeMap);
        
        Map<String, String> cancelMap = new Map<String, String>{'en' => CONTRACT_STATUS_CANCELLED,'de' => 'Annulliert','it' => 'Annullato','fr' => 'Annulé'};
        statusLanguaMap.put(CONTRACT_STATUS_CANCELLED, cancelMap);
        
        Map<String, String> draftMap = new Map<String, String>{'en' => CONTRACT_STATUS_DRAFT,'de' => 'Entwurf','it' => 'Bozza','fr' => 'Brouillon'};
        statusLanguaMap.put(CONTRACT_STATUS_DRAFT, draftMap);
        
        Map<String, String> terminatedMap = new Map<String, String>{'en' => CONTRACT_STATUS_TERMINATED,'de' => 'Gekündigt','it' => 'Terminato','fr' => 'Terminé'};
        statusLanguaMap.put(CONTRACT_STATUS_TERMINATED, terminatedMap);
        
        Map<String, String> expiredMap = new Map<String, String>{'en' => CONTRACT_STATUS_EXPIRED,'de' => 'Abgelaufen','it' => 'Scaduto','fr' => 'Expiré'};
        statusLanguaMap.put(CONTRACT_STATUS_EXPIRED, expiredMap);
        
        Map<String, String> productionMap = new Map<String, String>{'en' => CONTRACT_STATUS_PRODUCTION,'de' => 'Produktion','it' => 'In Produzione','fr' => 'Production'};
        statusLanguaMap.put(CONTRACT_STATUS_PRODUCTION, productionMap);
        
       Map<String, String> failedMap = new Map<String, String>{'en' => ConstantsUtil.CONTRACT_STATUS_FAILED,'de' => 'Failed','it' => 'Failed','fr' => 'Failed'};
        statusLanguaMap.put(ConstantsUtil.CONTRACT_STATUS_FAILED, failedMap);
    }
    
    public void ContractsOverviewControllerpage()
    {	
        subsIds = new set<Id>();
        getContactInfo();
        customer = SEL_Account.getAccountsById(new Set<Id>{accountId}).values()[0];
        System.debug('customer:'+customer);
        
        wrapper = sortWrapper((List<SubscriptionsWrapper>)JSON.deserialize(PDFData, List<SubscriptionsWrapper>.class));
        system.debug('wrapper: ' + wrapper);
        cleanWrapper(wrapper);
        
        for(SubscriptionsWrapper wrapperObject : wrapper){
            subsIds.add(wrapperObject.id);
            for(SubscriptionsWrapper wrapperObjectChild : wrapperObject.childrens){
                subsIds.add(wrapperObjectChild.id);
            }
             
        }
        if(!subsIds.isEmpty()) getSubscriptions(subsIds);
        
        getTranslatedProducts(wrapper);
       	getDetailInfo(wrapper);
        List<Document> doc = [SELECT id,Name from Document where DeveloperName IN ('Logo_LS','LSFooterBanners') Order by DeveloperName desc];
        if(!doc.isEmpty()){
           	LSFooterBannersLink = URL.getSalesforceBaseUrl().toExternalForm().split('\\.')[0] + '.documentforce.com/servlet/servlet.ImageServer?id=' + doc[0].id + '&oid=' + UserInfo.getOrganizationId(); 
        	system.debug('LSFooterBannersLink ::: '+LSFooterBannersLink);	
        	logo = URL.getOrgDomainUrl().toExternalForm() + '/servlet/servlet.ImageServer?id='  + doc[1].Id + '&oid=' + UserInfo.getOrganizationId();
        	system.debug('logo ::: '+logo); 
        }
        
        

    }
    public void cleanWrapper(List<SubscriptionsWrapper> wrapper){
        for(SubscriptionsWrapper wrapperObject : wrapper){
            if( wrapperObject.placeId != null && wrapperObject.placeId.startsWith('/')) wrapperObject.placeId=  wrapperObject.placeId.right(wrapperObject.placeId.length() -1);
            if( wrapperObject.id != null &&  wrapperObject.id.startsWith('/') )wrapperObject.id=  wrapperObject.id.right(wrapperObject.id.length() -1);
            if( wrapperObject.productId!= null && wrapperObject.productId.startsWith('/'))wrapperObject.productId =  wrapperObject.productId.right(wrapperObject.productId.length() -1);
            wrapperObject.productNameTranslated =  wrapperObject.productName;
            for(SubscriptionsWrapper wrapperObjectChild : wrapperObject.childrens){
                if( wrapperObjectChild.placeId != null && wrapperObjectChild.placeId.startsWith('/'))wrapperObjectChild.placeId=  wrapperObjectChild.placeId.right(wrapperObjectChild.placeId.length() -1);
            	if( wrapperObjectChild.id != null && wrapperObjectChild.id.startsWith('/'))wrapperObjectChild.id=  wrapperObjectChild.id.right(wrapperObjectChild.id.length() -1);
            	if( wrapperObjectChild.productId!= null && wrapperObjectChild.productId.startsWith('/'))wrapperObjectChild.productId =  wrapperObjectChild.productId.right(wrapperObjectChild.productId.length() -1);
            	wrapperObjectChild.productNameTranslated =  wrapperObjectChild.productName;
                
            }
        }
        
    }
    
    public void getDetailInfo(List<SubscriptionsWrapper> wrapper){
       	places = new Set<Id>();
        for(SubscriptionsWrapper wrapperObject : wrapper){
            if(wrapperObject.placeId != null){
               places.add(wrapperObject.placeId); 
            }
        }
        if(!places.isEmpty()) placeMap = SEL_Place.getPlacesById(places);
        system.debug('placeMap :: '+placeMap);
        
    }
    
    public void getSubscriptions(Set<Id> subsIds){
        system.debug('subsIds ::: '+subsIds);
        string filters = 'where Id IN :subsIds ';
        List<QuotePDFDisplayedName__mdt> quotePDFDisplayedNames = [SELECT Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, DisplayedName__c, ProductCodes__c, Synonyms__c FROM QuotePDFDisplayedName__mdt];
        displayedNameMap = new Map<String,String>();
        Set<String> quotePDFDisplayedNamesList  = new Set<String>();
        if(quotePDFDisplayedNames != null && quotePDFDisplayedNames.size() > 0)
        {
            List<String> productCodes = new List<String>();
            for(QuotePDFDisplayedName__mdt quotePDFDisplayedName : quotePDFDisplayedNames)
            {
                if(String.isNotBlank(quotePDFDisplayedName.DisplayedName__c) && !quotePDFDisplayedNamesList.contains(quotePDFDisplayedName.DisplayedName__c))
                {
                    quotePDFDisplayedNamesList.add(quotePDFDisplayedName.DisplayedName__c);
                }
                
                if(String.isNotBlank(quotePDFDisplayedName.ProductCodes__c) && String.isNotBlank(quotePDFDisplayedName.DisplayedName__c))
                {
                    for(String productCode : quotePDFDisplayedName.ProductCodes__c.split(';'))
                    {
                        displayedNameMap.put(productCode, quotePDFDisplayedName.DisplayedName__c);
                    }
                }
            }
        }
        
     subscriptions = new Map<Id, SBQQ__Subscription__c>();
     Set<String> fieldNames = new Set<String>(('SBQQ__TerminatedDate__c,SBQQ__Contract__r.SBQQ__ExpirationDate__c,SBQQ__Contract__r.TerminateDate__c,SBQQ__Contract__r.Cancel_Date__c,SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__Type__c,Id,Name, SBQQ__ProductName__c, SBQQ__AdditionalDiscountAmount__c, Subsctiption_Status__c, '+
                            + 'SBQQ__Product__c , SBQQ__Product__r.Manual_Activation__c, SBQQ__Product__r.Production__c, Place__c , Place_Name__c, SBQQ__SubscriptionType__c, ' +
                            + 'SBQQ__Bundled__c,  SBQQ__EndDate__c,  End_Date__c, In_Termination__c, Termination_Date__c, SBQQ__StartDate__c, Total__c, Package_Total__c, '+
                            + 'Subscription_Term__c, SBQQ__Product__r.Base_term_Renewal_term__c, SBQQ__Product__r.Product_Group__c, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c, '+
            				+ 'SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Contract__r.SBQQ__Opportunity__r.Name, SBQQ__Contract__r.AccountId, SBQQ__Contract__r.Account.Customer_Number__c, '+
            				+ 'SBQQ__Contract__r.SBQQ__Quote__c, SBQQ__Contract__r.SBQQ__Quote__r.Name, SBQQ__Contract__r.Account.Name, SBQQ__Product__r.Period_of_notice__c, '+
                            + 'SBQQ__Product__r.One_time_Fee__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, SBQQ__Quantity__c, SBQQ__Contract__c, SBQQ__Contract__r.ContractNumber,'+
        					+ 'SBQQ__Product__r.PlaceIDRequired__c,SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c,SBQQ__Product__r.ProductCode,SBQQ__Product__r.ExternalKey_ProductCode__c, SBQQ__QuoteLine__r.DocRowNumber__c,SBQQ__RequiredById__c,RequiredBy__c, SBQQ__ProductOption__c, '+
          					+ 'RequiredBy__r.Place__c,SBQQ__QuoteLine__r.Sort_Order__c,SBQQ__Product__r.Name,SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c,Subscription_Term__c,SBQQ__Product__r.Configuration_Model__c,SBQQ__ListPrice__c,Package_Total__c, '+
            				+ 'SBQQ__BillingFrequency__c,Edition__c,SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c, SBQQ__QuoteLine__r.Edition__r.Local_Guide_Main_Title__c, SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_2__c, SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_3__c, '+
            				+ 'SBQQ__Discount__c,SBQQ__QuoteLine__r.Base_term_Renewal_term__c,SBQQ__Quantity__c,SBQQ__QuoteLine__r.SBQQ__EffectiveQuantity__c,Place__r.PlaceID__c,Place__r.Name,Category__c,Location__c,SBQQ__QuoteLine__r.Edition__r.MinGuaranteedCopies__c').replaceAll('\\s+','').split(','));  
        
     
   	

        subscriptions = new Map<Id, SBQQ__Subscription__c>();
        
        if(fieldNames.size() > 0)
        {
            Boolean first = true;
            String querystr = 'SELECT ';
            for(String fieldName : fieldNames)
            {
                querystr += ((!first)?', ':'') + fieldName;
                first = false;
            }
            
            querystr += ' from SBQQ__Subscription__c '+filters +' ORDER BY SBQQ__QuoteLine__r.DocRowNumber__c NULLS LAST, SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c nulls last';
            
            system.debug('querystr :: '+querystr);
            try{
                subscriptions = new Map<Id, SBQQ__Subscription__c>((List<SBQQ__Subscription__c>)Database.query(querystr));
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
        }
        
        system.debug('subscriptions ::: '+subscriptions.size());
        
        

		masterProductIdsByPlaceIdMapOrder = new List<Id>();
        products = new Set<Id>();
        masterProductIdsWithPlace = new List<Id>();
        editionInfoByMasterSubId = new Map<Id, String>();
        numberOfProductOptionsByMasterSubId = new  Map<Id, Integer>();
        masterSubMapByPlace = new Map<Id, SBQQ__Subscription__c> ();
        masterSubByProductCode = new  Map<String, List<Id>>();
        childrenSubWithPlaceMap= new Map<Id, Map<Id, SBQQ__Subscription__c>>();
     	childrenSubWithoutPlaceMap = new Map<Id, Map<Id, SBQQ__Subscription__c>>();
        masterProductIdsByPlaceIdMap = new Map<Id, List<Id>>();
        places = new Set<Id>();
        masterProductIdsWithoutPlace = new List<Id>();
        masterSubWithoutPlaceMap = new Map<Id, SBQQ__Subscription__c>();
        productMap = new Map<Id, Product2>();
        //productsInfos = new Map<Id, productBundleInformation>();
        productNameTranslated = new Map<Id, String>();
        substitutionSubscriptionsBlock = '';
        
        for(SBQQ__Subscription__c sub : subscriptions.values()){   
             products.add(sub.SBQQ__Product__c);   
            

			if(sub.Edition__c != NULL){
                    String value = '';
                    if(!editionInfoByMasterSubId.containsKey(sub.Id)){
                        if(sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c != NULL){
                            value += ' - ' + String.valueOf(sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c);
                            
                            if(String.isNotBlank(sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Main_Title__c)){
                                value += ', ' + sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Main_Title__c;
                            }
                            
                            if(String.isNotBlank(sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_2__c)){
                                value += ', ' + sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_2__c;
                            }
                            
                            if(String.isNotBlank(sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_3__c)){
                                value += ', ' + sub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_3__c;
                            }
                            
                            if(String.isNotBlank(sub.Location__c) || String.isNotBlank(sub.Category__c)){
                                value += ', ';
                            }
                            
                            editionInfoByMasterSubId.put(sub.Id, value);
                        }
                    }
                }
            else if(sub.SBQQ__RequiredById__c != NULL && subscriptions.containsKey(sub.SBQQ__RequiredById__c) && subscriptions.get(sub.SBQQ__RequiredById__c).Edition__c != NULL){
                    String value = '';
                    if(!editionInfoByMasterSubId.containsKey(sub.Id)) {
                        if(subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c != NULL){
                            value += ' - ' + String.valueOf(subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c);
                            
                            if(String.isNotBlank(subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Main_Title__c)){
                                value += ', ' + subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Main_Title__c;
                            }
                            
                            if(String.isNotBlank(subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_2__c)){
                                value += ', ' + subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_2__c;
                            }
                            
                            if(String.isNotBlank(subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_3__c)){
                                value += ', ' + subscriptions.get(sub.SBQQ__RequiredById__c).SBQQ__QuoteLine__r.Edition__r.Local_Guide_Title_3__c;
                            }
                            
                            if(String.isNotBlank(subscriptions.get(sub.SBQQ__RequiredById__c).Location__c) || String.isNotBlank(subscriptions.get(sub.SBQQ__RequiredById__c).Category__c)){
                                value += ', ';
                            }
                            
                            editionInfoByMasterSubId.put(sub.Id, value);
                        }
                    }
                }
            else{
                    editionInfoByMasterSubId.put(sub.Id, '');
                }            
            
            if(((sub.Place__c != NULL && sub.SBQQ__Product__r.PlaceIDRequired__c == 'Yes') || (sub.Place__c == NULL && sub.SBQQ__Product__r.PlaceIDRequired__c != 'Yes')|| (sub.Place__c == NULL && sub.SBQQ__Product__r.PlaceIDRequired__c == 'Yes') || sub.SBQQ__Product__r.PlaceIDRequired__c == 'Optional' ) && sub.SBQQ__ProductOption__c == NULL){
                
                if(!numberOfProductOptionsByMasterSubId.containsKey(sub.Id))
                {
                    numberOfProductOptionsByMasterSubId.put(sub.Id, 0);
                }
                if(sub.Place__c != NULL)
                {
                    if(!masterSubMapByPlace.containsKey(sub.Place__c))
                    {
                        masterSubMapByPlace.put(sub.Place__c, sub);
                    }
                    
                    if(sub.SBQQ__Product__r.ProductCode != NULL)
                    {
                        if(!masterSubByProductCode.containsKey(sub.SBQQ__Product__r.ExternalKey_ProductCode__c))
                        {
                            masterSubByProductCode.put(sub.SBQQ__Product__r.ExternalKey_ProductCode__c, new List<Id>());
                        }
                        
                        masterSubByProductCode.get(sub.SBQQ__Product__r.ExternalKey_ProductCode__c).add(sub.Id);
                    }
                    
                    if(!childrenSubWithPlaceMap.containsKey(sub.Id))
                    {
                        childrenSubWithPlaceMap.put(sub.Id, new Map<Id, SBQQ__Subscription__c>());
                    }
                    
                    masterProductIdsWithPlace.add(sub.Id);
                    if(!masterProductIdsByPlaceIdMap.containsKey(sub.Place__c))
                    {
                        masterProductIdsByPlaceIdMap.put(sub.Place__c, new List<Id>());
                        masterProductIdsByPlaceIdMapOrder.add(sub.Place__c);
                    }
                    
                    masterProductIdsByPlaceIdMap.get(sub.Place__c).add(sub.Id);
                    places.add(sub.Place__c);
                
                }else{
                    masterProductIdsWithoutPlace.add(sub.Id);
                    
                    if(sub.SBQQ__Product__r.ProductCode != NULL)
                    {
                        if(!masterSubByProductCode.containsKey(sub.SBQQ__Product__r.ExternalKey_ProductCode__c))
                        {
                            masterSubByProductCode.put(sub.SBQQ__Product__r.ExternalKey_ProductCode__c, new List<Id>());
                        }
                        
                        masterSubByProductCode.get(sub.SBQQ__Product__r.ExternalKey_ProductCode__c).add(sub.Id);
                    }
                    
                    if(!childrenSubWithoutPlaceMap.containsKey(sub.Id))
                    {
                        childrenSubWithoutPlaceMap.put(sub.Id,  new Map<Id, SBQQ__Subscription__c>());
                    }
                    
                    masterSubWithoutPlaceMap.put(sub.Id, sub);
                }
                
                
            }
            else{
                if(sub.SBQQ__RequiredById__c != NULL && !numberOfProductOptionsByMasterSubId.containsKey(sub.SBQQ__RequiredById__c))
                {
                    numberOfProductOptionsByMasterSubId.put(sub.SBQQ__RequiredById__c, 0);
                }
                
                if(sub.SBQQ__RequiredById__c != NULL && sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c != 'Editor Only' || string.isBlank(sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c) )
                {
                	if(numberOfProductOptionsByMasterSubId.get(sub.SBQQ__RequiredById__c) != null) numberOfProductOptionsByMasterSubId.put(sub.SBQQ__RequiredById__c,numberOfProductOptionsByMasterSubId.get(sub.SBQQ__RequiredById__c) + 1);
                }
                
                if(sub.RequiredBy__r.Place__c != NULL){
                    system.debug('sub.Id :  '+sub.Id);
                    system.debug('sub.IName:  '+sub.Name);
                    if(!childrenSubWithPlaceMap.containsKey(sub.SBQQ__RequiredById__c))
                    {
                        childrenSubWithPlaceMap.put(sub.SBQQ__RequiredById__c, new Map<Id, SBQQ__Subscription__c>());
                    }
                    if(sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c != 'Editor Only' || string.isBlank(sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c) )
                    {
						system.debug('sub.Id222 :  '+sub.Id);
                        childrenSubWithPlaceMap.get(sub.SBQQ__RequiredById__c).put(sub.Id, sub);
                        
                    }
                }else
                {
                    if(!childrenSubWithoutPlaceMap.containsKey(sub.SBQQ__RequiredById__c))
                    {
                        childrenSubWithoutPlaceMap.put(sub.SBQQ__RequiredById__c,  new Map<Id, SBQQ__Subscription__c>());
                    }
                    
                    if(sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c != 'Editor Only' || string.isBlank(sub.SBQQ__ProductOption__r.SBQQ__QuoteLineVisibility__c))
                    {
                        childrenSubWithoutPlaceMap.get(sub.SBQQ__RequiredById__c).put(sub.Id, sub);
                    }
                }
                
            }
   
        }
        
         	placeMap = SEL_Place.getPlacesById(places);
            productMap = SEL_Product2.getProductsMap(products);
            
            Map<Id, SBQQ__Localization__c> locals = SEL_Localization.getTranslatedProductName(productMap.keySet(), language);
            
            for(SBQQ__Localization__c loc : locals.values()){
                productNameTranslated.put(loc.SBQQ__Product__c, adjustProductName(loc.SBQQ__Text__c, quotePDFDisplayedNames));
            }
        
            for(Product2 prod : productMap.values()){
                if(!productNameTranslated.containsKey(prod.Id))
                {
                    productNameTranslated.put(prod.Id, adjustProductName(prod.Name,quotePDFDisplayedNames));
                }
            }
        
        
        
    }
    
    public void getTranslatedProducts(List<SubscriptionsWrapper> wrapper){
        set<Id> productIds = new set<Id>();
        
        
       
        for(SubscriptionsWrapper wrapperObject : wrapper){
            //string prodId ='';
         	 wrapperObject.productNameTranslated =  wrapperObject.productName;
            
          
            if( wrapperObject.productId.startsWith('/')){
               wrapperObject.productId =  wrapperObject.productId.right(wrapperObject.productId.length() -1);
            }
            system.debug('UPDATED ID :: '+wrapperObject.productId);
            productIds.add(wrapperObject.productId);
            for(SubscriptionsWrapper child : wrapperObject.childrens){
                child.productNameTranslated =  child.productName;
                //string childProdId = '';
                if(child.productId.startsWith('/')){  child.productId = child.productId.right(child.productId.length() -1);}
                system.debug('UPDATED CHILD ID :: '+ child.productId);
                productIds.add( child.productId);
            }
        }
        
        
        
        system.debug('productIds :: '+productIds);
        List<SBQQ__Localization__c> localizations = [SELECT ID, SBQQ__Product__c, SBQQ__Language__c, SBQQ__Text__c 
                                                     FROM SBQQ__Localization__c
                                                     WHERE SBQQ__Product__c IN :productIds 
                                                     AND SBQQ__Label__c = :ConstantsUtil.TRANSLATION_LABEL_PRODUCT_NAME];      
        
       
        for(SBQQ__Localization__c loc : localizations){
            if(!String.isBlank(loc.SBQQ__Text__c)){
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                    productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                }else{
                    productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                }
            }
        }
        
        SYSTEM.debug('productLanguageTranslation ::: '+productLanguageTranslation);
        
        if(!productLanguageTranslation.isEmpty() && language !='en_US' && language !='en'){
            
            //productNameTranslated
               
            for(SubscriptionsWrapper wrapperObject : wrapper){
                system.debug('productNameTranslated :: '+wrapperObject.productNameTranslated+ ' wrapperObject.productName:: '+wrapperObject.productName);
				 if(!string.isBlank(productLanguageTranslation.get(wrapperObject.productId)?.get(language)))wrapperObject.productNameTranslated = productLanguageTranslation.get(wrapperObject.productId)?.get(language);
            	for(SubscriptionsWrapper child : wrapperObject.childrens){
					if(!string.isBlank(productLanguageTranslation.get(child.productId)?.get(language)))child.productNameTranslated = productLanguageTranslation.get(child.productId)?.get(language);
            	}
        	}
        }
    }
    
    
    public void getContactInfo(){
        
        List<Contact> con = [Select id,Name,Primary__c,Email,Business_Phone__c,Phone from contact where AccountId =:accountId  Order by Primary__c desc];
        if(!con.isEmpty()){
            for(Contact cons : con){
                if(cons.Primary__c){
                    contact = cons;
                    break;
                }else{
                   contact = cons;  
                }
            }
        }
    }
    
    private String adjustProductName(String productName, List<QuotePDFDisplayedName__mdt> namesToReplaceInProductName){
		String normalizedProductName = productName.normalizeSpace();        
        String replacedString = normalizedProductName;
        
        Map<String, String> namesToBeReplacedMap = new Map<String, String>();
        
        for(QuotePDFDisplayedName__mdt name : namesToReplaceInproductName){
            if(String.isNotBlank(name.DisplayedName__c)){
            	namesToBeReplacedMap.put(name.DisplayedName__c.toLowerCase(), name.DisplayedName__c);
            }
            
            if(String.isNotBlank(name.Synonyms__c)){
                List<String> synonyms = name.Synonyms__c.split(';');
                for(String syn : synonyms){
                	namesToBeReplacedMap.put(syn, name.DisplayedName__c);
                }
            }
        }
        
        for(String name : namesToBeReplacedMap.keySet()){
            replacedString = replacedString.replaceAll('(?i)'+name, namesToBeReplacedMap.get(name));
        }    
        
        return replacedString;
    }    

    
    public List<SubscriptionsWrapper> sortWrapper(List<SubscriptionsWrapper> wrapper) {
        orderedList_Place = new List<SubscriptionsWrapper>();
        orderedList = new List<SubscriptionsWrapper>();
      List<SubscriptionsWrapper>   orderedListInit = new List<SubscriptionsWrapper>();
		integer i=1;
        integer secStart =1;
        for(SubscriptionsWrapper wrapperObject : wrapper){
            if(wrapperObject.placeId != null){
                wrapperObject.rowNumber = i;
                orderedList_Place.add(wrapperObject);
                orderedListInit.add(wrapperObject);
                secStart = i+1;
                i++;
                
            }
        }
        for(SubscriptionsWrapper wrapperObject : wrapper){
            if(wrapperObject.placeId == null){
                 wrapperObject.rowNumber = secStart;
                orderedList.add(wrapperObject);
                orderedListInit.add(wrapperObject);
                secStart++;
            }
        }
        orderedList_Place.sort();
        orderedList.sort();
        return orderedListInit;
     
    }
    
    

    

   	
    
    public class SubscriptionsWrapper implements Comparable {
        @AuraEnabled public string id {get; set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String quoteName {get; set;} 
        @AuraEnabled public decimal rowPosition {get; set;}
        @AuraEnabled public decimal shortPosition  {get; set;} 
        @AuraEnabled public Date conTermCancelDate {get; set;}
        @AuraEnabled public string placeId {get; set;}
        @AuraEnabled public String placeName {get; set;}
        @AuraEnabled public string productId {get; set;}
        @AuraEnabled public String productName {get; set;}
        @AuraEnabled public String productNameTranslated {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String subType {get; set;}
        @AuraEnabled public Boolean showOptions {get; set;}
        @AuraEnabled public Date startDate {get; set;}
        @AuraEnabled public Date endDate {get; set;}
        @AuraEnabled public boolean inTermination {get; set;}
        @AuraEnabled public Date terminationDate {get; set;}
        @AuraEnabled public List<SubscriptionsWrapper> childrens {get; set;}
        @AuraEnabled public Decimal discount {get; set;}
        @AuraEnabled public Decimal total {get; set;}
        @AuraEnabled public String SubscriptionTerm {get; set;}
        @AuraEnabled public Decimal BasetermRenewalTerm {get; set;}
        @AuraEnabled public Decimal baseTermPrice {get; set;}
        @AuraEnabled public string contractId {get; set;}
        @AuraEnabled public String contractNumber {get; set;}
        @AuraEnabled public Decimal quantity {get; set;}
        @AuraEnabled public String productGroup {get; set;}
        @AuraEnabled public string accountId {get; set;}
        @AuraEnabled public String customerNumber {get; set;}
        @AuraEnabled public String accountName {get; set;}
        @AuraEnabled public string opportunityId {get; set;}
        @AuraEnabled public String opportunityName {get; set;}
        @AuraEnabled public string quoteId {get; set;}
        @AuraEnabled public String quoteNumber {get; set;}
        @AuraEnabled public String periodsOfNotice {get; set;}
        @AuraEnabled public Decimal bundleTotal {get; set;}
			@AuraEnabled public integer rowNumber {get; set;}        
        public SubscriptionsWrapper(){
            childrens = new List<SubscriptionsWrapper>();
        }
        
        public Integer compareTo(Object o) {
            SubscriptionsWrapper that = (SubscriptionsWrapper) o;
            if (this.shortPosition < that.shortPosition) return -1;
            else if (this.shortPosition > that.shortPosition) return 1;
            else return -1;

        }
        
    }
    
    
	
    

    

}