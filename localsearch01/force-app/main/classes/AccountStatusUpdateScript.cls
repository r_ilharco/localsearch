/**
 * Author      : Luciano Di Martino 
 * Date	       : 2020-21-05
 * Sprint      : phase 2
 * Work item   : Migration phase
 * Testclass   : SetAccountStatusBatch_Test
 * Package     : restore account status and number of active contracts.
 * Description : 
 * Changelog   : #1 vlaudato - Added new logic to populate last inactive date and Active DMC Contracts fields on Account
 */

public class AccountStatusUpdateScript {
	public static void updateAccountStatus(Set<Id> accIds){
        List<Account> accounts = [SELECT Id, Status__c, ClientRating__c,Last_Inactive_Date__c,
                                  (SELECT Id, StageName FROM Opportunities), 
                                  (SELECT Id, Status, TerminateDate__c, EndDate, DMC_Pricebook__c, Cancel_Date__c FROM Contracts), 
                                  (SELECT Id 
                                   FROM Orders
                                   WHERE Type != :ConstantsUtil.ORDER_TYPE_AMENDMENT and Custom_Type__c = :ConstantsUtil.ORDER_TYPE_ACTIVATION 
                                   and (Status = :ConstantsUtil.ORDER_STATUS_PRODUCTION OR Status = :ConstantsUtil.ORDER_STATUS_PUBLICATION OR Status = :ConstantsUtil.ORDER_STATUS_REJECTED )
                                  ) 
                                  FROM Account 
                                  WHERE Id in: accIds];
        
        List<Account> accountsToUpdate = new List<Account>();
        Integer activeContracts = 0;
        Integer draftContracts = 0;
        Integer intactiveContracts = 0;
        Integer activeDMCContracts = 0;
        Integer closedOpp = 0;
        
        for(Account a : accounts){
            if(a.Opportunities.size() == 0) a.Status__c = ConstantsUtil.ACCOUNT_STATUS_NEW;
            else{
                if(a.Contracts.size() == 0){ 
                    if(a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                    	a.Status__c = ConstantsUtil.ACCOUNT_STATUS_PROSPECT;
                    a.Active_DMC_Contracts__c = 0;
                    a.Active_Contracts__c = 0;
				}
                else{
                    activeContracts = 0;
                    draftContracts = 0;
                    intactiveContracts = 0;
                    activeDMCContracts = 0;
                    a.Last_Inactive_Date__c = null;
                    for(Contract c : a.Contracts){
                        if(a.Last_Inactive_Date__c == null && c.DMC_Pricebook__c== true && (c.Status == ConstantsUtil.CONTRACT_STATUS_CANCELLED || c.Status == ConstantsUtil.CONTRACT_STATUS_TERMINATED)){
                            Date lastInactiveDate = getLastInactiveDateWhenNull(c.TerminateDate__c, c.EndDate, c.Cancel_Date__c);
                            a.Last_Inactive_Date__c = lastInactiveDate;
                        }
                        else if(c.DMC_Pricebook__c== true && (c.Status == ConstantsUtil.CONTRACT_STATUS_CANCELLED || c.Status == ConstantsUtil.CONTRACT_STATUS_TERMINATED)){
                            Date lastInactiveDate = getLastInactiveDateWhenNotNull(a.Last_Inactive_Date__c, c.TerminateDate__c, c.EndDate, c.Cancel_Date__c);
                            a.Last_Inactive_Date__c = lastInactiveDate;
                        }
                        if(ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(c.Status)) activeContracts++;
                        if(ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(c.Status) && c.DMC_Pricebook__c) activeDMCContracts++; 
                        if(ConstantsUtil.CONTRACT_STATUS_DRAFT.equalsIgnoreCase(c.Status)) draftContracts++;      
                    }
                    
                    if(activeContracts > 0){
                        a.Active_Contracts__c = activeContracts;
                        if(a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                        	a.Status__c = ConstantsUtil.ACCOUNT_STATUS_ACTIVE;
                        if(activeDMCContracts > 0) {
                            a.Active_DMC_Contracts__c = activeDMCContracts;
                            a.Last_Inactive_Date__c = null;
                        }
                        else a.Active_DMC_Contracts__c = 0;
                    }
                    else{
                        if(draftContracts > 0 && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                            a.Status__c = ConstantsUtil.ACCOUNT_STATUS_PROSPECT;	    
                        else{
                            closedOpp = 0;
                            for(Opportunity o : a.Opportunities){
                                if(o.stageName.containsIgnoreCase('Closed')) closedOpp++;
                            }
                            if(closedOpp != a.Opportunities.size()  && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                                a.Status__c = ConstantsUtil.ACCOUNT_STATUS_PROSPECT;
                            else{
                                if(a.Orders.size() > 0  && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                                    a.Status__c = ConstantsUtil.ACCOUNT_STATUS_PROSPECT;
                                else if(a.ClientRating__c != ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS && a.ClientRating__c != ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS)
                                    a.Status__c = ConstantsUtil.ACCOUNT_STATUS_INACTIVE;
                            }
                        }
                    }
                }
            } 
            accountsToUpdate.add(a);
        }
        String log = '';
        AccountTriggerHandler.disableTrigger = true;
        List<Database.SaveResult> resultsAccount = Database.update(accountsToUpdate, false);
        AccountTriggerHandler.disableTrigger = false;
        for (Database.SaveResult sr : resultsAccount) {
            if (!sr.isSuccess()) {              
                for(Database.Error err : sr.getErrors()) {          
                    log +=err.getStatusCode() + ': ' + err.getMessage()+' - '+ err.getFields() + 'id: '+sr.getId();
                }
            }
        }
        if(log != ''){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'AccountStatusUpdateScript', 'AccountStatusUpdateScript.updateAccountStatus', null, ErrorHandler.ErrorCode.E_DML_FAILED, log, null, null, null, null, false);
        }
    }
    
    public static Date getLastInactiveDateWhenNull(Date terminationDate, Date endDate, Date cancelDate){
        Date lastInactiveDate;
        if(terminationDate != null){
            if(terminationDate <= Date.today()) lastInactiveDate = terminationDate;
        }
        if(cancelDate != null){
            if(cancelDate <= Date.today()) lastInactiveDate = cancelDate;
        }
        else if(endDate != null){
            if(endDate <= Date.today()) lastInactiveDate = endDate;
        }
        return lastInactiveDate;
    }
    
    public static Date getLastInactiveDateWhenNotNull(Date accountInactiveDate, Date terminationDate, Date endDate, Date cancelDate){
        Date lastInactiveDate;
        if(terminationDate != null){
            if(terminationDate <= Date.today() && terminationDate < accountInactiveDate) lastInactiveDate = terminationDate;
        }
        if(cancelDate != null){
            if(cancelDate <= Date.today() && cancelDate < accountInactiveDate) lastInactiveDate = cancelDate;
        }
        else if(endDate != null){
            if(endDate <= Date.today() && endDate < accountInactiveDate) lastInactiveDate = endDate;
        }
        return lastInactiveDate;
    }
}