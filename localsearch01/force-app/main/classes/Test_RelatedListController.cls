@istest 
public class Test_RelatedListController {

    @testsetup static void setup(){
     list<account> accounts = test_dataFactory.createAccounts('testAcc',3);
     insert accounts;
     for(integer i=2;i<3;i++){
            accounts[i].ParentId = accounts[0].Id;
        }
    update(accounts);
    Place__c places = Test_DataFactory.generatePlaces(accounts[0].id, 'Place');
		insert places;
     }
    @isTest
    public static void test_getRecordList(){
		Account acc = [select id from Account limit 1];
        RelatedListController.getRecordList(acc.Id);     
    }
    
}