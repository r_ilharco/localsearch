public class SRV_DocumentDownloadComplete{
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Validate params received in JSON Body.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    documentDownloadCompleteRequest	Received params
    * @return         String							Error message to return
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static String requestValidation(DocumentDownloadComplete.DocumentDownloadCompleteRequest documentDownloadCompleteRequest){
        
        String missingFieldsError = validateMandatoryFields(documentDownloadCompleteRequest);
        
        if(String.isEmpty(missingFieldsError)) return validateFieldsValue(documentDownloadCompleteRequest);
        else return missingFieldsError;
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Check for missing fields
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    documentDownloadCompleteRequest	Received params
    * @return         String							Error message to return
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static String validateMandatoryFields(DocumentDownloadComplete.DocumentDownloadCompleteRequest documentDownloadCompleteRequest){
        
        List<String> missingFields = new List<String>();
        String result;
        
        if(String.isBlank(documentDownloadCompleteRequest.quoteId)) missingFields.add('quoteId');
        if(String.isBlank(documentDownloadCompleteRequest.quoteDocumentId)) missingFields.add('quoteDocumentId');
        if(String.isBlank(documentDownloadCompleteRequest.signatureDate)) missingFields.add('signatureDate');
        if(String.isBlank(documentDownloadCompleteRequest.distributionPublicUrl)) missingFields.add('distributionPublicUrl');
        
        if(!missingFields.isEmpty()) result = 'Missing mandatory field(s): ' + missingFields;
        
        return result;
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Check for fields validity
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    documentDownloadCompleteRequest	Received params
    * @return         String							Error message to return
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static String validateFieldsValue(DocumentDownloadComplete.DocumentDownloadCompleteRequest documentDownloadCompleteRequest){
        
        String result;
        List<String> invalidValues = new List<String>();
        
        try{
            Id quoteId = Id.valueOf(documentDownloadCompleteRequest.quoteId);
            try{
                Id quoteDocumentId = Id.valueOf(documentDownloadCompleteRequest.quoteDocumentId);
            }
            catch(StringException se){
                invalidValues.add('quoteDocumentId');
            }
        }
        catch(StringException se){
            invalidValues.add('quoteId');
        }
        finally{
            if(!invalidValues.isEmpty()) result = 'Invalid value(s) for field(s): ' + invalidValues;
            else{
                Id quoteId = Id.valueOf(documentDownloadCompleteRequest.quoteId);
                Id quoteDocumentId = Id.valueOf(documentDownloadCompleteRequest.quoteDocumentId);
                if(!quoteId.getSObjectType().getDescribe().getName().equals('SBQQ__Quote__c')) result = 'Invalid Quote Id';
                if(!quoteDocumentId.getSObjectType().getDescribe().getName().equals('SBQQ__QuoteDocument__c')) result = 'Invalid Quote Document Id';
            }
        }
        
        return result;
    }
   	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Accept and update the Quote with Document Url and Signature Date.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    quote						Quote to update
    * @input param    signatureDate				Signature date (YYYY-MM-ddThh:mm:ss.SSSZ format)
    * @input param    distributionPublicUrl		Document URL
    * @return
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void updateQuote(SBQQ__Quote__c quote, String signatureDate, String distributionPublicUrl){
        
        signatureDate = signatureDate.replace('T',' ');
        signatureDate = signatureDate.substring(0,signatureDate.indexOf('.'));
        
        quote.Signed_Document_URL__c = distributionPublicUrl;
        quote.SignatureDate__c = Datetime.valueOfGmt(signatureDate);
        quote.Contract_Signed_Date__c = Date.valueOf(signatureDate);
        
        if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quote.SBQQ__Type__c))
            quote.SBQQ__Status__c = ConstantsUtil.Quote_Status_Accepted;
        
        try{
            update quote;
        }
        catch(Exception e){
            quote.SBQQ__Status__c = ConstantsUtil.QUOTE_STATUS_PRESENTED;
            update quote;
        }        
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * AUpdate the Quote Document to Signed.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    quoteDocumentId			QuoteDocument to update
    * @input param    signatureDate				Signature date (YYYY-MM-ddThh:mm:ss.SSSZ format)
    * @return
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void updateQuoteDocument(Id quoteDocumentId, String signatureDate){
        
        signatureDate = signatureDate.replace('T',' ');
        signatureDate = signatureDate.substring(0,signatureDate.indexOf('.'));
        
        SBQQ__QuoteDocument__c quoteDocument = new SBQQ__QuoteDocument__c(Id = quoteDocumentId);
        quoteDocument.IsEditableSignatureDate__c = true;
        quoteDocument.SignatureDate__c = Datetime.valueOfGmt(signatureDate);
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        update quoteDocument;
        
        quoteDocument.IsEditableSignatureDate__c = false;
        update quoteDocument;
        
    }
    
    public static void sendNotificationToSales(SBQQ__Quote__c quote, Id quoteDocumentId){
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, Name FROM SBQQ__QuoteDocument__c WHERE Id = :quoteDocumentId LIMIT 1];
        
        FeedItem post = new FeedItem();
        post.ParentId = quote.SBQQ__SalesRep__c;
        String message = Label.Namirial_Signed_Notification.replace('{documentName}', quoteDocument.Name);
        post.Body = message;
        post.Title = 'Digital Signature';
        post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ quoteDocumentId;
        
        insert post;
    }
}