/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-27-2019
 * Sprint      : 2
 * Work item   : US_61 - blocking down selling on a quote for the same place
 * Testclass   :
 * Package     : 
 * Description : Selector Class for SBQQ__Quote__c
 * Changelog   : 
 */

public with sharing class SEL_SBQQQuote {
    /**
	 * Gennaro Casola | Sprint 2 | Date 05-27-2019
	 * Description
     * Get Quote by Id specified in Set<Id> ids
	 *
	 */
    public static Map<Id, SBQQ__Quote__c> getQuoteById(Set<Id> ids)
    {
        return new Map<Id, SBQQ__Quote__c>([SELECT  Id, 
                                                    SBQQ__Type__c, 
                                                    Upgrade_Downgrade_Contract__c,
                                                    SBQQ__ExpirationDate__c,
                                                    SBQQ__Account__r.PreferredLanguage__c,
                                                    SBQQ__Account__c, 
                                                    SBQQ__Account__r.Customer_Number__c,
                                                    SBQQ__StartDate__c,
                                                    Name,
                                                    SBQQ__Primary__c,
                                                    Trial_CallId__c
                                            FROM    SBQQ__Quote__c 
                                            WHERE   Id IN :ids]);
    }    
    /**
     *  Fabio Maturo | Sprint 5 | Date 12/07/2019
     *  Get Quote Related to Billing Profile
     * */
    public static Map<Id, SBQQ__Quote__c> getQuoteByBPId(Set<Id> ids){
        
        return new Map<Id,SBQQ__Quote__c>([SELECT Id, Name, SBQQ__Status__c, SBQQ__Type__c, SBQQ__ExpirationDate__c, SBQQ__StartDate__c
                                           FROM SBQQ__Quote__c 
                                           WHERE Billing_Profile__c	 IN:ids
                                           AND SBQQ__Opportunity2__r.StageName NOT IN: ConstantsUtil.OPPTY_LIST_STAGE 
                                           AND SBQQ__Status__c NOT IN: ConstantsUtil.LIST_STATUS]);
        
    }
    
    public static List<SBQQ__Quote__c> getQuotesByIds(Set<Id> ids){
        return [SELECT  Id, 
                        SBQQ__Type__c, 
                        Upgrade_Downgrade_Contract__c,
                        SBQQ__ExpirationDate__c,
                        SBQQ__Account__r.PreferredLanguage__c,
                        SBQQ__Account__c, 
                        SBQQ__Account__r.Customer_Number__c,
                        SBQQ__StartDate__c,
                        Name,
                        SBQQ__Primary__c,
                        Trial_CallId__c,
                        External_Id__c
                FROM    SBQQ__Quote__c 
                WHERE   Id IN :ids];
    } 
}