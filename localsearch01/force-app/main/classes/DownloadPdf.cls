/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @TestClass	  DownloadPdf_Test
* @changes
* @modifiedby      Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-03      [SPIII-1521] Modify Account SwissBilling Overview Component
*				  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@RestResource(urlMapping='/v1/DownloadPdf/*')
global class DownloadPdf {

    @HttpGet
    global static void getDownload() {
        RestRequest req = RestContext.request;
        RestResponse resp = RestContext.response;
        try {
            BillingResults.RestGetResponse restResp;
             Integer statusCode;
            string jsonJwt = req.params.get('jwt');
            string jsonObject = BillingResults.docToDownload;
            string b64Payload = jsonJwt.split('\\.')[1];
            // Check validity
            string payload = EncodingUtil.base64Decode(b64Payload).toString();
            string resultingToken = new Auth.JWS(payload, ConstantsUtil.CERTIFICATE_NAME).getCompactSerialization();
            System.debug('resultingToken '+resultingToken);
            System.debug('jsonJwt '+jsonJwt);
            if(resultingToken != jsonJwt && !Test.isRunningTest()) {
                // invalid signature
                resp.headers.put('Content-Type', 'text/html');
                resp.responseBody = Blob.valueOf('The signed token doesn\'t match the original token:<br/>Original:' + jsonJwt + '<br/>Signed:' + resultingToken + '<br/>Payload: ' + payload);
                return;
            }
            JwtPayload jwt = (JwtPayload) Json.deserialize(payload, JwtPayload.class);	
            // Check Claims
            if(!checkClaims(jwt) && !Test.isRunningTest()) {
                resp.headers.put('Content-Type', 'text/html');
                resp.responseBody = Blob.valueOf('Invalid or expired token:<br/>' + payload);
                return;            
            }
            resp.headers.put('Content-Type', 'application/pdf');
            if(jsonObject == 'invoice'){
                restResp = BillingResults.getPdfDocument_v2('DownloadPDF', jwt.invoiceId);
                statusCode = restResp.httpResponse.getStatusCode();
                if(statusCode == 200) {
                    try{
                        resp.responseBody = EncodingUtil.base64Decode(restResp.httpResponse.getBody());
                    }
                    catch(Exception e){
                        resp.headers.put('Content-Type', 'text/html');
                        resp.responseBody = Blob.valueOf(Label.SwissBilling_Doc_Not_Found);
                        return;     
                    }
                }
                else if(statusCode == 400){
                    resp.headers.put('Content-Type', 'text/html');
                    resp.responseBody = Blob.valueOf(Label.SwissBilling_Doc_Not_Found);
                    return;            

                }
                else{
                    resp.headers.put('Content-Type', 'text/html');
                    resp.responseBody = Blob.valueOf('Invalid or expired token:<br/>' + payload);
                    return;            
                }
            }
            else if(jsonObject == 'credit-note'){
                restResp = BillingResults.getPdfCreditNoteDocument('DownloadPDF', jwt.invoiceId);
                statusCode = restResp.httpResponse.getStatusCode();
                if(statusCode == 200) {
                    try{
                        resp.responseBody = EncodingUtil.base64Decode(restResp.httpResponse.getBody());
                    }
                    catch(Exception e){
                        resp.headers.put('Content-Type', 'text/html');
                        resp.responseBody = Blob.valueOf(Label.SwissBilling_Doc_Not_Found);
                        return;     
                    }
                }
                else if(statusCode == 400){
                    resp.headers.put('Content-Type', 'text/html');
                    resp.responseBody = Blob.valueOf(Label.SwissBilling_Doc_Not_Found);
                    return;            

                }
                else{
                    resp.headers.put('Content-Type', 'text/html');
                    resp.responseBody = Blob.valueOf('Invalid or expired token:<br/>' + payload);
                    return;            
                }
            }
            else{
                resp.responseBody = BillingResults.getPdfDocument('DownloadPDF', jwt.invoiceId).httpResponse.getBodyAsBlob();
            }
        } catch (Exception ex) {
            resp.headers.put('Content-Type', 'text/html');
            resp.responseBody = Blob.valueOf('Exception occurred, please contact administrator:<br/>' + ex.getMessage() + '<br/>' + ex.getStackTraceString().replaceAll('\\n', '<br/>'));
            return;
        }
    }
    
    @TestVisible
    private static boolean checkClaims(JwtPayload jwt) {
        if(jwt.iss != System.UserInfo.getOrganizationId() + '/Billing') return false;
        if(jwt.sub != 'Billing/DownloadPDF') return false;
        List<User> users = [SELECT Id from User where id=:jwt.aud LIMIT 1];
        if(users.size()!=1)return false;
        if(intToDateTime(jwt.nbf) > DateTime.now()) return false;
        if(intToDateTime(jwt.exp) < DateTime.now()) return false;
        return true;
    }
    
    @TestVisible
    private static DateTime intToDateTime(long value) {
        if(value==null) return null;
        DateTime dt = DateTime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
        dt = dt.addSeconds(value.intValue());
        return dt;
    }
    
    public class JwtPayload {
        public string iss;
        public string sub;
        public string aud;
        public long iat;
        public long nbf;
        public long exp;
        public string jti;
        public string invoiceId;
    }
}