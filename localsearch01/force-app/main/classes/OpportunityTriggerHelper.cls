/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Trigger Helper Class
* @TestClass      Test_OpportunityTrigger
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Esposito Antonio <antesposito@deloitte.it>
* @created        2019-11-22
* @systemLayer    Invocation
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     	Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-07-16      	SPIII-1080 - Set ASAP Start Date on Quote Lines when allowed if the Opportunity 
*					is closed won.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     	Luciano di Martino <ldimartino@deloitte.it>
* 2020-07-28      	SPIII-2046 - Validation on opportunity creation: the dmc can't create an
					Opportunity if the related Account is outside his territory.

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-4307 - New Customer Flag - change calculation logic:
				  setActivationDateOnAccount commented 
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-11-24
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘


* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     	Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-12-16      	SPIII-4683 - startDateValidation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*
*/
public without sharing  class OpportunityTriggerHelper {
    
    public static void checkAccountDuplicate(List<Opportunity> newItems){
        set<Id> accountSetId = new set<Id>();
        for(opportunity newopp : newItems){
            accountSetId.add(newopp.AccountId);
        }
        Map<Id,Account> accMap = SEL_Account.getAccountsById(accountSetId);
        List <Id> oppToDelete = new List<Id>();
        
        for(opportunity newopp : newItems){
            if(accMap.get(newopp.AccountId).isDuplicated__c && !accMap.get(newopp.AccountId).MarkedAsNotDuplicated__c){
                if((math.abs((newopp.CreatedDate.getTime()-accMap.get(newopp.AccountId).CreatedDate.getTime())/1000))<10){
                    oppToDelete.add(newopp.id);
                }else{
                    if(!Test.isRunningTest()) newopp.addError(Label.OppErrorDuplicateAccount);
                }
            }
        }
        if(oppToDelete.size()>0){
            Service_Duplicate_Management.deleteOpportunity(oppToDelete);
        }
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Set Activation Date on Account in order to calculate "New Customer" flag.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	newItems    Trigger.New trigger context variable
    * @input param    	oldItems    Trigger.Old trigger context variable
    * @return   		void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    /*public static void setActivationDateOnAccount(List<Opportunity> newItems, Map<Id, Opportunity> oldItems){

        List<String> validPricebookExternalIds = new List<String>{ConstantsUtil.DMC_PRICEBOOK, ConstantsUtil.ONEPRESENCE_PRICEBOOK};
        Set<Id> validPricebookIds = new Set<Id>();
        List<Pricebook2> validPricebooks = [SELECT Id FROM Pricebook2 WHERE ExternalId__c IN :validPricebookExternalIds];
        for(Pricebook2 pb : validPricebooks) validPricebookIds.add(pb.Id);

        List<Id> primaryQuotesId = new List<Id>();
        List<Id> validOppIds = new List<Id>();
        List<Opportunity> oppsToCheck = new List<Opportunity>();
        for(Opportunity currentOpp : newItems){
            if(ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON.equalsIgnoreCase(currentOpp.StageName) && !ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON.equalsIgnoreCase(oldItems.get(currentOpp.Id).StageName)){
                primaryQuotesId.add(currentOpp.SBQQ__PrimaryQuote__c);
                oppsToCheck.add(currentOpp);
            }
        }
        if(!primaryQuotesId.isEmpty() && !oppsToCheck.isEmpty() && !validPricebookIds.isEmpty()){
            Map<Id,SBQQ__Quote__c> oppToQuote = new Map<Id,SBQQ__Quote__c>();
            List<SBQQ__Quote__c> primaryQuotes = [SELECT Id, SBQQ__Type__c, SBQQ__Opportunity2__c, Contract_Signed_Date__c FROM SBQQ__Quote__c WHERE Id IN :primaryQuotesId];
            for(SBQQ__Quote__c currentQuote : primaryQuotes){
                oppToQuote.put(currentQuote.SBQQ__Opportunity2__c, currentQuote);
            }
            
            if(!oppToQuote.isEmpty()){
                for(Opportunity currentOpp : oppsToCheck){
                    Boolean isQuote = ConstantsUtil.QUOTE_TYPE_QUOTE.equalsIgnoreCase(oppToQuote.get(currentOpp.Id).SBQQ__Type__c);
                    Boolean isUpgrade = ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(oppToQuote.get(currentOpp.Id).SBQQ__Type__c);
                    Boolean isReplacement = ConstantsUtil.ORDER_TYPE_REPLACEMENT.equalsIgnoreCase(oppToQuote.get(currentOpp.Id).SBQQ__Type__c);
                    if( (isQuote || isUpgrade || isReplacement) && validPricebookIds.contains(currentOpp.Pricebook2Id)){
                        validOppIds.add(currentOpp.Id);
                    }
                }
                if(!validOppIds.isEmpty()){
                    Map<Id,Account> accsToUpdate = new Map<Id,Account>();
                    List<Opportunity> opps = [SELECT Id, Account.Last_Inactive_Date__c, Account.Activation_Date__c, Account.Active_Contracts__c, Account.Active_DMC_Contracts__c, AccountId, SambaMigration__c FROM Opportunity WHERE Id IN :validOppIds];
                    for(Opportunity currentOpp : opps){
                        if(String.isNotBlank(currentOpp.AccountId)){
                            if(currentOpp.Account.Last_Inactive_Date__c == null){
                                if(currentOpp.Account.Activation_Date__c != null){
                                    if(currentOpp.Account.Activation_Date__c.addMonths(12) < Date.today() && currentOpp.Account.Active_DMC_Contracts__c == 0){
                                        currentOpp.Account.Activation_Date__c = Date.today();
                                        accsToUpdate.put(currentOpp.AccountId, currentOpp.Account);
                                    }
                                }
                                else{
                                    currentOpp.Account.Activation_Date__c = Date.today();
                                    accsToUpdate.put(currentOpp.AccountId, currentOpp.Account);
                                }
                                
                            }
                            else if(currentOpp.Account.Last_Inactive_Date__c != null){
                                if(currentOpp.Account.Activation_Date__c != null){
                                    if(currentOpp.Account.Last_Inactive_Date__c.addMonths(12) < Date.today() && currentOpp.Account.Activation_Date__c.addMonths(12) < Date.today() && currentOpp.Account.Active_DMC_Contracts__c == 0){
                                        currentOpp.Account.Activation_Date__c = Date.today();
                                        accsToUpdate.put(currentOpp.AccountId, currentOpp.Account);
                                    }
                                }else{
                                    currentOpp.Account.Activation_Date__c = Date.today();
                                    accsToUpdate.put(currentOpp.AccountId, currentOpp.Account);
                                } 
                            }
                        }
                    }
                    if(!accsToUpdate.isEmpty()){
                        AccountTriggerHandler.disableTrigger = true;
                        update accsToUpdate.values();
                        AccountTriggerHandler.disableTrigger = false;
                    }
                }				                
            }
        }
    }
    */
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Validate Quote Line start date before proceding to close won the opportunity, according to
    * ASAP Allowed, Min Start Date and Max Start Date fields on Product.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	newItems    Trigger.New trigger context variable
    * @input param    	oldItems    Trigger.Old trigger context variable
    * @return   		void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void startDateValidation(List<Opportunity> newItems, Map<Id, Opportunity> oldItems){
        
        Map<Id, Opportunity> opportunityClosedWon = new Map<Id, Opportunity>();
        
        
        for(Opportunity currentOpty : newItems){
            
            Boolean isClosedWon = currentOpty.StageName.equalsIgnoreCase(ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON);
            Boolean wasNotClosedWon = !oldItems.get(currentOpty.Id).StageName.equalsIgnoreCase(ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON);
            Boolean stageChangedToClosedWon = isClosedWon && wasNotClosedWon;
            
            if(stageChangedToClosedWon) opportunityClosedWon.put(currentOpty.Id, currentOpty);
        }
        
        if(!opportunityClosedWon.isEmpty()){
            Map<Id, List<SBQQ__QuoteLine__c>> opportunityToQuoteLines = new Map<Id, List<SBQQ__QuoteLine__c>>();
            Map<Id, String> opportunityToProductName = new Map<Id, String>();
            Set<Id> opportunityToAddError = new Set<Id>();
            Map<Id, SBQQ__QuoteLine__c> quoteLinesToUpdate = new Map<Id, SBQQ__QuoteLine__c>();
            
            Map<Id, SBQQ__QuoteLine__c> parentQuoteLines = SEL_SBQQQuoteLine.getParentQuoteLinesByOpprtunityIds(opportunityClosedWon.keySet());
            if(!parentQuoteLines.isEmpty()){
                for(SBQQ__QuoteLine__c currentQuoteLine : parentQuoteLines.values()){
                    if(!opportunityToQuoteLines.containsKey(currentQuoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c)) opportunityToQuoteLines.put(currentQuoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c, new List<SBQQ__QuoteLine__c>{currentQuoteLine});
                    else opportunityToQuoteLines.get(currentQuoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c).add(currentQuoteLine);
                }
                for(Id currentOptyId : opportunityToQuoteLines.keySet()){
                    
                    List<SBQQ__QuoteLine__c> quoteLinesForOpty = opportunityToQuoteLines.get(currentOptyId);
                    
                    for(SBQQ__QuoteLine__c currentQuoteLine : quoteLinesForOpty){
                        Boolean isReplacement = ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(currentQuoteLine.SBQQ__Quote__r.SBQQ__Type__c);
                        Boolean isUpgrade = ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(currentQuoteLine.SBQQ__Quote__r.SBQQ__Type__c);
                        if(!currentQuoteLine.SBQQ__Product__r.ASAP_Allowed__c){
                            if(String.isBlank(currentQuoteLine.SBQQ__Product__r.Min_Start_Date__c)){
                                if(currentQuoteLine.SBQQ__StartDate__c < Date.today() || ( (isReplacement || isUpgrade) && currentQuoteLine.SBQQ__StartDate__c == Date.today() )){
                                    opportunityToAddError.add(currentQuoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c);
                                    opportunityToProductName.put(currentOptyId, currentQuoteLine.SBQQ__Product__r.Name);
                                    break;
                                }
                            }
                            else{
                                Date minStartDate = Date.today().addDays(Integer.valueOf(currentQuoteLine.SBQQ__Product__r.Min_Start_Date__c));
                                if(currentQuoteLine.SBQQ__StartDate__c < minStartDate || ( (isReplacement || isUpgrade) && currentQuoteLine.SBQQ__StartDate__c == Date.today() )){
                                    opportunityToAddError.add(currentQuoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c);
                                    opportunityToProductName.put(currentOptyId, currentQuoteLine.SBQQ__Product__r.Name);
                                    break;
                                }
                            }
                        }
                        else{
                            if(updateStartDate(currentQuoteLine)) quoteLinesToUpdate.put(currentQuoteLine.Id, currentQuoteLine);
                        }
                    }
                }
                
                for(Id errorOptyId : opportunityToAddError){
                    String productName = opportunityToProductName.get(errorOptyId);
                    String errorMessageToShow = Label.Alert_Message_Opportunity_Closure_StartDate.replace('%%product%%', productName);
                    if(!Test.isRunningTest()) opportunityClosedWon.get(errorOptyId).addError(errorMessageToShow);
                }
                if(!quoteLinesToUpdate.isEmpty()){
                    
                    List<SBQQ__QuoteLine__c> childrenQuoteLinesToUpdate = new List<SBQQ__QuoteLine__c>();
                    
                    Map<Id, List<SBQQ__QuoteLine__c>> quoteLinesHierarchyMap = new Map<Id, List<SBQQ__QuoteLine__c>>();
                    List<SBQQ__QuoteLine__c> childrenQuoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__StartDate__c,End_Date__c FROM SBQQ__QuoteLine__c WHERE SBQQ__RequiredBy__c IN :quoteLinesToUpdate.keySet()];
                    
                    for(SBQQ__QuoteLine__c currentChildrenQL : childrenQuoteLines){
                        if(!quoteLinesHierarchyMap.containsKey(currentChildrenQL.SBQQ__RequiredBy__c)) quoteLinesHierarchyMap.put(currentChildrenQL.SBQQ__RequiredBy__c, new List<SBQQ__QuoteLine__c>{currentChildrenQL});
                        else quoteLinesHierarchyMap.get(currentChildrenQL.SBQQ__RequiredBy__c).add(currentChildrenQL);
                    }
                    if(!quoteLinesHierarchyMap.isEmpty()){
                        for(Id quoteId : quoteLinesHierarchyMap.keySet()){
                            for(SBQQ__QuoteLine__c childrenQuoteLine : quoteLinesHierarchyMap.get(quoteId)){
                                childrenQuoteLine.SBQQ__StartDate__c = quoteLinesToUpdate.get(quoteId).SBQQ__StartDate__c;
                                childrenQuoteLine.End_Date__c = quoteLinesToUpdate.get(quoteId).End_Date__c;
                                childrenQuoteLinesToUpdate.add(childrenQuoteLine);
                            }
                        }
                    }
                    List<SBQQ__QuoteLine__c> allQuoteLines = new List<SBQQ__QuoteLine__c>();
                    allQuoteLines.addAll(quoteLinesToUpdate.values());
                    allQuoteLines.addAll(childrenQuoteLinesToUpdate);
                    QuoteLineTriggerHandler.disableTrigger = true;
                    update allQuoteLines;
                    QuoteLineTriggerHandler.disableTrigger = false;
                }
            }
        }
    }
    
    public static Boolean updateStartDate(SBQQ__QuoteLine__c quoteLine){
        
        Boolean qlToUpdate = false;
        Boolean isReplacement = ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quoteLine.SBQQ__Quote__r.SBQQ__Type__c);
		Boolean isUpgrade = ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(quoteLine.SBQQ__Quote__r.SBQQ__Type__c);
        
        Date minStartDate = Date.today().addDays(Integer.valueOf(quoteLine.SBQQ__Product__r.Min_Start_Date__c));
		if( (isReplacement || isUpgrade) && minStartDate == Date.today()) minStartDate = minStartDate.addDays(1);
        
        if(quoteLine.SBQQ__StartDate__c < minStartDate){
            qlToUpdate = true;
            quoteLine.SBQQ__StartDate__c = minStartDate;
            quoteLine.End_Date__c = minStartDate.addMonths(Integer.valueOf(quoteLine.Subscription_Term__c)).addDays(-1);
        }
        
        return qlToUpdate;
    }
    
    public static void setPricebook(List<Opportunity> newItems){
        
        List<User> currentUser = [SELECT Id, Sales_Channel__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(!currentUser.isEmpty()){
            String userSalesChannel = currentUser[0].Sales_Channel__c;
            if(String.isNotBlank(userSalesChannel)){
                
                List<String> userSalesChannelsWithQuotes = new List<String>();
                List<String> userSalesChannels = userSalesChannel.split(';');
                
                for(String currentSalesChannel : userSalesChannels){
                    userSalesChannelsWithQuotes.add('\''+currentSalesChannel+'\'');
                }
                String salesChannelsForQuery = String.join(userSalesChannelsWithQuotes,',');
            	SRV_PricebookAssignment.pricebookAssignment(newItems, salesChannelsForQuery);
            }
        }
    }
    
    public static void checkAccountTerritory(Map<Id, Opportunity> newItems){
        Set<Id> accountIds = new Set<Id>();
        Map<Id, List<Id>> accToOppsMap = new Map<Id, List<Id>>();
        User currentUser = [SELECT Id, Code__c FROM User WHERE Id = :UserInfo.getUserId()];
        for(Opportunity op : newItems.values()){
            if(String.isNotBlank(op.AccountId) && currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_DMC){
                accountIds.add(op.AccountId);
                if(!accToOppsMap.containsKey(op.AccountId)){
                    accToOppsMap.put(op.AccountId, new List<Id>{op.Id});
                }else{
                    accToOppsMap.get(op.AccountId).add(op.Id);
                }
            }
        }
        if(accountIds.size() > 0){
            Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id, OwnerId, (SELECT Id, UserId FROM AccountTeamMembers) 
                                                              FROM Account WHERE Id IN :accountIds]);
            if(accounts.values().size() > 0){
                for(Id accId : accounts.keySet()){
                    if(accToOppsMap.containsKey(accId) && accounts.get(accId).OwnerId != currentUser.Id){
                        Boolean okTeamMember = false;
                        if(accounts.get(accId).AccountTeamMembers.size() > 0){
                            for(AccountTeamMember member : accounts.get(accId).AccountTeamMembers){
                                system.debug('member.UserId: ' + member.UserId);
                                if(member.UserId == currentUser.Id){
                                    okTeamMember = true;
                                }
                            }
                        }
                        if(!okTeamMember){
                            for(Id oppId : accToOppsMap.get(accId)){
                                if(!Test.isRunningTest()) newItems.get(oppId).addError(Label.AccountOnTheOpportunityCheck);
                            }  
                        }
                    }
                }
            }
        }
    }
    
    public class CustomException extends Exception{}
}