@isTest
public class TestSetupUtilClass {
    
public static  list <Account>  createAccounts (Integer num){
        list <Account> Accounts = new list <Account>();
       for(Integer i = 0; i < num; i++){
           Account acc = new Account();
           acc.Name = 'XXX ' + i;
           acc.GoldenRecordID__c = 'GRID'+i;
           acc.POBox__c = '10';
           acc.P_O_Box_Zip_Postal_Code__c ='101';
           acc.P_O_Box_City__c ='dh';               
           Accounts.add( acc);
           System.Debug('Account '+ acc);            
       }
       insert Accounts;
       return [select id from account];
   }
 
    public static void createLeads (Integer num){
        list<lead> leads = new list <lead>();
       for(Integer i = 0; i < num; i++){
           Lead ld = new Lead();
           ld.LastName = 'XXX ' + i;
           ld.GoldenRecordID__c = 'GRID'+i;

          ld.City = 'dh';
          ld.State = 'bd';
          ld.Company = 'ITL';
          ld.Street = 'utt';
          ld.PostalCode = '1230';
          ld.Country = 'bd';  
           
           leads.add(ld);
           System.Debug('Lead '+ ld);            
       }
       insert leads;
   }
   public static void createPlaces (Integer num){
       list<Place__c> plList = new list <Place__c>();
       for(Integer i = 0; i < num; i++){
           Place__c  pl = new Place__c ();
           pl.LastName__c  = 'XXX ' + i;
           pl.GoldenRecordID__c = 'GRID'+i;
           pl.City__c = 'dh';
           pl.State__c  = 'bd';
           pl.Company__c  = 'ITL';
           pl.Street__c  = 'utt';
           pl.PostalCode__c  = '1230';
           pl.Country__c  = 'bd';  
           pl.PlaceID__c ='PL00'+i;
           plList.add( pl);
           System.Debug('Place '+ pl);            
       }
       insert plList;
   } 
    public static string createAccount (Integer i){
         Account acc = new Account();
                   acc.Name = 'XXX ' + i;
                   acc.GoldenRecordID__c = 'GRID'+i;
        
                   acc.POBox__c = '10';
                   acc.P_O_Box_Zip_Postal_Code__c ='101';
                   acc.P_O_Box_City__c ='dh';    
                   
                   insert acc;
                   System.Debug('Account '+ acc); 
        return acc.Id;
    }
    
        
        public static void createContracts (Integer num){
            list <contract> contracts = new list <contract>();
            list <account> accounts = createAccounts(num);
             for(Integer i = 0; i < num; i++){        
                    string accid=accounts[i].id;  
                   Contract  cn = new Contract ();
                   cn.AccountId  = accid;
                   cn.StartDate  = Date.today();
                   cn.Status ='Draft';
                   
                   contracts.add( cn);
                   System.Debug('Contract '+ cn);            
       }     
       insert contracts ; 
   }
   public static list<account> GenerateAccounts(integer num) {
       list<account> res = new list<account> ();
       for(Integer i = 0; i < num; i++){
        Account acc = new Account();
                   acc.Name = 'XXX ' + i;
                   acc.GoldenRecordID__c = 'GRID'+i;
        
                   acc.POBox__c = '10';
                   acc.P_O_Box_Zip_Postal_Code__c ='101';
                   acc.P_O_Box_City__c ='dh';    
                    System.Debug('Account '+ acc); 
                   res.add( acc);
                  }
        return res;
   }

 //Scope : massive datapopulation from a list of accounts
 
   public static map<string, map<string, object>> DatapopulationFromAccount( list<Account> Accounts, boolean migration){
         
          /* pricebook2 pricebook = new pricebook2();
           priceBook.name ='Standard Price Book';
           priceBook.description = 'Test';
           priceBook.isActive=true;
           insert priceBook;
           list<pricebook2> priceList = [select id, Name  from priceBook2 where name = 'Standard Price Book' limit 1 ];
           id priceBookId = priceList.isEmpty() ? null : priceList[0].id;*/
            id priceBookId = Test.getStandardPricebookId();
           map<string, map<string, object>> res = new map<string, map<string, object>>();
           map<string, object> Optymap = new   map<string, object>();
           map<string, object> BillingProfileMap = new   map<string, object>();
           map<string, object> PrimaryContactMap = new   map<string, object>();
           list<opportunity> insertOptyList = new list<opportunity>();
           list<Billing_Profile__c> listBillingProfiles = new list <Billing_Profile__c>();
           list<contact> PrimaryContacts = new list <contact>();
           integer i = 0 ;
           for(account a : Accounts){             
              PrimaryContacts.add(ContactGenaratorByAccount(a)); 
              PrimaryContactMap.put(a.id,PrimaryContacts[i]);
              listBillingProfiles.add(BillingProfileGeneratorByAccount(a));
              BillingProfileMap.put(a.id,listBillingProfiles[i]); 
              date closeDate = system.today().addDays(i);              
              insertOptyList.add(OpportunityGeneratorByAccount(a,closeDate,priceBookId ));
              Optymap.put(a.id,insertOptyList[i]);
              i++;
           }
           list<Sobject> insertList = new list<Sobject>();
           // insertList.addall(listBillingProfiles);
           insertList.addall(PrimaryContacts);
           insertList.addall(insertOptyList);
           insert insertList;
           insert listBillingProfiles;//first insert contact then billing profile to get the key existing
           res.put('Opportunity', Optymap);
           res.put('BillingProfile',BillingProfileMap);
           res.put('PrimaryContact',PrimaryContactMap);
           list <SBQQ__Quote__c> testQuotes = QuoteGenerator (res.get('PrimaryContact') ,res.get('Opportunity'),res.get('BillingProfile'), res.get('BillingProfile').keyset(),priceBookId, migration);
           createProducts();
           quoteLineGenerator(testQuotes,createPlacesByAccount(Accounts));
           system.debug('inserted element: ' + Json.serialize(insertList));
           return res;

       }
       public static contact ContactGenaratorByAccount  (Account a){
            contact TempPrimaryContact = new contact();
              TempPrimaryContact.Email ='test@test.it';
              TempPrimaryContact.LastName = a.name;
              TempPrimaryContact.MailingCity = 'Roma';
              TempPrimaryContact.MailingCountry =   'Italy';
              TempPrimaryContact.MailingPostalCode = '00100';
              TempPrimaryContact.MailingState = 'Lazio';
              TempPrimaryContact.MailingStreet = 'via domodossola 12 ';             
              TempPrimaryContact.Phone = '+39123123123123';
              TempPrimaryContact.Salutation =   'Mr.';
              TempPrimaryContact.Language__c = 'German';
              TempPrimaryContact.AccountId = a.id;
              TempPrimaryContact.GoldenRecordID__c='C'+a.GoldenRecordID__c;
              return TempPrimaryContact;
       }
       public static Billing_Profile__c BillingProfileGeneratorByAccount (Account a){
              Billing_Profile__c TempBilling = new Billing_Profile__c();
              TempBilling.Customer__c=a.id;
              TempBilling.Billing_City__c = 'Roma';
              TempBilling.Billing_Country__c = 'Italia';
              TempBilling.Billing_Postal_Code__c = '00100';
              TempBilling.Billing_State__c = 'Lazio';
              TempBilling.Billing_Street__c = 'Via ' + a.name;
              TempBilling.Name = 'Billing_proFile_'+ a.name;
           	  TempBilling.Billing_Name__c = 'Billing_name_' + a.name ;
              TempBilling.Billing_Language__c = 'Italian';
              TempBilling.Billing_Contact__r = new contact(GoldenRecordID__c='C'+a.GoldenRecordID__c);
             return TempBilling;
       }
       public static opportunity OpportunityGeneratorByAccount (Account a, date CloseDate, id priceBookId){
           opportunity opty = new opportunity();
           Opty.ExternalOPTY__c = 'Test'+a.name;
           Opty.Pricebook2Id = priceBookId;
           Opty.StageName = 'Proposal';
           Opty.CloseDate = CloseDate; 
           Opty.AccountId =a.id;
           opty.name='Test'+a.name;
           return opty;
       }
       public static  list<SBQQ__Quote__c> QuoteGenerator (map<string, object> PrimaryMap, map<string, object> OpptyMap,map<string, object> BillingMap, set<string> AccIds,id pricebookId, boolean migration){
           
           id priceBook = pricebookId!=null ? pricebookId : [select id, Name  from priceBook2 where name = 'Standard Price Book' limit 1 ].id;
           list<SBQQ__Quote__c> QuoteList= new list<SBQQ__Quote__c> ();
           integer i = 0;
           for (string accId : AccIds){
               if(math.mod(i, 2) == 0){
               SBQQ__Quote__c TempQuote = new SBQQ__Quote__c();
               date TempDate = system.Today().addDays(-1);
               TempQuote.ContractSAMBA_Key__c = 'Samba'+i;
               TempQuote.SL_Migration__c = migration;
               TempQuote.SBQQ__BillingCountry__c = 'Italy';
               TempQuote.SBQQ__BillingName__c = 'Billing_'+i;
               TempQuote.SBQQ__EndDate__c =TempDate.addDays(2+i);
               TempQuote.SBQQ__ExpirationDate__c =TempDate.addDays(2*i);
               TempQuote.SBQQ__Key__c ='key'+i;
               TempQuote.SBQQ__Opportunity2__c =((opportunity)OpptyMap.get(accId)).id;
               TempQuote.SBQQ__PricebookId__c =priceBook;
               TempQuote.SBQQ__Primary__c = true;
               TempQuote.SBQQ__PrimaryContact__c =((contact)PrimaryMap.get(accId)).id;
               TempQuote.SBQQ__ShippingName__c='Test'+i;
               TempQuote.SBQQ__ShippingPostalCode__c='00100';
               TempQuote.SBQQ__ShippingState__c='Italy';
               TempQuote.SBQQ__ShippingStreet__c='via salaria n'+i;
               TempQuote.SBQQ__StartDate__c=TempDate.addDays(i);
               TempQuote.SBQQ__Status__c='draft';
               TempQuote.SBQQ__Account__c = accId;
               TempQuote.Billing_Profile__c  =((Billing_Profile__c)BillingMap.get(accId)).id;
               system.debug('inserting quote: '+ TempQuote);
               QuoteList.add(TempQuote);
               }else{
                SBQQ__Quote__c TempQuoteErr = new SBQQ__Quote__c();
               date TempDate = system.Today().addDays(-2);
               TempQuoteErr.ContractSAMBA_Key__c = 'baerr'+i;
               TempQuoteErr.SL_Migration__c = migration;
               TempQuoteErr.SBQQ__BillingCountry__c = 'Italy';
               TempQuoteErr.SBQQ__BillingName__c = 'Billing_err'+i;
               TempQuoteErr.SBQQ__EndDate__c =TempDate.addDays(2+i);
               TempQuoteErr.SBQQ__ExpirationDate__c =TempDate.addDays(2*i);
               TempQuoteErr.SBQQ__Key__c ='keyrr'+i;
               TempQuoteErr.SBQQ__Opportunity2__c =((opportunity)OpptyMap.get(accId)).id;
               TempQuoteErr.SBQQ__PricebookId__c =priceBook;
               TempQuoteErr.SBQQ__Primary__c = true;
               TempQuoteErr.SBQQ__ShippingName__c='Testrr'+i;
               TempQuoteErr.SBQQ__ShippingPostalCode__c='00100';
               TempQuoteErr.SBQQ__ShippingState__c='Italy';
               TempQuoteErr.SBQQ__ShippingStreet__c='via salaria n'+i;
               TempQuoteErr.SBQQ__StartDate__c=TempDate.addDays(i);
               TempQuoteErr.SBQQ__Status__c='draft';
               TempQuoteErr.SBQQ__Account__c = accId;
               system.debug('inserting quote: '+ TempQuoteErr);
               QuoteList.add(TempQuoteErr);
               }
                i++;              
           }
           system.debug('inserted Quote: ' + Json.serialize(QuoteList));
           
           insert QuoteList;
           return QuoteList;

       }
        public static void quoteLineGenerator (list <SBQQ__Quote__c> Quotelist,  map<string, place__C> PlaceMap){
          list<SBQQ__QuoteLine__c> QuoteLines =  new list <SBQQ__QuoteLine__c>();
            integer i = 0;
           for (SBQQ__Quote__c quote : Quotelist){               
               SBQQ__QuoteLine__c tempQuoteLine = new SBQQ__QuoteLine__c();
               tempQuoteLine.Place__r=PlaceMap.get(quote.SBQQ__Account__c);
               tempQuoteLine.SBQQ__StartDate__c = quote.SBQQ__StartDate__c;
               tempQuoteLine.SBQQ__Bundled__c= false;
               tempQuoteLine.SBQQ__Product__r = new product2(ExternalKey_ProductCode__c ='SLS001');
               tempQuoteLine.ExternalQL__c = 'test'+i; 
               tempQuoteLine.SBQQ__Quote__c = quote.id;
               i++;
               QuoteLines.add(tempQuoteLine);
              }
         Test.startTest();       
        insert QuoteLines;
         Test.stopTest();        
       }
       @isTest
	 public static void createProducts() {
        List<Product2> listProds = new List<Product2>();
        for(Integer i = 0; i < 3; i++) {
         Product2 pr = new Product2();
           pr.Name = 'XXX ' + i;
           pr.ProductCode = 'SLS00'+i;
           pr.IsActive=True;
           pr.SBQQ__ConfigurationType__c='Allowed';
           pr.SBQQ__ConfigurationEvent__c='Always';
           pr.SBQQ__QuantityEditable__c=True;
           pr.ExternalKey_ProductCode__c= 'SLS00'+i;
           pr.SBQQ__NonDiscountable__c=False;
           pr.SBQQ__SubscriptionType__c='Renewable'; 
           pr.SBQQ__SubscriptionPricing__c='Fixed Price';
           pr.SBQQ__SubscriptionTerm__c=12; 
           
            listProds.add(pr);
        }
    insert listProds;
    }
     public static map<string, place__C>  createPlacesByAccount (list<account> insertedAccounts){
       list<Place__c> plList = new list <Place__c>();
       map<string, place__C> placeMap = new map<string, place__C>();
       integer i=0;
       for(Account a :  insertedAccounts){
           Place__c  pl = new Place__c ();
           pl.LastName__c  = 'XXX ' + i;
           pl.GoldenRecordID__c = 'GRID'+i;
           pl.City__c = 'dh';
           pl.State__c  = 'bd';
           pl.Company__c  = 'ITL';
           pl.Street__c  = 'utt';
           pl.PostalCode__c  = '1230';
           pl.Country__c  = 'bd';  
           pl.PlaceID__c ='PL00'+i;
           pl.Account__c =a.id;
           plList.add( pl);
           placeMap.put(a.id, pl);
           i++;
           System.Debug('Place '+ pl);            
       }
       insert plList;
       return placeMap;
   } 
 }