/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class Batch_EvaluateUser2TerritoryAssignment implements Database.batchable<sObject>, Database.Stateful {
    
    global List<Id> staticValues = new List<Id>();
    global Map <Id, Id> accDMCTerr = new  Map <Id, Id> ();
    global Map <Id, Id> accSMATerr = new  Map <Id, Id> ();
    global Map <Id, Id> accSDITerr = new  Map <Id, Id> ();
    global Map<Id, Map<String, String>> userinTerMap = new  Map<Id, Map<String, String>>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        
        //OAVERSANO 20191007 Enhancement #33 -- START
        //Map<Id, Map<String, String>> userinTerMap = new  Map<Id, Map<String, String>>();
        //OAVERSANO 20191007 Enhancement #33 -- END
        //OAVERSANO 20191007 Enhancement #33 -- START
        for(UserTerritory2Association usinT : [select id, Territory2Id,UserId, RoleInTerritory2 
                                               from UserTerritory2Association 
                                               where id in: staticValues order by SystemModstamp DESC]){
                                                   
                                                   if(!userinTerMap.containsKey(usinT.Territory2Id )){
                                                       userinTerMap.put(usinT.Territory2Id, new Map<String, String>{usinT.RoleInTerritory2 => usinT.UserId});
                                                   }
                                                   else{
                                                       if(!userinTerMap.get(usinT.Territory2Id).containsKey(usinT.RoleInTerritory2))
                                                       {
                                                           Map<String, String> idUsTerMap = new Map<String, String>{usinT.RoleInTerritory2 => usinT.UserId};
                                                               userinTerMap.get(usinT.Territory2Id).putAll(idUsTerMap);  
                                                       }
                                                   }
                                                   
                                                   //OAVERSANO 20191007 Enhancement #33 -- END
                                               }
        
        List<Id> listAcctsIds = new List<Id>();
        for(ObjectTerritory2Association u: [SELECT ObjectId, Territory2Id,Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2.id FROM ObjectTerritory2Association WHERE 
                                            (Territory2Id in : userinTerMap.keySet() OR Territory2.ParentTerritory2Id in : userinTerMap.keySet() OR Territory2.ParentTerritory2.ParentTerritory2id in : userinTerMap.keySet())
                                            AND Object.Type =: ConstantsUtil.TERRITORY_OBJECT_TYPE]){
                                                listAcctsIds.add(u.ObjectId);
                                                // check 3rd level territory
                                                if(u.Territory2.ParentTerritory2.ParentTerritory2 != null){
                                                    accDMCTerr.put(u.ObjectId, u.Territory2Id);
                                                    accSMATerr.put(u.ObjectId, u.Territory2.ParentTerritory2.id);
                                                    accSDITerr.put(u.ObjectId, u.Territory2.ParentTerritory2.ParentTerritory2.id);       
                                                }
                                                // check 2rd level territory
                                                else if(u.Territory2.ParentTerritory2 != null){
                                                    accSMATerr.put(u.ObjectId, u.Territory2Id);
                                                    accSDITerr.put(u.ObjectId, u.Territory2.ParentTerritory2.id);
                                                }    
                                                else{
                                                    accSDITerr.put(u.ObjectId, u.Territory2Id);
                                                }    
                                            }
        String statusAcc = ConstantsUtil.ACCOUNT_STATUS_CLOSED;   
        String query = 'SELECT RegionalDirector__c, RegionalLeader__c, Sales_manager_substitute__c FROM Account WHERE Id IN: listAcctsIds AND Status__c !=: statusAcc';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Account> scope ){
        
        Set<Account> accs = new Set<Account>();
        System.debug('SDI to be update on record --> '+accSDITerr);
        for(Account a : scope){
           /* if(userinTerMap.containsKey(accDMCTerr.get(a.id))){
                if(userinTerMap.get(accDMCTerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_DMC)){
                    a.SalesUser__c = userinTerMap.get(accDMCTerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_DMC);
                }
                accs.add(a);
            }*/ //SPIII 1212
            if(userinTerMap.containsKey(accSDITerr.get(a.id))){
                if(userinTerMap.get(accSDITerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)){
                    a.RegionalDirector__c = userinTerMap.get(accSDITerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);  
                }  
                accs.add(a);
            }
            if(userinTerMap.containsKey(accSMATerr.get(a.id))){
                if(userinTerMap.get(accSMATerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP)){
                    a.Sales_manager_substitute__c = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP);  
                }
                if(userinTerMap.get(accSMATerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)){
                    a.RegionalLeader__c = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                }
                accs.add(a);
            }
            
        }
        
        system.debug('accounts to update : '+JSON.serializePretty(accs));//to remove
        List<Account> accToUpdate = new List<Account>();
        accToUpdate.addAll(accs);
        if(accToUpdate.size()>0){
            AccountTriggerHandler.disableTrigger = true;
            update accToUpdate;
            AccountTriggerHandler.disableTrigger = false;
        } 
    }     
    
    global void finish(Database.BatchableContext info){     
    }
}