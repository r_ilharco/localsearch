global class SLMigrationQuoteCopyGen_BatchSchedule implements Database.Batchable<sObject>,  Schedulable{

   global final String Query;
   global integer Days =40;
   global integer batchsize = 100; 
   global static string INSERTINGERROR = ConstantsUtil.quote_copy_insert_error;
   global final list<string> ParentProductList =new list<string>{'SLT001', 'SLS001'};
  
    private final string OPTY_STAGENAME_CLOSED_WON=ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
    private final string  OPTY_STAGENAME_CLOSED_LOST =ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST;
    // quote status
    private final string QUOTE_STATUS_ACCEPTED = ConstantsUtil.QUOTE_STATUS_ACCEPTED  ;    
    private final string  QUOTE_STAGENAME_REJECTED =ConstantsUtil.QUOTE_STAGENAME_REJECTED;
    private final string  QUOTE_STAGENAME_ACCEPTED = ConstantsUtil.QUOTE_STAGENAME_ACCEPTED;
    private final string  QUOTE_STAGENAME_DENIED =ConstantsUtil.QUOTE_STAGENAME_DENIED;
   global SLMigrationQuoteCopyGen_BatchSchedule (String q){
      Query=q; 
   }
   global SLMigrationQuoteCopyGen_BatchSchedule (  integer batchsize){
     this.batchsize = batchsize;
    }
    global SLMigrationQuoteCopyGen_BatchSchedule ( integer Days, integer batchsize){
     this(batchsize);
      this.batchsize = batchsize;
    }
    

    global Database.QueryLocator start(Database.BatchableContext BC){
         system.debug(' SLMigrationQuoteCopyGen_BatchSchedule - start ');
         Database.QueryLocator  query = Database.getQueryLocator([SELECT SBQQ__Account__c,SBQQ__EndDate__c,SBQQ__Opportunity2__c, SBQQ__Opportunity2__r.name ,SBQQ__PrimaryContact__c,
                                        (select id,SBQQ__Product__r.ProductCode from SBQQ__LineItems__r where SBQQ__Product__r.ProductCode in :ParentProductList),Billing_Profile__c,  SBQQ__StartDate__c,  SBQQ__Opportunity2__r.StageName FROM SBQQ__Quote__c
                                        where  SBQQ__Quote__c.InActivation__c=false and SBQQ__StartDate__c <= :system.today().adddays(Days) and  SBQQ__StartDate__c >= :system.today() and SL_Migration__c=true and SBQQ__Opportunity2__r.StageName != :OPTY_STAGENAME_CLOSED_LOST
                                         and SBQQ__Opportunity2__r.StageName != :OPTY_STAGENAME_CLOSED_WON and SBQQ__Status__c != :QUOTE_STAGENAME_ACCEPTED and  SBQQ__Status__c != :QUOTE_STAGENAME_REJECTED and  SBQQ__Status__c != :QUOTE_STAGENAME_DENIED  ]);
     
        return query;
    }
    //if scheduled from apex all parameters can be set othewise it takes the default
     global void execute(SchedulableContext sc)   {
       
        database.executebatch(this,batchsize);
    }
    
  global void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope){
       system.debug('@@@@@@ records: '+ scope);
      list <quote_copy__c> quCopyList = new list<quote_copy__c>();
      map<id, SBQQ__Quote__c>  quMap = new map<id, SBQQ__Quote__c> ();
     
      for (SBQQ__Quote__c qu : scope){
          quote_copy__c tempCopy = new quote_copy__c();
          tempCopy.Billing_Profilecopy__c = qu.Billing_Profile__c;
          tempCopy.SBQQ_StartDatecopy__c = qu.SBQQ__StartDate__c;
          tempCopy.SBQQ_Accountcopy__c = qu.SBQQ__Account__c;
          tempCopy.SBQQ_EndDatecopy__c = qu.SBQQ__EndDate__c;
          tempCopy.SBQQ_Opportunity2copy__c  = qu.SBQQ__Opportunity2__c;
          tempCopy.SBQQ_Opportunity_Statuscopy__c  = qu.SBQQ__Opportunity2__r.StageName;
          tempCopy.SBQQ_PrimaryContactcopy__c  = qu.SBQQ__PrimaryContact__c;
          tempCopy.quoteline_list__c=quotelineParentFinder(qu.SBQQ__LineItems__r);
          tempCopy.quote__c = qu.id;
          quCopyList.add(tempCopy);
          qu.InActivation__c=true;
          quMap.put(qu.id, qu);
      }
         system.debug('CopyList : '+ quCopyList);
        database.saveResult[] saveRes =  database.insert(quCopyList, false);
       system.debug('quCopyList save res: '+ saveRes);
        list <quote_copy__c> quoteCopySavelist = new list<quote_copy__c>();
        list<id> insertedcopyID = new list<id>();
        list<Log__c> Errors = new list<Log__c>();
        for (database.saveResult sv : saveRes ){
            if (sv.isSuccess()){
                insertedcopyID.add(sv.getId());
            }else {
              Log__c  log = new Log__c(
                    Name = insertingError,
                    Communication_Ok__c = false,
                    Error__c = json.serialize(sv.getErrors())
                );
                Errors.add(log);
            }
            
        }
        if (!Errors.isEmpty()){
            system.debug('Errors: ' + Errors);
            insert Errors;
        }

        if (!insertedcopyID.isEmpty())   
            quoteCopySavelist = [select SBQQ_Opportunity2copy__c, quote__c from  quote_copy__c where id IN :insertedcopyID];
       list <SBQQ__Quote__c>quoteSavelist = new list <SBQQ__Quote__c>();
        if (!quoteCopySavelist.isEmpty()) {
          
            for(quote_copy__c q :quoteCopySavelist ){
              SBQQ__Quote__c tempQ =  new SBQQ__Quote__c();
              tempQ.id = q.quote__c;
              tempQ.InActivation__c = true;
              quoteSavelist.add(tempQ);
            }
        }
        if (!quoteSavelist.isEmpty()){
          system.debug('quoteSavelist : '+ quoteSavelist);
            update quoteSavelist;
        }
   
    }

   global void finish(Database.BatchableContext BC){
         system.debug('SLMigrationQuoteCopyGen_BatchSchedule Batch - Ended');
   }
   private string quotelineParentFinder(list<SBQQ__Quoteline__c> inputList){
       list<string> resList=new list <string>();
       
       for (SBQQ__Quoteline__c q: inputList)
       resList.add(q.id);
       return string.join(resList, ':'); 
   }
}