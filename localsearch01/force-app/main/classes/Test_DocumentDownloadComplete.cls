@isTest
public class Test_DocumentDownloadComplete {
    
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.generateAccounts('Account Name Test', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Contact LastName Test', 1)[0];
        insert contact;
        Opportunity opty = Test_DataFactory.generateOpportunity('Opportnity Name Test', account.Id);
        insert opty;
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opty.Id LIMIT 1];
        quote.SBQQ__SalesRep__c = UserInfo.getUserId();
        update quote;
        
        Document document = new Document();
        document.Name = 'Document Name Test';
        document.Body = Blob.valueOf('Document Body Test');
        document.FolderId = UserInfo.getUserId();
        insert document;
        
        SBQQ__QuoteTemplate__c quoteTemplate = new SBQQ__QuoteTemplate__c();
        quoteTemplate.Name = 'Template Name Test';
        insert quoteTemplate;
        
        SBQQ__QuoteDocument__c quoteDocument = Test_DataFactory.generateQuoteDocument(quote.Id);
        quoteDocument.SBQQ__QuoteTemplate__c = quoteTemplate.Id;
        quoteDocument.SBQQ__DocumentId__c = document.Id;
        insert quoteDocument;
        
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = document.Name;
        contentVersion.PathOnClient = document.Name;
        contentVersion.VersionData = document.Body;
        contentVersion.IsMajorVersion = true;
        insert contentVersion;
        
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id LIMIT 1].ContentDocumentId;
        
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
        
        ContentDocumentLink contentDocumentLink2QuoteDocument = new ContentDocumentLink();
        contentDocumentLink2QuoteDocument.LinkedEntityId = quoteDocument.Id;
        contentDocumentLink2QuoteDocument.ContentDocumentId = contentDocumentId;
        contentDocumentLink2QuoteDocument.shareType = 'V';
        ContentDocumentLink contentDocumentLink2Quote = new ContentDocumentLink();
        contentDocumentLink2Quote.LinkedEntityId = quote.Id;
        contentDocumentLink2Quote.ContentDocumentId = contentDocumentId;
        contentDocumentLink2Quote.shareType = 'V';
        
        contentDocumentLinks.add(contentDocumentLink2QuoteDocument);
        contentDocumentLinks.add(contentDocumentLink2Quote);
        insert contentDocumentLinks;
        
        ContentDistribution contentDistribution = new ContentDistribution();
        contentDistribution.Name = contentVersion.Title;
        contentDistribution.ContentVersionId = contentVersion.Id;
        contentDistribution.PreferencesNotifyOnVisit = false;
        contentDistribution.PreferencesNotifyRndtnComplete = false;
        contentDistribution.PreferencesAllowViewInBrowser = true;
        contentDistribution.PreferencesLinkLatestVersion = true;
        contentDistribution.PreferencesPasswordRequired = false;
        contentDistribution.PreferencesAllowOriginalDownload = true;
        contentDistribution.PreferencesAllowOriginalDownload = false;
        insert contentDistribution;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_validationFailMissingFields(){
        
        String jsonBodyMissingQuote = '{"quoteId":"","quoteDocumentId":"quoteDocumentId","signatureDate":"signatureDate","distributionPublicUrl":"distributionPublicUrl"}';
        DocumentDownloadComplete.DocumentDownloadCompleteRequest requestDoc = new DocumentDownloadComplete.DocumentDownloadCompleteRequest('quoteId','quoteDocumentId','signatureDate','distributionPublicurl');
        Test.startTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();             
            req.requestURI = '/services/apexrest/document-download-complete';
            req.requestBody = Blob.valueOf(jsonBodyMissingQuote);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            DocumentDownloadComplete.signedDocumentReceived();
        	
        	System.assertEquals(400, res.statusCode);
        Test.stopTest();
    }
    
    @isTest
    public static void test_validationFailInvalidQuote(){
        
        String jsonBodyMissingQuote = '{"quoteId":"quoteId","quoteDocumentId":"quoteDocumentId","signatureDate":"signatureDate","distributionPublicUrl":"distributionPublicUrl"}';
        
        Test.startTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();             
            req.requestURI = '/services/apexrest/document-download-complete';
            req.requestBody = Blob.valueOf(jsonBodyMissingQuote);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            DocumentDownloadComplete.signedDocumentReceived();
        	
        	System.assertEquals(400, res.statusCode);
        Test.stopTest();
    }
    
    @isTest
    public static void test_validationFailInvalidQuoteDoc(){
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        
        String jsonBodyMissingQuote = '{"quoteId":"'+quote.Id+'","quoteDocumentId":"quoteDocumentId","signatureDate":"signatureDate","distributionPublicUrl":"distributionPublicUrl"}';
        
        Test.startTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();             
            req.requestURI = '/services/apexrest/document-download-complete';
            req.requestBody = Blob.valueOf(jsonBodyMissingQuote);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            DocumentDownloadComplete.signedDocumentReceived();
        	
        	System.assertEquals(400, res.statusCode);
        Test.stopTest();
    }
    
    @isTest
    public static void test_validJSON(){
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id FROM SBQQ__QuoteDocument__c LIMIT 1];
        
        String jsonBodyMissingQuote = '{"quoteId":"'+quote.Id+'","quoteDocumentId":"'+quoteDocument.Id+'","signatureDate":"2020-09-24T15:00:00.000Z","distributionPublicUrl":"distributionPublicUrl"}';
        
        Test.startTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();             
            req.requestURI = '/services/apexrest/document-download-complete';
            req.requestBody = Blob.valueOf(jsonBodyMissingQuote);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            DocumentDownloadComplete.signedDocumentReceived();
        Test.stopTest();
    }
}