global class connectPlaces implements Database.Batchable<SObject>, Database.Stateful  {
    
    global Database.QueryLocator start(Database.BatchableContext BC) { //Id='a0q1X000000jBzzQAE' AND  
		return Database.getQueryLocator([select Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate, Account__c, City__c, Company__c, Country__c, DataSource__c, Email__c, FirstName__c, GoldenRecordID__c, LastName__c, Lead__c, MergePlaces__c, MobilePhone__c, Nx_Amount__c, Nx_RefundStatus__c, Phone__c, PlaceID__c, PlaceIdAliases__c, PlaceStatus__c, Place_Type__c, PostalCode__c, Product_Information__c, QuoteItemId__c, SLmig_AmountCategory__c, SLmig_AmountLocation__c, SLmig_Related_LCMcomplex__c, SLmig_StartDate__c, SambaContract__c, SearchField_Address__c, SearchField_Name__c, SearchField_PhoneEmail__c, State__c, Status__c, Street__c, Title__c, Type__c, View_on_local_ch__c, View_on_search_ch__c, Website__c, nx_Paid__c, SL_Migration__c from Place__c where Account__c = null and Lead__c = null]);
	}   
    
    
	global void execute(Database.BatchableContext BC, List<Place__c> scope) {
        
        ATL_PlaceTriggerHelper.managePlace_Lead (scope,null);                      
        
  	}
    
    
	global void finish(Database.BatchableContext BC) {
        
    }
}