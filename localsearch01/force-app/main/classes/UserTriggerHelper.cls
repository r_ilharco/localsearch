/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* In this class is located the ADFS User Territory Management 
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created		  2019-12-11
* @systemLayer    Trigger
* @TestClass 	  Test_StandardUserHandler
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*
* @changes
* @modifiedby      Gennaro Casola
* 2019-12-11      SF2-422
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*
* @modifiedby      Gennaro Casola
* 2020-06-19      SPIII-1183 - Modify ADFS Logic based on Territory Model - 
*				  They have been cutted off the third-level territories from the territory model, 
*				  and so It have been included the DMCs, with the same logics of the SMAs, 
*				  in the Automatic User Territory Management throught the ADFS.
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*		  
* @changes
* @modifiedby      Gennaro Casola <gcasola@deloitte.it>
* 2020-06-30      [SPIII-1149] TERRITORY DMC ASSOCIATION
*				  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class UserTriggerHelper {
    public static Boolean skipDML = false;
    
    public static void userTerritoryConfiguration(Map<Id, SObject> newItems, Map<Id, SObject> oldItems, ConfigurationData cd){
        Map<Id, User> newMap = (Map<Id, User>) newItems;
        Set<String> territoryManagementUserCodesSet = new Set<String>{ConstantsUtil.ROLE_IN_TERRITORY_SDI,ConstantsUtil.ROLE_IN_TERRITORY_SMA,ConstantsUtil.ROLE_IN_TERRITORY_DMC};
        //UserTerritoryConfiguration utConfiguration = cd.utc;
        UTConfigurationData data = cd.utcData;
        
        prepareUserTerritoryConfiguration(cd, (Map<Id,User>)newItems);
        
        List<UserTerritory2Association> ut2associationsToUpsert = new List<UserTerritory2Association>();
        List<UserTerritory2Association> ut2associationsToDelete = new List<UserTerritory2Association>();
        
        List<GroupMember> groupMembersToUpsert = new List<GroupMember>();
        List<GroupMember> groupMembersToDelete = new List<GroupMember>();
        
        data.ut2associationsToUpsert = ut2associationsToUpsert;
        data.ut2associationsToDelete = ut2associationsToDelete;
        data.groupMembersToUpsert = groupMembersToUpsert;
        data.groupMembersToDelete = groupMembersToDelete;
        data.groupMembersToUpsertMap = new Map<Id, Set<Id>>();
        data.groupMembersToDeleteMap = new Map<Id, Set<Id>>();
        
        for(User u : newMap.values()){
            User oldU = null;
            if(!Trigger.isInsert && oldItems.containsKey(u.Id))
                oldU = (User)oldItems.get(u.Id);
            
            updateUserTerritoryConfiguration(u, oldU, cd);
        }
        
        System.debug('>>>data.ut2associationsToDelete: ' + data.ut2associationsToDelete);
        
        if(data.ut2associationsToDelete.size() > 0 && !skipDML)
            delete data.ut2associationsToDelete;
        
        System.debug('>>>data.ut2associationsToUpsert: ' + data.ut2associationsToUpsert);
        
        if(data.ut2associationsToUpsert.size() > 0 && !skipDML)
            upsert data.ut2associationsToUpsert;
        
        System.debug('>>>data.groupMembersToDelete:' + data.groupMembersToDelete);
        
        if(data.groupMembersToDelete.size() > 0 && !skipDML)
            delete data.groupMembersToDelete;
        
        System.debug('>>>data.groupMembersToUpsert:' + data.groupMembersToUpsert);
        
        if(data.groupMembersToUpsert.size() > 0 && !skipDML)
            upsert data.groupMembersToUpsert;
        
        skipDML = true;
    }
    
    @TestVisible
    private static void updateUserTerritoryConfiguration(User newU, User oldU, ConfigurationData cd){
        UserConfiguration uc = cd.uc;
        UserConfigurationData ucData = cd.ucData;
        UserTerritoryConfiguration utc = cd.utc;
        UTConfigurationData utcData = cd.utcData;
        
        String utcTerritoryDeveloperName;
        String utcPermissionSets;
        String utcGroups;
		UserTerritoryConfiguration__mdt userUTC;
        Boolean hasManagerChangedForUser = (oldU != NULL && (newU.ManagerId != oldU.ManagerId) || oldU == NULL); //It indicates that there is a variation in the manager assignment for the current user.
        Boolean existsUTC = false; //It indicates that exists a User Territory Configuration relative to the entered Department Number for the current user.
        Boolean existsUT2AssociationForUser = false; //It indicates if the user has already an association to a territory
        Boolean isUserActive = false;
        Boolean hasBeenUserActivated = false; //It indicates if the user has been activated
        Boolean hasBeenUserDeactivated = false; //It indicates if the user has been deactivated
        Boolean hasDNChangedForUser = false; //It indicates if the Department Number has been changed for the user.
        Boolean needsTerritoryAssociation = false; //It indicates if the territory already associated to the user is congruent with the configuration by DepartmentNumber.
        Boolean hasDifferentTerritoryRole = false; //It indicates if the territory role already attributed to the user is congruent with the configuration by DepartmentNumber.
        Boolean notManagedDepartmentNumber = false; //It indicates that the user doesn't take part in the territory management on the basis of the attributed department number or because department number value is blank.
        Boolean existsUT2AForUser = false; //It indicates that the user already has an association to a territory. (Single Value - consider only for SRD e SMA Roles for which is supposed that can exists only one association per user.)
        Boolean hasBeenUserCodeBlanked = false;
        Boolean hasBeenUserCodeChanged = false;
		String userTerritoryRole = newU.Code__c;
        
        hasBeenUserCodeBlanked = String.isBlank(newU.Code__c) && (oldU != NULL && String.isNotBlank(oldU.Code__c) || oldU == NULL);
        hasBeenUserCodeChanged = String.isNotBlank(newU.Code__c) && ((oldU != NULL && !newU.Code__c.equalsIgnoreCase(oldU.Code__c)) || oldU == NULL);
        isUserActive = newU.isActive && (oldU != NULL && (newU.isActive == oldU.IsActive) || oldU == NULL);
        hasBeenUserActivated = newU.isActive == true && (oldU != NULL && (newU.isActive != oldU.IsActive) || oldU == NULL);
        hasBeenUserDeactivated = (newU.isActive == false && (oldU != NULL && (newU.isActive != oldU.IsActive) || oldU == NULL));
        notManagedDepartmentNumber = ((!String.isBlank(newU.DepartmentNumber__c) 
                                       && (!utc.UTCByDepNumAndUserCode.containsKey(newU.DepartmentNumber__c) 
                                           || (utc.UTCByDepNumAndUserCode.containsKey(newU.DepartmentNumber__c) && !utc.UTCByDepNumAndUserCode.get(newU.DepartmentNumber__c).containsKey(newU.Code__c)))) 
                      					|| String.isBlank(newU.DepartmentNumber__c));
        hasDNChangedForUser =  String.isNotBlank(newU.DepartmentNumber__c) && ((oldU != NULL && !newU.DepartmentNumber__c.equalsIgnoreCase(oldU.DepartmentNumber__c)) || oldU == NULL);

		existsUTC = String.isNotBlank(newU.DepartmentNumber__c)
					&& utc.UTCByDepNumAndUserCode.containsKey(newU.DepartmentNumber__c)
            		&& utc.UTCByDepNumAndUserCode.get(newU.DepartmentNumber__c).containsKey(newU.Code__c)
					&& utc.userTerritoryConfigurationsMap.containsKey(utc.UTCByDepNumAndUserCode.get(newU.DepartmentNumber__c).get(newU.Code__c));
        
        if(existsUTC){
			userUTC = utc.userTerritoryConfigurationsMap.get(utc.UTCByDepNumAndUserCode.get(newU.DepartmentNumber__c).get(newU.Code__c));
			utcTerritoryDeveloperName = userUTC.Territory__c;
            userTerritoryRole = userUTC.Role__c;
            utcGroups = userUTC.Groups__c;
            
            if(!Test.isRunningTest())
            	utcPermissionSets = userUTC.PermissionSets__c;
            else
                utcPermissionSets = Test_StandardUserHandler.utcPermissionSets;
        }
        
        existsUTC = existsUTC && utc.territoryIdsByDeveloperName.containsKey(utcTerritoryDeveloperName);
        
        existsUT2AForUser = utc.ut2associationsIdsByUserId.containsKey(newU.Id) && utc.ut2associationsMap.containsKey(utc.ut2associationsIdsByUserId.get(newU.Id));
        
        needsTerritoryAssociation = existsUTC && ((existsUT2AForUser && (utc.territoryIdsByDeveloperName.get(utcTerritoryDeveloperName)
                      != utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).Territory2Id)) 
                        || !existsUT2AForUser);
                      
        hasDifferentTerritoryRole = existsUTC && existsUT2AForUser && ((String.isNotBlank(userTerritoryRole))
                       && (String.isNotBlank(userTerritoryRole) && !userTerritoryRole.equalsIgnoreCase(utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).RoleInTerritory2))
                      ||
                      String.isNotBlank(utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).RoleInTerritory2) 
                      && !utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).RoleInTerritory2.equalsIgnoreCase(userTerritoryRole));
        
        if(Trigger.operationType == System.TriggerOperation.AFTER_INSERT){
            UserTerritory2Association ut2a = new UserTerritory2Association();
            List<PermissionSetAssignment> permissionSetAssignmentsToUpsert;
            List<GroupMember> groupMembersToUpsert;
            
            if(existsUTC){
                //UserTerritory2Association
				ut2a.UserId = newU.Id;
				
				if(String.isNotBlank(utcTerritoryDeveloperName)
				   && utc.territoryIdsByDeveloperName.containsKey(utcTerritoryDeveloperName))
					ut2a.Territory2Id = utc.territoryIdsByDeveloperName.get(utcTerritoryDeveloperName);
					
				ut2a.RoleInTerritory2 = userTerritoryRole;
				
				utcData.ut2associationsToUpsert.add(ut2a);
                
                //UserTerritory -> PermissionSetAssignments
                if(String.isNotBlank(utcPermissionSets))
                {
                    Set<String> psUTCNames = new Set<String>(utcPermissionSets.split(';'));
                    if(ucData.psaToInsertByUserId.containsKey(newU.Id) 
                       && ucData.psaToInsertByUserId.get(newU.Id) != NULL 
                       && ucData.psaToInsertByUserId.get(newU.Id).size() > 0){
                        Set<String> psUCNames = new Set<String>();
                        for(Id psUCId : ucData.psaToInsertByUserId.get(newU.Id).keySet()){
                            if(uc.mapPermSetsById.containsKey(psUCId) && String.isNotBlank(uc.mapPermSetsById.get(psUCId).Name))
                            	psUCNames.add(uc.mapPermSetsById.get(psUCId).Name);
                        }
                        psUTCNames.removeAll(psUCNames);
                        
                        if(psUTCNames.size() > 0){
                            for(String psUTCName : psUTCNames){
                                if(uc.mapPermSets.containsKey(psUTCName)){
                                    PermissionSetAssignment ps = new PermissionSetAssignment();
                                    ps.PermissionSetId = uc.mapPermSets.get(psUTCName).Id;
                                    ps.AssigneeId = newU.Id;
                                    if(!ucData.psaToInsertByUserId.containsKey(newU.Id))
                                    	ucData.psaToInsertByUserId.put(newU.Id ,new Map<Id, PermissionSetAssignment>());
                                    
                                    ucData.psaToInsertByUserId.get(newU.Id).put(uc.mapPermSets.get(psUTCName).Id,ps);
                                }
                            }
                        }
                    }
                }
                
                //UserTerritory -> GroupMembers
                if(String.isNotBlank(utcGroups)){
                    Set<String> utcGroupDeveloperNamesSet = new Set<String>(utcGroups.split(';'));
                    Map<Id,GroupMember> userGroupMembersToInsert;
                    Map<Id,GroupMember> userGroupMembersToDelete;
                    
                    if(ucData.gmToInsertByUserId.containsKey(newU.Id) && ucData.gmToInsertByUserId.get(newU.Id).size() > 0){
                        userGroupMembersToInsert = ucData.gmToInsertByUserId.get(newU.Id);
                    }
                    
                    if(ucData.gmToDeleteByUserId.containsKey(newU.Id) && ucData.gmToDeleteByUserId.get(newU.Id).size() > 0){
                        userGroupMembersToDelete = ucData.gmToDeleteByUserId.get(newU.Id);
                    }
                    
                    for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                    	if(!utc.groupIdsByDeveloperNameAndType.containsKey(utcGroupDeveloperName) 
                           || (utc.groupIdsByDeveloperNameAndType.containsKey(utcGroupDeveloperName) && !utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).containsKey('Regular')))
                            utcGroupDeveloperNamesSet.remove(utcGroupDeveloperName);
                    }
                    
                    if(utcGroupDeveloperNamesSet != NULL && utcGroupDeveloperNamesSet.size() > 0){
                        for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                            Id groupId = utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).get('Regular');
                            
                            if(userGroupMembersToInsert != NULL && userGroupMembersToInsert.containsKey(groupId))
                                utcGroupDeveloperNamesSet.remove(groupId);
                            
                            if(userGroupMembersToDelete != NULL && userGroupMembersToDelete.containsKey(groupId)){
                                utcGroupDeveloperNamesSet.remove(utcGroupDeveloperName);
                                ucData.gmToDeleteByUserId.get(newU.Id).remove(groupId);
                            }
                        }
                    }
                    
                    if(utcGroupDeveloperNamesSet != NULL && utcGroupDeveloperNamesSet.size() > 0){
                        for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                            Id groupId = utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).get('Regular');
                            
                            GroupMember gm = new GroupMember();
                            gm.UserOrGroupId = newU.Id;
                            gm.GroupId = groupId;
                            ucData.gmToInsertByUserId.get(newU.Id).put(groupId, gm);
                        }
                    }
                    
                }
             }
        }else if(Trigger.operationType == System.TriggerOperation.AFTER_UPDATE){
            System.debug('>>newU.isActive: ' + newU.isActive);
            System.debug('>>oldU.isActive: ' + oldU.isActive);
            System.debug('>>utc.ut2associationsIdsByUserId.containsKey(newU.Id): ' + utc.ut2associationsIdsByUserId.containsKey(newU.Id));
            if(utc.ut2associationsIdsByUserId.containsKey(newU.Id)){
                System.debug('>>utc.ut2associationsMap.containsKey(utc.ut2associationsIdsByUserId.get(newU.Id): ' + utc.ut2associationsMap.containsKey(utc.ut2associationsIdsByUserId.get(newU.Id)));
                System.debug('>>utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).RoleInTerritory2: ' +utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)).RoleInTerritory2);
            }
            
            //UserTerritory2Association
            if(existsUTC && !notManagedDepartmentNumber 
               && (hasBeenUserCodeChanged 
                   || hasDNChangedForUser 
                   || (existsUT2AssociationForUser && (needsTerritoryAssociation || hasDifferentTerritoryRole)
                   || hasBeenUserActivated)
                   || !existsUT2AForUser
                  )
               ){
                UserTerritory2Association ut2a = new UserTerritory2Association();
                
                ut2a.UserId = newU.Id;
                
                if(String.isNotBlank(utcTerritoryDeveloperName)
                   && utc.territoryIdsByDeveloperName.containsKey(utcTerritoryDeveloperName))
                    ut2a.Territory2Id = utc.territoryIdsByDeveloperName.get(utcTerritoryDeveloperName);
					
                ut2a.RoleInTerritory2 = userTerritoryRole;
                
               	utcData.ut2associationsToUpsert.add(ut2a);
                
                //UserTerritory - DELETE PREVIOUS LINK
                if(utc.ut2associationsIdsByUserId.containsKey(newU.Id) 
                   && utc.ut2associationsMap.containsKey(utc.ut2associationsIdsByUserId.get(newU.Id)))
                    utcData.ut2associationsToDelete.add(utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)));
                
            }else if(notManagedDepartmentNumber || hasBeenUserDeactivated){
                System.debug('>>DELEUSERTERRITORY');
                
                //DELETE UserTerritory
                if((existsUT2AForUser)
                  || hasBeenUserDeactivated)
                {
                    if(existsUT2AForUser)
                   		utcData.ut2associationsToDelete.add(utc.ut2associationsMap.get(utc.ut2associationsIdsByUserId.get(newU.Id)));
                }
            }
            
            //UserTerritory -> PermissionSetAssignments
            if(String.isNotBlank(utcPermissionSets)){
				
                Set<Id> currentUTCPermissionSets = new Set<Id>();
                Map<Id,PermissionSetAssignment> ucPSAToInsertMap;
                Map<Id,PermissionSetAssignment> ucPSAToDeleteMap;
                
                Set<Id> permissionSetsToBeInserted = new Set<Id>();
                
                Set<Id> utcPSAToBeRemovedFromDeletionMap;
                Set<Id> utcPSAAlreadyInInsertionMap;
                Set<Id> utcPSAInSystem;
                
                for(String permSetDN : utcPermissionSets.split(';'))
                    if(uc.mapPermSets.containsKey(permSetDN))
                        currentUTCPermissionSets.add(uc.mapPermSets.get(permSetDN).Id);
                
                permissionSetsToBeInserted = currentUTCPermissionSets.clone();
                
                if(permissionSetsToBeInserted != NULL & permissionSetsToBeInserted.size() > 0){
                    if(uc.permissionSetAssignmentsByUserIdMap != NULL
                       && uc.permissionSetAssignmentsByUserIdMap.containsKey(newU.Id)
                       && uc.permissionSetAssignmentsByUserIdMap.get(newU.Id).size() > 0 )
                        utcPSAInSystem = uc.permissionSetAssignmentsByUserIdMap.get(newU.Id).keySet();
                    
                    if(ucData.psaToInsertByUserId.containsKey(newU.Id) && ucData.psaToInsertByUserId.get(newU.Id).size() > 0){
                        utcPSAInSystem.removeAll(ucData.psaToInsertByUserId.get(newU.Id).keySet());
                        ucPSAToInsertMap = ucData.psaToInsertByUserId.get(newU.Id);
                        utcPSAAlreadyInInsertionMap = ucPSAToInsertMap.keySet().clone();
                    }
                    
                    if(ucData.psaToDeleteByUserId.containsKey(newU.Id) && ucData.psaToDeleteByUserId.get(newU.Id).size() > 0){
                        utcPSAInSystem.removeAll(ucData.psaToDeleteByUserId.get(newU.Id).keySet());
                        ucPSAToDeleteMap = ucData.psaToDeleteByUserId.get(newU.Id);
                        utcPSAToBeRemovedFromDeletionMap = ucPSAToDeleteMap.keySet().clone();
                    }
                    
                    if(utcPSAInSystem != NULL && utcPSAInSystem.size() > 0)
                        permissionSetsToBeInserted.removeAll(utcPSAInSystem);
                    
                    if(utcPSAToBeRemovedFromDeletionMap != NULL && utcPSAToBeRemovedFromDeletionMap.size() > 0){
                        utcPSAToBeRemovedFromDeletionMap.retainAll(permissionSetsToBeInserted);
                        if(utcPSAToBeRemovedFromDeletionMap.size() > 0){
                            for(Id psId : utcPSAToBeRemovedFromDeletionMap){
                                if(ucPSAToDeleteMap.containsKey(psId))
                                    ucPSAToDeleteMap.remove(psId);
                                
                                if(permissionSetsToBeInserted.contains(psId))
                                    permissionSetsToBeInserted.remove(psId);
                            }
                        }
                    }
                    
                    if(utcPSAAlreadyInInsertionMap != NULL && utcPSAAlreadyInInsertionMap.size() > 0){
                        utcPSAAlreadyInInsertionMap.retainAll(permissionSetsToBeInserted);
                        if(utcPSAAlreadyInInsertionMap.size() > 0){
                            for(Id psId : utcPSAAlreadyInInsertionMap){
                                if(permissionSetsToBeInserted.contains(psId))
                                    permissionSetsToBeInserted.remove(psId);
                            }
                        }
                    }
                    
                    if(permissionSetsToBeInserted != NULL && permissionSetsToBeInserted.size() > 0){
                         for(Id permissionSetId : permissionSetsToBeInserted){
                             PermissionSetAssignment ps = new PermissionSetAssignment();
                             ps.PermissionSetId = permissionSetId;
                             ps.AssigneeId = newU.Id;
                             if(!ucData.psaToInsertByUserId.containsKey(newU.Id))
                                 ucData.psaToInsertByUserId.put(newU.Id, new Map<Id,PermissionSetAssignment>());
                             
                             ucData.psaToInsertByUserId.get(newU.Id).put(permissionSetId, ps);
                         }
                    }
                }
            }
            
            //UserTerritory -> GroupMembers
            Set<String> utcGroupDeveloperNamesSet;
            if(String.isNotBlank(utcGroups))
                utcGroupDeveloperNamesSet = new Set<String>(utcGroups.split(';'));
            else
                utcGroupDeveloperNamesSet = new Set<String>();
            
            Set<Id> remainingGroupIdsToDeleteSet = new Set<Id>();
            Map<Id,GroupMember> userGroupMembersToInsert;
            Map<Id,GroupMember> userGroupMembersToDelete;
            
            if(ucData.gmToInsertByUserId.containsKey(newU.Id) && ucData.gmToInsertByUserId.get(newU.Id).size() > 0){
                userGroupMembersToInsert = ucData.gmToInsertByUserId.get(newU.Id);
            }
            
            if(ucData.gmToDeleteByUserId.containsKey(newU.Id) && ucData.gmToDeleteByUserId.get(newU.Id).size() > 0){
                userGroupMembersToDelete = ucData.gmToDeleteByUserId.get(newU.Id);
            }
            
            if(String.isNotBlank(utcGroups)){
                
                for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                    if(!utc.groupIdsByDeveloperNameAndType.containsKey(utcGroupDeveloperName) 
                       || (utc.groupIdsByDeveloperNameAndType.containsKey(utcGroupDeveloperName) && !utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).containsKey('Regular')))
                        utcGroupDeveloperNamesSet.remove(utcGroupDeveloperName);
                }
                
                if(utcGroupDeveloperNamesSet != NULL && utcGroupDeveloperNamesSet.size() > 0){
                    for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                        Id groupId = utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).get('Regular');
                        
                        if(userGroupMembersToInsert != NULL && userGroupMembersToInsert.containsKey(groupId))
                            utcGroupDeveloperNamesSet.remove(groupId);
                        
                        if(userGroupMembersToDelete != NULL && userGroupMembersToDelete.containsKey(groupId)){
                            utcGroupDeveloperNamesSet.remove(utcGroupDeveloperName);
                            ucData.gmToDeleteByUserId.get(newU.Id).remove(groupId);
                        }
                    }
                }
                
                if(utcGroupDeveloperNamesSet != NULL && utcGroupDeveloperNamesSet.size() > 0){
                    for(String utcGroupDeveloperName : utcGroupDeveloperNamesSet){
                        Id groupId = utc.groupIdsByDeveloperNameAndType.get(utcGroupDeveloperName).get('Regular');
                        
                        GroupMember gm = new GroupMember();
                        gm.UserOrGroupId = newU.Id;
                        gm.GroupId = groupId;
                        
                        if(!ucData.gmToInsertByUserId.containsKey(newU.Id))
                            ucData.gmToInsertByUserId.put(newU.Id, new Map<Id, GroupMember>());
                        
                        ucData.gmToInsertByUserId.get(newU.Id).put(groupId, gm);
                    }
                }
            }
            
            if(utc.groupMembersByUserId != NULL 
               && (utc.groupMembersByUserId.containsKey(newU.Id) && utc.groupMembersByUserId.get(newU.Id).size() > 0)){
                   for(Id groupId : utc.groupMembersByUserId.get(newU.Id).keySet())
                       remainingGroupIdsToDeleteSet.add(groupId);
                   
                   if(remainingGroupIdsToDeleteSet != NULL && remainingGroupIdsToDeleteSet.size() > 0){
                       
                       if(userGroupMembersToInsert != NULL && userGroupMembersToInsert.size() > 0){
                           for(Id groupId : userGroupMembersToInsert.keySet())
                               remainingGroupIdsToDeleteSet.remove(groupId);
                       }
                       
                       if(userGroupMembersToDelete != NULL && userGroupMembersToDelete.size() > 0){
                           for(Id groupId : userGroupMembersToDelete.keySet())
                               remainingGroupIdsToDeleteSet.remove(groupId);
                       }
                       
                       if(remainingGroupIdsToDeleteSet.size() > 0){
                           for(Id groupId : remainingGroupIdsToDeleteSet){
                               	if(!ucData.gmToDeleteByUserId.containsKey(newU.Id))
                            		ucData.gmToDeleteByUserId.put(newU.Id, new Map<Id, GroupMember>());
                               
                           		ucData.gmToDeleteByUserId.get(newU.Id).put(groupId, utc.groupMembersMap.get(utc.groupMembersByUserId.get(newU.Id).get(groupId)));
                           }
                       }
                   }
               }
        }
    }
    
    public static void prepareUserTerritoryConfiguration(ConfigurationData cd, Map<Id, User> newUsers){
        UserTerritoryConfiguration utConfiguration = cd.utc;
        Map<Id, UserTerritoryConfiguration__mdt> userTerritoryConfigurationsMap = new Map<Id, UserTerritoryConfiguration__mdt>([SELECT Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, DepartmentNumber__c, Territory__c, PermissionSets__c, Role__c, UserRole__c, UserCode__c, Groups__c FROM UserTerritoryConfiguration__mdt]);
        Map<String, Map<String, Id>> UTCByDepNumAndUserCode = new Map<String, Map<String, Id>>();
        Set<String> groupDeveloperNamesUTC = new Set<String>();
        
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id, Name, Territory2TypeId, Territory2ModelId, ParentTerritory2Id, Description, ForecastUserId, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel, ContactAccessLevel, LastModifiedDate, LastModifiedById, SystemModstamp, DeveloperName FROM Territory2 WHERE Territory2Model.state = 'Active']);
        Map<String, Id> territoryIdsByDeveloperName = new Map<String, Id>();
        
        //Map<String, Map<String, Set<String>>> territoriesHierarchyMap = new Map<String, Map<String, Set<String>>>();
        //Map<String, Set<String>> secondLevelTerritoriesHierarchyMap = new Map<String, Set<String>>();
        
        Map<Id, UserTerritory2Association> ut2associationsMap = new Map<Id, UserTerritory2Association>([SELECT Id, UserId, Territory2Id, Territory2.Name, Territory2.DeveloperName, IsActive, RoleInTerritory2, LastModifiedDate, LastModifiedById, SystemModstamp FROM UserTerritory2Association WHERE UserId IN :newUsers.keySet()]);
        Map<Id, Id> ut2associationsIdsByUserId = new Map<Id, Id>();
        
        Map<Id, Group> groupsMap = new Map<Id, Group>([SELECT Id, DeveloperName, Name, Type FROM Group]);
        Map<String, Map<String,Id>> groupIdsByDeveloperNameAndType = new Map<String, Map<String,Id>>();
        
        Map<Id, GroupMember> groupMembersMap = new Map<Id, GroupMember>();
        Map<Id, Map<Id,Id>> groupMembersByUserId = new Map<Id, Map<Id,Id>>();
        
        if(userTerritoryConfigurationsMap != NULL && userTerritoryConfigurationsMap.size() > 0){
            for(UserTerritoryConfiguration__mdt utConf : userTerritoryConfigurationsMap.values()){
                if(String.isNotBlank(utConf.Groups__c))
                    groupDeveloperNamesUTC.addAll(utConf.Groups__c.split(';'));
                
                if(String.isNotBlank(utConf.DepartmentNumber__c) && !UTCByDepNumAndUserCode.containsKey(utConf.DepartmentNumber__c))
                    UTCByDepNumAndUserCode.put(utConf.DepartmentNumber__c, new Map<String,Id>());
                
                UTCByDepNumAndUserCode.get(utConf.DepartmentNumber__c).put(utConf.UserCode__c, utConf.Id);
            }
        }
        
        if(ut2associationsMap != NULL && ut2associationsMap.size() > 0){
            for(UserTerritory2Association ut2a : ut2associationsMap.values()){
                    ut2associationsIdsByUserId.put(ut2a.UserId, ut2a.Id);
            }
        }
        
        Set<String> territoryDeveloperNames = new Set<String>();
        
        if(territoriesMap != NULL && territoriesMap.size() > 0){
            for(Territory2 t : territoriesMap.values()){
                territoryDeveloperNames.add(t.DeveloperName);
				if(String.isNotBlank(t.DeveloperName))
				territoryIdsByDeveloperName.put(t.DeveloperName,t.Id);
            }
        }
        
        groupMembersMap = new Map<Id, GroupMember>([SELECT Id, GroupId, Group.DeveloperName, UserOrGroupId, SystemModstamp FROM GroupMember WHERE UserOrGroupId IN :newUsers.keySet() AND Group.DeveloperName IN :cd.groupsSetUTC]);
        
        /*if(territoriesMap != NULL && territoriesMap.size() > 0){
            
            for(Territory2 t : territoriesMap.values()){
                if(t.ParentTerritory2Id == NULL){
                    
					if(!territoriesHierarchyMap.containsKey(t.DeveloperName))
					territoriesHierarchyMap.put(t.DeveloperName, null);
                    
                }else
                {
                    if(territoriesMap.containsKey(t.ParentTerritory2Id) && territoriesMap.get(t.ParentTerritory2Id).ParentTerritory2Id == NULL){
                        
                        if(!territoriesHierarchyMap.containsKey(territoriesMap.get(t.ParentTerritory2Id).DeveloperName))
                            territoriesHierarchyMap.put(territoriesMap.get(t.ParentTerritory2Id).DeveloperName, new Map<String, Set<String>>());
                        
                        if(!territoriesHierarchyMap.get(territoriesMap.get(t.ParentTerritory2Id).DeveloperName).isEmpty() 
                            && !territoriesHierarchyMap.get(territoriesMap.get(t.ParentTerritory2Id).DeveloperName).containsKey(t.DeveloperName)){
                            territoriesHierarchyMap.get(territoriesMap.get(t.ParentTerritory2Id).DeveloperName).put(t.DeveloperName, new Set<String>());
                        	secondLevelTerritoriesHierarchyMap.put(t.DeveloperName, territoriesHierarchyMap.get(territoriesMap.get(t.ParentTerritory2Id).DeveloperName).get(t.DeveloperName));
                        }
                    }
                }
            }
        }*/
        
        if(groupsMap != NULL && groupsMap.size() > 0){
            
            for(Group g : groupsMap.values()){
                
                if(!groupIdsByDeveloperNameAndType.containsKey(g.DeveloperName))
                    groupIdsByDeveloperNameAndType.put(g.DeveloperName, new Map<String,Id>());
                
                groupIdsByDeveloperNameAndType.get(g.DeveloperName).put(g.Type,g.Id);
            }
        }
        
        if(groupMembersMap != NULL && groupMembersMap.size() > 0)
        {
            for(GroupMember gm : groupMembersMap.values()){
                
                if(!groupMembersByUserId.containsKey(gm.UserOrGroupId))
                    groupMembersByUserId.put(gm.UserOrGroupId, new Map<Id, Id>());
                
                groupMembersByUserId.get(gm.UserOrGroupId).put(gm.GroupId, gm.Id);
            }
        }
        
        utConfiguration.userTerritoryConfigurationsMap = userTerritoryConfigurationsMap;
        utConfiguration.UTCByDepNumAndUserCode = UTCByDepNumAndUserCode;
        
        utConfiguration.territoriesMap = territoriesMap;
        utConfiguration.territoryIdsByDeveloperName = territoryIdsByDeveloperName;
        
        //utConfiguration.territoriesHierarchyMap = territoriesHierarchyMap;
        //utConfiguration.secondLevelTerritoriesHierarchyMap = secondLevelTerritoriesHierarchyMap;
        
        utConfiguration.groupsMap = groupsMap;
        utConfiguration.groupIdsByDeveloperNameAndType = groupIdsByDeveloperNameAndType;
        
        utConfiguration.groupMembersMap = groupMembersMap;
        utConfiguration.groupMembersByUserId = groupMembersByUserId;
        
        utConfiguration.ut2associationsMap = ut2associationsMap;
        utConfiguration.ut2associationsIdsByUserId = ut2associationsIdsByUserId;
    }
    
    public class ConfigurationData{
        public UserConfiguration uc;
        public UserConfigurationData ucData;
        public UserTerritoryConfiguration utc;
        public UTConfigurationData utcData;
        public Set<String> permissionSetsSetUC;
        public Set<String> groupsSetUC;
        
        public Set<String> permissionSetsSetUTC;
        public Set<String> groupsSetUTC;
        
        public ConfigurationData(){
            this.uc = new UserConfiguration();
            this.ucData = new UserConfigurationData();
           	this.utc = new UserTerritoryConfiguration();
            this.utcData = new UTConfigurationData();
        }
    }
    
    public class UserConfiguration{
        public Map<Id, UserConfiguration__mdt> mapUConfById;
        public Map<Id, Profile> mapProfilesById;
        public Map<Id, UserRole> mapRoleById;
        public Map<Id, PermissionSet> mapPermSetsById;
        public Map<Id, CallCenter> mapCallCentersById;
        public Map<Id, PermissionSetLicense> mapPermSetLicById;
        
        public Map<String, UserConfiguration__mdt> mapUConf;
        public Map<String, Profile> mapProfiles;
        public Map<String, UserRole> mapRole;
        public Map<String, PermissionSet> mapPermSets;
        public Map<string, CallCenter> mapCallCenters;
        public Map<String, PermissionSetLicense> mapPermSetsLic;

        public Map<Id, PermissionSetAssignment> permissionSetAssignmentsMap; 	
        public Map<Id, Map<Id,Id>> permissionSetAssignmentsByUserIdMap;
        
        //S:ADDED GROUPMEMBERS LOGIC - 20200206
        public Map<Id, Group> groupsMap;
        //Map<String, Id> groupIdsByDeveloperName;
        public Map<String, Map<String,Id>> groupIdsByDeveloperNameAndType;
        
        public Map<Id, GroupMember> groupMembersMap;
        public Map<Id, Map<Id,Id>> groupMembersByUserId;
        
        public Map<Id, User> managers;
        //E:ADDED GROUPMEMBERS LOGIC - 20200206
    }
    
    public class UserConfigurationData{
        public Map<Id,Map<Id,PermissionSetAssignment>> psaToInsertByUserId;
        public Map<Id,Map<Id,PermissionSetAssignment>> psaToDeleteByUserId;
        public Map<Id,Map<Id,PermissionSetLicenseAssign>> psalToInsertByUserId;
        public Map<Id,Map<Id,PermissionSetLicenseAssign>> psalToDeleteByUserId;
        
        public Map<Id,Map<Id,GroupMember>> gmToInsertByUserId;
        public Map<Id,Map<Id,GroupMember>> gmToDeleteByUserId;
        
        /*//S:ADDED GROUPMEMBERS LOGIC - 20200206
        public List<GroupMember> groupMembersToUpsert;
        public List<GroupMember> groupMembersToDelete;
        //E:ADDED GROUPMEMBERS LOGIC - 20200206*/
        
        public UserConfigurationData()
        {
            gmToInsertByUserId = new Map<Id,Map<Id,GroupMember>>();
            gmToDeleteByUserId = new Map<Id,Map<Id,GroupMember>>();
            /*//S:ADDED GROUPMEMBERS LOGIC - 20200206
            groupMembersToUpsert = new List<GroupMember>();
            groupMembersToDelete = new List<GroupMember>();
            //E:ADDED GROUPMEMBERS LOGIC - 20200206*/
            psaToInsertByUserId = new Map<Id,Map<Id,PermissionSetAssignment>>();
            psaToDeleteByUserId = new Map<Id,Map<Id,PermissionSetAssignment>>();
            psalToInsertByUserId = new Map<Id,Map<Id,PermissionSetLicenseAssign>>();
            psalToDeleteByUserId = new Map<Id,Map<Id,PermissionSetLicenseAssign>>();
        }
    }
    
    public class UserTerritoryConfiguration{
        
        Map<Id, UserTerritoryConfiguration__mdt> userTerritoryConfigurationsMap;
       	Map<String, Map<String, Id>> UTCByDepNumAndUserCode;
        
        Map<Id, Territory2> territoriesMap;
        Map<String, Id> territoryIdsByDeveloperName;
        
        Map<String, Map<String, Set<String>>> territoriesHierarchyMap;
        Map<String, Set<String>> secondLevelTerritoriesHierarchyMap;
        
        Map<Id, UserTerritory2Association> ut2associationsMap;
        Map<Id, Id> ut2associationsIdsByUserId;
        
        Map<Id, Group> groupsMap;
        Map<String, Map<String,Id>> groupIdsByDeveloperNameAndType;
        
        Map<Id, GroupMember> groupMembersMap;
        Map<Id, Map<Id,Id>> groupMembersByUserId;   	    
    }
    
    class UTConfigurationData{
        List<UserTerritory2Association> ut2associationsToUpsert;
        List<UserTerritory2Association> ut2associationsToDelete;
        
        List<PermissionSetAssignment> permissionSetAssignmentsToUpsert;
        List<PermissionSetAssignment> permissionSetAssignmentsToDelete;
        
        List<GroupMember> groupMembersToUpsert;
        List<GroupMember> groupMembersToDelete;
        Map<Id,Set<Id>> groupMembersToUpsertMap;
        Map<Id,Set<Id>> groupMembersToDeleteMap;
    }
}