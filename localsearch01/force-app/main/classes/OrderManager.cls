/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date		   : 07-10-2019
 * Sprint      : Sprint 5
 * Work item   : SF2-210, Sprint5, Wave C, Order Management.
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */
 
@RestResource(urlMapping='/product-configuration/feedbacks')
global without sharing class OrderManager {
    
    @HttpPost
    global static void activate(){
        RestResponse res = RestContext.response;
        RestRequest request = RestContext.request;
        Order order;
        ResponseInterface responseInfo = new ResponseInterface();
        
        try{
            Id quoteId = null;
            responseInfo = (ResponseInterface)JSON.deserialize(request.requestbody.tostring(), ResponseInterface.class);
            String idCall = responseInfo.technical_fields.correlation_id;

            order = SEL_Order.getOrderByExternalId(idCall);
            //Log__c existingLog = SEL_Log.getLogByCorrelationId(idCall);
      

            if(order == null){//quote managemente
                Boolean isInvalidCorrelationId = SRV_OrderManager.orderIsNull(idCall, responseInfo, request );
                if(isInvalidCorrelationId){
                    res.responseBody = Blob.valueOf('Invalid correlation_id');
                    res.statusCode = 200;
                }
            }
            else if(order != null){
                SRV_OrderManager.evaluateAndExecuteAction(responseInfo, order, request);
                res.responseBody = Blob.valueOf('Ok');
                res.statusCode = 200;                     
            }
        }
        catch(Exception e) {
            LogUtility.saveOrderResponse(request, order, ConstantsUtil.LOG_RESULT_KO, responseInfo);
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
        }
    } 

    global class CustomException extends Exception {}        
}