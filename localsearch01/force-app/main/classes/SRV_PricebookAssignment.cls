/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Service class for Opportunity trigger to set Pricebook on Opportunity.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato <vlaudato@deloitte.it>
* @created        2020-07-24
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class SRV_PricebookAssignment {
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Based on the Sales Channel value set on the User,  Query the Pricebook by matching on the 
    * Sales Channel field, if the retrived Pricebooks are more than 1, it should not be set on 
    * Opportunity the Pricebook. If it’s only 1 Pricebook retrived through the Query set the 
    * value on Opportunity.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	newItems    Trigger.New trigger context variable
    * @return   		void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void pricebookAssignment(List<Opportunity> opportunities, String userSalesChannel){
        
        String queryPricebook = 'SELECT Id, ExternalId__c FROM Pricebook2 WHERE Sales_Channel__c includes (' + userSalesChannel +')';
        List<Pricebook2> availablePricebooks = Database.query(queryPricebook);
        
        if(!availablePricebooks.isEmpty()){
            
            Set<Id> pricebookIds = new Set<Id>();
            Set<Id> accessiblePricebookIds = new Set<Id>();
            Map<String, Id> externalIdPricebook = new Map<String, Id>();
            
            for(Pricebook2 currentPricebook : availablePricebooks){
                pricebookIds.add(currentPricebook.Id);
                externalIdPricebook.put(currentPricebook.ExternalId__c, currentPricebook.Id);
            }
            
            List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId IN: pricebookIds];
            
            for(UserRecordAccess recordAccess : listUserRecordAccess){
                if(recordAccess.HasReadAccess) accessiblePricebookIds.add(recordAccess.RecordId);
            }
            
            if(accessiblePricebookIds.size() == 1){
                for(Opportunity opportunity : opportunities) opportunity.Pricebook2Id = new List<Id>(accessiblePricebookIds)[0];
            }
            /*else{
                
                if( (!accessiblePricebookIds.isEmpty() && externalIdPricebook.containsKey(ConstantsUtil.ONEPRESENCE_PRICEBOOK)) || Test.isRunningTest()){
                    if(accessiblePricebookIds.contains(externalIdPricebook.get(ConstantsUtil.ONEPRESENCE_PRICEBOOK)) || Test.isRunningTest()){
                        
                        Map<Id, Opportunity> upgradeContractToOpty = new Map<Id, Opportunity>();
						for(Opportunity opportunity : opportunities) if(String.isNotBlank(opportunity.UpgradeDowngrade_Contract__c)) upgradeContractToOpty.put(opportunity.UpgradeDowngrade_Contract__c, opportunity);
						
                        if(!upgradeContractToOpty.isEmpty()){
                            Set<Id> onlyOPContractIds = getOnlyOPContracts(upgradeContractToOpty.keySet());
                            for(Id currentContractId : onlyOPContractIds) upgradeContractToOpty.get(currentContractId).Pricebook2Id = externalIdPricebook.get(ConstantsUtil.ONEPRESENCE_PRICEBOOK);
                        }
                        
                    }
                }
            }*/
        }
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * In upgrade process, checks if the upgraded contract has only OnePresence products.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	contractIds    	Upgraded Contracts to check
    * @return   		Set<Id>			Id of Contracts that have only OnePresence subscriptions
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    /*public static Set<Id> getOnlyOPContracts(Set<Id> contractIds){
        
        Set<Id> onlyOPContractIds = new Set<Id>();
        
        Map<Id, List<SBQQ__Subscription__c>> contractToSubscriptions = new Map<Id, List<SBQQ__Subscription__c>>();
        Map<Id, List<SBQQ__Subscription__c>> contractToOnePresenceSubscriptions = new Map<Id, List<SBQQ__Subscription__c>>();
        
        Map<Id, SBQQ__Subscription__c> upgradedSubscriptions = new Map<Id, SBQQ__Subscription__c>([SELECT 
                                                                                                   			Id, SBQQ__Contract__c, SBQQ__Product__r.ProductCode
                                                                                                   FROM		SBQQ__Subscription__c
                                                                                                   WHERE	RequiredBy__c = NULL
                                                                                                   AND		SBQQ__Contract__c IN :contractIds
                                                                                                  ]);
        
        for(SBQQ__Subscription__c currentSub : upgradedSubscriptions.values()){
            if(!contractToSubscriptions.containsKey(currentSub.SBQQ__Contract__c)) contractToSubscriptions.put(currentSub.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{currentSub});
            else contractToSubscriptions.get(currentSub.SBQQ__Contract__c).add(currentSub);
            if(ConstantsUtil.ONEPRESENCE001_AR.equalsIgnoreCase(currentSub.SBQQ__Product__r.ProductCode)){
                if(!contractToOnePresenceSubscriptions.containsKey(currentSub.SBQQ__Contract__c)) contractToOnePresenceSubscriptions.put(currentSub.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{currentSub});
                else contractToOnePresenceSubscriptions.get(currentSub.SBQQ__Contract__c).add(currentSub);
            }
        }
        
        if(!contractToSubscriptions.isEmpty()){
            for(Id currentContractId : contractToSubscriptions.keySet()){
                if(contractToOnePresenceSubscriptions.containsKey(currentContractId) && contractToSubscriptions.get(currentContractId).size() == contractToOnePresenceSubscriptions.get(currentContractId).size()){
                    onlyOPContractIds.add(currentContractId);
                }
            }
        }
        
        return onlyOPContractIds;
    }*/
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * In upgrade process, gets all Pricebooks visible to current user
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	userId    		Current User Id
    * @return   		Set<Id>			Id of Pricebooks accessible by the User
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Set<Id> getPricebookForUser(Id userId){
        
        Set<Id> accessiblePricebookIds = new Set<Id>();
        
        List<User> currentUser = [SELECT Id, Sales_Channel__c FROM User WHERE Id = :userId];
        
        if(!currentUser.isEmpty()){
            
            String userSalesChannel = currentUser[0].Sales_Channel__c;
            
            if(String.isNotBlank(userSalesChannel)){
                List<String> userSalesChannelsWithQuotes = new List<String>();
                List<String> userSalesChannels = userSalesChannel.split(';');
                
                for(String currentSalesChannel : userSalesChannels) userSalesChannelsWithQuotes.add('\''+currentSalesChannel+'\'');
                
                String salesChannelsForQuery = String.join(userSalesChannelsWithQuotes,',');
                
                String queryPricebook = 'SELECT Id FROM Pricebook2 WHERE Sales_Channel__c includes (' + salesChannelsForQuery +')';
                List<Pricebook2> availablePricebooks = Database.query(queryPricebook);
                
                if(!availablePricebooks.isEmpty()){
                    
                    Set<Id> pricebookIds = new Set<Id>();
                    
                    for(Pricebook2 currentPricebook : availablePricebooks) pricebookIds.add(currentPricebook.Id);
                    
                    List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId IN: pricebookIds];
                    
                    for(UserRecordAccess recordAccess : listUserRecordAccess){
                        if(recordAccess.HasReadAccess) accessiblePricebookIds.add(recordAccess.RecordId);
                    }
                }
            }
        }
        
        return accessiblePricebookIds;
    }
}