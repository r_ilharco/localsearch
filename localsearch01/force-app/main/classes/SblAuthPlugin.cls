global class SblAuthPlugin extends Auth.AuthProviderPluginClass {
    
    static final integer HTTP_TIMEOUT_MILLIS = Integer.valueOf(ConstantsUtil.HTTP_TIMEOUT_MILLIS);
    
    global String getCustomMetadataType() {
        return 'Swissbilling_Oauth__mdt';
    }
    
    global PageReference initiate(Map<string,string> authProviderConfiguration, String stateToPropagate) {
        string authUrl = authProviderConfiguration.get('Redirect_Url__c');
        if(authUrl == null || authUrl == '') {
            SblAuthPluginException tpException = new SblAuthPluginException('Redirect Url not defined in Auth. Provider', ErrorHandler.ErrorCode.E_NULL_VALUE);
            ErrorHandler.logError('SblAuthPlugin', 'SblAuthPlugin.AuthProviderPlugin', tpException);
            throw tpException;
        }
        return new PageReference(authUrl + '?state=' + stateToPropagate);
    }
    
    global Auth.AuthProviderTokenResponse handleCallback(Map<string,string> authProviderConfiguration, Auth.AuthProviderCallbackState state ) {
        String sfState = state.queryParameters.get('state');
      	Token token = getToken(authProviderConfiguration, sfState);
        return new Auth.AuthProviderTokenResponse('Sbl', token.access_token, 'refreshToken', sfState);
    }
    
    global override Auth.OAuthRefreshResult refresh(Map<String,String> authProviderConfiguration, String refreshToken) {
        Token token = getToken(authProviderConfiguration, refreshToken);
        return new Auth.OAuthRefreshResult(token.access_token, token.refresh_token);
    }
    
    global Auth.UserData  getUserInfo(Map<string,string> authProviderConfiguration, Auth.AuthProviderTokenResponse response) { 
        return new Auth.UserData('Sbl', null, 'Sbl', 'Sbl', null, null, 'Sbl', null, 'Sbl', null, null);
    }
    
    private Token getToken (Map<String,String> authProviderConfiguration, String refreshToken) {
        string clientId = authProviderConfiguration.get('Client_Id__c');
        string clientSecret = authProviderConfiguration.get('Client_Secret__c');
        string tokenEndpoint = authProviderConfiguration.get('Access_Token_Url__c');        
        string username = authProviderConfiguration.get('Username__c');
        string password = authProviderConfiguration.get('Password__c');
        string encUsrPass = EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
        Http httpClient = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(tokenEndpoint);
        req.setMethod('POST');
        req.setHeader('Authorization', 'Basic ' + encUsrPass);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setTimeout(HTTP_TIMEOUT_MILLIS);
        string payload = string.format('password={0}&username={1}&grant_type=password', new string[] { 
                                           EncodingUtil.urlEncode(clientSecret, 'UTF-8'), 
                                           EncodingUtil.urlEncode(clientId, 'UTF-8')
                                       });
        req.setBody(payload);
        HttpResponse res = httpClient.send(req);
        if(res.getStatusCode()!=200) {
            SblAuthPluginException tpException = new SblAuthPluginException('Token request returned ' + res.getStatus() + ' ' + res.getBody(), ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
            ErrorHandler.logError('SblAuthPlugin', 'SblAuthPlugin.AuthProviderPlugin', tpException);
            throw tpException;
        }        
        return (Token) JSON.deserialize(res.getBody(), Token.class);
    }
    
    private class Token {
    	public string access_token;
    	public string token_type;
    	public long expires_in;
    	public string refresh_token;
    }
    
    public class SblAuthPluginException extends ErrorHandler.GenericException {
        public SblAuthPluginException(string message, ErrorHandler.ErrorCode errorCode) {
            super(message, errorCode);
        }       
    }      
}