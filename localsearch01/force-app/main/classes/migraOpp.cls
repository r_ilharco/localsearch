global class migraOpp implements Database.Batchable<AggregateResult>, Database.Stateful  {
    
    public Integer num = 0;
    public Date endDate;
    public string oppName;
    public string batch;
    
    public migraOpp(string name, string batchName, date dt){
        oppName = name;
        batch = batchName;
        endDate = dt;
    }
	
	global Iterable<AggregateResult> start(Database.BatchableContext BC) {
		String query = 'SELECT Account__c FROM Place__c WHERE Quote_Migration_Batch__c = \''+ batch + '\' and status__c != \'disabled\' and Account__c != null group by Account__c ';		// Where OwnerId=\'0051r000008zov8AAA\' SL_Migration__c = true order by GoldenRecordID__c desc
		return new AggregateResultIterable(query); 
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
        num += 1;
        System.debug('##'+ num);
        List<Opportunity> lstOpp = new List<Opportunity>(); 
        Integer i = 0;
    	for(sObject sObj : scope) {
            AggregateResult ar = (AggregateResult)sObj;
            Opportunity opp = new Opportunity();
            opp.Name = oppName + ' - ' + num; 
            opp.AccountId = ar.get('Account__c').toString();
            opp.CloseDate = endDate; //Date.Today(); //(Date) ar.get('SLmig_StartDate__c');  
            opp.StageName = 'Proposal';
            opp.Quote_Migration_Batch__c = batch;
			i++;
            lstOpp.add(opp);
		}
        System.debug('lstOpp'+ lstOpp);
		insert lstOpp;
		//
	}
	
	global void finish(Database.BatchableContext BC) {
        
    }
    
}