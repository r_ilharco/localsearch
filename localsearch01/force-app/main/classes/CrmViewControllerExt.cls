public class CrmViewControllerExt {
    
	private final Account account;
    
    public CrmViewControllerExt(ApexPages.StandardController controller) {
        try {
            this.account = [SELECT id, Customer_Number__c FROM Account WHERE Id=:controller.getRecord().get('Id').toString() LIMIT 1];
        } catch (Exception ex) {}
    }
    
    public string getHtml() {
        return BillingResults.getCrmViewHtml(this.account);
    }
}