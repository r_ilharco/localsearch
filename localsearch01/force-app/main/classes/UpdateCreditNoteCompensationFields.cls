public class UpdateCreditNoteCompensationFields {
    @InvocableMethod(label='UpdateCreditNoteCompensationFields' description='UpdateCreditNoteCompensationFields')
    public static void updateCreditNoteCompensationFields(List<Id> recordIds){
        List<Credit_Note__c> creditNotes = [SELECT 
                                            		Id
                                            		,Contract__r.OwnerAtAutorenewal__c
                                            		,Contract__r.SBQQ__Quote__r.AccountOwner__c
                                            		,Contract__r.CompanySignedDate
                                            		,Contract__r.CompanySignedId
                                            FROM 	Credit_Note__c
                                            WHERE 	Id IN :recordIds];
        if(!creditNotes.isEmpty()){
            for(Credit_Note__c currentCreditNote : creditNotes){
                currentCreditNote.OwnerAtAutorenewal__c = currentCreditNote.Contract__r.OwnerAtAutorenewal__c;
                currentCreditNote.OwnerOnSignature__c = currentCreditNote.Contract__r.SBQQ__Quote__r.AccountOwner__c;
                currentCreditNote.CompanySignedDate__c = currentCreditNote.Contract__r.CompanySignedDate;
                currentCreditNote.CompanySignedBy__c = currentCreditNote.Contract__r.CompanySignedId;
                currentCreditNote.OriginalSeller__c = currentCreditNote.CompanySignedBy__c;
            }
            CreditNoteTriggerHandler.disableTrigger = true;
            update creditNotes;
            CreditNoteTriggerHandler.disableTrigger = false;
        }
    }
}