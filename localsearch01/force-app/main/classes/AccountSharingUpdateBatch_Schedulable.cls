global class AccountSharingUpdateBatch_Schedulable implements Schedulable{

    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        //if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'QuoteTerritoryChangedBatch%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
             if(batchs.isEmpty()|| !batchs.contains('QuoteTerritoryChangedBatch0')){
                QuoteTerritoryChangedBatch b0 = new QuoteTerritoryChangedBatch(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('QuoteTerritoryChangedBatch0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch1')){ 
                QuoteTerritoryChangedBatch b1 = new QuoteTerritoryChangedBatch(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch2')){
                QuoteTerritoryChangedBatch b2 = new QuoteTerritoryChangedBatch(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch3')){
                QuoteTerritoryChangedBatch b3 = new QuoteTerritoryChangedBatch(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch4')){
                QuoteTerritoryChangedBatch b4 = new QuoteTerritoryChangedBatch(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch5')){
                QuoteTerritoryChangedBatch b5 = new QuoteTerritoryChangedBatch(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch6')){
                QuoteTerritoryChangedBatch b6 = new QuoteTerritoryChangedBatch(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch7')){
                QuoteTerritoryChangedBatch b7 = new QuoteTerritoryChangedBatch(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch8')){
                QuoteTerritoryChangedBatch b8 = new QuoteTerritoryChangedBatch(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('QuoteTerritoryChangedBatch9')){
                QuoteTerritoryChangedBatch b9 = new QuoteTerritoryChangedBatch(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('QuoteTerritoryChangedBatch9', strSchedule,b9);
            }
        //}
    }   
}