global class ResetApiToken_Schedule implements Schedulable {

    global void execute(SchedulableContext sc)
    { 
        UtilityToken.ResetTokenSch();
    }
}