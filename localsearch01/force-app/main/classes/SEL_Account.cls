/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Luciano di Martino <ldimartino@deloitte.it>
 * Date        : 14-05-2019
 * Sprint      : 1
 * User Story  : 24
 * Testclass   : Test_CustomerInBoundApi
 * Package     : 
 * Description : 
 * Changelog   : 
                #1: SF2-253 - Validate Account and Billing Address on Quote Creation
                    SF2-264 - Legal Address Validation
                    gcasola 07-22-2019
 */

public class SEL_Account{
    public static Account getAccount(String accGoldenRecordId){
        return [SELECT Id, Name, BillingStreet, BillingCity, 
                    BillingState, BillingPostalCode, BillingCountry, 
                    ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, 
                    ShippingCountry, Phone, Email__c, GoldenRecordID__c, LCM_NX_MergeIDs__c, 
                    POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, Status__c,
                    Mobile_Phone__c, Fax, Website, UID__c, SalesChannel__c, ClientRating__c
                    FROM Account 
                    WHERE GoldenRecordID__c = :accGoldenRecordId 
                	OR Customer_Number__c = :accGoldenRecordId 
                ];
    }
    
    //A. L. Marotta 24-05-19
    //US #90 Wave 1, Sprint 2
    public static Account getAccountById(Id accountId){
        return [SELECT Id, Name, BillingStreet, BillingCity, 
                    BillingState, BillingPostalCode, BillingCountry, 
                    ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, 
                    ShippingCountry, Phone, Email__c, GoldenRecordID__c, LCM_NX_MergeIDs__c, 
                    POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, Status__c,
                    Mobile_Phone__c, Fax, Website, UID__c, SalesChannel__c, ClientRating__c, AddressValidated__c,
                    HRID__c, VAT_ID__c, Legal_Form__c, Address_Integration__c, SkipAddressValidation__c
                    FROM Account 
                    WHERE Id = :accountId                
               ];
    }
    
    public static Map<Id,Account> getAccountsById(Set<Id> ids)
    {
        return new Map<Id, Account>([SELECT Id, Name, BillingStreet, BillingCity, OwnerId, Owner.AssignedTerritory__c, TerritoryException__c,
                    BillingState, BillingPostalCode, BillingCountry, 
                    ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, 
                    ShippingCountry, Phone, Email__c, GoldenRecordID__c, LCM_NX_MergeIDs__c, 
                    POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, Status__c,
                    Mobile_Phone__c, Fax, Website, UID__c, SalesChannel__c, ClientRating__c, AddressValidated__c,
                    HRID__c, VAT_ID__c, Legal_Form__c, Address_Integration__c,CreatedDate,isDuplicated__c,MarkedAsNotDuplicated__c, Customer_Number__c, SkipAddressValidation__c
                    FROM Account 
                    WHERE Id IN :ids                
               ]);
    }
}