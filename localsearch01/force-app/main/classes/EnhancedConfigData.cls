public class EnhancedConfigData {
    @AuraEnabled public String optionId {get;set;}
    @AuraEnabled public String recordId {get;set;}  
    @AuraEnabled public String quoteId {get;set;}
    @AuraEnabled public String quoteType {get;set;}
    @AuraEnabled public String upDownContract {get;set;}
    @AuraEnabled public String startDateFromJSON {get;set;}
    @AuraEnabled public String startDatePrint {get;set;}
    @AuraEnabled public String accountId {get;set;}
    @AuraEnabled public String lineItemId {get;set;}
    @AuraEnabled public Map<String,Integer> optionIndexMap {get;set;}
    @AuraEnabled public Map<String,String> optionExtIdMap {get;set;}
    @AuraEnabled public Map<String,String> locationIdToPrice {get;set;}
    @AuraEnabled public Map<String,String> categoryIdToPrice {get;set;}
    @AuraEnabled public String masterProductCode {get;set;}
    @AuraEnabled public String masterProdExternalCode {get;set;}
    @AuraEnabled public String masterProductName {get;set;}
    @AuraEnabled public Boolean asapAllowed {get;set;}
    @AuraEnabled public String minDate {get;set;}
    @AuraEnabled public String maxDate {get;set;}
    @AuraEnabled public Result result {get;set;}
    @AuraEnabled public String selectedSubscriptionTerm {get;set;}
    @AuraEnabled public String selectedBillingFrequency {get;set;}
    @AuraEnabled public String localGuideNumb {get;set;}
    @AuraEnabled public String place {get;set;}
    @AuraEnabled public String placeId {get;set;}
    @AuraEnabled public String defaultPlaceName {get;set;}
    @AuraEnabled public String externalPlaceId {get;set;}
    @AuraEnabled public String placeUrl {get;set;}
    @AuraEnabled public String AMAurl {get;set;}
    @AuraEnabled public String localCHurl {get;set;}
    @AuraEnabled public String searchCHurl {get;set;}
    @AuraEnabled public String cCurl {get;set;}
    @AuraEnabled public String telEditorurl {get;set;}
    @AuraEnabled public String copyLocalUrl {get;set;}
    @AuraEnabled public String defaultCategoryId {get;set;}
    @AuraEnabled public String defaultLocationId {get;set;}
    @AuraEnabled public String defaultCategory {get;set;}
    @AuraEnabled public String defaultLocation {get;set;}
    @AuraEnabled public String defaultLanguage {get;set;}
    @AuraEnabled public String defaultContact {get;set;}
    @AuraEnabled public String defaultEditionId {get;set;}
    @AuraEnabled public String defaultEditionName {get;set;}
    @AuraEnabled public String allocationId {get;set;}
    @AuraEnabled public String defaultKeywords {get;set;}
    @AuraEnabled public String defaultDescription {get;set;}
    @AuraEnabled public Map<String, String> parametersForCasa {get;set;}
    @AuraEnabled public String pickupNumber {get;set;}
    @AuraEnabled public String defaultPlacementPhone {get;set;}
	@AuraEnabled public Date contractEndDate {get; set;}
    @AuraEnabled public Boolean agency {get;set;}
    @AuraEnabled public Boolean placeOfOtherAccount{get;set;}
    @AuraEnabled public Boolean parent {get;set;}
    @AuraEnabled public String parentId {get;set;}
    @AuraEnabled public Boolean placeIdRequired {get;set;}
    @AuraEnabled public Boolean showplaceId {get;set;}
    @AuraEnabled public String defaultStartDate {get;set;}
    @AuraEnabled public String defaultBillingFrequency {get;set;}
    @AuraEnabled public String defaultSubscriptionTerm {get;set;}
    @AuraEnabled public String guidelineDescription {get;set;}
    @AuraEnabled public String productGroup {get;set;}
    @AuraEnabled public String Target_Url {get;set;}
    @AuraEnabled public String Company_location {get;set;}
    @AuraEnabled public String Comment_field {get;set;}
    @AuraEnabled public String Campaign_language {get;set;}
    
    public EnhancedConfigData() {
        optionExtIdMap = new Map<String,String>();
        optionIndexMap = new Map<String,Integer>();
        locationIdToPrice = new Map<String, String>();
        categoryIdToPrice = new Map<String, String>();
        parametersForCasa = new Map<String, String>();
        parent = false;
        agency = false;
        result = new Result();
    }        
}