/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-20-2019
 * Sprint      : 1
 * Work item   : US_19
 * Testclass   :
 * Package     : 
 * Description : Selector Class for Category_Location__c
 * Changelog   : 
 */

public with sharing class SEL_CategoryAndLocation {
    public static List<Category_Location__c> getLocationsByPlace(Id id)
    {
        return [SELECT Id, mainLocation__c ,Category_Location__r.Location_it__c, Category_Location__r.Location_fr__c,
                       Category_Location__r.Location_de__c, Category_Location__r.Location_en__c, Category_Location__r.refId__c 
                FROM Category_Location__c 
                WHERE Place__c = :id 
                AND RecordType.DeveloperName = :ConstantsUtil.RELATION_RT_DEVELOPERNAME 
                AND Category_Location__r.RecordType.DeveloperName=:ConstantsUtil.LOCATION_RT_DEVELOPERNAME];
    }

    public static List<Category_Location__c> getCategoriesByPlace(Id id)
    {
        return [SELECT Id, Category_Location__r.Category_it__c, Category_Location__r.Category_fr__c, 
                       Category_Location__r.Category_de__c, Category_Location__r.Category_en__c, Category_Location__r.refId__c
                FROM Category_Location__c 
                WHERE Place__c = :id 
                AND RecordType.DeveloperName = :ConstantsUtil.RELATION_RT_DEVELOPERNAME 
                AND Category_Location__r.RecordType.DeveloperName=:ConstantsUtil.CATEGORY_RT_DEVELOPERNAME];
    }

    public static List<Category_Location__c> getLocationsByPrivatePlace(Id id)
    {
        return [SELECT Id, mainLocation__c, Category_Location__r.Location_it__c, Category_Location__r.Location_fr__c,
                       Category_Location__r.Location_de__c, Category_Location__r.Location_en__c 
                FROM Category_Location__c 
                WHERE PlacePrivate__c = :id 
                AND RecordType.DeveloperName = :ConstantsUtil.RELATION_RT_DEVELOPERNAME 
                AND Category_Location__r.RecordType.DeveloperName=:ConstantsUtil.LOCATION_RT_DEVELOPERNAME];
    }

    public static List<Category_Location__c> getCategoriesByPrivatePlace(Id id)
    {
        return [SELECT Id, Category_Location__r.Category_it__c, Category_Location__r.Category_fr__c, 
                       Category_Location__r.Category_de__c, Category_Location__r.Category_en__c 
                FROM Category_Location__c 
                WHERE PlacePrivate__c = :id 
                AND RecordType.DeveloperName = :ConstantsUtil.RELATION_RT_DEVELOPERNAME 
                AND Category_Location__r.RecordType.DeveloperName=:ConstantsUtil.CATEGORY_RT_DEVELOPERNAME];
    }

    public static List<Category_Location__c> getCategoriesAndLocationsByRefIds(Set<String> refIds)
    {
        String query = 'SELECT Id, refId__c, RecordType.DeveloperName ' +
                        ' FROM Category_Location__c' + 
                        ' WHERE (RecordType.DeveloperName = \'' + ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + '\'' + 
                                ' OR RecordType.DeveloperName = \'' + ConstantsUtil.LOCATION_RT_DEVELOPERNAME + '\')' +
                        ' AND refId__c IN :refIds';
        
        System.debug(query);
        
        return Database.query(query);
    }

    public static List<Category_Location__c> getCategoriesAndLocationsByRefIds(Id placeId, Set<String> refIds) //, Boolean notIn)
    {
        String query = 'SELECT Id, Category_Location__r.Id, Category_Location__r.refId__c ' +
                        ' FROM Category_Location__c' + 
                        ' WHERE Place__c = \'' + placeId + '\'' + 
                        ' AND RecordType.DeveloperName = \'' + ConstantsUtil.RELATION_RT_DEVELOPERNAME + '\'' +
                        ' AND (Category_Location__r.RecordType.DeveloperName = \'' + ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + '\'' + 
                                ' OR Category_Location__r.RecordType.DeveloperName = \'' + ConstantsUtil.LOCATION_RT_DEVELOPERNAME + '\')' +
                        ' AND Category_Location__r.refId__c IN :refIds';
                        //(notIn?'NOT ':'') + 'IN :refIds';
        
        //System.debug(query);
        
        return Database.query(query);
    }
    
    public static List<Category_Location__c> getCategoriesAndLocationsByhashCodes(Set<Integer> hashCodes)
    {
        String query = 'SELECT Id, clHashCode__c, RecordType.DeveloperName ' +
                        ' FROM Category_Location__c' + 
                        ' WHERE (RecordType.DeveloperName = \'' + ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + '\'' + 
                                ' OR RecordType.DeveloperName = \'' + ConstantsUtil.LOCATION_RT_DEVELOPERNAME + '\')' +
                        ' AND clHashCode__c IN :hashCodes';
        
        System.debug(query);
        
        return Database.query(query);
    }

    public static List<Category_Location__c> getCategoriesAndLocationsByhashCodes(Id placeId, Set<Integer> hashCodes) //, Boolean notIn)
    {
        String query = 'SELECT Id, Category_Location__r.Id, Category_Location__r.clHashCode__c ' +
                        ' FROM Category_Location__c' + 
                        ' WHERE Place__c = \'' + placeId + '\'' + 
                        ' AND RecordType.DeveloperName = \'' + ConstantsUtil.RELATION_RT_DEVELOPERNAME + '\'' +
                        ' AND (Category_Location__r.RecordType.DeveloperName = \'' + ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + '\'' + 
                                ' OR Category_Location__r.RecordType.DeveloperName = \'' + ConstantsUtil.LOCATION_RT_DEVELOPERNAME + '\')' +
                        ' AND Category_Location__r.clHashCode__c IN :hashCodes';
                        //(notIn?'NOT ':'') + 'IN :refIds';
        
        //System.debug(query);
        
        return Database.query(query);
    }
}