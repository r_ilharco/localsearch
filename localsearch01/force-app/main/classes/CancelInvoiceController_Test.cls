@IsTest
public class CancelInvoiceController_Test {
    
    @testSetup
    private static void testData(){

        TestUtils.Mock data = TestUtils.Mocks[1];
        Account account = new Account(Name=data.company, BillingStreet=data.street[0] + ' ' + data.street_number[0], BillingState=data.state[0], 
                BillingCity=data.city[0], BillingPostalCode = data.postal_code[0], BillingCountry = data.country, 
            	GoldenRecordID__c='A-GR-000', Type='Kunde', Customer_number__c = data.customer_number.replace('-',''));        	 
        
        insert account;
        Contact contact = new Contact(FirstName=data.first_name, LastName=data.last_name, email=data.email, GoldenRecordID__c='C-GR-000', 
                   MailingCity=data.city[1], MailingCountry=data.country, MailingPostalCode=data.postal_code[1], MailingState=data.state[1], 
                   MailingStreet=data.street[1] + ' ' + data.street_number[1], Language__c='Italian', AccountId = account.Id);
        insert contact;
        Invoice__c invoice = new Invoice__c (
        	Customer__c = account.Id, Correlation_Id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B506', Billing_Channel__c = 'Print', Booking_Status__c = ConstantsUtil.ABACUS_STATUS_SUCCESS, 
            Invoice_Code__c = 'FAK', Status__c = ConstantsUtil.INVOICE_STATUS_PAYED
        );
        insert invoice;
        


    }


	private static testmethod void test_001() {
        System.assert(null == CancelInvoiceController.getInvoice(null), 'Null invoice Id should return null invoice');
        System.assert(null == CancelInvoiceController.getInvoice('a1J000000000000AAA'), 'Invalid invoice Id should return null invoice');
        TestUtils.Mock data = TestUtils.Mocks[0];
        Account account = new Account(Name=data.company, BillingStreet=data.street[0] + ' ' + data.street_number[0], BillingState=data.state[0], 
                BillingCity=data.city[0], BillingPostalCode = data.postal_code[0], BillingCountry = data.country, 
            	GoldenRecordID__c='A-GR-001', Type='Kunde', Customer_number__c = data.customer_number.replace('-',''));        	 
        
		insert account;
        Contact contact = new Contact(FirstName=data.first_name, LastName=data.last_name, email=data.email, GoldenRecordID__c='C-GR-001', 
                   MailingCity=data.city[1], MailingCountry=data.country, MailingPostalCode=data.postal_code[1], MailingState=data.state[1], 
                   MailingStreet=data.street[1] + ' ' + data.street_number[1], Language__c='Italian', AccountId = account.Id);
        insert contact;
        Invoice__c invoice = new Invoice__c (
        	Customer__c = account.Id, Correlation_Id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B505', Billing_Channel__c = 'Print', Booking_Status__c = ConstantsUtil.ABACUS_STATUS_SUCCESS, 
            Invoice_Code__c = 'FAK', Status__c = ConstantsUtil.INVOICE_STATUS_PAYED
        );
        insert invoice;
        Invoice__c foundInvoice = CancelInvoiceController.getInvoice(invoice.Id);
        System.assertEquals(invoice.Id, foundInvoice.Id, 'Invoice not found');

        List<Invoice_Order__c> invOrds = Test_DataFactory.createInvoiceOrders(1, foundInvoice.Id);
        insert invOrds;
		Test.startTest();
        Test_Billing.SwissBillingHttpResponseMock jsonResponse = new Test_Billing.SwissBillingHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        string res = CancelInvoiceController.cancelByInvoiceId(foundInvoice.Id);
        Test.stopTest();
    }

    @isTest
    private static void testChangeStatusOnAlreadyCancelledInvoiceSWB() {

        List<Invoice__C> invoice = [select id from invoice__c where correlation_id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B506'];
        //invoice
        CancelInvoiceController.changeStatusOnAlreadyCancelledInvoiceSWB(invoice[0]);

    }


    @isTest
    private static void testCancelInvoicesIfAlreadyCancelled() {

        List<Invoice__C> invoice = [select id from invoice__c where correlation_id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B506'];

        Test.startTest();
        Test_Billing.SwissBillingHttpResponseMock2 jsonResponse = new Test_Billing.SwissBillingHttpResponseMock2();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        string res = CancelInvoiceController.cancelByInvoiceId(invoice[0].Id);
        Test.stopTest();

    }

    

}