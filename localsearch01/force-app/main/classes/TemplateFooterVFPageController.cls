/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Gennaro Casola <gcasola@deloitte.it>
 * Date        : 08-16-2019
 * Sprint      : Sprint 7
 * Work item   : SF2-321, Sprint 7, Wave D - Quote and Contract Template
 * Testclass   : 
 * Package     : 
 * Description :
 * Changelog   : SF2-345, Sprint 9, Wave E - Update of Quote and Contract Template - gcasola - 09-17-2019
 */

public class TemplateFooterVFPageController {
    
    public Configuration conf {get; set;}
    public String language {get; set;}
    public String templateLanguage {get; set;}
    public Id quoteId {get;set;}
    public Id templateId {get;set;}
    public SBQQ__QuoteTemplate__c template {get; set;}
    public SBQQ__Quote__c quote {get; set;}
    public User salesRep {get; set;}
    public Account customer {get; set;}
    public Contact primaryContact {get; set;}
    //public String legalContractTerm {get; set;}
    public List<String> legalContractTerms {get; set;}
    public String legalContractTerms_orig {get; set;}
    public Map<Id, SBQQ__Localization__c> quoteTermsTranslation {get; set;}
    public String LSFooterBannersLink {get; set;}
    public Boolean isTeleSalesTemplate {get; set;}
    
    public TemplateFooterVFPageController()
    {
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        templateId = (Id)ApexPages.currentPage().getParameters().get('tid');
        language = (String)ApexPages.currentPage().getParameters().get('language');
        
        //quoteId = 'a0q1X000002DT6YQAW';
		//templateId = 'a0o1X000000ODWBQA4';
        
        TemplateFooterVFPageController(quoteId, templateId, language);
        
    }
    
    public TemplateFooterVFPageController(Id quoteId, Id templateId, String language)
    {
        TemplateFooterVFPageController(quoteId, templateId, language);
    }
    
    public void TemplateFooterVFPageController(Id quoteId, Id templateId, String language)
    {
        List<QuoteDocumentDebuggerSetting__mdt> qdbsList = [SELECT Id, DeveloperName, UserId__c, QuoteId__c, Language__c, TemplateId__c FROM QuoteDocumentDebuggerSetting__mdt WHERE UserId__c = :UserInfo.getUserId() AND IsActive__c = true LIMIT 1];
        
        if(Test.isRunningTest()){
            QuoteDocumentDebuggerSetting__mdt qdbs = new QuoteDocumentDebuggerSetting__mdt();
            qdbs.UserId__c = UserInfo.getUserId();
           	qdbs.QuoteId__c = quoteId;
            qdbs.Language__c = language;
            qdbs.TemplateId__c = templateId;
            
            qdbsList = new List<QuoteDocumentDebuggerSetting__mdt>();
            qdbsList.add(qdbs);
        }
        
        if(qdbsList != NULL && qdbsList.size() == 1 && qdbsList[0].UserId__c == UserInfo.getUserId()){
            quoteId = qdbsList[0].QuoteId__c;
            templateId = qdbsList[0].TemplateId__c;
            language = qdbsList[0].Language__c;
            this.quoteId = qdbsList[0].QuoteId__c;
            this.templateId = qdbsList[0].TemplateId__c;
            this.language = qdbsList[0].Language__c;
        }
        
        /*if(language != 'de' && language != 'it' && language !='fr')
            language = 'de';*/

        LSFooterBannersLink = URL.getSalesforceBaseUrl().toExternalForm().split('\\.')[0] + '.documentforce.com/servlet/servlet.ImageServer?id=' + DynamicContentTemplateBuilder.refIdLSBanner + '&oid=' + UserInfo.getOrganizationId(); 
        
        conf = new Configuration();
        conf.border.width = QuoteLinesVFPageController.conf_border_width;
        conf.border.style = 'solid';
        conf.font.family = ConstantsUtil.QUOTETEMPLATE_FONT;
        
        quote = SEL_Quote.getQuoteById(new Set<Id>{quoteId}).values()[0];
        customer = SEL_Account.getAccountsById(new Set<Id>{quote.SBQQ__Account__c}).values()[0];
        template = SEL_QuoteTemplate.getTemplatesById(new Set<Id>{templateId}).values()[0];
        legalContractTerms = new List<String>();
        
        templateLanguage = template.Name.substringAfterLast('-').trim();
        
        if(template.Name.containsIgnoreCase(ConstantsUtil.TEMPLATENAME_SUBSTRING_TELESALES))
            isTeleSalesTemplate = true;
        else
            isTeleSalesTemplate = false;
        
        if(String.isNotBlank(templateLanguage) && (templateLanguage.equalsIgnoreCase('it') 
               ||
               templateLanguage.equalsIgnoreCase('fr')
               ||
               templateLanguage.equalsIgnoreCase('de')))
		{
            language = templateLanguage.toLowerCase();
        }else
        {
			if(language != 'de' && language != 'it' && language !='fr')
				language = 'de';
        }
        
        this.language = language;
       
        if(quote.Filtered_Primary_Contact__c != NULL)
        {
            primaryContact = SEL_Contact.getContactsById(new Set<Id>{quote.Filtered_Primary_Contact__c}).values()[0];
        }
        salesRep = SEL_User.getUsersById(new Set<Id>{quote.SBQQ__SalesRep__c}).values()[0];
        quoteTermsTranslation = SEL_Localization.getQuoteTemplateTranslations(new Set<Id>{templateId}, new List<String>{'SBQQ__TermsConditions__c'}, language);
        
        if(template.SBQQ__TermsConditions__c != NULL)
        {
            if(quoteTermsTranslation.size() > 0)
            {
                legalContractTerms_orig = quoteTermsTranslation.values()[0].SBQQ__LongTextArea__c;
                //legalContractTerm = quoteTermsTranslation.values()[0].SBQQ__LongTextArea__c.replaceAll('\n\n', '&#xA;');
            }else
            {   
                legalContractTerms_orig = template.SBQQ__TermsConditions__c;
                //legalContractTerm = template.SBQQ__TermsConditions__c.replaceAll('\n\n', '&#xA;');
            }
        }
        legalContractTerms = legalContractTerms_orig.split('\n');
    }
    
    
    
    public class Configuration
    {
        public Border border {get; set;}
        public Font font {get; set;}
        
        Configuration()
        {
            border = new Border();
            font = new Font();
        }
    }
    
    public class Border{
        public String width {get; set;}
        public String style {get; set;}
    }
    
    public class Font{
        public String family{get; set;}
    }
}