/**
 * Author	   : Orlando Aversano <oaversano@deloitte.it>
 * Date		   : 11-10-2019
 * Sprint      : 11 Wave F
 * Work item   : OrderProductTriggerHandler
 * Testclass   : Test_OrderProductTrigger
 * Package     : 
 * Description : Trigger Handler on OrderItem
 * Changelog   : 
 */

public class OrderProductTriggerHandler implements ITriggerHandler{
	public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        OrderProductTriggerHelper.calculateOrderItemTotal((List<OrderItem>)newItems);
        OrderProductTriggerHelper.downsellingDateCalculation((List<OrderItem>)newItems);
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, OrderItem> mapOI = (Map<Id, OrderItem>) newItems;
        Set<Id> orderIds = new Set<Id>();
       	
        for(OrderItem newOi : mapOI.values()) orderIds.add(newOi.OrderId);
        
        List<Order> relatedOrder = new List<Order>();
        relatedOrder = [SELECT Id, Custom_Type__c, Type FROM Order WHERE Id IN: orderIds];
        
        If(!relatedOrder[0].Type.equalsIgnoreCase(ConstantsUtil.ORDER_TYPE_TERMINATION)){
        	OrderProductTriggerHelper.updateContractingMethod((Map<Id, OrderItem>) newItems,(Map<Id, OrderItem>) oldItems);
        }
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    public void AfterInsert(Map<Id, SObject> newItems) {
        OrderProductTriggerHelper.updateReservationEndDate((List<OrderItem>)newItems.values());
        OrderProductTriggerHelper.setOrderProducts((List<OrderItem>)newItems.values());
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    public class CustomException extends Exception {}
}