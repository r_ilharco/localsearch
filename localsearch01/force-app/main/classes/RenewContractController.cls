/** ──────────────────────────────────────────────────────────────────────────────────────────────────
* @TestClass      Test_RenewContractController 
* @changes		  SPIII-2064 - Add a validation Check on Renewal, Amend and Upgrade button on Contract
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-29
/** ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public without sharing class RenewContractController {
	
    @AuraEnabled
    public static RenewContractWrapper renewContract(String recordId){
        
        RenewContractWrapper renewContractWrapper = new RenewContractWrapper();
        
        try{
            User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
            Set<Id> accountTeamMembers = new Set<Id>();
            List<Contract> contractToRenew = [SELECT Id, SBQQ__Evergreen__c, Account.OwnerId,AccountId, SBQQ__RenewalQuoted__c,SBQQ__RenewalOpportunity__c, Status, EndDate, In_Termination__c, (SELECT Id, SBQQ__Product__r.Family FROM SBQQ__Subscriptions__r) FROM Contract WHERE Id = :recordId LIMIT 1];
            if(!contractToRenew.isEmpty()){
                for(AccountTeamMember member : [select id, UserId from AccountTeamMember where Accountid =: contractToRenew[0].AccountId ]){
                    accountTeamMembers.add(member.UserId);
                }	
                if(contractToRenew[0].SBQQ__Subscriptions__r.size() > 0){
                    for(SBQQ__Subscription__c sub : contractToRenew[0].SBQQ__Subscriptions__r){
                        if(sub.SBQQ__Product__r.Family == ConstantsUtil.PRODUCT2_FAMILY_DISUSED){
                            renewContractWrapper.success = false;
                            renewContractWrapper.errorMessage = Label.DISUSED_NOT_RENEWABLE;//Label.DISUSED_NOT_RENEWABLE;  
                            return renewContractWrapper;
                        }
                    }
                }
                if(contractToRenew[0].Account.OwnerId != currentUser.id 
               && currentUser.Profile.Name!= ConstantsUtil.SysAdmin
               && currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_DMC
               && !accountTeamMembers.contains(currentUser.ID)){
                   renewContractWrapper.success = false;
                   renewContractWrapper.errorMessage = Label.Upgrade_Downgrade_Amend_Validation;
                   return renewContractWrapper;
               }
                else if(!ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(contractToRenew[0].Status)){
                    renewContractWrapper.success = false;
                    renewContractWrapper.errorMessage = Label.RENEW_ContractNotActive;  
                    return renewContractWrapper;
                }else if(contractToRenew[0].In_Termination__c){
                    renewContractWrapper.success = false;
                    renewContractWrapper.errorMessage = Label.RENEW_ContractInTermination;  
                    return renewContractWrapper;
                }
                if(contractToRenew[0].SBQQ__Evergreen__c == false){
                    List<SBQQ__Subscription__c> disusedSub = [select id from SBQQ__Subscription__c where SBQQ__Product__r.Family  =: ConstantsUtil.PRODUCT2_FAMILY_DISUSED
                                                              and SBQQ__RequiredById__c = null and SBQQ__Contract__c = :recordId];
                    if(!disusedSub.isEmpty()){
                        renewContractWrapper.success = false;
                        renewContractWrapper.errorMessage = Label.RENEW_ContractEndDateMissing;
                        return renewContractWrapper;
                    }
                    Date maxDate = Date.today().addMonths(12);
                    if(contractToRenew[0].SBQQ__RenewalQuoted__c){
                        //List<Opportunity> renewalOpportunity = [SELECT Id FROM Opportunity WHERE SBQQ__RenewedContract__c =: recordId ORDER BY CreatedDate DESC LiMIT 1];                     
                        if(String.isNotBlank(contractToRenew[0].SBQQ__RenewalOpportunity__c)){
                            renewContractWrapper.success = false;
                            renewContractWrapper.errorMessage = Label.RENEW_OppAlreadyExist; 
                            renewContractWrapper.renewalOpportunityUrl = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + contractToRenew[0].SBQQ__RenewalOpportunity__c;
                        }
                        else{
                            renewContractWrapper.success = false;
                            renewContractWrapper.errorMessage = Label.Generic_Error;
                        }
                    }
                    else if(contractToRenew[0].EndDate > maxDate){
                        renewContractWrapper.success = false;
                        renewContractWrapper.errorMessage = Label.RENEW_EndDateGreaterThan12;
                    }
                    else{
                        contractToRenew[0].SBQQ__RenewalQuoted__c = true;
                        update contractToRenew[0];
                        List<Opportunity> renewalOpportunity = [SELECT Id FROM Opportunity WHERE StageName != 'Closed Lost' AND SBQQ__RenewedContract__c =: recordId ORDER BY CreatedDate DESC LiMIT 1];
                        if(!renewalOpportunity.isEmpty()){
                            renewContractWrapper.success = true;
                            renewContractWrapper.renewalOpportunityUrl = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + renewalOpportunity[0].Id;
                        }
                        else{
                            renewContractWrapper.success = false;
                            renewContractWrapper.errorMessage = Label.Generic_Error;
                        }
                    }
                }
                else{
                    renewContractWrapper.success = false;
                    renewContractWrapper.errorMessage = Label.RENEW_ContractEndDateMissing;
                }
            }
        }
        catch(Exception e){
        	renewContractWrapper.success = false;
			renewContractWrapper.errorMessage = Label.Generic_Error;
        }
        return renewContractWrapper;
    }
    
    @AuraEnabled
    public static RenewContractWrapper contractStatusCheck(String recordId){
        return null;
    }
    public class RenewContractWrapper {
        @AuraEnabled
        public Boolean success {get;set;}
        @AuraEnabled
        public String errorMessage {get;set;}
        @AuraEnabled
        public String renewalOpportunityUrl {get;set;}
    }
}