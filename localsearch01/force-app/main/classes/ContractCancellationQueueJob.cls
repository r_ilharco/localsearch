global without sharing class ContractCancellationQueueJob implements Queueable {

	private List<Contract> vlistContract;
   
    public ContractCancellationQueueJob(List<Contract> listContract) {
        vlistContract= listContract;       
    }    
  global void execute(QueueableContext SC) {
	 ContractUtility.CancellationContractJob(vlistContract);
  }
}