/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date	       : 07-12-2019
 * Sprint      : Sprint 5
 * Work item   : SF2-210, Sprint5, Wave C, Order Management.
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */

global class OrderSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
       
	List<Order> orderList = new List<Order>();
        Date dt_from = Date.valueOf(System.Today()) - Integer.valueOf(Label.Contract_job_Prior_days);
        Date dt_to = Date.valueOf(System.Today());    
        
        //Order of Activation 
        orderList = [SELECT  Id, 
                             Type,
                             OrderNumber, 
                             External_Id__c,  
                             Status, 
                             OwnerId, 
                             EffectiveDate, 
                             EndDate, 
                             AccountId, 
                             SBQQ__Quote__c,
                             Custom_Type__c 
                     FROM    Order 
                     WHERE   (  Status =: ConstantsUtil.ORDER_STATUS_DRAFT AND 
                                EffectiveDate >= :dt_from AND 
                                EffectiveDate  <= :dt_to)
                     AND     Type =: ConstantsUtil.ORDER_TYPE_ACTIVATION];

        if(!orderList.isEmpty()){
                OrderActivationQueueJob orderActivation = new OrderActivationQueueJob(orderList);
        	System.enqueueJob(orderActivation);        
        }

         
      
 	//Order of Termination 
        orderList = [   SELECT  Id, 
                     	        Type,
                                Name,
                     		OrderNumber, 
                                Status, 
                                OwnerId,
                     		External_Id__c,  
                                EffectiveDate, 
                                EndDate, 
                                AccountId, 
                                SBQQ__Quote__c,
                                Custom_Type__c
                        FROM    Order
                        WHERE   (Status =: ConstantsUtil.ORDER_STATUS_DRAFT AND 
                                EndDate <= :dt_to)
                        AND     Type =: ConstantsUtil.ORDER_TYPE_TERMINATION];

        OrderTerminationQueueJob orderTermination = new OrderTerminationQueueJob(orderList);
        System.enqueueJob(orderTermination);      

        //Order of Cancellation 
        orderList = [   SELECT  Id,
                     	        External_Id__c,
                                Status,
                                AccountId,
                                SBQQ__Quote__c,
                                    Type,
                                EffectiveDate,
                                EndDate,
                                Custom_Type__c
                        FROM    Order 
                        WHERE  (Status =: ConstantsUtil.ORDER_STATUS_DRAFT AND 
                                EffectiveDate >= :dt_from AND 
                                EffectiveDate  <= :dt_to)
                        AND    Type =: ConstantsUtil.ORDER_TYPE_CANCELLATION];

        OrderCancellationQueueJob orderCancellation = new OrderCancellationQueueJob(orderList);
        System.enqueueJob(orderCancellation);         
    }
}