/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* RetryOrderActivationController - This class is the controler of RetryOrderActivation cmp
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author        
* @created        
* @systemLayer    Controller
* @TestClass      RetryOrderActivationControllerTest
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella
* 2020-12-29      SPIII-4515 -Fulfilled Orders - New service to republish the product 
config JSON (only for localBE and searchBE) - modified in case of "Activated" 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class RetryOrderActivationController {

    @AuraEnabled
    public static String retryOrderActivation(String recordId){
        System.debug('ORDER ID --> '+recordId);
        String result = 'KO';
        String profileName;
        List<OrderItem> orderItems = new List<OrderItem>();
        Set<OrderItem> ois = new Set<OrderItem>();
        Set<String> queryFields;
        String parentOrder = '';
        String parenOrderStatus ='';
        List<Order> orderToUpdate = SEL_Order.getOrdersByIds(new List<Id>{recordId});
        if(!orderToUpdate.isEmpty()){
            if(orderToUpdate[0].Status != ConstantsUtil.ORDER_STATUS_CANCELLED){
                //parentOrder = orderToUpdate[0].Parent_Order__c; // to remove comment if you want a redirect message
                if(orderToUpdate[0].Type == ConstantsUtil.ORDER_TYPE_UPGRADE && orderToUpdate[0].Custom_Type__c == ConstantsUtil.ORDER_TYPE_ACTIVATION){
                    Opportunity opp = [select id, (select id,status from orders where type =:ConstantsUtil.ORDER_TYPE_UPGRADE and custom_type__c = :ConstantsUtil.ORDER_TYPE_TERMINATION) from opportunity where id =: orderToUpdate[0].OpportunityId limit 1];
                    if(opp != null){
                        if(opp.orders.size()>0){
                            //parentOrder = opp.orders[0].id; // to remove comment if you want a redirect message
                            parenOrderStatus= opp.orders[0].status;
                        }
                    }
                }
                if (orderToUpdate[0].Custom_Type__c == ConstantsUtil.ORDER_TYPE_ACTIVATION){
                    if(orderToUpdate[0].Contract__r.Status == ConstantsUtil.CONTRACT_STATUS_TERMINATED || orderToUpdate[0].Contract__r.Status == ConstantsUtil.CONTRACT_STATUS_CANCELLED ){
                        throw new AuraHandledException(Label.ResentToBE_contract_status_error);
                    }
                    else{ 
                        List<Order> ord = [select id from order where (custom_type__c =:ConstantsUtil.ORDER_TYPE_TERMINATION or custom_type__c =:ConstantsUtil.ORDER_TYPE_CANCELLATION)
                                           and id !=: recordId
                                           and contract__c =:orderToUpdate[0].Contract__c
                                           and status !=: ConstantsUtil.ORDER_STATUS_CANCELLED 
                                           and status !=: ConstantsUtil.ORDER_STATUS_DRAFT
                                           and contract__c != null
                                           order by createddate desc];
                        if(!ord.isEmpty()){
                            throw new AuraHandledException(Label.ResentToBE_termination_ongoing_error);
                        }
                    }
                }
                if(orderToUpdate[0].Status != ConstantsUtil.ORDER_STATUS_ACTIVATED ){
                    orderToUpdate[0].Status = ConstantsUtil.ORDER_STATUS_DRAFT;
                    update orderToUpdate[0];
                }
                else{
                    profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
                    List<PermissionSetAssignment> pSAssign = [SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId =:Userinfo.getUserId() 
                                                              and permissionSet.Name =: ConstantsUtil.ORDERMANAGEMENTPS_LABEL];
                    if(profileName != ConstantsUtil.SYSADMINPROFILE_LABEL && pSAssign.isEmpty() && profileName != ConstantsUtil.PRODUCTMANAGERPROFILE_LABEL){
                        return result;
                    }
                    queryFields = ConstantsUtil.QUERY_FIELDS;
                    orderItems = SEL_OrderItem.getOrderItemsByOrderIds(new Set<Id>{orderToUpdate[0].id}, queryFields);
                    ois.addAll(orderItems);
                    List<Order_Management__mdt> orderManagementMetadata = SRV_OrderUtil.getOrderManagementMdt(ois);
                    if(orderManagementMetadata.isEmpty()){
                        return result;
                    }
                }    
                if(orderToUpdate[0].Type == ConstantsUtil.ORDER_TYPE_UPGRADE && orderToUpdate[0].Custom_Type__c == ConstantsUtil.ORDER_TYPE_ACTIVATION){
                    if(orderToUpdate[0].Status == ConstantsUtil.ORDER_STATUS_ACTIVATED || parenOrderStatus == ConstantsUtil.ORDER_STATUS_ACTIVATED){
                        OrderUtility.changeOrderStatus(orderToUpdate, true);
                    }
                    else{
                        throw new AuraHandledException(Label.Termination_Order_Before +';'+parentOrder);
                    }
                }
                else{
                    //OrderUtility.activateContracts(new List<Id>{recordId});  
                    System.enqueueJob(new SRV_OrderManager.QueueableActivateContracts(new List<Id>{recordId})); 
                }
                result = 'OK';
                result += ';'+parentOrder;
            }
        }
        return result;
    }
}
