@isTest
public class UtilityTokenTest implements HttpCalloutMock{
    
    public HttpResponse respond(HTTPRequest req){
        String body = '{"test": "test"}';
        HttpResponse resp = new HttpResponse();
        resp.setStatus('OK');
        resp.setStatusCode(Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS));
        resp.setBody(body);
        return resp;
    }
    
    @isTest
    public static void testGgetToken(){
        try{
            Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
            UtilityToken.getToken();
        }catch(Exception e){         
           System.Assert(false, e.getMessage());
        }    
    }
    
    @isTest
    public static void testRefreshToken(){
        try{
            Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
            UtilityToken.refreshToken();
        }catch(Exception e){         
           System.Assert(false, e.getMessage());
        }    
    }
    
    @isTest
    public static void testGetAccessToken(){
        try{
            Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
            UtilityToken.accessTokenInformation token = UtilityToken.getAccessToken(ConstantsUtil.SERVICE_WINDREAM_TOKEN);
            string ss = token.access_token;
            string tokenType = token.token_type;
            integer expiresIn = token.expires_in;
        }catch(Exception e){         
           System.Assert(false, e.getMessage());
        }    
    }
    
    @isTest
    public static void testResetTokenSch(){
        try{
            Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
            UtilityToken.ResetTokenSch();
        }catch(Exception e){         
           System.Assert(false, e.getMessage());
        }    
    }
    
    @isTest
    public static void getTokenOptm(){
        try{
            Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
            UtilityToken.getTokenOptm();
        }catch(Exception e){         
           System.Assert(false, e.getMessage());
        }    
    }
   

}