/*
* SPIII 1853 - Places Related List on Account 
* TestClass: Test_RelatedListController
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mazzarella Mara <mamazzarella@deloitte.it>
* @created        2020-07-17
* @systemLayer    Controller 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/

public without sharing class RelatedListController{
    
    @AuraEnabled
    public static List<Place__c> getRecordList( Id recId) {  
        Set<Id> recIds = new Set<Id>();
        List<Place__c> listsObjects = new List<Place__c>();
        List<Account> childAccount  = new List<Account> ();
        try{
            recIds.add(recId);
            childAccount = [select id from Account where ParentId =:recId];
            if(!childAccount.isEmpty()){
                for(Account acc: childAccount){
                    recIds.add(acc.Id);
                }
            }
            listsObjects= [SELECT Id,Account__c, Name, Phone__c,SearchField_Address__c, Account__r.Name FROM Place__c where Account__c =:recIds];     
        }
        catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return listsObjects;            
    }  
}