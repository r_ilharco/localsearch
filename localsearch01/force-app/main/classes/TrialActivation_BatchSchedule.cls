/* It is recommended to schedule **ResetApiToken_Schedule** class schedule before run this Trial batch class otherwise there might throw callout exception  ..
    **error: You have uncommitted work pending. Please commit or rollback before calling out**
// Solution Example :
    
    ResetApiToken_Schedule theJob = new ResetApiToken_Schedule();
    String sch = '0 45 14 * * ? *';
    System.schedule('ResetApiToken_Schedule Batch', sch, theJob);

    or 
    Execute manually before batch 
     UtilityToken.ResetTokenSch();
*/
global class TrialActivation_BatchSchedule implements Database.Batchable<SObject>, Schedulable, Database.AllowsCallouts {
    
    private String sSQL=null;
    public TrialActivation_BatchSchedule() //String query 
    {
    }
    
    global database.querylocator start(Database.BatchableContext bc)
    { 
        
        Date dt_from = Date.valueOf(System.Today()) - Integer.valueOf(Label.Trial_Process_Prior_Days);
        Date dt_to = Date.valueOf(System.Today()) + Integer.valueOf(Label.Trial_Process_Post_Days);
        
        String quoteStatus=ConstantsUtil.QUOTE_STATUS_PRESENTED;
        
       // String Qname='Q-00258';
            
        /*sSQL = 'SELECT Id, Name, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Account__c, SBQQ__Quote__r.SBQQ__Primary__c, '+
                 'SBQQ__Quote__r.SBQQ__Opportunity2__c, SBQQ__ProductCode__c, SBQQ__ProductName__c, SBQQ__Quantity__c, '+
                 'Place__c, Place__r.PlaceID__c, Status__c,SBQQ__RequiredBy__c, SBQQ__StartDate__c, '+
                 'TrialStartDate__c, TrialEndDate__c,SBQQ__Quote__r.Trial_CallId__c FROM SBQQ__QuoteLine__c '+
                 'WHERE (TrialStatus__c  = :vStatus AND TrialStartDate__c >= :dt_from AND TrialStartDate__c <=:dt_to and Place__c !=null) ';
		*/
        
        //SBQQ__Bundled__c == false means this Quote Line for either main product or optional product
        sSQL = 'SELECT ID, SBQQ__Primary__c, SBQQ__Opportunity2__c, SBQQ__Account__c, Trial_CallId__c, SBQQ__StartDate__c,' +
                    '(SELECT Id, Name, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Account__c, SBQQ__Quote__r.SBQQ__Primary__c,'+
                                                                            'SBQQ__Quote__r.SBQQ__Opportunity2__c, SBQQ__ProductCode__c, SBQQ__ProductName__c, SBQQ__Quantity__c,'+
                                                                            'Place__c, Place__r.PlaceID__c, Status__c, SBQQ__RequiredBy__c, SBQQ__StartDate__c,'+
                                                                            'TrialStartDate__c, TrialEndDate__c, TrialStatus__c,SBQQ__Quote__r.Trial_CallId__c '+
                                                                            'FROM SBQQ__LineItems__r WHERE SBQQ__Quantity__c > 0) ' +
                                                    'FROM SBQQ__Quote__c WHERE SBQQ__Status__c = :quoteStatus';
                                        
        							  
        //and SBQQ__Quote__r.name=:Qname        
        //AND TrialStartDate__c = :dt        
       //zakir changed Status__c with TrialStatus__c
       return Database.getQueryLocator(sSQL);   

    }
    
    global void execute(Database.BatchableContext bc, SBQQ__Quote__c[] listQuotes)
    {
        //TrialActivationManager.sendTrialRequest(listQLines);
        ContractUtility.callAMAQLine(listQuotes);
    }
    global void finish(Database.BatchableContext bc)
    {        
    }
    
    global void execute(SchedulableContext sc)
    {
        //System.schedule('Trial Activation Request Schedulabled', '0 0 13 * * ?', new TrialActivation_BatchSchedule());
        
        TrialActivation_BatchSchedule job = new TrialActivation_BatchSchedule();
        Database.executeBatch(job,50); //First error: Too many callouts: 101
    }
}