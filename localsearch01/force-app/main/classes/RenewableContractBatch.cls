/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-13-2019
 * Sprint      : 3
 * Work item   : SF2-88 Contract Renewal - US_65 Contract automatically renewed at subscription end date (implicit)
 * Testclass   : Test_RenewableContractBatch
 * Package     : 
 * Description : 
 * Changelog   :
 *              #1 SF2-298 - Contract - Enhancement - 1 - gcsasola - 08-07-2019 - Replaced the calculation logic for next renewal date using Base Term of the related product instead of Subscription Term 
 */

global class RenewableContractBatch implements Database.batchable<sObject>, Schedulable{
    
    global void execute(SchedulableContext sc) {
       RenewableContractBatch b = new RenewableContractBatch(); 
       Database.executebatch(b, 50);
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, AccountId, SBQQ__OpportunityPricebookId__c FROM Contract ' +
                                        'WHERE Id IN (SELECT SBQQ__Contract__c FROM SBQQ__Subscription__c WHERE '+
                                        'In_Termination__c = FALSE AND Next_Renewal_Date__c <= TOMORROW ' +
                                        'AND SBQQ__Contract__r.SBQQ__Evergreen__c = TRUE AND ' +
                                        'Subsctiption_Status__c = \'' + ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE + '\' )'
                                        );
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){

        Map<Id, Map<SBQQ__Subscription__c, List<SBQQ__Subscription__c>>> subscriptionsHierarchyMap = new Map<Id, Map<SBQQ__Subscription__c,List<SBQQ__Subscription__c>>>();
        Map<Id, List<SBQQ__Subscription__c>> subscriptionChildren = new Map<Id, List<SBQQ__Subscription__c>>();
            
        Set<Id> contractIds = new Set<Id>();
        for(Contract currentContract : scope){
            contractIds.add(currentContract.Id);
        }

        Map<Id, Id> contractAndAccount = new Map<Id,Id>();
        Map<Id, Integer> baseTermByContractId = new Map<Id, Integer>();
        
        Map<Id, Map<Id, PricebookEntry>> pbEntryListPriceByPriceBookId= new Map<Id, Map<Id, PricebookEntry>>();
        Map<Id, PricebookEntry> pbEntries = new Map<Id, PricebookEntry>();
        Map<Id, Id> priceBookBycontractId = new Map<Id, Id>();
        
        Map<Id, Order> renewalOrderToInsertByContractId = new Map<Id, Order>();
        Map<SBQQ__Subscription__c, OrderItem> renewalParentOrderProductBySub = new Map<SBQQ__Subscription__c, OrderItem>();
        List<OrderItem> renewalChidlOrderItemToInsert = new List<OrderItem>();
        
        for(Contract currentContract : scope){
            contractAndAccount.put(currentContract.Id, currentContract.AccountId);
            priceBookBycontractId.put(currentContract.Id, currentContract.SBQQ__OpportunityPricebookId__c);

            if(!pbEntryListPriceByPriceBookId.containsKey(currentContract.SBQQ__OpportunityPricebookId__c))
                pbEntryListPriceByPriceBookId.put(currentContract.SBQQ__OpportunityPricebookId__c, new Map<Id, PricebookEntry>());
            
        }
        
        //getActivePriceBookEntriesByPriceBookId, Changed because we must ensure that renewable can use pricebookinactive
        for(PricebookEntry pbEntry : SEL_PricebookEntry.getPriceBookEntriesByPriceBookId(pbEntryListPriceByPriceBookId.keySet()).values())
            pbEntryListPriceByPriceBookId.get(pbEntry.Pricebook2Id).put(pbEntry.Product2Id, pbEntry);
        
        Map<Id, SBQQ__Subscription__c> subscriptionsMap = new Map<Id, SBQQ__Subscription__c>(   [SELECT Id, SBQQ__Product__c,
                                                                                                        Promotion_Applied__c,Promotion_Type__c, 
                                                                                                        SBQQ__Product__r.Subscription_Term__c, 
                                                                                                        SBQQ__Contract__r.ActivatedDate,
                                                                                                        Total__c, End_Date__c, Next_Renewal_Date__c,
                                                                                                        SBQQ__Contract__c, SBQQ__RequiredById__c,
                                                                                                        SBQQ__Quantity__c, Subscription_Term__c,
                                                                                                        SBQQ__Product__r.Base_term_Renewal_term__c,
                                                                                                        SBQQ__RequiredByProduct__r.Base_term_Renewal_term__c,
                                                                                                        SBQQ__Product__r.One_time_Fee__c, SBQQ__ListPrice__c,
                                                                                                 		SBQQ__QuoteLine__r.SBQQ__ListPrice__c, Number_of_Renewals__c,
                                                                                                        SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c,
                                                                                                 		SBQQ__BillingFrequency__c, RequiredBy__r.SBQQ__BillingFrequency__c,
                                                                                                 		RequiredBy__r.SBQQ__Product__r.Base_term_Renewal_term__c
                                                                                                FROM    SBQQ__Subscription__c
                                                                                                WHERE   SBQQ__Contract__c IN :contractIds
                                                                                                AND     In_Termination__c = FALSE
                                                                                                AND     Next_Renewal_Date__c <= TOMORROW
                                                                                                AND     Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE]);
        
        Map<Id, Date> subscriptionToEndDate = new Map<Id, Date>();
		//SPIII - 3827, ldimartino START: billing frequency handling
		Map<String, Integer> freqToBaseTermMap = new Map<String, Integer>();
        Map<Integer, String> baseTermToFreqMap = new Map<Integer, String>();
        Map<Id, Date> contractToEndDate = new Map<Id, Date>();
		List<Invoice_Cycle__mdt> billingFrequencies = new List<Invoice_Cycle__mdt>();
        billingFrequencies = [SELECT MasterLabel, Number_of_months__c FROM Invoice_Cycle__mdt];
        for(Invoice_Cycle__mdt bf : billingFrequencies){
            freqToBaseTermMap.put(bf.MasterLabel, (Integer)bf.Number_of_months__c);
            baseTermToFreqMap.put((Integer)bf.Number_of_months__c, bf.MasterLabel);
        } 
        //SPIII - 3827, ldimartino END
        for(SBQQ__Subscription__c sub : subscriptionsMap.values()){
			
            subscriptionToEndDate.put(sub.Id, sub.End_Date__c);
            contractToEndDate.put(sub.SBQQ__Contract__c, sub.End_Date__c);

            if(sub.SBQQ__Product__r.One_time_Fee__c){
                sub.Subsctiption_Status__c = ConstantsUtil.CONTRACT_STATUS_EXPIRED;
            }
            else{
                if(sub.Total__c != null && sub.Total__c != 0){
                    system.debug('sub.Total__c '+sub.Total__c );
                    Decimal renewalPrice = sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c * sub.SBQQ__Quantity__c;
                    system.debug('renewalPrice ::: '+renewalPrice + ' sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c :: '+sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c);
                    if(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null && sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != 0 && !ConstantsUtil.FIRST_TERM_DISCOUNT.equalsIgnoreCase(sub.Promotion_Type__c)){
                        Decimal discountAmount = (Decimal) (renewalPrice*sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                        sub.Total__c = sub.Total__c + (renewalPrice - discountAmount);
                    }
                    else sub.Total__c = sub.Total__c + renewalPrice;
                }
                system.debug('sub.Total__c '+sub.Total__c );
                    
                //SPIII - 3827, ldimartino START: billing frequency handling
                Integer numberOfMonths = 0;
                Integer totalMonths = 0;
                Integer baseTerm = 0; 
                if(sub.SBQQ__BillingFrequency__c != null){
                    numberOfMonths = freqToBaseTermMap.get(sub.SBQQ__BillingFrequency__c);
                }else{
                    numberOfMonths = freqToBaseTermMap.get(sub.RequiredBy__r.SBQQ__BillingFrequency__c);
                }      
                if(sub.SBQQ__Product__r.Base_term_Renewal_term__c != null){
                    baseTerm = Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c);
                }else{
                    baseTerm = Integer.valueOf(sub.RequiredBy__r.SBQQ__Product__r.Base_term_Renewal_term__c);
                }
                if(numberOfMonths > baseTerm) sub.SBQQ__BillingFrequency__c = baseTermToFreqMap.get(numberOfMonths);
				//SPIII - 3827, ldimartino END
				
                if(sub.Number_of_Renewals__c != null) sub.Number_of_Renewals__c += 1;
                else sub.Number_of_Renewals__c = 1;
                
                if(sub.SBQQ__RequiredById__c != NULL){

                    sub.Next_Renewal_Date__c = sub.Next_Renewal_Date__c.addMonths(Integer.valueOf(sub.SBQQ__RequiredByProduct__r.Base_term_Renewal_term__c));
                    sub.End_Date__c = sub.End_Date__c.addMonths(Integer.valueOf(sub.SBQQ__RequiredByProduct__r.Base_term_Renewal_term__c));
                    
                    if(!subscriptionChildren.containsKey(sub.SBQQ__RequiredById__c)){
                        subscriptionChildren.put(sub.SBQQ__RequiredById__c, new List<SBQQ__Subscription__c>());
                    }
                    subscriptionChildren.get(sub.SBQQ__RequiredById__c).add(sub);
                }
                else{
                    sub.Next_Renewal_Date__c = sub.Next_Renewal_Date__c.addMonths(Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c));
                    sub.End_Date__c = sub.End_Date__c.addMonths(Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c));
                    if(!subscriptionsHierarchyMap.containsKey(sub.SBQQ__Contract__c)){
                        subscriptionsHierarchyMap.put(sub.SBQQ__Contract__c, new Map<SBQQ__Subscription__c, List<SBQQ__Subscription__c>>());
                    }
                    subscriptionsHierarchyMap.get(sub.SBQQ__Contract__c).put(sub, new List<SBQQ__Subscription__c>());
                    baseTermByContractId.put(sub.SBQQ__Contract__c, Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c));
                }
            }
        }
        update subscriptionsMap.values();
        
        for(Id parentSubId : subscriptionChildren.keySet()){
        if(subscriptionsMap.containsKey(parentSubId) && subscriptionsHierarchyMap.containsKey(subscriptionsMap.get(parentSubId).SBQQ__Contract__c) && subscriptionChildren.containsKey(parentSubId)){
            subscriptionsHierarchyMap.get(subscriptionsMap.get(parentSubId).SBQQ__Contract__c).get(subscriptionsMap.get(parentSubId)).addAll(subscriptionChildren.get(parentSubId));
        }
        }
        
        for(Id contractId : subscriptionsHierarchyMap.keySet()){
            Order renewalOrder = new Order();
            renewalOrder.Type = ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL;
            renewalOrder.Contract__c = contractId;
            renewalOrder.AccountId = contractAndAccount.get(contractId);
            renewalOrder.EffectiveDate = contractToEndDate.get(contractId);
            renewalOrder.nextRenewalDate__c = Date.today().addDays(1).addMonths(baseTermByContractId.get(contractId));
            renewalOrder.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
            renewalOrder.Pricebook2Id = priceBookBycontractId.get(contractId);
            renewalOrderToInsertByContractId.put(contractId,renewalOrder);
        }
        
        insert renewalOrderToInsertByContractId.values();
        
        for(Id contractId : subscriptionsHierarchyMap.keySet()){
            Id orderId = renewalOrderToInsertByContractId.get(contractId).Id;
            for(SBQQ__Subscription__c parentSub : subscriptionsHierarchyMap.get(contractId).keySet()){
                OrderItem renewalParentOrderItem = new OrderItem();
                renewalParentOrderItem.ServiceDate = subscriptionToEndDate.get(parentSub.Id).addDays(1);
                renewalParentOrderItem.Subscription_Term__c = parentSub.SBQQ__Product__r.Base_term_Renewal_term__c;
                renewalParentOrderItem.End_Date__c =  subscriptionToEndDate.get(parentSub.Id).addDays(1).addMonths(Integer.valueOf(parentSub.SBQQ__Product__r.Base_term_Renewal_term__c));
                renewalParentOrderItem.Product2Id = parentSub.SBQQ__Product__c;
                renewalParentOrderItem.SBQQ__Subscription__c = parentSub.Id;
                renewalParentOrderItem.OrderId = orderId;
                renewalParentOrderItem.Quantity = parentSub.SBQQ__Quantity__c;
                renewalParentOrderItem.PricebookEntryId = pbEntryListPriceByPriceBookId.get(priceBookBycontractId.get(contractId)).get(parentSub.SBQQ__Product__c).Id;
                renewalParentOrderItem.UnitPrice = pbEntryListPriceByPriceBookId.get(priceBookBycontractId.get(contractId)).get(parentSub.SBQQ__Product__c).UnitPrice;
                if(parentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null && parentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != 0){
                    Decimal price = parentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c * parentSub.SBQQ__Quantity__c;
                    if(parentSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null && parentSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != 0 && !ConstantsUtil.FIRST_TERM_DISCOUNT.equalsIgnoreCase(parentSub.Promotion_Type__c)  ){
                        Decimal discountAmount = (Decimal) (price*parentSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                        renewalParentOrderItem.Total__c = price - discountAmount;
                    }
                    else{
                        renewalParentOrderItem.Total__c = price;
                    }
                    system.debug(' renewalParentOrderItem.Total__c :: ' + renewalParentOrderItem.Total__c );
                }
                renewalParentOrderProductBySub.put(parentSub,renewalParentOrderItem);
            }
        }
        
        insert renewalParentOrderProductBySub.values();
        
        for(SBQQ__Subscription__c parentSub : renewalParentOrderProductBySub.KeySet()){
            for(SBQQ__Subscription__c childSub : subscriptionsHierarchyMap.get(parentSub.SBQQ__Contract__c).get(parentSub)){
                OrderItem renewalChildOrderItem = new OrderItem();
                renewalChildOrderItem.ServiceDate = subscriptionToEndDate.get(childSub.Id).addDays(1);
                renewalChildOrderItem.Subscription_Term__c = parentSub.SBQQ__Product__r.Base_term_Renewal_term__c;
                renewalChildOrderItem.End_Date__c = subscriptionToEndDate.get(childSub.Id).addDays(1).addMonths(Integer.valueOf(parentSub.SBQQ__Product__r.Base_term_Renewal_term__c));
                renewalChildOrderItem.Product2Id = childSub.SBQQ__Product__c;
                renewalChildOrderItem.SBQQ__Subscription__c = childSub.Id;
                renewalChildOrderItem.OrderId = renewalParentOrderProductBySub.get(parentSub).OrderId;
                renewalChildOrderItem.SBQQ__RequiredBy__c = renewalParentOrderProductBySub.get(parentSub).Id;
                renewalChildOrderItem.Quantity = childSub.SBQQ__Quantity__c;
                if(childSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null && childSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != 0){
                    Decimal price = childSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c * childSub.SBQQ__Quantity__c;
                    if(childSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null && childSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != 0 && !ConstantsUtil.FIRST_TERM_DISCOUNT.equalsIgnoreCase(parentSub.Promotion_Type__c)){
                        Decimal discountAmount = (Decimal) (price*childSub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                        renewalChildOrderItem.Total__c = price - discountAmount;
                    }
                    else{
                        renewalChildOrderItem.Total__c = price;
                    }
                    system.debug(' renewalParentOrderItem.Total__c :: ' + renewalChildOrderItem.Total__c );
                }
                renewalChildOrderItem.PricebookEntryId = pbEntryListPriceByPriceBookId.get(priceBookBycontractId.get(parentSub.SBQQ__Contract__c)).get(childSub.SBQQ__Product__c).Id;
                renewalChildOrderItem.UnitPrice = pbEntryListPriceByPriceBookId.get(priceBookBycontractId.get(parentSub.SBQQ__Contract__c)).get(childSub.SBQQ__Product__c).UnitPrice;
                renewalChidlOrderItemToInsert.add(renewalChildOrderItem);
            }
        }
        insert renewalChidlOrderItemToInsert;
        
        for(Order renewalOrder : renewalOrderToInsertByContractId.values()){
            renewalOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        }
        OrderTriggerHandler.disableTrigger = true;
        SBQQ.TriggerControl.disable();
        update renewalOrderToInsertByContractId.values();
		OrderTriggerHandler.disableTrigger = false;
        
        Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem>();
        for(OrderItem oi : renewalParentOrderProductBySub.values()){
            oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
            orderItemsToUpdate.put(oi.Id, oi);
        }
        for(OrderItem oi : renewalChidlOrderItemToInsert){
            oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
            orderItemsToUpdate.put(oi.Id, oi); 
        }

        if(!orderItemsToUpdate.isEmpty()){
            OrderProductTriggerHandler.disableTrigger = true;
            update orderItemsToUpdate.values();
            OrderProductTriggerHandler.disableTrigger = false;
        }
        SBQQ.TriggerControl.enable();
    }

    global void finish(Database.BatchableContext info){}

}