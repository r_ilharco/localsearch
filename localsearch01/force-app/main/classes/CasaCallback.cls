@RestResource(urlMapping='/casaCallback')
global without sharing class CasaCallback {
	
    @HttpGet
    global static void getCasaCallback() {
        
        String endpoint ='http://demo7950514.mockable.io/addallocation'; //Remove after test
        
        Map<String,String> inputParams = RestContext.request.params;
        SRV_CasaCallback.CasaParams casaParams = SRV_CasaCallback.getInputParams(inputParams);
        
        String formattedStartDate = SRV_CasaCallback.formatDate(casaParams.start_date);
        String formattedEndDate = SRV_CasaCallback.formatDate(casaParams.end_date);
        String formattedExpDate = SRV_CasaCallback.formatDate(casaParams.expiration_date);
        
        //POST callout to CASA
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('category', casaParams.category);
        parameters.put('region', casaParams.region);
        parameters.put('start_date', formattedStartDate);
        parameters.put('end_date', formattedEndDate);
        parameters.put('expiration_date', formattedExpDate);
        parameters.put('slot', casaParams.slot);
        
        SRV_EnhancedExtProdConfiguration.CasaAdContextResponse casaResponse = SRV_EnhancedExtProdConfiguration.adContextCallout(endpoint, null, parameters);
        if(casaResponse != null){
            casaParams.ad_context_allocation = casaResponse.id;
            if(!SRV_CasaCallback.publishPlatformEvent(casaParams)){
                RestResponse res = RestContext.response;
                res.responseBody = Blob.valueOf('Internal Server Error');
                res.statusCode = 500;   
            }
        }
        else{
            if(!SRV_CasaCallback.publishErrorPlatformEvent()){
                RestResponse res = RestContext.response;
                res.responseBody = Blob.valueOf('Internal Server Error');
                res.statusCode = 500;
            }
        }
    }
}