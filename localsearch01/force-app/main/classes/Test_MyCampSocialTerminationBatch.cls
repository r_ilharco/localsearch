@isTest
public class Test_MyCampSocialTerminationBatch {
    
    @testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;		
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        //quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        //update quote;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_terminationOrderSchedulable(){
        SBQQ.TriggerControl.disable(); 
        OrderTriggerHandler.disableTrigger = true;
        Test.startTest();
        ByPassFlow__c bpf = [SELECT Id,Name,FlowNames__c FROM ByPassFlow__c];
        Account account = [SELECT Id FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today().addDays(-4)));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        
        fatherOI.ServiceDate = Date.today().addDays(-4);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        for(OrderItem oi : childrenOIs){
            oi.ServiceDate = Date.today().addDays(-4);
        }
        insert childrenOIs;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        OrderProductTriggerHandler.disableTrigger = true;
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        insert terminationOrder;
        GlobalConfiguration_cs__c setting = new GlobalConfiguration_cs__c();
        setting.MyCamp_Social_Termination_Days__c = 14.0;
        insert setting;
        
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
		fatherOrderItem.ServiceDate = Date.today().addDays(Integer.valueOf(setting.MyCamp_Social_Termination_Days__c));
        insert fatherOrderItem;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOrderItem.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        for(OrderItem oi : childrenOrderItems){
            oi.ServiceDate = Date.today().addDays(Integer.valueOf(setting.MyCamp_Social_Termination_Days__c));
        }
        insert childrenOrderItems;
        
        Integer nextNDays = Integer.valueOf(setting.MyCamp_Social_Termination_Days__c);
        Integer nextNDaysMinus1 = Integer.valueOf(setting.MyCamp_Social_Termination_Days__c)-1;
		Set<String> myCampSocialProductCodes = new Set<String>{ConstantsUtil.MYCSOCIAL_BOOSTER, ConstantsUtil.MYCSOCIAL_SOCIAL ,ConstantsUtil.MYCSOCIAL_AWARE ,ConstantsUtil.MYCSOCIAL_LEAD};
		
		String query = ('SELECT Id, MigratedTermination__c, Contract__c, Contract__r.Phase1Source__c FROM Order WHERE Id IN (' +
                        'SELECT OrderId ' +
                        'FROM OrderItem ' +
                        'WHERE ServiceDate = NEXT_N_DAYS:' + nextNDays + ' ' +
                        'AND   ServiceDate > NEXT_N_DAYS:' + nextNDaysMinus1 + ' ' +
                        'AND ( Order.Type = \'' + ConstantsUtil.ORDER_TYPE_CANCELLATION + '\' OR Order.Custom_Type__c = \'' + ConstantsUtil.ORDER_TYPE_TERMINATION + '\') ' +
                        'AND Order.Status = \'' + ConstantsUtil.ORDER_STATUS_DRAFT + '\'' +
                        'AND Product2.Product_Group__c = \'' + ConstantsUtil.MYCAMPAIGN_PROD_GROUP + '\'' +
                        'AND Product2.ProductCode IN :myCampSocialProductCodes ' +
                        'AND Order.MigratedTermination__c = FALSE' +
                        ')'
                       );
        
        List<Order> orders = Database.query(query);
        
        MyCampSocialTerminationBatch b = new MyCampSocialTerminationBatch();
        b.execute(null, orders);
        database.executebatch(b, 1);
        
        Test.stopTest();
        OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable(); 
    }
    
    public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','MyCAMPAIGNS SOCIAL Booster');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','FBB');
        fieldApiNameToValueProduct.put('KSTCode__c','8931');
        fieldApiNameToValueProduct.put('KTRCode__c','1201000003');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCampaigns Social Booster');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','1');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c',ConstantsUtil.MYCAMPAIGN_PROD_GROUP);
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode',ConstantsUtil.MYCSOCIAL_BOOSTER);
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','Setup MyCAMPAIGNS SOCIAL');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','FBS');
        fieldApiNameToValueProduct.put('KSTCode__c','8931');
        fieldApiNameToValueProduct.put('KTRCode__c','1201000003');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCampaigns Social Booster');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c',ConstantsUtil.MYCAMPAIGN_PROD_GROUP);
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode',ConstantsUtil.O_MYCSOCIAL_SETUP001);
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Order generateOrder(Contract c, String orderType){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.Type = orderType;
        order.Custom_Type__c = orderType;
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null){
                c.Cancel_Date__c = Date.today();
                order.EffectiveDate = Date.today();
            }
        }
        else{
            
            if(c.TerminateDate__c == null){
                c.TerminateDate__c = Date.today().addDays(-1);
                order.EffectiveDate = Date.today().addDays(-1);
            }
            else{
                order.EffectiveDate = c.TerminateDate__c;
            }
            order.ContractId = c.id;
        }
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today().addDays(-1);
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('FATHER OI ---> '+JSON.serializePretty(fatherOrderItem));
        return fatherOrderItem; 
    }
    public static OrderItem generateChildrenOrderItem(SBQQ__Subscription__c subscription, Order order, String requiredBy, String pbeId){
        OrderItem oi = new OrderItem();
        oi.ServiceDate = Date.today().addDays(-1);
        oi.Quantity = subscription.SBQQ__Quantity__c;
        oi.UnitPrice = subscription.SBQQ__ListPrice__c;   
        oi.Product2Id = subscription.SBQQ__Product__c;
        oi.PricebookEntryId = pbeId;
        oi.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        oi.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c;
        oi.SBQQ__Subscription__c = subscription.Id;
        oi.OrderId = order.Id;
        oi.SBQQ__RequiredBy__c = requiredBy;  
        oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('CHILD OI ---> '+JSON.serializePretty(oi));
        return oi;   
    }
}