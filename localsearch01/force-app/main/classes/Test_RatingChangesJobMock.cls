@isTest
global class Test_RatingChangesJobMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        BillingResults.CustomerRatingUpdate customerRating = new BillingResults.CustomerRatingUpdate();
        customerRating.client_id= 'pippo';
        customerRating.client_name= 'paperino';
       	customerRating.amount_at_risk= 12.2;
        customerRating.new_client_rating= 'Basic';
        customerRating.old_client_rating= 'diamond';
        customerRating.time_stamp= null;
        List<BillingResults.CustomerRatingUpdate> listCustomerRating = new List<BillingResults.CustomerRatingUpdate>();
        listCustomerRating.add(customerRating);
        BillingResults.CustomerRatingReport report = new BillingResults.CustomerRatingReport();
        report.time_stamp = null;
        report.customer_ratings = listCustomerRating;
        String JsonMsg = JSON.serialize(report);
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(JsonMsg);
        res.setStatusCode(200);
        return res;
    }
}