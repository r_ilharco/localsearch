/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-02-2019
 * Sprint      : 
 * Work item   : SF2-177 Invoices pdf have to be accessible
 * Testclass   : Test_ViewInvoicePDFDocumentController
 * Package     : 
 * Description : Controller for VF Page 'ViewInvoicePDFDocument' on QuickAction 'View Invoice PDF'
/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*		  
* @changes
* @modifiedby      Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-06      [SPIII-1504] Modify View Invoice PDF Custom Link on Invoice Object
*				  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ViewInvoicePDFDocumentController {
    
    final private Invoice__c invoice;    
    
    public ViewInvoicePDFDocumentController(ApexPages.StandardController stdController) {
        this.invoice = [SELECT Id, Name, Correlation_Id__c, Invoice_Code__c
                        FROM Invoice__c 
                        WHERE Id = :stdController.getId()];
    }
    
    public String pdf {
        get {
            String str = viewPdfDocument(this.invoice);
            return (String.isNotBlank(str)) ? str : null;
        }
        private set;
    }
    
    public static String viewPdfDocument(Invoice__c invoice) { 
        BillingResults.RestGetResponse restResp;
        if(invoice.Correlation_Id__c == null || invoice.Invoice_Code__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.Generic_Error));
            return null;
        }
        Billing_Setting__mdt setting_mdt = [SELECT Is_Credit_Note_PDF__c,Is_Invoice_PDF__c FROM
                                            Billing_Setting__mdt WHERE DeveloperName  =:invoice.Invoice_Code__c];
        if(setting_mdt.Is_Invoice_PDF__c){
        	 restResp= BillingResults.getPdfDocument_v2('DownloadPDF', invoice.Correlation_Id__c); 
        }
        else if(setting_mdt.Is_Credit_Note_PDF__c){
        	restResp = BillingResults.getPdfCreditNoteDocument('DownloadPDF', invoice.Correlation_Id__c); 
        }
        //BillingResults.RestGetResponse restResp = BillingResults.getPdfDocument('AttachPdfDocument', invoice.Correlation_Id__c);
        try{
            Integer statusCode = restResp.httpResponse.getStatusCode();
            if(statusCode == 200) {
                try{
                    system.debug('response body: ' + restResp.httpResponse.getBody());
                    system.debug('response body: ' + restResp.httpResponse.getBodyAsBlob());
                    String body = restResp.httpResponse.getBody();
                    if(String.isNotBlank(body)){
                        return body;
                    }else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.InvoicePDF_Not_Available));
                        return null;
                    }
                    
                } catch (Exception ex) {
                    ErrorHandler.log(System.LoggingLevel.ERROR, 'Billing', 'Billing.AttachPdfDocument', ex, ErrorHandler.ErrorCode.E_UNKNOWN, '', restResp.httpResponse.getBody(), restResp.correlationId, 'AttachPdfDocument', null, false);
                    //A.L. Marotta 02-01-20 - custom label for error
                    //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,ex.getMessage()));
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.Invoice_PDF_error));
                }
            } else if(statusCode != 400) { // Ignore bad request. Document is probably not ready 
                ErrorHandler.log(System.LoggingLevel.ERROR, 'Billing', 'Billing.AttachPdfDocument', null, ErrorHandler.ErrorCode.E_UNKNOWN, 'HTTP ' + statusCode + '-' + restResp.httpResponse.getStatus(), restResp.httpResponse.getBody(), restResp.correlationId, 'AttachPdfDocument', null, false);
                //A.L. Marotta 02-01-20 - custom label for error
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'HTTP StatusCode: ' + statusCode + '-' + restResp.httpResponse.getStatus()));
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.Invoice_PDF_error_code + statusCode));
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,Label.SwissBilling_Doc_Not_Found));
                return null;
            }
        }
        catch (Exception ex) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.Invoice_PDF_error));
        }
        return null;
    }
    
}