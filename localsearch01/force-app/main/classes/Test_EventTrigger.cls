/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Event Trigger (Handler and Helper)
 * Changelog   : 
 */

@isTest
public class Test_EventTrigger {

    @testSetup
    public static void test_setupData(){
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        EventTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.generateAccounts('Test Account', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Opportunity opty = test_DataFactory.generateOpportunity('Test Opportunity',account.Id);
        insert opty;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        EventTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insertUpdateOpty(){
        
        List<String> fieldNamesContact = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
        String queryContacts =' SELECT ' +String.join( fieldNamesContact, ',' ) +' FROM Contact LIMIT 1';
        Contact contact = Database.query(queryContacts);
        
        List<String> fieldNamesOpportunity = new List<String>(Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().keySet());
        String queryOpportunities =' SELECT ' +String.join( fieldNamesOpportunity, ',' ) +' FROM Opportunity LIMIT 1';
        Opportunity opty = Database.query(queryOpportunities);
        
        Test.startTest();
        Event e = new Event();
        e.DurationInMinutes = 5;
        e.ActivityDateTime = Datetime.now();
        e.Subject = ConstantsUtil.EVENT_SUBJECT_CALL;
        e.WhoId = contact.Id;
        e.WhatId = opty.Id;
        insert e;
        Test.stopTest();
    }
    
    @isTest
    public static void test_insertUpdateQuote(){
        
        List<String> fieldNamesContact = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
        String queryContacts =' SELECT ' +String.join( fieldNamesContact, ',' ) +' FROM Contact LIMIT 1';
        Contact contact = Database.query(queryContacts);
        
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes =' SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuotes);
        
        Test.startTest();
        Event e = new Event();
        e.DurationInMinutes = 5;
        e.ActivityDateTime = Datetime.now();
        e.Subject = ConstantsUtil.EVENT_SUBJECT_CALL;
        e.WhoId = contact.Id;
        e.WhatId = quote.Id;
        insert e;
        
        e.DurationInMinutes = 10;
        update e;
        Test.stopTest();
    }
    
    @isTest
    public static void test_deleteUndelete(){
        
        List<String> fieldNamesContact = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
        String queryContacts =' SELECT ' +String.join( fieldNamesContact, ',' ) +' FROM Contact LIMIT 1';
        Contact contact = Database.query(queryContacts);
        
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes =' SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuotes);
        
        Test.startTest();
        Event e = new Event();
        e.DurationInMinutes = 5;
        e.ActivityDateTime = Datetime.now();
        e.Subject = ConstantsUtil.EVENT_SUBJECT_CALL;
        e.WhoId = contact.Id;
        e.WhatId = quote.Id;
        insert e;
        delete e;
        undelete e;
        Test.stopTest();
    }
}