public class TopLinkRegionsResponseWrapper {
    public List<Domicile> domicile {get;set;}
    public List<Domicile> neighbours {get;set;}
    
    public class Domicile{
        public String id {get;set;}
        public String name {get;set;}
        public String price {get;set;}
        public String href {get;set;}
    }
}