@isTEst
public class NotifyCreateOrUpdateContractPBTest {

    @isTest
    static void testNotifyCreateOrUpdateContract(){
        
         try{
             
             User u = new user();
             u.LastName = 'Test Code';
             u.Email = 'iqbal.rocky@arollotech.com';
             u.Alias = 'Tcode';
             u.Username = 'iqbal.roc@arollo.com';
             u.CommunityNickname = 'test12';
             u.LocaleSidKey = 'en_US';
             u.TimeZoneSidKey = 'GMT';
             u.ProfileID = '00e1r0000027zBpAAI';
             u.LanguageLocaleKey = 'en_US';
             u.EmailEncodingKey = 'UTF-8';
     
             insert u;
             system.debug('User' +u);
             
         	Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
                       
            insert acc;
        
        	Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
                
            insert opp;
            System.Debug('opp '+ opp); 

  			Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
        
           
        	SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            
            insert quot;
            System.Debug('Quote '+ quot);
        
       

        
        	Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Opportunity__c= opp.Id;
        	myContract.SBQQ__Quote__c= quot.Id;
            myContract.OwnerId= u.Id;
                
            test.startTest();
            insert myContract;
            System.debug('Contract: '+ myContract); 
          
            myContract.Status='Active';
            update myContract;
              
          	Contract myContract1 = new Contract();
            myContract1.AccountId = acc.Id;
            myContract1.Status = 'Draft';
            myContract1.StartDate = Date.today();
            myContract1.TerminateDate__c = Date.today();
            myContract1.SBQQ__Opportunity__c= opp.Id;
        	myContract1.SBQQ__Quote__c= quot.Id;
            myContract1.OwnerId= u.Id;   
            
            insert myContract1;
            System.debug('Contract: '+ myContract1); 
             
            test.stopTest();
        }
        catch(Exception e)
        {
            system.assert(false, e.getMessage());
        }
        
    }
    
    
 }