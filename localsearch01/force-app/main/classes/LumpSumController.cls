public without sharing class LumpSumController{

    @AuraEnabled
    public static void lumpRequestedUpdate(String selectedRows){
        Set<Id> subIds = new Set<Id>();
        system.debug('selectedRows = ' + selectedRows);
        List<SBQQ__Subscription__c> subs = (List<SBQQ__Subscription__c>)System.JSON.deserialize(selectedRows, List<SBQQ__Subscription__c>.class);
        for(SBQQ__Subscription__c sub : subs){
            system.debug('sub.id = ' + sub.id);
            subIds.add(sub.id);
        }
        system.debug('subIds: ' + subIds);
        List<SBQQ__Subscription__c> listSubs = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        listSubs = [SELECT Id, Billing_Status__c FROM SBQQ__Subscription__c  WHERE Id IN :subIds OR SBQQ__RequiredById__c IN :subIds];
        system.debug('listSubs: ' +listSubs);
		for(SBQQ__Subscription__c sub : listSubs){
            sub.Billing_Status__c = ConstantsUtil.LUMP_REQUESTED;
            subsToUpdate.add(sub);
        }
        if(subsToUpdate.size() > 0){
           update subsToUpdate;
        }
        system.debug('Lump Requested');
    }
    @AuraEnabled
    public static SubscriptionsResponse getSubscriptionToBillByContractId(id contractId){
        system.debug('contractId = ' + contractId);
        SubscriptionsResponse res = new SubscriptionsResponse();
        Set<Id> productIds = new Set<Id>();
        Set<Id> subscriptionIds = new Set<Id>();
        Map<Id, Decimal> subAmountMap = new Map<Id, Decimal>();
        Map<Id, Id> sonToFatherMap = new Map<Id, Id>();
        String language = UserInfo.getLanguage();
        List<SBQQ__Subscription__c> listSubs = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> listMasterSubs = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> listChildSubs = new List<SBQQ__Subscription__c>();
        listMasterSubs = [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__Product__c, SBQQ__RequiredById__c 
                    FROM SBQQ__Subscription__c 
                    WHERE SBQQ__Contract__c = :contractId AND SBQQ__RequiredById__c = null AND Billing_Status__c = null AND  Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE];
        system.debug('ldimartino, listMasterSubs: ' + listMasterSubs);
        if(listMasterSubs.size() == 0){
            res.isSizeZero = true;
        }else{
            listSubs.addAll(listMasterSubs);
        }
        listChildSubs = [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__Product__c, SBQQ__RequiredById__c 
                    FROM SBQQ__Subscription__c 
                    WHERE SBQQ__Contract__c = :contractId AND SBQQ__RequiredById__c != null AND Billing_Status__c = null AND  Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE];
        if(listMasterSubs.size() > 0){
            listSubs.addAll(listChildSubs);
        }
        system.debug('ldimartino, listSubs: ' + listSubs);
        for(SBQQ__Subscription__c s : listSubs){
            productIds.add(s.SBQQ__Product__c);
            subscriptionIds.add(s.Id);
            if(s.SBQQ__RequiredById__c != null){
                sonToFatherMap.put(s.Id, s.SBQQ__RequiredById__c);
            }
        }
        subAmountMap = SRV_Invoice.remainingAmountCalculation(subscriptionIds);
        for(Id subId : subAmountMap.keySet()){
            Decimal totalAmount = 0;
            if(sonToFatherMap.containsKey(subId) && subAmountMap.containsKey(subId) && subAmountMap.containsKey(sonToFatherMap.get(subId))){
                totalAmount = subAmountMap.get(subId);
                subAmountMap.put(sonToFatherMap.get(subId),subAmountMap.get(sonToFatherMap.get(subId)) + totalAmount);
            }
        }
        system.debug('subAmountMap: ' + subAmountMap);
        List<SBQQ__Localization__c> localizations = [SELECT ID, SBQQ__Product__c, SBQQ__Language__c, SBQQ__Text__c 
                                                     FROM SBQQ__Localization__c 
                                                     WHERE SBQQ__Product__c IN :productIds 
                                                     AND SBQQ__Label__c = :ConstantsUtil.TRANSLATION_LABEL_PRODUCT_NAME];
        Map<ID, Map<String, String>> productLanguageTranslation = new Map<ID, Map<String, String>>();
        for(SBQQ__Localization__c loc : localizations){
            if(!String.isBlank(loc.SBQQ__Text__c)){
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                    productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                }else{
                    productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                }
            }
        }
        for(SBQQ__Subscription__c sub: listSubs){
            if(sub.SBQQ__RequiredById__c != null){
                continue;
            }
            String productName = sub.SBQQ__ProductName__c;
            if(productLanguageTranslation.containsKey(sub.SBQQ__Product__c) && productLanguageTranslation.get(sub.SBQQ__Product__c).containsKey(language)){
                productName = productLanguageTranslation.get(sub.SBQQ__Product__c).get(language);
            }
            Subscriptionswrapper wrap = new Subscriptionswrapper();
            wrap.id = sub.id;
            wrap.name = sub.Name;
            wrap.productId = sub.SBQQ__Product__c;
            wrap.productName = productName;
            if(subAmountMap.containsKey(sub.id)){
                wrap.remainingAmount = subAmountMap.get(sub.id);
            }else{
                wrap.remainingAmount = 0;
            }
            res.subscriptions.add(wrap);
        }
        return res;
    }
    
    public class Subscriptionswrapper{
        @AuraEnabled public Id id {get; set;}
        @AuraEnabled public string name {get; set;}
        @AuraEnabled public Id productId {get; set;}
        @AuraEnabled public string productName {get; set;}
        @AuraEnabled public Decimal remainingAmount {get; set;}
    }
    
    public class SubscriptionsResponse{
        @AuraEnabled public boolean isSizeZero {get; set;}
        @AuraEnabled public List<Subscriptionswrapper> subscriptions {get; set;}
        public SubscriptionsResponse(){
            subscriptions = new List<Subscriptionswrapper>();
            this.isSizeZero = false;
        }
    }
}
