//Changelog:
//gcasola: SF2-351 Category Location Refactor 09-09-2019
/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
/*    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SPIII-1678 API Name changed 
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-07              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@IsTest
private class Test_PlaceDataHandlerService {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Place Name,Quote Line Management';
        insert byPassFlow;
    }
    
    @TestSetup
    static void setupData()
    {
        insertBypassFlowNames();
        Test_DataFactory.insertFutureUsers();
        QuoteLineTriggerHandler.disableTrigger = true;
        //test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        list<user> usersTel = [select id,name,code__c from user where Code__c = 'DMC'];
        user testuser = users[0];
        system.runas(testuser){
            List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
            PermissionSetAssignment psain = new PermissionSetAssignment();
            psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
            psain.AssigneeId = testuser.Id;
            psas.add(psain);
            insert psas;
        }
        permissionSetAssignment psa = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        user u = [select id from user where id=: psa.AssigneeId][0];
        
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        system.runas(testuser){
            acc.AccountSource = ConstantsUtil.ACC_SOURCE_MANUAL_LCM;
            acc.Customer_Number__c = '12345';
            acc.OwnerId = userinfo.getUserId();
            insert acc;
        }
        opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
        opp.OwnerId = Userinfo.getuserid();
        insert opp;
        list<account> accs = new list<account>();
        accs.add(acc);
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
        insert cont;
        List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accs, cont, 1);
        insert bp;
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote1.Allocation_not_empty__c = true;
        quote1.Ad_Allocation_Ids__c = '5de7a446b919f40001888947';
        quote1.SBQQ__ExpirationDate__c = date.today();
        quote1.Billing_Profile__c = bp[0].id;
        insert quote1;
        Product2 product1 = generateProduct();
        insert product1;
        
        SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c 
                                FROM SBQQ__Quote__c LIMIT 1];
        Product2 product3 = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                             ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                             Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                             Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                             Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                             FROM Product2 LIMIT 1 ];
        
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product3.Id, Date.today().addDays(-1), null, null, null, 'Annual');
        quoteLine.Samba_Migration__c = true;
        test.starttest();
        insert quoteLine;
        test.stopTest();
        
        Place__c place = Test_DataFactory.generatePlaces(acc.Id, 'Place Name Test');
        place.PlaceID__c = 'externalPlaceId';
        place.Account__c = acc.Id;
        place.CustomerNumber__c = '12345';
        insert place;
        
        Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
        List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',place.id,cntrct.id,1);
        subs[0].SBQQ__QuoteLine__c = quoteLine.Id;
        subs[0].SBQQ__Product__c = product1.Id;
        subs[0].SBQQ__SubscriptionStartDate__c   = Date.today().addMonths(-13);
        subs[0].SBQQ__Account__c = acc.Id;
        subs[0].Total__c = 500;
        subs[0].Subscription_Term__c = '12';
        system.runas(testuser){
            //List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',place.id,cntrct.id,1);
            insert subs;
        }
        
        /*List<Invoice__c> invoices = Test_DataFactory.createInvoices(1,ConstantsUtil.INVOICE_STATUS_COLLECTED,'    Standard0' );
        invoices[0].invoice_code__c = 'FAK';
        invoices[0].amount__c = 2000;
        invoices[0].customer__c = acc.id;
        invoices[0].tax_mode__c= ConstantsUtil.BILLING_TAX_MODE_EXEMPT;
        invoices[0].billing_profile__c = bp[0].id;
        insert invoices;
        List<Invoice_Order__c> orders = Test_DataFactory.createInvoiceOrders(1, invoices[0].id);
        orders[0].contract__c = cntrct.id;
        insert orders;
        List<Invoice_item__c> items = Test_dataFactory.createInvoiceItems(1,orders[0].id);
        items[0].Amount__c = 1500;
        items[0].subscription__c = subs[0].id;
        insert items;*/
        QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    static void Test_getPlaces()
    {              
        Try{
            //insertBypassFlowNames();
            List<Place__c> places = Test_DataFactory.createPlaces('PName',2);
            insert places;
            RestRequest req = new RestRequest();
            req.params.put('placeId','PL001');//PlaceID__c 
            RestResponse res = new RestResponse();
            
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            PlaceDataHandlerService.getPlaces();
            Test.stopTest();
        }catch(Exception e){                                
            System.Assert(false,e.getMessage());
        }
    }
    
    @isTest
    public static void Test_syncPlaceData_placeIDNotExists(){
        Try{
            List<Place__c> places = Test_DataFactory.createPlaces('PName',10);
            insert places;
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;
            
            Test.startTest();
            List<String> PlaceIdAliases = new list<String> {'097'};
                
            User sysAdmin = [SELECT Id FROM User WHERE Username = 'sysAdminUser@test.com' LIMIT 1];

            System.runAs(sysAdmin){
                PlaceDataHandlerService.syncPlaceData(
                    'NY', 
                    'CompanyName',
                    'US',
                    'info@test.com',
                    'FirstName',
                    'LastName',
                    'externalPlaceId',//PlaceID__c 
                    '809798',
                    null, //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Phone
                    '9496',
                    'Street',
                    null, //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Website
                    'Title',
                    PlaceIdAliases,
                    null,
                    null,
                    null,
                    'new',
                    'SearchName',
                    'Business',
                    'GoldenRecordId',
                    '12345',
                    '',
                    'LocalCH',
                    Datetime.now(),
                    new PlaceDataHandlerService.PoBoxDetails('9496','1234','NY'));
            }
            Test.stopTest();
        }catch(Exception e){                                
            System.Assert(false,e.getMessage());
        }
    }
    
    @isTest
    public static void Test_syncPlaceData_placeIDExists(){
        Try{
            List<Place__c> places = Test_DataFactory.createPlaces('PName',10);
            insert places;
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            List<String> PlaceIdAliases = new list<String> {'0978978'};
            User sysAdmin = [SELECT Id FROM User WHERE Username = 'sysAdminUser@test.com' LIMIT 1];

            System.runAs(sysAdmin){
                PlaceDataHandlerService.syncPlaceData(
                    'NY', 
                    'CompanyName',
                    'US',
                    'info@test.com',
                    'FirstName',
                    'LastName',
                    'externalPlaceId',//PlaceID__c 
                    '809798',
                    '809798',
                    '9496',
                    'Street',
                    'Website',
                    'Title',
                    PlaceIdAliases,
                    null,
                    null,
                    null,
                    'new',
                    'SearchName',
                    'Business',
                    'GoldenRecordId',
                    '12345',
                    '',
                    'LocalCH',
                    Datetime.now(),
                    new PlaceDataHandlerService.PoBoxDetails('9496','12345','NY'));
            }
            Test.stopTest();
        }catch(Exception e){                                
            System.Assert(false,e.getMessage());
        }
    }
    
    @isTest
    public static void test_PlaceDataSync_insertPlace_categories_and_locations(){
        Try{
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;
            
            Test.startTest();
            List<String> PlaceIdAliases = new list<String> {'0978978'};
                PlaceDataHandlerService.CategoryLocation mainCity = new PlaceDataHandlerService.CategoryLocation('mainCityId',false,new PlaceDataHandlerService.NameType('mainCityIt','mainCityFr','mainCityDe','mainCityEn'));
            List<PlaceDataHandlerService.CategoryLocation> categories = new List<PlaceDataHandlerService.CategoryLocation>{
                new PlaceDataHandlerService.CategoryLocation('categoryId',true,new PlaceDataHandlerService.NameType('categoryIt','categoryFr','categoryDe','categoryEn')),
                    new PlaceDataHandlerService.CategoryLocation('categoryId1',false,new PlaceDataHandlerService.NameType('category1It','category1Fr','category1De','category1En')),
                    new PlaceDataHandlerService.CategoryLocation('categoryId2',true,new PlaceDataHandlerService.NameType('category2It','category2Fr','category2De','category2En'))
                    };
                        List<PlaceDataHandlerService.CategoryLocation> locations = new List<PlaceDataHandlerService.CategoryLocation>{
                            new PlaceDataHandlerService.CategoryLocation('locationId',false,new PlaceDataHandlerService.NameType('locationIt','locationFr','locationDe','locationEn')),
                                new PlaceDataHandlerService.CategoryLocation('locationId1',false,new PlaceDataHandlerService.NameType('location1It','location1Fr','location1De','location1En')),
                                mainCity
                                };
                                    Set<Integer> categories_hashCodes = new Set<Integer>();
            Set<Integer> locations_hashCodes = new Set<Integer>();
            Integer mainCity_hashCode = (ConstantsUtil.LOCATION_RT_DEVELOPERNAME + mainCity.Name.It.trim().toLowerCase() + mainCity.Name.En.trim().toLowerCase() + mainCity.Name.De.trim().toLowerCase() + mainCity.Name.Fr.trim().toLowerCase()).hashCode();
            locations_hashCodes.add(mainCity_hashCode);
            
            for(PlaceDataHandlerService.CategoryLocation loc : locations)
            {
                locations_hashCodes.add((ConstantsUtil.LOCATION_RT_DEVELOPERNAME + loc.Name.It.trim().toLowerCase() + loc.Name.En.trim().toLowerCase() + loc.Name.De.trim().toLowerCase() + loc.Name.Fr.trim().toLowerCase()).hashCode());
            }
            
            for(PlaceDataHandlerService.CategoryLocation cat : categories)
            {
                categories_hashCodes.add((ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + cat.Name.It.trim().toLowerCase() + cat.Name.En.trim().toLowerCase() + cat.Name.De.trim().toLowerCase() + cat.Name.Fr.trim().toLowerCase()).hashCode());
            }
            User sysAdmin = [SELECT Id FROM User WHERE Username = 'sysAdminUser@test.com' LIMIT 1];

            System.runAs(sysAdmin){
                PlaceDataHandlerService.syncPlaceData(
                    'NY', 
                    'CompanyName',
                    'US',
                    'info@test.com',
                    'FirstName',
                    'LastName',
                    'externalPlaceId',//PlaceID__c 
                    '809798',
                    '809798', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Phone
                    '9496',
                    'Street',
                    'http://test.com.t', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Website
                    'Title',
                    PlaceIdAliases,
                    mainCity,
                    categories,
                    locations,
                    'new',
                    'SearchName',
                    'Business',
                    'GoldenRecordId',
                    '12345',
                    '',
                    'LocalCH',
                    Datetime.now(),
                    new PlaceDataHandlerService.PoBoxDetails('9496','12345','NY'));
            }
            Test.stopTest();
            
            Place__c p = [SELECT Id FROM Place__c WHERE PlaceID__c = 'externalPlaceId' LIMIT 1];
            List<Category_Location__c> returnedCategories = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.CATEGORY_RT_DEVELOPERNAME).getRecordTypeId() AND clHashCode__c IN :categories_hashCodes];
            List<Category_Location__c> returnedLocations = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId() AND clHashCode__c IN :locations_hashCodes];
            List<Category_Location__c> returnedRelations = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId() AND Place__c = :p.Id];
            
            System.assert(p != null);
            System.assertEquals(categories_hashCodes.size(), returnedCategories.size());
            System.assertEquals(locations_hashCodes.size(), returnedLocations.size());
            System.assertEquals(categories_hashCodes.size() + locations_hashCodes.size(), returnedRelations.size());
        }catch(Exception e){                                
            System.Assert(false,e.getMessage());
        }
    }
    
    @isTest
    public static void test_PlaceDataSync_updatePlace_categories_and_locations(){
        Try{
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;
            
            List<String> PlaceIdAliases = new list<String> {'0978978'};
                PlaceDataHandlerService.CategoryLocation mainCity = new PlaceDataHandlerService.CategoryLocation('mainCityId',false,new PlaceDataHandlerService.NameType('mainCityIt','mainCityFr','mainCityDe','mainCityEn'));
            List<PlaceDataHandlerService.CategoryLocation> categories = new List<PlaceDataHandlerService.CategoryLocation>{
                new PlaceDataHandlerService.CategoryLocation('categoryId',true,new PlaceDataHandlerService.NameType('categoryIt','categoryFr','categoryDe','categoryEn')),
                    new PlaceDataHandlerService.CategoryLocation('categoryId1',false,new PlaceDataHandlerService.NameType('category1It','category1Fr','category1De','category1En')),
                    new PlaceDataHandlerService.CategoryLocation('categoryId2',true,new PlaceDataHandlerService.NameType('category2It','category2Fr','category2De','category2En'))
                    };
                        List<PlaceDataHandlerService.CategoryLocation> locations = new List<PlaceDataHandlerService.CategoryLocation>{
                            new PlaceDataHandlerService.CategoryLocation('locationId',false,new PlaceDataHandlerService.NameType('locationIt','locationFr','locationDe','locationEn')),
                                new PlaceDataHandlerService.CategoryLocation('locationId1',false,new PlaceDataHandlerService.NameType('location1It','location1Fr','location1De','location1En')),
                                mainCity
                                };
                                    Set<Integer> categories_hashCodes = new Set<Integer>();
            Set<Integer> locations_hashCodes = new Set<Integer>();
            Integer mainCity_hashCode = (ConstantsUtil.LOCATION_RT_DEVELOPERNAME + mainCity.Name.It.trim().toLowerCase() + mainCity.Name.En.trim().toLowerCase() + mainCity.Name.De.trim().toLowerCase() + mainCity.Name.Fr.trim().toLowerCase()).hashCode();
            locations_hashCodes.add(mainCity_hashCode);
            
            for(PlaceDataHandlerService.CategoryLocation loc : locations)
            {
                locations_hashCodes.add((ConstantsUtil.LOCATION_RT_DEVELOPERNAME + loc.Name.It.trim().toLowerCase() + loc.Name.En.trim().toLowerCase() + loc.Name.De.trim().toLowerCase() + loc.Name.Fr.trim().toLowerCase()).hashCode());
            }
            
            for(PlaceDataHandlerService.CategoryLocation cat : categories)
            {
                categories_hashCodes.add((ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + cat.Name.It.trim().toLowerCase() + cat.Name.En.trim().toLowerCase() + cat.Name.De.trim().toLowerCase() + cat.Name.Fr.trim().toLowerCase()).hashCode());
            }
            User sysAdmin = [SELECT Id FROM User WHERE Username = 'sysAdminUser@test.com' LIMIT 1];

            System.runAs(sysAdmin){
                PlaceDataHandlerService.syncPlaceData(
                    'NY', 
                    'CompanyName',
                    'US',
                    'info@test.com',
                    'FirstName',
                    'LastName',
                    'externalPlaceId',//PlaceID__c 
                    '809798',
                    '809798', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Phone
                    '9496',
                    'Street',
                    'http://test.com.t', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Website
                    'Title',
                    PlaceIdAliases,
                    mainCity,
                    categories,
                    locations,
                    'new',
                    'SearchName',
                    'Business',
                    'GoldenRecordId',
                    '12345',
                    '',
                    'LocalCH',
                    Datetime.now(),
                    new PlaceDataHandlerService.PoBoxDetails('9496','12345','NY'));
            }
            
            Test.startTest();
            PlaceDataHandlerService.CategoryLocation newLoc = new PlaceDataHandlerService.CategoryLocation('locationId2',false,new PlaceDataHandlerService.NameType('location2It','location2Fr','location2De','location2En'));
            PlaceDataHandlerService.CategoryLocation newCat = new PlaceDataHandlerService.CategoryLocation('categoryId3',true,new PlaceDataHandlerService.NameType('category3It','category3Fr','category3De','category3En'));   
            
            categories.add(newCat);
            locations.add(newLoc);  
            locations_hashCodes.add((ConstantsUtil.LOCATION_RT_DEVELOPERNAME + newLoc.Name.It.trim().toLowerCase() + newLoc.Name.En.trim().toLowerCase() + newLoc.Name.De.trim().toLowerCase() + newLoc.Name.Fr.trim().toLowerCase()).hashCode());       
            categories_hashCodes.add((ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + newCat.Name.It.trim().toLowerCase() + newCat.Name.En.trim().toLowerCase() + newCat.Name.De.trim().toLowerCase() + newCat.Name.Fr.trim().toLowerCase()).hashCode());
            System.runAs(sysAdmin){
                PlaceDataHandlerService.syncPlaceData(
                    'NY', 
                    'CompanyName',
                    'US',
                    'info@test.com',
                    'FirstName12345',
                    'LastName',
                    'externalPlaceId',//PlaceID__c 
                    '809798',
                    '809798', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Phone
                    '9496',
                    'Street',
                    'http://test.com.t', //Vincenzo Laudato 2019-05-31 AS-12 *** Invalid Website
                    'Title',
                    PlaceIdAliases,
                    mainCity,
                    categories,
                    locations,
                    'new',
                    'SearchName',
                    'Business',
                    'GoldenRecordId',
                    '12345',
                    '',
                    'LocalCH',
                    Datetime.now(),
                    new PlaceDataHandlerService.PoBoxDetails('9496','12345','NY'));
            }
            
            
            Test.stopTest();
            
            Place__c p = [SELECT Id, FirstName__c, LastName__c FROM Place__c WHERE PlaceID__c = 'externalPlaceId' LIMIT 1];
            List<Category_Location__c> returnedCategories = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.CATEGORY_RT_DEVELOPERNAME).getRecordTypeId() AND clHashCode__c IN :categories_hashCodes];
            List<Category_Location__c> returnedLocations = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId() AND clHashCode__c IN :locations_hashCodes];
            List<Category_Location__c> returnedRelations = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId() AND Place__c = :p.Id];
            
            System.assert(p != null);
            System.assertEquals('FirstName12345', p.FirstName__c);
            
            System.assertEquals(categories_hashCodes.size(), returnedCategories.size());
            System.assertEquals(locations_hashCodes.size(), returnedLocations.size());
            System.assertEquals(categories_hashCodes.size() + locations_hashCodes.size(), returnedRelations.size());
        }catch(Exception e){                                
            System.Assert(false,e.getMessage());
        }
    }
    @isTest
    public static void test_equalsOverrideCategoryLocation()
    {
        PlaceDataHandlerService.CategoryLocation loc = new PlaceDataHandlerService.CategoryLocation('locationId',false,new PlaceDataHandlerService.NameType('locationIt','locationFr','locationDe','locationEn'));
        PlaceDataHandlerService.CategoryLocation cat = new PlaceDataHandlerService.CategoryLocation('categoryId',true,new PlaceDataHandlerService.NameType('categoryIt','categoryFr','categoryDe','categoryEn'));
        PlaceDataHandlerService.CategoryLocation loc1 = new PlaceDataHandlerService.CategoryLocation('locationId',false,new PlaceDataHandlerService.NameType('locationIt','locationFr','locationDe','locationEn'));
        
        System.assertEquals(true, loc == loc1);
        System.assertEquals(false, loc == cat);
    }
    
    public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}
