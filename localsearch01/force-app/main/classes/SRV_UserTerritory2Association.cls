/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Service Class to query the Users Territory Information and the relative Territory Model Hierarchy.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-08-06
* @systemLayer    Service

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Gennaro Casola
* 2020-08-06      SPIII-2042 - Territorial hierarchy shift from Account to Order object
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class SRV_UserTerritory2Association {
    public static UserTerritoryInformation getUserTerritoryHierarchyByUserIds(Set<Id> userIds){
        UserTerritoryInformation userTerritoryInformation = new UserTerritoryInformation();
        Map<Id, Map<String, Id>> userTerritoryHierarchyMap = new Map<Id, Map<String, Id>>();
        Map<Id, UserTerritory2Association> ut2aMap = SEL_UserTerritory2Association.getUserTerritory2AssocationsByUserId(userIds);
        Map<Id, UserTerritory2Association> ut2aByTerritoryIdsMap;
        Set<Id> territoryIds = new Set<Id>();
        
        if(ut2aMap != NULL && ut2aMap.size() > 0){
            userTerritoryInformation.ut2aMap = ut2aMap;
            for(UserTerritory2Association ut2a : ut2aMap.values()){
                territoryIds.add(ut2a.Territory2Id);
                userTerritoryInformation.ut2asByUserIdMap.put(ut2a.UserId, ut2a.Id);
                
                if(!userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id))
                    userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.put(ut2a.Territory2Id, new Map<String,Id>());
                
                userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2,ut2a.Id);
                
                if(ut2a.Territory2.ParentTerritory2Id != NULL){
                    territoryIds.add(ut2a.Territory2.ParentTerritory2Id);
                    if(!userTerritoryInformation.territoryHierarchy.containsKey(ut2a.Territory2.ParentTerritory2Id))
                        userTerritoryInformation.territoryHierarchy.put(ut2a.Territory2.ParentTerritory2Id, new Set<Id>());
                    
                    userTerritoryInformation.territoryHierarchy.get(ut2a.Territory2.ParentTerritory2Id).add(ut2a.Territory2Id);
                }else{
                    if(!userTerritoryInformation.territoryHierarchy.containsKey(ut2a.Territory2Id))
                        userTerritoryInformation.territoryHierarchy.put(ut2a.Territory2Id, new Set<Id>());
                }
            }
            
            if(territoryIds != NULL && territoryIds.size() > 0){
                ut2aByTerritoryIdsMap = SEL_UserTerritory2Association.getUserTerritory2AssocationsByTerritoryIds(territoryIds);
                if(ut2aByTerritoryIdsMap != NULL && ut2aByTerritoryIdsMap.size() > 0){
                    userTerritoryInformation.ut2aMap.putAll(ut2aByTerritoryIdsMap);
                    for(UserTerritory2Association ut2a : ut2aByTerritoryIdsMap.values()){
                        userTerritoryInformation.ut2asByUserIdMap.put(ut2a.UserId, ut2a.Id);
                        if(!userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id))
                            userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.put(ut2a.Territory2Id, new Map<String,Id>());
                        
                        userTerritoryInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2,ut2a.Id);
                        
                        if(ut2a.Territory2.ParentTerritory2Id != NULL){
                            if(!userTerritoryInformation.territoryHierarchy.containsKey(ut2a.Territory2.ParentTerritory2Id))
                                userTerritoryInformation.territoryHierarchy.put(ut2a.Territory2.ParentTerritory2Id, new Set<Id>());
                            
                            userTerritoryInformation.territoryHierarchy.get(ut2a.Territory2.ParentTerritory2Id).add(ut2a.Territory2Id);
                        }else{
                            if(!userTerritoryInformation.territoryHierarchy.containsKey(ut2a.Territory2Id))
                                userTerritoryInformation.territoryHierarchy.put(ut2a.Territory2Id, new Set<Id>());
                        }
                    }
                }
                
            }
        }
        
        return userTerritoryInformation;
    }
    
    public class UserTerritoryInformation{
        public Map<Id, Set<Id>> territoryHierarchy = new Map<Id, Set<Id>>();
        public Map<Id, Map<String, Id>> ut2aByTerritoryIdAndRoleMap = new Map<Id, Map<String, Id>>();
        public Map<Id, Id> ut2asByUserIdMap = new Map<Id, Id>();
        public Map<Id, UserTerritory2Association> ut2aMap = new Map<Id, UserTerritory2Association>();
    }
}