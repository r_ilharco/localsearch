@isTest
public class Test_OrderUtility {
    
    @testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;		
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    static void test_OrderUtility_AutomaticActivation(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        OrderUtility.activateContracts(new List<Id>{order.Id});
        Test.stopTest();
    }
    
    @isTest
    static void test_OrderUtility_AutomaticTermination(){
        SBQQ.TriggerControl.disable(); 
        ByPassFlow__c bpf = [SELECT Id,Name,FlowNames__c FROM ByPassFlow__c];
        Account account = [SELECT Id FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c,Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        OrderProductTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        insert terminationOrder;
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOrderItem.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        insert childrenOrderItems;
        
        OrderUtility.activateContracts(new List<Id>{terminationOrder.Id});
        Test.stopTest();
        OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable(); 
    }
    
    @isTest
    static void test_OrderUtility_AutomaticCancellation(){
        SBQQ.TriggerControl.disable(); 
        ByPassFlow__c bpf = [SELECT Id,Name,FlowNames__c FROM ByPassFlow__c];
        Account account = [SELECT Id FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        OrderProductTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_CANCELLATION);
        insert terminationOrder;
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOrderItem.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        insert childrenOrderItems;
        
        OrderUtility.activateContracts(new List<Id>{terminationOrder.Id});
        Test.stopTest();
        OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable(); 
    }
    
    /*@isTest
    static void test_OrderUtility_AutomaticAmendment(){
        SBQQ.TriggerControl.disable();
        //Query for Account
        List<String> fieldNamesAccount = new List<String>(Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount =' SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount); 
        //Query for Billing Profile
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c LIMIT 1';
        Billing_Profile__c billingProfile = Database.query(queryBP); 
        //Query for Contact
        List<String> fieldNamesContact = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
        String queryContact =' SELECT ' +String.join( fieldNamesContact, ',' ) +' FROM Contact LIMIT 1';
        Contact contact = Database.query(queryContact); 
        //Query for Quote
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote =' SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote); 
        System.debug('QUOTE --> '+JSON.serializePretty(quote));
        //Query for QuoteLines
        List<String> fieldNamesQuoteLines = new List<String>(Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLines =' SELECT ' +String.join( fieldNamesQuoteLines, ',' ) +' FROM SBQQ__QuoteLine__c';
        List<SBQQ__QuoteLine__c> quoteLines = Database.query(queryQuoteLines); 
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        //Create active contract structure
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        OrderProductTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        
        //Create Amendment Opportunity+Quote+Order
        Opportunity amendOpportunity = Test_DataFactory.generateOpportunity('Amendment Opportunity', account.Id);
		amendOpportunity.SBQQ__AmendedContract__c = contract.Id;
        insert amendOpportunity;
        SBQQ__Quote__c amendQuote = Test_DataFactory.generateQuote(amendOpportunity.Id, account.Id, contact.Id, billingProfile.Id);
        amendQuote.SBQQ__ContractingMethod__c = 'By Subscription End Date';
        amendQuote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_AMENDMENT;
		insert amendQuote;
        //String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        //QuoteModel qm = (QuoteModel) JSON.deserialize(quoteJSON, QuoteModel.class);
        //System.debug('QUOTE MODEL --> '+JSON.serializePretty(qm));
        QuoteTriggerHandler.disableTrigger = false;
        /*Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_CANCELLATION);
        insert terminationOrder;
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
        childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOrderItem.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        insert childrenOrderItems;
        
        OrderUtility.activateContracts(new List<Id>{terminationOrder.Id});
        Test.stopTest();
        OrderProductTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable(); 
    }
    */
    public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Order generateOrder(Contract c, String orderType){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.Type = orderType;
        order.Custom_Type__c = orderType;
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null){
                c.Cancel_Date__c = Date.today();
                order.EffectiveDate = Date.today();
            }
        }
        else{
            
            if(c.TerminateDate__c == null){
                c.TerminateDate__c = Date.today();
                order.EffectiveDate = Date.today();
            }
            else{
                order.EffectiveDate = c.TerminateDate__c;
            }
            order.ContractId = c.id;
        }
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today();
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('FATHER OI ---> '+JSON.serializePretty(fatherOrderItem));
        return fatherOrderItem; 
    }
    public static OrderItem generateChildrenOrderItem(SBQQ__Subscription__c subscription, Order order, String requiredBy, String pbeId){
        OrderItem oi = new OrderItem();
        oi.ServiceDate = Date.today();
        oi.Quantity = subscription.SBQQ__Quantity__c;
        oi.UnitPrice = subscription.SBQQ__ListPrice__c;   
        oi.Product2Id = subscription.SBQQ__Product__c;
        oi.PricebookEntryId = pbeId;
        oi.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        oi.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c;
        oi.SBQQ__Subscription__c = subscription.Id;
        oi.OrderId = order.Id;
        oi.SBQQ__RequiredBy__c = requiredBy;  
        oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('CHILD OI ---> '+JSON.serializePretty(oi));
        return oi;   
    }
    
    public class QuoteModel {
        public SBQQ__Quote__c record;
        public QuoteLineModel[] lineItems;
        public QuoteLineGroupModel[] lineItemGroups;
        public Integer nextKey;
        public Boolean applyAdditionalDiscountLast;
        public Boolean applyPartnerDiscountFirst;
        public Boolean channelDiscountsOffList;
        public Decimal customerTotal;
        public Decimal netTotal;
        public Decimal netNonSegmentTotal;
    } 
    public class QuoteLineModel {
        public SBQQ__QuoteLine__c record;
        public Boolean amountDiscountProrated;
        public Integer parentGroupKey;
        public Integer parentItemKey;
        public Integer key;
        public Boolean upliftable;
        public String configurationType;
        public String configurationEvent;
        public Boolean reconfigurationDisabled;
        public Boolean descriptionLocked;
        public Boolean productQuantityEditable;
        public Decimal productQuantityScale;
        public String dimensionType;
        public Boolean productHasDimensions;
        public Decimal targetCustomerAmount;
        public Decimal targetCustomerTotal;
    }
    public class QuoteLineGroupModel {
        public SBQQ__QuoteLineGroup__c record;
        public Decimal netNonSegmentTotal;
        public Integer key;
    }
}