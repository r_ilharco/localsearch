public class TrialActivationUtil {

    @AuraEnabled
    public static QuoteRelatedWrapper getQuoteLineItems(Id recordId) {
        
        Map<Id, QuoteLineWrapper> mapQuoteLineWrap = new Map<Id, QuoteLineWrapper>();
        Map<Id, SBQQ__QuoteLine__c> mapQuoteLines = new Map<Id, SBQQ__QuoteLine__c>();
        try {
            for(SBQQ__QuoteLine__c ql: [SELECT Id, Name, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Account__c,
                                        SBQQ__Quote__r.SBQQ__Opportunity2__c, SBQQ__Product__c, SBQQ__ProductCode__c, SBQQ__ProductName__c, SBQQ__Quantity__c,
                                        Place__c, Place__r.PlaceID__c, Place__r.Name, Status__c, SBQQ__RequiredBy__c, SBQQ__StartDate__c,
                                        TrialStartDate__c, TrialEndDate__c, TrialStatus__c, SBQQ__Product__r.Eligible_for_Trial__c
                                        FROM SBQQ__QuoteLine__c
                                        WHERE SBQQ__Quote__c = :recordId ]){
                                            
                if(ql.SBQQ__RequiredBy__c == null && ql.SBQQ__Product__r.Eligible_for_Trial__c == TRUE){
                    mapQuoteLineWrap.put(ql.Id, new QuoteLineWrapper(ql));
                }
				else if(ql.SBQQ__RequiredBy__c != null ){
                    mapQuoteLines.put(ql.Id, ql);
                }
            }
            for(SBQQ__QuoteLine__c ql : mapQuoteLines.values()){
                if(mapQuoteLineWrap.containsKey(ql.SBQQ__RequiredBy__c)){
                    mapQuoteLineWrap.get(ql.SBQQ__RequiredBy__c).children.add(new QuoteLineWrapper(ql));
                }
            }
        }
        catch (Exception ex) {
            ErrorHandler.logInfo('TrialActivationUtil', '', ex.getMessage());
        }
        QuoteRelatedWrapper qrw = new QuoteRelatedWrapper();
        qrw.listQuoteLines = mapQuoteLineWrap.values();
        return qrw;
    }

    @AuraEnabled
    public static String saveTriaActivation(String strQLineIds) {
        
        String returnString = 'OK';
        String quoteId;
        
        Savepoint savePoint;
        if(!Test.isRunningTest()) savePoint = Database.setSavepoint();

        try {       
            List<String> setQLId = new List<String>();
            setQLId = strQLineIds.split(',');
            
            Set<Id> quoteIdsToUpdate = new Set<Id>();
            
            if(!setQLId.isEmpty()){
                List<SBQQ__QuoteLine__c> qls = SEL_SBQQQuoteLine.getQLinesByIds(setQLId);
                //Each quoteline has the same related quote
                if(String.isBlank(qls[0].SBQQ__Quote__r.Trial_CallId__c)){
                    quoteId = qls[0].SBQQ__Quote__c;
                    qls[0].SBQQ__Quote__r.Trial_CallId__c = TrialActivationUtil.getCallId(quoteId);
                    update qls[0].SBQQ__Quote__r;
                } 
                
                List<SBQQ__Quote__c> quotes = SEL_SBQQQuote.getQuotesByIds(new Set<Id>{quoteId});
                TrialActivationManager.sendTrialRequest(quotes, setQLId);
                
                Task nTask = new Task(  Subject = 'Trial Activation', 
                                      OwnerId = UserInfo.getUserId(), 
                                      ActivityDate = Date.today()+5, 
                                      WhatId = quoteId,
                                      Status = 'Offen');
                insert nTask;  
            }
            else throw new CustomException('QuoteLines not selected - setQLId is empty');
        }
        catch(CustomException cEx){
            ErrorHandler.logInfo('TrialActivationUtil', '', cEx.getMessage());
            returnString = 'KO';
            if(!Test.isRunningTest()) Database.rollback(savePoint);
        }
        catch(Exception ex) {
            ErrorHandler.logInfo('TrialActivationUtil', '', ex.getMessage());
            returnString = 'KO';
            if(!Test.isRunningTest()) Database.rollback(savePoint);
        }          
        return returnString;
    }
    
    public static String getCallId(String quoteId) {
        String hashString = quoteId + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        return EncodingUtil.convertToHex(hash);
    }

    public class CustomException extends Exception{}

    public class QuoteRelatedWrapper {
        @AuraEnabled 
        List<QuoteLineWrapper> listQuoteLines {get; set;}
        
        public QuoteRelatedWrapper() {
            listQuoteLines = new List<QuoteLineWrapper>();
        }
    }

    public class QuoteLineWrapper {
        @AuraEnabled Id quoteLineId {get; set;}
        @AuraEnabled Id quoteId {get; set;}
        @AuraEnabled string name {get; set;}
        @AuraEnabled String placeId {get; set;}
        @AuraEnabled string placeName {get; set;}
        @AuraEnabled Id productId {get; set;}
        @AuraEnabled string productCode {get; set;}
        @AuraEnabled string productName {get; set;}
        @AuraEnabled string trialStartDate {get; set;}
        @AuraEnabled string trialEndDate {get; set;}
        @AuraEnabled string status {get; set;}
        @AuraEnabled List<QuoteLineWrapper> children {get; set;}
        
        public QuoteLineWrapper(SBQQ__QuoteLine__c ql) {
            quoteLineId = ql.Id;
            quoteId = ql.SBQQ__Quote__c;
            name = ql.Name;
            placeId = ql.Place__r.PlaceID__c;
            placeName = ql.Place__r.Name;
            productId = ql.SBQQ__Product__c;
            productCode = ql.SBQQ__ProductCode__c;
            productName = ql.SBQQ__ProductName__c;
            if(ql.TrialStartDate__c != null) trialStartDate = ql.TrialStartDate__c.format();
            if(ql.TrialEndDate__c != null) trialEndDate = ql.TrialEndDate__c.format();
            status = ql.TrialStatus__c;

            children = new List<QuoteLineWrapper>();
        }
    }
}