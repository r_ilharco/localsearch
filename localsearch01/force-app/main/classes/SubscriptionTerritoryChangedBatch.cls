/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class SubscriptionTerritoryChangedBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    private List<Id> accountIds;
    private String sSQL = null;
    public SubscriptionTerritoryChangedBatch(List<Id> accountIds){
        this.accountIds = new List<Id>(accountIds);
        system.debug('accountIds in constructor: ' + accountIds);
    }
    
    global database.querylocator start(Database.BatchableContext bc){      
        system.debug('accountIds: '+ this.accountIds);
        sSQL = 'SELECT Id, SBQQ__Contract__r.Account.Owner_Changed__c  FROM SBQQ__Subscription__c WHERE SBQQ__Contract__r.AccountId IN :accountIds';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        Map<Id, SBQQ__Subscription__c> subs = new Map<Id, SBQQ__Subscription__c>(scope);
        system.debug('subs: ' + subs);
        try{
            Map<Id, String> subToAreaCode = new Map<Id,String>();
            Map<Id, Group> groupsMap = new Map<Id, Group>();
            List<Id> territoryIds = new List<Id>();
            Map<String,Id> thirdToSecondLevel = new Map<String,Id>();
            Set<String> terrDevName = new Set<String>();
            List<UserTerritory2Association> territoryAssociations = new List<UserTerritory2Association>();
            Map<Id, List<Id>> areaCodeTerritoryRolesMap = new Map<Id, List<Id>>();
            Map<String,Id> groupDevName = new Map<String,Id>();
            
            if(subs.values().size() > 0){
                List<SBQQ__Subscription__Share> oldSubShares = [SELECT Id FROM SBQQ__Subscription__Share WHERE RowCause = :Schema.SBQQ__Quote__Share.RowCause.Territory_Changed_Sharing__c AND ParentId IN:subs.keySet()];
                if(oldSubShares.size() > 0){
                    delete oldSubShares;
                }
                Set<String> areaCodes = new Set<String>();
           /*     for(SBQQ__Subscription__c s : subs.values()){
                    if(s.SBQQ__Contract__r.Account.Area_Code__c != null){
                        areaCodes.add(s.SBQQ__Contract__r.Account.Area_Code__c);
                        subToAreaCode.put(s.Id,s.SBQQ__Contract__r.Account.Area_Code__c);
                    }
                }
                
                if(areaCodes.size() > 0){
                    territoryAssociations = [SELECT Id, UserId, Territory2.Name, 
                                             Territory2.ParentTerritory2Id, 
                                             Territory2.ParentTerritory2.DeveloperName, 
                                             Territory2.ParentTerritory2.Name,
                                             Territory2.DeveloperName
                                             FROM UserTerritory2Association 
                                             WHERE Territory2.Name IN :areaCodes];
                    for(UserTerritory2Association terrAs : territoryAssociations){
                        territoryIds.add(terrAs.Territory2.ParentTerritory2Id);
                        terrDevName.add(terrAs.Territory2.ParentTerritory2.DeveloperName);
                        terrDevName.add(terrAs.Territory2.DeveloperName);
                        thirdToSecondLevel.put(terrAs.Territory2.Name, terrAs.Territory2.ParentTerritory2Id);
                    }
                    
                    if(terrDevName.size() > 0){
                        groupsMap = SEL_Group.getGroupsByDevNameAndType(terrDevName,'Territory');
                        for(Group aGroup : groupsMap.values()){
                            groupDevName.put(aGroup.DeveloperName, aGroup.Id); 
                        }
                        
                        if(groupDevName.size() > 0){
                            for(UserTerritory2Association territory : territoryAssociations){
                                List<Id> groupIds =  new List<Id>();
                                if(groupDevName.containsKey(territory.Territory2.ParentTerritory2.DeveloperName)){
                                    groupIds.add(groupDevName.get(territory.Territory2.ParentTerritory2.DeveloperName));
                                }
                                if(groupDevName.containsKey(territory.Territory2.DeveloperName)){
                                    groupIds.add(groupDevName.get(territory.Territory2.DeveloperName));
                                }
                                if(groupIds.size() > 0){
                                    if(!areaCodeTerritoryRolesMap.containsKey(territory.Territory2.ParentTerritory2Id)){
                                        areaCodeTerritoryRolesMap.put(territory.Territory2.ParentTerritory2Id, groupIds);
                                    }else{
                                        areaCodeTerritoryRolesMap.get(territory.Territory2.ParentTerritory2Id).addAll(groupIds);
                                    }
                                    
                                }
                            }
                            List<SBQQ__Subscription__Share> newSubsShares = new List<SBQQ__Subscription__Share >();
                            for(SBQQ__Subscription__c sub : subs.values()){
                                Id secondLevelId = thirdToSecondLevel.get(subToAreaCode.get(sub.Id));
                                if(areaCodeTerritoryRolesMap.containsKey(secondLevelId)){
                                    for(Id groupId : areaCodeTerritoryRolesMap.get(secondLevelId)){
                                        SBQQ__Subscription__Share newSubShare = new SBQQ__Subscription__Share();
                                        newSubShare.ParentId = sub.id;
                                        newSubShare.UserOrGroupId = groupId;
                                        newSubShare.AccessLevel = 'Read';
                                        newSubShare.RowCause = Schema.SBQQ__Subscription__Share.RowCause.Territory_Changed_Sharing__c;
                                        newSubsShares.add(newSubShare);
                                    }
                                }
                            }
                            if(newSubsShares.size() > 0){
                                insert newSubsShares;
                            }
                        }
                    }                    
                    
                }*/
            }
        }catch(Exception ex){
            system.debug('stackTrace: ' + ex.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext bc){     
        
    }
    
    global void execute(SchedulableContext sc){
    }
    
}