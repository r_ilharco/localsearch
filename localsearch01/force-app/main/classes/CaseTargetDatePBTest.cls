@isTest
public class CaseTargetDatePBTest {
  
    @isTest
    static void testCaseTargetDateLowPriority(){
        
        try{
            Case cs = new Case();
        	cs.RecordTypeId='0121r000000VicNAAS';
        	cs.Priority= 'Low';
       
        	test.startTest();
        	insert cs;
        	update cs;
        	test.stopTest();     

 			}
        	catch(Exception e){	                            
           		System.Assert(false, e.getMessage());
			}
        
    }
    
    @isTest
    static void testCaseTargetDateMediumPriority(){
        
        try{
            Case cs = new Case();
        	cs.RecordTypeId='0121r000000VicNAAS';
        	cs.Priority= 'Medium';
        
        	test.startTest();
        	insert cs;
        	update cs;
        	test.stopTest();

			}
        catch(Exception e){	                            
           		System.Assert(false, e.getMessage());
			}
        
        
    }
    
     @isTest
    static void testCaseTargetDateHighPriority(){
        
        try{
            Case cs = new Case();
            cs.RecordTypeId='0121r000000VicNAAS';
            cs.Priority= 'High';
             
            test.startTest();
            insert cs;
            update cs;
            test.stopTest();

			}
        catch(Exception e){	                            
           		System.Assert(false, e.getMessage());
			}
        
    }
         
}