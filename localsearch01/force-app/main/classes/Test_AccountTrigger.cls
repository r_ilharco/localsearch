/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-01
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Account Trigger (Handler and Helper)
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_AccountTrigger {
    
   
    @testSetup
    public static void test_setupData(){

    }

    
    @isTest
    public static void test_insert(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 2, false);
        insert accounts;
        
        merge accounts[0] accounts[1];
        delete accounts[0];
        Test.stopTest();
    }

    @isTest
    public static void test_updateZip(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Test Name', 1, false)[0];
        insert account;
        AccountTriggerHandler.disableTrigger = false;
        
        Territory2 territory = [SELECT Id FROM Territory2 LIMIT 1];
        ObjectTerritory2Association object2TerritoryAssociation = new ObjectTerritory2Association(ObjectId = account.Id, Territory2Id = territory.Id, AssociationCause='Territory2Manual');
        insert object2TerritoryAssociation;
        
        account.BillingPostalCode = '8006';

        update account;
        

    }
    @isTest
    public static void test_manageFalseTerritoryException(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Test Name', 1, false)[0];
        insert account;
        AccountTriggerHandler.disableTrigger = false;
        
        User u = [select id from user where isActive = true and Code__c =:ConstantsUtil.ROLE_IN_TERRITORY_SMA limit 1];
        account.OwnerId = u.id;
        update account;
    }
	
    @isTest
    public static void test_updateZip_Territory(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Test Name', 1, false)[0];
        insert account;
        AccountTriggerHandler.disableTrigger = false;
        
        Territory2 territory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id != NULL LIMIT 1];
        ObjectTerritory2Association object2TerritoryAssociation = new ObjectTerritory2Association(ObjectId = account.Id, Territory2Id = territory.Id, AssociationCause='Territory2Manual');
        insert object2TerritoryAssociation;
        
        account.BillingPostalCode = '8006';

        update account;
        

    }

    
    @isTest
    public static void test_updateArea(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Test Name', 1, false)[0];
        insert account;
        AccountTriggerHandler.disableTrigger = false;
        
        Territory2 territory = [SELECT Id FROM Territory2 LIMIT 1];
        ObjectTerritory2Association object2TerritoryAssociation = new ObjectTerritory2Association(ObjectId = account.Id, Territory2Id = territory.Id, AssociationCause='Territory2Manual');
        insert object2TerritoryAssociation;
        update account;
        
    }
    
    @isTest
    public static void test_updateStatus(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        Test_DataFactory.insertFutureUsers();
        
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Test Name', 1, false)[0];
        insert account;
        AccountTriggerHandler.disableTrigger = false;
        
        Territory2 territory = [SELECT Id FROM Territory2 LIMIT 1];
        ObjectTerritory2Association object2TerritoryAssociation = new ObjectTerritory2Association(ObjectId = account.Id, Territory2Id = territory.Id, AssociationCause='Territory2Manual');
        insert object2TerritoryAssociation;
        
        account.Status__c = ConstantsUtil.ACCOUNT_STATUS_BLOCKED;
        update account;
    }

     @isTest
    public static void test_checkParentId(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        AccountTriggerHandler.disableTrigger = true;
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 3, false);
        insert accounts;
        AccountTriggerHandler.disableTrigger = false;
		accounts[0].ParentId = accounts[1].id;
       // accounts[1].ParentId = accounts[2].id;
        update accounts;
        List<Account> accounts2 = Test_DataFactory.generateAccounts('Deloitte M', 1, false);
        accounts2[0].Parentid = accounts[1].id;
        //insert accounts2;
    }
    
    @IsTest
    public static void test_resetSubmitterField(){
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        insert accounts;
        
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, ApprovalStep__c, Tech_Request_Assignment_Approved__c, Tech_Request_to_Sell_Approved__c FROM Account]);
        Account newAccount = accountsMap.values()[0];
        Account oldAccount = newAccount.clone(true, true, true, true);
        Map<Id, Account> newAccountsMap = new Map<Id,Account>();
        Map<Id, Account> oldAccountsMap = new Map<Id,Account>();
        
        newAccountsMap.put(newAccount.Id, newAccount);
        oldAccountsMap.put(oldAccount.Id, oldAccount);
        
        newAccount.ApprovalStep__c = NULL;
        oldAccount.ApprovalStep__c = 'Approved';
        
        Test.startTest();
        	ATL_AccountTriggerHelper.resetSubmitterField(newAccountsMap, oldAccountsMap);
        Test.stopTest();
    }
    
    @IsTest
    public static void test_insertCustomerNumber_OK(){
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        insert accounts;
        
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, ApprovalStep__c, Tech_Request_Assignment_Approved__c, Tech_Request_to_Sell_Approved__c FROM Account]);
        Account newAccount = accountsMap.values()[0];
        newAccount.AccountSource = 'Online';
        newAccount.Customer_Number__c = NULL;
        Customer_Number_cs__c cn = new Customer_Number_cs__c();
        cn.Next_Customer_Number__c = 200614338;
        insert cn;
        
        Test.startTest();
        	ATL_AccountTriggerHelper.insertCustomerNumber(new List<Account>{newAccount});
        Test.stopTest();
    }
    
    @IsTest
    public static void test_insertCustomerNumber_KO(){
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        insert accounts;
        
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, ApprovalStep__c, Tech_Request_Assignment_Approved__c, Tech_Request_to_Sell_Approved__c FROM Account]);
        Account newAccount = accountsMap.values()[0];
        newAccount.AccountSource = 'Online';
        
        Test.startTest();
        	ATL_AccountTriggerHelper.insertCustomerNumber(new List<Account>{newAccount});
        Test.stopTest();
    }
    
    @IsTest
    public static void test_manageTerritoryAssignmentRules(){
        Map<Id, UserTerritory2Association> ut2aAssociation = new Map<Id, UserTerritory2Association>([SELECT Id, Territory2Id, UserId FROM UserTerritory2Association WHERE Territory2.Name = 'Territory 4.03' AND RoleInTerritory2 = 'DMC' AND Territory2.ParentTerritory2Id != NULL LIMIT 1]);
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        accounts[0].BillingPostalCode = '8005';
        insert accounts;
        
        accounts[0].OwnerId = ut2aAssociation.values()[0].UserId;
        update accounts[0];
        
        accounts[0].TerritoryException__c = false;
        update accounts[0];
        
        accounts[0].OwnerId = ut2aAssociation.values()[0].UserId;
        
        AccountTriggerHandler.disableTrigger = true;
        update accounts[0];
        AccountTriggerHandler.disableTrigger = false;
        
        accounts[0].TerritoryException__c = true;
        update accounts[0];
        
        accounts[0].TerritoryException__c = false;
        AccountTriggerHandler.disableTrigger = true;
        update accounts[0];
        AccountTriggerHandler.disableTrigger = false;
        
        accounts[0].BillingPostalCode = '1734';
        update accounts[0];
    }
    
    @IsTest
    public static void test_updateRelatedOpportunityQuoteOwner(){
        Map<Id, UserTerritory2Association> ut2aAssociation = new Map<Id, UserTerritory2Association>([SELECT Id, Territory2Id, UserId FROM UserTerritory2Association WHERE Territory2.Name = 'Territory 4.03' AND RoleInTerritory2 = 'DMC' AND Territory2.ParentTerritory2Id != NULL LIMIT 1]);
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        insert accounts;
        

        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity',  accounts[0].Id);
        opportunity.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS; 
       	opportunity.Automated_Opportunity_Flow__c = ConstantsUtil.OPPORTUNITY_FLOW_DMC;
        insert opportunity;
        
        accounts[0].OwnerId = ut2aAssociation.values()[0].UserId;
        update accounts[0];
        
    }
}