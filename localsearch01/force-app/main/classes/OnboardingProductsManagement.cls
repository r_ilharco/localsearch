public class OnboardingProductsManagement{
    
    public class InputParam{
        @InvocableVariable
        public String objType;
        
        @InvocableVariable
        public Id recId;
    }
    
    @InvocableMethod(label='Onboarding Products Management' description='Onboarding Products Management')
    public static void invokeOnboardingProductsManagement(List<InputParam> myData){
        
        System.debug('********** '+myData);
        List<Case> caseToInsert = new List<Case>();
        
        Map<String,Set<Id>> recIdObjTypeMap = new Map<String,Set<Id>>();
        Set<String> objT = new Set<String>();
        Set<Id> subscrId = new Set<Id>();
        Set<Id> subscrOptId = new Set<Id>();
        
        
        for(InputParam aIP : myData){
            Set<Id> temp = recIdObjTypeMap.get(aIP.objType);
            if (temp == null){
                recIdObjTypeMap.put(aIP.objType, new Set<Id>{aIP.recId});
                objT.add(aIP.objType);
            } else {
                temp.add(aIP.recId);
            }
        }
        
        System.debug('recIdObjTypeMap >> ' + recIdObjTypeMap);
        
        List<RecordType> recTypeCaseMgmt = [SELECT Id,DeveloperName 
                                            FROM RecordType
                                            WHERE IsActive = true
                                            AND SobjectType = 'Case'
                                            AND BusinessProcessId IN (SELECT Id 
                                                                      FROM BusinessProcess
                                                                      WHERE Name='Case Management - Support Process')
                                           ];
        
        Map<String,Id> rtDevNameId = new Map<String,Id>();
        for(RecordType rt : recTypeCaseMgmt){
            
            rtDevNameId.put(rt.DeveloperName,rt.Id);
        }
        
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        List<Onboarding_Product_Management__mdt> onboardingPrd = new List<Onboarding_Product_Management__mdt>();
        Map<String,Onboarding_Product_Management__mdt> prodOnboardingRec = new Map<String,Onboarding_Product_Management__mdt>();
        Set<String> prodOnboarding = new Set<String>();
        //Map<Id, Id> idSubRequiredByPlaceId = new Map<Id, Id>();
        //Set<Id> idSubRequiredBy = new Set<Id>();
        Map<Id, Id> idAccPrimCont = new Map<Id, Id>();
        Set<Id> idsAcc = new Set<Id>();
        
        if(objT.size()>0){
            onboardingPrd = [SELECT Product__c, Product_Code__c, Topic__c, Type__c, Case_Subject__c,Case_Record_Type_DevName__c,Portal__c,Case_Origin__c FROM Onboarding_Product_Management__mdt WHERE Type__c IN: objT ];
            for (Onboarding_Product_Management__mdt onboardRec : onboardingPrd){
                prodOnboardingRec.put(onboardRec.Product_Code__c,onboardRec);
                prodOnboarding.add(onboardRec.Product_Code__c);
                
            }
        }
        try{
            /*if ((recIdObjTypeMap.get('Subscription')).size()>0 && onboardingPrd.size()>0){

subscrId = recIdObjTypeMap.get('Subscription');
subscriptions = [SELECT Id, SBQQ__Contract__c, SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Account__c, SBQQ__Account__r.PreferredLanguage__c, Place__c, SBQQ__Product__r.Name FROM SBQQ__Subscription__c WHERE Id IN : subscrId];
*/
            
            if (onboardingPrd.size()>0){
                
                subscrId = recIdObjTypeMap.get('Subscription');
                subscrOptId = recIdObjTypeMap.get('Option');
                
                System.debug('subscrId >> ' + subscrId );
                System.debug('subscrOptId >> ' + subscrOptId  );
                // Inizio - aggiunta prima condizione per le subscription miste -- FA 15-11-2015
                if(subscrId!=null && subscrOptId!=null){
                    subscriptions = [SELECT Id, SBQQ__Contract__c, SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Account__c, SBQQ__Account__r.PreferredLanguage__c,
                                     Place__c,SBQQ__RequiredById__c,SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, SBQQ__QuoteLine__r.LBx_Contact__r.FirstName, 
                                     SBQQ__QuoteLine__r.LBx_Contact__r.LastName, SBQQ__QuoteLine__r.LBx_Contact__r.Email, SBQQ__QuoteLine__r.LBx_Contact__r.Phone,
                                     SBQQ__QuoteLine__r.LBx_Contact__r.Mobile_Phone__c, SBQQ__Product__r.ProductCode, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.LBx_Contact__c
                                     FROM SBQQ__Subscription__c 
                                     WHERE Id IN : subscrId 
                                     OR (Subsctiption_Status__c = 'Active'
                                         AND Id IN : subscrOptId)
                                    ];
                    
                }else // Fine - aggiunta prima condizione per le subscription miste -- FA 15-11-2015
                    if(subscrId!=null){
                        subscriptions = [SELECT Id, SBQQ__Contract__c, SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Account__c, SBQQ__Account__r.PreferredLanguage__c,
                                         Place__c,SBQQ__RequiredById__c,SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, SBQQ__Product__r.ProductCode, SBQQ__QuoteLine__r.LBx_Contact__r.FirstName,
                                         SBQQ__QuoteLine__r.LBx_Contact__r.LastName, SBQQ__QuoteLine__r.LBx_Contact__r.Email, SBQQ__QuoteLine__r.LBx_Contact__r.Phone,
                                         SBQQ__QuoteLine__r.LBx_Contact__r.Mobile_Phone__c
                                         FROM SBQQ__Subscription__c 
                                         WHERE Id IN : subscrId ];
                    }
                else if(subscrOptId!=null){
                    subscriptions = [SELECT Id, SBQQ__Contract__c, SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Account__c, SBQQ__Account__r.PreferredLanguage__c,
                                     Place__c,SBQQ__RequiredById__c,SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, SBQQ__Product__r.ProductCode,
                                     SBQQ__QuoteLine__r.LBx_Contact__r.FirstName, SBQQ__QuoteLine__r.LBx_Contact__r.LastName, SBQQ__QuoteLine__r.LBx_Contact__r.Email, SBQQ__QuoteLine__r.LBx_Contact__r.Phone, 
                                     SBQQ__QuoteLine__r.LBx_Contact__r.Mobile_Phone__c,SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.LBx_Contact__c
                                     FROM SBQQ__Subscription__c 
                                     WHERE Subsctiption_Status__c = 'Active'
                                     AND Id IN : subscrOptId];
                    
                    // Inizio modifica - passaggio non necessario in quanto il campo SBQQ__RequiredById__c dell'option non è ancora reperibile - FA 15-11-2019
                    /*for(SBQQ__Subscription__c subRequiredBy : subscriptions){

if (!String.isBlank(subRequiredBy.SBQQ__RequiredById__c)){

idSubRequiredBy.add(subRequiredBy.SBQQ__RequiredById__c);
}
}

if(idSubRequiredBy.size()>0){

List<SBQQ__Subscription__c> partentSubscript = [SELECT Id, Place__c FROM SBQQ__Subscription__c WHERE Id IN : idSubRequiredBy];

for(SBQQ__Subscription__c ps : partentSubscript){
idSubRequiredByPlaceId.put(ps.Id,ps.Place__c);
}

}*/
                    // Fine modifica - passaggio non necessario in quanto il campo SBQQ__RequiredById__c dell'option non è ancora reperibile - FA 15-11-2019
                }
                for(SBQQ__Subscription__c accSub : subscriptions){
                    
                    if (!String.isBlank(accSub.SBQQ__Account__c)){
                        
                        idsAcc.add(accSub.SBQQ__Account__c);
                    }
                }
                
                if(idsAcc.size()>0){
                    
                    List<Contact> primContacts = [SELECT Id, AccountId FROM Contact WHERE Primary__c = true AND AccountId IN : idsAcc];
                    
                    for(Contact pcs : primContacts){
                        idAccPrimCont.put(pcs.AccountId,pcs.Id);
                    }
                    
                }
            }
            
            for (SBQQ__Subscription__c sub : subscriptions){
                System.debug('sub.SBQQ__Product__r.ProductCode>> ' + sub.SBQQ__Product__r.ProductCode);
                System.debug('&&& sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c>> ' + (!String.isBlank(sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c))+'    '+sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c);
                //if(prodOnboarding.contains(sub.SBQQ__Product__r.Name)){ // modifica per il Product Code - FA 14-11-2019
                if(prodOnboarding.contains(sub.SBQQ__Product__r.ProductCode) && PassExtraValidation(sub)){
                    //Onboarding_Product_Management__mdt onbProdRec = prodOnboardingRec.get(sub.SBQQ__Product__r.Name);
                    Onboarding_Product_Management__mdt onbProdRec = prodOnboardingRec.get(sub.SBQQ__Product__r.ProductCode); // modifica per il Product Code - FA 14-11-2019
                    Case caseNewSubscr = new Case();
                    caseNewSubscr.Subscription__c = sub.Id;
                    caseNewSubscr.Contract__c = sub.SBQQ__Contract__c;
                    if (!String.isBlank(sub.Place__c)){
                        caseNewSubscr.Place__c = sub.Place__c; // FA 14-11-2019
                        /*} else if (!String.isBlank(sub.SBQQ__RequiredById__c) && idSubRequiredBy.contains(sub.SBQQ__RequiredById__c)){
System.debug(' if della sub.SBQQ__RequiredById__c >> ' + sub.SBQQ__RequiredById__c);
caseNewSubscr.Place__c = idSubRequiredByPlaceId.get(sub.SBQQ__RequiredById__c); // FA 14-11-2019
System.debug(' caseNewSubscr.Place__c >> ' + caseNewSubscr.Place__c);
}*/
                    } else if (!String.isBlank(sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c)){
                        System.debug(' if della sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c'+ sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c);
                        caseNewSubscr.Place__c = sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c;
                        System.debug(' caseNewSubscr.Place__c >> ' + caseNewSubscr.Place__c);
                    }
                    caseNewSubscr.Opportunity__c = sub.SBQQ__Contract__r.SBQQ__Opportunity__c ;
                    caseNewSubscr.AccountId = sub.SBQQ__Account__c;
                    caseNewSubscr.Language__c = sub.SBQQ__Account__r.PreferredLanguage__c;
                    //
                    if(!String.isBlank(sub.SBQQ__QuoteLine__r.LBx_Contact__c)){
                        caseNewSubscr.ContactId = sub.SBQQ__QuoteLine__r.LBx_Contact__c;
                    }else if(!String.isBlank(sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.LBx_Contact__c)){
                        caseNewSubscr.ContactId = sub.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.LBx_Contact__c;
                    }
                    else if(!String.isBlank(sub.SBQQ__Account__c) && idsAcc.contains(sub.SBQQ__Account__c)){
                        caseNewSubscr.ContactId = idAccPrimCont.get(sub.SBQQ__Account__c);
                    }
                    caseNewSubscr.Subject = onbProdRec.Case_Subject__c;
                    caseNewSubscr.RecordTypeId = rtDevNameId.get(onbProdRec.Case_Record_Type_DevName__c);
                    caseNewSubscr.Topic__c = onbProdRec.Topic__c;
                    caseNewSubscr.Product__c = onbProdRec.Product__c;
                    if(onbProdRec.Portal__c != null)
                        caseNewSubscr.Portal__c = onbProdRec.Portal__c;
                    if(!String.isBlank(onbProdRec.Case_Origin__c)){
                        caseNewSubscr.Origin = onbProdRec.Case_Origin__c;
                    }
                    caseToInsert.add(caseNewSubscr);
                    
                }
                
            }
            
            if(caseToInsert.size()>0){
                insert caseToInsert;
            }
            
        }        
        catch (Exception e){
            system.debug('Error: ' + e.getMessage());
        }
    }
    
    private static boolean PassExtraValidation(SBQQ__Subscription__c sub ){
      if(sub == null || sub.SBQQ__Product__r == null) return true;
        
        if(sub.SBQQ__Product__r.ProductCode != ConstantsUtil.ONEPRESENCE001_AR) return true;
        
        boolean reachSFound= false; 
        boolean agencyFound = false;
        
        List<SBQQ__Subscription__c> ls =  [SELECT Id, Product_Code__c
                                           FROM SBQQ__Subscription__c 
                                           WHERE Subsctiption_Status__c = 'Active' and SBQQ__RequiredById__c IN (:sub.Id) ];
        
        set<string> agencyService = new set<string>{'O_ONEP_AGENCYSERVICE_S_001','O_ONEP_AGENCYSERVICE_M_001','O_ONEP_AGENCYSERVICE_L_001'};
            
            for(SBQQ__Subscription__c s : ls){
                if(s.Product_Code__c == 'O_ONEP_REACH_M_001' ||  s.Product_Code__c == 'O_ONEP_REACH_L_001' ) return true; // if subproducts found Reach M or L create case
                if(s.Product_Code__c == 'O_ONEP_REACH_S_001'){
                    reachSFound = true;
                }
                
                if(agencyService.contains(s.Product_Code__c )){
                    agencyFound = true;
                }
                
                if(reachSFound && agencyFound){ // if subproducts found Reach S and Agency create case
                    return true;
                }  
            }
        return false;
    }
    
}