@isTest
public class Test_ManualProcessSendEmailToCustomer {
	@TestSetup
    public static void setup(){
        List<Account> listAccount = Test_DataFactory.generateAccounts('Test Account Name', 1, false);
        insert listAccount;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', listAccount[0].Id);
        insert opportunity;
        List<Contact> listContact = Test_DataFactory.generateContactsForAccount(listAccount[0].Id, 'Contact Last Name', 1);
        insert listContact;
		SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opportunity.Id, listAccount[0].Id , listContact[0].Id, null);   
        insert quote;
        SBQQ__QuoteTemplate__c quoteTemplate = new SBQQ__QuoteTemplate__c();
        insert quoteTemplate;
        SBQQ__QuoteDocument__c quoteDocument = Test_DataFactory.generateQuoteDocument(quote.Id);
        quoteDocument.SBQQ__QuoteTemplate__c = quoteTemplate.Id;
        insert quoteDocument;
        ContentNote n = new ContentNote();
        n.title = 'My Note from Apex';
        insert n;
        ContentDocumentLink link = new ContentDocumentLink();
        link.LinkedEntityId = quoteDocument.Id;
        link.ContentDocumentId = n.id;
        link.ShareType = 'V';
        link.Visibility = 'AllUsers';
        insert link;
    }
    
    @isTest
    public static void ManualProcessSendEmailToCustomer(){
        List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, SBQQ__Quote__c, SBQQ__AttachmentId__c, SBQQ__CustomerAmount__c, SBQQ__DocumentId__c, SBQQ__ElectronicSignature__c, SBQQ__Key__c, SBQQ__ListAmount__c, SBQQ__NetAmount__c, SBQQ__Opportunity__c, SBQQ__OutputFormat__c, SBQQ__PaperSize__c, SBQQ__PrimaryContactId__c, SBQQ__QuoteTemplate__c, SBQQ__SignatureStatus__c, SBQQ__Template__c, SBQQ__Version__c, SBQQ__ViewRecordId__c, URL__c, SignatureDate__c, EnvelopeId__c, SignedDocumentId__c, NamirialSignature_ProcessType__c, ManualProcess__c FROM SBQQ__QuoteDocument__c LIMIT 1];
        
        Test.startTest();
        Profile p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.debug('Profile -> ' + UserInfo.getProfileId() + ' ' + p.Name);
        ManualProcessSendEmailToCustomer.manualProcessSendEmailToCustomer(quoteDocuments);
        Test.stopTest();
    }
    
    @isTest
    public static void ManualProcessSendEmailToCustomer_Italian(){
        List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id, IsDeleted, Name, SBQQ__Quote__r.Filtered_Primary_Contact__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, SBQQ__Quote__c, SBQQ__AttachmentId__c, SBQQ__CustomerAmount__c, SBQQ__DocumentId__c, SBQQ__ElectronicSignature__c, SBQQ__Key__c, SBQQ__ListAmount__c, SBQQ__NetAmount__c, SBQQ__Opportunity__c, SBQQ__OutputFormat__c, SBQQ__PaperSize__c, SBQQ__PrimaryContactId__c, SBQQ__QuoteTemplate__c, SBQQ__SignatureStatus__c, SBQQ__Template__c, SBQQ__Version__c, SBQQ__ViewRecordId__c, URL__c, SignatureDate__c, EnvelopeId__c, SignedDocumentId__c, NamirialSignature_ProcessType__c, ManualProcess__c FROM SBQQ__QuoteDocument__c LIMIT 1];
        Contact c = [SELECT Language__c FROM Contact WHERE Id = :quoteDocuments[0].SBQQ__Quote__r.Filtered_Primary_Contact__c];
        c.Language__c = 'Italian';
        update c;
        Test.startTest();
        Profile p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.debug('Profile -> ' + UserInfo.getProfileId() + ' ' + p.Name);
        ManualProcessSendEmailToCustomer.manualProcessSendEmailToCustomer(quoteDocuments);
        Test.stopTest();
    }
    
    @isTest
    public static void ManualProcessSendEmailToCustomer_French(){
        List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id, IsDeleted, Name, SBQQ__Quote__r.Filtered_Primary_Contact__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, SBQQ__Quote__c, SBQQ__AttachmentId__c, SBQQ__CustomerAmount__c, SBQQ__DocumentId__c, SBQQ__ElectronicSignature__c, SBQQ__Key__c, SBQQ__ListAmount__c, SBQQ__NetAmount__c, SBQQ__Opportunity__c, SBQQ__OutputFormat__c, SBQQ__PaperSize__c, SBQQ__PrimaryContactId__c, SBQQ__QuoteTemplate__c, SBQQ__SignatureStatus__c, SBQQ__Template__c, SBQQ__Version__c, SBQQ__ViewRecordId__c, URL__c, SignatureDate__c, EnvelopeId__c, SignedDocumentId__c, NamirialSignature_ProcessType__c, ManualProcess__c FROM SBQQ__QuoteDocument__c LIMIT 1];
        Contact c = [SELECT Language__c FROM Contact WHERE Id = :quoteDocuments[0].SBQQ__Quote__r.Filtered_Primary_Contact__c];
        c.Language__c = 'French';
        update c;
        Test.startTest();
        Profile p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.debug('Profile -> ' + UserInfo.getProfileId() + ' ' + p.Name);
        ManualProcessSendEmailToCustomer.manualProcessSendEmailToCustomer(quoteDocuments);
        Test.stopTest();
    }
    
    @isTest
    public static void ManualProcessSendEmailToCustomer_NoLanguage(){
        List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id, IsDeleted, Name, SBQQ__Quote__r.Filtered_Primary_Contact__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, SBQQ__Quote__c, SBQQ__AttachmentId__c, SBQQ__CustomerAmount__c, SBQQ__DocumentId__c, SBQQ__ElectronicSignature__c, SBQQ__Key__c, SBQQ__ListAmount__c, SBQQ__NetAmount__c, SBQQ__Opportunity__c, SBQQ__OutputFormat__c, SBQQ__PaperSize__c, SBQQ__PrimaryContactId__c, SBQQ__QuoteTemplate__c, SBQQ__SignatureStatus__c, SBQQ__Template__c, SBQQ__Version__c, SBQQ__ViewRecordId__c, URL__c, SignatureDate__c, EnvelopeId__c, SignedDocumentId__c, NamirialSignature_ProcessType__c, ManualProcess__c FROM SBQQ__QuoteDocument__c LIMIT 1];
        Contact c = [SELECT Language__c FROM Contact WHERE Id = :quoteDocuments[0].SBQQ__Quote__r.Filtered_Primary_Contact__c];
        c.Language__c = NULL;
        update c;
        Test.startTest();
        Profile p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        System.debug('Profile -> ' + UserInfo.getProfileId() + ' ' + p.Name);
        ManualProcessSendEmailToCustomer.manualProcessSendEmailToCustomer(quoteDocuments);
        Test.stopTest();
    }
    
}