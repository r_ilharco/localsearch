@isTest
public class RetryOrderActivationControllerTest {
	@TestSetup
    public static void setup(){
        
        Test_OrderUtility.test_setupData(); 
        Account acc = [select id from Account limit 1];
        Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
        list<order> orders = test_dataFactory.createOrders(acc,cntrct,'activated',1);
        insert orders;
    }
    
    
    @isTest
    public static void RetryOrderActivationController(){
        Order ord = [select Id, OwnerId, ContractId, AccountId, Pricebook2Id, OriginalOrderId, OpportunityId, QuoteId, EffectiveDate, EndDate, IsReductionOrder, Status, Description, CustomerAuthorizedById, CustomerAuthorizedDate, CompanyAuthorizedById, CompanyAuthorizedDate, Type, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, ShippingAddress, Name, PoDate, PoNumber, OrderReferenceNumber, BillToContactId, ShipToContactId, ActivatedDate, ActivatedById, StatusCode, OrderNumber, TotalAmount, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, IsDeleted, SystemModstamp, LastViewedDate, LastReferencedDate, SBQQ__Contracted__c, SBQQ__ContractingMethod__c, SBQQ__PaymentTerm__c, SBQQ__PriceCalcStatusMessage__c, SBQQ__PriceCalcStatus__c, SBQQ__Quote__c, SBQQ__RenewalTerm__c, SBQQ__RenewalUpliftRate__c, SBQQ__OrderBookings__c, SBQQ__TaxAmount__c, Order_Loaded__c, Number_of_Products__c, Billing_Channel__c, Billing_Profile__c, CallId__c, Call_API__c, Cluster_Id__c, ContractSAMBA_Key__c, Contract__c, Created_date_delay__c, Custom_Type__c, Document_Url__c, External_Id__c, Filtered_Primary_Contact__c, LastModified_Date_delay__c, Manual_Activation__c, Migrated__c, New_Customer__c, No_Clawback__c, Parent_Order__c, Products__c, SambaIsBilled__c, SambaIsStartConfirmed__c, Samba_ExternalID__c, Type__c, nextRenewalDate__c, Manual_Activation_Roll__c, Manual_Amendment_Count__c, Manual_Termination_Count__c, Partial_Amendment_Roll__c, Partial_Roll__c, Total_Net_Amount__c, Processed__c, Contract_Check__c  from Order LIMIT 1];
        
        Test.startTest();
        RetryOrderActivationController.retryOrderActivation(ord.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void RetryOrderActivationifActivatedController(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        OrderTriggerHandler.disableTrigger= true;
        OrderProductTriggerHandler.disableTrigger=true;
        SBQQ.TriggerControl.disable(); 
        order.status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        update order;
        OrderTriggerHandler.disableTrigger= false;
        OrderProductTriggerHandler.disableTrigger= false;
        SBQQ.TriggerControl.enable(); 
        Test.startTest();
        RetryOrderActivationController.retryOrderActivation(order.Id);
        Test.stopTest();
    }
     @isTest
    public static void RetryOrderActivationifActivatedUpgradeController(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        order.Type = ConstantsUtil.ORDER_TYPE_UPGRADE;
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
        OrderTriggerHandler.disableTrigger= true;
        OrderProductTriggerHandler.disableTrigger=true;
        SBQQ.TriggerControl.disable(); 
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
       
        order.status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        update order;
        OrderTriggerHandler.disableTrigger= false;
        OrderProductTriggerHandler.disableTrigger= false;
        SBQQ.TriggerControl.enable(); 
        Test.startTest();
        RetryOrderActivationController.retryOrderActivation(order.Id);
        Test.stopTest();
        
    }
}