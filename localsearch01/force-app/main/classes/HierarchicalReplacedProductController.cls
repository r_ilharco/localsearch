/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Apex Class Controller for HierachicalReplacedProduct.cmp, It retrieves the hierarchy of Subscriptiodns 
* based on the argument passed (QuoteId) in order to show them into the TreeGrid Component.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mara Mazzarella   <mamazzarella@deloitte.it>
* @created        2020-08-10
* @systemLayer    Invocation
* @TestCloass     TestHierarchicalReplacedProductCtrl 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class HierarchicalReplacedProductController {
    private static CurrentUserInfo currentUserInfo = new CurrentUserInfo();
     
      @AuraEnabled
     public static SubscriptionsResponse getReplacedByQuoteId(Id quoteId){
         Set<Id> lineIds = new Set<Id>();
         for(SBQQ__QuoteLine__c ql :[select id from SBQQ__QuoteLine__c where SBQQ__Quote__c =:quoteId ]){
             lineIds.add(ql.Id);
         }
         SubscriptionsResponse res = new SubscriptionsResponse();
         Set<Id> productIds = new Set<Id>();
         String language = UserInfo.getLanguage();
         Map<Id, SBQQ__Subscription__c> subsMap = SEL_Subscription.getReplacedSubscriptionsByQuoteId(lineIds);        
         for(SBQQ__Subscription__c s : subsMap.values()){
             productIds.add(s.SBQQ__Product__c);
         }
         
         List<SBQQ__Localization__c> localizations = [SELECT ID, SBQQ__Product__c, SBQQ__Language__c, SBQQ__Text__c 
                                                      FROM SBQQ__Localization__c 
                                                      WHERE SBQQ__Product__c IN :productIds 
                                                      AND SBQQ__Label__c = :ConstantsUtil.TRANSLATION_LABEL_PRODUCT_NAME];
         
         Map<ID, Map<String, String>> productLanguageTranslation = new Map<ID, Map<String, String>>();
         
         for(SBQQ__Localization__c loc : localizations){
             if(!String.isBlank(loc.SBQQ__Text__c)){
                 Map<String, String> languageTranslation = new Map<String, String>();
                 languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                 if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                     productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                 }else{
                     productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                 }
             }
         }
         
         for(SBQQ__Subscription__c sub: subsMap.values()){
             String productName = sub.SBQQ__ProductName__c;
             
             if(productLanguageTranslation.containsKey(sub.SBQQ__Product__c) && productLanguageTranslation.get(sub.SBQQ__Product__c).containsKey(language)){
                 productName = productLanguageTranslation.get(sub.SBQQ__Product__c).get(language);
             }
             
             Subscriptionswrapper wrap = new Subscriptionswrapper();
             wrap.id = sub.id;
             wrap.name = sub.Name;
             wrap.placeId = sub.Place__c;
             wrap.billingStatus = sub.Billing_Status__c;
             wrap.placeName = sub.Place_Name__c;
             wrap.place_ID = sub.Place__r.PlaceId__c;
             wrap.productId = sub.SBQQ__Product__c;
             wrap.productName = productName;
             wrap.qlProductName = sub.Replaced_by_Product__r.SBQQ__Product__r.Name;
             wrap.status = sub.Subsctiption_Status__c;
             wrap.startDate = sub.SBQQ__StartDate__c;
             wrap.endDate = sub.End_Date__c;
             wrap.inTermination = String.valueOf(sub.In_Termination__c);
             wrap.terminationDate = sub.Termination_Date__c;
             wrap.total = sub.Total__c;
             wrap.SubscriptionTerm = sub.Subscription_Term__c;
             wrap.baseTermPrice = sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
             wrap.quantity = sub.SBQQ__Quantity__c;
             wrap.lineId = sub.Replaced_by_Product__c;
             wrap.lineName = sub.Replaced_by_Product__r.Name;
             wrap.contractId = sub.SBQQ__Contract__c;
             wrap.contractNumber = sub.SBQQ__Contract__r.ContractNumber;
             
             if(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
                 Decimal roundedDiscount = (Decimal)Math.round(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                 wrap.discount = roundedDiscount;
             }
             if(String.isNotBlank(sub.SBQQ__Product__r.Base_term_Renewal_term__c)){
                 wrap.BasetermRenewalTerm = Decimal.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c);
             }
             wrap.showOptions = sub.SBQQ__Bundled__c || sub.Subsctiption_Status__c != ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
             
             if(sub.SBQQ__RequiredById__c == null){
                 res.subscriptions.add(wrap);
             } 
             else {
                 for(Subscriptionswrapper w : res.subscriptions){
                     if(w.id == sub.SBQQ__RequiredById__c){
                         w.childrens.add(wrap);
                         break;
                     }
                 } 
             }
         }
         res.valid = true;
         System.debug('response '+res);
         return res;
     }
     @AuraEnabled
     public static ComponentAccessResponse getAccess(Id recordId)
     {
         ComponentAccessResponse caResponse = new ComponentAccessResponse();
         caResponse.isUserAllowed = true;
         caResponse.isComponentEnabled = true;
         Map<Id, SBQQ__Quote__c> quotesMap = SEL_Quote.getQuoteById(new Set<Id>{recordId});
         Boolean isUserAllowed = currentUserInfo.dmcUser || currentUserInfo.adminUser || currentUserInfo.digitalAgencyUser || currentUserInfo.teleSalesUser;
         
         if(!isUserAllowed){
             caResponse.isUserAllowed = false;
             caResponse.isComponentEnabled = false;
             caResponse.info.errorMessages.add(Label.Namirial_UI_ErrorMessage_NoSufficientPermissions);
         }else if(quotesMap != NULL && quotesMap.size() == 1){
             SBQQ__Quote__c quote = quotesMap.values()[0];
             Map<Id, SBQQ__QuoteLine__c> quoteLinesMap = SEL_SBQQQuoteLine.getParentQLinesByQuoteId(new Set<Id>{quote.Id});
             if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quote.SBQQ__Type__c)){
                 caResponse.isUserAllowed = false;
                 caResponse.isComponentEnabled = false;
                 caResponse.info.errorMessages.add(Label.ProductSubstitution_Replacement_QuoteType_Required);
             }else if(quoteLinesMap == NULL || (quoteLinesMap != NULL && quoteLinesMap.size() == 0)){
                 caResponse.isUserAllowed = false;
                 caResponse.isComponentEnabled = false;
                 caResponse.info.errorMessages.add(Label.ProductSubstitution_AtLeast_One_QuoteLine_Needed);
             }
         }
         
         return caResponse;
     }
     @AuraEnabled
     public static String getProfileName(){
         Id profileId = userinfo.getProfileId();
         String profileName=[SELECT Id, Name FROM Profile WHERE Id=:profileId].Name;
         List<PermissionSetAssignment> pSAssign = [SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId =:Userinfo.getUserId()];
         for(PermissionSetAssignment ps : pSAssign){
             profileName +=';'+ps.PermissionSet.Name;
         }
         return profileName;
     }
     
      public class SubscriptionsResponse{
         @AuraEnabled public boolean valid {get; set;}
         @AuraEnabled public List<Subscriptionswrapper> subscriptions {get; set;}
         public SubscriptionsResponse(){
             subscriptions = new List<Subscriptionswrapper>();
         }
     }
     
     public class Subscriptionswrapper{
         @AuraEnabled public Id id {get; set;}
         @AuraEnabled public string name {get; set;}
         @AuraEnabled public Id placeId {get; set;}
         @AuraEnabled public string placeName {get; set;}
         @AuraEnabled public String place_ID {get; set;}
         @AuraEnabled public Id productId {get; set;}
         @AuraEnabled public string productName {get; set;}
         @AuraEnabled public string qlProductName {get; set;}
         @AuraEnabled public string billingStatus {get; set;}
         @AuraEnabled public string status {get; set;}
         @AuraEnabled public Boolean showOptions {get; set;}
         @AuraEnabled public Date startDate {get; set;}
         @AuraEnabled public Date endDate {get; set;}
         @AuraEnabled public String inTermination {get; set;}
         @AuraEnabled public Date terminationDate {get; set;}
         @AuraEnabled public List<Subscriptionswrapper> childrens {get; set;}
         @AuraEnabled public Decimal discount {get; set;}
         @AuraEnabled public Decimal total {get; set;}
         @AuraEnabled public string SubscriptionTerm {get; set;}
         @AuraEnabled public Decimal BasetermRenewalTerm {get; set;}
         @AuraEnabled public Decimal baseTermPrice {get; set;}
         @AuraEnabled public Decimal quantity {get; set;}
         @AuraEnabled public Id lineId {get; set;}
         @AuraEnabled public String lineName {get; set;}
         @AuraEnabled public Id contractId {get; set;}
         @AuraEnabled public String contractNumber {get; set;}
         
         public Subscriptionswrapper(){
             childrens = new List<Subscriptionswrapper>();
         }
     }
     public class ComponentAccessResponse extends UIResponseMessage{
         @AuraEnabled public Boolean isUserAllowed;
         @AuraEnabled public Boolean isComponentEnabled;
         @AuraEnabled public CurrentUserInfo currentUserInfo;
         
         ComponentAccessResponse(){
             currentUserInfo = new CurrentUserInfo();
         }
     }
     public virtual class UIResponseMessage{
         @AuraEnabled public Info info;
         
         UIResponseMessage()
         {
             this.info = new Info();
         }
     }
     
     public class Info{
         @AuraEnabled public List<String> messages;
         @AuraEnabled public String status;
         @AuraEnabled public String errorCode;
         @AuraEnabled public List<String> errorMessages;
         
         Info()
         {
             this.messages = new List<String>();
             this.errorMessages = new List<String>();
         }
     }
      public class CurrentUserInfo{
         @AuraEnabled public Boolean dmcUser;
         @AuraEnabled public Boolean teleSalesUser;
         @AuraEnabled public Boolean salesUser;
         @AuraEnabled public Boolean adminUser;
         @AuraEnabled public Boolean digitalAgencyUser;
         
         CurrentUserInfo(){
             User currentUser = [SELECT Id, Name, Profile.Name, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Label IN (:ConstantsUtil.DMCPERMISSIONSET_LABEL)) FROM User WHERE Id = :UserInfo.getUserId()];
             this.dmcUser = false;
             this.teleSalesUser = false;
             this.salesUser = false;
             this.adminUser = false;
             this.digitalAgencyUser = false;
             
             if(ConstantsUtil.TELESALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
                 this.teleSalesUser = true;
             }else if(ConstantsUtil.SALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
                 this.salesUser = true;
             }else if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                 this.adminUser = true;
             }else if(ConstantsUtil.DIGITALAGENCYPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                 this.digitalAgencyUser = true;
             }
             
             if(currentUser.PermissionSetAssignments.size() > 0){
                 this.dmcUser = true;
             }
         }
     }
 }