/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 03-02-2020
 * Sprint      : 
 * Work item   :
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */

public class ManualProcessSendEmailToCustomer {
    
    @InvocableMethod(label='ManualProcessSendEmailToCustomer' description='Send an email for confirmation to the customer')
    public static void manualProcessSendEmailToCustomer(List<SBQQ__QuoteDocument__c> quoteDocuments)
    {
        Map<Id, EmailTemplate> emailTemplatesMap = new Map<Id, EmailTemplate>();
        Map<String, Id> emailTemplateIdsByLanguage = new Map<String, Id>();
        Set<Id> quoteDocumentIds = new Set<Id>();
        Map<Id, ContentDocumentLink> contentDocumentLinksMap = new Map<Id, ContentDocumentLink>();
        Map<Id, SBQQ__Quote__c> quotesMap = new Map<Id,SBQQ__Quote__c>();
        Map<Id, Set<Id>> contentDocumentLinksByQuoteDocument = new Map<Id, Set<Id>>();
        Set<Id> quoteIds = new Set<Id>();
        Set<Id> quoteTemplateIds = new Set<Id>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        
        String emailTemplateLIKE = ConstantsUtil.EMAIL_TEMPLATE_MANUAL_PROCESS_SEND_EMAIL + '%';
        emailTemplatesMap = new Map<Id, EmailTemplate>([SELECT Id, Name, DeveloperName, HtmlValue, Subject FROM EmailTemplate WHERE DeveloperName LIKE :emailTemplateLIKE]);
        
        if(emailTemplatesMap != NULL && emailTemplatesMap.size() > 0)
        {
            for(EmailTemplate et: emailTemplatesMap.values())
            {
                if(et.DeveloperName.containsIgnoreCase('de') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('de'))
                {
                    emailTemplateIdsByLanguage.put('de',et.Id);
                }else if(et.DeveloperName.containsIgnoreCase('it') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('it'))
                {
                    emailTemplateIdsByLanguage.put('it',et.Id);
                }else if(et.DeveloperName.containsIgnoreCase('fr') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('fr'))
                {
                    emailTemplateIdsByLanguage.put('fr',et.Id);
                }
            }
        }
        
        if(quoteDocuments != NULL && quoteDocuments.size() > 0)
        {
            Map<Id, SBQQ__QuoteTemplate__c> quoteTemplatesMap;
            
            for(SBQQ__QuoteDocument__c qd : quoteDocuments)
            {
                quoteDocumentIds.add(qd.Id);
                if(qd.SBQQ__Quote__c != NULL)
                	quoteIds.add(qd.SBQQ__Quote__c);
                if(qd.SBQQ__QuoteTemplate__c != NULL)
                    quoteTemplateIds.add(qd.SBQQ__QuoteTemplate__c);
            }
            
            quoteTemplatesMap = SEL_QuoteTemplate.getTemplatesById(quoteTemplateIds);
            
            if((quoteDocumentIds != NULL && quoteDocumentIds.size() > 0) && (quoteIds != NULL && quoteIds.size() > 0) && (quoteTemplatesMap != NULL && quoteTemplatesMap.size() > 0))
            {
            	contentDocumentLinksMap = new Map<Id, ContentDocumentLink>([SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.LatestPublishedVersionId
                                                                        FROM ContentDocumentLink 
                                                                        WHERE LinkedEntityId IN :quoteDocumentIds]);
                quotesMap = SEL_Quote.getQuoteById(quoteIds);
                
                if(contentDocumentLinksMap != NULL && contentDocumentLinksMap.size() > 0)
                {
                    for(ContentDocumentLink cdl : contentDocumentLinksMap.values())
                    {
                        if(!contentDocumentLinksByQuoteDocument.containsKey(cdl.LinkedEntityId))
                        {
                            contentDocumentLinksByQuoteDocument.put(cdl.LinkedEntityId, new Set<Id>());
                        }
                        
                        contentDocumentLinksByQuoteDocument.get(cdl.LinkedEntityId).add(cdl.Id);
                    }
                    
                    if(quotesMap != NULL && quotesMap.size() > 0)
                    {
                        for(SBQQ__QuoteDocument__c qd : quoteDocuments)
                        {
                            if(quoteTemplatesMap.containsKey(qd.SBQQ__QuoteTemplate__c)
                               && String.isNotBlank(quoteTemplatesMap.get(qd.SBQQ__QuoteTemplate__c).Name) 
                               && !quoteTemplatesMap.get(qd.SBQQ__QuoteTemplate__c).Name.containsIgnoreCase(ConstantsUtil.TEMPLATENAME_SUBSTRING_TELESALES))
                            {
                                Data data = new Data();
                                data.qd = qd;
                                data.quotesMap = quotesMap;
                                data.contentDocumentLinksMap = contentDocumentLinksMap;
                                data.contentDocumentLinksByQuoteDocument = contentDocumentLinksByQuoteDocument;
                                data.emailTemplateIdsByLanguage = emailTemplateIdsByLanguage;
                                data.emailTemplatesMap = emailTemplatesMap;
                                createMessages(messages, data);
                            }
                        }
                        
                        if(messages != NULL && messages.size() > 0)
                        {
                            Messaging.SendEmailResult[] results;
                            if(!Test.isRunningTest())
                            	results = Messaging.sendEmail(messages);
                            
                            if (results != NULL && results.size() == 1 && results[0].success) {
                                System.debug('The email was sent successfully.');
                            } else if(results != NULL && results.size() == 1) {
                                System.debug('The email failed to send: '
                                      + results[0].errors[0].message);
                            }
                        }
                    }
                }
                
            }
        }
    }
    
    public static void createMessages(List<Messaging.SingleEmailMessage> messages, Data data)
    {
        String subject = '';
        String plainTextBody = '';
        String language = 'de';
        Id templateId = null;
        String bodyHTML = '';
        String email = '';
        Id toContactId = null;
        SBQQ__QuoteDocument__c qd;
        SBQQ__Quote__c quote;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        if(data.qd != NULL)
            qd = data.qd;
        
        if(qd != NULL)
        {
            if(qd.SBQQ__Quote__c != NULL && data.quotesMap != NULL && data.quotesMap.containsKey(qd.SBQQ__Quote__c))
                quote = data.quotesMap.get(qd.SBQQ__Quote__c);
            
            if(quote != NULL)
            {
                if(quote.Filtered_Primary_Contact__c != NULL)
                {
                    toContactId = quote.Filtered_Primary_Contact__c;
                    language = quote.Filtered_Primary_Contact__r.Language__c;
                }else if(quote.SBQQ__PrimaryContact__c != NULL)
                {
                    toContactId = quote.SBQQ__PrimaryContact__c;
                    language = quote.SBQQ__PrimaryContact__r.Language__c;
                }
                
                                    
                if(String.isNotBlank(quote.SBQQ__SalesRep__r.Email))
                {
                    email = data.quotesMap.get(qd.SBQQ__Quote__c).SBQQ__SalesRep__r.Email;
                }
                
                if(String.isNotBlank(language))
                {
                    switch on language
                    {
                        when 'German' {
                            language = 'de';
                            if(data.emailTemplateIdsByLanguage.containsKey('de'))
                            	templateId = data.emailTemplateIdsByLanguage.get('de');
                        }
                        when 'Italian' {
                            language = 'it';
                            if(data.emailTemplateIdsByLanguage.containsKey('it'))
                            	templateId = data.emailTemplateIdsByLanguage.get('it');
                        }
                        when 'French' {
                            language = 'fr';
                            if(data.emailTemplateIdsByLanguage.containsKey('fr'))
                            	templateId = data.emailTemplateIdsByLanguage.get('fr');
                        }
                        when else {
                            language = 'de';
                            if(data.emailTemplateIdsByLanguage.containsKey('de'))
                            	templateId = data.emailTemplateIdsByLanguage.get('de');
                        }
                    }
                }else
                {
                    language = 'de';
                    if(data.emailTemplateIdsByLanguage.containsKey('de'))
                            	templateId = data.emailTemplateIdsByLanguage.get('de');
                }
                
                if(toContactId != NULL && templateId != NULL)
                {
                    Set<String> ids = new Set<String>();
                    if(data.emailTemplatesMap.containsKey(templateId) && String.isNotBlank(data.emailTemplatesMap.get(templateId).HtmlValue))
                    {
                        bodyHTML = data.emailTemplatesMap.get(templateId).HtmlValue.replace('{EMAILDMC}',email);
                    }
                    
                    if(data.emailTemplatesMap.containsKey(templateId) && String.isNotBlank(data.emailTemplatesMap.get(templateId).Subject))
                    {
                        subject = data.emailTemplatesMap.get(templateId).Subject;
                    }
                    
                    message.setToAddresses(new List<Id>{toContactId});
                    message.setTargetObjectId(toContactId);
                    //message.setTemplateId(templateId);
                    message.setSubject(subject);
                    message.setHtmlBody(bodyHTML);
                    message.setWhatId(qd.Id);
                    if(data.contentDocumentLinksByQuoteDocument != NULL && data.contentDocumentLinksByQuoteDocument.containsKey(qd.Id))
                    {
                        for(Id cdl : data.contentDocumentLinksByQuoteDocument.get(qd.Id))
                        {
                            if(data.contentDocumentLinksMap != NULL && data.contentDocumentLinksMap.containsKey(cdl) && data.contentDocumentLinksMap.get(cdl).ContentDocument.LatestPublishedVersionId != NULL)
                            {
                                ids.add(String.valueOf(data.contentDocumentLinksMap.get(cdl).ContentDocument.LatestPublishedVersionId));
                            }
                        }
                        
                        if(ids != NULL && ids.size() > 0)
                        {
                            message.setEntityAttachments(new List<String>(ids));
                            messages.add(message);
                        }
                    }
                }
            }
        }
        
    }
    
    private class Data{
        SBQQ__QuoteDocument__c qd;
        Map<Id, SBQQ__Quote__c> quotesMap;
        Map<Id, ContentDocumentLink> contentDocumentLinksMap;
        Map<Id, Set<Id>> contentDocumentLinksByQuoteDocument;
        Map<Id, EmailTemplate> emailTemplatesMap;
        Map<String, Id> emailTemplateIdsByLanguage;
    }
}