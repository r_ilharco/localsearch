public class QuoteDocumentUtility {
    
    @InvocableMethod
    public static void processQuoteDocument(List<String> quoteIds){
        try{
            if(quoteIds.size() > 0){
                Id quoteId = quoteIds[0];
                List<SBQQ__Quote__c> quoteList = [SELECT Id, Name FROM SBQQ__Quote__c WHERE Id =: quoteId];
                if(quoteList.size() > 0){
                    System.debug('Quote Name= ' + quoteList[0].Name);
                    generateAndSaveQuoteDocument(quoteList[0].Id, quoteList[0].Name);
                }
            }
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.QUOTE_DOCUMENT_AUTO_BP_NAME, null, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, quoteIds[0]);
        }
        
    }
    
    public static void generateAndSaveQuoteDocument(Id quoteId, String quoteName){
        try{
            List<Id> documentIds = new List<Id>();
            String language = ConstantsUtil.QUOTE_DOCUMENT_LANGUAGE;						//'DE';       
            String outputFormat = ConstantsUtil.QUOTE_DOCUMENT_OUTPUT_FORMAT;				//'PDF';
            String paperSize = ConstantsUtil.QUOTE_DOCUMENT_PAPER_SIZE;						//'Default'
            Id templateId = getQuoteTemplateId(ConstantsUtil.QUOTE_DOCUMENT_TEMPLATE_NAME);	//'Quote-German';        
            String documentName = getQuoteDocumentName(quoteName.trim(), Label.Quote_Document_Name_Date_Format);							//'Q-00000-20190212-1840';
            //Id quoteId = 'a0q0E000002L0KIQA0';
            
            if(templateId != null && documentName != null){
                Id generateDocumentJobId = null;
                Id saveProposalJobId = null;
                
                if(!Test.isRunningTest()){
                    generateDocumentJobId = SBQQ.QuoteDocumentController.generateDocument(language, quoteId, templateId, documentName, outputFormat, paperSize, documentIds);                                      
                    documentIds.add(quoteId);                    
                    saveProposalJobId = SBQQ.QuoteDocumentController.saveProposal(language, quoteId, templateId, documentName, outputFormat, paperSize, documentIds);                   
                }
                
                System.debug('Generate Document Job Id= ' + generateDocumentJobId);
                System.debug('Save Proposal Job Id= ' + saveProposalJobId);
                                
                ErrorHandler.logInfo(ConstantsUtil.QUOTE_DOCUMENT_AUTO_BP_NAME, null, ConstantsUtil.CPQ_QUOTE_DOCUMENT_GENERATE_METHODS_CALLED, null, quoteName);
            }
        
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.QUOTE_DOCUMENT_AUTO_BP_NAME, null, e, ErrorHandler.ErrorCode.E_UNKNOWN, e.getMessage(), null, quoteName);
        }
    }
    
   
    
    public static String getQuoteDocumentName(String quoteName, String nameFormat){
        String documentName = null;
        try{
            String dateTimeInfo = Datetime.now().format(nameFormat); //yyyyMMdd-HHmm
            documentName = quoteName + '-' +  dateTimeInfo;            
            system.debug('Document Name = ' + documentName);
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.QUOTE_DOCUMENT_AUTO_BP_NAME, null, e, ErrorHandler.ErrorCode.E_MISSING_CONTENT, e.getMessage(), null, quoteName);
        }
               
        return documentName;        
    }
    
    public static Id getQuoteTemplateId(String quoteTemplateName){
        Id quoteTemplateId = null;
        try{
            SBQQ__QuoteTemplate__c quoteTemplate = [SELECT Id FROM SBQQ__QuoteTemplate__c WHERE Name =: quoteTemplateName LIMIT 1];            
            /*if(quoteTemplateList.size() > 0){            
                quoteTemplateId = quoteTemplateList[0].Id;
            }  */        
            quoteTemplateId = quoteTemplate.Id;
            system.debug('Quote Template Id= ' + quoteTemplateId);
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.QUOTE_DOCUMENT_AUTO_BP_NAME, null, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, quoteTemplateName);
        }
        
        return quoteTemplateId;     
    }

}