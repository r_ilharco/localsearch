/**
* Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Mara Mazzarella <mamazzarella@deloitte.it>
 * Date		   : 07-10-2019
 * Sprint      : 10
 * Work item   : 
 * Testclass   : Test_OrderTrigger
 * Package     : 
 * Description : Trigger Handler on Order
 * Changelog   : 
 */

public class OrderTriggerHandler implements ITriggerHandler{
	public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        OrderTriggerHelper.updateSignatureAndXactlyFields((List<Order>)newItems);
        OrderTriggerHelper.updateTerritoryInformation((List<Order>)newItems);
        OrderTriggerHelper.updateNewCustomerFlag((List<Order>)newItems);
        if(FeatureManagement.checkPermission(ConstantsUtil.PERMISSION_Renovero_Migration)){
            OrderTriggerHelper.setStartDate_Renovero((List<Order>)newItems);
        }else{
            OrderTriggerHelper.setStartDate((List<Order>)newItems);
        }
        
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        OrderTriggerHelper.checkIfTerminationOrderClosed((Map<Id, Order>)newItems,(Map<Id, Order>)oldItems);
        OrderTriggerHelper.deleteAllocation((List<Order >) newItems.values(), (Map<Id, Order>)newItems, (Map<Id, Order>)oldItems);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    public void AfterInsert(Map<Id, SObject> newItems) {}
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){

        Map<Id,Order> fulfilledOrdersMap = new Map<Id,Order>();
        
        for(SObject anObj : newItems.values()){
            Order oldOrder = (Order) oldItems.get(anObj.Id);
            Order anOrder = (Order) anObj;
            
            if((ConstantsUtil.ORDER_STATUS_ACTIVATED).equalsIgnoreCase(anOrder.Status) && anOrder.Status != oldOrder.Status){
                fulfilledOrdersMap.put(anOrder.Id, anOrder);
            }
        }
        
        if(!fulfilledOrdersMap.isEmpty()){
            OrderTriggerHelper.executeTasks(fulfilledOrdersMap);
        }

        //Change order items status when Order is cancelled - vlaudato
        OrderTriggerHelper.cancelOrderItems((List<Order >) newItems.values(), (Map<Id, Order>)newItems, (Map<Id, Order>)oldItems);
    }
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}