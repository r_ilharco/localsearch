public class SRV_Place {
    
	public static Set<Id> checkQuoteAndContract(Set<Id> changedPlaceIds){

       Map <Id, SBQQ__QuoteLine__c> quoteLines = new Map<Id, SBQQ__QuoteLine__c> ( SEL_SBQQQuoteLine.getQLinesByPlaceAndQuoteStatus ( changedPlaceIds,  new List<String> { 'Presented' } ) );
       Set<Id> placeNotValid = new Set<Id> ();
       System.debug('Inside checkQuoteAndContract');
        
       if(quoteLines.keySet().isEmpty()) {
            Map<Id,SBQQ__Subscription__c> subscriptions = new Map <Id, SBQQ__Subscription__c>( SEL_Subscription.getSubscriptionsByPlace (changedPlaceIds, new List<String> { 'Active', 'Draft' } ) );
            System.debug('quoteLines empty');
                
            if(!subscriptions.keySet().isEmpty()) {
                System.debug('supscription not empty');
                for ( Id i : subscriptions.keySet()) {
                     placeNotValid.add(subscriptions.get(i).Place__c); 
                }
            }
           else {
               System.debug('subscriptions empty'); 
           }
        }
        else {
            System.debug('quoteLine not empty');
            for ( Id i : quoteLines.keySet()) {
                placeNotValid.add(quoteLines.get(i).Place__c); 
            }
        }
        
        return placeNotValid;
    }
    
}