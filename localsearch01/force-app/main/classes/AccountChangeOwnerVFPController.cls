/**
 * @testClass Test_AccountChangeOwnerVFPCtrl
 * */

 public class AccountChangeOwnerVFPController {
    public List<Id> accountIds {get;set;}

	public AccountChangeOwnerVFPController(ApexPages.StandardSetController controller){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error Message.'));
    	List<Account> accounts = New List<Account>();
        List<Id> accountIdsTemp = new List<Id>();
    	accounts = controller.getSelected();
        system.debug('accounts: ' + accounts);
        for(Account acc : accounts){
            accountIdsTemp.add(acc.id);
        }
        this.accountIds = accountIdsTemp;
        system.debug('accountIdsTemp: ' + accountIdsTemp);
        system.debug('this.accountIds: ' + this.accountIds);
    }
    
    @RemoteAction
    public static String hasUserGrantMassive(List<String> accountIds){
        
        String result = '';
        System.debug('accountIds -> ' + JSON.serialize(accountIds));
        if(accountIds != NULL){
            for(Integer i = 0; i < accountIds.size(); i++){
                accountIds[i] = accountIds[i].deleteWhitespace();
            }
        }
        Boolean userAllowed = false;
        Boolean territoryAllowed = true;
        Boolean errorDMC = false;
        //ResponseHasUserGrant result = new ResponseHasUserGrant();
        List<Territory2> territories = new List<Territory2>();
        Set<String> territoriesToCheck = new Set<String>();
        Map<String, List<String>> territoryAccountsMap = new Map<String, List<String>>();
        List<UserTerritory2Association> territoryAssociations = new List<UserTerritory2Association>();
        List<Account> accounts = new List<Account>();
        //String ids = accountIds[0].remove(' ').remove('[').remove(']');
        Set<String> recordIds = new Set<String>(accountIds);//new Set<String>(ids.split(','));
        System.debug('recordIds -> ' + JSON.serialize(recordIds));
		User currentUser = [SELECT Id, Name, Profile.Name, Code__c, AssignedTerritory__c, (SELECT PermissionSet.Name, PermissionSet.Label, PermissionSet.PermissionsModifyAllData FROM PermissionSetAssignments WHERE PermissionSet.Name IN (:ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME)) FROM User WHERE Id = :UserInfo.getUserId()];
        accounts = [SELECT Id, Assigned_Territory__c FROM Account WHERE Id IN :recordIds];
        if(ConstantsUtil.ROLE_IN_TERRITORY_DMC.equalsIgnoreCase(currentUser.Code__c)) errorDMC = true;
        for(Account a : accounts){
            if(String.isNotBlank(a.Assigned_Territory__c)){
            	if(currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA || currentUser.Code__c == 'SMAL' || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD){
                    if(a.Assigned_Territory__c != currentUser.AssignedTerritory__c){
                        territoryAllowed = false;
                    }  
                }else if(CurrentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SDI){
                    territoriesToCheck.add(a.Assigned_Territory__c);
                }
            }
        }
        if(territoriesToCheck.size() > 0){
            territories = [SELECT Id, ParentTerritory2.Name FROM Territory2 WHERE Name IN :territoriesToCheck AND ParentTerritory2Id != null];
            for(Territory2 terr : territories){
                if(terr.ParentTerritory2.Name != CurrentUser.AssignedTerritory__c){
                     territoryAllowed = false;
                }
            }
        }
        userAllowed = isAllowed(currentUser);
        
        if(errorDMC) result = Label.ChangeOwner_errorDMC_massive;
        else if(!territoryAllowed) result = Label.ChangeOwner_quickaction_territoryError_msg + ' [' + currentUser.AssignedTerritory__c + ']';
        else if(!userAllowed && !territoryAllowed) result = Label.changeOwner_quickaction_InsufficientPermissions_msg;
		
        system.debug(result);
        return result;
    }
    
    public static Boolean isAllowed(User currentUser){
        Boolean okAllowed = false;
        Boolean okProfile = false;
        Boolean okPermissionSetConfiguration = false;
        if(ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
               okProfile = true;
           }
        if(ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name) ||
                 ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) || 
                 ConstantsUtil.ROLE_IN_TERRITORY_SDI.equalsIgnoreCase(currentUser.Code__c) ||
                 ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                     okPermissionSetConfiguration = true;
                 }
        else if(currentUser.PermissionSetAssignments.size() > 0){
            for(PermissionSetAssignment psa: currentUser.PermissionSetAssignments){
                if((ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && 
                   ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME.equalsIgnoreCase(psa.PermissionSet.Name)) || 
                     psa.PermissionSet.PermissionsModifyAllData){
                       okPermissionSetConfiguration = true;
                   }
            }
        }
        okAllowed = okPermissionSetConfiguration && okProfile;
        return okAllowed;
    }
}