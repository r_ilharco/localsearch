/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2019-06-14
 * Sprint      : Hotfix 2
 * Work item   : AS-46
 * Testclass   : Billing_Test
 * Package     : 
 * Description : Avoid Too Many DML Rows exception
 * Changelog   : Used a batch to handle and reduce DML operation per Apex Invocation.
 */

public without sharing class SblCustomerRatingChangesBatch implements Database.Batchable<SObject> {
    
    public Map<String, BillingResults.CustomerRatingUpdate> resultMap = new Map<String, BillingResults.CustomerRatingUpdate>();

    public SblCustomerRatingChangesBatch(Map<String, BillingResults.CustomerRatingUpdate> resultMap) {
        this.resultMap = resultMap;
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext){

        Set<String> accountIds = new Set<String>();
        accountIds.addAll(resultMap.keySet());

        String query=   'SELECT Id, '+
                        +'Customer_Number__c, '
                        +'Amount_At_Risk__c, '
                        +'ClientRating__c '+
                        +'FROM Account '+
                        +'WHERE Customer_Number__c IN: accountIds' ;
        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext batchableContext, List<Account> scope) {

        List<BillingResults.CustomerRatingUpdate> worsenRatings = new List<BillingResults.CustomerRatingUpdate>();

		for(Account account : scope) {
            BillingResults.CustomerRatingUpdate rating = resultMap.get(account.Customer_Number__c);
            if(account.ClientRating__c == rating.new_client_rating && account.Amount_At_Risk__c == rating.amount_at_risk) {
                continue;       
            }
            if(BillingHelper.CustomerRatingList.indexOf(account.ClientRating__c) < BillingHelper.CustomerRatingList.indexOf(rating.new_client_rating)) {
                worsenRatings.add(rating);
            }

            //US_13, Sprint 1, Wave 1, Account Blocked.
            //Update of Account's Status in "Blocked" depending on the client rating retrieved from SwissBilling ENDPOINT.
            //The Custom Metadata "BlockedAccountClientRatings__mdt" contains all client ratings for which an account should be blocked.
            System.debug('rating.new_client_rating: ' + rating.new_client_rating);
            System.debug('rating.amount_at_risk: ' + rating.amount_at_risk);
            List<AccountStatusConfiguration__mdt> accountStatusConfig = [SELECT Id, NextAccountStatus__c, Reason__c, Complete_label_PB__c FROM AccountStatusConfiguration__mdt WHERE MasterLabel = :rating.new_client_rating];
            if(accountStatusConfig.size() > 0)
            {
                account.ClientRating__c = accountStatusConfig[0].Complete_label_PB__c;
                account.Amount_At_Risk__c = rating.amount_at_risk;
                account.Status__c = accountStatusConfig[0].NextAccountStatus__c;
                //account.Reason__c = accountStatusConfig[0].Reason__c;
            }
                
        }
        update scope;
	}

    public void finish(Database.BatchableContext batchableContext){

    }
}