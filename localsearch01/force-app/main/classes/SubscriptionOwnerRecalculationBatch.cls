/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class SubscriptionOwnerRecalculationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    private List<Id> accountIds;
    private String sSQL = null;
    public SubscriptionOwnerRecalculationBatch(List<Id> accountIds){
        accountIds = new List<Id>(accountIds);
    }
    
    global database.querylocator start(Database.BatchableContext bc){      
        /*sSQL = 'SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__r.AccountId IN :accountIds';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);*/
        return null;
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        Map<Id, SBQQ__Subscription__c> subscriptions = new Map<Id, SBQQ__Subscription__c>(scope);
        List<SBQQ__Subscription__Share> newSubShares = new List<SBQQ__Subscription__Share>();
        //try{
        if(subscriptions.values().size() > 0){
            Set<String> areaCodes = new Set<String>();
       /*     for(SBQQ__Subscription__c s : subscriptions.values()){
                if(s.SBQQ__Contract__r.Account.Area_Code__c != null){
                    areaCodes.add(s.SBQQ__Contract__r.Account.Area_Code__c);
                }
            }
            if(areaCodes.size() > 0){
                List<UserTerritory2Association> territoryAssociations = [SELECT Id, UserId, Territory2.Name from UserTerritory2Association where Territory2.Name IN :areaCodes AND RoleInTerritory2 IN : ConstantsUtil.ROLES_FOR_OWNER_CALCULATION]; 
                if(territoryAssociations.size() > 0){
                    Map<String, List<Id>> areaCodeTerritoryRolesMap = new Map<String, List<Id>>();
                    for(UserTerritory2Association territory : territoryAssociations){
                        if(!areaCodeTerritoryRolesMap.containsKey(territory.Territory2.Name)){
                            areaCodeTerritoryRolesMap.put(territory.Territory2.Name, new List<Id>{territory.UserId});
                        }else{
                            areaCodeTerritoryRolesMap.get(territory.Territory2.Name).add(territory.UserId);
                        }
                    }
                    List<SBQQ__Subscription__Share> oldSubShares = [SELECT Id FROM SBQQ__Subscription__Share WHERE RowCause = :Schema.SBQQ__Quote__Share.RowCause.Owner_Recalculation__c];
                    if(oldSubShares.size() > 0){
                        delete oldSubShares;
                    }
                    List<SBQQ__Subscription__Share> newSubShares = new List<SBQQ__Subscription__Share>();
                    for(SBQQ__Subscription__c sub : subscriptions.values()){
                        if(sub.SBQQ__Contract__r.Account.Area_Code__c != null){
                            if(areaCodeTerritoryRolesMap.containsKey(sub.SBQQ__Contract__r.Account.Area_Code__c)){
                                for(String user : areaCodeTerritoryRolesMap.get(sub.SBQQ__Contract__r.Account.Area_Code__c)){
                                    SBQQ__Subscription__Share newSubShare = new SBQQ__Subscription__Share();
                                    newSubShare.ParentId = sub.id;
                                    newSubShare.UserOrGroupId = user;
                                    newSubShare.AccessLevel = 'Read';
                                    newSubShare.RowCause = Schema.SBQQ__Quote__Share.RowCause.Owner_Recalculation__c;
                                    newSubShares.add(newSubShare);
                                }
                            }
                        }
                    }
                    if(newSubShares.size() > 0){
                        system.debug('newSubShares: ' +  newSubShares);
                        insert newSubShares;
                    }
                }
            }*/
        }
        /*}catch(Exception ex){
            system.debug('stackTrace: ' + ex.getStackTraceString());
        }*/
    }
    global void finish(Database.BatchableContext bc){        
    }
}