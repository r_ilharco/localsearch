public without sharing class ContractsOverviewForAccountController{
	
    @AuraEnabled
    public static List<PicklistValue> retrievePicklistValues(String sObjectApiName, String fieldApiName){
        
        System.debug('Object ' + sObjectApiName + ' field ' + fieldApiName);
        List<PicklistValue> pickVals = new List<PicklistValue>();
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectApiName);
        Sobject objectName = targetType.newSObject();
        Schema.sObjectType sobjectType = objectName.getSObjectType();
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldsMap = sobjectDescribe.fields.getMap();
        
        List<Schema.PicklistEntry> picklistValues = fieldsMap.get(fieldApiName).getDescribe().getPickListValues();
        
        if(fieldApiName.equalsIgnoreCase('SBQQ__SubscriptionType__c')){
            PicklistValue pickval = new PicklistValue();
            pickval.label = '';
            pickval.value = '';
            pickVals.add(pickval);
        }
        
        for(Schema.PicklistEntry currentPicklistEntry : picklistValues){
            PicklistValue pickval = new PicklistValue();
            pickval.label = currentPicklistEntry.getLabel();
            pickval.value = currentPicklistEntry.getValue();
            pickVals.add(pickval);
        }
        return pickvals;
	}
    
    @AuraEnabled
    public static SubscriptionsResponse getSubscriptionByAccount(id accountId, Boolean isInit, String queryParam, String startDate, String endDate, List<String> statuses, String selectedType, String selectedPeriod){
        
        SubscriptionsResponse res = new SubscriptionsResponse();
        Set<Id> productIds = new Set<Id>();
        String language = UserInfo.getLanguage();
        List<SBQQ__Subscription__c> listSubs = new List<SBQQ__Subscription__c>();
        
        String queryToPerform = 'SELECT Id,Name, SBQQ__ProductName__c, SBQQ__AdditionalDiscountAmount__c, Subsctiption_Status__c, SBQQ__RequiredById__c,SBQQ__Contract__r.Status,SBQQ__TerminatedDate__c, '+
                              + 'SBQQ__Product__c , SBQQ__Product__r.Manual_Activation__c, SBQQ__Product__r.Production__c, Place__c , Place_Name__c, SBQQ__SubscriptionType__c, ' +
                              + 'SBQQ__Bundled__c,  SBQQ__EndDate__c,  End_Date__c, In_Termination__c, Termination_Date__c, SBQQ__StartDate__c, Total__c, Package_Total__c, '+
                              + 'Subscription_Term__c, SBQQ__Product__r.Base_term_Renewal_term__c, SBQQ__Product__r.Product_Group__c, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c, '+
            				  + 'SBQQ__Contract__r.SBQQ__Opportunity__c, SBQQ__Contract__r.SBQQ__Opportunity__r.Name, SBQQ__Contract__r.AccountId, SBQQ__Contract__r.Account.Customer_Number__c, '+
            				  + 'SBQQ__Contract__r.SBQQ__Quote__c, SBQQ__Contract__r.SBQQ__Quote__r.Name, SBQQ__Contract__r.Account.Name, SBQQ__Product__r.Period_of_notice__c, '+
            				  + 'SBQQ__QuoteLine__r.DocRowNumber__c,SBQQ__QuoteLine__r.SBQQ__Quote__r.Name, '
                              + 'SBQQ__Product__r.One_time_Fee__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, SBQQ__Quantity__c, SBQQ__Contract__c, SBQQ__Contract__r.ContractNumber FROM SBQQ__Subscription__c ';
        
        String whereCause = 'WHERE SBQQ__Contract__r.AccountId = :accountId ';
        
        String orderByCause = 'ORDER BY SBQQ__RequiredById__c ASC NULLS FIRST ';
        
        if(!isInit){
            if(String.isNotBlank(queryParam)){
                String param = '%'+queryParam+'%';
                whereCause += 'AND (SBQQ__ProductName__c LIKE :param OR SBQQ__Product__r.Product_Group__c LIKE :param OR Place_Name__c LIKE :param  OR Subsctiption_Status__c LIKE :param OR SBQQ__SubscriptionType__c LIKE :param OR SBQQ__Contract__r.ContractNumber LIKE :param OR RequiredBy__r.Place_Name__c LIKE :param) ';
            }
            if(statuses != null && !statuses.isEmpty()) whereCause += 'AND Subsctiption_Status__c IN :statuses ';
            if(String.isNotBlank(selectedType)) whereCause += 'AND SBQQ__SubscriptionType__c = :selectedType ';
            if(String.isNotBlank(selectedPeriod)){
              //  if(!selectedPeriod.equals('LAST_N_MONTHS:6')) whereCause += 'AND SBQQ__StartDate__c = '+ String.escapeSingleQuotes(selectedPeriod) +' AND End_Date__c = ' + String.escapeSingleQuotes(selectedPeriod) + ' ';
              //  else whereCause += 'AND (SBQQ__StartDate__c = '+ String.escapeSingleQuotes(selectedPeriod) +' OR SBQQ__StartDate__c = THIS_MONTH) AND (End_Date__c = ' + String.escapeSingleQuotes(selectedPeriod) + ' OR End_Date__c = THIS_MONTH) ';
              whereCause += 'AND SBQQ__StartDate__c = '+ String.escapeSingleQuotes(selectedPeriod) +' ';
            }
            if(String.isNotBlank(startDate) && String.isBlank(endDate)){
                Date sD = Date.valueOf(startDate);
                whereCause += 'AND SBQQ__StartDate__c = :sD ';
            }
            else if(String.isNotBlank(endDate) && String.isBlank(startDate)){
                Date eD = Date.valueOf(endDate);
                whereCause += 'AND End_Date__c = :eD ';
            }
            else if(String.isNotBlank(startDate) && String.isNotBlank(endDate)){
                Date sD2 = Date.valueOf(startDate);
                Date eD2 = Date.valueOf(endDate);
                whereCause += 'AND SBQQ__StartDate__c >= :sD2 AND End_Date__c <= :eD2 ';
            }
        }
        
        System.debug('Performed SOQL -> ' + queryToPerform + whereCause + orderByCause);
        listSubs = Database.query(queryToPerform + whereCause + orderByCause);
        if(listSubs.size() == 0) res.isSizeZero = true;
        
        Map<Id, List<SBQQ__Subscription__c>> subHierarchy = new Map<Id, List<SBQQ__Subscription__c>>();
        
        for(SBQQ__Subscription__c s : listSubs){
            productIds.add(s.SBQQ__Product__c);
            if(String.isBlank(s.SBQQ__RequiredById__c)) subHierarchy.put(s.Id, new List<SBQQ__Subscription__c>());
            else{
                if(subHierarchy.containsKey(s.SBQQ__RequiredById__c)) subHierarchy.get(s.SBQQ__RequiredById__c).add(s);
                else subHierarchy.put(s.SBQQ__RequiredById__c, new List<SBQQ__Subscription__c>{s});
            }
        }
        
        
        List<SBQQ__Localization__c> localizations = SEL_Localization.getProductNameTranslations(productIds);
        
        Map<Id, Map<String, String>> productLanguageTranslation = new Map<Id, Map<String, String>>();
        
        for(SBQQ__Localization__c loc : localizations){
            
            if(!String.isBlank(loc.SBQQ__Text__c)){
                
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)) productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                else productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
            }
        }
        
        if(isinit){
            Decimal totalRevenueAmount = 0;
            for(SBQQ__Subscription__c sub : listSubs){
                if(ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE.equalsIgnoreCase(sub.Subsctiption_Status__c)) totalRevenueAmount += sub.Total__c;
            }
            res.totalRevenueAmount = totalRevenueAmount;
        }
        
        for(SBQQ__Subscription__c sub : listSubs){
            String productName = sub.SBQQ__ProductName__c;
            
            if(productLanguageTranslation.containsKey(sub.SBQQ__Product__c) && productLanguageTranslation.get(sub.SBQQ__Product__c).containsKey(language)){
                productName = productLanguageTranslation.get(sub.SBQQ__Product__c).get(language);
            }
            
            SubscriptionsWrapper wrap = new SubscriptionsWrapper();
            wrap.id = sub.id;
            wrap.quoteName = sub.SBQQ__QuoteLine__r.SBQQ__Quote__r.Name.removeStart('Q-');
            wrap.rowPosition = sub.SBQQ__QuoteLine__r.DocRowNumber__c != null ? sub.SBQQ__QuoteLine__r.DocRowNumber__c : 1;
            wrap.shortPosition= decimal.valueOf(wrap.quoteName+string.valueOf(wrap.rowPosition));
            if(ConstantsUtil.CONTRACT_STATUS_CANCELLED.equalsIgnoreCase(sub.SBQQ__Contract__r.Status) || ConstantsUtil.CONTRACT_STATUS_TERMINATED.equalsIgnoreCase(sub.SBQQ__Contract__r.Status)){
                if(sub.SBQQ__TerminatedDate__c!= null){
                  wrap.conTermCancelDate = sub.SBQQ__TerminatedDate__c;
                }else{
                   wrap.conTermCancelDate = sub.Termination_Date__c;
                }
            }
            wrap.name = sub.Name;
            wrap.placeId = sub.Place__c;
            wrap.placeName = sub.Place_Name__c;
            wrap.productId = sub.SBQQ__Product__c;
            wrap.productName = productName;
            wrap.status = sub.Subsctiption_Status__c;
            wrap.startDate = sub.SBQQ__StartDate__c;
            wrap.endDate = sub.End_Date__c;
            wrap.inTermination = boolean.valueOf(sub.In_Termination__c);
            wrap.terminationDate = sub.Termination_Date__c;
            wrap.total = sub.Total__c;
            wrap.subType = sub.SBQQ__SubscriptionType__c;
            wrap.SubscriptionTerm = sub.Subscription_Term__c;
            wrap.baseTermPrice = sub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            if(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
                Decimal roundedDiscount = (Decimal)Math.round(sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                wrap.discount = roundedDiscount;
            }
            if(String.isNotBlank(sub.SBQQ__Product__r.Base_term_Renewal_term__c)) wrap.BasetermRenewalTerm = Decimal.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c);
            wrap.showOptions = sub.SBQQ__Bundled__c || sub.Subsctiption_Status__c != ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
            wrap.contractId = sub.SBQQ__Contract__c;
            wrap.contractNumber = sub.SBQQ__Contract__r.ContractNumber;
            wrap.quantity = sub.SBQQ__Quantity__c;
            wrap.productGroup = sub.SBQQ__Product__r.Product_Group__c;
            wrap.accountId = sub.SBQQ__Contract__r.AccountId;
            wrap.customerNumber = sub.SBQQ__Contract__r.Account.Customer_Number__c;
            wrap.accountName = sub.SBQQ__Contract__r.Account.Name;
            wrap.opportunityId = sub.SBQQ__Contract__r.SBQQ__Opportunity__c;
            wrap.opportunityName = sub.SBQQ__Contract__r.SBQQ__Opportunity__r.Name;
            wrap.quoteId = sub.SBQQ__Contract__r.SBQQ__Quote__c;
            wrap.quoteNumber = sub.SBQQ__Contract__r.SBQQ__Quote__r.Name;
            if(String.isNotBlank(sub.SBQQ__Product__r.Period_of_notice__c)) wrap.periodsOfNotice = sub.SBQQ__Product__r.Period_of_notice__c;
            
			Decimal oneTimeFeeTotal = 0;
            Decimal packageTotal = sub.Package_Total__c != null ? sub.Package_Total__c : 0;
            if(!Test.isRunningTest() && String.isBlank(sub.SBQQ__RequiredById__c)){
                List<SBQQ__Subscription__c> childrenSubs = subHierarchy?.get(sub.Id);
                if(!childrenSubs?.isEmpty()){
                    for(SBQQ__Subscription__c childSub : childrenSubs){
                        if(childSub.SBQQ__Product__r.One_time_Fee__c) oneTimeFeeTotal += childSub.Total__c;
                    }
                }
                Decimal period = (Decimal)(Integer.valueOf(sub.Subscription_Term__c))/(Integer.valueOf(sub.SBQQ__Product__r.Base_term_Renewal_term__c));
				Decimal totalWithoutOTF = Integer.valueOf(sub.Subscription_Term__c) <= 12 ? packageTotal - oneTimeFeeTotal : (Decimal)(packageTotal - oneTimeFeeTotal)/period;
                wrap.annualTotal = totalWithoutOTF /*+ oneTimeFeeTotal*/;
            }
            
            
            
            //the total will display the total master product value while bundle total will display the overall sum of master product and its options – not on annual base but overall
            if(sub.SBQQ__RequiredById__c == null) {
                wrap.bundleTotal = wrap.total;
               	res.subscriptions.add(wrap);
            } 
            else{
               for(Subscriptionswrapper w : res.subscriptions){
                    if(w.id == sub.SBQQ__RequiredById__c){
                        if(sub.Total__c == null) w.bundleTotal += 0 ; 
                        else w.bundleTotal += sub.Total__c;
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }
            
            
        }
        res.valid = true;
        return res;
    }
    
    public class SubscriptionsWrapper{
        @AuraEnabled public Id id {get; set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String quoteName {get; set;} 
        @AuraEnabled public decimal rowPosition {get; set;}
        @AuraEnabled public decimal shortPosition  {get; set;} 
        @AuraEnabled public Date conTermCancelDate {get; set;}
        @AuraEnabled public Id placeId {get; set;}
        @AuraEnabled public String placeName {get; set;}
        @AuraEnabled public Id productId {get; set;}
        @AuraEnabled public String productName {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String subType {get; set;}
        @AuraEnabled public Boolean showOptions {get; set;}
        @AuraEnabled public Date startDate {get; set;}
        @AuraEnabled public Date endDate {get; set;}
        @AuraEnabled public boolean inTermination {get; set;}
        @AuraEnabled public Date terminationDate {get; set;}
        @AuraEnabled public List<SubscriptionsWrapper> childrens {get; set;}
        @AuraEnabled public Decimal discount {get; set;}
        @AuraEnabled public Decimal total {get; set;}
        @AuraEnabled public String SubscriptionTerm {get; set;}
        @AuraEnabled public Decimal BasetermRenewalTerm {get; set;}
        @AuraEnabled public Decimal baseTermPrice {get; set;}
        @AuraEnabled public Id contractId {get; set;}
        @AuraEnabled public String contractNumber {get; set;}
        @AuraEnabled public Decimal quantity {get; set;}
        @AuraEnabled public String productGroup {get; set;}
        @AuraEnabled public Id accountId {get; set;}
        @AuraEnabled public String customerNumber {get; set;}
        @AuraEnabled public String accountName {get; set;}
        @AuraEnabled public Id opportunityId {get; set;}
        @AuraEnabled public String opportunityName {get; set;}
        @AuraEnabled public Id quoteId {get; set;}
        @AuraEnabled public String quoteNumber {get; set;}
        @AuraEnabled public String periodsOfNotice {get; set;}
        @AuraEnabled public Decimal bundleTotal {get; set;}
        @AuraEnabled public Decimal annualTotal {get; set;}
        
        public SubscriptionsWrapper(){
            childrens = new List<SubscriptionsWrapper>();
        }
    }
    
    public class SubscriptionsResponse{
        @AuraEnabled public Boolean valid {get; set;}
        @AuraEnabled public Boolean isSizeZero {get; set;}
        @AuraEnabled public Decimal totalRevenueAmount {get;set;}
        @AuraEnabled public List<SubscriptionsWrapper> subscriptions {get; set;}
        public SubscriptionsResponse(){
            subscriptions = new List<SubscriptionsWrapper>();
            this.isSizeZero = false;
        }
    }
    
    public class PicklistValue{
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
    }
    
    
    @AuraEnabled
    public static OrderItemsResponse getOrderItemsByAccount(id accountId, Boolean isInit, String queryParam, String startDate, String endDate,String selectedPeriod,List<String> statuses){

        OrderItemsResponse res = new OrderItemsResponse();
        Set<Id> productIds = new Set<Id>();
        String language = UserInfo.getLanguage();
        List<OrderItem> listOrderItems= new List<OrderItem>();


        String queryToPerform = 'SELECT Id,Order.OrderNumber,Order.Total_Net_Amount__c,Order.EffectiveDate, OrderId,Order.AccountId,Order.Account.Name,Order.Account.Customer_Number__c,OrderItemNumber,Place__c, Place__r.Name,Product2Id,Product2.Name, '+
                              + 'Total__c , Package_Total__c, UnitPrice,Order.OpportunityId, Order.Opportunity.Name,Order.SBQQ__Quote__r.Name,Order.SBQQ__Quote__c,Order.Status, ' +
                              + 'ServiceDate, EndDate,End_Date__c,Quantity,Subscription_Term__c,Product2.Base_term_Renewal_term__c,SBQQ__RequiredBy__c,Product2.Product_Group__c, '+
            				  + 'SBQQ__Status__c,PricebookEntry.Product2.Name,SBQQ__QuoteLine__r.Base_term_Renewal_term__c,SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c,'+
            				  + 'SBQQ__QuoteLine__r.Subscription_Term__c,SBQQ__QuoteLine__r.SBQQ__ListPrice__c,SBQQ__Subscription__r.SBQQ__ListPrice__c,SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c,'+
            				  + 'SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c,SBQQ__Contract__c,SBQQ__Contract__r.ContractNumber,SBQQ__Subscription__r.Subscription_Term__c,Product2.One_time_Fee__c '+
                              + 'FROM OrderItem ';
        
        String whereCause = 'WHERE Order.AccountId = :accountId '
            				+'And Order.Status IN :statuses ';
        
        String orderByCause = ' ORDER BY SBQQ__RequiredBy__c ASC NULLS FIRST';
        
        if(!isInit){
            if(String.isNotBlank(queryParam)){
                String param = '%'+queryParam+'%';
                whereCause += 'AND (Product2.Name LIKE :param OR Product2.Product_Group__c LIKE :param OR Place__r.Name LIKE :param  OR SBQQ__Status__c LIKE :param OR SBQQ__SubscriptionType__c LIKE :param OR Order.OrderNumber LIKE :param OR SBQQ__RequiredBy__r.Place__r.Name LIKE :param) ';
            }
            if(String.isNotBlank(selectedPeriod)){
                //if(!selectedPeriod.equals('LAST_N_MONTHS:6')) whereCause += 'AND ServiceDate = '+ String.escapeSingleQuotes(selectedPeriod) +' AND EndDate = ' + String.escapeSingleQuotes(selectedPeriod) + ' ';
                //else whereCause += 'AND (ServiceDate = '+ String.escapeSingleQuotes(selectedPeriod) +' OR ServiceDate = THIS_MONTH) AND (EndDate = ' + String.escapeSingleQuotes(selectedPeriod) + ' OR EndDate = THIS_MONTH) ';
                 whereCause += 'AND ServiceDate = '+ String.escapeSingleQuotes(selectedPeriod) +' ';
            }
            else{
              	if(String.isNotBlank(startDate) && String.isBlank(endDate)){
                	Date sD = Date.valueOf(startDate);
                	whereCause += 'AND ServiceDate = :sD ';
            	}
            	else if(String.isNotBlank(endDate) && String.isBlank(startDate)){
                	Date eD = Date.valueOf(endDate);
                	whereCause += 'AND EndDate = :eD ';
            	}
            	else if(String.isNotBlank(startDate) && String.isNotBlank(endDate)){
                	Date sD2 = Date.valueOf(startDate);
                	Date eD2 = Date.valueOf(endDate);
                	whereCause += 'AND ServiceDate >= :sD2 AND EndDate <= :eD2 ';
            	}  
            } 
        }
        
        System.debug('Performed SOQL -> ' + queryToPerform + whereCause + orderByCause);
        listOrderItems = Database.query(queryToPerform + whereCause + orderByCause);
        system.debug('listOrderItems ::: '+listOrderItems.size());
        if(listOrderItems.size() == 0) res.isSizeZero = true;
        
        Map<Id, List<OrderItem>> subHierarchy = new Map<Id, List<OrderItem>>();
        
          
        
        for(OrderItem s : listOrderItems){
            productIds.add(s.Product2Id);
            if(String.isBlank(s.SBQQ__RequiredBy__c)) subHierarchy.put(s.Id, new List<OrderItem>());
            else{
                if(subHierarchy.containsKey(s.SBQQ__RequiredBy__c)) subHierarchy.get(s.SBQQ__RequiredBy__c).add(s);
                else subHierarchy.put(s.SBQQ__RequiredBy__c, new List<OrderItem>{s});
            }
        } 
        
        List<SBQQ__Localization__c> localizations = SEL_Localization.getProductNameTranslations(productIds);
        
        Map<Id, Map<String, String>> productLanguageTranslation = new Map<Id, Map<String, String>>();
        
        for(SBQQ__Localization__c loc : localizations){
            if(!String.isBlank(loc.SBQQ__Text__c)){
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                    productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                }else{
                    productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                }
            }
        }
        
        /*if(isinit){
            Decimal totalOrderAmount = 0;
            for(OrderItem orderItem : listOrderItems){
              if(orderItem.Total__c != null) totalOrderAmount += orderItem.Total__c;
            }
            res.totalOrderAmount = totalOrderAmount;
        }*/
        
        for(OrderItem orderItem : listOrderItems){
            String productName = orderItem.PricebookEntry.Product2.Name;
           // system.debug('orderItem .OrderItemNumber ::: '+ orderItem.OrderItemNumber);
            if(productLanguageTranslation.containsKey(orderItem.Product2Id) && productLanguageTranslation.get(orderItem.Product2Id).containsKey(language)){
                productName = productLanguageTranslation.get(orderItem.Product2Id).get(language);
            }

            OrderItemWrapper wrap = new OrderItemWrapper();
            wrap.id = orderItem.id;
            wrap.name = orderItem.OrderItemNumber;
            wrap.orderNumber = orderItem.Order.OrderNumber;
            wrap.orderId = orderItem.OrderId;
            wrap.placeId = orderItem.Place__c;
            wrap.placeName = orderItem.Place__r.Name;
            wrap.productId = orderItem.Product2Id;
            wrap.productName = productName;
            wrap.status = orderItem.SBQQ__Status__c;
            wrap.orderStatus = orderItem.Order.Status;
            wrap.startDate = orderItem.ServiceDate;
            wrap.endDate = orderItem.EndDate;
            wrap.unitPrice = orderItem.UnitPrice;
 			wrap.SubscriptionTerm = orderItem.Subscription_Term__c;
            
			if(orderItem.Total__c != null) wrap.total = orderItem.Total__c;  
            else wrap.total = 0; 
           
            if(orderItem.SBQQ__QuoteLine__r.Base_term_Renewal_term__c != null) wrap.baseTerm = Integer.valueOf(orderItem.SBQQ__QuoteLine__r.Base_term_Renewal_term__c);
            else if(orderItem.SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c != null) wrap.baseTerm = Integer.valueOf(orderItem.SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c);
            if(orderItem.SBQQ__QuoteLine__r.Subscription_Term__c != null) wrap.subTerm = Integer.valueOf(orderItem.SBQQ__QuoteLine__r.Subscription_Term__c);
            else if(orderItem.SBQQ__Subscription__r.Subscription_Term__c != null) wrap.subTerm = Integer.valueOf(orderItem.SBQQ__Subscription__r.Subscription_Term__c);
            if(orderItem.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null) wrap.baseTermPrice = orderItem.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            else if(orderItem.SBQQ__Subscription__r.SBQQ__ListPrice__c != null) wrap.baseTermPrice = orderItem.SBQQ__Subscription__r.SBQQ__ListPrice__c;
            if(orderItem.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
                Decimal roundedDiscount = (Decimal)Math.round(orderItem.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                wrap.discount = roundedDiscount;
            }
            else if(orderItem.SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
               	Decimal roundedDiscount = (Decimal)Math.round(orderItem.SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                wrap.discount = roundedDiscount; 
            }
            
            if(String.isNotBlank(orderItem.Product2.Base_term_Renewal_term__c)) wrap.BasetermRenewalTerm = Decimal.valueOf(orderItem.Product2.Base_term_Renewal_term__c);
           	wrap.contractId = orderItem.SBQQ__Contract__c;
            wrap.contractNumber = orderItem.SBQQ__Contract__r.ContractNumber;
            wrap.quantity = orderItem.Quantity;
            wrap.productGroup = orderItem.Product2.Product_Group__c;
            wrap.accountId = orderItem.Order.AccountId;
            wrap.customerNumber = orderItem.Order.Account.Customer_Number__c;
            wrap.accountName = orderItem.Order.Account.Name;
            wrap.opportunityId = orderItem.Order.OpportunityId; 
            wrap.opportunityName = orderItem.Order.Opportunity.Name;
            wrap.quoteId = orderItem.Order.SBQQ__Quote__c;
            wrap.quoteNumber = orderItem.Order.SBQQ__Quote__r.Name;
            
            if(orderItem.SBQQ__RequiredBy__c == null){
                wrap.bundleTotal = wrap.total;
               	res.orderItems.add(wrap);
            } 
            else{
                for(OrderItemWrapper w : res.orderItems){
                    if(w.id == orderItem.SBQQ__RequiredBy__c){
                        if(orderItem.Total__c == null) w.bundleTotal += 0 ; 
                        else w.bundleTotal += orderItem.Total__c;
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }
            
            
          /*  Decimal oneTimeFeeTotal = 0;
            if(!Test.isRunningTest() && String.isBlank(orderItem.SBQQ__RequiredBy__c)){
                List<OrderItem> childrenOrderItems = subHierarchy?.get(orderItem.Id);
                if(!childrenOrderItems?.isEmpty()){
                    for(OrderItem childOI : childrenOrderItems){
                        if(childOI.Product2.One_time_Fee__c) oneTimeFeeTotal += childOI.Total__c;
                    }
                }
                if(orderItem.Subscription_Term__c != null && orderItem.Product2.Base_term_Renewal_term__c != null && orderItem.Package_Total__c!= null){
                    
                 //   wrap.bundleTotal = wrap.baseTermPrice;
               // }else{
                    Decimal period = (Decimal)(Integer.valueOf(orderItem.Subscription_Term__c))/(Integer.valueOf(orderItem.Product2.Base_term_Renewal_term__c));
                    //if(orderItem.Package_Total__c!= null){
                       Decimal totalWithoutOTF = Integer.valueOf(orderItem.Subscription_Term__c) <= 12 ? orderItem.Package_Total__c - oneTimeFeeTotal : (Decimal)(orderItem.Package_Total__c - oneTimeFeeTotal)/period;
                		wrap.bundleTotal = totalWithoutOTF + oneTimeFeeTotal; 
                    //} 
                }
	            
            }
            
            if(orderItem.SBQQ__RequiredBy__c == null) res.orderItems.add(wrap);
            else{
                for(OrderItemWrapper w : res.orderItems){
                    if(w.id == orderItem.SBQQ__RequiredBy__c){
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }*/
            
            
            
            
            
            
            
            
            
        }
        res.valid = true;        
        system.debug('res ::: '+res.orderItems.size());
        return res;
        
    }    
    

    public class OrderItemWrapper{
        @AuraEnabled public Id id {get; set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public Id placeId {get; set;}
        @AuraEnabled public String placeName {get; set;}
        @AuraEnabled public Id productId {get; set;}
        @AuraEnabled public String productName {get; set;}
        @AuraEnabled public Boolean showOptions {get; set;}
        @AuraEnabled public Date startDate {get; set;}
        @AuraEnabled public Date endDate {get; set;}
        @AuraEnabled public List<OrderItemWrapper> childrens {get; set;}
        @AuraEnabled public Decimal total {get; set;}
        @AuraEnabled public String SubscriptionTerm {get; set;}
        @AuraEnabled public Decimal BasetermRenewalTerm {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public Decimal unitPrice {get; set;}
        @AuraEnabled public Id contractId {get; set;}
        @AuraEnabled public String contractNumber {get; set;} 
        @AuraEnabled public Decimal quantity {get; set;}
        @AuraEnabled public String productGroup {get; set;}
        @AuraEnabled public Id accountId {get; set;}
        @AuraEnabled public String customerNumber {get; set;}
        @AuraEnabled public String accountName {get; set;}
        @AuraEnabled public Id opportunityId {get; set;}
        @AuraEnabled public String opportunityName {get; set;}
        @AuraEnabled public Id quoteId {get; set;}
        @AuraEnabled public String quoteNumber {get; set;}
        @AuraEnabled public Decimal bundleTotal {get; set;}
		@AuraEnabled public String orderNumber {get; set;}
        @AuraEnabled public Id orderId {get; set;}
		@AuraEnabled public string orderStatus {get; set;} 
        @auraEnabled public Integer baseTerm {get; set;}	
        @auraEnabled public Integer subTerm {get; set;}	
        @auraEnabled public Decimal baseTermPrice {get; set;}	
        @auraEnabled public Decimal discount {get; set;}
        
        
        public OrderItemWrapper(){
            childrens = new List<OrderItemWrapper>();
        }
    }

    public class OrderItemsResponse{
        @AuraEnabled public Boolean valid {get; set;}
        @AuraEnabled public Boolean isSizeZero {get; set;}
        @AuraEnabled public Decimal totalOrderAmount {get;set;}
        @AuraEnabled public List<OrderItemWrapper> orderItems {get; set;}
        public OrderItemsResponse(){
            orderItems = new List<OrderItemWrapper>();
            this.isSizeZero = false;
        }
    }
}