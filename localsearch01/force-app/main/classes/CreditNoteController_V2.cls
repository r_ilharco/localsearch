/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-5972 Credit Notes - Reimbursed Invoices 'Invoice Sending Error'
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2021-03-16
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public class CreditNoteController_V2 {    
    @AuraEnabled        
    public static Map<String, object> getDefaultCreditNoteValues(string invoiceItemRecordId) {
        Map<String, object> result = new Map<String, Object>();
        try{
            system.debug('invoiceItemRecordId: ' + invoiceItemRecordId);
            List<Invoice_Item__c> invoiceItems = [SELECT Id, Invoice_Order__r.Invoice__c, Subscription__c, Subscription__r.SBQQ__Contract__c, Invoice_Order__c, Accrual_From__c, Amount__c, Accrual_To__c
                                                  FROM Invoice_Item__c
                                                  WHERE Id = :invoiceItemRecordId
                                                  LIMIT 1];
            List<Invoice_Order__c> invoiceOrderList = new List<Invoice_Order__c>();
            String invoiceId;
            String subscriptionId; 
            String invoiceOrderId; 
            if(invoiceItems.size() > 0){
                invoiceId = invoiceItems[0].Invoice_Order__r.Invoice__c;
                subscriptionId = invoiceItems[0].Subscription__c; 
                invoiceOrderId = invoiceItems[0].Invoice_Order__c; 
                invoiceOrderList = [SELECT Invoice__c, Invoice__r.Invoice_Code__c, Invoice__r.Process_Mode__c, Invoice__r.Status__c,
                                    Invoice__r.Amount__c, Invoice__r.Customer__c, Invoice__r.Billing_Profile__c, Invoice__r.Billing_Profile__r.Channels__c,
                                    Contract__c, Contract__r.TerminateDate__c, Invoice__r.Tax_Mode__c
                                    FROM Invoice_Order__c 
                                    WHERE Id =: invoiceOrderId LIMIT 1];         
            }
            if(invoiceOrderList.size() > 0){
                
                String invoiceCodeValue = 'FAK';                         
                if(invoiceOrderList[0].Invoice__r.Invoice_Code__c == invoiceCodeValue){
                    String processModeValue = 'ModeSwissList';
                    //if(invoiceOrderList[0].Invoice__r.Process_Mode__c != processModeValue){
                        String statusValueDraft = 'Draft';
                        String statusValueInCollection = 'In Collection';
                        String statusValueCancelled = 'Cancelled';
                        if(invoiceOrderList[0].Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_DRAFT 
                        && invoiceOrderList[0].Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_IN_COLLECTION
                        && invoiceOrderList[0].Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_CANCELLED
                        && invoiceOrderList[0].Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_SENDING_ERROR
                        && invoiceOrderList[0].Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_DELIVERY_FAILED){
                            if(invoiceOrderList[0].Invoice__r.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                                result.put('Was_Exempt__c', ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_YES);
                            }else{
                                result.put('Was_Exempt__c', ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_NO);
                            }
                            result.put('Manual__c', true);
                            system.debug('Manual__c: ' + true);
                            
                            result.put('Status__c', 'Ready for billing');
                            system.debug('Status__c: ' + 'Ready for billing');
                            
                            result.put('Contract__c', invoiceOrderList[0].Contract__c);
                            system.debug('Contract__c: ' + invoiceOrderList[0].Contract__c);
                            
                            result.put('Account__c', invoiceOrderList[0].Invoice__r.Customer__c);
                            system.debug('Account__c: ' + invoiceOrderList[0].Invoice__r.Customer__c);
                            
                            result.put('Execution_Date__c', Date.today());//invoiceOrderList[0].Contract__r.TerminateDate__c);
                            system.debug('Execution_Date__c: ' + Date.today());
                            ExistingCreditNoteInfo existingCreditNotesForInvoice = getExistingCreditNoteInfo(invoiceId);
                            ExistingCreditNoteInfo existingCreditNotesForInvoiceItem = getExistingCreditNoteInfoForInvoiceItem(subscriptionId, invoiceId);
                            Integer totalNumber = 0;
                            Decimal totalAmount = 0;
                            if(existingCreditNotesForInvoice != null && existingCreditNotesForInvoice.totalNumber > 0){
                                totalNumber = existingCreditNotesForInvoice.totalNumber;
                                totalAmount = existingCreditNotesForInvoice.totalAmount;                                
                            }
                            Decimal maxPossibleAmount = invoiceOrderList[0].Invoice__r.Amount__c - totalAmount;
                            
                            Decimal totalAmountOnSub = 0;
                            if(existingCreditNotesForInvoiceItem != null && existingCreditNotesForInvoiceItem.totalNumber > 0){
                                totalAmountOnSub =  existingCreditNotesForInvoiceItem.totalAmount;  
                                system.debug('totalAmountOnSub: ' + existingCreditNotesForInvoiceItem.totalAmount);
                            }
                            Decimal possibleAmount = invoiceItems[0].Amount__c - totalAmountOnSub;
                            
                            if(possibleAmount < maxPossibleAmount){
                                result.put('Max_Possible_Amount', possibleAmount);    
                                system.debug('possibleAmount: ' + possibleAmount);
                                result.put('Value__c', possibleAmount);    
                            }else{
                                result.put('Max_Possible_Amount', maxPossibleAmount);    
                                system.debug('Max_Possible_Amount: ' + maxPossibleAmount);
                                result.put('Value__c', maxPossibleAmount);    
                            }
                            /*result.put('Max_Possible_Amount', 1000000);    
                                system.debug('possibleAmount: ' + 1000000);
                                result.put('Value__c', 1000000);*/  
                            
                            result.put('Reimbursed_Invoice__c', invoiceOrderList[0].Invoice__c);
                            system.debug('Reimbursed_Invoice__c: ' + invoiceOrderList[0].Invoice__c);
                            
                            system.debug('invoice : ' + invoiceOrderList[0].Invoice__r);
                            system.debug('bp: ' + invoiceOrderList[0].Invoice__r.Billing_Profile__r);
                            
                            Billing_Profile__c billingProfile = invoiceOrderList[0].Invoice__r.Billing_Profile__r;                                 
                            if(billingProfile != null){
                                result.put('Billing_Profile__c', invoiceOrderList[0].Invoice__r.Billing_Profile__c);
                                system.debug('billingProfile: ' + invoiceOrderList[0].Invoice__r.Billing_Profile__c);
                                
                                system.debug('all billing channel: ' + billingProfile.Channels__c);
                                
                                if(billingProfile.Channels__c != null){
                                    string channel = billingProfile.Channels__c.substringBefore(';');
                                    result.put('Billing_Channel__c', channel);                                   
                                    system.debug('1st billing channel: ' + channel);
                                }
                                else{
                                    result.put('Billing_Channel__c', '');
                                }
                            }
                            
                            List<SBQQ__Subscription__c> subscriptionList = [SELECT Id, Name, SBQQ__ContractNumber__c, SBQQ__ProductName__c, SBQQ__EndDate__c, 
                                                                            Next_Invoice_Date__c, SBQQ__Contract__r.SBQQ__Evergreen__c, SBQQ__StartDate__c, 
                                                                            SBQQ__Product__r.Deferred_Revenue__c, SBQQ__Product__r.One_time_Fee__c
                                                                            FROM SBQQ__Subscription__c 
                                                                            WHERE Id = :subscriptionId
                                                                            LIMIT 1];                            
                            if(subscriptionList.size() > 0){
                                String title = subscriptionList[0].SBQQ__ContractNumber__c + ' ' + subscriptionList[0].SBQQ__ProductName__c;                                
                                if(totalNumber > 0){
                                    title = title + ' N' + (totalNumber+1);
                                }
                                
                                result.put('Name', title);
                            	system.debug('Name: ' + title);
                                
                                result.put('Subscription__c', subscriptionList[0].Id);
                                system.debug('Subscription__c: ' + subscriptionList[0].Id);   
                                system.debug('Period_To__c: ' + subscriptionList[0].Next_Invoice_Date__c);
                                If (subscriptionList[0].SBQQ__Product__r.One_time_Fee__c || !subscriptionList[0].SBQQ__Product__r.Deferred_Revenue__c || subscriptionList[0].Next_Invoice_Date__c == null ){
                                    result.put('Period_To__c', invoiceItems[0].Accrual_To__c);
                                	system.debug('Period_To__c: ' + invoiceItems[0].Accrual_To__c);
                                }else{
                                    result.put('Period_To__c', subscriptionList[0].Next_Invoice_Date__c.addDays(-1));
                                    system.debug('Period_To__c: ' + subscriptionList[0].Next_Invoice_Date__c.addDays(-1));
                                }
                                
                                
                                result.put('Period_From__c', invoiceItems[0].Accrual_From__c);
                                system.debug('Period_From__c: ' + invoiceItems[0].Accrual_From__c);
                                
                                System.debug('curr: ' + UserInfo.getDefaultCurrency());
                                result.put('Currency', UserInfo.getDefaultCurrency());
                            }                      
                        }
                        else{
                            result.put('Error_Message', Label.Invoice_Status_Error.replace('[STATUS]',invoiceOrderList[0].Invoice__r.Status__c));                     
                            return result;
                        }
                    /*}
                    else{
                        result.put('Error_Message', ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST);
                        system.debug('Error_Message: ' + ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST);                    
                        return result;
                    }   */            
                }
                else{
                    result.put('Error_Message', ConstantsUtil.INVOICE_CODE_FAK);
                    system.debug('Error_Message: ' + ConstantsUtil.INVOICE_CODE_FAK);              
                    return result;
                }                
            }
            
            Credit_Note__c c = new Credit_Note__c();
            c.Manual__c = true;
            
            // ldimartino, SF-398, START
            List<Invoice__c> invoiceList = [SELECT Tax_Mode__c
                                                       FROM Invoice__c 
                                                       WHERE Id =: invoiceId LIMIT 1];
            if(invoiceList.size() > 0){
                if(invoiceList[0].Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                    c.Was_Exempt__c = ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_YES;
                }else{
                    c.Was_Exempt__c = ConstantsUtil.CREDIT_NOTE_WAS_EXEMPT_NO;
                }
            }
            system.debug('was_Exempt: ' + c.Was_Exempt__c);
            // ldimartino, SF-398, END
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.CREDIT_NOTE_BP_NAME, ConstantsUtil.CREDIT_NOTE_BP_NAME, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage() + ' --- ' + e.getLineNumber(), null, invoiceItemRecordId+'');
            result.put('Error_Message', e.getMessage() + e.getStackTraceString() + e.getLineNumber());
        }
        system.debug('result: ' + result);
        return result;
    }
    
    @AuraEnabled 
    public static Boolean isAllowedCheck(string invoiceItemRecordId){
        Boolean isOk = false;
        List<Invoice_Item__c> invoiceItems = [SELECT Id, Invoice_Order__r.Invoice__c, Invoice_Order__r.Invoice__r.Amount__c, Subscription__c, Subscription__r.SBQQ__Contract__c, Invoice_Order__c, Accrual_From__c, Amount__c, Accrual_To__c
                                                  FROM Invoice_Item__c
                                                  WHERE Id = :invoiceItemRecordId
                                                  LIMIT 1]; 
        Id invoiceId = invoiceItems[0].Invoice_Order__r.Invoice__c;
        String subscriptionId = invoiceItems[0].Subscription__c;
        ExistingCreditNoteInfo existingCreditNotesForInvoice = getExistingCreditNoteInfo(invoiceId);
        ExistingCreditNoteInfo existingCreditNotesForInvoiceItem = getExistingCreditNoteInfoForInvoiceItem(subscriptionId, invoiceId);
        Integer totalNumber = 0;
        Decimal totalAmount = 0;
        Decimal maxValue = 0;
        if(existingCreditNotesForInvoice != null && existingCreditNotesForInvoice.totalNumber > 0){
            totalNumber = existingCreditNotesForInvoice.totalNumber;
            totalAmount = existingCreditNotesForInvoice.totalAmount;                                
        }
        Decimal maxPossibleAmount = invoiceItems[0].Invoice_Order__r.Invoice__r.Amount__c - totalAmount;
        Decimal totalAmountOnSub = 0;
        if(existingCreditNotesForInvoiceItem != null && existingCreditNotesForInvoiceItem.totalNumber > 0){
            totalAmountOnSub =  existingCreditNotesForInvoiceItem.totalAmount;  
            system.debug('totalAmountOnSub: ' + existingCreditNotesForInvoiceItem.totalAmount);
        }
        Decimal possibleAmount = invoiceItems[0].Amount__c - totalAmountOnSub;
        if(possibleAmount < maxPossibleAmount){
            system.debug('possibleAmount: ' + possibleAmount);
            maxValue = possibleAmount;   
        }else{
            system.debug('maxPossibleAmount: ' + maxPossibleAmount);
            maxValue = maxPossibleAmount; 
        }
        if(maxValue > 0){
            isOk = true;
        }
        return isOk;
    }
    
    public static ExistingCreditNoteInfo getExistingCreditNoteInfo(Id invoiceId){
        ExistingCreditNoteInfo creditNotesInfo;
        try{            
            creditNotesInfo = new ExistingCreditNoteInfo();
            List<AggregateResult> creditNoteList = [SELECT sum(value__c) TotalAmount, count(value__c) TotalCreditNotes, Reimbursed_Invoice__c 
                                                   FROM Credit_Note__c 
                                                   GROUP BY Reimbursed_Invoice__c 
                                                   HAVING Reimbursed_Invoice__c =: invoiceId LIMIT 1];
            system.debug('creditNoteListForInvoice: ' + creditNoteList);
            if(creditNoteList.size() > 0){
                creditNotesInfo.totalNumber = (Integer)creditNoteList[0].get('TotalCreditNotes');
                creditNotesInfo.totalAmount = (Decimal)creditNoteList[0].get('TotalAmount');
            }
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.CREDIT_NOTE_BP_NAME, ConstantsUtil.CREDIT_NOTE_BP_NAME, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, invoiceId+'');
        }
        
        return creditNotesInfo;
    }
    
    public static ExistingCreditNoteInfo getExistingCreditNoteInfoForInvoiceItem(String subscriptionId, Id invoiceId){
        ExistingCreditNoteInfo creditNotesInfo;
        try{            
            creditNotesInfo = new ExistingCreditNoteInfo();
            List<AggregateResult> creditNoteList = [SELECT sum(value__c) TotalAmount, count(value__c) TotalCreditNotes, Subscription__c 
                                                   FROM Credit_Note__c 
                                                   GROUP BY Subscription__c, Reimbursed_Invoice__c 
                                                   HAVING Subscription__c =: subscriptionId AND Reimbursed_Invoice__c=: invoiceId LIMIT 1];       
            system.debug('creditNoteListForSub: ' + creditNoteList);
            if(creditNoteList.size() > 0){
                creditNotesInfo.totalNumber = (Integer)creditNoteList[0].get('TotalCreditNotes');
                creditNotesInfo.totalAmount = (Decimal)creditNoteList[0].get('TotalAmount');
            }
            
        }catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.CREDIT_NOTE_BP_NAME, ConstantsUtil.CREDIT_NOTE_BP_NAME, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, subscriptionId+'');
        }
        return creditNotesInfo;
    }
    
    public class ExistingCreditNoteInfo{
        Integer totalNumber{get; set;}
        Decimal totalAmount{get; set;}
        
        public ExistingCreditNoteInfo(){
            this.totalNumber = 0;
            this.totalAmount = 0;
        }
    }

}
