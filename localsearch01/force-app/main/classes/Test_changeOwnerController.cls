@isTest
public class Test_changeOwnerController {
    
    @testSetup
    static void setup() {
        Test_DataFactory.insertFutureUsers();
        List<Account> accs = Test_DataFactory.generateAccounts('Test', 1, false);
        insert accs;
        Territory2 t = [SELECT Id, Name FROM Territory2 WHERE (NOT Name LIKE '%Region%') LIMIT 1];
        Account acc = [SELECT Id, Assigned_Territory__c FROM Account LIMIT 1];
        AccountTriggerHandler.disableTrigger = true;
        acc.Assigned_Territory__c = t.Name;
        update acc;
    }
    
    @isTest 
    static void testchangeOwnerController(){
        
        User u = [SELECT Id, Code__c FROM User WHERE Username = 'sdrtester@test.com']; 
        system.debug('TESTCLASS, User Code:' + u.Code__c);
        UserTriggerHandler.disableTrigger = true;
        u.Code__c = 'SRD';
        update u;
        UserTriggerHandler.disableTrigger = false;
        User us = [SELECT Id, Code__c FROM User WHERE Username = 'sdrtester@test.com']; 
        Territory2 t = [SELECT Id, Name FROM Territory2 WHERE (NOT Name LIKE '%Region%') LIMIT 1];
        UserTerritory2Association uta = new UserTerritory2Association();
        uta.Territory2Id = t.id;
        uta.UserId = us.Id;
        uta.RoleInTerritory2 = us.Code__c;
        UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        insert uta;
        UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
        Account ac = [SELECT Id FROM Account LIMIT 1];
        List<String> accountIds = new List<String>();
        accountIds.add(ac.id);
        Test.startTest();
        System.runAs(u) {
            changeOwnerController.hasUserGrant(ac.id);
            changeOwnerController.hasUserGrantMassive(JSON.serialize(accountIds));
            changeOwnerController.getDMCs('a', 'User', ac.id);
            AccountTriggerHandler.disableTrigger = true;
            changeOwnerController.updateOwner(ac.id, Json.serialize(u), false);
            changeOwnerController.updateOwnerMassive(accountIds, Json.serialize(u));
            AccountTriggerHandler.disableTrigger = false;
        }
        Test.stopTest();
    }

    @isTest 
    static void testchangeOwnerControllerAdmin(){
        User us = [SELECT Id FROM User WHERE Username = 'sysAdminUser@test.com'];  
        Territory2 t = [SELECT Id, Name FROM Territory2 WHERE (NOT Name LIKE '%Region%') LIMIT 1];
        Test.startTest();
        Account acc = [SELECT Id, Assigned_Territory__c FROM Account LIMIT 1];
        AccountTriggerHandler.disableTrigger = true;
        acc.Assigned_Territory__c = t.Name;
        update acc;
        Account ac = [SELECT Id, Assigned_Territory__c FROM Account LIMIT 1];
        List<String> accountIds = new List<String>();
        accountIds.add(ac.id);
        System.runAs(us) {
            changeOwnerController.hasUserGrant(ac.id);
            changeOwnerController.hasUserGrantMassive(JSON.Serialize(accountIds));
            changeOwnerController.getDMCs('a', 'User', ac.id);
            AccountTriggerHandler.disableTrigger = true;
            changeOwnerController.updateOwner(ac.id, Json.serialize(us), false);
            changeOwnerController.updateOwner(ac.id, Json.serialize(us), true);
            changeOwnerController.updateOwnerMassive(accountIds, Json.serialize(us));
            AccountTriggerHandler.disableTrigger = false;
        }        
        Test.stopTest();
    }
}