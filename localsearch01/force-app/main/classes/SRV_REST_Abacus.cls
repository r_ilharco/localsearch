/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date		   : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : This class contains some useful methods for AbacusTransferJob: The implemented methods deal with the REST Callout
 * Changelog   : 
 */

public class SRV_REST_Abacus {
        
    private String method;
    private String contentTypeKey;
    private String contentTypeValue;
    private HttpRequest request;
    private String correlationId;
    private String jsonPayload;
    
    public SRV_REST_Abacus(){}
        
    public SRV_REST_Abacus (String endPoint, String correlationId, String jsonPayload ) {
        this.contentTypeKey = 'Content-Type';
        this.method = 'POST';
        this.contentTypeValue = 'application/json;charset=UTF-8';
        this.request = new HttpRequest ();
        this.request.setEndpoint(endPoint);
        this.request.setMethod(method);
        this.request.setHeader(contentTypeKey, contentTypeValue);
        this.request.setHeader(DOMC_Abacus.TRACE_HEAD_CORRELATION_ID, correlationId);
        this.request.setBody(jsonPayload);
        
    }
    
    public HttpResponse restCall(){ 
        Http httpClient = new Http();      
        HttpResponse response = httpClient.send(request);
        System.debug(response.getBody());
        return response;
    }
    
    public Error_Log__c checkStatus (HttpResponse response) {
        Integer statusCode = response.getStatusCode();
      //  System.response.getBody();
        Error_Log__c error = new Error_Log__c ();
        
        if (statusCode == 200) {
			System.debug ('ALL OK ' + statusCode);
        }
        else {
            System.debug ('NOT ALL OK ' + statusCode);
            //throw an Exception with the message 'Abacus save action returned ' + statusCode.
            throw new AbacusHelper.AbacusHelperException('Abacus save action returned ' + statusCode, ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
        }
		
        error = ErrorHandler.createLogInfo('Billing', 'Billing.sendToAbacus', 'Bill sent to Abacus', jsonPayload , correlationId); 
        System.debug(error);
        return error;
    }
    
}