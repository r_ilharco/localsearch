
global class TrialUpdateBeforeActive_BatchSch implements Database.Batchable<SObject>, Schedulable, Database.AllowsCallouts {
    
    private String sSQL=null;
    public TrialUpdateBeforeActive_BatchSch() //String query 
    {
    }
    
    global database.querylocator start(Database.BatchableContext bc)
    {         
        Date dt_from = Date.valueOf(System.Today()) - Integer.valueOf(Label.Trial_Process_Prior_Days);
        Date dt_to = Date.valueOf(System.Today());
        
        String vStatus=ConstantsUtil.QUOTELINE_STATUS_TRIAL_REQUESTED;
        sSQL = 'SELECT Id,TrialStatus__c,TrialStartDate__c,TrialEndDate__c FROM SBQQ__QuoteLine__c WHERE (TrialStatus__c  = :vStatus AND TrialStartDate__c >= :dt_from AND TrialStartDate__c <=:dt_to and TrialEndDate__c =null) ';
        return Database.getQueryLocator(sSQL);   
    }
    
    global void execute(Database.BatchableContext bc, SBQQ__QuoteLine__c[] listQLines)
    {
        List<SBQQ__QuoteLine__c> qltoupdate = new List<SBQQ__QuoteLine__c>();

        for (SBQQ__QuoteLine__c ql:listQLines){ 

            if(ql.TrialStartDate__c==null) {
                ql.TrialStartDate__c = Date.today();
                ql.TrialEndDate__c = Date.today()+Integer.valueOf(Label.Trial_end_prior_days);
            }else{
                ql.TrialEndDate__c = ql.TrialStartDate__c+Integer.valueOf(Label.Trial_end_prior_days);
            }
        // System.debug(ql.TrialStartDate__c);
        qltoupdate.add(ql);
        }
         update qltoupdate;
     
    }
    global void finish(Database.BatchableContext bc)
    {        
    }
    
    global void execute(SchedulableContext sc)
    {
        //System.schedule('Trial Activation Request Schedulabled', '0 0 13 * * ?', new TrialUpdateBeforeActive_BatchSch());
        
        TrialUpdateBeforeActive_BatchSch job = new TrialUpdateBeforeActive_BatchSch();
        Database.executeBatch(job,50); //First error: Too many callouts: 101
    }
}