@isTest
public class Test_SwissbillingDataHandlerService {
	@testSetup
    public static void testSetup(){
        AccountTriggerHandler.disableTrigger = true;
        Test_Billing.testSetup();
        AccountTriggerHandler.disableTrigger = false;
    }
    
    private static testmethod void swissbillingFeedbackTestingSUCCESS(){
		CreditNoteTriggerHandler.disableTrigger = true;
        Billing.collectAndSend(1);
        Invoice__c invoice = [SELECT Id, Name, Correlation_Id__c FROM Invoice__c LIMIT 1];
        SwissbillingWrappers.DetailsObj det = new SwissbillingWrappers.DetailsObj();
        det.ErrorDescription = '';
        det.ErrorCode = '';
        List<SwissbillingWrappers.FeedbackInvoicesObj> feeds = new List<SwissbillingWrappers.FeedbackInvoicesObj>();
        SwissbillingWrappers.FeedbackInvoicesObj feed = new SwissbillingWrappers.FeedbackInvoicesObj();
        feed.Status = 'SUCCESS';
        feed.RequestTimestamp = ''; 
        feed.ResponseTimestamp = '';
        feed.CorrelationId = invoice.Correlation_Id__c;
        feed.DocNumber = invoice.Name;
        feed.Details = det;
        feeds.add(feed);
        SwissbillingWrappers.StatusInvoiceSummary summary = new SwissbillingWrappers.StatusInvoiceSummary();
        summary.FeedbackInvoices = feeds;
        String JsonMsg = JSON.serialize(summary);
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoices';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        SwissbillingDataHandlerService.invoiceFeedbackPostCallout();
        Test.stopTest();
		CreditNoteTriggerHandler.disableTrigger = false;
    }
    
    private static testmethod void swissbillingFeedbackTestingERROR(){
        CreditNoteTriggerHandler.disableTrigger = true;
        Billing.collectAndSend(1);
        Invoice__c invoice = [SELECT Id, Name, Correlation_Id__c FROM Invoice__c LIMIT 1];
        SwissbillingWrappers.DetailsObj det = new SwissbillingWrappers.DetailsObj();
        det.ErrorDescription = '';
        det.ErrorCode = '';
        List<SwissbillingWrappers.FeedbackInvoicesObj> feeds = new List<SwissbillingWrappers.FeedbackInvoicesObj>();
        SwissbillingWrappers.FeedbackInvoicesObj feed = new SwissbillingWrappers.FeedbackInvoicesObj();
        feed.Status = 'ERROR';
        feed.RequestTimestamp = ''; 
        feed.ResponseTimestamp = '';
        feed.CorrelationId = invoice.Correlation_Id__c;
        feed.DocNumber = invoice.Name;
        feed.Details = det;
        feeds.add(feed);
        SwissbillingWrappers.StatusInvoiceSummary summary = new SwissbillingWrappers.StatusInvoiceSummary();
        summary.FeedbackInvoices = feeds;
        String JsonMsg = JSON.serialize(summary);
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/invoices';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        SwissbillingDataHandlerService.invoiceFeedbackPostCallout();
        Test.stopTest();
        SwissbillingWrappers.SalesInvoiceSummary salesSummary = new SwissbillingWrappers.SalesInvoiceSummary('1234','1234', 'testName', 12.5, 'testStatus', 2.5);
    	CreditNoteTriggerHandler.disableTrigger = false;
    }
}