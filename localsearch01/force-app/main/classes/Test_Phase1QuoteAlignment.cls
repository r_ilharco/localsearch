@isTest
public class Test_Phase1QuoteAlignment {
	
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        quote.Phase1Source__c = true;
        quote.SBQQ__CustomerDiscount__c = 10;
        update quote;
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        
        Integer count = 0;
        for(SBQQ__QuoteLine__c currentQL : childrenQLs){
            currentQL.Subscription_Term__c = null;
            switch on count {
                when 0 {
                    currentQL.SBQQ__Discount__c = 15;
                }
                when 1 {
                    currentQL.SBQQ__AdditionalDiscountAmount__c = 10.5;
                }
            }
            count++;
        }
        insert childrenQLs;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;

    }
    
    @isTest
    public static void test_batchTest(){
        List<AggregateResult> quoteLinesToPass = [SELECT SBQQ__Quote__c FROM SBQQ__QuoteLine__c GROUP BY SBQQ__Quote__c];
        Test.startTest();
        Phase1QuoteAlignment b = new Phase1QuoteAlignment();
        b.execute(null, quoteLinesToPass);
        Test.stopTest();
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c';
        List<SBQQ__QuoteLine__c> quoteLines = Database.query(queryQuoteLine);
        
        for(SBQQ__QuoteLine__c currentQL : quoteLines){
            System.assertEquals('12', currentQL.Subscription_Term__c);
            System.assertNotEquals(0, currentQL.Total__c);
            if(String.isBlank(currentQL.SBQQ__RequiredBy__c)){
                System.assertNotEquals(0, currentQL.Package_Total__c);
            }
        }
    }
}