/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Mara Mazzarella <mamazzarella@deloitte.it>
 * Date        : 2019-12-04
 * Sprint      : Sprint 7
 * Work item   : Samba migration
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */

global without sharing class setCustomerNumberOnMigratedAccount implements Database.Batchable<sObject>, Schedulable{

    global void execute(SchedulableContext sc) {
        setCustomerNumberOnMigratedAccount setCustomerNumber = new setCustomerNumberOnMigratedAccount(); 
        Database.executeBatch(setCustomerNumber, 1000);
    }
	global String log = '';
    global Database.QueryLocator start(Database.BatchableContext bc){       
      return Database.getQueryLocator('select id, Customer_Number__c,Status__c,Last_Inactive_Date__c,Activation_Date__c from account where Customer_Number__c = null or SystemModStamp < TODAY'+
                                     ' order by Customer_Number__c nulls first, LastModifiedDate asc LIMIT 10000');
    }
    
    global void execute(Database.BatchableContext info, List<Account> scope){
        System.debug('**insertCustomerNumber');
        Map<Id,Account>accIds = new Map<Id,Account>();
        Set<String> activeAcc = new Set<String>();
        boolean customerNumberChanged = false;
        Customer_Number_cs__c customerNumber = Customer_Number_cs__c.getOrgDefaults();
        System.debug('**customerNumber: '+customerNumber);
		try {
            for(Account acc : scope){
                log += acc.Id+',';
                if(acc.Customer_Number__c == null){
                    long nextValue = customerNumber.Next_Customer_Number__c.longValue();
                    customerNumber.Next_Customer_Number__c = nextValue + 1;
                    customerNumberChanged = true;
                    acc.Customer_Number__c = String.valueOf(nextValue).replaceAll('[^0-9]', '');
                }
                accIds.put(acc.Id,acc);
            }
            for(AggregateResult res :[select AccountId, count(id) from Contract where Status ='Active' and AccountId in: accIds.keySet() group by AccountId]){
            	system.debug('res'+ res);
                String accId =(String)res.get('AccountId');
                activeAcc.add(accId);
                accIds.get(accId).Status__c ='Active';
                accIds.get(accId).Active_Contracts__c = (Integer)res.get('expr0');
            }
            for(Contract c :[select AccountId, TerminateDate__c, CompanySignedDate from Contract where Status !='Active' and AccountId in: accIds.keySet()]){
                if(!activeAcc.contains(c.AccountId)){
                	accIds.get(c.AccountId).Status__c ='Inactive';
                    if(accIds.get(c.AccountId).Last_Inactive_Date__c == null){
                        accIds.get(c.AccountId).Last_Inactive_Date__c = c.TerminateDate__c;
                    }
                    else if (c.TerminateDate__c>accIds.get(c.AccountId).Last_Inactive_Date__c){
                        accIds.get(c.AccountId).Last_Inactive_Date__c = c.TerminateDate__c;   
                    }
                }
            }
            if(customerNumberChanged) {
                update customerNumber;
                update accIds.values();
            }
        }
        catch (Exception e) {
        	ErrorHandler.log(System.LoggingLevel.ERROR, 'setCustomerNumberOnMigratedAccount', 'setCustomerNumberOnMigratedAccount.execute', e, ErrorHandler.ErrorCode.E_DML_FAILED, 'AccountIds :'+log, null, null, null, null, false);
        } 
    }
    
    global void finish(Database.BatchableContext context) {}
    
}