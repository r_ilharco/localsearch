/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*	Trigger Handler for the Quote Document Object
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-03-18
* @systemLayer    Trigger
* @TestClass      test_quoteDocumentTrigger
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Gennaro Casola
* 2020-07-14      SPIII-1965 / Create new Field on Quote Document - Update "ExpirationDate__c" on
*				  Quote Document Creation with "Today + X" days where the X Days are retrived from
*			      QuoteDocumentSetting__mdt
*
* @changes		  SPIII-1883 - Digital Signature Expiration Management - Revoke "On-Going" Quote Documents on Quote Rejection
*				  delete envelope callout to Namirial
* @modifiedby     Gennaro Casola <gcasola@deloitte.it> 
* 2020-07-20
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-2437 - Quote Document is erased from Salesforce only if URL is populated AND Windream Upload Status = Upload Successful
*                 As soon as the quote document is uploaded correctly, Quote Document is erased from Salesforce
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-09-07
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-4307 - New Customer Flag - change calculation logic: setActivationDateOnAccount
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-11-24
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-4390 - Delete envelope after the quote document's signature method
*                 switches to manual process
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2021-01-26
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class QuoteDocumentTriggerHandler implements ITriggerHandler{
    public static Boolean hasBeenQuoteRejected = false;
    public static String triggerName {get;set;}
	public static Boolean disableTrigger {
        get {
            if(disableTrigger == null) {
                Bypass_Triggers__c orgwideBypass = Bypass_Triggers__c.getOrgDefaults();
                if(orgwideBypass.Trigger_Name__c != NULL) {
                    List<String> allTrgNames = orgwideBypass.Trigger_Name__c.split(',');
                    if(allTrgNames.contains(triggerName)) return true;
                } else {
                    Bypass_Triggers__c profilewideBypass = Bypass_Triggers__c.getInstance(Userinfo.getProfileId());
                    if(profilewideBypass.Trigger_Name__c != NULL) {
                        List<String> allTrgNames = profilewideBypass.Trigger_Name__c.split(',');
                        if(allTrgNames.contains(triggerName)) return true;
                    } else {
                        Bypass_Triggers__c userwideBypass = Bypass_Triggers__c.getInstance(Userinfo.getUserId());
                        if(userwideBypass.Trigger_Name__c != NULL) {
                            List<String> allTrgNames = userwideBypass.Trigger_Name__c.split(',');
                            if(allTrgNames.contains(triggerName)) return true;
                        }
                    }
                }
                return false;
            } else {
                return disableTrigger;
            }
        } set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
    public void BeforeInsert(List<SObject> newItems){
        QuoteDocumentTriggerHelper.updateExpirationDates((List<SBQQ__QuoteDocument__c>)newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        QuoteDocumentTriggerHelper.checkIfAnyUploadedDocument(newItems, oldItems);
        QuoteDocumentTriggerHelper.updateQuotesOfManuallySignedDocuments(newItems,oldItems);
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems){}
    
    public void AfterInsert(Map<Id, SObject> newItems){
        QuoteDocumentTriggerHelper.createContentVersion(newItems);
        QuoteDocumentTriggerHelper.updateQuoteStatus(newItems);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        QuoteDocumentTriggerHelper.switchToManualProcess((Map<Id, SBQQ__QuoteDocument__c>) newItems, (Map<Id, SBQQ__QuoteDocument__c>) oldItems);
        QuoteDocumentTriggerHelper.documentStatusUpdated((Map<Id, SBQQ__QuoteDocument__c>) newItems, (Map<Id, SBQQ__QuoteDocument__c>) oldItems);
        QuoteDocumentTriggerHelper.updateQuotesStatusOfExpiredORRevokedDocuments(newItems,oldItems);
        QuoteDocumentTriggerHelper.updateQuotesOfManuallySignedDocuments(newItems,oldItems);
        QuoteDocumentTriggerHelper.deleteContectIfQuoteDocumentUploaded(newItems,oldItems);
        //QuoteDocumentTriggerHelper.deleteEnvelopForManualDocs((Map<Id, SBQQ__QuoteDocument__c>)oldItems, (Map<Id, SBQQ__QuoteDocument__c>) newItems);
        QuoteDocumentTriggerHelper.setActivationDateOnAccount((Map<Id, SBQQ__QuoteDocument__c>)newItems, (Map<Id, SBQQ__QuoteDocument__c>) oldItems);
        QuoteDocumentTriggerHelper.updateTerritoryInformationOnQuote((Map<Id, SBQQ__QuoteDocument__c>)newItems, (Map<Id, SBQQ__QuoteDocument__c>) oldItems);
    }
    public void AfterDelete(Map<Id, SObject> oldItems){}
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}