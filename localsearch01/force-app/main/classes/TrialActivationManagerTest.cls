@isTest
public class TrialActivationManagerTest implements HttpCalloutMock {
    
    public Integer type;
    private String body = '{"message":"test message"}';
    
    TrialActivationManagerTest(Integer typeHere){
        this.type=typeHere;
    } 
    
    public HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        
        if(this.type==1){           
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setStatus('');
            res.setBody(body);
        }
        
        if(this.type==2){
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody(body);
        }
        
        if(this.type==3){
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(201);
            res.setStatus('OK');
            res.setBody(body);
        }
        
        return res;
    }
    
    @isTest
    public static void testTrialActivationManager1()
    {          
        try{
            GlobalConfiguration_cs__c setting = new GlobalConfiguration_cs__c();
			setting.skillCallTrial__c = true;
            insert setting;
            
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            Test.setMock(HttpCalloutMock.class, new TrialActivationManagerTest(1));
            test.startTest();
            TrialActivationManager.sendTrialRequest(listQLines);
            test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }

    
    @isTest
    public static void testTrialActivationManager2()
    {          
        try{
            GlobalConfiguration_cs__c setting = new GlobalConfiguration_cs__c();
			setting.skillCallTrial__c = false;
            insert setting;
            
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            Test.setMock(HttpCalloutMock.class, new TrialActivationManagerTest(2));
            test.startTest();
            TrialActivationManager.sendTrialRequest(listQLines);
            test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }
    
    
    @isTest
    public static void testTrialActivationManager3()
    {          
        try{
            GlobalConfiguration_cs__c setting = new GlobalConfiguration_cs__c();
			setting.skillCallTrial__c = false;
            insert setting;
            
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            Test.setMock(HttpCalloutMock.class, new TrialActivationManagerTest(1));
            test.startTest();
            TrialActivationManager.sendTrialRequest(listQLines);
            test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(true,e.getMessage());
        }
    }

    
    @isTest
    public static void testTrialActivationManager4()
    {          
        try{
            GlobalConfiguration_cs__c setting = new GlobalConfiguration_cs__c();
			setting.skillCallTrial__c = false;
            insert setting;
            
            List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>(); 
            SBQQ__QuoteLine__c qLine = TrialActivationUtil_Test.getQuoteLine();
            listQLines.add(qLine);
            
            Test.setMock(HttpCalloutMock.class, new TrialActivationManagerTest(3));
            test.startTest();
            TrialActivationManager.sendTrialRequest(listQLines);
            test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(False,e.getMessage());
        }
    }
    
    
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Quote Line Management';
        byPassFlow.SetupOwnerId = Userinfo.getUserId();
        insert byPassFlow;
    }
}