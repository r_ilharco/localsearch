/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*  TestClass of AccountSelector
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mazzarella Mara <mamazzarella@deloitte.it>
* @created		  06-12-2019
* @systemLayer    Test 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class Test_AccountSelector {
    
    @testSetup
    public static void setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Account Status Management,Send Owner Notification,Community User Creation,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
    }
    
    @isTest 
    public static void test_getAccountsWithDefaultRecords(){
        Set<Id> ids = new Set<Id>();
        for(Account a : [select id from Account]) {
            ids.add(a.id);
        }
        AccountSelector.getAccountsWithDefaultRecords(ids);
        
    }
    
    @isTest 
    public static void test_getAccountsWithDefaultBillingProfile(){
        
        Set<Id> ids = new Set<Id>();
        for(Account a : [select id from Account]){
            ids.add(a.id);
        }
        AccountSelector.getAccountsWithDefaultBillingProfile(ids);
    }
}