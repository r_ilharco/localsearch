@isTest
global class Test_AdContextReservationMock implements HttpCalloutMock{
    
    public Boolean simulateError {get;set;}
    
	global HTTPResponse respond(HTTPRequest req) {
        //String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.AD_CONTEXT_ALLOCATION);
        if(!simulateError){
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"href":"https://backend.preprod.localsearch.systems/ad-context-allocations/testAllocationId","id":"testAllocationId","product_code":"testProductCode","category":{"href":"https://backend.preprod.localsearch.systems/categories/testCategoryId","classification":"LOW"},"slot":"testSlot","start_date":"2020-06-02","disabled":false,"assets":{"href":"https://backend.preprod.localsearch.systems/ad-context-allocations/testAllocationId/assets"},"expiration_date":"2020-07-02","status":"Reserved"}');
            res.setStatusCode(200);
            return res;
        }
        else{
			HttpResponse res = new HttpResponse();
            res.setHeader('Content-	Type', 'application/json');
            res.setStatusCode(200);
            return res;            
        }
        
    }
}