@isTest
public class ApprovalACLTrigger_test {

    
    
    
    @isTest static void ApprovalACLTrigger_test (){

        Id recordTypeAcc = [SELECT id from RecordType WHERE DeveloperName = 'TypAcc'].Id;
        Id recordTypeCon = [SELECT id from RecordType WHERE DeveloperName = 'TypCon'].Id;
        Id recordTypeLead = [SELECT id from RecordType WHERE DeveloperName = 'TypLea'].Id;

        Test.startTest();
            
            Account newAccount = new Account(
                Name = 'aTest1',
                P_O_Box_City__c = 'aTest1',
                P_O_Box_Zip_Postal_Code__c = 'aTest1',
                POBox__c = 'aTest1',
                PreferredLanguage__c = 'German',
                Status__c = 'New'
            );
            insert newAccount;
        
            ApprovalACL__c appAccount = new ApprovalACL__c(
                //Name = 'Test1',
                RecordTypeId = recordTypeAcc,
                acc_AccountName__c = 'Test1',
                acc_Billing_City__c = 'Test1',
                acc_Billing_Country__c = 'Test1',
                acc_Billing_Zip_PostalCode__c = 'Test1',
                acc_P_O_Box_City__c = 'Test1',
                acc_P_O_Box_Zip_Postal_Code__c = 'Test1',
                acc_POBox__c = 'Test1',
                acc_PreferredLanguage__c = 'German',
                acc_Status__c = 'New'
            );
            
            insert appAccount;
            
            List <Account> lstAccount = [SELECT Id FROM Account LIMIT 1];
        
            Id idAccount = lstAccount[0].Id; 
        
            ApprovalACL__c appContact = new ApprovalACL__c(
           // Name = 'TestCon1',
            RecordTypeId = recordTypeCon,
            con_AccountId__c = newAccount.id,
            con_Business_Phone__c = '123456789',
            con_Email__c = 'TestCon1@testcon1.test',
            con_FirstName__c = 'TestCon1',
            con_GoldenRecordID__c = 'TestCon1',
            con_LastName__c = 'TestCon1',
            con_PO_Box__c = 'TestCon1',
            con_PO_BoxCity__c = 'TestCon1',
            con_PO_BoxZip_PostalCode__c = 'TestCon1',
            con_Position__c = 'TestCon1'
            );
            
            insert appContact;

            ApprovalACL__c appLead = new ApprovalACL__c(
                //Name = 'TestLea1',
                RecordTypeId = recordTypeLead,
                lea_Company__c = 'TestLea1',
                lea_FirstName__c = 'TestLea1',
                lea_LastName__c = 'TestLea1',
                lea_LeadSource__c = 'Advertising',
                lea_MobilePhone__c = '123456789',
                lea_P_O_Box__c = 'TestLea1',
                lea_P_O_Box_City__c = 'TestLea1',
                lea_P_O_Box_Zip_Postal_Code__c = 'TestLea1',
                lea_Phone__c = '123456789',
                lea_Rating__c = 'Promising',
                lea_Status__c = 'Open'
            );
            insert appLead;

        list <ApprovalACL__c> lstApproval = [SELECT Check2__c FROM ApprovalACL__c ];
        list <ApprovalACL__c> updApp= new List<ApprovalACL__c>();
        
        for(ApprovalACL__c app1 : lstApproval) {
            app1.check2__c = 'OK';
            updApp.add(app1);
        }
        update updApp;
        
        Test.stopTest();
        
        
    }
}