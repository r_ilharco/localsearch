public class UtilityToken {
    public static masheryTokenInfo getToken()
    {
        masheryTokenInfo token = new masheryTokenInfo();
        token = UtilityToken.refreshToken();
        return token;
    }
    public static masheryTokenInfo getTokenOptm(){
        masheryTokenInfo token = new masheryTokenInfo();
       
        Profile p = [select id from profile where name = :ConstantsUtil.SysAdmin limit 1];
        system.debug('masheryTokenInfo>profileId >> '+p.Id);
      	Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(p.Id);
        decimal cached_millisecs;

       if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
            system.debug('millisecs: ' +cached_millisecs);
       }

      if(c2.Token__c == null || c2.Token__c == '' ||((cached_millisecs/1000).round()>=(Integer)c2.Expires__c)){
            token = UtilityToken.refreshToken();
          
            c2.Token__c = token.access_token;
            c2.Token_Type__c = token.token_type;
            c2.Expires__c = token.expires_in;
            upsert c2;
            database.upsert(c2, true);

            system.debug('Empty masheryTokenInfo>profileId'+c2);
       } else{
           token.access_token= c2.Token__c;
           token.token_type= c2.Token_Type__c;
           token.expires_in= (Integer)c2.Expires__c;
          			
            system.debug('nonEmpty masheryTokenInfo>profileId'+c2);
       }
        System.debug('Token >>'+token);
    
        return token;        
	}
    
      
    public static void ResetTokenSch(){
        masheryTokenInfo token = new masheryTokenInfo();

        Profile p = [select id from profile where name = :ConstantsUtil.SysAdmin limit 1];
        system.debug('masheryTokenInfo>profileId >> '+p.Id);
        Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(p.Id);
        
        token = UtilityToken.refreshToken();
        
        c2.Token__c = token.access_token;
        c2.Token_Type__c = token.token_type;
        c2.Expires__c = token.expires_in;
        upsert c2;
        database.upsert(c2, true);
    }
   
    public static masheryTokenInfo refreshToken(){
        
        //SF2 -210 Order Management - <mamazzarella@deloitte.it>
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.SERVICE_MASHERY); 
        
        Id profileId= userinfo.getProfileId();
        Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(profileId);
		//SF2 -210 Order Management - <mamazzarella@deloitte.it>

        System.debug('Mashery_Setting__c '+ c2);
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint(c2.Endpoint__c +methodName);
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'grant_type='+c2.Grant_Type__c+'&client_id='+c2.Client_Id__c+'&client_secret='+c2.Client_Secret__c;
        System.debug('body: '+body+' - Token: '+c2.Token__c);
        request.setBody(body);
        system.debug('request:'+request);
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 201 || response.getStatusCode() == 200){
            System.debug(response.getBody());
            masheryTokenInfo parse = (masheryTokenInfo)JSON.deserialize(response.getBody(), masheryTokenInfo.class);
            System.debug('masheryTokenInfo:'+parse);
            return parse;                                
        }else {
            System.debug('The status code returned was not expected: ' +
            response.getStatusCode() + ' ' + response.getStatus());
            System.debug(response.getBody());
            return null;
        }
    }
    
     public static masheryTokenInfo refreshTokenTemp(){
        
        //SF2 -210 Order Management - <mamazzarella@deloitte.it>
        //String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.SERVICE_MASHERY); 
        
        //Id profileId= userinfo.getProfileId();
        //Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(profileId);
		//SF2 -210 Order Management - <mamazzarella@deloitte.it>

        //System.debug('Mashery_Setting__c '+ c2);
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://apicloud-test.localsearch.tech/ei/security/token');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'grant_type=client_credentials&client_id=eg35g3uc2gt4tqz5yb4a76rn&client_secret=748t5fXJGD';
        //System.debug('body: '+body+' - Token: '+c2.Token__c);
        request.setBody(body);
        system.debug('request:'+request);
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 201 || response.getStatusCode() == 200){
            System.debug(response.getBody());
            masheryTokenInfo parse = (masheryTokenInfo)JSON.deserialize(response.getBody(), masheryTokenInfo.class);
            System.debug('masheryTokenInfo:'+parse);
            return parse;                                
        }else {
            System.debug('The status code returned was not expected: ' +
            response.getStatusCode() + ' ' + response.getStatus());
            System.debug(response.getBody());
            return null;
        }
    }
    
      public static accessTokenInformation getAccessToken(string serviceName){
        CustomMetadataUtil serviceConfig = CustomMetadataUtil.getCustMetadataValues(serviceName); 
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        
        request.setEndpoint(serviceConfig.endPoint);
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    	//request.setHeader('authorization', 'Basic '+ configWindream.token);
  
        string body = 'grant_type='+serviceConfig.grantType+'&client_id='+serviceConfig.clientId+'&client_secret='+serviceConfig.clientSecret;
        system.debug(serviceName + ' = body: '+body+' - Saved Token: '+serviceConfig.token);
        
        request.setBody(body);
        system.debug(serviceName + ' = request:'+request);
        
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == Integer.valueOf(ConstantsUtil.STATUSCODE_201) || response.getStatusCode() == Integer.valueOf(ConstantsUtil.STATUSCODE_SUCCESS)) {
            System.debug(serviceName + ' = reponse body: ' + response.getBody());
            accessTokenInformation token = (accessTokenInformation)JSON.deserialize(response.getBody(), accessTokenInformation.class);
            System.debug(serviceName + ' = AcceccTokenInfo:'+token);
            
            return token;
                        
        } else {
            System.debug(serviceName + ' = The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            System.debug(serviceName + ' = reponse body: ' + response.getBody());
            return null;
        }
    }
    
    public class accessTokenInformation {
        public String access_token{get; set;}
        public String token_type{get; set;}
        public Integer expires_in{get; set;}
    }
    
    public class masheryTokenInfo {
        public String access_token{get; set;}
        public String token_type{get; set;}
        public Integer expires_in{get; set;}
    }
}