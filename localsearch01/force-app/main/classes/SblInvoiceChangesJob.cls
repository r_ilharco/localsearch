public class SblInvoiceChangesJob implements Queueable, Database.AllowsCallouts {
    private DateTime startDate;
    private DateTime endDate;
    
    public SblInvoiceChangesJob() {
        // Without parameters, will check for last successful run, and fetch from that date.
        String className = String.valueOf(this).substring(0, String.valueOf(this).indexOf(':'));
        List<AsyncApexJob> lastJobs = [SELECT CreatedDate from AsyncApexJob where Status='Completed' 
                              and ApexClassId in (SELECT Id FROM ApexClass WHERE name=:className) 
                              Order By CreatedDate desc LIMIT 1];
        if(lastJobs.size() > 0) { 
            this.startDate = lastJobs[0].CreatedDate; 
            //Vincenzo Laudato
            this.endDate = Datetime.now();
        }
    }
    
    public SblInvoiceChangesJob(DateTime startDate, DateTime endDate) {
    	this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public void execute(QueueableContext context) {
        //Vincenzo Laudato 2019-06-14 AS-46 *** START
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(this.startDate, this.endDate);

        if(summaries != null){
            Map<String, BillingResults.InvoiceSummary> resultMap = new Map<String, BillingResults.InvoiceSummary>();
            for(BillingResults.InvoiceSummary summary : summaries.sales_invoice_summaries) {
                //Vincenzo Laudato - RollBack - Tibco released the fix
                //Datetime currentSummaryTs = Datetime.valueOf(summary.timestamp);
                //if(currentSummaryTs >= this.startDate && currentSummaryTs <= this.endDate){
                    BillingResults.InvoiceSummary storedSummary = resultMap.get(summary.sales_invoice_id);
                    system.debug('storedSummaty: ' + storedSummary);
                if(storedSummary != null) {
                        storedSummary.mergeSummary(summary);
                    } else {
                        resultMap.put(summary.sales_invoice_id, summary);
                    }    
                //}
                system.debug('resultMap: ' + resultMap);
            }
            if(!resultMap.isEmpty()){
                system.debug('here for the invoice changes batch');
                Database.executeBatch(new SblInvoicesChangesBatch(resultMap), 500);
            }
        }
        //Vincenzo Laudato 2019-06-14 AS-46 *** END
    }
}