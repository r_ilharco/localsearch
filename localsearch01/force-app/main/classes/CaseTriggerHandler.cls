public without sharing class CaseTriggerHandler implements ITriggerHandler{
    
    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
    
    public void BeforeInsert(List<SObject> newItems){
        
        List<Case> newCases = (List<Case>) newItems;
        
        List<RecordType> recTypeCaseMgmt = [SELECT Id , DeveloperName
                                            FROM RecordType
                                            WHERE IsActive = true
                                            AND SobjectType = 'Case'
                                            AND BusinessProcessId IN (SELECT Id 
                                                                      FROM BusinessProcess
                                                                      WHERE Name='Case Management - Support Process')
                                            ];
        Set<Id> recTypeIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();

        Map<String,Id> rtDevNameId = new Map<String,Id>();
        Map<Id,String> rtIdDevName = new Map<Id,String>(); // FA 16-11-2019
        for (RecordType recType : recTypeCaseMgmt) {
            recTypeIds.add(recType.Id);
            rtDevNameId.put(recType.DeveloperName,recType.Id); // FA 06-11-2019
            rtIdDevName.put(recType.Id,recType.DeveloperName); // FA 16-11-2019
        }

        // Inizio modifica - gestione web-to-case - 23-10-2019 - FA
        Map<String, List<Id>> mapAccWeb = new Map<String, List<Id>>();
        Map<String, List<Id>> mapUserWeb = new Map<String, List<Id>>();
        Set<String> emailWebToCase = new Set<String>();
        Set<String> phoneWebToCase = new Set<String>();
        String emailWebToCaseString = '';
        String phoneWebToCaseString = '';

        for (Case contWebToCase : newCases){
            if(contWebToCase.Origin == 'Web'){

                if(!String.isBlank(contWebToCase.SuppliedEmail) && emailWebToCaseString.length()>0){
                    emailWebToCaseString = emailWebToCaseString+' OR '+contWebToCase.SuppliedEmail;
                    emailWebToCase.add(contWebToCase.SuppliedEmail);
                } else if(!String.isBlank(contWebToCase.SuppliedEmail)){
                    emailWebToCaseString = contWebToCase.SuppliedEmail;
                    emailWebToCase.add(contWebToCase.SuppliedEmail);
                }

                if(!String.isBlank(contWebToCase.SuppliedPhone) && phoneWebToCaseString.length()>0){
                    phoneWebToCaseString = phoneWebToCaseString+' OR '+contWebToCase.SuppliedPhone;
                    phoneWebToCase.add(contWebToCase.SuppliedPhone);
                }else if(!String.isBlank(contWebToCase.SuppliedPhone)){
                    phoneWebToCaseString = contWebToCase.SuppliedPhone;
                    phoneWebToCase.add(contWebToCase.SuppliedPhone);
                }
            }
        }

        system.debug('*****>>>> emailWebToCaseString = '+emailWebToCaseString);
        system.debug('*****>>>> phoneWebToCaseString = '+phoneWebToCaseString);

        List<Account> associatedAcc = new List<Account>();
        List<List<Account>> associatedAccListEmail = new List<List<Account>>(); // FA 12-12-2019
        List<List<Account>> associatedAccListPhone = new List<List<Account>>(); // FA 12-12-2019
        List<User> associatedUser = new List<User>(); // Internal Support RT - FA 20-11-2019

        // Inizio - da SOQL A SOSL per Account -- FA 12-12-2019
        /*if (emailWebToCase.size()>0 || phoneWebToCase.size()>0){
            associatedAcc = [SELECT Id, Email__c, Phone FROM Account WHERE (Email__c IN: emailWebToCase AND Email__c != NULL) OR (Phone IN: phoneWebToCase AND Phone != NULL)];
            if(emailWebToCase.size()>0) // controllo size emailWebToCase - FA 28-11-2019
                associatedUser = [SELECT Id, Email FROM User WHERE Email != NULL AND Email IN: emailWebToCase AND Profile.UserLicense.Name ='Salesforce' AND IsActive = true]; // Internal Support RT - FA 20-11-2019
        }*/

        if (emailWebToCase.size()>0){

            String searchQuery = 'FIND \'' + emailWebToCaseString + '\' IN EMAIL FIELDS RETURNING Account(Id, Email__c, Phone)';
            associatedAccListEmail = search.query(searchQuery);
            associatedUser = [SELECT Id, Email FROM User WHERE Email != NULL AND Email IN: emailWebToCase AND Profile.UserLicense.Name ='Salesforce' AND IsActive = true]; // Internal Support RT - FA 20-11-2019
        }

        if (phoneWebToCase.size()>0){
            String searchQuery = 'FIND \'' + phoneWebToCaseString + '\' IN PHONE FIELDS RETURNING Account(Id, Email__c, Phone)';
            associatedAccListPhone = search.query(searchQuery);
        }

        Set<Id> accountSetId = new Set<Id>();
        List<Account> accountList = new List<Account>();
        if(associatedAccListEmail.size()>0)
            accountList.addAll((List<Account>)associatedAccListEmail[0]);
        if(associatedAccListPhone.size()>0)
            accountList.addAll((List<Account>)associatedAccListPhone[0]);

        System.debug('accountList = '+ accountList);

        for (Account uniqueAcc : accountList) {
            if (accountSetId.add(uniqueAcc.Id)) {
                associatedAcc.add(uniqueAcc);
            }
        }
        System.debug('aaaaa bbbbb cccccc = '+ associatedAcc);

        // Fine - da SOQL A SOSL per Account -- FA 12-12-2019

        // Inizio - Internal Support RT - FA 20-11-2019
        for (User mapUser : associatedUser ){
            if(!String.isBlank(mapUser.Email)){
                List<Id> temp = mapUserWeb.get(mapUser.Email);
                if (temp == null){
                    mapUserWeb.put(mapUser.Email, new List<Id>{mapUser.Id});
                } else {
                    temp.add(mapUser.Id);
                }
            }
        }
        // Fine - Internal Support RT - FA 20-11-2019

        for (Account mapAcc : associatedAcc ){
            if(!String.isBlank(mapAcc.Email__c)){
                List<Id> temp = mapAccWeb.get(mapAcc.Email__c);
                if (temp == null){
                    mapAccWeb.put(mapAcc.Email__c, new List<Id>{mapAcc.Id});
                } else {
                    temp.add(mapAcc.Id);
                }
            }
            if(!String.isBlank(mapAcc.Phone)){
                List<Id> temp = mapAccWeb.get(mapAcc.Phone);
                if (temp == null){
                    mapAccWeb.put(mapAcc.Phone, new List<Id>{mapAcc.Id});
                } else {
                    temp.add(mapAcc.Id);
                }
            }
        }

        // Inizio modifica - Ricerca Contact per gestione web-to-case - 30-10-2019 - FA
        Map<String, List<Id>> mapContWeb = new Map<String, List<Id>>();
        Map<String, Id> mapAccPrimCont = new Map<String, Id>();
        Set<Id> accIdForPrimCont = new Set<Id>();
        for (Account accIdPrim : associatedAcc) {
            accIdForPrimCont.add(accIdPrim.Id);
        }

        List<Contact> associatedCont = new List<Contact>();

        // Inizio - da SOQL A SOSL per Contact -- FA 12-12-2019

        /*if (emailWebToCase.size()>0 || phoneWebToCase.size()>0 || accIdForPrimCont.size()>0){
            associatedCont = [SELECT Id, Email, Phone, AccountId, Primary__c 
                              FROM Contact 
                              WHERE (Email IN: emailWebToCase AND Email != NULL)
                              OR (Phone IN: phoneWebToCase AND Phone != NULL)
                              OR (Primary__c=true AND AccountId IN: accIdForPrimCont AND AccountId != NULL)
                             ];
        }*/
        List<Contact> associatedCont1 = new List<Contact>();
        List<List<Contact>> associatedCont2 = new List<List<Contact>>();
        if (emailWebToCase.size()>0 || accIdForPrimCont.size()>0){
            associatedCont1 = [SELECT Id, Email, Phone, AccountId, Primary__c 
                               FROM Contact 
                               WHERE (Email IN: emailWebToCase AND Email != NULL)
                               OR (Primary__c=true AND AccountId IN: accIdForPrimCont AND AccountId != NULL)
                              ];
        }
        if (phoneWebToCase.size()>0){
            String searchQuery = 'FIND \'' + phoneWebToCaseString + '\' IN PHONE FIELDS RETURNING Contact(Id, Email, Phone, AccountId, Primary__c)';
            associatedCont2 = search.query(searchQuery);
        }

        Set<Id> contactSetId = new Set<Id>();

        if(associatedCont2.size()>0)
            associatedCont1.addAll((List<Contact>)associatedCont2[0]);

        System.debug('associatedCont1 = '+ associatedCont1);

        for (Contact uniqueCont : associatedCont1) {
            if (contactSetId.add(uniqueCont.Id)) {
                associatedCont.add(uniqueCont);
            }
        }
        System.debug('aaaaa bbbbb cccccc = '+ associatedCont);        

        // Fine - da SOQL A SOSL per Contact -- FA 12-12-2019

        for (Contact mapCont : associatedCont){
            
            if(!String.isBlank(mapCont.Email)){
                List<Id> temp = mapContWeb.get(mapCont.Email);
                if (temp == null){
                    mapContWeb.put(mapCont.Email, new List<Id>{mapCont.Id});
                } else {
                    temp.add(mapCont.Id);
                }
            }
            if(!String.isBlank(mapCont.Phone)){
                List<Id> temp = mapContWeb.get(mapCont.Phone);
                if (temp == null){
                    mapContWeb.put(mapCont.Phone, new List<Id>{mapCont.Id});
                } else {
                    temp.add(mapCont.Id);
                }
            }
            if(accIdForPrimCont.contains(mapCont.AccountId) && mapCont.Primary__c == true){
                mapAccPrimCont.put(mapCont.AccountId, mapCont.Id);
            }
        }
        // Fine modifica - Ricerca Contact per gestione web-to-case - 30-10-2019 - FA


        try{
            for(Case currCase: newCases) {
                System.debug('**** OnBeforeInsert -- newAccount - web to case = '+currCase);
                System.debug('recTypeIds ***'+recTypeIds);
                if(recTypeIds.contains(currCase.RecordTypeId) && currCase.Origin=='Web'){
                    // Inizio modifica - Ricerca Contact per gestione web-to-case - 30-10-2019 - FA
                    // Inizio - Internal Support RT - FA 20-11-2019
                    if( rtIdDevName.get(currCase.RecordTypeId) == 'Internal_Support' && mapUserWeb.containsKey(currCase.SuppliedEmail)){
                        List<Id> idsUser = mapUserWeb.get(currCase.SuppliedEmail);
                        System.debug('idsCont.size()==1 ' + (idsUser.size()==1)+' idsCont '+idsUser);
                        if(idsUser.size()==1){
                            currCase.Employee__c = idsUser[0];
                        }
                    } // Fine - Internal Support RT - FA 20-11-2019
                    else if(mapContWeb.containsKey(currCase.SuppliedEmail)){
                        List<Id> idsCont = mapContWeb.get(currCase.SuppliedEmail);
                        System.debug('idsCont.size()==1 ' + (idsCont.size()==1)+' idsCont '+idsCont);
                        if(idsCont.size()==1){
                            currCase.ContactId = idsCont[0];
                        }
                    }else if(mapContWeb.containsKey(currCase.SuppliedPhone)){
                        List<Id> idsCont = mapContWeb.get(currCase.SuppliedPhone);
                        if(idsCont.size()==1){
                            currCase.ContactId = idsCont[0];
                        }
                    // Fine modifica - Ricerca Contact per gestione web-to-case - 30-10-2019 - FA
                    }else if(mapAccWeb.containsKey(currCase.SuppliedEmail)){
                        List<Id> idsAcc = mapAccWeb.get(currCase.SuppliedEmail);
                        if(idsAcc.size()==1){
                            currCase.AccountId = idsAcc[0];
                            currCase.ContactId = mapAccPrimCont.get(idsAcc[0]); // Aggiunta - dalla Ricerca Contact per gestione web-to-case - 30-10-2019 - FA
                        }
                    }else if(mapAccWeb.containsKey(currCase.SuppliedPhone)){
                        List<Id> idsAcc = mapAccWeb.get(currCase.SuppliedPhone);
                        if(idsAcc.size()==1){
                            currCase.AccountId = idsAcc[0];
                            currCase.ContactId = mapAccPrimCont.get(idsAcc[0]); // Aggiunta - dalla Ricerca Contact per gestione web-to-case - 30-10-2019 - FA
                        }                
                    }
                }
            }
        }
        catch (Exception e){
            system.debug('Error: ' + e.getMessage());
        }
        // Fine modifica - gestione web-to-case - 23-10-2019 - FA

        // Inizio Modifica - Riconoscimento Topic in Email-to-Case -- 16-11-2019 FA
        Set<String> searchableQueue = new Set<String>();
        List<Topic_Assignment_Case__mdt> topicList = [SELECT Record_Type_DevName__c,Topic__c,Case_Origin__c,Case_Owner__c FROM Topic_Assignment_Case__mdt];
        Map<String, String> keyTopicMap = new Map<String, String>();
        Set<String> keyTopic = new Set<String>();

        for(Topic_Assignment_Case__mdt tl : topicList ){
            String key = tl.Record_Type_DevName__c+'_'+tl.Case_Origin__c+'_'+tl.Case_Owner__c;
            System.debug('key >>> '+key);
            keyTopic.add(key);
            keyTopicMap.put(key,tl.Topic__c);
            searchableQueue.add(tl.Case_Owner__c);

        }

        // Fine Modifica - Riconoscimento Topic in Email-to-Case -- 16-11-2019 FA

        // Inizio modifica - gestione email to case - IBM CODES - 06-11-2019 - FA

        List<IBM_Code__mdt> ibmCodesList = [SELECT Code__c,Complaint__c,Language__c,Queue_Owner__c,Record_Type_DevName__c,Topic__c FROM IBM_Code__mdt];
        
        Set<String> ibmCodes = new Set<String>();
        Map<String, IBM_Code__mdt> ibmCodesMap = new Map<String, IBM_Code__mdt>();
        //Set<String> searchableQueue = new Set<String>(); // new set spostato più in alto - 16-11-2019 FA
        Map<String, Id> queueIBMCodeRecMap = new Map<String, Id>();
        Map<Id,String> queueTopicAssignMap = new Map<Id,String>();
        Integer maxLengthCode = 0; //FA 09-11-2019
        Integer minLengthCode = 1000; //FA 09-11-2019

        for (IBM_Code__mdt ibmc : ibmCodesList){

            ibmCodes.add(ibmc.Code__c);
            ibmCodesMap.put(ibmc.Code__c, ibmc);
            if (!String.isBlank(ibmc.Queue_Owner__c)){
                searchableQueue.add(ibmc.Queue_Owner__c);
            }

            //Inizio modifica - calcolo lunghezza codice - FA 09-11-2019
            System.debug('ibm.Code__c = '+ibmc.Code__c);
            if(maxLengthCode < ibmc.Code__c.length()){
                maxLengthCode = ibmc.Code__c.length();
            }
            if(minLengthCode > ibmc.Code__c.length()){
                minLengthCode = ibmc.Code__c.length();
            }
            System.debug('maxLengthCode = '+maxLengthCode+' minLengthCode = '+minLengthCode);
            //Fine modifica - calcolo lunghezza codice - FA 09-11-2019
        }
        if (searchableQueue.size()>0){

            List<QueueSobject> ownerQuToAssign = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject WHERE Queue.DeveloperName IN: searchableQueue];
            for (QueueSobject owQueue : ownerQuToAssign) {
                queueIBMCodeRecMap.put(owQueue.Queue.DeveloperName, owQueue.Queue.Id); 
                queueTopicAssignMap.put(owQueue.Queue.Id,owQueue.Queue.DeveloperName); // Aggiunta per riconoscimento Topic in Email-to-Case -- 16-11-2019 FA
            }
            System.debug('&&&&&&&&&&& queueIBMCodeRecMap '+queueIBMCodeRecMap);
        }

        try{    
            for(Case emailToCase : newCases){

                if(emailToCase.Origin == 'Email'){
                    // Inizio Modifica - Riconoscimento Topic in Email-to-Case -- 16-11-2019 FA

                    String controlKey = rtIdDevName.get(emailToCase.RecordTypeId)+'_'+emailToCase.Origin+'_'+queueTopicAssignMap.get(emailToCase.OwnerId);
                    System.debug('controlKey >>> '+controlKey);
                    if(keyTopic.contains(controlKey)){
                        emailToCase.Topic__c = keyTopicMap.get(controlKey);
                    }
                    // Fine Modifica - Riconoscimento Topic in Email-to-Case -- 16-11-2019 FA

                    String foundCode = '';
                    IBM_Code__mdt ibmCodeToUse = new IBM_Code__mdt();
                    
                    /*if(emailToCase.Subject.length()>=5 && ibmCodes.contains(emailToCase.Subject.substring(0,5))){
                        foundCode = emailToCase.Subject.substring(0,5);
                        system.debug('*****firstFourCh  '+emailToCase.Subject.substring(0,5));

                    }else if (emailToCase.Subject.length()>=4 && ibmCodes.contains(emailToCase.Subject.substring(0,4))){
                        foundCode = emailToCase.Subject.substring(0,4);
                        system.debug('*****firstFiveCh  '+emailToCase.Subject.substring(0,4));

                    }*/
                    //Inizio modifica - calcolo lunghezza codice - FA 09-11-2019
                    if(minLengthCode <= maxLengthCode){
                        for(Integer i = maxLengthCode; i >= minLengthCode; i--){
                            if(emailToCase.Subject.length()>=i && ibmCodes.contains(emailToCase.Subject.substring(0,i))){
                                foundCode = emailToCase.Subject.substring(0,i);
                                system.debug('*****first '+i+' char of the email subject containing the code '+emailToCase.Subject.substring(0,i));
                                break;
                            } 
                        }
                    }
                    //Fine modifica - calcolo lunghezza codice - FA 09-11-2019

                    if (!String.isBlank(foundCode)){
                        ibmCodeToUse = ibmCodesMap.get(foundCode);
                        
                        emailToCase.Complaint__c = ibmCodeToUse.Complaint__c;
                        emailToCase.Language__c = ibmCodeToUse.Language__c;
                        emailToCase.Topic__c = ibmCodeToUse.Topic__c;

                        if(!String.isBlank(ibmCodeToUse.Record_Type_DevName__c)){
                            emailToCase.RecordTypeId = rtDevNameId.get(ibmCodeToUse.Record_Type_DevName__c);

                        }
                        
                        if(!String.isBlank(ibmCodeToUse.Queue_Owner__c)){
                            emailToCase.OwnerId = queueIBMCodeRecMap.get(ibmCodeToUse.Queue_Owner__c);    

                        }
                    }
                }
                system.debug('*****dopo assegnazione '+emailToCase);
            }
        }catch (Exception e){
            system.debug('Error: ' + e.getMessage());
    
        }
        
        // Fine modifica - gestione email to case - IBM CODES - 06-11-2019 - FA


        // Inizio modifica - gestione parent case - 21-10-2019 - FA
        List<Allowed_Days_Parent_Case__mdt> daysObj = [SELECT Allowed_Days__c FROM Allowed_Days_Parent_Case__mdt ORDER BY Allowed_Days__c ASC];
        
        Map<Id, List<Id>> mapContIdCase = new Map<Id, List<Id>>();
        Map<Id, Case> mapIdCaseCont = new Map<Id, Case>(); 
        Map<Id, List<Id>> mapAccIdCase = new Map<Id, List<Id>>();
        Map<Id, Case> mapIdCaseAcc = new Map<Id, Case>();             

        Set<Id> accId = new Set<Id>();
        Set<Id> contId = new Set<Id>();
        Set<String> days = new Set<String>();

        for (Case contCase : newCases){

            if(!String.isBlank(contCase.ContactId))
                contId.add(contCase.ContactId);
            else
                accId.add(contCase.AccountId);
        
        }

        for (Allowed_Days_Parent_Case__mdt d : daysObj){
            days.add(d.Allowed_Days__c);

        }
        System.debug('Custom Metadata ---- Allowed Days - Parent Case : '+days);
        
        List<Case> parentCase = [SELECT Id, RecordTypeId, AccountId, ContactId, Topic__c, Sub_Topic__c
                                 FROM Case
                                 WHERE Status != 'Closed'
                                 AND SLA_Days_From_Opening__c IN: days
                                 AND (
                                    (ContactId IN: contId AND ContactId != NULL)
                                    OR (AccountId IN: accId AND AccountId != NULL AND ContactId = NULL)
                                 )
                                 ORDER BY CreatedDate DESC
                                ];

        for (Case mapContCase : parentCase ){

            if(!String.isBlank(mapContCase.ContactId)){
                mapIdCaseCont.put(mapContCase.Id,mapContCase);
                List<Id> temp = mapContIdCase.get(mapContCase.ContactId);
                if (temp == null){
                    mapContIdCase.put(mapContCase.ContactId, new List<Id>{mapContCase.Id});
                } else {
                    temp.add(mapContCase.Id);
                }

            }else{
                mapIdCaseAcc.put(mapContCase.Id,mapContCase);
                List<Id> temp = mapAccIdCase.get(mapContCase.AccountId);
                if (temp == null){
                    mapAccIdCase.put(mapContCase.AccountId, new List<Id>{mapContCase.Id});
                } else {
                    temp.add(mapContCase.Id);
                }

            }
        
        }
        // Fine modifica - gestione parent case - 21-10-2019 - FA

        try{
            
            for(Case currentCase: newCases) {
                System.debug('**** OnBeforeInsert -- new = '+currentCase);
                if(recTypeIds.contains(currentCase.RecordTypeId)) {
                
                    //if(currentCase.Origin != 'Phone' && currentCase.Origin != 'Chat'){ // commentato FA 06-11-2019
                    if(currentCase.Origin != 'Phone' && currentCase.Origin != 'Manual' && currentCase.Origin != 'Manually' && currentCase.Origin != 'Chat' && currentCase.Origin != 'Email'){ 
                        currentCase.Run_Assignment__c=true;
                    }   
                        
                    // Inizio modifica - gestione parent case - 21-10-2019 - FA
                    if(!String.isBlank(currentCase.ContactId)){
                        if(mapContIdCase.containsKey(currentCase.ContactId)){
                            List<Id> idsCaseCont = mapContIdCase.get(currentCase.ContactId);
                            for(Id idCC : idsCaseCont){
                                Case caseCont = mapIdCaseCont.get(idCC);
                                if (caseCont.Id !=currentCase.Id && caseCont.RecordTypeId == currentCase.RecordTypeId && caseCont.Topic__c == currentCase.Topic__c && caseCont.Sub_Topic__c == currentCase.Sub_Topic__c){
                                    currentCase.ParentId = idCC;
                                    break;
                                }
                            }
                        }    

                    }else if(!String.isBlank(currentCase.AccountId)){
                        if(mapAccIdCase.containsKey(currentCase.AccountId)){
                            List<Id> idsCaseAcc = mapAccIdCase.get(currentCase.AccountId);
                            for(Id idCA : idsCaseAcc){
                                Case caseAcc = mapIdCaseAcc.get(idCA);
                                if (caseAcc.Id !=currentCase.Id && caseAcc.RecordTypeId == currentCase.RecordTypeId && caseAcc.Topic__c == currentCase.Topic__c && caseAcc.Sub_Topic__c == currentCase.Sub_Topic__c){
                                    currentCase.ParentId = idCA;
                                    break;
                                }
                            }
                        }
                    }
                    // Fine modifica - gestione parent case - 21-10-2019 - FA
                }
            }
        }
        catch (Exception e){
            system.debug('Error: ' + e.getMessage());
        }
        // Fine Case Management - boolean 'Run Assignment' (Rule) field - 02-10-2019 --- FA
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        Map<Id, Case> oldCasesMap = (Map<Id, Case>) oldItems;
        Map<Id, Case> newCasesMap = (Map<Id, Case>) newItems;
        CaseTriggerHelper.pendingStatusChangedCheck(oldCasesMap, newCasesMap);
        CaseTriggerHelper.duplicateAccountsCheck(oldCasesMap, newCasesMap);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems){}
    public void AfterInsert(Map<Id, SObject> newItems){
        Map<Id, Case> newCasesMap = (Map<Id, Case>) newItems;
        CaseTriggerHelper.forceTriggerAssigmentRules(newCasesMap);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        List<Case> oldCases = (List<Case>) oldItems.values();
        List<Case> newCases = (List<Case>) newItems.values();
        Map<Id, Case> oldCasesMap = (Map<Id, Case>) oldItems;
        Map<Id, Case> newCasesMap = (Map<Id, Case>) newItems;
        
        List<RecordType> recTypeCaseMgmt = [SELECT Id, DeveloperName
                                            FROM RecordType 
                                            WHERE IsActive = true 
                                            AND SobjectType = 'Case' 
                                            AND BusinessProcessId IN (SELECT Id 
                                                                      FROM BusinessProcess 
                                                                      WHERE Name='Case Management - Support Process')
                                           ];
        Set<Id> recTypeIds = new Set<Id>();
        Map<Id, String> recTypeIdDevName = new Map<Id, String>(); //01-11-2019 FA

        for (RecordType recType : recTypeCaseMgmt) {
            recTypeIds.add(recType.Id);
            recTypeIdDevName.put(recType.Id,recType.DeveloperName); //01-11-2019 FA
        }

        Set<Id> caseIds = new Set<Id>();
        List<Case> newCaseList = newCases;

        for (Case c : newCaseList) {
            caseIds.add(c.Id);
        }

        List<Case> caseToUpdate = [SELECT Id,RecordTypeId,RecordType.DeveloperName,Run_Assignment__c,Language__c,Case_Level__c,Forwarded__c,Sub_Topic__c,Topic__c,OwnerId,Owner.Type,Status,Complaint__c,Owner.Name
                                   FROM Case 
                                   WHERE Id IN: caseIds];

        //Inizio modifica 18-10-2019 - FA 
        Set<String> caseLevel = new Set<String>();
        Map<String, Id> queueMap = new Map<String, Id>();

        if(caseToUpdate.size()>0){

            for (Case  cl : caseToUpdate) {
                System.debug('&&&&&&&&&&& cl.Case_Level__c '+cl.Case_Level__c);
                if(cl.Case_Level__c!=null){
                    caseLevel.add(cl.Case_Level__c); 
                }
            }

            if(caseLevel.size()>0){
                
                List<QueueSobject> ownerQueueToAssign = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject WHERE Queue.DeveloperName IN: caseLevel];
                for (QueueSobject gr : ownerQueueToAssign) {
                    queueMap.put(gr.Queue.DeveloperName, gr.Queue.Id); 
                }
                System.debug('&&&&&&&&&&& queueMap '+queueMap);

            }

        }
        //Fine modifica 18-10-2019 - FA  

        try{
            List<Case> updateCases = new List<Case>();
            for(Case currentCase: caseToUpdate) {

                Boolean updateCheck = false;
                Case oldCase = oldCasesMap.get(currentCase.Id);
                System.debug('**** OnAfterUpdate -- old = '+oldCase);
                System.debug('**** OnAfterUpdate -- new = '+currentCase);

                if(recTypeIds.contains(currentCase.RecordTypeId)){
                    
                    String ownerType = currentCase.Owner.Type; //01-11-2019 FA

                    //Inizio modifica 18-10-2019 - FA
                        /*if (currentCase.RecordTypeId != oldCase.RecordTypeId
                         || currentCase.Language__c != oldCase.Language__c 
                         || currentCase.Topic__c != oldCase.Topic__c
                         || currentCase.Case_Level__c != oldCase.Case_Level__c 
                         || (currentCase.Sub_Topic__c != oldCase.Sub_Topic__c && currentCase.RecordType.DeveloperName == 'Contract_Management')
                         ){
                        currentCase.Status = 'Escalated';
                        currentCase.Run_Assignment__c=true;
                        updateCheck=true;
                    }*/

                    //Inizio - Run Assignment Rule da 'Other' ad un altro del case management - 01-11-2019 FA
                    if(currentCase.RecordTypeId != oldCase.RecordTypeId && recTypeIds.contains(oldCase.RecordTypeId)){
                        String devNameRT = recTypeIdDevName.get(oldCase.RecordTypeId);
                        if(devNameRT=='Other' && currentCase.Status != 'New'){
                            currentCase.Status = 'Escalated';
                            //currentCase.Run_Assignment__c = true; // FA 20-11-2019
                            updateCheck=true;
                        }
                    }
                    //Fine - Run Assignment Rule da 'Other' ad un altro del case management - 01-11-2019 FA

                    //if(currentCase.Case_Level__c != oldCase.Case_Level__c){ //FA 02-12-2019
                    if(currentCase.Forwarded__c == 'Yes'){ //FA 02-12-2019                        
                        /*if(currentCase.Case_Level__c == null){
                            currentCase.Case_Level__c = oldCase.Case_Level__c;

                        }else{*/
                        currentCase.OwnerId = queueMap.get(currentCase.Case_Level__c);
                        currentCase.Status = 'Escalated';
                        currentCase.Forwarded__c = 'No'; //FA 02-12-2019 
                        ownerType = 'Queue'; //01-11-2019 FA
                        if(currentCase.Case_Level__c == 'Complaint_Management'){
                            currentCase.Complaint__c = true;
                        }
                        //}
                        updateCheck=true;
                    }
                    //Fine modifica 18-10-2019 - FA

                    // Inizio modifica status escalated - 02-12-2019 FA
                    else if(currentCase.OwnerId != oldCase.OwnerId && ownerType == 'Queue' && currentCase.Status != 'New'){
                        currentCase.Status = 'Escalated';
                        if(currentCase.Owner.Name == 'Complaint Management'){
                            currentCase.Complaint__c = true;
                        }
                        updateCheck=true;
                    }
                    // FIne modifica status escalated - 02-12-2019 FA

                    //if(currentCase.OwnerId != oldCase.OwnerId && currentCase.Owner.Type == 'User'){ //commentato --- 01-11-2019 FA
                    if(currentCase.OwnerId != oldCase.OwnerId && ownerType == 'User'){ 
                        Datetime nowDateTime = System.now();
                        currentCase.SLA_Date_Time_Taken_In_Charge__c= nowDateTime;
                        currentCase.Status = 'In Progress';
                        updateCheck=true;
                    }

                }

                if(updateCheck)
                    updateCases.add(currentCase);
            }
            update updateCases;
        
        }
        catch (Exception e){
            system.debug('Error: ' + e.getMessage());
        }
        // Fine Case Management - boolean 'Run Assignment' (Rule) field - 02-10-2019 --- FA
    }
    public void AfterDelete(Map<Id, SObject> oldItems){}
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}