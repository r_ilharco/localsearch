/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class Test_CategoryLocationTrigger {
	
    @testSetup
    public static void setupData(){
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;
        
        Category_Location__c location = new Category_Location__c();
        location.Name = 'Test Location Name';
        location.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId();
        location.Location_de__c = 'Location DE';
        location.Location_en__c = 'Location EN';
        location.Location_fr__c = 'Location FR';
        location.Location_it__c = 'Location IT';
        insert location;
        
        Category_Location__c relation = new Category_Location__c();
        relation.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId();
    	relation.Category_Location__c = location.Id;
        relation.mainLocation__c = true;
        relation.Place__c = place.Id;
        insert relation;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_categoryLocationTrigger(){
        
        Id locationRTId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId();
		
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        List<Category_Location__c> location = [SELECT Id FROM Category_Location__c WHERE RecordTypeId = :locationRTId LIMIT 1];
        
        Category_Location__c relation = new Category_Location__c();
        relation.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId();
    	relation.Category_Location__c = location[0].Id;
        relation.Place__c = place.Id;
        insert relation;
        
		relation.mainLocation__c = true;
        update relation;

        delete relation;
    }
}