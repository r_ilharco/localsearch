@isTest
public class Test_Phase1ContractAlignment {
	
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        insert childrenQLs;
        
        Contract contract = generateContract(quote, true);
        insert contract;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        SBQQ__Subscription__c fatherSub = generateFatherSubscription(contract, fatherQuoteLine);
        insert fatherSub;
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        update fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubscriptions = generateChildrenSubscriptions(contract, childrenQLs, fatherSub.Id);
        insert childrenSubscriptions;
        for(SBQQ__Subscription__c currentSub : childrenSubscriptions){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        }
        update childrenSubscriptions;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;

    }
    
    @isTest
    public static void test_batchTest(){
        Test.startTest();
        Phase1ContractAlignment_Sched dab = new Phase1ContractAlignment_Sched();
      	dab.execute(null);
        Test.stopTest();
		      
        Map<Id, Contract> contracts = new Map<Id, Contract>([SELECT Id FROM Contract]);
        
        List<Order> activationOrders = [SELECT Id, Type, Custom_Type__c, Status, SBQQ__Contracted__c FROM Order WHERE Contract__c IN :contracts.keySet()];
        
        for(Order currentOrder : activationOrders){
            System.assertEquals(ConstantsUtil.ORDER_TYPE_NEW, currentOrder.Type);
            System.assertEquals(ConstantsUtil.ORDER_TYPE_ACTIVATION, currentOrder.Custom_Type__c);
            System.assertEquals(ConstantsUtil.ORDER_STATUS_ACTIVATED, currentOrder.Status);
            System.assertEquals(true, currentOrder.SBQQ__Contracted__c);
        }
    }
    
    public static Contract generateContract(SBQQ__Quote__c quote, Boolean isEvergreen){
        Contract contract = new Contract();
        contract.AccountId = quote.SBQQ__Account__c;
        contract.Phase1Source__c = true;
        contract.StartDate = Date.today();
        //contract.EndDate = Date.today().addMonths(12);
        contract.SBQQ__Evergreen__c = isEvergreen;
		contract.SBQQ__Opportunity__c = quote.SBQQ__Opportunity2__c;
        contract.SBQQ__Quote__c = quote.Id;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_DRAFT;
        return contract;
    }
	
    public static SBQQ__Subscription__c generateFatherSubscription(Contract contract, SBQQ__QuoteLine__c fatherQuoteLine){
        SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
        sub.SBQQ__Product__c = fatherQuoteLine.SBQQ__Product__c;
        sub.SBQQ__SubscriptionStartDate__c = Date.today();
        sub.End_Date__c = Date.today().addMonths(12);
        sub.SBQQ__Account__c = contract.AccountId;
        sub.SBQQ__Contract__c = contract.Id;
        sub.SBQQ__ListPrice__c = fatherQuoteLine.SBQQ__ListPrice__c;
        sub.SBQQ__Quantity__c = fatherQuoteLine.SBQQ__Quantity__c;
        sub.SBQQ__QuoteLine__c = fatherQuoteLine.Id;
        sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT;
        return sub;
    }
    
    public static List<SBQQ__Subscription__c> generateChildrenSubscriptions(Contract contract, List<SBQQ__QuoteLine__c> qls, String requiredBy){
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c ql : qls){
            SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
            sub.SBQQ__Product__c = ql.SBQQ__Product__c;
            sub.SBQQ__SubscriptionStartDate__c = Date.today();
            sub.End_Date__c = Date.today().addMonths(12);
            sub.SBQQ__Account__c = contract.AccountId;
            sub.SBQQ__Contract__c = contract.Id;
            sub.SBQQ__ListPrice__c = ql.SBQQ__ListPrice__c;
            sub.SBQQ__Quantity__c = ql.SBQQ__Quantity__c;
            sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT;
            sub.SBQQ__RequiredById__c = requiredBy;
            sub.RequiredBy__c = requiredBy;
            sub.SBQQ__QuoteLine__c = ql.Id;
            sub.PricebookEntryId__c = ql.SBQQ__PricebookEntryId__c;
            subs.add(sub);
        }
        return subs;
    }
    
}