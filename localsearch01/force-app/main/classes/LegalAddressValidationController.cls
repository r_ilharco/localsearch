/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date		   : 28-05-2019
 * Sprint      : 2
 * Work item   : #90 #91 #92 #93
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
                #1: SF2-264 - Legal Address Validation - gcasola - 07-19-2019 - Address_Validated changed Type from Checkbox to Picklist
                #2: SF2-253 - Validate Account and Billing Address on Quote Creation
                    SF2-264 - Legal Address Validation
                    gcasola 07-22-2019
                #3: SF2-218, Wave C, Sprint 6, Logging - Error Handling - 07-24-2019 gcasola
				#4: SF2-772 - Address Validation Review
 */




public without sharing class LegalAddressValidationController {

    @AuraEnabled
    public static String validateRecordLegalAddress(Id recordId){
        String returnMessage = '';
        if(!SRV_IntegrationOutbound.getSkipAddressValidationFlag(recordId)){
            SRV_IntegrationOutbound.AddressValidationResult avResult = SRV_IntegrationOutbound.syncValidateAddress(recordId);
            if(avResult != NULL)
            {
                returnMessage = avResult.result;
                if(ConstantsUtil.ADDRESS_VALIDATION_KO.equalsIgnoreCase(avResult.result) && String.isNotBlank(avResult.error.error_code)){
                    returnMessage = avResult.error.error_code;
                }
            }
        }else
            returnMessage = 'SKIPPED';
            
        System.debug('********** response: '+returnMessage);

        return returnMessage;
    }
}