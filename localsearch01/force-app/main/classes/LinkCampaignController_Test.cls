@isTest
public class LinkCampaignController_Test {
	
    
    
    @testSetup static void setup(){
  
        AccountTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;

        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
		Opportunity opportunity = Test_DataFactory.generateOpportunity('TestOpportunity_01', account.Id);
        insert opportunity;
        Test_Billing.generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        system.debug('prod :: '+prod);
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c where SBQQ__Opportunity2__c =:opportunity.id LIMIT 1];
        system.debug('q.id '+q.Id);
        QuoteLineTriggerHandler.disableTrigger = true;
        Test_Billing.generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        
        List<SBQQ__QuoteLine__c> qls = [SELECT Id,Promotion_Applied__c,Promotion_Type__c, SBQQ__Discount__c,SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        system.debug('qls :: '+qls);
        for(SBQQ__QuoteLine__c ql : qls){
           ql.Promotion_Applied__c = true;
           ql.Promotion_Type__c = ConstantsUtil.FULL_TERM_DISCOUNT;
           ql.SBQQ__Discount__c = 50;  
        }
        update qls;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        
        
       	Campaign camp =  Test_DataFactory.createCampaign();
		camp.Sales_Channel__c ='DMC;Telesales;Digital Agency';
        insert camp;
        
        AccountTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;

    }    
    
    @isTest
    public static void getActiveCampaingsByType_test(){

        test.startTest();
        List<User> usersTeleSales = Test_DataFactory.createUsers(ConstantsUtil.TELESALESPROFILE_LABEL,1);
         usersTeleSales[0].IsActive = true;
        insert usersTeleSales;
        List<User> userTelesales = [Select id, Profile.Name  from user where Profile.Name = :ConstantsUtil.TELESALESPROFILE_LABEL and IsActive = true Limit 1];
        system.runAs(userTelesales[0]){
             LinkCampaignController.getActiveCampaingsByType([Select id from Campaign Limit 1].Id,[select Id from Opportunity where name= 'TestOpportunity_01'].id);
        }
        test.stopTest(); 
  
    }
    
    
     @isTest
    public static void getActiveCampaingsByTypeSales_test(){
       test.startTest();
        List<User> usersSales = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL,1);
         usersSales[0].IsActive = true;
        insert usersSales;
        List<User> userSales = [Select id, Profile.Name  from user where Profile.Name = :ConstantsUtil.SALESPROFILE_LABEL and IsActive = true Limit 1];
        system.runAs(userSales[0]){
            
             LinkCampaignController.getActiveCampaingsByType([Select id from Campaign Limit 1].Id,[select Id from Opportunity where name= 'TestOpportunity_01'].id);
        }
     
        test.stopTest();        
    }
    @isTest
    public static void getActiveCampaingsByTypeDigital_test(){
        test.startTest();
        List<User> usersDigitalAgency = Test_DataFactory.createUsers(ConstantsUtil.DIGITALAGENCYPROFILE_LABEL,1);
        usersDigitalAgency[0].IsActive = true;
        insert usersDigitalAgency;
        List<User> userDigital = [Select id, Profile.Name  from user where Profile.Name = :ConstantsUtil.DIGITALAGENCYPROFILE_LABEL and IsActive = true Limit 1];
        system.runAs(userDigital[0]){
            
             LinkCampaignController.getActiveCampaingsByType([Select id from Campaign Limit 1].Id,[select Id from Opportunity where name= 'TestOpportunity_01'].id);
        }
     
        test.stopTest();        
    }
     @isTest
    public static void getActiveCampaingsByTypeAdmn_test(){
        test.startTest();
         SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c where SBQQ__Opportunity2__c in (Select id from Opportunity where Name ='TestOpportunity_01') Limit 1];
      	LinkCampaignController.getActiveCampaingsByType([Select id from Campaign Limit 1].Id,[select Id from Opportunity where name= 'TestOpportunity_01'].id);
        LinkCampaignController.removeQLPromotions(q.Id);
        test.stopTest();        
    }
    
      @isTest
    public static void getActiveCampaingsByTypeuserCustumerCare_test(){
        test.startTest();
             LinkCampaignController.checkPublicGroupMembership(UserInfo.getUserId());
        test.stopTest();   
            }
   
    
    
    
}