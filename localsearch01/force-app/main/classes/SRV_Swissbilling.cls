public with sharing class SRV_Swissbilling {
    public static HttpResponse getInvoiceStatusByIdCallout(String correlationId){
        Map<String,string> parameters = new Map<string,String>();
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2 ;
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else{
            c2 = Mashery_Setting__c.getOrgDefaults();
        }
        //invoices/{correlationId}/status
        String methodName = ConstantsUtil.SWISSBILLING_GET_STATUS_START + correlationId + ConstantsUtil.SWISSBILLING_GET_STATUS_END;
        String endpoint = c2.Endpoint__c + methodName;
        Decimal cached_millisecs;
        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        Boolean IsNewToken=False;
        if(!Test.isRunningTest() && (String.isBlank(c2.Token__c) || ( (cached_millisecs/1000).round() >= (Integer) c2.Expires__c))){
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        } else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        System.debug('token '+token.access_token);             
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        System.debug('endpoint '+endpoint);
        //request.setEndpoint(endpoint);
        request.setEndpoint(endpoint);
        request.setHeader('X-LS-Tracing-CorrelationId', correlationId);
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setHeader('charset', 'UTF-8');
        request.setMethod('GET');
        request.setTimeout(30000);
        HttpResponse response = http.send(request);
        system.debug('responseForgetInvoiceStatusByIdCallout: ' + response);
        return response;
    }

    public static HttpResponse sendInvoices(String requestJson, String correlationId, String methodName){
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2 ;
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else{
            c2 = Mashery_Setting__c.getOrgDefaults();
        }
        Decimal cached_millisecs;
        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        system.debug('c2: ' + c2);
        if(!Test.isRunningTest()){
            Boolean IsNewToken=False;
            if( String.isBlank(c2.Token__c) || ( (cached_millisecs/1000).round() >= (Integer) c2.Expires__c)){
                IsNewToken=True;
                token = UtilityToken.refreshToken();
            } else{
                token.access_token= c2.Token__c;
                token.token_type= c2.Token_Type__c;
                token.expires_in= (Integer)c2.Expires__c;           
            }
        }else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;  
        }
        Integration_Config__mdt integrationConfig = [SELECT Method__c FROM Integration_Config__mdt WHERE MasterLabel = :ConstantsUtil.SwissbillingTransfer];
        String endpoint = c2.Endpoint__c + integrationConfig.Method__c;
        System.debug('token '+token.access_token);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        system.debug('endpoint: ' + endpoint);
        request.setBody(requestJson);
        System.debug('REQUEST BODY: ************** '+requestJson);
        request.setHeader('Content-Type','application/json');
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('POST');
            /*HttpRequest req = new HttpRequest();
            req.setEndpoint(BillingHelper.billingEndpoint + '/jobs/transfer');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, correlationId);
            req.setHeader(ConstantsUtil.TRACE_HEAD_BP_NAME, 'SendToSwissbilling');
            req.setHeader(ConstantsUtil.TRACE_HEAD_INITIATOR, 'Salesforce');
            req.setHeader(ConstantsUtil.TRACE_HEAD_CALLING_APP, 'Billing');
            req.setHeader(ConstantsUtil.TRACE_HEAD_BO_NAME, 'Invoice__c');
            req.setHeader(ConstantsUtil.TRACE_HEAD_BO_ID, invoice.Id);             
            req.setBody(documentJson);
            req.setTimeout(30000);*/
        request.setTimeout(60000);
        HttpResponse response = http.send(request);
        system.debug('response: ' + response);
        return response;
    }
}