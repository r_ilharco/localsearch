global class Phase1ContractAlignment_Sched implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'Phase1ContractAlignment%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('Phase1ContractAlignment0')){
                Phase1ContractAlignment b0 = new Phase1ContractAlignment(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('Phase1ContractAlignment0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment1')){ 
                Phase1ContractAlignment b1 = new Phase1ContractAlignment(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment2')){
                Phase1ContractAlignment b2 = new Phase1ContractAlignment(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment3')){
                Phase1ContractAlignment b3 = new Phase1ContractAlignment(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment4')){
                Phase1ContractAlignment b4 = new Phase1ContractAlignment(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment5')){
                Phase1ContractAlignment b5 = new Phase1ContractAlignment(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment6')){
                Phase1ContractAlignment b6 = new Phase1ContractAlignment(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment7')){
                Phase1ContractAlignment b7 = new Phase1ContractAlignment(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment8')){
                Phase1ContractAlignment b8 = new Phase1ContractAlignment(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('Phase1ContractAlignment9')){
                Phase1ContractAlignment b9 = new Phase1ContractAlignment(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('Phase1ContractAlignment9', strSchedule,b9);
            }
        }
    }
}