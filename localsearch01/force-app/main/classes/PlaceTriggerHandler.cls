/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Sara Dubbioso <gcasola@deloitte.it>
 * Date		   : 23-05-2019
 * Sprint      : 2
 * Work item   : 5
 * Testclass   :
 * Package     : 
 * Description : PlaceTriggerHandler
 * Changelog   : 
 */

public class PlaceTriggerHandler implements ITriggerHandler{
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public static boolean IsFromBachJob ;
    public static boolean isFromUploadAPI=false;
    
    public static String triggerName {get;set;}
    public static Boolean disableTrigger {
        get {
            if(disableTrigger == null) {
                Bypass_Triggers__c orgwideBypass = Bypass_Triggers__c.getOrgDefaults();
                if(orgwideBypass.Trigger_Name__c != NULL) {
                    List<String> allTrgNames = orgwideBypass.Trigger_Name__c.split(',');
                    if(allTrgNames.contains(triggerName)) return true;
                } else {
                    Bypass_Triggers__c profilewideBypass = Bypass_Triggers__c.getInstance(Userinfo.getProfileId());
                    if(profilewideBypass.Trigger_Name__c != NULL) {
                        List<String> allTrgNames = profilewideBypass.Trigger_Name__c.split(',');
                        if(allTrgNames.contains(triggerName)) return true;
                    } else {
                        Bypass_Triggers__c userwideBypass = Bypass_Triggers__c.getInstance(Userinfo.getUserId());
                        if(userwideBypass.Trigger_Name__c != NULL) {
                            List<String> allTrgNames = userwideBypass.Trigger_Name__c.split(',');
                            if(allTrgNames.contains(triggerName)) return true;
                        }
                    }
                }
                return false;
            } else {
                return disableTrigger;
            }
        } set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
    
    public PlaceTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
            
    public void BeforeInsert(List<SObject> newItems)
    {
        system.debug('Place Trigger On Before Insert');
    }
    public void AfterInsert(Map<Id, SObject> newItems)
    {
        system.debug('Place Trigger On After Insert');
        ATL_PlaceTriggerHelper.managePlace_Lead ((List<Place__c>)newItems.values(),null);
        ATL_PlaceTriggerHelper.placeCreationNotification((List<Place__c>)newItems.values());

    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
    {
        system.debug('Place Trigger On After Update ');
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, Object> oldItems)
    {
        system.debug('Place Trigger On Before Update ');
        ATL_PlaceTriggerHelper.checkPlace( (Map<Id, Place__c>)newItems, (Map<Id, Place__c>)oldItems );
        ATL_PlaceTriggerHelper.checkApproval ((Map<Id, Place__c>)newItems, (Map<Id, Place__c>)oldItems );

    }

    public void BeforeDelete(Map<Id, SObject> oldItems) 
    {
        system.debug('Place Trigger On Before Delete ');
    }
    public void AfterDelete(Map<Id, SObject> oldItems)
    {
        system.debug('Place Trigger On After Delete ');
    }
    public void AfterUndelete(Map<Id, SObject> oldItems) 
    {
        system.debug('Place Trigger On After Undelete ');
    }

    @future 
    public static void OnAfterUpdateAsync(Set<ID> newPlaceIDs)
    {

    }      
    public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext
    {
        get{ return !IsTriggerContext;}
    }
}