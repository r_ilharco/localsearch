/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* SFRO-389
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created		  2020-06-05
* @systemLayer    Invocation
* @TestClass 	  Test_AccountUserTerritoryAssignmentBatch
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-1184
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-06-22          
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reassign owner if territory changes (SPIII 1214)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-07-09  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        When a DMC is reallocated from a territory to another one, 
				  then the owner of all his accounts is the SMA of the Account territory (SPIII 2045)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-07-27  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class AccountUserTerritoryAssignmentBatch implements Database.batchable<sObject>, Database.Stateful, Schedulable {
    Integer minutes = Integer.valueOf(Label.AccountUserTerritoryAssignmentBatch_TimeFrame_in_Minutes);
    Map<Id, UserTerritory2Association> ut2aMap;
    Map<Id,UserTerritory2Association> todayInsertedUT2AMap = new Map<Id,UserTerritory2Association>();
    Map<Id, Map<String, Id>> usersByTerritoryAndRoleMap = new Map<Id, Map<String, Id>>();
    Map<Id,List<Id>> mapTerrDMCs = new Map<Id,List<Id>>();
    List<String> userCodes = new List<String>{ConstantsUtil.ROLE_IN_TERRITORY_SDI,ConstantsUtil.ROLE_IN_TERRITORY_SMA,ConstantsUtil.ROLE_IN_TERRITORY_SMD,ConstantsUtil.ROLE_IN_TERRITORY_DMC};
    Map<Id,ObjectTerritory2Association> ot2aMap;
    Map<Id, Id> objToOT2AMap = new Map<Id, Id>();
    Map<Id,Territory2> territoriesWithoutSDIMap;
    Map<Id,Territory2> territoriesWithoutSMAMap;
    Map<Id,Territory2> territoriesWithoutSMDMap;
    Map<Id,Territory2> territoriesWithoutDMCMap;
    Map<Id,Territory2> deletedUserAssociationsTerritories;
    Map<Id,UserTerritory2Association> replacingUserUT2AMap = new Map<Id,UserTerritory2Association>();
    Map<Id, Map<String, Id>> replacingUserMap = new Map<Id, Map<String, Id>>();
    Map<Integer, Set<Id>> territorySetsByLevelMap = new Map<Integer, Set<Id>>{
        	0 => new Set<Id>(),
            1 => new Set<Id>(),
            2 => new Set<Id>()
   	};
    
    Set<Id> territoryIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    
    private List<Error_Log__c> errorLogs = new List<Error_Log__c>();
    
    public AccountUserTerritoryAssignmentBatch(){}
    
    public AccountUserTerritoryAssignmentBatch(Integer minutes){
        this.minutes = minutes;
    }
    
    global void execute(SchedulableContext sc) {
       AccountUserTerritoryAssignmentBatch accountUserTerritoryAssignmentBatch = new AccountUserTerritoryAssignmentBatch(); 
       Database.executebatch(accountUserTerritoryAssignmentBatch, 2000);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        ut2aMap = new Map<Id, UserTerritory2Association>([
                SELECT Id, UserId, Territory2Id, Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2Id,
                IsActive, RoleInTerritory2, LastModifiedDate, LastModifiedById 
                FROM UserTerritory2Association 
                WHERE IsActive = true 
                AND Territory2.Territory2Model.State = 'Active'
                ORDER BY LastModifiedDate
            ]);
        
        for(UserTerritory2Association ut2a : ut2aMap.values()){
            if(ut2a.LastModifiedDate > System.now().addMinutes(-minutes))
                todayInsertedUT2AMap.put(ut2a.Id, ut2a);
        }
        
        //System.debug('todayInsertedUT2AMap --> ' + JSON.serialize(todayInsertedUT2AMap));
        
        if(todayInsertedUT2AMap != NULL && todayInsertedUT2AMap.size() > 0){
            for(UserTerritory2Association ut2a : todayInsertedUT2AMap.values()){
                if(!usersByTerritoryAndRoleMap.containsKey(ut2a.Territory2Id))
                    usersByTerritoryAndRoleMap.put(ut2a.Territory2Id, new Map<String, Id>());
                
                usersByTerritoryAndRoleMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2,ut2a.UserId);
                
                if(ut2a.Territory2.ParentTerritory2Id == NULL)
                    territorySetsByLevelMap.get(0).add(ut2a.Territory2Id);
                else if(ut2a.Territory2.ParentTerritory2Id != NULL 
                        && ut2a.Territory2.ParentTerritory2.ParentTerritory2Id == NULL)
                    territorySetsByLevelMap.get(1).add(ut2a.Territory2Id);
                else if(ut2a.Territory2.ParentTerritory2Id != NULL 
                        && ut2a.Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                        && ut2a.Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                    territorySetsByLevelMap.get(2).add(ut2a.Territory2Id);
                
                territoryIds.add(ut2a.Territory2Id);
            }
        }
        
        //System.debug('territorySetsByLevelMap(usersByTerritoryAndRoleMap) --> ' + JSON.serialize(territorySetsByLevelMap));
        //System.debug('usersByTerritoryAndRoleMap --> ' + JSON.serialize(usersByTerritoryAndRoleMap));
        
        deletedUserAssociationsTerritories = new Map<Id, Territory2>([
            SELECT Id, DeletedUserAssociations__c
            FROM Territory2 
            WHERE Territory2Model.State = 'Active'
            AND DeletedUserAssociations__c > 0
        ]);
        
        //System.debug('deletedUserAssociationsTerritories --> ' + JSON.serialize(deletedUserAssociationsTerritories));
        
        Set<Id> deletedUserAssociationsTerritoryIds = new Set<Id>();
        
        if(deletedUserAssociationsTerritories != NULL && deletedUserAssociationsTerritories.size() > 0){
            deletedUserAssociationsTerritoryIds = deletedUserAssociationsTerritories.keySet();
            
            if(ut2aMap != NULL && ut2aMap.size() > 0){
                for(UserTerritory2Association ut2a : ut2aMap.values()){
                    if(deletedUserAssociationsTerritories.containsKey(ut2a.Territory2Id)){
                        //System.debug('ut2a --> ' + JSON.serialize(ut2a));
                        if(!replacingUserMap.containsKey(ut2a.Territory2Id))
                            replacingUserMap.put(ut2a.Territory2Id, new Map<String,Id>());
                        
                        replacingUserMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2,ut2a.UserId);
                        
                        if(ut2a.Territory2.ParentTerritory2Id == NULL)
                            territorySetsByLevelMap.get(0).add(ut2a.Territory2Id);
                        else if(ut2a.Territory2.ParentTerritory2Id != NULL 
                                && ut2a.Territory2.ParentTerritory2.ParentTerritory2Id == NULL)
                            territorySetsByLevelMap.get(1).add(ut2a.Territory2Id);
                        else if(ut2a.Territory2.ParentTerritory2Id != NULL 
                                && ut2a.Territory2.ParentTerritory2.ParentTerritory2Id != NULL
                                && ut2a.Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                            territorySetsByLevelMap.get(2).add(ut2a.Territory2Id);
                        
                        territoryIds.add(ut2a.Territory2Id);
                    }
                }
            }
        }
        
       // System.debug('territorySetsByLevelMap(deletedUserAssociationsTerritories) --> ' + JSON.serialize(territorySetsByLevelMap));
        //System.debug('replacingUserMap --> ' + JSON.serialize(replacingUserMap));
        
        territoriesWithoutSDIMap = new Map<Id, Territory2>([
            SELECT Id 
            FROM Territory2 
            WHERE Id NOT IN (SELECT Territory2Id 
                             FROM UserTerritory2Association 
                             WHERE RoleInTerritory2 = :ConstantsUtil.ROLE_IN_TERRITORY_SDI) 
            AND ParentTerritory2Id = NULL 
            AND Territory2Model.State = 'Active'
            AND ID IN :deletedUserAssociationsTerritoryIds
        ]);
        
        //System.debug('territoriesWithoutSDIMap --> ' + JSON.serialize(territoriesWithoutSDIMap));
        
        territoriesWithoutSMAMap = new Map<Id, Territory2>([
            SELECT Id 
            FROM Territory2 
            WHERE Id NOT IN (SELECT Territory2Id 
                             FROM UserTerritory2Association 
                             WHERE RoleInTerritory2 = :ConstantsUtil.ROLE_IN_TERRITORY_SMA) 
            AND ParentTerritory2.ParentTerritory2Id = NULL
            AND ParentTerritory2Id != NULL
            AND Territory2Model.State = 'Active'
            AND ID IN :deletedUserAssociationsTerritoryIds
        ]);
        
        //System.debug('territoriesWithoutSMAMap --> ' + JSON.serialize(territoriesWithoutSMAMap));
        
        territoriesWithoutSMDMap = new Map<Id, Territory2>([
            SELECT Id 
            FROM Territory2 
            WHERE Id NOT IN (SELECT Territory2Id 
                             FROM UserTerritory2Association 
                             WHERE RoleInTerritory2 = :ConstantsUtil.ROLE_IN_TERRITORY_SMD) 
            AND ParentTerritory2.ParentTerritory2Id = NULL
            AND ParentTerritory2Id != NULL
            AND Territory2Model.State = 'Active'
            AND ID IN :deletedUserAssociationsTerritoryIds
        ]);
        
        //System.debug('territoriesWithoutSMDMap --> ' + JSON.serialize(territoriesWithoutSMDMap));
        
        territoriesWithoutDMCMap = new Map<Id, Territory2>([
            SELECT Id 
            FROM Territory2 
            WHERE Id NOT IN (SELECT Territory2Id 
                             FROM UserTerritory2Association 
                             WHERE RoleInTerritory2 = :ConstantsUtil.ROLE_IN_TERRITORY_DMC) 
            //AND ParentTerritory2.ParentTerritory2.ParentTerritory2Id = NULL
            //AND ParentTerritory2.ParentTerritory2Id != NULL // SPIII-1184
            AND ParentTerritory2.ParentTerritory2Id = NULL
            AND ParentTerritory2Id != NULL
            AND Territory2Model.State = 'Active'
            AND ID IN :deletedUserAssociationsTerritoryIds
        ]);
        
        //System.debug('territoriesWithoutDMCMap --> ' + JSON.serialize(territoriesWithoutDMCMap));
        
        if(territoriesWithoutSDIMap != NULL && territoriesWithoutSDIMap.size() > 0){
            territorySetsByLevelMap.get(0).addAll(territoriesWithoutSDIMap.keySet());
            territoryIds.addAll(territoriesWithoutSDIMap.keySet());
        }
        
        if(territoriesWithoutSMAMap != NULL && territoriesWithoutSMAMap.size() > 0){
            territorySetsByLevelMap.get(1).addAll(territoriesWithoutSMAMap.keySet());
            territoryIds.addAll(territoriesWithoutSMAMap.keySet());
        }
        
        if(territoriesWithoutSMDMap != NULL && territoriesWithoutSMDMap.size() > 0){
            territorySetsByLevelMap.get(1).addAll(territoriesWithoutSMDMap.keySet());
            territoryIds.addAll(territoriesWithoutSMDMap.keySet());
        }
        
        if(territoriesWithoutDMCMap != NULL && territoriesWithoutDMCMap.size() > 0){
            //territorySetsByLevelMap.get(2).addAll(territoriesWithoutDMCMap.keySet());
            territorySetsByLevelMap.get(1).addAll(territoriesWithoutDMCMap.keySet()); // SPIII-1184
            territoryIds.addAll(territoriesWithoutDMCMap.keySet());
            
            
        }
        for(UserTerritory2Association  utaDmc : [select id, Territory2Id, UserId from UserTerritory2Association 
                                                 where RoleInTerritory2 =: ConstantsUtil.ROLE_IN_TERRITORY_DMC
                                                 and Territory2Id IN :deletedUserAssociationsTerritoryIds ]){
                                                     if(!mapTerrDMCs.containsKey(utaDmc.Territory2Id)){
                                                         mapTerrDMCs.put(utaDmc.Territory2Id, new List<Id>{utaDmc.UserId});                                            
                                                     }
                                                     else {
                                                         mapTerrDMCs.get(utaDmc.Territory2Id).add(utaDmc.UserId);  
                                                     }
                                                 }
        //System.debug('mapTerrDMCs --> ' + mapTerrDMCs);
        //System.debug('territorySetsByLevelMap(territoriesWithout*Map) --> ' + JSON.serialize(territorySetsByLevelMap));
        //System.debug('territorySetsByLevelMap --> ' + JSON.serialize(territorySetsByLevelMap));
        
        Set<Id> firstLevelTerritorySet = territorySetsByLevelMap.get(0);
        Set<Id> secondLevelTerritorySet = territorySetsByLevelMap.get(1);
        Set<Id> thirdLevelTerritorySet = territorySetsByLevelMap.get(2);
        String TERRITORY_OBJECT_TYPE = ConstantsUtil.TERRITORY_OBJECT_TYPE;
        String statusAcc = ConstantsUtil.ACCOUNT_STATUS_CLOSED; 
        
        String query = 'SELECT RegionalDirector__c, RegionalLeader__c, Sales_manager_substitute__c,OwnerId,Owner.Code__c, ' +
            			'(SELECT ObjectId,' + 
                        ' Territory2Id,' + 
                        ' Territory2.ParentTerritory2Id,' + 
                        ' Territory2.ParentTerritory2.ParentTerritory2Id,' + 
                        ' Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2Id' + 
                        ' FROM ObjectTerritory2Associations)' +
            		   ' FROM Account WHERE Id IN ' + 
                           '(SELECT ObjectId' +
                            ' FROM ObjectTerritory2Association' + 
                            ' WHERE ' +
            				' (' + 
                            ' 	(' + 
                            ' 		Territory2Id IN :thirdLevelTerritorySet' + 
                            ' 		OR Territory2.ParentTerritory2Id IN :secondLevelTerritorySet' + 
                            ' 		OR Territory2.ParentTerritory2.ParentTerritory2id IN :firstLevelTerritorySet' + 
                            ' 	)' + 
                            ' OR' + 
                            ' 	(' +
            				'		Territory2Id IN :secondLevelTerritorySet' + 
                            ' 		OR Territory2.ParentTerritory2Id IN :firstLevelTerritorySet' +
            				' 	)' + 
                            ' OR Territory2Id IN :firstLevelTerritorySet' + 
                            ' ) ' + 
                            ' AND Object.Type = :TERRITORY_OBJECT_TYPE)' +
            				' AND Status__c != :statusAcc';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Account> scope ){
        Map<Id, Account> accountsMap = new Map<Id, Account>(scope);
        Set<Id> accountIdsToUpdateSet = new Set<Id>();
        List<Account> accountsToUpdateList = new List<Account>();
        
        //System.debug('accountsMap--> ' + JSON.serialize(accountsMap));
        //System.debug('usersByTerritoryAndRoleMap--> ' + JSON.serialize(usersByTerritoryAndRoleMap));
        //System.debug('replacingUserMap--> ' + JSON.serialize(replacingUserMap));
        
        for(Account acc : accountsMap.values()){
            ObjectTerritory2Association ot2a = acc.ObjectTerritory2Associations[0];
            
            Boolean firstTerritoryLevel = ot2a.Territory2.ParentTerritory2Id == NULL;
            
            Boolean secondTerritoryLevel = ot2a.Territory2.ParentTerritory2Id != NULL 
                && ot2a.Territory2.ParentTerritory2.ParentTerritory2Id == NULL;
            
        /*   Boolean thirdTerritoryLevel = ot2a.Territory2.ParentTerritory2Id != NULL 
                && ot2a.Territory2.ParentTerritory2.ParentTerritory2Id != NULL 
                && ot2a.Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL;*/
            
            //System.debug('acc--> ' + JSON.serialize(acc));
            //System.debug('ot2a--> ' + JSON.serialize(ot2a));
            
            if(firstTerritoryLevel){
                //THE ACCOUNT IS ASSOCIATED TO THE FIRST TERRITORY LEVEL
                if(territoriesWithoutSDIMap.containsKey(ot2a.Territory2Id) && acc.RegionalDirector__c != NULL){
                    acc.RegionalDirector__c = NULL;    
                    accountIdsToUpdateSet.add(acc.Id);
                }else if((!usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) || 
                          (usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) 
                           && !usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI))) 
                         && (replacingUserMap.containsKey(ot2a.Territory2Id) && replacingUserMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                             && replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI) != acc.RegionalDirector__c)
                        ){
                            acc.RegionalDirector__c = replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                            accountIdsToUpdateSet.add(acc.Id);
                        }
                
                if(usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI) != acc.RegionalDirector__c){
                       acc.RegionalDirector__c = usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                       accountIdsToUpdateSet.add(acc.Id);
                   }
                
                if(acc.RegionalLeader__c != NULL){
                    if(acc.OwnerId==acc.RegionalLeader__c && acc.RegionalDirector__c!= null){
                        //System.debug('update owner with sdi');
                        acc.OwnerId=acc.RegionalDirector__c;
                    }
                    acc.RegionalLeader__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }
                
                if(acc.Sales_manager_substitute__c != NULL){
                    acc.Sales_manager_substitute__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }
                
              /*if(acc.SalesUser__c != NULL){ //SPIII 1212
                    acc.SalesUser__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }*/
                
            }else if(secondTerritoryLevel){
                //THE ACCOUNT IS ASSOCIATED TO THE SECOND TERRITORY LEVEL
                if(territoriesWithoutSDIMap.containsKey(ot2a.Territory2.ParentTerritory2Id) && acc.RegionalDirector__c != NULL){
                    acc.RegionalDirector__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }else if((!usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2.ParentTerritory2Id) || 
                          (usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2.ParentTerritory2Id) 
                           && !usersByTerritoryAndRoleMap.get(ot2a.Territory2.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI))) 
                         && (replacingUserMap.containsKey(ot2a.Territory2.ParentTerritory2Id)
                             && replacingUserMap.get(ot2a.Territory2.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                             && replacingUserMap.get(ot2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI) != acc.RegionalDirector__c)
                        ){
                            acc.RegionalDirector__c = replacingUserMap.get(ot2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                            accountIdsToUpdateSet.add(acc.Id);
                        }
                
                if(usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2.ParentTerritory2Id)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI) != acc.RegionalDirector__c){
                       acc.RegionalDirector__c = usersByTerritoryAndRoleMap.get(ot2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                       accountIdsToUpdateSet.add(acc.Id);
                   }
                
                if(territoriesWithoutSMAMap.containsKey(ot2a.Territory2Id) && acc.RegionalLeader__c != NULL){
                    if(acc.OwnerId==acc.RegionalLeader__c && acc.RegionalDirector__c!= null){
                        //System.debug('update owner with sdi');
                        acc.OwnerId=acc.RegionalDirector__c;
                    }
                    acc.RegionalLeader__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }else if((!usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) || 
                          (usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) 
                           && !usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA))) 
                         && (replacingUserMap.containsKey(ot2a.Territory2Id) && replacingUserMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)
                             && replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA) != acc.RegionalLeader__c)
                        ){
                            //System.debug('update owner with sma replaced'+acc.OwnerId + 'dir ' +acc.RegionalDirector__c);
                            acc.RegionalLeader__c = replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                            if((acc.OwnerId==acc.RegionalDirector__c ||acc.OwnerId== acc.Sales_manager_substitute__c) && acc.RegionalLeader__c!= null){
                                //System.debug('update owner with sma replaced');
                                acc.OwnerId=acc.RegionalLeader__c;
                            }
                            accountIdsToUpdateSet.add(acc.Id);
                        }
                
                if(usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA) != acc.RegionalLeader__c){
                       //System.debug('update owner with sma '+acc.OwnerId + 'dir ' +acc.RegionalDirector__c);
                       acc.RegionalLeader__c = usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                       if((acc.OwnerId==acc.RegionalDirector__c ||acc.OwnerId== acc.Sales_manager_substitute__c) && acc.RegionalLeader__c!= null){
                           //System.debug('update owner with sma ');
                           acc.OwnerId=acc.RegionalLeader__c;
                       }
                       accountIdsToUpdateSet.add(acc.Id);
                   }
                
                if(territoriesWithoutSMDMap.containsKey(ot2a.Territory2Id) && territoriesWithoutSMDMap.get(ot2a.Territory2Id) != NULL){
                    acc.Sales_manager_substitute__c = NULL;
                    accountIdsToUpdateSet.add(acc.Id);
                }else if((!usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) || 
                          (usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id) 
                           && !usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD))) 
                         && (replacingUserMap.containsKey(ot2a.Territory2Id) && replacingUserMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD)
                             && replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD) != acc.Sales_manager_substitute__c)
                        ){
                            acc.Sales_manager_substitute__c = replacingUserMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD);
                            accountIdsToUpdateSet.add(acc.Id);
                        }
                
                if(usersByTerritoryAndRoleMap.containsKey(ot2a.Territory2Id)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD)
                   && usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD) != acc.Sales_manager_substitute__c	
                  ){
                      acc.Sales_manager_substitute__c = usersByTerritoryAndRoleMap.get(ot2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD);
                      accountIdsToUpdateSet.add(acc.Id);
                  }
				//System.debug('ot2a.Territory2Id '+ot2a.Territory2Id);
                //System.debug('acc.OwnerId '+acc.OwnerId);
                //System.debug('acc.Owner.Code__c '+acc.Owner.Code__c);
                //System.debug('territoriesWithoutDMCMap '+territoriesWithoutDMCMap);
                if(!deletedUserAssociationsTerritories.isEmpty()
                   && ((mapTerrDMCs.containsKey(ot2a.Territory2Id) && !mapTerrDMCs.get(ot2a.Territory2Id).contains(acc.OwnerId))|| mapTerrDMCs.isEmpty())
                   && acc.OwnerId != acc.Sales_manager_substitute__c
                   && acc.OwnerId != acc.RegionalLeader__c
                   && acc.OwnerId != acc.RegionalDirector__c
                  //&& !userCodes.contains(acc.Owner.Code__c)
                  ){
                       if(acc.RegionalLeader__c != null )
                       	acc.OwnerId =acc.RegionalLeader__c;
                       else if(acc.Sales_manager_substitute__c != null)
                           acc.OwnerId =acc.Sales_manager_substitute__c;
                       else
                           acc.OwnerId =acc.RegionalDirector__c;
                       accountIdsToUpdateSet.add(acc.Id);
                   }
                
            }
        }
        
        for(Id accountId : accountIdsToUpdateSet)
            accountsToUpdateList.add(accountsMap.get(accountId));
        
        AccountTriggerHandler.disableTrigger = true;
        List<Database.SaveResult> srs = Database.update(accountsToUpdateList, false);
        
        if(srs != NULL && srs.size() > 0){
            for(Database.SaveResult sr : srs){
                if(!sr.isSuccess()){
                    Account acc = (accountsMap.containsKey(sr.getId())?accountsMap.get(sr.getId()):NULL);
                    for(Database.Error err : sr.getErrors()) {
                        String errorMessage = 'The following error has occurred for \'' + sr.getId() + '\'. ' + 
                            err.getStatusCode() + ': ' + err.getMessage() + '. ' +
                            'Account fields that affected this error: ' + err.getFields() + '. ';
                        
                        //System.debug('The following error has occurred for \'' + sr.getId() + '\'.');                    
                        //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        //System.debug('Account fields that have affected this error: ' + err.getFields());
                        
                       	errorLogs.add(ErrorHandler.createLogError('AccountUserTerritoryAssignmentBatch', 'AccountUserTerritoryAssignmentBatch.execute', null, ErrorHandler.ErrorCode.E_DML_FAILED, errorMessage, (acc!=NULL)?JSON.serialize(acc):NULL , null, acc));
                    }
                }
            }
        }
        AccountTriggerHandler.disableTrigger = false;
        
        if(errorLogs != NULL && errorLogs.size() > 0)
            insert errorLogs;
    }
    
    global void finish(Database.BatchableContext info){        
        Map<Id,Territory2> territoriesToUpdateMap = new Map<Id,Territory2>();
        
        if(deletedUserAssociationsTerritories != NULL && deletedUserAssociationsTerritories.size() > 0){
        	territoriesToUpdateMap = new Map<Id,Territory2>([SELECT Id, DeletedUserAssociations__c FROM Territory2 WHERE Id IN :deletedUserAssociationsTerritories.keySet()]);
        
            if(territoriesToUpdateMap != NULL && territoriesToUpdateMap.size() > 0)
                for(Territory2 t : territoriesToUpdateMap.values()){
                    //System.debug('BEFORE [' + t.Id + '] t.DeletedUserAssociations__c --> ' + t.DeletedUserAssociations__c);
                	if(t.DeletedUserAssociations__c > 0 
                       && deletedUserAssociationsTerritories.get(t.Id).DeletedUserAssociations__c > 0 
                       && (t.DeletedUserAssociations__c - deletedUserAssociationsTerritories.get(t.Id).DeletedUserAssociations__c) >= 0){
                           //System.debug('[' + t.Id + '] deletedUserAssociationsTerritories.get(t.Id).DeletedUserAssociations__c) --> ' + deletedUserAssociationsTerritories.get(t.Id).DeletedUserAssociations__c);
                		   t.DeletedUserAssociations__c -= deletedUserAssociationsTerritories.get(t.Id).DeletedUserAssociations__c;
                       }
                    else if(t.DeletedUserAssociations__c == NULL)
                        t.DeletedUserAssociations__c = 0;
                    
                    //System.debug('AFTER [' + t.Id + '] t.DeletedUserAssociations__c --> ' + t.DeletedUserAssociations__c);
                }
            
        	update territoriesToUpdateMap.values();
        }
    }
}