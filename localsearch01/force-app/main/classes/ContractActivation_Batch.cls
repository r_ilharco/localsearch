global class ContractActivation_Batch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts {
    
    private String sSQL=null;
    public ContractActivation_Batch() //String query 
    {
    }
    
    global database.querylocator start(Database.BatchableContext bc)
    { 
        
        Date dt_from = Date.valueOf(System.Today()) - (Integer.valueOf(Label.Contract_job_Prior_days));
        Date dt_to = Date.valueOf(System.Today());
        
        String status=ConstantsUtil.CONTRACT_STATUS_PRODUCTION;    
        // String status1=ConstantsUtil.CONTRACT_STATUS_PRODUCTION;    


        sSQL = 'Select Id, Name, CallId__c, Status, OwnerId, StartDate, EndDate, AccountId, SBQQ__Quote__c from Contract '+
                 'WHERE ( (Status = :status) AND StartDate >= :dt_from AND StartDate <= :dt_to AND SBQQ__Quote__c !=NULL AND CallId__c != NULL) ';

        return Database.getQueryLocator(sSQL);   
    }
    
    global void execute(Database.BatchableContext bc, Contract[] listContract)
    {
        ContractUtility.ActivateContractsBatchJob(listContract);
    }
    global void finish(Database.BatchableContext bc)
    {        
    }
    
    global void execute(SchedulableContext sc)
    {
        //Activate Contract Batch

        ContractActivation_Batch job = new ContractActivation_Batch();
        Database.executeBatch(job,50); //First error: Too many callouts: 101
    }
}