/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
*	The Territory Assignment Rules Service
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-10-05
* @systemLayer    Service
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@IsTest
public class Test_SRV_TerritoryRules {
    
    @TestSetup
    public static void setupData(){
        List<Account> accounts = Test_DataFactory.generateAccounts('Test Name', 1, false);
        insert accounts;
    }
       
    @IsTest
    public static void test_setFieldsOnAccount(){
        List<Account> accounts = [SELECT Id, OwnerId, BillingPostalCode, TerritoryException__c FROM Account];
        Territory2 territory2_01 = [SELECT Id FROM Territory2 WHERE Name = 'Territory 2.01' LIMIT 1];
        Territory2 region2 = [SELECT Id FROM Territory2 WHERE Name = 'Region 2' LIMIT 1];
        
        Test.startTest();
        	SRV_TerritoryRules.setFieldsOnAccount(accounts[0],NULL, SRV_TerritoryRules.getUserTerritoryHierarchyMap(),SRV_TerritoryRules.getTerritoryDeveloperNamesByPostalCodeMap());
			SRV_TerritoryRules.setFieldsOnAccount(accounts[0], territory2_01.Id, SRV_TerritoryRules.getUserTerritoryHierarchyMap(),SRV_TerritoryRules.getTerritoryDeveloperNamesByPostalCodeMap());
        	SRV_TerritoryRules.setFieldsOnAccount(accounts[0], region2.Id, SRV_TerritoryRules.getUserTerritoryHierarchyMap(),SRV_TerritoryRules.getTerritoryDeveloperNamesByPostalCodeMap());
        Test.stopTest();
    }
    
    @IsTest
    public static void test_getTerritoryDeveloperNamesByPostalCodeMap(){
        Test.startTest();
        	SRV_TerritoryRules.getTerritoryDeveloperNamesByPostalCodeMap();
        Test.stopTest();
    }
    
    @IsTest
    public static void test_getUserTerritoryHierarchyMap(){
        Test.startTest();
        	SRV_TerritoryRules.getUserTerritoryHierarchyMap();
        Test.stopTest();
    }
    
}