@IsTest
public class Tibco_Test {

	private static final String OAUTH_TOKEN = 'testToken';
	private static final String REFRESH_TOKEN = 'refreshToken';
    private static final String PROVIDER = 'Tibco';
	private static final String AUTH_URL = 'http://www.dummy.com/authUrl';
	private static final String KEY = 'testKey';
	private static final String SECRET = 'testSecret';
	private static final String STATE_TO_PROPOGATE  = 'testState';
	private static final String ACCESS_TOKEN_URL = 'http://www.dummy.com/accessTokenUrl';
    private static final string TIBCO_METADATA_API_NAME ='Tibco_Oauth__mdt';

    private static Map<String,String> setupAuthProviderConfig () {
           Map<String,String> authProviderConfiguration = new Map<String,String>();
           authProviderConfiguration.put('Redirect_Url__c', KEY);
           authProviderConfiguration.put('Client_Id__c', AUTH_URL);
           authProviderConfiguration.put('Client_Secret__c', SECRET);
           authProviderConfiguration.put('Access_Token_Url__c', ACCESS_TOKEN_URL);
           return authProviderConfiguration;
    }

    static testMethod void test_Init() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        Tibco tibcoPlugin = new Tibco();
        System.assertEquals(TIBCO_METADATA_API_NAME, tibcoPlugin.getCustomMetadataType());
        PageReference expectedUrl =  new PageReference(authProviderConfiguration.get('Redirect_Url__c') + '?state=' + STATE_TO_PROPOGATE);
        PageReference actualUrl = tibcoPlugin.initiate(authProviderConfiguration, STATE_TO_PROPOGATE);
        System.assertEquals(expectedUrl.getUrl(), actualUrl.getUrl());
    }

    static testMethod void test_BadConfig() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        Tibco tibcoPlugin = new Tibco();
        authProviderConfiguration.remove('Redirect_Url__c');
        Tibco.TibcoPluginException expectedException;
        try {
        	PageReference actualUrl = tibcoPlugin.initiate(authProviderConfiguration, STATE_TO_PROPOGATE);
        } catch (Tibco.TibcoPluginException tpe) {
            expectedException = tpe;
        }
        System.assertEquals(ErrorHandler.ErrorCode.E_NULL_VALUE.name(), expectedException.ErrorCode);
    }
    
    static testMethod void test_CorrectResponse() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        Tibco tibcoPlugin = new Tibco();
        Test.setMock(HttpCalloutMock.class, new TibcoHttpResponseMock());
        
        Map<String,String> queryParams = new Map<String,String>();
        queryParams.put('state', STATE_TO_PROPOGATE);
        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null, null, queryParams);
        Auth.AuthProviderTokenResponse actualResponse = tibcoPlugin.handleCallback(authProviderConfiguration, cbState);
        Auth.AuthProviderTokenResponse expectedResponse = new Auth.AuthProviderTokenResponse('Tibco', OAUTH_TOKEN, REFRESH_TOKEN, STATE_TO_PROPOGATE);
        
        System.assertEquals(expectedResponse.provider, actualResponse.provider);
        System.assertEquals(expectedResponse.oauthToken, actualResponse.oauthToken);
        System.assertEquals(expectedResponse.oauthSecretOrRefreshToken, actualResponse.oauthSecretOrRefreshToken);
        System.assertEquals(expectedResponse.state, actualResponse.state);

        Auth.UserData actualUserData = tibcoPlugin.getUserInfo(authProviderConfiguration, actualResponse);
        Auth.UserData expectedUserData = new Auth.UserData('Tibco', null, 'Tibco', 'Tibco', null, null, 'Tibco', null, 'Tibco', null, null);
        
        System.assertNotEquals(expectedUserData,null);
        System.assertEquals(expectedUserData.lastName, actualUserData.lastName);
        System.assertEquals(expectedUserData.fullName, actualUserData.fullName);
        System.assertEquals(expectedUserData.username, actualUserData.username);
        System.assertEquals(expectedUserData.provider, actualUserData.provider);
        System.assertEquals(expectedUserData.identifier, actualUserData.identifier);
    }

    static testMethod void test_Refresh() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        Tibco tibcoPlugin = new Tibco();
        Test.setMock(HttpCalloutMock.class, new TibcoHttpResponseMock());

        Auth.OAuthRefreshResult actualResponse = tibcoPlugin.refresh(authProviderConfiguration, REFRESH_TOKEN);
        Auth.OAuthRefreshResult expectedResponse = new Auth.OAuthRefreshResult(OAUTH_TOKEN, null);
        
        System.assertEquals(expectedResponse.accessToken, actualResponse.accessToken);
        System.assertEquals(expectedResponse.refreshToken, actualResponse.refreshToken);
    }
    
    static testMethod void test_ErrorResponse() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        Tibco tibcoPlugin = new Tibco();
        TibcoHttpResponseMock badResponse = new TibcoHttpResponseMock();
        badResponse.IsValidAnswer = false;
        Test.setMock(HttpCalloutMock.class, badResponse);
        
        Map<String,String> queryParams = new Map<String,String>();
        queryParams.put('state', STATE_TO_PROPOGATE);
        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null, null, queryParams);
        Tibco.TibcoPluginException expectedException;
        try {
        	Auth.AuthProviderTokenResponse actualAuthProvResponse = tibcoPlugin.handleCallback(authProviderConfiguration, cbState);
        } catch (Tibco.TibcoPluginException tpe) {
            expectedException = tpe;
        }
        System.assertEquals(ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE.name(), expectedException.ErrorCode);
    }
    
	public  class TibcoHttpResponseMock implements HttpCalloutMock {
       private boolean isValidAnswerField = true;
       public boolean IsValidAnswer {
           public get { return isValidAnswerField; }
           public set { isValidAnswerField = value; }
       }
       public HTTPResponse respond(HTTPRequest req) {
           HttpResponse res = new HttpResponse();
           if(isValidAnswerField) {
               Map<string, object> token_response = new Map<string, object> {
                   'access_token' => OAUTH_TOKEN,
                       'token_type' => 'bearer',
                       'expires_in' => 3600
                       };
               
               res.setHeader('Content-Type', 'application/application/json');
               res.setBody(JSON.serialize(token_response));
               res.setStatusCode(200);               
           } else {
               res.setBody('<h1>401 - Unauthorized</h1>');
               res.setStatusCode(401);                 
           }
           return res;
       }   
	}
}