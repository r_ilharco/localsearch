/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> 
 * Date		   : 
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

@RestResource(urlMapping='/invoices')
global with sharing class SwissbillingDataHandlerService {
	@HttpPost
	global static void invoiceFeedbackPostCallout(){
		RestResponse res = RestContext.response;
        RestRequest request = RestContext.request;
		SwissbillingWrappers.StatusInvoiceSummary invoiceSummary = new SwissbillingWrappers.StatusInvoiceSummary();
		List<Error_Log__c> errors = new List<Error_Log__c>();
		try{
			Set<String> correlationIds = new set<String>();
			Map<String, SwissbillingWrappers.FeedbackInvoicesObj> corrIdInvoiceMap = new Map<String, SwissbillingWrappers.FeedbackInvoicesObj>();
			Map<String, SwissbillingWrappers.FeedbackInvoicesObj> failedInvoiceMap = new Map<String, SwissbillingWrappers.FeedbackInvoicesObj>();
            system.debug('request.requestbody.tostring(): ' + request.requestbody.tostring());
            Error_Log__c er = new Error_Log__c();
            er.Message__c = request.requestbody.tostring();
            insert er;
			invoiceSummary = SwissbillingWrappers.StatusInvoiceSummary.deserializeJson(request.requestbody.tostring());
            //invoiceSummary = (SwissbillingWrappers.StatusInvoiceSummary)JSON.deserialize(request.requestbody.tostring(), (SwissbillingWrappers.StatusInvoiceSummary.class));
			system.debug('invoiceSummary: ' + invoiceSummary);
			for(SwissbillingWrappers.FeedbackInvoicesObj feedback : invoiceSummary.FeedbackInvoices){
				if(feedback.Status == 'SUCCESS'){
										corrIdInvoiceMap.put(feedback.CorrelationId, feedback);
				}else{
					failedInvoiceMap.put(feedback.CorrelationId, feedback);
				}
				correlationIds.add(feedback.CorrelationId);
			}
			Map<Id, Invoice__c> invoices = new Map<Id, Invoice__c>([SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                							Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                							Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c,
											Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
											Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
											Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
											Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
										(SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
										FROM Invoice__c
										WHERE Correlation_Id__c IN :correlationIds]);
			if(invoices.values().size() > 0){
				List<Invoice_Order__c> invoiceOrders = [
					SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
					(SELECT Accrual_From__c, Accrual_To__c, Amount__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
						Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c,
						Product__r.ProductCode, Product__c, Subscription__c, Subscription__r.Place__c, Subscription__r.Place__r.Name, Subscription__r.Place__r.PostalCode__c, Subscription__r.Place__r.City__c,
						Credit_Note__c, Credit_Note__r.Reimbursed_Invoice__r.Name, Credit_Note__r.Reimbursed_Invoice__r.Invoice_Date__c, Credit_Note__r.Period_From__c, Credit_Note__r.Period_To__c,
						Credit_Note__r.Execution_Date__c, Credit_Note__r.Nx_Refunded_Place__c, Credit_Note__r.Manual__c,
						Subscription__r.SBQQ__RequiredById__c, Subscription__r.SBQQ__Product__r.One_time_Fee__c,
						Subscription__r.SBQQ__RequiredByProduct__c, Invoice_Order__r.Invoice__r.Status__c, Subscription__r.SBQQ__Product__r.Deferred_Revenue__c,
						Subscription__r.SBQQ__QuoteLine__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c,
						Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
						Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__Description__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.Name, 
						Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.PostalCode__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.City__c
					FROM Invoice_Items__r
					),
					Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
					FROM Invoice_Order__c where Invoice__c in :invoices.keySet()
					ORDER BY Item_Row_Number__c
				];
				Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = new Map<Id, List<Invoice_Order__c>>();
				for(Invoice_Order__c invoiceOrder : invoiceOrders) {
					List<Invoice_Order__c> invoiceOrderList = invoiceOrdersMap.get(invoiceOrder.Invoice__c);
					if(invoiceOrderList == null) {
						invoiceOrderList = new List<Invoice_Order__c>();
						invoiceOrdersMap.put(invoiceOrder.Invoice__c, invoiceOrderList);                
					}
					invoiceOrderList.Add(invoiceOrder);
				}
				List<Invoice__c> invoiceUpdates = new List<Invoice__c>();
				for(Invoice__c invoice : invoices.values()){
					if(corrIdInvoiceMap.containsKey(invoice.Correlation_Id__c)){
						invoice.Name = corrIdInvoiceMap.get(invoice.Correlation_Id__c).DocNumber;
						invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
                        system.debug('invoice: '+ invoice);
						errors.add(ErrorHandler.createLogInfo('SwissbillingDataHandlerService', 'SwissbillingDataHandlerService.invoiceFeedbackPostCallout', 'Bill sent to Swissbilling', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));         
					}else if(failedInvoiceMap.containsKey(invoice.Correlation_Id__c)){
						invoice.Name = ConstantsUtil.INVOICE_PENDING_NAME;
						invoice.Status__c = ConstantsUtil.INVOICE_STATUS_IN_COLLECTION;
                        errors.add(ErrorHandler.createLogError('SwissbillingDataHandlerService', 'SwissbillingDataHandlerService.invoiceFeedbackPostCallout', null, failedInvoiceMap.get(invoice.Correlation_Id__c).Details.ErrorCode, failedInvoiceMap.get(invoice.Correlation_Id__c).Details.ErrorDescription, 'Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice));
					}
                    invoiceUpdates.add(invoice);
                }
				List<Credit_Note__c> creditNotesUpdates = new List<Credit_Note__c>();
				List<SBQQ__Subscription__c> subscriptionsUpdates = new List<SBQQ__Subscription__c>();
				List<Contract> contractsUpdates = new List<Contract>();
				Set<Id> subscriptionsSet = new Set<Id>();
				Set<Id> creditNotesSet = new Set<Id>();
				for(List<Invoice_Order__c> invoiceOrdersList : invoiceOrdersMap.values()) {
					for(Invoice_Order__c invoiceOrder : invoiceOrdersList) {
						for(Invoice_Item__c invoiceItem : invoiceOrder.Invoice_Items__r) {
                            system.debug('invoiceItem.Invoice_Order__r.Invoice__r.Status__c:' +  invoiceItem.Invoice_Order__r.Invoice__r.Status__c);
							if(invoices.get(invoiceItem.Invoice_Order__r.Invoice__c).Status__c == ConstantsUtil.INVOICE_STATUS_COLLECTED){
								if(invoiceItem.Subscription__c  != null) {
                                    String tempDate = invoiceItem.Period__c.substringAfter('-');
                                    tempDate = tempDate.trim();
                                    system.debug('tempDate: ' + tempDate);
                                    Integer year = Integer.valueOf(tempDate.mid(6,4));
                                    Integer month = Integer.valueOf(tempDate.mid(3,2));
                                    Integer day = Integer.valueOf(tempDate.mid(0,2));
                                    Date nextInvoiceDate = Date.newInstance(year, month, day);
                                    system.debug('nextInvoiceDate: ' + nextInvoiceDate);
                                    if(!subscriptionsSet.contains(invoiceItem.Subscription__c)) {
										if(invoiceItem.Subscription__r.SBQQ__Product__r.One_time_Fee__c /*|| !invoiceItem.Subscription__r.SBQQ__Product__r.Deferred_Revenue__c*/){
											subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, One_time_Fee_Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
										}else{
											subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
										}
                                        system.debug('subscriptionsUpdates: ' + subscriptionsUpdates);
										subscriptionsSet.add(invoiceItem.Subscription__c);
									}
								}
								if(invoiceItem.Credit_Note__c != null) {
									if(!creditNotesSet.contains(invoiceItem.Credit_Note__c)) {
										creditNotesUpdates.add(new Credit_Note__c(Id = invoiceItem.Credit_Note__c, Status__c = ConstantsUtil.CREDIT_NOTE_STATUS_BILLED));
										creditNotesSet.add(invoiceItem.Credit_Note__c);
									}
								}
							}
						}
					}
				}
				contractsUpdates = SwissbillingHelper.contractsToBeUpdated(subscriptionsUpdates);
				update invoiceUpdates;
				if(subscriptionsUpdates.size() > 0) {
					update subscriptionsUpdates;
				}
				if(contractsUpdates.size() > 0){
					update contractsUpdates;
				}
				if(creditNotesUpdates.size() > 0) {
					update creditNotesUpdates;
				}
				res.statusCode = 200;
			}else{
				if(!correlationIds.isEmpty()){
					//errors.add(ErrorHandler.createLogInfo('SwissbillingDataHandlerService', 'SwissbillingDataHandlerService.invoiceFeedbackPostCallout', 'Invoices not found', 'Responde status code: ' + res.statusCode, 'Transaction Id:' + invoiceSummary.CorrelationId, null));
					res.responseBody = Blob.valueOf('Invoices not found');
				}else{
					//errors.add(ErrorHandler.createLogInfo('SwissbillingDataHandlerService', 'SwissbillingDataHandlerService.invoiceFeedbackPostCallout', 'empty request', 'Responde status code: ' + res.statusCode, 'Transaction Id:' + invoiceSummary.CorrelationId, null));
					res.responseBody = Blob.valueOf('empty request');
				}
				res.statusCode = 404;
			}
		}catch(Exception ex){
			errors.add(ErrorHandler.createLogError('SwissbillingDataHandlerService ' + ex.getMEssage(), 'SwissbillingDataHandlerService.invoiceFeedbackPostCallout', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'Transaction Id: test', null, null));
			res.responseBody = Blob.valueOf(ex.getStackTraceString());
			res.statusCode = 500;
		}finally{
			if(errors.size() > 0) {
                insert errors;
            }
		}
	}
}
//JSON STRUCTURE
/*{
    "FeedbackInvoices:":
    [
        {
            "Status": "string",
            "RequestTimestamp": "datetime",
            "ResponseTimestamp": "datetime",
            "CorrelationId": "string",
            "DocNumber": "integer",
            "Details" : {
                "ErrorDescription" : "string",
                "ErrorCode" : "string"
            } 
        }
    ]
}*/