/**
* Author	   : Alessandro L. Marotta <amarotta@deloitte.it>
* Date		   : 23-12-2019
* Sprint      : 13
* Work item   : 
* Testclass   : Test_FutureActivationContractBatch
* Package     : 
* Description : Batch class for activating contracts and subscriptions with start date in the future
* Changelog   : 2020-09-30 -Cancellation order stuck in Draft
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @modifiedby    Mara Mazzarella
 * 2020-11-09     SPIII-3794 Context products-Subscription & order relationship with de-allocation
 *				  in CASA - deleteAllocation when Termination order is fulfilled.
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class FutureActivationContractBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts{
    
    
    //A.L. Marotta 31-12-19 - execute this from developer console to run this batch every day at midnight
    //System.schedule('FutureActivationContractBatch', '0 0 00 * * ?', new FutureActivationContractBatch());
    
    global void execute(SchedulableContext sc) {
        FutureActivationContractBatch b = new FutureActivationContractBatch(); 
        database.executebatch(b, 1);
    }
    
    //select id, Status from Contract where StartDate > TODAY and Status IN ('Draft', 'Production')
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String myWebsitePG = ConstantsUtil.MYWEBSITE_GROUP;
        List<String> subStatus = new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION, ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT};
        String query = 'SELECT Id, Status, In_Termination__c, In_Cancellation__c ' +
            'FROM Contract ' +
            'WHERE Id IN ' + 
            '(SELECT SBQQ__Contract__c FROM SBQQ__Subscription__c WHERE (Subsctiption_Status__c = \'' + ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT + '\' AND SBQQ__StartDate__c <= TODAY)'+
			'or (Subsctiption_Status__c= \'' + ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION + '\' and (SBQQ__Contract__r.Status =\'' + ConstantsUtil.CONTRACT_STATUS_ACTIVE + '\' or SBQQ__Contract__r.Status =\'' + ConstantsUtil.CONTRACT_STATUS_DRAFT + '\'))'+
            'OR (In_Termination__c = TRUE AND Termination_Date__c <= TODAY))';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        Map<Id, Contract> mapCtr = new Map<Id,Contract>();
        Map<Id, SBQQ__Subscription__c> subsToUpdate = new Map<Id, SBQQ__Subscription__c>();
        Map<Id, Set<SBQQ__Subscription__c>> contractToSubs = new Map<Id, Set<SBQQ__Subscription__c>>();
        
        Boolean updateContract = false;
        
        for(Contract aCntr : scope){
            mapCtr.put(aCntr.Id,aCntr);
        }
        
        List<SBQQ__Subscription__c> allSubsForContract = [SELECT 	Id, 
                                                          			SBQQ__Contract__c, 
                                                          			Subsctiption_Status__c, 
                                                          			In_Termination__c, 
                                                          			Termination_Date__c,
                                                          			SBQQ__StartDate__c,
                                                          			SBQQ__Contract__r.In_Termination__c,
                                                          			SBQQ__Contract__r.In_Cancellation__c,
                                                          			Ad_Context_Allocation__c,
                                                          			Activation_Confirmed__c
                                                          FROM 		SBQQ__Subscription__c
                                                          WHERE 	SBQQ__Contract__c IN :mapCtr.KeySet()];
        
        Set<Id> subscriptionIds = new Set<Id>();
                                                          
        for(SBQQ__Subscription__c aSub : allSubsForContract){
            subscriptionIds.add(aSub.Id);
            if(!contractToSubs.containsKey(aSub.SBQQ__Contract__c)){
                contractToSubs.put(aSub.SBQQ__Contract__c, new Set<SBQQ__Subscription__c>{aSub});
            }
            else{
                contractToSubs.get(aSub.SBQQ__Contract__c).add(aSub);
            }
        }

        List<String> validOrderTypes = new List<String>{ConstantsUtil.ORDER_TYPE_CANCELLATION,ConstantsUtil.ORDER_TYPE_TERMINATION};
        Set<Id> validSubsToTerminateCancel = new Set<Id>();
        List<OrderItem> cancellationTerminationOrderItems = [   
                                                                SELECT  Id, SBQQ__Subscription__c
                                                                FROM    OrderItem 
                                                                WHERE   Order.Custom_Type__c IN :validOrderTypes 
                                                                AND     SBQQ__Subscription__c IN :subscriptionIds 
                                                                AND     SBQQ__Status__c = :ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED
                                                            ];
        for(OrderItem oi : cancellationTerminationOrderItems){
            validSubsToTerminateCancel.add(oi.SBQQ__Subscription__c);
        }

        if(!contractToSubs.isEmpty()){
            for(Id currentKey : contractToSubs.keySet()){
                Boolean updateContractActivation = false;
                Boolean updateContractTermnation = false;
                Integer countTerminatedSub = 0;
                
                for(SBQQ__Subscription__c currentSub : contractToSubs.get(currentKey)){
                    Boolean isProduction = ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION.equals(currentSub.Subsctiption_Status__c);
                    Boolean isDraft = ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT.equals(currentSub.Subsctiption_Status__c);
                    Boolean isActive = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE.equals(currentSub.Subsctiption_Status__c);
                    Boolean isTerminated = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED.equals(currentSub.Subsctiption_Status__c);
                    Boolean isCancelled= ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED.equals(currentSub.Subsctiption_Status__c);
                    
                    if(( (isProduction && currentSub.Activation_Confirmed__c) || isDraft) && currentSub.SBQQ__StartDate__c <= Date.today()){
                        currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
                        subsToUpdate.put(currentSub.Id, currentSub);
                        updateContractActivation = true;
                    }
                    else if(currentSub.Termination_Date__c <= Date.today() && currentSub.In_Termination__c && !validSubsToTerminateCancel.isEmpty() && validSubsToTerminateCancel.contains(currentSub.Id)){
                        if(mapCtr.get(currentKey).In_Cancellation__c){
                           // if(String.isNotBlank(currentSub.Ad_Context_Allocation__c)) currentSub.Tech_Delete_Allocation__c = true;
                            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED;
                            currentSub.In_Termination__c = false;
                            subsToUpdate.put(currentSub.Id, currentSub);
                            countTerminatedSub++;
                        }
                        else{
                          //  if(String.isNotBlank(currentSub.Ad_Context_Allocation__c)) currentSub.Tech_Delete_Allocation__c = true;
                            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
                            currentSub.In_Termination__c = false;
                            subsToUpdate.put(currentSub.Id, currentSub);
                            countTerminatedSub++;
                        }
                    }
                    else if(isTerminated || isCancelled){
                        countTerminatedSub++;
                    }
                    else if(isProduction){ // Activate Website product contract Move this to last check to prevent SPIII-4609
                        updateContractActivation = true;
                    }
                }
                if(countTerminatedSub == contractToSubs.get(currentKey).size()){
                    updateContractTermnation = true;
                }
                
                if(updateContractActivation){
                    mapCtr.get(currentKey).Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
                    updateContract = true;
                }
                else if(updateContractTermnation){
                    if(mapCtr.get(currentKey).In_Cancellation__c){
                        mapCtr.get(currentKey).Status = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
                        mapCtr.get(currentKey).In_Cancellation__c = false;
                    }
                    else{
                        mapCtr.get(currentKey).Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
                        mapCtr.get(currentKey).In_Termination__c = false;
                    }
                    updateContract = true;
                }
            }  	
        }
        
        Savepoint sp;
        try{
            sp = Database.setSavepoint();
            if(!subsToUpdate.isEmpty()) update subsToUpdate.values();
            if(updateContract) update mapCtr.values();
            
            for(Contract currentContract : mapCtr.values()){
                if(currentContract.Status.equalsIgnoreCase(ConstantsUtil.CONTRACT_STATUS_CANCELLED)) System.enqueueJob(new QueueableCancelInvoice(currentContract));
                else if(currentContract.Status.equalsIgnoreCase(ConstantsUtil.CONTRACT_STATUS_TERMINATED)) System.enqueueJob(new QueueableCancelContract(currentContract.Id));
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            System.debug('Update failed, reason: '+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext info){     
    }
    
    public class QueueableCancelInvoice implements Queueable {
        
        Contract cancelledContract;
        
        public QueueableCancelInvoice(Contract cancelledContract) {
            this.cancelledContract = cancelledContract;
        }
        public void execute(QueueableContext context) {
            List<Contract_Setting__mdt> contractCancellation = [SELECT  InvoiceStatus__c FROM    Contract_Setting__mdt WHERE   MasterLabel = 'ContractCancellation_Process']; 
            List<String> invoiceStatus = contractCancellation[0].InvoiceStatus__c.split(';');
            cancelInvoices(cancelledContract.Id, invoiceStatus);
        }
    }
    
    public class QueueableCancelContract implements Queueable {
        
        Id terminatedContractId;
        
        public QueueableCancelContract(Id terminatedContractId) {
            this.terminatedContractId = terminatedContractId;
        }
        public void execute(QueueableContext context) {
            Billing.cancelcontract(terminatedContractId);
        }
    }
    
    @TestVisible
    private static void cancelInvoices(String contractId, List<String> invoiceStatus){
        Map<Id, Invoice__c> invoiceToCancel = SEL_Invoices.getInvoicesByContractIDAndInvoiceStatus(contractId, invoiceStatus);
        
        if(!invoiceToCancel.isEmpty()){
            List<AggregateResult> results = SEL_InvoiceOrders.countInvoices(invoiceToCancel.keySet());
            for(AggregateResult r : results){
                if(r.get('number') == 1){
                    SRV_Invoice.cancelInvoices(new Set<Id> (invoiceToCancel.keySet())); 
                } 
            }
        }    
    }
    
}