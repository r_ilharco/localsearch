/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Task Trigger (Handler and Helper)
 * Changelog   : 
 */

@isTest
public class Test_TaskTrigger {
    
    @testSetup
    public static void test_setupData(){
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        TaskTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        TaskTriggerHandler.disableTrigger = false;
    }
	
    @isTest
    public static void test_insertUpdate(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        
        Task taskToInsert = new Task();
        taskToInsert.WhatId = account.Id;
        taskToInsert.WhoId = contact.Id;
        taskToInsert.Subject = ConstantsUtil.EVENT_SUBJECT_CALL;
        insert taskToInsert;
        update taskToInsert;
        delete taskToInsert;
        undelete taskToInsert;
    }
}