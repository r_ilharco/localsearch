//CHANGELOG: #1: SF2-264 - Legal Address Validation - gcasola - 07-19-2019 - Address_Validated changed Type from Checkbox to Picklist
//           #2: SF2-253 - Validate Account and Billing Address on Quote Creation
//               SF2-264 - Legal Address Validation
//                  gcasola 07-22-2019
@isTest
public class Test_LegalAddressValidationController {
	
    @testSetup
    public static void setupData(){
        BillingProfileTriggerHandler.disableTrigger = true;
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Account> acc = Test_DataFactory.createAccounts('Test',1);
        insert acc;
        
        List<Contact> relatedContact = Test_DataFactory.createContacts(acc[0], 1);
        relatedContact[0].PO_BoxCity__c = 'City';
        relatedContact[0].PO_BoxZip_PostalCode__c = '12345';
        relatedContact[0].PO_Box__c = 'Test';
        insert relatedContact[0];
        
        List<Billing_Profile__c> billingProfile = Test_DataFactory.createBillingProfiles(acc, relatedContact, 1);
        insert billingProfile;
        BillingProfileTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void validateAddress_AccountOK(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(acc[0].Id);
        
        List<Account> accUpdated = [SELECT AddressValidated__c FROM Account LIMIT 1];
        Test.stopTest();
    }
    
    @isTest
    public static void validateAddress_AccountSKIPPED(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        AccountTriggerHandler.disableTrigger = true;
            acc[0].SkipAddressValidation__c = true;
            update acc[0];
        AccountTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(acc[0].Id);
        
        /*List<Account> accUpdated = [SELECT AddressValidated__c FROM Account LIMIT 1];
        System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_VALIDATED_UIDREGISTER, accUpdated[0].AddressValidated__c);*/
        Test.stopTest();
    }
    
    @isTest
    public static void validateAddress_AccountKO(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(acc[0].Id);
        
        List<Account> accUpdated = [SELECT AddressValidated__c FROM Account LIMIT 1];
        //System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED, accUpdated[0].AddressValidated__c);
        Test.stopTest();
    }
    
    @isTest
    public static void validateAddress_BillingProfileOK(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Billing_Profile__c> bf = [SELECT Id, 
                             					Customer__c,
                                                Billing_City__c, 
                                                Billing_Street__c, 
                                                Billing_Postal_Code__c, 
                                                Phone__c, 
                                                Mail_address__c, 
                                                AddressValidated__c
                                        FROM Billing_Profile__c
                                        LIMIT 1];
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(bf[0].Id);
        List<Billing_Profile__c> bfUpdated = [SELECT AddressValidated__c FROM Billing_Profile__c LIMIT 1];
       // System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_VALIDATED_UIDREGISTER, bfUpdated[0].AddressValidated__c);
        Test.stopTest();
    }
    
   	/*@isTest
    public static void validateAddress_BillingProfileCatch(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Billing_Profile__c> bf = [SELECT Id, 
                             					Customer__c,
                                                Billing_City__c, 
                                                Billing_Street__c, 
                                                Billing_Postal_Code__c, 
                                                Phone__c, 
                                                Mail_address__c, 
                                                AddressValidated__c
                                        FROM Billing_Profile__c
                                        LIMIT 1];
        
        bf[0].Billing_Postal_Code__c = '2000';
        update bf[0];
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(bf[0].Id);
        List<Billing_Profile__c> bfUpdated = [SELECT AddressValidated__c FROM Billing_Profile__c LIMIT 1];
        System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED, bfUpdated[0].AddressValidated__c);
        Test.stopTest();
    }*/
    
    @isTest
    public static void validateAddress_BillingProfileKO(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO));
        List<Billing_Profile__c> bf = [SELECT Id, 
                             					Customer__c,
                                                Billing_City__c, 
                                                Billing_Street__c, 
                                                Billing_Postal_Code__c, 
                                                Phone__c, 
                                                Mail_address__c, 
                                                AddressValidated__c
                                        FROM Billing_Profile__c
                                        LIMIT 1];
        
        Test.startTest();
        LegalAddressValidationController.validateRecordLegalAddress(bf[0].Id);
        List<Billing_Profile__c> bfUpdated = [SELECT AddressValidated__c FROM Billing_Profile__c LIMIT 1];
        //System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED, bfUpdated[0].AddressValidated__c);
        Test.stopTest();
    }
}