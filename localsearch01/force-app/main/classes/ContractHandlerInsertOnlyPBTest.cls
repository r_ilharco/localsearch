@isTest
public class ContractHandlerInsertOnlyPBTest {

    @isTest
    static Void testContractHandlerInsert(){
        
        try{
         Account acc= new Account(Name='TestAccount',
                        				PreferredLanguage__c = 'English',
                          				POBox__c='Ramna'  ,
                           				P_O_Box_City__c	='Dhaka',          
                           				P_O_Box_Zip_Postal_Code__c='Dhaka-1000'	          
                        );
    				
		insert acc;
    	System.debug('Acc: '+acc);
        
        Contract cntrct= new Contract();
        cntrct.AccountId= acc.Id;
        cntrct.StartDate= system.today();
        
        test.startTest();
        insert cntrct;
        test.stopTest();
        System.debug('Acc: '+ cntrct);
        }
        catch(Exception e)
        {
            system.assert(false,e.getMessage());
        }
    }
      
}