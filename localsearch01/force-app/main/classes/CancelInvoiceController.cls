public without sharing class CancelInvoiceController {
    
	@AuraEnabled
    public static Invoice__c getInvoice(Id invoiceId) {
        if(invoiceId == null) return null;
        List<Invoice__c> invoices = [SELECT Id, Name, Correlation_Id__c, Payment_Status__c, Booking_Status__c, Status__c, Customer__c, Customer__r.Name,
                                     Open_Amount__c, Total__c
                                     from Invoice__c where id=:invoiceId LIMIT 1];
        if(invoices.size() != 1)  return null;
        return invoices[0];
    }

    @AuraEnabled
    public static string cancelByInvoiceId(Id invoiceId) {
        //To be improved - Temporal solution
        String msg = null;
        /*Id userId=userinfo.getUserId();
        Integer hascsc_perset=[SELECT Count()  FROM PermissionSetAssignment WHERE AssigneeId = :userId AND PermissionSet.Name = :Label.BillingPermissionSetName];
        if(hascsc_perset == 0){           
            msg = Label.validateCancelInvoice;
            return msg;
        } */           

        if(invoiceId == null) return null;
        List<Invoice__c> invoices = [SELECT Id, Name, Correlation_Id__c,  Status__c, Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Open_Amount__c, Total__c,
                                        Billing_Profile__r.Billing_Contact__c, Billing_Profile__r.Billing_Language__c, (SELECT Contract__c FROM Invoice_Orders__r LIMIT 1)
                                        FROM Invoice__c where id =:invoiceId LIMIT 1];            
        if(invoices.size() != 1) return null;

        //ldimartino, SF2-23, Sprint 10 START
        String queueName = ConstantsUtil.UNPAID_CASE_QUEUE_NAME; 
        String recordTypeName = ConstantsUtil.UNPAID_CASE_RECORD_TYPE_NAME;
        List<Case> cases = new List<Case>();
        List<Group> queues = [Select Id, DeveloperName from Group where DeveloperName = :queueName and Type='Queue' LIMIT 1];
        List<RecordType> recordTypes = [Select Id, DeveloperName from RecordType where DeveloperName = :recordTypeName LIMIT 1];
        try{
            HttpResponse sbResponse = SRV_Swissbilling.getInvoiceStatusByIdCallout(invoices[0].Correlation_Id__c);
            system.debug('sbResponse: '+ sbResponse);
            system.debug('sbResponseBody: '+ sbResponse.getBody());

            
            if(sbResponse.getStatusCode() == 200){
                
                SwissbillingWrappers.SalesInvoiceSummary invoiceSummaries = (SwissbillingWrappers.SalesInvoiceSummary)JSON.deserialize(sbResponse.getBody(), SwissbillingWrappers.SalesInvoiceSummary.class);
                system.debug('invoiceSummaries: ' + invoiceSummaries);


                if(invoiceSummaries.open_amount != invoiceSummaries.total_amount_incl_tax){
                    
                    for(Invoice__c inv : invoices) {
                        cases.add(new Case(
                            AccountId = inv.Customer__c, 
                            Origin = ConstantsUtil.UNPAID_CASE_ORIGIN, 
                            //OwnerId = queues[0].id,
                            RecordTypeId = recordTypes[0].id, 
                            Language__c = invoices[0].Billing_Profile__r.Billing_Language__c,
                            ContactId = inv.Billing_Profile__r.Billing_Contact__c, 
                            Contract__c = inv.Invoice_Orders__r[0].Contract__c, 
                            Description = 'Cancellation Failure ' + inv.Name + ':' + ConstantsUtil.INVOICE_NOT_UNPAID + '\n',
                            Priority = ConstantsUtil.UNPAID_CASE_PRIORITY,  
                            Topic__c = ConstantsUtil.BILL_CASE_TOPIC,  
                            Sub_Topic__c = ConstantsUtil.CASE_SUBTOPIC_INV_CORR,  
                            Subject = 'Cancellation failure ' + inv.Name, 
                            Invoice__c = inv.Id 
                        ));
                    }
                }else{
                    AbacusCancellationJsonWrapper abacusWrapper = new AbacusCancellationJsonWrapper(invoices[0].Name , Date.today());
                    String AbacusPayload = JSON.serialize(abacusWrapper);
                    HttpResponse abacusResponse = SRV_Abacus.postCallout(AbacusPayload, invoices[0].Correlation_Id__c, ConstantsUtil.ABACUS_CREDIT_NOTE);
                    system.debug('abacusResponse: ' + abacusResponse);
                    if(abacusResponse.getStatusCode() == 200){}
                    else throw new SwissbillingHelper.SwissbillingHelperException('Invoice Cancellation (ABACUS) ' + abacusResponse.getStatusCode() + '\n' + abacusResponse.getBody() + '\n'+ AbacusPayload , ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                }

                // nnaumovski, SPIII-5325 start ***  
                System.debug('contains C');
                if(invoiceSummaries.payment_status.containsIgnoreCase('c')){
                    try{
                        Boolean isUpdatedInvoice = changeStatusOnAlreadyCancelledInvoiceSWB(invoices[0]);
                        if(isUpdatedInvoice){
                            Billing.resetSubscriptions(new List<Id>{invoices[0].Id});
                        }
                    }catch(Exception ex){
                        ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', 'Billing.CancelInvoiceController', ex, String.valueOf(ErrorHandler.ErrorCode.E_UNKNOWN), 'Error while cancelling already cancelled invoice.', null, invoices[0].Correlation_Id__c, 'Billing.CancelInvoiceController', null, false, invoices[0]);
                    }
                }else{
                // nnaumovski, SPIII-5325 end ***  

                    SwissbillingHelper.ResponseWrapper response = SwissbillingHelper.cancelInvoice(invoices[0]);
                    system.debug('response: ' + response);
                    if(response.isOk) {
                        Billing.resetSubscriptions(new List<id> { invoiceId });
                        msg = null;
                        if(cases.size() > 0) insert cases;
                    }else if(!String.isBlank(response.body)) msg = response.body;
                }
            
            }else{
                msg = ConstantsUtil.GENERIC_ERROR_CODE;
                throw new SwissbillingHelper.SwissbillingHelperException('Invoice Cancellation (SWISSBILLING) ' + sbResponse.getStatusCode() + '\n' + sbResponse.getBody() + '\n', ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
            }
        }catch(DmlException ex){
            ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', 'Billing.CancelInvoiceController', ex, String.valueOf(ErrorHandler.ErrorCode.E_UNKNOWN), 'Error creating delivery failure notification cases', null, invoices[0].Correlation_Id__c, 'Billing.CancelInvoiceController', null, false, invoices[0]);
        }catch(Exception cEx){
            ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', 'Billing.CancelInvoiceController', cEx, String.valueOf(ErrorHandler.ErrorCode.E_UNKNOWN), 'Error sending cancellation request', null, invoices[0].Correlation_Id__c, 'Billing.CancelInvoiceController', null, false, invoices[0]);
        }
        return msg;  
        // ldimartino, hotfix, AS-110 *** END     
    }

    // nnaumovski, SPIII-5325 start ***  
    // this method will be called once we have a situation that invoice is already cancelled on SWB and we need to align SF with SWB
    public static Boolean changeStatusOnAlreadyCancelledInvoiceSWB(Invoice__c invoice){

        Boolean isSuccess = true;
        invoice.Status__c = ConstantsUtil.INVOICE_STATUS_CANCELLED;

        try{
            update invoice;
        }catch(Exception e){
            isSuccess = false;
        }

        return isSuccess;
    }
    // nnaumovski, SPIII-5325 end ***  
}