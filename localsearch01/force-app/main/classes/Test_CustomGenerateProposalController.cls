@isTest
public class Test_CustomGenerateProposalController {
 	@TestSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<Product2> banners = new List<Product2>();
        banners.add(generteProduct_localBANNER());
        banners.add(generteFatherProduct_searchBANNER());
        insert banners;
        List<PricebookEntry> pbesBanner = new List<PricebookEntry>();
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[0].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[1].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        insert pbesBanner;

        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 1', account.Id));
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 2', account.Id));
        insert opportunities;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c';
        List<SBQQ__Quote__c> quotes = Database.query(queryQuotes);
        
        List<Billing_Profile__c> billingProfile = Test_DataFactory.generateBillingProfiles(account.Id,'Test Billing Profile', null, 1);
        insert billingProfile;
		quotes[0].Billing_Profile__c = billingProfile[0].Id;
        quotes[1].Billing_Profile__c = billingProfile[0].Id;
        quotes[1].Replacement__c = true;
        update quotes;
        
        List<SBQQ__QuoteLine__c> fatherQuoteLines = new List<SBQQ__QuoteLine__c>();
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quotes[0].Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', pbesBanner[0].Id));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quotes[0].Id, banners[1].Id, Date.today(), contact.Id, null, null, 'Annual', pbesBanner[1].Id));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quotes[1].Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', pbesBanner[0].Id));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quotes[1].Id, banners[1].Id, Date.today(), contact.Id, null, null, 'Annual', pbesBanner[1].Id));
        fatherQuoteLines[0].Url__c = 'test.ch';
        fatherQuoteLines[1].Url__c = 'test.ch';
        fatherQuoteLines[2].Url__c = 'test.ch';
        fatherQuoteLines[3].Url__c = 'test.ch';
        insert fatherQuoteLines;
        
        Order activationOrder = createActivationOrder(account.Id, quotes[0].Id);
        insert activationOrder;
        
        List<OrderItem> fatherOrderItems = new List<OrderItem>();
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrder.Id, fatherQuoteLines[0]));
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrder.Id, fatherQuoteLines[1]));
        insert fatherOrderItems;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(activationOrder, false);
        insert contract;
        
        List<SBQQ__Subscription__c> fatherSubs = new List<SBQQ__Subscription__c>();
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItems[0]));
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItems[1]));
        for(SBQQ__Subscription__c sub : fatherSubs){
            sub.Subscription_Term__c = fatherQuoteLines[0].Subscription_Term__c;
        }
        insert fatherSubs;

        fatherOrderItems[0].SBQQ__Subscription__c = fatherSubs[0].Id;
        fatherOrderItems[1].SBQQ__Subscription__c = fatherSubs[1].Id;
        update fatherOrderItems;
        
        activationOrder.Contract__c = contract.Id;
        activationOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        activationOrder.SBQQ__Contracted__c = true;
        update activationOrder;
        
        //contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = activationOrder.Id;
        update contract;
        
        fatherSubs[0].Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        fatherSubs[1].Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        update fatherSubs;
        
        fatherSubs[0].Replaced_by_Product__c = fatherQuoteLines[2].Id;
        //fatherSubs[1].Replaced_by_Product__c = fatherQuoteLines[3].Id;
        update fatherSubs;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }   
    
    @isTest
    public static void test_customGenerateProposalController(){

        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        QuoteLineTriggerHandler.disableTrigger = true;
        update quote;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        SRV_IntegrationOutbound.AddressValidationResult avResult = new SRV_IntegrationOutbound.AddressValidationResult();
        avResult.isActive = true;
        avResult.result = 'KO';
        avResult.error = new ValidateAddressResponseWrapper.ErrorObj();
                
        Test.startTest();
        CustomGenerateProposalController.validateQuoteBeforeGeneratingProposal(quote.Id);
        CustomGenerateProposalController.generateNumberingOnQls(quote.Id);
        CustomGenerateProposalController.validateQLStartDate(quote);
        Test.stopTest();
    }
    
    @isTest
    public static void test_customGenerateProposalController2(){

        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        QuoteLineTriggerHandler.disableTrigger = true;
        update quote;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        SRV_IntegrationOutbound.AddressValidationResult avResult = new SRV_IntegrationOutbound.AddressValidationResult();
        avResult.isActive = true;
        avResult.result = 'KO';
        avResult.error = new ValidateAddressResponseWrapper.ErrorObj();
                
        Test.startTest();
        CustomGenerateProposalController.bpLegalAddressValidation(quote,avResult);
        CustomGenerateProposalController.accountLegalAddressValidation(quote,avResult);
        Test.stopTest();
    }
    
    @isTest
    public static void test_customGenerateProposalController3(){

        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        QuoteLineTriggerHandler.disableTrigger = true;
        update quote;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        SRV_IntegrationOutbound.AddressValidationResult avResult = new SRV_IntegrationOutbound.AddressValidationResult();
        avResult.isActive = true;
        avResult.result = 'OK';
        avResult.error = new ValidateAddressResponseWrapper.ErrorObj();
                
        Test.startTest();
        CustomGenerateProposalController.bpLegalAddressValidation(quote,avResult);
        CustomGenerateProposalController.accountLegalAddressValidation(quote,avResult);
        Test.stopTest();
    }
    
    @isTest
    public static void test_replacementQuoteError(){
        
        String quoteTypeReplacement = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Type__c = :quoteTypeReplacement LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        Test.startTest();
        CustomGenerateProposalController.checkReplacementSubscriptions(quote.Id);
        CustomGenerateProposalController.blockProductSellingOnSamePlaceNew(quote);
        Test.stopTest();
    }
    
    @isTest
    public static void test_replacementQuoteSuccess(){
        
        String quoteTypeReplacement = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Type__c = :quoteTypeReplacement LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id FROM SBQQ__Subscription__c];
        
        subscriptions[0].Replaced_by_Product__c = quoteLines[0].Id;
        subscriptions[1].Replaced_by_Product__c = quoteLines[1].Id;
        update subscriptions;
        
        Test.startTest();
        CustomGenerateProposalController.getReplacedSubscriptions(quote.Id);
        CustomGenerateProposalController.getQuoteType(quote.Id);
        CustomGenerateProposalController.checkReplacementSubscriptions(quote.Id);
        Test.stopTest();
    }

   @isTest
    public static void test_contactDetails(){
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
       
        List<Product2> productsToInsert = new List<Product2>();
      	Product2 prod =  Test_SBQQQuoteTrigger.generateProduct_vipPlace();
        insert prod;

        List<String> fieldNamesAccount = new List<String>(Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount =' SELECT ' + String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        Billing_Profile__c billingProfile = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :account.Id LIMIT 1];
        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id LIMIT 1];
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        insert opportunity;
        
        SBQQ__Quote__c quote = [SELECT Id,Replacement__c,SBQQ__Opportunity2__c,SBQQ__Account__c FROM SBQQ__Quote__c Where SBQQ__Opportunity2__c = :opportunity.Id LIMIT 1];
       	
        
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, prod.Id, Date.today(), contact.Id, null, null, 'Annual');
        fatherQL.SBQQ__Bundle__c= true;
        insert fatherQL;
        contact.Email ='';
       	contact.FirstName ='';
        contact.Phone ='';
        update contact;
        Test.startTest();
        	CustomGenerateProposalController.checkLBxContactDetails(quote.Id);
        Test.stopTest();
        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }


    public static Product2 generteProduct_localBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteFatherProduct_searchBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','generteFatherProduct_searchBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','SBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','SBANNER');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','searchBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','SBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Order createActivationOrder(String accountId, String quoteId){
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c', ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type', ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate', String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId', accountId);
        fieldApiNameToValue.put('SBQQ__Quote__c', quoteId);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        return order;
    }
}