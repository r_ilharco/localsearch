@IsTest
public class Test_activeMigratedOrderSchedulable {
static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
}
    @Testsetup  
    static void setup(){
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        AccountTriggerHandler.disableTrigger = true;
        insertBypassFlowNames();
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generateFatherProduct());
        productsToInsert.add(generateChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opportunity.Id, account.Id, contact.Id, null);
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote.SBQQ__CustomerDiscount__c = 5 ;
        quote.SBQQ__Primary__c = true;
        insert quote;
        opportunity.SBQQ__PrimaryQuote__c = quote.Id;
        update opportunity;
               
         SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        		
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
         Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        order.SambaIsStartConfirmed__c = true;
        insert order;
        
         SBQQ__QuoteLine__c fatherQL1;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL1 = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL1);
        fatherOI.SambaIsBilled__c = true;
        insert fatherOI;
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        AccountTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
    }

    @istest 
    static void test_activeMigratedOrderSchedulable(){
        
        test.startTest();
        		OrderProductTriggerHandler.disableTrigger = true;
		OrderTriggerHandler.disableTrigger = true;
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        activeMigratedOrder_Schedulable dab = new activeMigratedOrder_Schedulable();
        dab.execute(null);
        		OrderProductTriggerHandler.disableTrigger = false;
		OrderTriggerHandler.disableTrigger = false;
        test.stopTest();   
    }
    
    @isTest
    static void test_activeMigratedOrderExecute(){
        
        List<Order> ord = [SELECT Id, OwnerId, ContractId, AccountId, Pricebook2Id, OriginalOrderId, OpportunityId, QuoteId, EffectiveDate, EndDate, IsReductionOrder, Status, Description, CustomerAuthorizedById, CustomerAuthorizedDate, CompanyAuthorizedById, CompanyAuthorizedDate, Type, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, ShippingAddress, Name, PoDate, PoNumber, OrderReferenceNumber, BillToContactId, ShipToContactId, ActivatedDate, ActivatedById, StatusCode, OrderNumber, TotalAmount, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, IsDeleted, SystemModstamp, LastViewedDate, LastReferencedDate, SBQQ__Contracted__c, SBQQ__ContractingMethod__c, SBQQ__PaymentTerm__c, SBQQ__PriceCalcStatusMessage__c, SBQQ__PriceCalcStatus__c, SBQQ__Quote__c, SBQQ__RenewalTerm__c, SBQQ__RenewalUpliftRate__c, SBQQ__OrderBookings__c, SBQQ__TaxAmount__c, Order_Loaded__c, Number_of_Products__c, Billing_Channel__c, Billing_Profile__c, CallId__c, Call_API__c, Cluster_Id__c, ContractSAMBA_Key__c, Contract__c, Created_date_delay__c, Custom_Type__c, Document_Url__c, External_Id__c, Filtered_Primary_Contact__c, LastModified_Date_delay__c, Manual_Activation__c, Migrated__c, New_Customer__c, No_Clawback__c, Parent_Order__c, Products__c, SambaIsBilled__c, SambaIsStartConfirmed__c, Samba_ExternalID__c, Type__c, nextRenewalDate__c, Manual_Activation_Roll__c, Manual_Amendment_Count__c, Manual_Termination_Count__c, Partial_Amendment_Roll__c, Partial_Roll__c, Total_Net_Amount__c, Processed__c, Contract_Check__c FROM Order];
        test.startTest();
		OrderProductTriggerHandler.disableTrigger = true;
		OrderTriggerHandler.disableTrigger = true;
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        activeMigratedOrder dab = new activeMigratedOrder(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X','w','W'});
        dab.execute(null);
        dab.start(null);
        dab.execute(null,ord );
        dab.finish(null);
        OrderProductTriggerHandler.disableTrigger = false;
		OrderTriggerHandler.disableTrigger = false;
        test.stopTest();  
        
    }
    
    public static Product2 generateFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generateChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
}