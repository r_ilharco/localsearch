global class TerminateFutureSubscriptions implements Schedulable{
    global void execute(SchedulableContext ctx) {
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c sub : [SELECT Id, Termination_Date__c, Subsctiption_Status__c 
                                         FROM SBQQ__Subscription__c 
                                         WHERE Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_IN_TERMINATION and Termination_Date__c = TODAY]){
                                             //LastModifiedDate != TODAY ---- is removed for test class purpose
                //Change subscription status
                sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
                subscriptionsToUpdate.add(sub);
            }
    
        if(subscriptionsToUpdate.size() > 0){
            update subscriptionsToUpdate;
        }
    }
}