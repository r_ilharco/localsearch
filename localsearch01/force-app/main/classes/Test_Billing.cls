@isTest
public class Test_Billing {
    @testSetup
    public static void testSetup(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
        AccountTriggerHandler.disableTrigger = true;
		byPassFlow.FlowNames__c = 'Community User Creation,Set Contract Start/End date for Quote,Subscriptions Handler,Send Owner Notification';
		insert byPassFlow; 
        //SBQQ.TriggerControl.disable(); 
        createAccount();
        List<Account> accounts = [SELECT Id FROM Account where Active_Contracts__c != null];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
        }
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        createOpp(accounts[0].id);
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c LIMIT 1];
        q.Billing_Profile__c = profiles[0].Id;
        QuoteTriggerHandler.disableTrigger = true;
        update q;
        QuoteTriggerHandler.disableTrigger = false;
        system.debug('quote: ' + q);
        generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        generateLocalization(prod.Id);
        QuoteLineTriggerHandler.disableTrigger = true;
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        QuoteLineTriggerHandler.disableTrigger = false;
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        generateContract(q.Id, accounts[0].id, opp.Id);
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        c.Status = 'Active';
        update c;
        SubscriptionTriggerHandler.disableTrigger = true;
        generateSubs(c, qls);
        SubscriptionTriggerHandler.disableTrigger = false;
        List<SBQQ__Subscription__c> subs = [select Id, SBQQ__Contract__r.AccountId, SBQQ__Contract__c, Subsctiption_Status__c, Next_Invoice_Date__c, SBQQ__QuoteLine__c, SBQQ__NetPrice__c, SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, SBQQ__Contract__r.SBQQ__Quote__c from SBQQ__Subscription__c ];
        system.debug('subs: ' + subs);
        createCreditNote(subs);
        createVatSchedules();
        createPrintingProfiles();
        system.debug('Printing profile: ' + [SELECT Id, Name ,Bill_Type__c,Contact_Mail__c ,Doc_Type__c ,From_Date__c,To_Date__c,Title__c ,Process_Mode__c,Language__c FROM Printing_Profile__c]);
        Numbering_cs__c num = new Numbering_cs__c();
        num.Next_Invoice_Number__c = 400000000;
        insert num;
        Mashery_Setting__c setting = new Mashery_Setting__c();
        setting.Client_Id__c = 'qmwyz3avgvkwq45qjq3recaqamdythnk';
        setting.Client_Secret__c = 'CFHCQrApkEjDy5zmyGX7gKDQ2HruNnfQ';
        setting.Expires__c = 1;
        setting.Token_Type__c = 'bearer';
        setting.Grant_Type__c = 'client_credentials';
        setting.Endpoint__c = 'https://apicloud-preprod.localsearch.tech/';
        setting.Token__c = 'e5brpmtgup9wx3cqsq7gwrd2';
        insert setting;
        Billing_Setting__mdt billingSetting = new Billing_Setting__mdt();
        billingSetting.Days_Until_Billing__c = 0;
        billingSetting.Label = ConstantsUtil.BILLING_SETTINGS_METADATA_LABEL;
        AccountTriggerHandler.disableTrigger = false;
        //Billing.collectAndSend(1);
        //Billing.generateInvoices(subs);
        //SBQQ.TriggerControl.enable(); 
    }
	
    public static void createAccount(){
        Account ac = new Account();
        ac.Name = 'TestNameAccount';
        ac.AddressValidated__c = 'validated_address';
        ac.BillingCity = 'Zürich';
        ac.BillingCountry = 'Switzerland';
        ac.BillingPostalCode = '8005';
        ac.BillingStreet = 'Förrlibuckstrasse 62';
        ac.Customer_Number__c = '2000258872';
        ac.LegalEntity__c = 'AG';
        ac.P_O_Box_City__c = 'Buchberg';
        ac.P_O_Box_Zip_Postal_Code__c = '8454';
        ac.PreferredLanguage__c = 'German';
        ac.Status__c = 'New';
        ac.Active_DMC_Contracts__c =1;
        ac.Active_Contracts__c =1;
        insert ac;
    } 
    
    public static void createOpp(Id accId){
        Date today = Date.today();
        Opportunity opp = new Opportunity();
        opp.AccountId = accId;
        opp.CloseDate = today.addDays(15);
        opp.Name = 'TestNameOpp';
        opp.StageName = 'Accepted';
        insert opp;
    }
    
    public static void generateQuoteLine(String quoteId, String productId, Date startDate, String requiredById, String billingFreq){
		SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__StartDate__c = startDate;
		quoteLine.CategoryID__c = 'testcategoryid';
        quoteLine.Category__c = 'Category Name';
        quoteLine.LocationID__c = 'testlocationid';
        quoteLine.Location__c = 'Location Name';
        quoteLine.SBQQ__Quantity__c= 1;
        quoteLine.SBQQ__ListPrice__c = 1290;
        quoteLine.Total__c = 490;
        quoteLine.Ad_Context_Allocation__c = 'testadcontextallocationid';
        quoteLine.Campaign_Id__c = 'testcampaignid';
        quoteLine.Contract_Ref_Id__c = 'testcontractrefid';
        quoteLine.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
        quoteLine.SBQQ__RequiredBy__c = requiredById;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.SBQQ__BillingFrequency__c =  billingFreq;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.Subscription_Term__c = '12';
		insert quoteLine;
	}
    
    public static void generateProduct(Boolean trueOrFalseOneTime){
        Product2 product = new Product2();
        //Required fields to fill
        product.Name = 'MyCOCKPIT Basic';
        product.Product_Group__c = 'MyCockpit';
        product.ProductCode = 'MCOBASIC001';
        product.ExternalKey_ProductCode__c = 'BCB';
        product.Family = 'Tool & Services';
        product.One_time_Fee__c = trueOrFalseOneTime;
        product.SBQQ__Component__c = false;
        product.SBQQ__NonDiscountable__c = false;
        product.SBQQ__SubscriptionTerm__c = 12;
        product.SBQQ__SubscriptionType__c = 'Renewable';
        product.Billing_Group__c = 'Monthly';
        product.Configuration_Model__c = 'Auto-renewal';
        //product.Subscription_Term__c = {'12'};
        product.Base_term_Renewal_term__c = '12';
        product.Event_Type__c = 'On Boarding';
        
        //Fields needed for Order Management Process
        product.Manual_Activation__c = true;
        product.Production__c = false;
        product.Priority__c = '10';
		//if(fieldApiNameToValue.containsKey('Upselling_Disable__c')) product.Upselling_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Upselling_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Downselling_Disable__c')) product.Downselling_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Downselling_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Upgrade_Disable__c')) product.Upgrade_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Upgrade_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Downgrade_Disable__c')) product.Downgrade_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Downgrade_Disable__c'));
        
        //Only for Print Products
        /*product.Adv_Width__c = fieldApiNameToValue.get('Adv_Width__c');
        product.Adv_Height__c = fieldApiNameToValue.get('Adv_Height__c');
        product.Adv_Type__c = fieldApiNameToValue.get('Adv_Type__c');
        product.Adv_Page_Type__c = fieldApiNameToValue.get('Adv_Page_Type__c');*/
        //End only for print products
        //End fields needed for Order Management Process
        
        //Standard values
        product.PlaceIDRequired__c = 'No';
        product.SBQQ__PricingMethod__c = ConstantsUtil.PRICING_METHOD;
        product.SBQQ__SubscriptionPricing__c = ConstantsUtil.SUB_PRICING;
        product.BillingChannel__c = ConstantsUtil.PROD_BILLING_CHANNEL;
        product.InvoiceCycleSet__c = ConstantsUtil.PROD_INVOICE_CYCLE_SET;
        product.SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL;
        product.SBQQ__TaxCode__c = ConstantsUtil.TAX_MODE;
        product.Period_of_notice__c = ConstantsUtil.PERIOD_NOTICE;
        product.IsActive = true;
        
        //Fields needed for billing
        product.KTRCode__c = '1202010005';
        product.KTRDesignation__c = 'MyCockpit Basic';
        product.KSTCode__c = '8932';
        product.Credit_Account__c = '334301';
        //End fields needed for billing
        
        //Needed fields for Trial
        product.Eligible_for_Trial__c = false;
        //End needed fields for Trial 
        
        insert product;
    }
    
    public static void generateContract(Id quoteId, Id AccountId, Id oppId){
        Contract c = new Contract();
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        c.SBQQ__Opportunity__c = oppId;
        c.SBQQ__Quote__c = quoteId;
        c.AccountId = AccountId;
        insert c;
    }
    
    public static void generateSubs(Contract contr, List<SBQQ__QuoteLine__c> quoteLines){
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, SBQQ__BillingType__c='Advance',
                                                        SBQQ__ChargeType__c = 'Recurring', SBQQ__Discount__c=0.0, SBQQ__ListPrice__c=quoteLine.SBQQ__ListPrice__c, 
                                                        SBQQ__Number__c = quoteLine.SBQQ__Number__c, SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c = FALSE, 
                                                        SBQQ__Product__c = quoteLine.SBQQ__Product__c, SBQQ__Quantity__c = quoteLine.SBQQ__Quantity__c, SBQQ__QuoteLine__c = quoteLine.Id, 
                                                        Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.today(), SBQQ__SubscriptionEndDate__c=null,
                                                        SBQQ__Contract__c = contr.Id, SBQQ__Account__c = contr.AccountId, SBQQ__Bundled__c = FALSE, Status__c = 'Active', Total__c = 500, Subscription_Term__c='12'));
        }
        insert subscriptions;
    }
    
    public static void createVatSchedules() {
        insert new List<Vat_Schedule__c> {
            new Vat_Schedule__c(Tax_Code__c = 'Standard', Start_Date__c=null, End_Date__c=Date.today().addDays(365), Value__c=7.7, Name='Standard 7.7%')
        };
    }
    
    public static void createPrintingProfiles(){
        Printing_Profile__c printingProfile = new Printing_Profile__c();
        printingProfile.Name = 'Printing Profile FAK_DE_STD';
        printingProfile.Bill_Type__c = 'PaymentSlip';
        printingProfile.Contact_Mail__c = 'testEmail@test.it';
        printingProfile.Doc_Type__c = 'FAK';
        printingProfile.From_Date__c = Date.today().addDays(-60);
        printingProfile.To_Date__c = Date.today().addDays(365);
        printingProfile.Title__c = 'Rechnung';
        printingProfile.Process_Mode__c = 'Standard1';
        printingProfile.Language__c = 'German';
        printingProfile.Sender__c = 'Swisscom Directories AG, Förrlibuckstrasse 62, Postfach, 8021 Zürich 1 UID: CHE-102.951.148 MWST';
        insert printingProfile;
        
        Printing_Profile__c pf = [SELECT Id FROM Printing_Profile__c LIMIT 1];
        
        List<Printing_Profile_Footer__c> ppfs = new List<Printing_Profile_Footer__c>();
        Printing_Profile_Footer__c ppf = new Printing_Profile_Footer__c ();
        ppf.Name = 'Printing Profile Footer FAK_DE_STD';
        ppf.Text__c = '<p>Printing Profile Footer FAK_DE_STD</p>';
        ppf.Type__c = 'Text';
        ppf.Printing_Profile__c = pf.id;
        ppfs.add(ppf);
        Printing_Profile_Footer__c ppf2 = new Printing_Profile_Footer__c ();
        ppf2.Name = 'Ppf Image with url';
        ppf2.Image_Url__c = 'testUrl';
        ppf2.Type__c = 'Image';
        ppfs.add(ppf2);
        Printing_Profile_Footer__c ppf3 = new Printing_Profile_Footer__c ();
        ppf3.Name = 'Ppf Image without url';
        ppf3.Type__c = 'Image';
        ppfs.add(ppf3);
        insert ppfs;
        
        Printing_Profile__c printingProfileGUT = new Printing_Profile__c();
        printingProfileGUT.Name = '	Printing Profile GUT_DE_STD';
        printingProfileGUT.Bill_Type__c = 'PaymentSlip';
        printingProfileGUT.Contact_Mail__c = 'testEmail@test.it';
        printingProfileGUT.Doc_Type__c = 'GUT';
        printingProfileGUT.From_Date__c = Date.today().addDays(-60);
        printingProfileGUT.To_Date__c = Date.today().addDays(365);
        printingProfileGUT.Title__c = 'Rechnung';
        printingProfileGUT.Process_Mode__c = 'Standard1';
        printingProfileGUT.Language__c = 'German';
        printingProfile.Sender__c = 'Swisscom Directories AG, Förrlibuckstrasse 62, Postfach, 8021 Zürich 1 UID: CHE-102.951.148 MWST';
        insert printingProfileGUT;
    }
    
    public static void createCreditNote(List<SBQQ__Subscription__c> subs){
        List<Credit_Note__c> creditNotes = new List<Credit_Note__c>();
        for(SBQQ__Subscription__c sub : subs){
            creditNotes.add(new Credit_Note__c(
                Account__c = sub.SBQQ__Contract__r.AccountId,
                Automated__c = FALSE,
                Contract__c = sub.SBQQ__Contract__c,
                Description__c = 'Test Credit Note N 1',
                Execution_Date__c = Date.today(),
                Period_From__c = Date.today().addDays(-60),
                Period_To__c = Date.today().addDays(60),
                Status__c = ConstantsUtil.CREDIT_NOTE_STATUS_READY_FOR_BILLING,
                Subscription__c = sub.id,
                Name = 'Credit Note Name',
                Value__c = 250
            ));
        }
        if(creditNotes.size() > 0){
            insert creditNotes;
        }       
    }
    
    public static void generateLocalization(Id prodId){
        SBQQ__Localization__c loc = new SBQQ__Localization__c();
        loc.SBQQ__APIName__c = 'Name';
        loc.SBQQ__Label__c = 'Product Name';
        loc.SBQQ__Language__c = 'it';
        loc.SBQQ__Text__c = 'testTrad';
        loc.SBQQ__Product__c = prodId;
        insert loc;
    }
    
    private static testmethod void invoiceCreationForIdAndMultipleSubscriptions(){
        Test.startTest();
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        List<SBQQ__Subscription__c> subs = [SELECT Id, SBQQ__RequiredById__c, SBQQ__QuoteLine__c, SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, SBQQ__Contract__r.SBQQ__Quote__c FROM SBQQ__Subscription__c];
        for(Integer i = 1; i < subs.size(); i++){
            subs[i].SBQQ__RequiredById__c = subs[0].id;            
        }
        update subs;
        system.debug('subs: ' + subs);
        List<Id> contractId = new List<Id>();
        contractId.add(c.id);
        Product2 prod = [SELECT Id, ProductCode FROM Product2 LIMIT 1];
        prod.ProductCode = 'slt001';
        update prod;
        Credit_Note__c cn = [SELECT Id, Nx_Refunded_Place__c FROM Credit_Note__c LIMIT 1];
        Place__c p = new Place__c();
        insert p;
        Place__c p2 = [SELECT Id FROM Place__c];
        cn.Nx_Refunded_Place__c = p2.id;
        update cn;
        Billing.collectAndSend(contractId);
        List<Invoice_Item__c> items = [SELECT Id FROM Invoice_Item__c];
        Set<Id> itemIds = new Set<Id>();
        for(Invoice_Item__c item : items){
            itemIds.add(item.id);
        }
        Billing.removeReplacedItems(itemIds);
        Billing.cancelContract(c.id);
        Test.stopTest();
    }
    
    private static testmethod void invoiceCreationForNumberOfContracts(){
        Test.startTest();
        Billing.collectAndSend(1);
        List<Invoice__c> invoices = [SELECT Id, Name, Invoice_Number__c FROM Invoice__c];
        system.debug('invoices: ' + invoices);
        Set<Id> invoiceIds = new Set<Id>();
        for(Invoice__c inv : invoices){
            invoiceIds.add(inv.id);
        }
        AbacusHttpResponseMock jsonResponse = new AbacusHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        SBQQ.TriggerControl.disable();  
        SwissbillingBulkifiedTransferJob swissbillingBulkBatchJob = new SwissbillingBulkifiedTransferJob();
        //Database.executeBatch(swissbillingBulkBatchJob, 10);
        List<sObject> objs = swissbillingBulkBatchJob.start(null);
        swissbillingBulkBatchJob.execute(null, objs);
        swissbillingBulkBatchJob.finish(null);
        SwissbillingBulkifiedTransferJob swissbillingBulkBatchJob2 = new SwissbillingBulkifiedTransferJob(invoiceIds);
        SwissbillingBulkifiedTransferJob swissbillingBulkBatchJob3 = new SwissbillingBulkifiedTransferJob(invoiceIds, invoiceIds);
        SBQQ.TriggerControl.enable(); 
        system.debug('invoices: ' + invoices);
        List<Id> invIds = new List<Id>(invoiceIds);
        Billing.resetSubscriptions(invIds);
        Test.stopTest();
    }

    private static testmethod void invoiceCreationForNumberOfContractsCanceledRework(){
        Test.startTest();
        Billing.collectAndSend(1);
        List<Invoice__c> invoices = [SELECT Id, Name, Invoice_Number__c FROM Invoice__c];
        system.debug('invoices: ' + invoices);
        Set<Id> invoiceIds = new Set<Id>();
        for(Invoice__c inv : invoices){
            inv.Status__c = ConstantsUtil.INVOICE_STATUS_CANCELLED;
            inv.Cancelled_Rework_Status__c = ConstantsUtil.INVOICE_CANCELED_REWORK_STATUS_TOBEINVOICED;
            inv.Invoice_Date__c = Date.today().addDays(-1);
            invoiceIds.add(inv.id);
        }
        update invoices;
        AbacusHttpResponseMock jsonResponse = new AbacusHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        SBQQ.TriggerControl.disable();  
        SwissbillingBulkifiedTransferJob swissbillingBulkBatchJob = new SwissbillingBulkifiedTransferJob();
        Database.executeBatch(swissbillingBulkBatchJob);
        List<Id> invIds = new List<Id>(invoiceIds);
        Billing.resetSubscriptions(invIds);
        Test.stopTest();
        System.assert([SELECT Id, Cancelled_Rework_Status__c FROM Invoice__c WHERE Cancelled_Rework_Status__c = :ConstantsUtil.INVOICE_CANCELED_REWORK_STATUS_RESENT].size() > 0);
    }

    private static testmethod void invoiceCreationForNumberOfContractsIntegrationStatusTest(){
        Test.startTest();
        Billing.collectAndSend(1);
        List<Invoice__c> invoices = [SELECT Id, Name, Invoice_Number__c FROM Invoice__c];
        system.debug('invoices: ' + invoices);
        Set<Id> invoiceIds = new Set<Id>();
        for(Integer i = 0;i <  invoices.size();i++){
            if(i == 1) {
                invoices[i].Integration_Status__c = ConstantsUtil.INVOICE_STATUS_TO_BE_DELETED;
            }
            if(i == 0) {
                invoices[i].Integration_Status__c = ConstantsUtil.INVOICE_STATUS_NOT_SENT;
            }
        }
        update invoices;
        AbacusHttpResponseMock jsonResponse = new AbacusHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        SBQQ.TriggerControl.disable();  
        SwissbillingBulkifiedTransferJob swissbillingBulkBatchJob = new SwissbillingBulkifiedTransferJob();
        Database.executeBatch(swissbillingBulkBatchJob);
        List<Id> invIds = new List<Id>(invoiceIds);
        Billing.resetSubscriptions(invIds);
        Test.stopTest();
        System.assert([SELECT Id FROM Invoice__c].size() ==  invoices.size());
    }
    
    public static testmethod void cancelInvoiceTestToSB(){
        Test.startTest();
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        List<Id> contractId = new List<Id>();
        contractId.add(c.id);
        Billing.collectAndSend(contractId);
        Invoice__c inv = [SELECT Id FROM Invoice__c LIMIT 1];
        CancelInvoiceController.getInvoice(null);
        SwissBillingHttpResponseMock jsonResponse = new SwissBillingHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        CancelInvoiceController.cancelByInvoiceId(inv.Id);
        Test.stopTest();
    }
    
    public static testmethod void cancelInvoiceTestToAbacus(){
        Test.startTest();
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        List<Id> contractId = new List<Id>();
        contractId.add(c.id);
        Billing.collectAndSend(contractId);
        Invoice__c inv = [SELECT Id FROM Invoice__c LIMIT 1];
        CancelInvoiceController.getInvoice(inv.id);
        AbacusHttpResponseMock jsonResponse = new AbacusHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        CancelInvoiceController.cancelByInvoiceId(inv.Id);
        Test.stopTest();
    }
    
    public static testmethod void taxHelperTesting(){

        Test.startTest();
        TaxHelper.getVatItems(ConstantsUtil.INVOICE_TAX_CODE_EXEMPTION, Date.today(), Date.today().addDays(65), 599, null);
        TaxHelper.getVatItems('Standard', Date.today(), Date.today(), 599, null);
        TaxHelper.getVatItems('Standard', Date.today(), Date.today().addDays(60), 599, Date.today().addDays(60));
        TaxHelper.getVatItemsForCreditNoteToAbacus('Standard', Date.today(), Date.today(), 599, Date.today().addDays(60));
        TaxHelper.getVatItemsForCreditNoteToAbacus('Standard', Date.today(), Date.today().addDays(60), 599, Date.today().addDays(50));
        Test.stopTest();
    }
    
    public static testmethod void swissbillingHelperTest(){
        Test.startTest();
        Billing.collectAndSend(1);
        List<Invoice__c> invoices = [SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c, 
                Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
                Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
                Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
                Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
                (SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
                FROM Invoice__c];    
        Credit_Note__c cn = [SELECT Id, Reimbursed_Invoice__c, Reimbursed_Invoice__r.Name FROM Credit_Note__c LIMIT 1];
        cn.Reimbursed_Invoice__c = invoices[0].id;
        update cn;
        Credit_Note__c cn2 = [SELECT Id, Reimbursed_Invoice__c, Reimbursed_Invoice__r.Name, Reimbursed_Invoice__r.Invoice_Date__c FROM Credit_Note__c LIMIT 1];
        system.debug('cn Reimbursed_Invoice__r.Invoice_Date__: ' + cn2.Reimbursed_Invoice__r.Invoice_Date__c);
        system.debug('invoices.size: ' + invoices.size());
        Integer index;
        Boolean credit = false;
        for(Invoice__c inv : invoices){
            if(inv.Amount__c < 0){
                index = invoices.indexOf(inv);
                credit = true;
            }
            system.debug('inv.amount:' + inv.Amount__c);
        }
        if(credit){
            Invoice__c inv = new Invoice__c(Id = cn2.Reimbursed_Invoice__c, Name = '450210456');
            update inv;
            List<Invoice_Order__c> invoiceOrders = [
                SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
                (SELECT Accrual_From__c, Accrual_To__c, Amount__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
                 Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c,
                 Product__r.ProductCode, Product__c, Subscription__c, Subscription__r.Place__c, Subscription__r.Place__r.Name, Subscription__r.Place__r.PostalCode__c, Subscription__r.Place__r.City__c,
                 Credit_Note__c, Credit_Note__r.Reimbursed_Invoice__r.Name, Credit_Note__r.Reimbursed_Invoice__r.Invoice_Date__c, Credit_Note__r.Period_From__c, Credit_Note__r.Period_To__c,
                 Credit_Note__r.Execution_Date__c, Credit_Note__r.Nx_Refunded_Place__c, Credit_Note__r.Manual__c, Credit_Note__r.Automated__c,
                 Subscription__r.SBQQ__RequiredById__c, Subscription__r.SBQQ__Product__r.One_time_Fee__c, Subscription__r.RequiredBy__r.Total__c,
                 Subscription__r.SBQQ__RequiredByProduct__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductName__c,
                 Subscription__r.SBQQ__QuoteLine__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c, Subscription__r.RequiredBy__c,
                 Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
                 Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__Description__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.Name, 
                 Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.PostalCode__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.City__c
                 FROM Invoice_Items__r
                ),
                Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
                FROM Invoice_Order__c where Invoice__c in :invoices
                ORDER BY Item_Row_Number__c
            ];
            
            Printing_Profile__c printingProfile = SwissbillingHelper.getPrintingProfile('German',invoices.get(index).Invoice_Code__c, invoices.get(index).Bill_Type__c,
                                                                                        invoices.get(index).Process_Mode__c, invoices.get(index).Invoice_Date__c);
            SwissbillingHelper.getGroupList(invoiceOrders, invoices.get(index), printingProfile);  
        }
        Datetime dt;
        SwissbillingHelper.toISO8601String(dt);
        Date d;
        SwissbillingHelper.toISO8601String(d);
        SwissbillingHelper.getDocTypeByCode('test');
        SwissbillingHelper.getBillingChannelValue('test');
        List<Invoice_Tax__c> taxes = new List<Invoice_Tax__c>();
        SwissbillingHelper.getTaxList(taxes);
        List<Printing_Profile_Footer__c> ppfs = [SELECT Id, Type__c, Image_Url__c FROM Printing_Profile_Footer__c WHERE Type__c = 'Image' AND Image_Url__c = null];
        try{
            SwissbillingHelper.getFooterList(ppfs);
        }catch(Exception e){
            system.debug('exception (footer): ' + e.getMessage());
        }        
        List<Printing_Profile_Footer__c> ppfs2 = [SELECT Id,Type__c, Image_Url__c FROM Printing_Profile_Footer__c WHERE Type__c = 'Image' AND Image_Url__c != null];
        SwissbillingHelper.getFooterList(ppfs2);
        SwissbillingHelper.getFullName('testFirst', 'testLast');
        SwissbillingHelper.getFullName('testFirst', null);
        SwissbillingHelper.getPeriodEndDate(ConstantsUtil.BILLING_FREQUENCY_SEMIANNUAL, Date.today(), Date.today().addDays(365));
        SwissbillingHelper.getPeriodEndDate(ConstantsUtil.BILLING_FREQUENCY_QUARTERLY, Date.today(), Date.today().addDays(365));
        SwissbillingHelper.getPeriodEndDate(ConstantsUtil.BILLING_FREQUENCY_MONTHLY, Date.today(), Date.today().addDays(365));
        try{
            SwissbillingHelper.getPeriodEndDate(ConstantsUtil.BILLING_FREQUENCY_INVOICE_PLAN, Date.today(), Date.today().addDays(365));
        }catch(Exception e){
            system.debug('exception (periodEndDate): ' + e.getMessage());
        }  
        SwissbillingHelper.getPeriodEndDate('test', Date.today(), Date.today().addDays(5));
        Invoice__c inv = [SELECT Id, Name, Correlation_Id__c,  Status__c, Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Open_Amount__c, Total__c,
                          Billing_Profile__r.Billing_Contact__c, Billing_Profile__r.Billing_Language__c, (SELECT Contract__c FROM Invoice_Orders__r LIMIT 1)
                          FROM Invoice__c WHERE Name != 'Pending' LIMIT 1];
        SBHelperHttpResponseMock sbHelperMock = new SBHelperHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, sbHelperMock);
        SwissbillingHelper.cancelInvoice(inv);
        List<SBQQ__Subscription__c> subs = [SELECT Id FROM SBQQ__Subscription__c];
        SwissbillingHelper.contractsToBeUpdated(subs);
        Test.stopTest();
    }
    
    public class SwissBillingHttpResponseMock implements HttpCalloutMock {
        //public integer answerType = 200;
        public string invoiceDocId = 'invoice-id';
       
        public HTTPResponse respond(HTTPRequest req) {
            system.debug('TestReq: ' + req);
            system.debug('req.getEndpoint(): ' + req.getEndpoint());
            // Assert body and headers are as expectedcse
            HttpResponse res = new HttpResponse();
            //if(req.getEndpoint().contains('status')){
                Map<string, object> responseMap = new Map<string, object> {
                    'open_amount' => '15',
                        'total_amount_incl_tax' => '20',
                        'payment_status' => '1'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
            //}
            /*switch on answerType {
		when 200 {
                    Map<string, object> responseMap = new Map<string, object> {
                        'open_amount' => '15',
                        'total_amount_incl_tax' => '25'
                    };
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody(JSON.serialize(responseMap));
                    res.setStatusCode(200);                
                }
                when 400 {
                    res.setBody('<h1>400 - Bad Request</h1>');
                    res.setStatusCode(400);                     
                }
                when 401 {
                    res.setBody('<h1>401 - Unauthorized</h1>');
                    res.setStatusCode(401);                     
                }
            }*/
            
            return res;
        }
    }

    public class SwissBillingHttpResponseMock2 implements HttpCalloutMock {
        //public integer answerType = 200;
        public string invoiceDocId = 'invoice-id';
       
        public HTTPResponse respond(HTTPRequest req) {
            system.debug('TestReq: ' + req);
            system.debug('req.getEndpoint(): ' + req.getEndpoint());
            // Assert body and headers are as expectedcse
            HttpResponse res = new HttpResponse();
            //if(req.getEndpoint().contains('status')){
                Map<string, object> responseMap = new Map<string, object> {
                    'open_amount' => '20',
                        'total_amount_incl_tax' => '20',
                        'payment_status' => 'c1'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
       
            
            return res;
        }
    }
    
    public class AbacusHttpResponseMock implements HttpCalloutMock {
        public string invoiceDocId = 'invoice-id';
       
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            if(req.getEndpoint().contains('token')){
                Map<string, object> responseMap = new Map<string, object> {
                    'access_token' => '123456789',
                        'expires_in' => '3600',
                        'token_type' => 'bearer'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
            }else{
                Map<string, object> responseMap = new Map<string, object> {
                    'open_amount' => '15',
                        'total_amount_incl_tax' => '15'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
            }
            
            
            
                        
            return res;
        }
    }
    
    public class SBHelperHttpResponseMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            return res;
        }
    }
}