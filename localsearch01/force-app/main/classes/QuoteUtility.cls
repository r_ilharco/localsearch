public without sharing class QuoteUtility {
    
    @AuraEnabled
    public static Boolean compareOwner(Id recordId, String currentUser){
        try{
            SBQQ__Quote__c quote = [SELECT id,
                                    name,
                                    OwnerId
                                    from SBQQ__Quote__c where id= :recordId];
            
            Id profileId = userinfo.getProfileId();
            System.debug('Profile id: '+profileId);
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;    
            
            if(profileName == 'System Administrator' || quote.OwnerId == currentUser){
                return true;
            }
            else {
                return false;
            }
            
        } catch(Exception e){
            system.debug(e.getMessage());
            return null;
        }
    }
    
    
    @AuraEnabled
    public static SBQQ__Quote__c getOpportunityData(Id recordId){
        try{
            SBQQ__Quote__c quote = [SELECT id,
                                    name,
                                    SBQQ__Primary__c,
                                    SBQQ__Opportunity2__r.Id,
                                    SBQQ__Opportunity2__r.Name, 
                                    SBQQ__Opportunity2__r.StageName, 
                                    SBQQ__Opportunity2__r.Opportunity_reason__c 
                                    from SBQQ__Quote__c where id= :recordId];
            
            return quote;
        } catch(Exception e){
            system.debug(e.getMessage());
            return null;
        }
    }
    
    @AuraEnabled
    public static Map<String,String> getStage(){
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> stageValues = fieldResult.getPicklistValues();
        Map<String, String> options = new Map<String,String>();
        for (Schema.PicklistEntry stage: stageValues) {
            if(stage.getValue() == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON || stage.getValue() == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST){
                options.put(stage.getValue(), stage.getLabel());
            }
        }
        return options;
    }
    
    @AuraEnabled
    public static Map<String,String> getCloseReason(){
        Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_reason__c.getDescribe();
        List<Schema.PicklistEntry> lossReason = fieldResult.getPicklistValues();
        Map<String, String> options = new Map<String,String>();
        for (Schema.PicklistEntry reason: lossReason) {
            options.put(reason.getValue(), reason.getLabel());
        }
        
        return options;
    }
    
    @AuraEnabled
    public static void closeOpp(String oppId, String closeStage, String closeLostReason){
        system.debug(oppId);
        try{
            
            Opportunity opp = [Select ID, Name, StageName, Opportunity_reason__c From Opportunity where id = :oppId];
            opp.StageName = closeStage;
            if(String.isNotBlank(closeStage)){
                opp.Opportunity_reason__c = closeLostReason;
            }
            system.debug(opp.StageName + ' -----> ' + opp.Opportunity_reason__c);
            // update opp;
            Database.SaveResult SR = Database.update(opp); 
            
            system.debug('SR.get' +SR.getErrors());      
        }catch(DmlException ex) {
            throw new AuraHandledException(ex.getDmlMessage(0));
        }catch(Exception e){
            system.debug('eMess '+e.getMessage());
            system.debug('eMess '+e.getLineNumber());
            system.debug('eMess '+e.getTypeName());
            system.debug('eMess '+e.getCause());
            
            
            throw new AuraHandledException(e.getMessage());
        }
    }
}