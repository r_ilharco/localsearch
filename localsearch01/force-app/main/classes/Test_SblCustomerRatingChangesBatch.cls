@isTest
public class Test_SblCustomerRatingChangesBatch {
    
    @testSetup
    static void setup() {
        //Test_Billing.testSetup();
        //Billing.collectAndSend(2);
        List<Account> accs = Test_DataFactory.createAccounts('testAcc', 2);
        insert accs;
        accs[0].Customer_Number__c = accs[1].Id;
        for(Account a: accs)
            a.ClientRating__c = 'N-Unknown';
        update accs;
        AccountTriggerHandler.disableTrigger = true;
        
    }
    
    @isTest
    public static void test_runSblCustomerRatingChangesBatch(){
        
        List<String> ids = new List<String>();
        Map<String, BillingResults.CustomerRatingUpdate> resultMap = new Map<String, BillingResults.CustomerRatingUpdate>();
        for(Account acc: [SELECT Id, Name, Customer_Number__c FROM Account]){
        	BillingResults.CustomerRatingUpdate bill = new BillingResults.CustomerRatingUpdate();
            resultMap.put(''+acc.Id, bill);
            System.debug('***CUSTOMER: '+acc);
    	}
        
        Set<String> accountIds = new Set<String>();
        accountIds.addAll(resultMap.keySet());
        System.debug('***RESULT MAP: '+ resultMap);
        System.debug('***QUERY RESULT: '+ [SELECT Id, Customer_Number__c,Amount_At_Risk__c,ClientRating__c FROM Account WHERE Customer_Number__c IN: accountIds]);
        
        
        Test.startTest();
        SblCustomerRatingChangesBatch sblRCB = new SblCustomerRatingChangesBatch(resultMap);
        Id batchId = Database.executeBatch(sblRCB);
        Test.stopTest(); 

	}
    
    
}