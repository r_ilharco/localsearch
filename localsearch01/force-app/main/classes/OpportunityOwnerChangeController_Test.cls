@isTest
public class OpportunityOwnerChangeController_Test {
    
    @isTest
    static void setup() {
        UserTriggerHandler.disableTrigger=true;
        Test_DataFactory.insertFutureUsers();
        UserTriggerHandler.disableTrigger=false;
        List<Account> accs = Test_DataFactory.generateAccounts('Test', 1, false);
        accs[0].Assigned_Territory__c = 'Territory 2.01';
        insert accs;
    }
    
    
    @isTEst
    public static void changeOwner_test(){
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        
        insert account;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        
        
        
        Map<String, Id> profilesMap = Test_DataFactory.getProfilesMap(); 
        List<User> userList = new List<User>();
        User telesalesUser = Test_DataFactory.generateUser();
        telesalesUser.ProfileId = profilesMap.get(ConstantsUtil.TELESALESPROFILE_LABEL);
        telesalesUser.Username = 'telesalestestuser@ls.ch.test';
        telesalesUser.Code__c = 'DMC';
        userList.add(telesalesUser);
        User salesUser = Test_DataFactory.generateUser();
        salesUser.ProfileId = profilesMap.get('Sales');
        salesUser.Username = 'salesUsertestuser@ls.ch.test';
        salesUser.Code__c = 'DMC';
        userList.add(salesUser);
        UserTriggerHandler.disableTrigger = true;
        insert userList;
        UserTriggerHandler.disableTrigger = false;
        
        OpportunityTriggerHandler.disableTrigger = true;
        opportunity.OwnerId = telesalesUser.Id;
        insert opportunity;
        OpportunityTriggerHandler.disableTrigger = false;
        
        
        test.startTest();
        List<opportunity> opp = [Select Id from opportunity];
        List<String> oppIds = new List<String>();
        for(opportunity o : opp){
            oppIds.add(o.Id);
        }
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(opp);
        OpportunityOwnerChangeController  obj = new OpportunityOwnerChangeController(sc);		
        OpportunityOwnerChangeController.changeOpportunityOwner(oppIds);
        
        
        system.runAs(telesalesUser){
            OpportunityOwnerChangeController.changeOpportunityOwner(oppIds);
        }
        system.runAs(salesUser){
            OpportunityOwnerChangeController.changeOpportunityOwner(oppIds);
        }
        
        opportunity.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS;
        OpportunityTriggerHandler.disableTrigger = true;
        update opportunity;
        List<opportunity> opp2 = [Select Id from opportunity];
        List<String> oppIds2 = new List<String>();
        for(opportunity o : opp2){
            oppIds2.add(o.Id);
        }
        OpportunityOwnerChangeController.changeOpportunityOwner(oppIds2);
        OpportunityTriggerHandler.disableTrigger = false;
        test.stopTest();
        
       
        
        
    }
}