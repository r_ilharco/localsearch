@isTest
public class AccountTerritoryManagement_BatchTest {
    @testSetup static void setup(){
        test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users2 = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users2[0];
        system.runas(testuser){
            List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
            PermissionSetAssignment psain = new PermissionSetAssignment();
            psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
            psain.AssigneeId = testuser.Id;
            psas.add(psain);
            insert psas;
        }
        PermissionSetAssignment PSA = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        string UserId = PSA.AssigneeId;
        user u = [select id from user where id=: UserId][0];
        system.runas(u){
            List<Account> lstAccount = Test_DataFactory.createAccounts('Test', 20);
            for (account a : lstAccount){
                a.dataSource__c = 'LCM';
                a.AccountSource = ConstantsUtil.ACC_SOURCE_MANUAL_LCM;
                a.OwnerId = u.id;
            }
            insert lstAccount;
        }
        List<User> users = Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        List<Territory2> terrs = new List<Territory2>();
		System.RunAs(u){        
            Territory2Type terrType  = [SELECT id, DeveloperName from Territory2Type where DeveloperName =: ConstantsUtil.TERRITORY_TYPE LIMIT 1][0];
        	Territory2Model terrModel = [SELECT id,name from territory2Model LIMIT 1];
            Territory2 t3 = new Territory2(Name = 'TerritoryLevel3',Territory2ModelId=terrModel.Id, DeveloperName = 'TerritoryLevel3', Territory2TypeId = terrType.id);
            insert t3;
            terrs.add(t3);
            Territory2 t2 = new Territory2(Name = 'TerritoryLevel2',Territory2ModelId=terrModel.Id, ParentTerritory2Id = t3.id,  DeveloperName = 'TerritoryLevel2', Territory2TypeId = terrType.id);
            insert t2;
            terrs.add(t2);
            Territory2 t1 = new Territory2(Name = 'TerritoryLevel1',Territory2ModelId=terrModel.Id, ParentTerritory2Id = t2.id,  DeveloperName = 'TerritoryLevel1', Territory2TypeId = terrType.id);
            insert t1;
            terrs.add(t1);
        }
    }    
    
    @isTest
    public static void AccountTerritoryManagementLevel1(){   
        List <Account> lstAccount = [select id from Account];
        List<Territory2> terrs = [select id, name from Territory2 WHERE Territory2Model.State = 'Active'];
        Test.startTest();
        List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
        for(Account a : lstAccount){
            ObjectTerritory2Association obj = new ObjectTerritory2Association();
            obj.ObjectId = a.id;
            obj.Territory2Id = terrs[0].id;
            obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
            lstTerr.add(obj);
        }      
        insert lstTerr;
        list<string> values = new list<string>();
        values.add('Tstring');
        AccountTerritoryManagement_Batch obj = new AccountTerritoryManagement_Batch(values);
        DataBase.executeBatch(obj);
        obj.execute(null,lstTerr);
        obj.execute(null);
        Test.stopTest();
    }
    
    @isTest
    public static void AccountTerritoryManagementLevel2(){   
        List <Account> lstAccount = [select id from Account];
        List<Territory2> terrs = [select id, name from Territory2 WHERE Territory2Model.State = 'Active'];
        terrs[3].ParentTerritory2 = terrs[4];
        Test.startTest();
        List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
        for(Account a : lstAccount){
            ObjectTerritory2Association obj = new ObjectTerritory2Association();
            obj.ObjectId = a.id;
            obj.Territory2Id = terrs[3].id;
            obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
            obj.Territory2 = terrs[3];
            lstTerr.add(obj);
        }      
        insert lstTerr;
        list<string> values = new list<string>();
        values.add('Tstring');
        AccountTerritoryManagement_Batch obj = new AccountTerritoryManagement_Batch(values);
        DataBase.executeBatch(obj);
        obj.execute(null,lstTerr);
        Test.stopTest();
    }
   /* @isTest
    public static void AccountTerritoryManagementLevel3(){   
        PermissionSetAssignment PSA = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        string UserId = PSA.AssigneeId;
        user u = [select id from user where id=: UserId][0];
        system.runas(u){
            List <Account> lstAccount = [select id from Account];
            
            List<Territory2> terrs = [select id from Territory2];
            terrs[0].name = '123456';
            terrs[0].ParentTerritory2 = terrs[1];
            terrs[1].parentTerritory2 = terrs[2];
            terrs[2].name = '654321';
            Test.startTest();
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : lstAccount){
                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = terrs[2].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                obj.Territory2 = terrs[0];
                lstTerr.add(obj);
            }      
            insert lstTerr;
            List<UserTerritory2Association> userTerrAssoList = new List<UserTerritory2Association>();
            UserTerritory2Association objUserTerritory2Association1 = new UserTerritory2Association(Territory2Id= terrs[0].Id, UserId= u.Id, RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
    		UserTerritory2Association objUserTerritory2Association2 = new UserTerritory2Association(Territory2Id= terrs[1].Id, UserId= u.Id, RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            UserTerritory2Association objUserTerritory2Association3 = new UserTerritory2Association(Territory2Id= terrs[2].Id, UserId= u.Id, RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
			userTerrAssoList.add(objUserTerritory2Association1);
            userTerrAssoList.add(objUserTerritory2Association2);
            userTerrAssoList.add(objUserTerritory2Association3);
            insert userTerrAssoList;
            list<string> values = new list<string>();
            values.add('Tstring');
            AccountTerritoryManagement_Batch obj = new AccountTerritoryManagement_Batch(values);
            DataBase.executeBatch(obj);
            obj.execute(null,lstTerr);
            Test.stopTest();
        }
    }*/
    //OAVERSANO 20191008 Enhancement #33 -- START
    /* @isTest
public static void AccountTerritoryManagementLevel4(){   
List <Account> lstAccount = [select id from Account];
List<Territory2> terrs = [select id from Territory2 order by DeveloperName desc LIMIT 2];
List<User> users = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL,1);
system.runAs(users[0]){
UserTerritory2Association usTerrAss = new UserTerritory2Association(UserId = users[0].Id, Territory2Id = terrs[0].Id, RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP);
insert usTerrAss;
}
Test.startTest();
List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
for(Account a : lstAccount){
ObjectTerritory2Association obj = new ObjectTerritory2Association();
obj.ObjectId = a.id;
obj.Territory2Id = terrs[0].id;
obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
lstTerr.add(obj);
}      
insert lstTerr;
list<string> values = new list<string>();
values.add('Tstring');
AccountTerritoryManagement_Batch obj = new AccountTerritoryManagement_Batch(values);
DataBase.executeBatch(obj);
obj.execute(null,lstTerr);
Test.stopTest();
}*/
    //OAVERSANO 20191008 Enhancement #33 -- END
}
