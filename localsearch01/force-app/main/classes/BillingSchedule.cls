global class BillingSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
		Billing_Batch_Size_cs__c batchSizeSetting = Billing_Batch_Size_cs__c.getOrgDefaults();
        Integer numberOfContracts = (Integer)batchSizeSetting.Size_Value__c;
        if(!Test.isRunningTest())Billing.collectAndSend(numberOfContracts);
    }
}