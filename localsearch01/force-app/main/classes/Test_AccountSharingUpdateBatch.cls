@isTest
public class Test_AccountSharingUpdateBatch {
	static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
            insert byPassTrigger;
		}
    
	@TestSetup
    public static void setup(){
        insertBypassFlowNames();
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        acc[0].Territory_Changed__c = true;
        acc[0].Owner_Changed__c = true;
        insert acc;
      
    }
    
    @isTest
    public static void test_AccountSharingUpdateBatch(){  
                
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        Database.BatchableContext db = null;
        AccountSharingUpdateBatch tsc = new AccountSharingUpdateBatch();
        Database.executeBatch(tsc,100);
        //tsc.execute(null);
        //tsc.start(db);
        //tsc.execute(db, new list<Account>{listAccount});
       // tsc.finish(db);
        //listAccount.Owner_Changed__c = true;
        //update listAccount;
        //tsc.execute(db, new list<Account>{listAccount});
        //tsc.finish(db);
        Test.stopTest(); 
    }
}