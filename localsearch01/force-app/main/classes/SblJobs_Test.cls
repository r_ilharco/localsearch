@IsTest
public class SblJobs_Test {
    
    @testSetup
    static void setupData(){
        List<Account> accounts = Test_DataFactory.createAccounts('Test',1);
        accounts[0].Customer_Number__c = '9B80B84D-2EAB-4003-BA91-723DA772D758';
        insert accounts;
        
        List<Invoice__c> invoices = Test_DataFactory.createInvoices(1, 'Active', 'Standard0');
        invoices[0].Correlation_Id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B505';
        insert invoices;
    }

    static testmethod void test_CustomerRatingJob() {
        BillingResults_Test.HttpResponseMockProxy proxy =  BillingResults_Test.buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
		System.enqueueJob(new SblCustomerRatingChangesJob(Date.newInstance(2019, 1, 1), null));
		System.enqueueJob(new SblCustomerRatingChangesJob());
        SblCustomerRatingChangesJob test = new SblCustomerRatingChangesJob();
        System.Test.stopTest();
    }

    static testmethod void test_DeliveriesChangesJob() {
        BillingResults_Test.HttpResponseMockProxy proxy =  BillingResults_Test.buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
		System.enqueueJob(new SblDeliveriesChangesJob(Date.newInstance(2019, 1, 1), null));
		System.enqueueJob(new SblDeliveriesChangesJob());
        SblDeliveriesChangesJob test = new SblDeliveriesChangesJob();
        System.Test.stopTest();
    }
    
    static testmethod void test_InvoiceChangesJob() {
        BillingResults_Test.HttpResponseMockProxy proxy =  BillingResults_Test.buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
		System.enqueueJob(new SblInvoiceChangesJob(Date.newInstance(2019, 1, 1), null));
		System.enqueueJob(new SblInvoiceChangesJob());
        SblInvoiceChangesJob test = new SblInvoiceChangesJob();
        System.Test.stopTest();
    }     
}