/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29        
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to fields listed in SPIII 1057 removed
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-03     
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/


@isTest
public class Test_Trigger {
    @TestSetup
    public static void makeData () {
        List<Account> accounts = Test_DataFactory.createAccounts('testAccount', 2);
        insert accounts;
        
        Product2 product = generteFatherProduct_localBANNER();
        insert product;
        
        Opportunity oppo = test_DataFactory.generateOpportunity('Test Oppo', accounts[0].Id);
        insert oppo;
        
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        insert contacts;
        String opportunityId = oppo.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        QuoteLineTriggerHandler.disableTrigger = true;
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today(), contacts[0].Id, null, null, 'Annual');
        fatherQL.Url__c = 'https://test.it';
        fatherQL.Url2__c = 'https://test.it';
        fatherQL.Url3__c = 'https://test.it';
        fatherQL.Url4__c = 'https://test.it';
        insert fatherQL;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        Place__c place = new Place__c();
        place.Name = 'test place';
        place.LastName__c = 'test last';
        place.Company__c = 'test company';
        place.City__c = 'test city';
        place.Country__c = 'test coountry';
        place.PostalCode__c = '1234';
        
        insert place;
        
        
        List<Contact> contactsInsert = Test_DataFactory.createContacts(accounts[1].id, 1);
        insert contactsInsert;
    }
    
    @isTest
    static void test_Account()
    {
        Test.startTest();
            List<Account> accounts = Test_DataFactory.generateAccounts('AccountTest XYZ', 1, false);
            List<Account> accounts1 = Test_DataFactory.generateAccounts('Test Name', 1, false);
            accounts[0].Customer_Number__c = null;
            accounts[0].LegalEntity__c = 'Foundation';
            accounts1[0].BillingStreet = 'ABCDE xxx';
            accounts1[0].Customer_Number__c = '1234';
            accounts1[0].UID__c = '';
            accounts1[0].GoldenRecordID__c = '000000001';
            accounts1[0].LegalEntity__c = 'Association';
            
            insert accounts[0];
            insert accounts1[0];
        
            accounts[0].Customer_Number__c = '1111';
            update accounts[0];
        Test.stopTest();
    }
    
    @isTest static void test_ApprovalACL(){

        Id recordTypeAcc = [SELECT id from RecordType WHERE DeveloperName = 'TypAcc'].Id;
        Id recordTypeCon = [SELECT id from RecordType WHERE DeveloperName = 'TypCon'].Id;
        Id recordTypeLead = [SELECT id from RecordType WHERE DeveloperName = 'TypLea'].Id;

        Test.startTest();
            
            Account newAccount = new Account(
                Name = 'aTest1',
                P_O_Box_City__c = 'aTest1',
                P_O_Box_Zip_Postal_Code__c = 'aTest1',
                POBox__c = 'aTest1',
                PreferredLanguage__c = 'German',
                Status__c = 'New'
            );
            insert newAccount;
        
            ApprovalACL__c appAccount = new ApprovalACL__c(
                //Name = 'Test1',
                RecordTypeId = recordTypeAcc,
                acc_AccountName__c = 'Test1',
                acc_Billing_City__c = 'Test1',
                acc_Billing_Country__c = 'Test1',
                acc_Billing_Zip_PostalCode__c = 'Test1',
                acc_P_O_Box_City__c = 'Test1',
                acc_P_O_Box_Zip_Postal_Code__c = 'Test1',
                acc_POBox__c = 'Test1',
                acc_PreferredLanguage__c = 'German',
                acc_Status__c = 'New'
            );
            
            insert appAccount;
            
            List <Account> lstAccount = [SELECT Id FROM Account LIMIT 1];
        
            Id idAccount = lstAccount[0].Id; 
        
            ApprovalACL__c appContact = new ApprovalACL__c(
           // Name = 'TestCon1',
            RecordTypeId = recordTypeCon,
            con_AccountId__c = newAccount.id,
            con_Business_Phone__c = '123456789',
            con_Email__c = 'TestCon1@testcon1.test',
            con_FirstName__c = 'TestCon1',
            con_GoldenRecordID__c = 'TestCon1',
            con_LastName__c = 'TestCon1',
            con_PO_Box__c = 'TestCon1',
            con_PO_BoxCity__c = 'TestCon1',
            con_PO_BoxZip_PostalCode__c = 'TestCon1',
            con_Position__c = 'TestCon1'
            );
            
            insert appContact;

            ApprovalACL__c appLead = new ApprovalACL__c(
                //Name = 'TestLea1',
                RecordTypeId = recordTypeLead,
                lea_Company__c = 'TestLea1',
                lea_FirstName__c = 'TestLea1',
                lea_LastName__c = 'TestLea1',
                lea_LeadSource__c = 'Advertising',
                lea_MobilePhone__c = '123456789',
                lea_P_O_Box__c = 'TestLea1',
                lea_P_O_Box_City__c = 'TestLea1',
                lea_P_O_Box_Zip_Postal_Code__c = 'TestLea1',
                lea_Phone__c = '123456789',
                lea_Rating__c = 'Promising',
                lea_Status__c = 'Open'
            );
            insert appLead;

        list <ApprovalACL__c> lstApproval = [SELECT Check2__c FROM ApprovalACL__c ];
        list <ApprovalACL__c> updApp= new List<ApprovalACL__c>();
        
        for(ApprovalACL__c app1 : lstApproval) {
            app1.check2__c = 'OK';
            updApp.add(app1);
        }
        update updApp;
        
        Test.stopTest();
        
        
    }
    
    @isTest
    static void test_BillingProfile()
    {
        Test.startTest();
            List<Account> accounts = [SELECT Id, UID__c FROM Account WHERE Name = 'testAccount 0' LIMIT 1];
            System.debug('Accounts ' + accounts);
            List<Contact> contacts = [SELECT Id FROM Contact WHERE LastName = 'LastName' LIMIT 1];
            List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts, contacts, 1);
            insert profiles;
        Test.stopTest();
    }
    
    /*@isTest
    static void test_Case()
    {
        ByPassFlow__c bpf = insertBypassFlowNames_Case();
        List<Case> cList = new List<Case>();
        List<Case> cList2 = new List<Case>();
        List<Case> cList3 = new List<Case>();
        List<Case> cList4 = new List<Case>();
        
        Account customerAcc = new Account(Name='customerAccount');
        customerAcc.Type = 'Customer';
        customerAcc.PreferredLanguage__c= 'German';
        customerAcc.Status__c = 'New';
        customerAcc.BillingCity = 'Zürich';
        customerAcc.BillingCountry = 'Switzerland';
        customerAcc.BillingPostalCode = '8005';
        customerAcc.BillingStreet = 'Förrlibuckstrasse, 62';
        customerAcc.Phone = '+39999999';
        customerAcc.Email__c = 'oggi@ciao.com.net';
        customerAcc.Customer_Number__c = '370980914380193402183823752875028734832';
        insert customerAcc;
        
        Contact secondCont = new Contact(LastName='secondContact');
        secondCont.MailingCity = 'Zürich';
        secondCont.MailingCountry = 'Switzerland';
        secondCont.MailingPostalCode = '8005';
        secondCont.MailingStreet = 'Förrlibuckstrasse, 62';
        secondCont.AccountId = customerAcc.Id;
        secondCont.Phone = '+35555555';
        secondCont.Email = 'albero@bello.com.net';
        insert secondCont;
        
        Contact primaryCont = new Contact(LastName='primaryContact');
        primaryCont.MailingCity = 'Zürich';
        primaryCont.MailingCountry = 'Switzerland';
        primaryCont.MailingPostalCode = '8005';
        primaryCont.MailingStreet = 'Förrlibuckstrasse, 62';
        primaryCont.AccountId = customerAcc.Id;
        primaryCont.Primary__c = true;
        insert primaryCont;
        
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
        
        User u = new user();
        u.LastName = 'mylastName';
        u.Email = 'testclass@test.class';
        u.Alias = 'tclass';
        u.Username = 'testclass@test.class';
        u.CommunityNickname = 'testclass@test.class';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u; 
        
        Test.startTest();
            Case case1 = new Case();
            case1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Customer Data').getRecordTypeId();
            case1.Status = 'New';
            case1.AccountId = customerAcc.Id;
            case1.Origin = 'Phone';
            case1.Topic__c = 'Change Customer Data';
            //case1.Sub_Topic__c = 'Quality-Check';
            case1.Language__c = 'German';
            cList.add(case1);
            
            Case case2 = new Case();
            case2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Other').getRecordTypeId();
            case2.Status = 'New';
            case2.ContactId = primaryCont.Id;
            case2.Origin = 'Email';
            case2.Language__c = 'German';
            cList.add(case2);
            
            insert cList;     
            
            Case case3 = new Case();
            case3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Customer Data').getRecordTypeId();
            case3.Status = 'New';
            case3.AccountId = customerAcc.Id;
            case3.Origin = 'Phone';
            case3.Topic__c = 'Change Customer Data';
            //case3.Sub_Topic__c = 'Quality-Check';
            case3.Language__c = 'French';
            cList2.add(case3);
            
            Case case4 = new Case();
            case4.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Other').getRecordTypeId();
            case4.Status = 'New';
            case4.SuppliedEmail = 'albero@bello.com.net';
            case4.Origin = 'Email';
            case4.Subject = 'GE03I';
            case4.Sub_Topic__c = 'Contract Copy';
            cList2.add(case4);
            
            insert cList2;
            
            Case case5 = new Case();
            case5.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case5.Status = 'New';
            case5.SuppliedEmail = 'oggi@ciao.com.net';
            case5.Origin = 'Web';
            case5.Topic__c = 'Contract Information and Changes';
            case5.Sub_Topic__c = 'Replacement';
            case5.Language__c = 'French';
            cList3.add(case5);
            
            Case case6 = new Case();
            case6.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case6.Status = 'New';
            case6.SuppliedPhone = '+39999999';
            case6.Origin = 'Web';
            case6.Topic__c = 'Contract Information and Changes';
            case6.Sub_Topic__c = 'Replacement';
            case6.Language__c = 'French';
            cList3.add(case6);
            
            Case case7 = new Case();
            case7.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case7.Status = 'New';
            case7.SuppliedEmail = 'albero@bello.com.net';
            case7.Origin = 'Web';
            case7.Topic__c = 'Finance';
            case7.Language__c = 'French';
            cList3.add(case7);
            
            Case case8 = new Case();
            case8.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case8.Status = 'New';
            case8.SuppliedPhone = '+35555555';
            case8.Origin = 'Web';
            case8.Topic__c = 'Finance';
            case8.Case_Level__c= 'Customer_Care';
            case8.Language__c = 'French';
            cList3.add(case8);
            
            Case case9 = new Case();
            case9.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Support').getRecordTypeId();
            case9.Status = 'New';
            case9.SuppliedEmail = 'testclass@test.class';
            case9.Origin = 'Web';
            case9.Topic__c = 'Other';
            case9.Language__c = 'French';
            cList3.add(case9);
            
            Case case10 = new Case();
            case10.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing and Payments').getRecordTypeId();
            case10.Status = 'New';
            case10.Customer_Number__c = '370980914380193402183823752875028734832';
            case10.Origin = 'Web';
            case10.Topic__c = 'Finance';
            case10.Language__c = 'French';
            cList3.add(case10);
            
            insert cList3;
            
            case1.Case_Level__c= 'Complaint_Management';
            case1.Forwarded__c= 'Yes';
            
            case2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Management').getRecordTypeId();
            case2.Topic__c = 'Contract Information and Changes';
            //case2.Sub_Topic__c = 'Contract Copy';
            
            case8.Case_Level__c= '';
            
            cList4.add(case1);
            cList4.add(case2);
            cList4.add(case8);
            
            update cList4;
        Test.stopTest();
        delete bpf;
    }*/
    
    static ByPassFlow__c insertBypassFlowNames_Case(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Case Management';
            insert byPassTrigger;
            return byPassTrigger;
    }
    
    @isTest
    static void test_CategoryLocation()
    {
        Test.startTest();
            Category_Location__c category = Test_DataFactory.getCategoriesAndLocations('CategoryTest',ConstantsUtil.CATEGORY_RT_DEVELOPERNAME,1,null, null)[0];
            List<Category_Location__c> locations = Test_DataFactory.getCategoriesAndLocations('LocationTest',ConstantsUtil.LOCATION_RT_DEVELOPERNAME,2,null, null);
            insert category;
            insert locations;
        Test.stopTest();
    }
    
    @isTest
    static void test_Contract()
    {
        ByPassFlow__c bpf = insertBypassFlowNames_Contract();
        Test.startTest();
            Product2 product1 = [SELECT Id FROM Product2 LIMIT 1];
            Place__c place = [SELECT Id FROM Place__c LIMIT 1];
            SBQQ__QuoteLine__c quoteLine = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1];
            Account acc = [SELECT Id FROM Account LIMIT 1];
            
            Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
            insert cntrct;
            List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',place.id,cntrct.id,1);
            subs[0].SBQQ__QuoteLine__c = quoteLine.Id;
            subs[0].SBQQ__Product__c = product1.Id;
            subs[0].SBQQ__SubscriptionStartDate__c   = Date.today().addMonths(-13);
            subs[0].SBQQ__Account__c = acc.Id;
            insert subs;
        Test.stopTest();
        delete bpf;
    }
    
    
    static ByPassFlow__c insertBypassFlowNames_Contract(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
        return byPassFlow;
    }
    
    @isTest
    static void test_CreditNote()
    {
        Test.startTest();
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            
            insert place;
            System.debug('Place: '+ place);
            
            Account acc = [SELECT Id FROM Account LIMIT 1];
            
            Contact myContact = [SELECT Id FROM Contact WHERE AccountId = :acc.Id LIMIT 1];
            System.Debug('Contact: '+ myContact);
            
            Billing_Profile__c billProfile = new Billing_Profile__c();
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '1234';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'test';   
            billProfile.Name = 'Test Bill Prof Name';
    
            insert billProfile;
            System.Debug('Bill Profile: '+ billProfile);
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            
            insert myContract;
            System.debug('Contract: '+ myContract);       
            
            Invoice__c invoice = new Invoice__c();
            invoice.Customer__c = acc.Id;
            invoice.Billing_Profile__c = billProfile.Id;
            invoice.Invoice_Code__c = 'FAK';
            invoice.Process_Mode__c = 'Standard0';
            invoice.Status__c = 'Collected';
            invoice.Amount__c = 230;
            
            insert invoice;
            System.debug('invoice: '+ invoice);
            
            Invoice_Order__c invoiceOrder = new Invoice_Order__c();
            invoiceOrder.Invoice__c = invoice.Id;
            invoiceOrder.Contract__c = myContract.Id;
            
            insert invoiceOrder;
            System.debug('Invoice Order: '+ invoiceOrder);
            
            Credit_Note__c creditNote = new Credit_Note__c();
            creditNote.Account__c = acc.Id;
            creditNote.Reimbursed_Invoice__c = invoice.Id;
            creditNote.Contract__c = myContract.Id;
            creditNote.Value__c = 100;
            creditNote.Execution_Date__c = Date.today();
            
            insert creditNote;
        Test.stopTest();
    }
    
    @IsTest static void test_Event()
    {
        Contact cont = [select Id, IsDeleted, MasterRecordId, AccountId, LastName, FirstName, Salutation, MiddleName, Suffix, Name, RecordTypeId, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, OtherLatitude, OtherLongitude, OtherGeocodeAccuracy, OtherAddress, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingLatitude, MailingLongitude, MailingGeocodeAccuracy, MailingAddress, Phone, Fax, MobilePhone, HomePhone, OtherPhone, AssistantPhone, ReportsToId, Email, Title, Department, AssistantName, LeadSource, Birthdate, Description, OwnerId, HasOptedOutOfEmail, HasOptedOutOfFax, DoNotCall, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastCURequestDate, LastCUUpdateDate, LastViewedDate, LastReferencedDate, EmailBouncedReason, EmailBouncedDate, IsEmailBounced, PhotoUrl, Jigsaw, JigsawContactId, IndividualId, Language__c, Position__c, Benutzername__c, Personalnummer__c, Regionaldirektor__c, Regionalleiter__c, RD__c, RL__c, DataSource__c, GoldenRecordID__c, MailingAddressValidationDate__c, MailingAddressValidationStatus__c, PO_BoxCity__c, PO_BoxZip_PostalCode__c, PO_Box__c, Title__c, ZIPExtension__c, c_o__c, Primary__c, Business_Phone__c, Contact_Personal_Function__c, GoldenrecordId_Migration__c, Mobile_Phone__c from Contact LIMIT 1]; 
        Opportunity oppo = [select Id, IsDeleted, AccountId, IsPrivate, Name, Description, StageName, Amount, Probability, ExpectedRevenue, TotalOpportunityQuantity, CloseDate, Type, NextStep, LeadSource, IsClosed, IsWon, ForecastCategory, ForecastCategoryName, CampaignId, HasOpportunityLineItem, Pricebook2Id, OwnerId, Territory2Id, IsExcludedFromTerritory2Filter, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, FiscalQuarter, FiscalYear, Fiscal, ContactId, LastViewedDate, LastReferencedDate, SyncedQuoteId, ContractId, HasOpenActivity, HasOverdueTask, Budget_Confirmed__c, Discovery_Completed__c, ROI_Analysis_Completed__c, SBQQ__AmendedContract__c, Loss_Reason__c, SBQQ__Contracted__c, SBQQ__CreateContractedPrices__c, SBQQ__OrderGroupID__c, SBQQ__Ordered__c, SBQQ__PrimaryQuote__c, SBQQ__QuotePricebookId__c, SBQQ__Renewal__c, SBQQ__RenewedContract__c, BulkCreation__c, ExternalOPTY__c, Quote_Migration_Batch__c, Opportunity_reason__c, PushCount__c, Amount__c, Cluster_Id__c, Downgrade__c, SambaMigration__c, UpgradeDowngrade_Contract__c, Upgrade__c from Opportunity LIMIT 1];
        Event ev = new Event();
        ev.DurationInMinutes = 5;
        ev.ActivityDateTime = Datetime.now();
        ev.Subject = 'Call';
        ev.WhoId = cont.Id;
        ev.WhatId = oppo.Id;
        List<Event> listEvent = new List<Event>();
        listEvent.add(ev); 
        
        Test.startTest();
        insert listEvent;
        listEvent[0].DurationInMinutes = 10;
        update listEvent;
        Test.stopTest();
    }
    
    @IsTest static void test_Lead()
    {
        Test.startTest();
            list<lead> leads = test_datafactory.createleads('TestFirst','TestLast',1);
            insert leads;
        Test.stopTest();
    }
    
    @IsTest static void test_Order()
    {
        Test.startTest();
            Account account = [SELECT Id FROM Account LIMIT 1];
            Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
            SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
            List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
            Map<String,String> fieldApiNameToValue = new Map<String,String>();
            fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
            fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
            fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
            fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
            fieldApiNameToValue.put('AccountId',account.Id);
            fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
            Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
            order.OpportunityId = opportunity.Id;
            insert order;
            SBQQ__QuoteLine__c fatherQL;
            List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
            for(SBQQ__QuoteLine__c ql : quoteLines){
                if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                    fatherQL = ql;
                }
                else childrenQLs.add(ql);
            }
            OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
            fatherOI.Ad_Context_Allocation__c = 'TestContextAllocationId';
            fatherOI.Related_Product_Group__c = 'TestPG';
            insert fatherOI;
        Test.stopTest();
    }
    
    static ByPassFlow__c insertBypassFlowNames_QuoteDocument(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
        return byPassTrigger;
    }
    
    @IsTest static void test_QuoteDocument(){
        Test.startTest();
            ByPassFlow__c byPassTrigger = insertBypassFlowNames_QuoteDocument();
            Account acc = [SELECT Id FROM Account LIMIT 1];
            List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
            insert cont;
            opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];  
            insert opp;
            SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
            insert quote;
            document doc = new document();
            doc.name = 'testname';
            doc.Body = Blob.valueOf('testbody');
            doc.FolderId = UserInfo.getUserId();
            insert doc;
            SBQQ__QuoteDocument__c document = test_dataFactory.generateQuoteDocument(quote.id);
            SBQQ__QuoteDocument__c document1 = test_dataFactory.generateQuoteDocument(quote.id);
            insert document1;
            document.SBQQ__DocumentId__c = doc.id;
            insert document;
            SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
            template.Name = ConstantsUtil.TEMPLATENAME_SUBSTRING_TELESALES;
            insert template;
            document1.SBQQ__DocumentId__c = doc.id;
            document1.SBQQ__Template__c = template.id;
            document.SBQQ__QuoteTemplate__c = template.id;
            update document;
            update document1;
            delete byPassTrigger;
        Test.stopTest();
    }
    
    @IsTest static void test_UserTerritory2Association()
    {
        
        Test.startTest();
            Territory2 objTerr = [SELECT id FROM Territory2 LIMIT 1];
            List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
            insert u;
            UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr.Id, UserId= u[0].Id,
                                                                                                   RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            insert objUserTerritory2Association;
        Test.stopTest();
    }
    
    @IsTest static void test_Task()
    {
        Test.startTest();
            Task t = new Task();
            insert t;
        Test.stopTest();
    }
    
    
    public static Product2 generteFatherProduct_localBANNER(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }

}