@isTest
public class Test_CustomChangeAccountController {
	@testSetup
    public static void setupData(){
        AccountTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        List<Account> accs = Test_DataFactory.generateAccounts('TestAccount_', 2, false);
        insert accs;
        List<Place__c> places = Test_DataFactory.createPlaces('TestPlace_', 1);
        insert places;
        Place__c p = [SELECT Id, Account__c FROM Place__c LIMIT 1];
        p.Account__c = accs[0].Id;
        update p;
        AccountTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
    }
    
    public static testmethod void testFindByName(){
        CustomChangeAccountController.findByName('TestAcc', 'Account');
    }
    
    public static testmethod void testUpdateAccount(){
        Place__c p = [SELECT Id, Account__c FROM Place__c LIMIT 1];
        Account a = [SELECT Id FROM Account WHERE Id != :p.Account__c LIMIT 1];
        AccountTriggerHandler.disableTrigger = true;
        CustomChangeAccountController.updateAccount(p.id, Json.serialize(a));
        AccountTriggerHandler.disableTrigger = false;
    }
}