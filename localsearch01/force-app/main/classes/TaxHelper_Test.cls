@isTest()
public class TaxHelper_Test {
    private static testmethod void test_001() {

        Date dateFrom = Date.newInstance(2018, 3, 5);
        Date dateTo = Date.newInstance(2019, 3, 4);
        
        insert new Vat_Schedule__c(Tax_Code__c = ConstantsUtil.BILLING_TAX_CODE_HIGH, Start_Date__c=null, End_Date__c=null, Value__c=10.0);
        insert new Vat_Schedule__c(Tax_Code__c = ConstantsUtil.BILLING_TAX_CODE_HIGH, Start_Date__c=Date.today(), End_Date__c=null, Value__c=15.0);
        // insert new Vat_Schedule__c(Tax_Code__c = ConstantsUtil.BILLING_TAX_CODE_LOW, Start_Date__c=null, End_Date__c=dateFrom, Value__c=2.0);
        // insert new Vat_Schedule__c(Tax_Code__c = ConstantsUtil.BILLING_TAX_CODE_LOW, Start_Date__c=dateFrom.addDays(1), End_Date__c=null, Value__c=2.5);
        
        string hash = TaxHelper.getHashKey(ConstantsUtil.BILLING_TAX_CODE_HIGH, dateFrom, dateTo);
        /* System.assertEquals(ConstantsUtil.BILLING_TAX_CODE_HIGH + ':20180305-20190304', hash);
        
        System.assertEquals(null, TaxHelper.getMinDate(null, null));
        System.assertEquals(dateFrom, TaxHelper.getMinDate(dateFrom, null));
        System.assertEquals(dateTo, TaxHelper.getMinDate(null, dateTo));
        System.assertEquals(dateFrom, TaxHelper.getMinDate(dateFrom, dateTo));
        System.assertEquals(dateFrom, TaxHelper.getMinDate(dateTo, dateFrom));
        System.assertEquals(null, TaxHelper.getMaxDate(null, null));
        System.assertEquals(dateFrom, TaxHelper.getMaxDate(dateFrom, null));
        System.assertEquals(dateTo, TaxHelper.getMaxDate(null, dateTo));
        System.assertEquals(dateTo, TaxHelper.getMaxDate(dateFrom, dateTo));
        System.assertEquals(dateTo, TaxHelper.getMaxDate(dateTo, dateFrom));
        
        List<TaxHelper.VatListItem> items = TaxHelper.getVatItems(ConstantsUtil.BILLING_TAX_CODE_HIGH, dateFrom, dateTo, 100.0, Date.today());
        System.assert(items.size() == 1);
        System.assertEquals(dateFrom, items[0].dateFrom);
        System.assertEquals(dateTo, items[0].dateTo);
        System.assertEquals(100.0, items[0].net);
        System.assertEquals(10.0, items[0].percentage);
        System.assertEquals(10.0, items[0].vat);
        System.assertEquals(110.0, items[0].total);
        System.assertEquals(ConstantsUtil.BILLING_TAX_CODE_HIGH, items[0].vatCode);
		
        boolean exceptionThrown = false;
        try {
        	items = TaxHelper.getVatItems('Invented Tax', dateFrom, dateTo, 100.0, null);
        } catch (Exception ex) {
            System.assert(ex instanceOf TaxHelper.TaxHelperException);
            System.assertEquals(ErrorHandler.ErrorCode.E_NO_TAX_SCHEDULE.name(), ((TaxHelper.TaxHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        System.assertEquals(true, exceptionThrown, 'An exception should have been thrown');
		exceptionThrown = false;
        try {
        	items = TaxHelper.getVatItems(ConstantsUtil.BILLING_TAX_CODE_HIGH, Date.today(), Date.newInstance(2021, 3, 4), 100.0, null);
        } catch (Exception ex) {
            System.assert(ex instanceOf TaxHelper.TaxHelperException);
            System.assertEquals(ErrorHandler.ErrorCode.E_TAX_SCHEDULE_OVERLAP.name(), ((TaxHelper.TaxHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        System.assertEquals(true, exceptionThrown, 'An exception should have been thrown');
		exceptionThrown = false;

        try {
        	items = TaxHelper.getVatItems(ConstantsUtil.BILLING_TAX_CODE_LOW, dateFrom, dateTo, 100.0, null);
        } catch (Exception ex) {
            System.assert(ex instanceOf TaxHelper.TaxHelperException);
            System.assertEquals(ErrorHandler.ErrorCode.E_NO_TAX_SCHEDULE.name(), ((TaxHelper.TaxHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        System.assertEquals(true, exceptionThrown, 'An exception should have been thrown');

        */ 
    }
}