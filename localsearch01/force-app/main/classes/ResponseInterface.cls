public class ResponseInterface {
    
	public String status{get; set;}
    public List<String> source_element_ids {get; set;}
    public ResponseInterfaceTechnical technical_fields{get; set;}
    public ResponseInterfaceDetails details{get; set;}
    
    public ResponseInterface(){
        technical_fields = new ResponseInterfaceTechnical();
        details = new ResponseInterfaceDetails();
        source_element_ids = new List<String>();
    }
    
    public class ResponseInterfaceTechnical{
        public String correlation_id {get; set;}
    }
    
    public class ResponseInterfaceDetails{
        public String error_code {get; set;}
        public String  error_description {get; set;}
    }
}