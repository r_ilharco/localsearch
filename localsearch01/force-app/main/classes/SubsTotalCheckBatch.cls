global class SubsTotalCheckBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    private String sSQL = null;
    
    public SubsTotalCheckBatch(){
    }
    
    global database.querylocator start(Database.BatchableContext bc){
        Integer daysForBilling = 0;
		List<Billing_Setting__mdt> settingSngl = new List<Billing_Setting__mdt>();
        settingSngl = [select Days_Until_Billing__c from Billing_Setting__mdt where Label =: ConstantsUtil.BILLING_SETTINGS_METADATA_LABEL];
        system.debug('days until billing: ' + settingSngl[0].Days_Until_Billing__c);
        if(settingSngl[0].Days_Until_Billing__c != null){
            daysForBilling = (Integer)settingSngl[0].Days_Until_Billing__c;
        }
        String contractStatusActive = ConstantsUtil.CONTRACT_STATUS_ACTIVE;     
        String subStatusActive = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        String amendmentType = ConstantsUtil.ORDER_TYPE_AMENDMENT;
        Date dateToCheck = Date.today().addDays(-daysForBilling).addDays(+1);
        sSQL = 'SELECT Id FROM SBQQ__Subscription__c WHERE Subsctiption_Status__c = :subStatusActive AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c  != null '+
               'AND SBQQ__Contract__r.Status = :contractStatusActive AND (SBQQ__EndDate__c = NULL OR SBQQ__EndDate__c > TODAY ) AND One_time_Fee_Billed__c = false '+
               'AND SBQQ__NetPrice__c != 0 AND SBQQ__QuoteLine__c != NULL AND (SBQQ__TerminatedDate__c = NULL OR SBQQ__TerminatedDate__c > TODAY ) AND Total__c != 0 ' +
               'AND ((Next_Invoice_Date__c = NULL AND SBQQ__StartDate__c <= :dateToCheck) OR (Next_Invoice_Date__c != NULL AND Next_Invoice_Date__c < :dateToCheck)) ' +
               'AND Amount_Checked__c = false AND SBQQ__OrderProduct__r.Order.Type != :amendmentType AND SkipBilling__c = false';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        Set<Id> masterSubsId = new Set<Id>();
        Set<Id> technicalIssueIdSet = new Set<Id>();
        Set<Id> skipBillingIdSet = new Set<Id>();
        Map<Id, SBQQ__Subscription__c> subscriptionsToUpdate = new Map<Id, SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        Set<Id> subIds = new Set<Id>();
        for(SBQQ__Subscription__c sub : scope){
            subIds.add(sub.Id);
        }
        
        List<SBQQ__Subscription__c> subs = [SELECT  Id, SBQQ__QuoteLine__r.Base_term_Renewal_term__c, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Base_term_Renewal_term__c, 
                                            SBQQ__QuoteLine__r.Subscription_Term__c, SBQQ__QuoteLine__r.SBQQ__Quantity__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, Technical_Billing_Issue__c, 
                                            SBQQ__QuoteLine__r.SBQQ__Discount__c, SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c, SBQQ__Product__r.One_time_Fee__c, Total__c,
                                            SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c, SBQQ__QuoteLine__r.SBQQ__AdditionalDiscountAmount__c,
                                            SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Subscription_Term__c, Total_Amount_To_Bill__c, Wrong_Amount_To_Bill__c, SkipBilling__c,
                                            SBQQ__BillingFrequency__c, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__BillingFrequency__c, RequiredBy__c, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c
                                            FROM SBQQ__Subscription__c
                                            WHERE Id IN :subIds];
        
        for(SBQQ__Subscription__c subToCheck : subs){
            Decimal totalToCheck = 0;
            Decimal discountAmount = 0;
            Integer totalMonths = 0;
            Decimal baseTerm = 0;
            Integer numberOfMonths = 0;
            Decimal amountDifference = 0;
            Invoice_Cycle__mdt billing_frequency = new Invoice_Cycle__mdt();
            if(subToCheck.SBQQ__BillingFrequency__c != null){
                billing_frequency = [SELECT Number_of_months__c FROM Invoice_Cycle__mdt WHERE MasterLabel = :subToCheck.SBQQ__BillingFrequency__c];
            }else{
                billing_frequency = [SELECT Number_of_months__c FROM Invoice_Cycle__mdt WHERE MasterLabel = :subToCheck.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__BillingFrequency__c];
            }  
            numberOfMonths = (Integer)billing_frequency.Number_of_months__c;
            totalToCheck = subToCheck.SBQQ__QuoteLine__r.SBQQ__Quantity__c*subToCheck.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            if(subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null && subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != 0 && !subToCheck.SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c){
                discountAmount = (totalToCheck/100)*subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c;
                totalToCheck = totalToCheck-discountAmount;
            }
            if(subToCheck.SBQQ__QuoteLine__r.Base_term_Renewal_term__c != null){
                baseTerm = subToCheck.SBQQ__QuoteLine__r.Base_term_Renewal_term__c;
                system.debug('baseTermSon: ' + baseTerm);
            }else{
                baseTerm = subToCheck.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Base_term_Renewal_term__c;
                system.debug('baseTermFather: ' + baseTerm);
            }
            if(!subToCheck.SBQQ__Product__r.One_time_Fee__c){
                if(baseTerm >= numberOfMonths){
                    totalToCheck = totalToCheck/(baseTerm/numberOfMonths);
                }else{
                    totalToCheck = totalToCheck * (numberOfMonths/baseTerm);
                }
            }

            if(subToCheck.Total__c < 0 || subToCheck.Total__c == null) {
                subToCheck.Technical_Billing_Issue__c = true;
                if(subToCheck.RequiredBy__c != null){
                    masterSubsId.add(subToCheck.RequiredBy__c);
                    technicalIssueIdSet.add(subToCheck.RequiredBy__c);
                }else{
                    masterSubsId.add(subToCheck.Id);
                    technicalIssueIdSet.add(subToCheck.Id);
                } 
            } else {
                subToCheck.Technical_Billing_Issue__c = false;
            }
            subToCheck.Total_Amount_To_Bill__c = Test.isRunningTest() && subToCheck.Total_Amount_To_Bill__c == null ? 0 : subToCheck.Total_Amount_To_Bill__c;
            amountDifference = totalToCheck - subToCheck.Total_Amount_To_Bill__c;
            system.debug('totalToCheck:' + totalToCheck);
            system.debug('subToCheck.Total_Amount_To_Bill__c:' + subToCheck.Total_Amount_To_Bill__c);
            system.debug('amountDifference:' + amountDifference);
            if(Math.abs(amountDifference) > 2 || subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c > 100 || 
               (subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c < 0 && subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null)){
                subToCheck.Wrong_Amount_To_Bill__c = true;
                subToCheck.SkipBilling__c = true;
                    if(subToCheck.RequiredBy__c != null){
                        masterSubsId.add(subToCheck.RequiredBy__c);
                        skipBillingIdSet.add(subToCheck.RequiredBy__c);
                    }else{
                        masterSubsId.add(subToCheck.Id);
                        skipBillingIdSet.add(subToCheck.Id);
                    } 
            }else{
                subToCheck.Wrong_Amount_To_Bill__c = false;
                subToCheck.SkipBilling__c = false; 
            }
            subToCheck.Amount_Checked__c = true;
            subscriptionsToUpdate.put(subToCheck.id, subToCheck);           
        }
        if(masterSubsId.size() > 0){
            subsToUpdate = [SELECT  Id, RequiredBy__c, Total_Amount_To_Bill__c, Wrong_Amount_To_Bill__c, SkipBilling__c, Technical_Billing_Issue__c, Total__c
                                     FROM SBQQ__Subscription__c 
                                     WHERE Id IN :masterSubsId OR RequiredBy__c IN :masterSubsId];
            for(SBQQ__Subscription__c csub : subsToUpdate){
                if(technicalIssueIdSet.contains(csub.Id) || technicalIssueIdSet.contains(csub.RequiredBy__c)) {
                    csub.Technical_Billing_Issue__c = true;
                }
                if(skipBillingIdSet.contains(csub.Id) || skipBillingIdSet.contains(csub.RequiredBy__c)) {
                    csub.SkipBilling__c = true;
                }
                if(!subscriptionsToUpdate.containsKey(csub.Id)){
                    subscriptionsToUpdate.put(csub.id, csub);
                }else{
                    SBQQ__Subscription__c tempSub = subscriptionsToUpdate.get(csub.id);
                    if(technicalIssueIdSet.contains(csub.Id) || technicalIssueIdSet.contains(tempSub.RequiredBy__c)) {
                        tempSub.Technical_Billing_Issue__c = true;
                    }
                    if(skipBillingIdSet.contains(csub.Id) || skipBillingIdSet.contains(tempSub.RequiredBy__c)) {
                        tempSub.SkipBilling__c = true;
                    }
                    subscriptionsToUpdate.put(tempSub.id, tempSub);
                }                
            }
        }
        
        if(subscriptionsToUpdate.values().size() > 0){
            update subscriptionsToUpdate.values();
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    
    global void execute(SchedulableContext sc){
        SubsTotalCheckBatch b = new SubsTotalCheckBatch(); 
        database.executebatch(b,150);
    }
}