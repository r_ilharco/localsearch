/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Quote Line Trigger Handler Class.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created        2019-05-22
* @testClass	  Test_SBQQQuoteLineTrigger
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-08-04	  Block product selling on same place based on product group.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-11-20	  Removed validation on the configuration of more than 2 localBANNER/searchBANNER
                  with the same combination of category, location, language and slot.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class QuoteLineTriggerHandler implements ITriggerHandler{
    private static boolean firstBeforeInsertRun = true;
	public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems){
        if (firstBeforeInsertRun) { firstBeforeInsertRun = false; }
       	ATL_QuoteLineTriggerHelper.setRenewalQuoteLineFields((List<SBQQ__QuoteLine__c >) newItems);
      	ATL_QuoteLineTriggerHelper.setFieldsFromQuote((List<SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.setfieldforMigration((List<SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.checkAdditionalOptiononInsert((List<SBQQ__QuoteLine__c >) newItems, null);
        ATL_QuoteLineTriggerHelper.quoteSellingValidation((List<SBQQ__QuoteLine__c >) newItems, new Map<Id,SBQQ__QuoteLine__c >());
        ATL_QuoteLineTriggerHelper.setSubTermOnChildrenQL((List<SBQQ__QuoteLine__c >) newItems, null, null);
    	ATL_QuoteLineTriggerHelper.billingFrequencyValidation((List<SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.checkSubscriptionType((List<SBQQ__QuoteLine__c >) newItems);
        //ATL_QuoteLineTriggerHelper.myWebsiteFreeCheck((List<SBQQ__QuoteLine__c>) newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        ATL_QuoteLineTriggerHelper.blockProductSellingOnSamePlace((Map<Id, SBQQ__QuoteLine__c >) newItems,  (Map<Id,SBQQ__QuoteLine__c >) oldItems);
        ATL_QuoteLineTriggerHelper.setDiscountforRenewalMigrated(newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) oldItems);
        ATL_QuoteLineTriggerHelper.checkAdditionalOption((Map<Id,SBQQ__QuoteLine__c >) newItems, (Map<Id,SBQQ__QuoteLine__c >) oldItems);
        ATL_QuoteLineTriggerHelper.preventContactChange((Map<Id,SBQQ__QuoteLine__c >) newItems, (Map<Id,SBQQ__QuoteLine__c >) oldItems);
       // ATL_QuoteLineTriggerHelper.blockSellingMoreThanFiveVipPLace((Map<Id,SBQQ__QuoteLine__c >) newItems, (Map<Id,SBQQ__QuoteLine__c >) oldItems);
        
        List<SBQQ__QuoteLine__c> validQuoteLines = new List<SBQQ__QuoteLine__c>();
        Map<Id,SBQQ__QuoteLine__c> oldQlMap = (Map<Id,SBQQ__QuoteLine__c >) oldItems;
        for(SBQQ__QuoteLine__c currentQL : (List<SBQQ__QuoteLine__c>)newItems.values()){
            Boolean placeChanged = currentQL.Place__c != oldQlMap.get(currentQL.Id).Place__c;
            Boolean startDateChanged = currentQL.SBQQ__StartDate__c != oldQlMap.get(currentQL.Id).SBQQ__StartDate__c;
            if(placeChanged || startDateChanged){
                validQuoteLines.add(currentQL);
            }
        }
        if(!validQuoteLines.isEmpty()) ATL_QuoteLineTriggerHelper.quoteSellingValidation(validQuoteLines, (Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.setSubTermOnChildrenQL(null, (Map<Id, SBQQ__QuoteLine__c>) newItems, (Map<Id, SBQQ__QuoteLine__c>) oldItems);
        ATL_QuoteLineTriggerHelper.checkProductToAccount((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.productValidationCheck((Map<Id,SBQQ__QuoteLine__c >) newItems);
        //SPIII-5345
        if(firstBeforeInsertRun == false){
            for(SBQQ__QuoteLine__c currentQL : (List<SBQQ__QuoteLine__c>)newItems.values()){
                if(ConstantsUtil.QUOTE_TYPE_RENEWAL.equalsIgnoreCase(currentQL.Quote_Type__c) && currentQL.Campaign_Id__c != null){
                    currentQL.Campaign_Id__c = null;
                }
            }
            ATL_QuoteLineTriggerHelper.cleanPromotionalProcessOnRenewal((Map<Id,SBQQ__QuoteLine__c >) newItems);
        }
        ATL_QuoteLineTriggerHelper.checkQuantitiesOnMyWEBSITETranslationOptions((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.relatedProductGroupPopulation((Map<Id,SBQQ__QuoteLine__c >) newItems);
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        ATL_QuoteLineTriggerHelper.checkAdditionalOptiononDelete((Map<Id, SBQQ__QuoteLine__c >) oldItems);
    }
 
    public void AfterInsert(Map<Id, SObject> newItems) {
        ATL_QuoteLineTriggerHelper.blockProductSellingOnSamePlace((Map<Id, SBQQ__QuoteLine__c >) newItems, null);
        //ATL_QuoteLineTriggerHelper.blockSellingMoreThanFiveVipPLace((Map<Id,SBQQ__QuoteLine__c >) newItems,null);
       	ATL_QuoteLineTriggerHelper.checkAdditionalOptiononInsert((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id, SBQQ__QuoteLine__c>) newItems);
        ATL_QuoteLineTriggerHelper.checkProductToAccount((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.productValidationCheck((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.checkQuantitiesOnMyWEBSITETranslationOptions((Map<Id,SBQQ__QuoteLine__c >) newItems);
		//ATL_QuoteLineTriggerHelper.checkCategoryLocationv2((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.checkPlace((Map<Id,SBQQ__QuoteLine__c >) newItems);  
		//Vincenzo Laudato - Assets mgmt for Advertising Print products
        ATL_QuoteLineTriggerHelper.insertTaskForPrint((List<SBQQ__QuoteLine__c>) newItems.values());
        ATL_QuoteLineTriggerHelper.myWebsiteFreeCheck_v2((Map<Id,SBQQ__QuoteLine__c >) newItems);
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        //ATL_QuoteLineTriggerHelper.qtyValidation((List<SBQQ__QuoteLine__c >) newItems.values());
        //ATL_QuoteLineTriggerHelper.placeValidation((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) newItems);
        //Vincenzo Laudato - Check category-location-language combination for LocalBanner and SearchBanner
        //ATL_QuoteLineTriggerHelper.checkCategoryLocationv2((Map<Id,SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.checkPlace((Map<Id,SBQQ__QuoteLine__c >) newItems);
    }
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}