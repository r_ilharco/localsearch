global class ActivationOrderBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts {
    
    global void execute(SchedulableContext sc) {
        ActivationOrderBatch b = new ActivationOrderBatch(); 
        database.executebatch(b);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator('SELECT Id, Status, Type, Custom_Type__c, SBQQ__Contracted__c, Parent_Order__c ' +
                                       'FROM Order ' +
                                       'WHERE Id IN (SELECT OrderId FROM OrderItem WHERE SBQQ__Status__c = \''+ ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED + '\' AND SBQQ__RequiredBy__c = NULL) ' +
                                       'AND Status = \'' + ConstantsUtil.ORDER_STATUS_PUBLICATION + '\' AND Migrated__c = FALSE');
    }
    
    global void execute(Database.BatchableContext info, List<Order> scope ){
        System.debug('ActivationOrderBatch '+ scope);
        Map<Id, List<OrderItem>> orderToOrderItem = new Map<Id, List<OrderItem>>();
        Map<Id, Order> idToOrder = new Map<Id, Order>();
        List<Order> orderToUpdate = new List<Order>();
        List<Id> ordersIds = new List<Id>();
        List<Id> parentOrdersIds = new List<Id>();
        
        for(Order o : scope){
            ordersIds.add(o.Id);
            idToOrder.put(o.Id, o);
        }
        
        List<OrderItem> orderItems = [SELECT Id, SBQQ__Status__c, OrderId, SBQQ__RequiredBy__c FROM OrderItem WHERE OrderId IN :ordersIds];
        
        for(OrderItem oi : orderItems){
            if(!orderToOrderItem.containsKey(oi.OrderId)){
                orderToOrderItem.put(oi.OrderId, new List<OrderItem>{oi});
            }
            else{
                orderToOrderItem.get(oi.OrderId).add(oi);
            }
        }
        
        if(!orderToOrderItem.isEmpty() && !idToOrder.isEmpty()){
            for(Id currentKey : orderToOrderItem.keySet()){
                Integer currentOrderItemsSize = 0;
                for(OrderItem oi :orderToOrderItem.get(currentKey)){
                    if(String.isBlank(oi.SBQQ__RequiredBy__c)){
                        currentOrderItemsSize++;
                    }
                }
                Integer countActive = 0;
                for(OrderItem currentOI : orderToOrderItem.get(currentKey)){
                    if( String.isBlank(currentOI.SBQQ__RequiredBy__c) && (ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED.equalsIgnoreCase(currentOI.SBQQ__Status__c) || ConstantsUtil.ORDER_ITEM_STATUS_REJECTED.equalsIgnoreCase(currentOI.SBQQ__Status__c))) countActive++;
                }
                if(currentOrderItemsSize == countActive){
                    idToOrder.get(currentKey).Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                    if(ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(idToOrder.get(currentKey).Custom_Type__c)){
                        idToOrder.get(currentKey).SBQQ__Contracted__c = true;
                    }
                    else if(ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(idToOrder.get(currentKey).Custom_Type__c) && ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(idToOrder.get(currentKey).Type)){
                        parentOrdersIds.add(idToOrder.get(currentKey).Parent_Order__c);
                    }
                    orderToUpdate.add(idToOrder.get(currentKey));
                }
            }
        }
        if(!parentOrdersIds.isEmpty()){
            List<Order> upgradeDowngradeOrders = SEL_Order.getOrdersByIds(parentOrdersIds);
			OrderUtility.changeOrderStatus(upgradeDowngradeOrders, true);
        }
        if(!orderToUpdate.isEmpty()) update orderToUpdate;
        
    }     
    
    global void finish(Database.BatchableContext info){     
    }
}