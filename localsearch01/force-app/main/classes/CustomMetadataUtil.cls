public class CustomMetadataUtil {
    public String extSystem {get; set;}
    public String endpoint {get; set;}
    public String token {get; set;}
    public String clientId {get; set;}
    public String clientSecret {get; set;}
    public String grantType {get; set;}
    
    public CustomMetadataUtil(String extSystem, String endpoint, String token, String clientId, String clientSecret, string grantType){
        this.extSystem = extSystem;
        this.endpoint = endpoint;
        this.token = token;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
    }
    
    public static CustomMetadataUtil getCustMetadataValues(String systemName)
    {
        List<Integration_Config__mdt> integrationConfigMDTList = [SELECT  Id, 
                                                                          MasterLabel, 
                                                                          Endpoint__c, 
                                                                          Token__c, 
                                                                          Client_Id__c, 
                                                                          Client_Secret__c, 
                                                                          GrantType__c 
                                                                  FROM    Integration_Config__mdt 
                                                                  WHERE   DeveloperName=:systemName 
                                                                  AND     Active__c = true LIMIT 1];
        
        system.debug('getCustMetadataValues: '+integrationConfigMDTList.size());
        
        if(integrationConfigMDTList.size() > 0 ){
            Integration_Config__mdt integrationConfigMDT = integrationConfigMDTList[0];
            system.debug('config:'+integrationConfigMDT);
            CustomMetadataUtil obj =  new CustomMetadataUtil(
                integrationConfigMDT.MasterLabel,
                integrationConfigMDT.Endpoint__c,
                integrationConfigMDT.Token__c,
                integrationConfigMDT.Client_Id__c,
                integrationConfigMDT.Client_Secret__c,
                integrationConfigMDT.GrantType__c
            );
            system.debug('obj:'+obj);
            return obj;
        }
        return null;

    }
 
    //SF-210 Order Management, Sprint 5 - 2019-07-17 - <mamazzarella@deloitte.it>
    public static String getMethodName(String systemName){
        
        List<Integration_Config__mdt> integrationConfigMDTList = [SELECT  Id, 
                                                                          Method__c
                                                                  FROM    Integration_Config__mdt 
                                                                  WHERE   DeveloperName=:systemName 
                                                                  AND     Active__c = true LIMIT 1];
        
        if(!integrationConfigMDTList.isEmpty()){
            Integration_Config__mdt integrationConfigMDT = integrationConfigMDTList[0];
            System.debug('config:'+integrationConfigMDT);
            return integrationConfigMDT.Method__c;
        }
        return null;
    }
    
    public static String getEndpoint(String systemName){
        
        List<Integration_Config__mdt> integrationConfigMDTList = [SELECT  Id, 
                                                                          Endpoint__c
                                                                  FROM    Integration_Config__mdt 
                                                                  WHERE   DeveloperName=:systemName 
                                                                  AND     Active__c = true LIMIT 1];
        
        if(!integrationConfigMDTList.isEmpty()){
            Integration_Config__mdt integrationConfigMDT = integrationConfigMDTList[0];
            return integrationConfigMDT.Endpoint__c;
        }
        return null;
    }
}