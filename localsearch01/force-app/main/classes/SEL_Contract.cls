/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 14-05-2019
 * Sprint      : 1
 * User Story  : 25
 * Testclass   : Test_ContractInBoundApi
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class SEL_Contract{
    
    public static Map<Id, Contract> getContractsByIds(Set<Id> contractIds){
        return new Map<Id, Contract>([SELECT	Id
                                      			,Status
                                      			,OwnerOnSignature__c
                                      			,OwnerAtAutorenewal__c
                                      			,CompanySignedId
                                      			,CompanySignedDate
                                      			,AccountId
                                      			,In_Cancellation__c
                                      			,StartDate
                                      			,Account.OwnerId
                                      			,SBQQ__Quote__c
                                      			,SBQQ__Quote__r.SBQQ__PriceBook__c
                                      			,SBQQ__Quote__r.SBQQ__Opportunity2__c
                                      			,SBQQ__Quote__r.SBQQ__SalesRep__c
                                        FROM 	Contract
                                        WHERE 	Id IN :contractIds]);
    }
    
    
    public static Map<Id, Contract> getContractsWithMasterSubscriptions(Set<Id> ids)
    {
        return new Map<Id, Contract>(
            [
                SELECT Id, 
                			(SELECT Id, SBQQ__EndDate__c, SBQQ__Product__r.Product_Group__c 
                             FROM SBQQ__Subscriptions__r
                             WHERE(
                							(Place__c != '' AND SBQQ__Product__r.PlaceIDRequired__c = 'Yes') 
                                                                OR 
                							(Place__c = '' AND SBQQ__Product__r.PlaceIDRequired__c = 'No')
                                            OR SBQQ__Product__r.PlaceIDRequired__c = 'Optional'
                							)
            					AND SBQQ__ProductOption__c = NULL
                            )
                FROM Contract WHERE Id IN :ids
            ]
        );
    }
    
    public static Contract getContract(String contractNumber){
        return [SELECT ID, ContractNumber, ActivatedDate, StartDate,
                    EndDate,Status, TerminateDate__c
                    FROM Contract 
                    WHERE ContractNumber = :contractNumber];
    }
    
    public static Map<Id, Contract> getContractsByQuoteIdsAndStatus(Set<Id> quoteIds, List<String> Status)
    {
        return new Map<Id,Contract>([
            SELECT Id, SBQQ__Quote__c
            FROM Contract
            WHERE SBQQ__Quote__c IN :quoteids
            AND Status IN :Status
        ]);
    }

    public static Map<Id,Contract> getContractByBillingProfile(Set<Id> billingProfileId){
        
        return new Map<Id,Contract>([SELECT Id, Status, ContractNumber
                                     FROM Contract
                                     WHERE SBQQ__Quote__r.Billing_Profile__c IN: billingProfileId
                                     AND Billed__c = false
                                     AND (In_Termination__c = false OR In_Cancellation__c = false)]);
        
    }
    
    public static Map<Id,Contract> getContractBycontractIds(Set<Id> contractIds){
 		return new Map<Id,Contract>([SELECT Id, 
                                     (SELECT Id, SBQQ__Quantity__c, SBQQ__NetPrice__c FROM SBQQ__Subscriptions__r),
                                     Status, 
                                     ContractNumber,
                                     TerminateDate__c,
                                     In_Termination__c,
                                     Cancel_Date__c,
                                     In_Cancellation__c,
                                     SBQQ__Quote__r.Name
                                     FROM Contract
                                     WHERE Id IN: contractIds
                                     AND Billed__c = false]);
    }
    
     
    public static Map<Id,Contract> getAllContractBycontractIds(Set<Id> contractIds){
 		return new Map<Id,Contract>([SELECT Id, 
                                     (SELECT Id, SBQQ__Quantity__c, SBQQ__NetPrice__c FROM SBQQ__Subscriptions__r),
                                     Status, 
                                     ContractNumber,
                                     TerminateDate__c,
                                     In_Termination__c,
                                     Cancel_Date__c,
                                     In_Cancellation__c,
                                     Pricebook2Id,
                                     EndDate
                                     FROM Contract
                                     WHERE Id IN: contractIds
                                     ]);
    }
    
    //Vincenzo Laudato SF-210 - Order Management
    public static List<Contract> getContractById(String contractId){
        return [ SELECT Id,
                        Status,
                        TerminateDate__c,
                        Cancel_Date__c,
                        In_Cancellation__c,
                     	Name, 
                        CallId__c, 
                        OwnerId,
                        StartDate, 
                        EndDate, 
                        AccountId, 
                        SBQQ__Quote__r.SBQQ__PriceBook__c,
                        SBQQ__Quote__c,
                        Pricebook2Id,
                        In_Termination__c 
                FROM    Contract
                WHERE   Id = :contractId
                LIMIT   1];
    }
    
    public static List<Contract> getContractByIds(Set<Id> contractIds){
        return [ SELECT Id,
                        SBQQ__Order__c,
                		SBQQ__Order__r.External_Id__c, 
                        External_Id__c,
                        Activation_Order__c,
                		Activation_Order__r.External_Id__c
                FROM    Contract
                WHERE   Id IN :contractIds
				];
    }
    
    public static List<Contract> getContractByStatus(String statusC, String statusS){
 		return [SELECT Id, 
                (SELECT Id, Status__c, SBQQ__SubscriptionStartDate__c FROM SBQQ__Subscriptions__r WHERE Status__c =: statusS),
                Status
                FROM Contract
                WHERE Status =: statusC];
    }
}