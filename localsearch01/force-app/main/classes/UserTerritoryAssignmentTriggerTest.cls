/* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class UserTerritoryAssignmentTriggerTest {
    @testSetup static void setup(){
        UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        List<Account> lstAccount = Test_DataFactory.createAccounts('Test', 20);
        insert lstAccount;
        UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
        List<User> users = Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        //List<Territory2> terrs = Test_DataFactory.createTerritories(ConstantsUtil.TERRITORY_TYPE,users[0]);
        UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        delete [SELECT Id FROM UserTerritory2Association];
        UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void ShouldUniqueTerritory(){
        UserTriggerHandler.disableTrigger = true;
        List<Territory2> objTerr = [select id from Territory2];
        Profile p = [SELECT id, Name FROM Profile where name =: ConstantsUtil.SysAdmin].get(0);  
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        
        try
        {
            Test.startTest();
            UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                                   RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            insert objUserTerritory2Association;
            
            UserTerritory2Association objUserTerritory2Association2 = new UserTerritory2Association(Territory2Id= objTerr[1].Id, UserId= u[0].Id,
                                                                                                    RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            insert objUserTerritory2Association2;
            UserTerritory2Association objUserTerritory2Association3 = new UserTerritory2Association(Territory2Id= objTerr[2].Id, UserId= u[0].Id,
                                                                                                    RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            Test.stopTest();
        }
        catch(Exception e){
            //System.Assert(e.getMessage().contains(label.TerritoryUserAssignment));
        }
        UserTriggerHandler.disableTrigger = false;
    }
    @isTest
    public static void ShouldNotUniqueTerritory(){
        UserTriggerHandler.disableTrigger = true;
        List<Territory2> objTerr = [select id from Territory2];
        Profile p = [SELECT id, Name FROM Profile where name =: ConstantsUtil.SysAdmin].get(0);  
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        
        try
        {
            Test.startTest();
            UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                                   RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
            insert objUserTerritory2Association;
            
            UserTerritory2Association objUserTerritory2Association2 = new UserTerritory2Association(Territory2Id= objTerr[1].Id, UserId= u[0].Id,
                                                                                                    RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_SDI);
            insert objUserTerritory2Association2;
            Test.stopTest();
        }
        catch(Exception e){
            //System.Assert(e.getMessage().contains(label.TerritoryUserAssignment));
        }
        UserTriggerHandler.disableTrigger = false;
    }
    
    //OAVERSANO 20191008 Enhancement #33 -- START
    @isTest
    public static void testMethodUpdateAccountDMC(){
        UserTriggerHandler.disableTrigger = true;
        List<Account> accList = [SELECT Id FROM Account]; 
        List<Territory2> objTerr = [select id from Territory2 WHERE Territory2.Territory2Model.State = 'Active'];
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        List<User> usersSales = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL,1);
        List<User> usersTeleSales = Test_DataFactory.createUsers(ConstantsUtil.TELESALESPROFILE_LABEL,1);
        system.runAs(u[0]){
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : accList){
                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = objTerr[0].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                lstTerr.add(obj);
            }     
            update accList; 
            insert lstTerr;
        }
        Test.startTest();
        UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                               RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
        insert objUserTerritory2Association;
        list<id> listOldids = new list<id>();
        listoldids.add(objUserTerritory2Association.id);
        Test.stopTest();
        try
        {
            delete objUserTerritory2Association;
        }
        catch(Exception e){
            System.debug('err: '+e.getMessage());
        }
        UserTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void testMethodUpdateAccountTESTIF(){
        UserTriggerHandler.disableTrigger = true;
        List<Account> accList = [SELECT Id FROM Account]; 
        List<Territory2> objTerr = [select id,ParentTerritory2.ParentTerritory2id from Territory2 where ParentTerritory2.ParentTerritory2id=null AND Territory2Model.State = 'Active'];
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        List<User> usersSales = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL,1);
        List<User> usersTeleSales = Test_DataFactory.createUsers(ConstantsUtil.TELESALESPROFILE_LABEL,1);
        system.runAs(u[0]){
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : accList){
                a.RegionalLeader__c = u[0].id;
                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = objTerr[0].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                lstTerr.add(obj);
            }     
            update accList; 
            insert lstTerr;
        }
        Test.startTest();
        UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                               RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_DMC);
        insert objUserTerritory2Association;
        list<id> listOldids = new list<id>();
        listoldids.add(objUserTerritory2Association.id);
        Test.stopTest();
        try
        {
            update objUserTerritory2Association;
            delete objUserTerritory2Association;
        }
        catch(Exception e){
            System.debug('err: '+e.getMessage());
        }
        UserTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void testMethodUpdateAccountSDI(){
        UserTriggerHandler.disableTrigger = true;
        List<Account> accList = [SELECT Id FROM Account]; 
        List<Territory2> objTerr = [select id from Territory2 WHERE Territory2.Territory2Model.State = 'Active' ORDER BY DeveloperName desc LIMIT 2];
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        system.runAs(u[0]){
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : accList){

                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = objTerr[1].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                lstTerr.add(obj);
            } 

            insert lstTerr;
        }
        Test.startTest();
        UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[1].Id, UserId= u[0].Id,
                                                                                               RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_SDI);
        insert objUserTerritory2Association;
        Test.stopTest();
        try
        {
            delete objUserTerritory2Association;
        }
        catch(Exception e){
            System.debug('err: '+e.getMessage());
        }
        UserTriggerHandler.disableTrigger = false;
    }
    @isTest
    public static void testMethodUpdateAccountSMA(){
        UserTriggerHandler.disableTrigger = true;
        List<Account> accList = [SELECT Id FROM Account]; 
        List<Territory2> objTerr = [select id,ParentTerritory2.ParentTerritory2id from Territory2 where ParentTerritory2.ParentTerritory2id=null AND Territory2Model.State = 'Active'];
        system.debug('objTerr: '+JSON.serializePretty(objTerr));
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        system.runAs(u[0]){
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : accList){
                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = objTerr[0].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                lstTerr.add(obj);
            }      
            insert lstTerr;
        }
        Test.startTest();
        UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                               RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_SMA);
        insert objUserTerritory2Association;
        Test.stopTest();
        try
        {
            delete objUserTerritory2Association;
        }
        catch(Exception e){
            System.debug('err: '+e.getMessage());
        }
        UserTriggerHandler.disableTrigger = false;
    }
    @isTest
    public static void testMethodUpdateAccountSMA_DEP(){
        UserTriggerHandler.disableTrigger = true;
        List<Account> accList = [SELECT Id FROM Account]; 
        List<Territory2> objTerr = [select id,ParentTerritory2.ParentTerritory2id from Territory2 where ParentTerritory2.ParentTerritory2id=null AND Territory2Model.State = 'Active'];
        system.debug('objTerr: '+JSON.serializePretty(objTerr));
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        system.runAs(u[0]){
            List<ObjectTerritory2Association> lstTerr= new List<ObjectTerritory2Association>();
            for(Account a : accList){
                ObjectTerritory2Association obj = new ObjectTerritory2Association();
                obj.ObjectId = a.id;
                obj.Territory2Id = objTerr[0].id;
                obj.AssociationCause = ConstantsUtil.MANUAL_TERRITORY_ASSOCIATION;
                lstTerr.add(obj);
            }      
            insert lstTerr;
        }
        Test.startTest();
        UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(Territory2Id= objTerr[0].Id, UserId= u[0].Id,
                                                                                               RoleInTerritory2=ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP);
        insert objUserTerritory2Association;
        Test.stopTest();
        try
        {
            delete objUserTerritory2Association;
        }
        catch(Exception e){
            System.debug('err: '+e.getMessage());
        }
        UserTriggerHandler.disableTrigger = false;
    }
}