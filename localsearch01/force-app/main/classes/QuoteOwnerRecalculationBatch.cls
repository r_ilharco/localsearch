/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class QuoteOwnerRecalculationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    private List<Id> accountIds;
    private String sSQL = null;
    public QuoteOwnerRecalculationBatch(List<Id> accountIds){
        this.accountIds = new List<Id>(accountIds);
        system.debug('accountIds in constructor: ' + accountIds);
    }
    
    global database.querylocator start(Database.BatchableContext bc){      
        /*system.debug('accountIds: '+ this.accountIds);
        sSQL = 'SELECT Id, SBQQ__Opportunity2__r.Account.Owner_Changed__c  FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.AccountId IN :accountIds';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);*/
        return null;
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Quote__c> scope){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>(scope);
        system.debug('quotes: ' + quotes);
        List<SBQQ__Quote__Share> newQuoteShares = new List<SBQQ__Quote__Share>();
        //try{
        if(quotes.values().size() > 0){
            Set<String> areaCodes = new Set<String>();
         /*  for(SBQQ__Quote__c q : quotes.values()){
                if(q.SBQQ__Opportunity2__r.Account.Area_Code__c != null){
                    areaCodes.add(q.SBQQ__Opportunity2__r.Account.Area_Code__c);
                }
            }
            if(areaCodes.size() > 0){
                List<UserTerritory2Association> territoryAssociations = [SELECT Id, UserId, Territory2.Name from UserTerritory2Association where Territory2.Name IN :areaCodes]; 
                if(territoryAssociations.size() > 0){
                    Map<String, List<Id>> areaCodeTerritoryRolesMap = new Map<String, List<Id>>();
                    for(UserTerritory2Association territory : territoryAssociations){
                        if(!areaCodeTerritoryRolesMap.containsKey(territory.Territory2.Name)){
                            areaCodeTerritoryRolesMap.put(territory.Territory2.Name, new List<Id>{territory.UserId});
                        }else{
                            areaCodeTerritoryRolesMap.get(territory.Territory2.Name).add(territory.UserId);
                        }
                    }
                    system.debug('areaCodeTerritoryRolesMap: '+ areaCodeTerritoryRolesMap);
                    List<SBQQ__Quote__Share> oldQuoteShares = [SELECT Id FROM SBQQ__Quote__Share WHERE RowCause = :Schema.SBQQ__Quote__Share.RowCause.Owner_Recalculation__c];
                    if(oldQuoteShares.size() > 0){
                        delete oldQuoteShares;
                    }
                    List<SBQQ__Quote__Share> newQuoteShares = new List<SBQQ__Quote__Share>();
                    for(SBQQ__Quote__c quote : quotes.values()){
                        if(quote.SBQQ__Opportunity2__r.Account.Area_Code__c != null){
                            if(areaCodeTerritoryRolesMap.containsKey(quote.SBQQ__Opportunity2__r.Account.Area_Code__c)){
                                for(String user : areaCodeTerritoryRolesMap.get(quote.SBQQ__Opportunity2__r.Account.Area_Code__c)){
                                    SBQQ__Quote__Share newQShare = new SBQQ__Quote__Share();
                                    newQShare.ParentId = quote.id;
                                    newQShare.UserOrGroupId = user;
                                    newQShare.AccessLevel = 'Read';
                                    newQShare.RowCause = Schema.SBQQ__Quote__Share.RowCause.Owner_Recalculation__c;
                                    newQuoteShares.add(newQShare);
                                }
                            }
                        }
                    }
                    if(newQuoteShares.size() > 0){
                        system.debug('newQuoteShares: ' + newQuoteShares);
                        insert newQuoteShares;
                        /*Database.SaveResult[] srList = Database.insert(newQuoteShares, false);
                        for (Database.SaveResult sr : srList) {
                            if (sr.isSuccess()) {
                                System.debug('Successfully inserted the following quote: ' + sr.getId());
                            }
                            else {              
                                System.debug('there was an error with the following quote: ' + sr.getId());
                                for(Database.Error err : sr.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                    System.debug('quote fields that affected this error: ' + err.getFields());
                                }
                                quotes.get(sr.getId()). = true;
                            }
                        }
                    }
                }
            }*/
        }
        /*}catch(Exception ex){
            system.debug('stackTrace: ' + ex.getStackTraceString());
        }*/
    }
    global void finish(Database.BatchableContext bc){     
        
    }
}