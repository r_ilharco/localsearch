public class SRV_CampaignStatusInbound {
	
    public static void activateOrder(OrderItem myCampFather){
        Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem>();
        //Getting all order items.
        List<OrderItem> orderItems = SEL_OrderItem.getOrderItemByOrderId(myCampFather.OrderId);
        
        if(!orderItems.isEmpty()){
            Boolean updateOrder = true;
            String orderStatus = orderItems[0].Order.Status;
            if(ConstantsUtil.ORDER_STATUS_PRODUCTION.equals(orderStatus)){
                for(OrderItem currentOrderItem : orderItems){
                    if(myCampFather.Id.equals(currentOrderItem.Id) || myCampFather.Id.equals(currentOrderItem.SBQQ__RequiredBy__c)){
                        //Setting the new servicedate and status only for order items related to the current campaign
                        currentOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
                        currentOrderItem.ServiceDate = Date.today();
                        orderItemsToUpdate.put(currentOrderItem.Id, currentOrderItem);
                    }
                }
                for(OrderItem currentOrderItem : orderItems){
                    if(!ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED.equals(currentOrderItem.SBQQ__Status__c)){
                        updateOrder = false;
                    }
                }
                Savepoint sp;
                try{
                    sp = Database.setSavepoint();
                    if(!orderItemsToUpdate.isEmpty()){
                        update orderItemsToUpdate.values();
                    }
                    if(updateOrder){
                        Order orderToUpdate = new Order();
                        orderToUpdate.Id = myCampFather.OrderId;
                        orderToUpdate.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                        orderToUpdate.SBQQ__Contracted__c = true;
                        update orderToUpdate;
                    }
                }
                catch(Exception e){
                    Database.rollback(sp);
                    throw new CampaignStatusInbound.CustomException(e.getMessage());
                }
            }
            else throw new CampaignStatusInbound.CustomException('Invalid status: Order cannot be activated.');
        }
        else throw new CampaignStatusInbound.CustomException('Order cannot be found.');
    }
    
    public static void terminateOrder(SBQQ__Subscription__c myCampFather){
        Set<Id> subscriptionIds = new Set<Id>();
		Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem>();
        
        String contractId;
        List<SBQQ__Subscription__c> myCampSubs = [SELECT Id, Subsctiption_Status__c, SBQQ__Contract__c, Termination_Date__c, SBQQ__TerminatedDate__c, In_Termination__c 
                                                  FROM SBQQ__Subscription__c 
                                                  WHERE SBQQ__RequiredById__c = : myCampFather.Id 
                                                  OR Id = :myCampFather.Id];
        
        for(SBQQ__Subscription__c currentSub : myCampSubs){
            subscriptionIds.add(currentSub.Id);
            contractId = currentSub.SBQQ__Contract__c;
        }
        
        List<OrderItem> orderItems = [SELECT Id, OrderId, Order.Status, Order.Type
                                      FROM OrderItem 
                                      WHERE Order.Status = :ConstantsUtil.ORDER_STATUS_PRODUCTION 
                                      AND (Order.Type = :ConstantsUtil.ORDER_TYPE_TERMINATION OR Order.Type = :ConstantsUtil.ORDER_TYPE_CANCELLATION) 
                                      AND SBQQ__Subscription__c IN :subscriptionIds];
        
        if(!orderItems.isEmpty()){
            
            Boolean updateOrder = true;
            String orderStatus = orderItems[0].Order.Status;
			String orderId = orderItems[0].OrderId;
            String orderType = orderItems[0].Order.Type;
            
            if(ConstantsUtil.ORDER_STATUS_PRODUCTION.equals(orderStatus)){
                for(OrderItem currentOrderItem : orderItems){
                    currentOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
                    currentOrderItem.ServiceDate = Date.today();
                    orderItemsToUpdate.put(currentOrderItem.Id, currentOrderItem);
                }
            }
            
            if(!orderItemsToUpdate.isEmpty()) update orderItemsToUpdate.values();
            
            List<OrderItem> allOrderItems = SEL_OrderItem.getOrderItemByOrderId(orderId);
            for(OrderItem currentOrderItem : allOrderItems){
                if(!ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED.equals(currentOrderItem.SBQQ__Status__c)){
                    updateOrder = false;
                }
            }
            if(updateOrder){
                Order orderToUpdate = new Order();
                orderToUpdate.Id = orderId;
                orderToUpdate.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                update orderToUpdate;
                
                for(SBQQ__Subscription__c currentSubscription : myCampSubs){
                    currentSubscription.Termination_Date__c = Date.today();
                    currentSubscription.SBQQ__TerminatedDate__c = Date.today();
                    //currentSubscription.End_Date__c = Date.today();
                    //currentSubscription.In_Termination__c = false;
                }
                update myCampSubs;
                
                Boolean updateContract = true;
                List<SBQQ__Subscription__c> allSubs = SEL_Subscription.getSubscriptionsByContractIds(new Set<Id>{contractId});
                for(SBQQ__Subscription__c currentSubscription : allSubs){
                    if(!currentSubscription.In_Termination__c){
                        updateContract = false;
                    }
                }
                if(updateContract){
                    Contract contractToUpdate = new Contract();
                    contractToUpdate.Id = contractId;
                    if(ConstantsUtil.ORDER_TYPE_TERMINATION.equals(orderType)){
                        //contractToUpdate.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
                        contractToUpdate.TerminateDate__c = Date.today();
                        contractToUpdate.In_Termination__c = true;
                    }
                    else if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equals(orderType)){
                        //contractToUpdate.Status = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
                        contractToUpdate.Cancel_Date__c = Date.today();
                        contractToUpdate.In_Cancellation__c = true;
                    }
                    update contractToUpdate;
                }
            }
            else throw new CampaignStatusInbound.CustomException('Order cannot be activated.');
        }
        else throw new CampaignStatusInbound.CustomException('Order cannot be found.'); 
    }
}