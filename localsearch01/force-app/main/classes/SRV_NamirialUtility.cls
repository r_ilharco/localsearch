/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Digital signature process - Service class
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-09-30
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class SRV_NamirialUtility {
    
	private static QuoteDocumentSetting__mdt quoteDocumentSettingsMDT = [SELECT DaysUntilExpire__c, DaysUntilSendEmailProhibited__c FROM QuoteDocumentSetting__mdt LIMIT 1];
    private static final Integer MAX_DAYS_UNTIL_EXPIRE = 28;
    private static final Integer DEFAULT_DAYS_UNTIL_EXPIRE = 7;
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Generate a map with email subject and email body translated in Italian, French and German.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @return	Map<String, Map<String, String>>	Map LanguageCode -> ProcessName -> Phrase
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
	public static Map<String, Map<String, String>> generateLabelByLangCodeMap(){

        Map<String, Map<String, String>> namirialLabelsByLangCodeMap = new Map<String, Map<String, String>>();
        namirialLabelsByLangCodeMap.put('de-CH', new Map<String, String>());
        namirialLabelsByLangCodeMap.put('fr-CH', new Map<String, String>());
        namirialLabelsByLangCodeMap.put('it-CH', new Map<String, String>());
        
        namirialLabelsByLangCodeMap.get('de-CH').put('Namirial_email_mailto_phrase',Label.Namirial_email_mailto_phrase_de);
        namirialLabelsByLangCodeMap.get('de-CH').put('Namirial_email_subject',Label.Namirial_email_subject_de);
        namirialLabelsByLangCodeMap.get('de-CH').put('Namirial_email_customer_advisor',Label.Namirial_email_customer_advisor_de);
        namirialLabelsByLangCodeMap.get('de-CH').put('Namirial_email_mailto_phrase2',Label.Namirial_email_mailto_phrase2_de);
        
        namirialLabelsByLangCodeMap.get('it-CH').put('Namirial_email_mailto_phrase',Label.Namirial_email_mailto_phrase_it);
        namirialLabelsByLangCodeMap.get('it-CH').put('Namirial_email_subject',Label.Namirial_email_subject_it);
        namirialLabelsByLangCodeMap.get('it-CH').put('Namirial_email_customer_advisor',Label.Namirial_email_customer_advisor_it);
        namirialLabelsByLangCodeMap.get('it-CH').put('Namirial_email_mailto_phrase2',Label.Namirial_email_mailto_phrase2_it);
        
        namirialLabelsByLangCodeMap.get('fr-CH').put('Namirial_email_mailto_phrase',Label.Namirial_email_mailto_phrase_fr);
        namirialLabelsByLangCodeMap.get('fr-CH').put('Namirial_email_subject',Label.Namirial_email_subject_fr);
        namirialLabelsByLangCodeMap.get('fr-CH').put('Namirial_email_customer_advisor',Label.Namirial_email_customer_advisor_fr);
        namirialLabelsByLangCodeMap.get('fr-CH').put('Namirial_email_mailto_phrase2',Label.Namirial_email_mailto_phrase2_fr);
        
        return namirialLabelsByLangCodeMap;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method calculates the validity of the Quote Document being signed.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	SBQQ__QuoteDocument__c    	Quote Document record being signed
    * @return			Integer						Days of validity of the Quote Document
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
	public static Integer calculateDaysUntilExpire(SBQQ__QuoteDocument__c quoteDocument){

        Integer daysUntilExpire = Integer.valueOf(quoteDocumentSettingsMDT.DaysUntilExpire__c);

        Date qdExpirationDate = Date.today().addDays(daysUntilExpire);
        if(quoteDocument.SBQQ__Quote__c != NULL){
            if(quoteDocument.SBQQ__Quote__r.SBQQ__ExpirationDate__c != NULL){
                if(qdExpirationDate > quoteDocument.SBQQ__Quote__r.SBQQ__ExpirationDate__c){
                    daysUntilExpire = Date.today().daysBetween(quoteDocument.SBQQ__Quote__r.SBQQ__ExpirationDate__c);
                }
            }
        }

        if(daysUntilExpire > MAX_DAYS_UNTIL_EXPIRE) daysUntilExpire = MAX_DAYS_UNTIL_EXPIRE;
        else if(daysUntilExpire < 1) daysUntilExpire = DEFAULT_DAYS_UNTIL_EXPIRE;

        return daysUntilExpire;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method retrieves customer informations such as First Name, Last Name, Email and preferred
    * Language that will be used to send the signed document via e-mail.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	SBQQ__QuoteDocument__c    	Quote Document record being signed
    * @return			PrimaryContactData			Wrapper class containing all the customer infos
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static PrimaryContactData retrieveContactData(SBQQ__QuoteDocument__c quoteDocument){

        PrimaryContactData contactData = new PrimaryContactData();

        if(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__c != NULL){
            contactData.emailPrimaryContact = String.isNotBlank(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Email) ? quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Email : '';
            contactData.firstNamePrimaryContact = String.isNotBlank(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.FirstName) ? quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.FirstName : '';
            contactData.lastNamePrimaryContact = String.isNotBlank(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.LastName) ? quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.LastName : '';
        	contactData.language = String.isNotBlank(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Language__c) ? quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Language__c : '';
            contactData.accountLanguage = String.isNotBlank(quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Account.PreferredLanguage__c) ? quoteDocument.SBQQ__Quote__r.Filtered_Primary_Contact__r.Account.PreferredLanguage__c : '';
            contactData.emailLanguage = String.isNotBlank(contactData.language) ? contactData.language : String.isNotBlank(contactData.accountLanguage) ? contactData.accountLanguage : 'German';
        }
        
        switch on quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.LanguageLocaleKey {
            when 'de'{
                contactData.salesLanguageCode = 'de-CH';
            }
            when 'fr'{
                contactData.salesLanguageCode = 'fr-CH';
            }
            when 'it'{
                contactData.salesLanguageCode = 'it-CH';
            }
            when else{
                contactData.salesLanguageCode = 'de-CH';                 
            }
        }

        switch on contactData.emailLanguage
        {
            when 'German' {
                contactData.emailLanguageCode = 'de-CH';
            }
            when 'Italian' {
                contactData.emailLanguageCode = 'it-CH';
            }
            when 'French' {
                contactData.emailLanguageCode = 'fr-CH';
            }
            when else {
                contactData.emailLanguageCode = 'de-CH';
            }
        }

        return contactData;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method checks if a valid method has been selected to proceed with the Digital Signature.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	method    							Selected Signature Method
    * @input param		namirialSignatureProcessResponse	Wrapper class containing all the infos
    * 														to send back to Lightning Component
    * 														"SignTheDocument"
    * @return			Boolean								True if the selected method is valid,
    * 														false otherwise
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Boolean isValidMethod(String method, NamirialUtility.NamirialSignatureProcessResponse namirialSignatureProcessResponse){

        Boolean isSendEmailMetod = ConstantsUtil.SENDMAIL_NAMIRIALSIGNATUREMETHOD_LABEL.equalsIgnoreCase(method);
        Boolean isSignNowMethod = ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL.equalsIgnoreCase(method);
        Boolean isManualMethod = ConstantsUtil.MANUAL_NAMIRIALSIGNATUREMETHOD_LABEL.equalsIgnoreCase(method);
        if(!isSendEmailMetod && !isSignNowMethod && !isManualMethod){
            namirialSignatureProcessResponse.processFailed = true;
            namirialSignatureProcessResponse.errorMessage = Label.Namirial_InvalidSignature;
            return false;
        }
        else return true;
    }
	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method checks if the customer record (Filtered Primary Contact on Quote) has the FirstName
    * LastName and Email fields populated.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	contactData    						Wrapper class containing all the customer 
    * 														infos
    * @input param		namirialSignatureProcessResponse	Wrapper class containing all the infos
    * 														to send back to Lightning Component
    * 														"SignTheDocument"
    * @return			Boolean								True if FirstName, LastName and Email 
    * 														fields are populated, false otherwise
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Boolean isValidContact(PrimaryContactData contactData, NamirialUtility.NamirialSignatureProcessResponse namirialSignatureProcessResponse){
        
        Boolean emailBlank = String.isBlank(contactData.emailPrimaryContact);
        Boolean firstNameBlank = String.isBlank(contactData.firstNamePrimaryContact);
        Boolean lastNameBlank = String.isBlank(contactData.lastNamePrimaryContact);
        if(emailBlank || firstNameBlank || lastNameBlank){

            namirialSignatureProcessResponse.errorMessage = Label.Namirial_PrimaryContact_Check + ' ';
            
            if(emailBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get('Email').getDescribe().getLabel();
            if(firstNameBlank && emailBlank) namirialSignatureProcessResponse.errorMessage += ', ';
            if(firstNameBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get('FirstName').getDescribe().getLabel();
            if(lastNameBlank && (firstNameBlank || emailBlank)) namirialSignatureProcessResponse.errorMessage += ', ';
            if(lastNameBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get('LastName').getDescribe().getLabel();
            namirialSignatureProcessResponse.processFailed = true;
            return false;
        }
        else return true;
    }
	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method checks if the sales record (SBQQ__SalesRep__c field on Quote) has the FirstName
    * LastName and Email fields populated.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	contactData    						Wrapper class containing all the customer 
    * 														infos
    * @input param		namirialSignatureProcessResponse	Wrapper class containing all the infos
    * 														to send back to Lightning Component
    * 														"SignTheDocument"
    * @return			Boolean								True if FirstName, LastName and Email 
    * 														fields are populated, false otherwise
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Boolean isValidSalesRep(SBQQ__QuoteDocument__c quoteDocument, NamirialUtility.NamirialSignatureProcessResponse namirialSignatureProcessResponse){
        
        Boolean emailBlank = String.isBlank(quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.Email);
        Boolean firstNameBlank = String.isBlank(quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.FirstName);
        Boolean lastNameBlank = String.isBlank(quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.LastName);
        if(emailBlank || firstNameBlank || lastNameBlank){

            namirialSignatureProcessResponse.errorMessage = Label.Namirial_SalesRep_Check + ' ';
            
            if(emailBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap().get('Email').getDescribe().getLabel();
            if(firstNameBlank && emailBlank) namirialSignatureProcessResponse.errorMessage += ', ';
            if(firstNameBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap().get('FirstName').getDescribe().getLabel();
            if(lastNameBlank && (firstNameBlank || emailBlank)) namirialSignatureProcessResponse.errorMessage += ', ';
            if(lastNameBlank) namirialSignatureProcessResponse.errorMessage += Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap().get('LastName').getDescribe().getLabel();
            namirialSignatureProcessResponse.processFailed = true;
            return false;
        }
        else return true;
    }
    
     /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method updates the Quote in Presented status and the Quote Document in "Sent" status. Also
    * add the envelope id received from Namirial on Quote Document field EnvelopeId__c.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param		quoteDocument						Quote Document being signed
    * @input param		envelopeId							Envelope id received from Namirial integration
    * @input param		method								Selected signature method
    * @input param		daysUntilExpire						Days of validity of the Quote Document
    * @input param		namirialSignatureProcessResponse	Wrapper class containing all the infos
    * 														to send back to Lightning Component
    * 														"SignTheDocument"
    * @return			void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static void updateQuoteAndDocument(SBQQ__QuoteDocument__c quoteDocument, String envelopeId, String method, Integer daysUntilExpire,string workstep_redirection_url){
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(Id = quoteDocument.SBQQ__Quote__c);
        quote.SBQQ__Status__c = ConstantsUtil.QUOTE_STATUS_PRESENTED;
        
        quoteDocument.EnvelopeId__c = envelopeId;
        quoteDocument.NamirialSignature_ProcessType__c = method;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        quoteDocument.NamirialSignature_Url__c = workstep_redirection_url;
        quoteDocument.ExpirationDate__c = Date.today().addDays(daysUntilExpire);
        
        
        
        update quoteDocument;
        Database.update(quote, false);
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method builds the body to send to TIBCO in order to upload the document on Namirial and
    * do the Send Envelope callout. Step 1 contains the Sales Rep infos that will be sent to Namirial,
    * while Step 2 contains the Customer infos.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	uploadPrepareAndSendRequest    		Wrapper class for callout's request
    * @input param		quoteDocument						Quote Document being signed
    * @input param		doc									Document record related to Quote Document
    * @input param		contactData    						Wrapper class containing all the customer 
    * 														infos
    * @input param		method								Selected signature method
    * @return			void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static NamirialJSONWrappers.UploadPrepareAndSendRequest buildPrepareSendRequestBody(SBQQ__QuoteDocument__c quoteDocument, Document doc, PrimaryContactData contactData, String method, Integer daysUntilExpire){
        
        NamirialJSONWrappers.UploadPrepareAndSendRequest uploadPrepareAndSendRequest = new NamirialJSONWrappers.UploadPrepareAndSendRequest();
        
        Map<String, Map<String, String>> namirialLabelsByLangCodeMap = generateLabelByLangCodeMap();
        
        uploadPrepareAndSendRequest.sspfile_uploadtemporary.file.name = EncodingUtil.urlEncode(doc.Name, 'UTF-8');
        uploadPrepareAndSendRequest.sspfile_uploadtemporary.file.data = EncodingUtil.base64Encode(doc.Body);

        List<NamirialJSONWrappers.Steps> steps = new List<NamirialJSONWrappers.Steps>();
        
        NamirialJSONWrappers.Steps step1 = new NamirialJSONWrappers.Steps();
        step1.order_index = 1;
        step1.recipients.add(buildRecipientSalesRep(quoteDocument, contactData, namirialLabelsByLangCodeMap, false));
        steps.add(step1);

        NamirialJSONWrappers.Steps step2 = new NamirialJSONWrappers.Steps();
        step2.order_index = 2;
        step2.recipients.add(buildRecipientCustomer(quoteDocument, method, contactData, namirialLabelsByLangCodeMap, false));
        steps.add(step2);
        
        NamirialJSONWrappers.Steps step3 = new NamirialJSONWrappers.Steps();
        step3.order_index = 3;
        step3.recipients.add(buildRecipientCustomer(quoteDocument, method, contactData, namirialLabelsByLangCodeMap, true));
        steps.add(step3);
        
        NamirialJSONWrappers.Steps step4 = new NamirialJSONWrappers.Steps();
        step4.order_index = 4;
        step4.recipients.add(buildRecipientSalesRep(quoteDocument, contactData, namirialLabelsByLangCodeMap, true));
        steps.add(step4);
        
        uploadPrepareAndSendRequest.send_envelope_description.steps = steps;
        uploadPrepareAndSendRequest.send_envelope_description.name = doc.Name;
        uploadPrepareAndSendRequest.send_envelope_description.email_body = namirialLabelsByLangCodeMap.get(contactData.emailLanguageCode).get('Namirial_email_mailto_phrase') + ' <a href="mailto:' +  quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.Email + '">' + namirialLabelsByLangCodeMap.get(contactData.emailLanguageCode).get('Namirial_email_customer_advisor') + '</a>.';
        uploadPrepareAndSendRequest.send_envelope_description.email_subject = namirialLabelsByLangCodeMap.get(contactData.emailLanguageCode).get('Namirial_email_subject');
        uploadPrepareAndSendRequest.send_envelope_description.days_until_expire = daysUntilExpire;
        
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2;
        if(!p.isEmpty()) c2 = Mashery_Setting__c.getInstance(p[0].Id);    
        else c2 = Mashery_Setting__c.getOrgDefaults();
        String callbackMethodName = CustomMetadataUtil.getMethodName(ConstantsUtil.NAMIRIAL_INTEGRATIONCONFIG_CALLBACK);

        uploadPrepareAndSendRequest.send_envelope_description.status_update_callback_url = c2.Endpoint__c + callbackMethodName + '?Envelope=##EnvelopeId##&Action=##Action##&QuoteDocumentId=' + quoteDocument.Id + '&ActionCode=' + ConstantsUtil.actionCodesByMethods.get(method);

        return uploadPrepareAndSendRequest;
    }
    
    public static NamirialJSONWrappers.Recipients buildRecipientSalesRep(SBQQ__QuoteDocument__c quoteDocument, PrimaryContactData contactData, Map<String, Map<String, String>> namirialLabelsByLangCodeMap, Boolean isCCRecipient){
		
        NamirialJSONWrappers.Recipients recipient = new NamirialJSONWrappers.Recipients();
        recipient.first_name = quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.FirstName;
        recipient.last_name = quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.LastName;
        recipient.email = quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.Email;
        recipient.language_code = contactData.salesLanguageCode;
        recipient.disable_email = true;
        recipient.allow_delegation = false;
        
        if(isCCRecipient){
            recipient.disable_email = false;
            recipient.email_body_extra = namirialLabelsByLangCodeMap.get(contactData.salesLanguageCode).get('Namirial_email_mailto_phrase2') + ' <a href="mailto:' +  quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.Email + '">' + namirialLabelsByLangCodeMap.get(contactData.salesLanguageCode).get('Namirial_email_customer_advisor') + '</a>.';
        }
        
        return recipient;
    }
    
    public static NamirialJSONWrappers.Recipients buildRecipientCustomer(SBQQ__QuoteDocument__c quoteDocument, String method, PrimaryContactData contactData, Map<String, Map<String, String>> namirialLabelsByLangCodeMap, Boolean isCCRecipient){
		
        NamirialJSONWrappers.Recipients recipient = new NamirialJSONWrappers.Recipients();
        recipient.first_name = contactData.firstNamePrimaryContact;
        recipient.last_name = contactData.lastNamePrimaryContact;
        recipient.email = contactData.emailPrimaryContact;
        recipient.language_code = contactData.emailLanguageCode;
        recipient.disable_email = ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL.equalsIgnoreCase(method);
        recipient.allow_delegation = false;
        
        if(isCCRecipient){
            recipient.email_body_extra = namirialLabelsByLangCodeMap.get(contactData.emailLanguageCode).get('Namirial_email_mailto_phrase2') + ' <a href="mailto:' +  quoteDocument.SBQQ__Quote__r.SBQQ__SalesRep__r.Email + '">' + namirialLabelsByLangCodeMap.get(contactData.emailLanguageCode).get('Namirial_email_customer_advisor') + '</a>.';
			recipient.disable_email = false;
        }
        
        return recipient;
    }
    
    public class PrimaryContactData{
        public String emailPrimaryContact{get;set;}
        public String firstNamePrimaryContact{get;set;}
        public String lastNamePrimaryContact{get;set;}
        public String language{get;set;}
        public String accountLanguage{get;set;}
        public String emailLanguage{get;set;}
        public String emailLanguageCode{get;set;}
        public String salesLanguageCode{get;set;}
    }
    
}