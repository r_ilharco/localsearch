global class SubscriptionTerritoryChangedBatch_Sched implements Schedulable{

    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'SubscriptionTerritoryChangedBatch%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
 			if(batchs.isEmpty()|| !batchs.contains('SubscriptionTerritoryChangedBatch0')){
                SubscriptionTerritoryChangedBatch b0 = new SubscriptionTerritoryChangedBatch(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('SubscriptionTerritoryChangedBatch0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch1')){ 
                SubscriptionTerritoryChangedBatch b1 = new SubscriptionTerritoryChangedBatch(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch2')){
                SubscriptionTerritoryChangedBatch b2 = new SubscriptionTerritoryChangedBatch(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch3')){
                SubscriptionTerritoryChangedBatch b3 = new SubscriptionTerritoryChangedBatch(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch4')){
                SubscriptionTerritoryChangedBatch b4 = new SubscriptionTerritoryChangedBatch(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch5')){
                SubscriptionTerritoryChangedBatch b5 = new SubscriptionTerritoryChangedBatch(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch6')){
                SubscriptionTerritoryChangedBatch b6 = new SubscriptionTerritoryChangedBatch(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch7')){
                SubscriptionTerritoryChangedBatch b7 = new SubscriptionTerritoryChangedBatch(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch8')){
                SubscriptionTerritoryChangedBatch b8 = new SubscriptionTerritoryChangedBatch(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('SubscriptionTerritoryChangedBatch9')){
                SubscriptionTerritoryChangedBatch b9 = new SubscriptionTerritoryChangedBatch(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('SubscriptionTerritoryChangedBatch9', strSchedule,b9);
            }
        }
    }   
}