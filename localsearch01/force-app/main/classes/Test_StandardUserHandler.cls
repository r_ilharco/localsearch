@isTest
public class Test_StandardUserHandler {
    public static String utcPermissionSets = 'Case_Management_Complaint;Case_Assignment_Permission;Data_Management;Duplicate_Management;Customer_Service_Supervisor;CPQUserCustomSB;View_Reports_and_Dashboards;Knowledge_Agent;';
	@testSetup static void setupData()
    {
        /*SetupEntityAccess  sea = new SetupEntityAccess();
        CustomPermission cp = [SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass'];
        PermissionSet ps = [SELECT Id, Name, Profile.Name FROM PermissionSet WHERE ProfileId = :UserInfo.getProfileId()];
        SetupEntityAccess seaQueried = [SELECT Id FROM SetupEntityAccess WHERE SetupEntityId = :cp.Id AND  ParentId = :ps.Id LIMIT 1];
        System.debug('>>>FeatureManagement.checkPermission(\'ADFSByPass\'): ' + FeatureManagement.checkPermission('ADFSByPass'));
        if(seaQueried == NULL){
            sea.SetupEntityId = cp.Id;
            sea.ParentId = ps.Id;
            insert sea;
        }
        TriggerHandler.bypass('UserTriggerHandler');*/
        PermissionSet ps = [SELECT Id, Name, Profile.Name FROM PermissionSet WHERE Name = 'CRMUserPsl' LIMIT 1];
        if(ps == NULL)
        {
            ps = new PermissionSet();
            ps.Name = 'CRMUserPsl';
            ps.Label = 'CRMUserPsl';
            insert ps;
        }
        Test_DataFactory.insertFutureUsers();
    }
    
    @isTest
    static void test_createUserDMC()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierDMCTest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'username@test.it',
                'user.federationidentifier' => 'FederationIdentifierDMCTest',
                'user.code__c' => 'DMC',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1DMC',
                'user.lastname' => 'lastName1DMC',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'DMCt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'departmentNumberTest',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
        Test.startTest();
        		try{
        			suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    
                }
        Test.stopTest();
    }
    
    @isTest
    static void test_createUserSMA()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierSMATest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'usernameSMA@test.it',
                'user.federationidentifier' => 'FederationIdentifierSMATest',
                'user.code__c' => 'SMA',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1SMA',
                'user.lastname' => 'lastName1SMA',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'SMAt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06-VKL6.14',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
        Test.startTest();
        		try{
        			suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    
                }
        Test.stopTest();
    }
    
    @isTest
    static void test_createUserSRD()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierSRDTest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'usernameSRD@test.it',
                'user.federationidentifier' => 'FederationIdentifierSRDTest',
                'user.code__c' => 'SRD',
                'user.phone' => '+393422222221',
                'user.email' => 'emailSRD@test.it',
                'user.firstname' => 'firstName1SRD',
                'user.lastname' => 'lastName1SRD',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'SRDt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
        Test.startTest();
        		try{
        			suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    
                }
        Test.stopTest();
    }
    
    @isTest
    static void test_updateUser()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        User dmcUser = [SELECT Id, Code__c FROM User WHERE Username = 'dmctester@test.com'];
        User managerUser = [SELECT Id, Code__c, FederationIdentifier FROM User WHERE Username = 'managertester@test.com'];
        dmcUser.Code__c = 'DMC';
        dmcUser.FederationIdentifier = 'FederationIdentifierTest';
        
        System.debug('managerUser: ' + JSON.serialize(managerUser));
        
        update dmcUser;
        
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierTest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'username@test.it',
                'user.federationidentifier' => 'FederationIdentifierTest',
                'user.code__c' => 'SMA',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1',
                'user.lastname' => 'lastName1',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'test01',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06-VKL6.14',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
        Test.startTest();
        		try
                {
        			suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    
                }
        		attributes.remove('user.federationidentifier');
        		attributes.remove('user.manager');
        		communityId = UserInfo.getUserId();
        		try{
        			suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    System.assert(e != NULL);
                }
        Test.stopTest();
    }
    
    @isTest
    static void test_updateUser2()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        User dmcUser = [SELECT Id, Code__c FROM User WHERE Username = 'dmctester@test.com'];
        User managerUser = [SELECT Id, Code__c, FederationIdentifier FROM User WHERE Username = 'managertester@test.com'];
        dmcUser.Code__c = 'DMC';
        dmcUser.FederationIdentifier = 'FederationIdentifierTest';
        
        System.debug('managerUser: ' + JSON.serialize(managerUser));
        
        update dmcUser;
        
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierTest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'username@test.it',
                'user.federationidentifier' => 'FederationIdentifierTest',
                'user.code__c' => 'SRD',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1',
                'user.lastname' => 'lastName1',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'test01',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        Boolean isStandard = true;
        StandardUserHandler suh = new StandardUserHandler();
        Test.startTest();
        		try
                {
        			suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    
                }
        		attributes.remove('user.federationidentifier');
        		attributes.remove('user.manager');
        		communityId = UserInfo.getUserId();
        		try{
        			suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                }catch(Exception e)
                {
                    System.assert(e != NULL);
                }
        Test.stopTest();
    }
    
    @isTest
    static void test_updateUserSMA()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierSMATest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
            'user.username' => 'usernameSMA@test.it',
                'user.federationidentifier' => 'FederationIdentifierSMATest',
                'user.code__c' => 'SMA',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1SMA',
                'user.lastname' => 'lastName1SMA',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'SMAt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06-VKL6.14',
                'user.callcenter' => 'AltitudeSalesforceConnector'
                };
        
        StandardUserHandler suh = new StandardUserHandler();
        
        try{
            suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        }catch(Exception e)
        {
            
        }
        
        Boolean isStandard = true;
        
        Test.startTest();
        	try
            {
                attributes.put('user.departmentnumber','LCH-SLD-RD05-VKL5.09');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','LCH-SLD-RD05');
                attributes.put('user.code__c','SRD');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','LCH-SLD-RD06');
                attributes.put('user.code__c','SRD');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                /*attributes.put('user.departmentnumber','');
                attributes.put('user.code__c','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','SMA');
                attributes.put('user.code__c','LCH-SLD-RD06-VKL6.14');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','');
                attributes.put('user.code__c','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);*/
            }catch(Exception e)
            {
                
            }
        Test.stopTest();
    }
    
    @isTest
    static void test_updateUserSMA2()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'FederationIdentifierSMATest';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
            'user.username' => 'usernameSMA@test.it',
                'user.federationidentifier' => 'FederationIdentifierSMATest',
                'user.code__c' => 'SMA',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1SMA',
                'user.lastname' => 'lastName1SMA',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'SMAt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'LCH-SLD-RD06-VKL6.14',
                'user.callcenter' => 'AltitudeSalesforceConnector'
                };
        
        StandardUserHandler suh = new StandardUserHandler();
        
        try{
            suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        }catch(Exception e)
        {
            
        }
        
        Boolean isStandard = true;
        
        Test.startTest();
        	try
            {
                /*attributes.put('user.departmentnumber','LCH-SLD-RD05-VKL5.09');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','LCH-SLD-RD05');
                attributes.put('user.code__c','SRD');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','LCH-SLD-RD06');
                attributes.put('user.code__c','SRD');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);*/
                attributes.put('user.departmentnumber','');
                attributes.put('user.code__c','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','SMA');
                attributes.put('user.code__c','LCH-SLD-RD06-VKL6.14');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.departmentnumber','');
                attributes.put('user.code__c','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
            }catch(Exception e)
            {
                
            }
        Test.stopTest();
    }
    
    @isTest
    static void test_updateUserDMC()
    {
        List<SetupEntityAccess> seas = [SELECT Id, ParentId, SetupEntityId, SetupEntityType, SystemModstamp FROM SetupEntityAccess WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = 'ADFSByPass')];
        delete seas;
        User managerUser = [SELECT Id, Code__c, FederationIdentifier FROM User WHERE Username = 'managertester@test.com'];
        Id userId = NULL;
        Id samlSsoProviderId = NULL;
        Id communityId = NULL;
        Id portalId = NULL;
        String federationIdentifier = 'managerFederationId';
        String assertion = NULL;
        
        Boolean create = true;
        Map<String, String> attributes = new Map<String, String>{
                'user.username' => 'usernameSMA@test.it',
                'user.federationidentifier' => 'managerFederationId',
                'user.code__c' => 'SMA',
                'user.phone' => '+393422222221',
                'user.email' => 'emailSMA@test.it',
                'user.firstname' => 'firstName1SMA',
                'user.lastname' => 'lastName1SMA',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'SMAt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => '',
                'user.departmentnumber' => 'LCH-SLD-RD06-VKL6.14',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        StandardUserHandler suh = new StandardUserHandler();
        suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        
       	attributes = new Map<String, String>{
                'user.username' => 'username@test.it',
                'user.federationidentifier' => 'FederationIdentifierDMCTest',
                'user.code__c' => 'DMC',
                'user.phone' => '+393422222221',
                'user.email' => 'email@test.it',
                'user.firstname' => 'firstName1DMC',
                'user.lastname' => 'lastName1DMC',
                'user.languagelocalekey' => 'de',
                'user.localesidkey' => 'de_CH',
                'user.alias' => 'DMCt1',
                'user.timezonesidkey' => 'Europe/Berlin',
                'user.emailencodingkey' => 'ISO-8859-1',
                'user.employeekey' => 'employeekeyTest',
                'user.manager' => 'managerFederationId',
                'user.departmentnumber' => 'departmentNumberTest',
                'user.callcenter' => 'AltitudeSalesforceConnector'
        };
        
        try{
            suh.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        }catch(Exception e)
        {
            
        }
        
        Boolean isStandard = true;
        
        Test.startTest();
        	try
            {
                attributes.put('user.code__c','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
                attributes.put('user.code__c','DMC');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);      
            	attributes.put('user.manager','');
                suh.updateUser(userId, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);  
            }catch(Exception e)
            {
                
            }
        Test.stopTest();
    }
}