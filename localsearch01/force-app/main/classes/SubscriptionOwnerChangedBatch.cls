global class SubscriptionOwnerChangedBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    private List<Id> accountIds;
    private String sSQL = null;
    public SubscriptionOwnerChangedBatch(List<Id> accountIds){
        this.accountIds = new List<Id>(accountIds);
        system.debug('accountIds in constructor: ' + accountIds);
    }
    
    global database.querylocator start(Database.BatchableContext bc){    
        List<Id> accId = new List<Id>(this.accountIds);
        system.debug('accId: ' + accId);
        sSQL = 'SELECT Id, SBQQ__Contract__r.Account.OwnerId  FROM SBQQ__Subscription__c WHERE SBQQ__Contract__r.Account.Owner.isActive = true  AND SBQQ__Contract__r.AccountId IN :accId';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        system.debug('subscriptions: ' + scope);
        Map<Id, SBQQ__Subscription__c> subscriptions = new Map<Id, SBQQ__Subscription__c>(scope);
        system.debug('subscriptionIds: ' + subscriptions.keySet());

        List<SBQQ__Subscription__Share> oldSubShares = [SELECT Id FROM SBQQ__Subscription__Share WHERE RowCause = :Schema.SBQQ__Quote__Share.RowCause.Owner_Changed_Sharing__c AND ParentId IN :subscriptions.keySet()];
        if(oldSubShares.size() > 0){
            delete oldSubShares;
        }
        List<SBQQ__Subscription__Share> newSubShares = new List<SBQQ__Subscription__Share>();
        for(SBQQ__Subscription__c sub : subscriptions.values()){
            if(sub.SBQQ__Contract__r.Account.OwnerId != null){
                SBQQ__Subscription__Share newSubShare = new SBQQ__Subscription__Share();
                newSubShare.ParentId = sub.id;
                newSubShare.UserOrGroupId = sub.SBQQ__Contract__r.Account.OwnerId;
                newSubShare.AccessLevel = 'Read';
                newSubShare.RowCause = Schema.SBQQ__Subscription__Share.RowCause.Owner_Changed_Sharing__c;
                newSubShares.add(newSubShare);
            }
        }
        
        if(newSubShares.size() > 0){
            system.debug('newSubShares: ' +  newSubShares);
            insert newSubShares;
            system.debug('newSubShares inserted');
        }
        //}catch(Exception ex){
          //  system.debug('stackTrace: ' + ex.getStackTraceString());
        //}
    }
    global void finish(Database.BatchableContext bc){        
    }
    
    global void execute(SchedulableContext sc){
    }
}