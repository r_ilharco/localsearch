/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date        : 06-02-2019
 * Sprint      : Sprint 2
 * Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
 *               US_75 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (downgrade) to replace a product with another one
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   : 
 *              #1 SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 06-26-2019
 */

@isTest
public class CPQSellingOverrideTest {
    static ByPassFlow__c insertBypassFlowNames(){
            ByPassFlow__c byPassFlow = new ByPassFlow__c();
            byPassFlow.Name = Userinfo.getProfileId();
            byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
            insert byPassFlow;
            return byPassFlow;
    }
    @testSetup static void setup(){
        ByPassFlow__c bPF = insertBypassFlowNames();
        
        Account acct = Test_DataFactory.createAccounts('AccountTest', 1)[0];
        insert acct;
        
        List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',1);
        places[0].Account__c = acct.id;
        
        insert places;
        
        Swisslist_CPQ.config();
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        List<Opportunity> oppts = Test_DataFactory.createOpportunities('OppTest','Qualification',acct.Id,2);
        insert oppts;

        List<SBQQ__Quote__c> quoteDraft = Test_DataFactory.createQuotes('Draft',oppts[0], 1); 
        insert quoteDraft;
        
        List<Contract>cont= Test_DataFactory.getContracts(acct,'Draft',2);
        insert cont;
        cont[0].Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        cont[0].SBQQ__Evergreen__c = false;
        cont[0].EndDate = Date.today().addDays(29);
        update cont[0];
       
        List<SBQQ__Subscription__c> subs = Test_DataFactory.getSubscriptions(acct,places[0], cont[0], new List<Product2>{products.get(productIdByCode.get('SLS001'))},1);
        insert subs;
        subs[0].Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subs[0].place__c = places[0].Id;        
        update subs[0];
        
        quoteDraft[0].SBQQ__PricebookId__c = Test.getStandardPricebookId();
        quoteDraft[0].Upgrade_Downgrade_Contract__c = cont[0].id;
        update quoteDraft[0];
   
        delete bPF; 
    }
    
    @isTest
    public static void ShouldDoCustomSearch(){ 
        
        SObject quote = [select id, SBQQ__PricebookId__c, SBQQ__Type__c, SBQQ__Status__c from SBQQ__Quote__c][0];
        Map<String,Object> fieldValuesMap = new Map<String,Object>();
        CPQSellingOverride c = new CPQSellingOverride();
        
        c.search(quote,fieldValuesMap);
        c.isSearchCustom(quote,fieldValuesMap);
        c.isInputHidden(quote, null);
        c.isSuggestCustom(quote, null);
        c.getInputDefaultValue(quote, null);
        c.getAdditionalSearchFilters(quote, null);
        c.suggest(quote, fieldValuesMap);
        c.isFilterHidden(quote, null);
        c.getAdditionalSuggestFilters(quote, fieldValuesMap);
        c.getFilterDefaultValue(quote, null);
    }
    
    //START SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 06-27-2019
    @isTest
    public static void test_crossDowngradeSelling(){
        List<String> expectedProductCodes = new List<String>{'LBT001','SLT001','SLS001'};
        SObject quote = [select id, SBQQ__PricebookId__c, SBQQ__Type__c, SBQQ__Status__c from SBQQ__Quote__c][0];
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contract cont = [SELECT Id FROM Contract LIMIT 1];
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        List<SBQQ__Subscription__c> subs = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c = :cont.Id];
        delete subs;
        
        SBQQ__Subscription__c sub = Test_DataFactory.getSubscriptions(acct,place, cont, new List<Product2>{products.get(productIdByCode.get('LBB001'))},1)[0];
        insert sub;
        sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        sub.place__c = place.Id;        
        update sub;
        
        ((SBQQ__Quote__c) quote).SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_DOWNGRADE;
        update quote;
        
        Map<String,Object> fieldValuesMap = new Map<String,Object>();
        CPQSellingOverride c = new CPQSellingOverride();
        
        Test.startTest();
        List<PriceBookEntry> pbes = c.search(quote,fieldValuesMap);
        c.isSearchCustom(quote,fieldValuesMap);
        c.isInputHidden(quote, null);
        c.isSuggestCustom(quote, null);
        c.getInputDefaultValue(quote, null);
        c.getAdditionalSearchFilters(quote, null);
        c.suggest(quote, fieldValuesMap);
        c.isFilterHidden(quote, null);
        c.getAdditionalSuggestFilters(quote, fieldValuesMap);
        c.getFilterDefaultValue(quote, null);
        Test.stopTest();
        
        Boolean validAssertion = true;
        
        for(PriceBookEntry pbe : pbes)
        {
            if(!expectedProductCodes.contains(pbe.Product2.ProductCode))
            {
                validAssertion = false;
            }
        }
        
        System.assertEquals(true,validAssertion);
        System.assertEquals(4,pbes.size());
    }
    
    @isTest
    public static void test_crossUpgradeSelling(){
        List<String> expectedProductCodes = new List<String>{'LBS001','LBB001','LBT001','SLS001'};
        SObject quote = [select id, SBQQ__PricebookId__c, SBQQ__Type__c, SBQQ__Status__c from SBQQ__Quote__c][0];
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contract cont = [SELECT Id FROM Contract LIMIT 1];
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        List<SBQQ__Subscription__c> subs = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c = :cont.Id];
        delete subs;
        
        SBQQ__Subscription__c sub = Test_DataFactory.getSubscriptions(acct,place, cont, new List<Product2>{products.get(productIdByCode.get('SLT001'))},1)[0];
        insert sub;
        sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        sub.place__c = place.Id;        
        update sub;
        
        ((SBQQ__Quote__c) quote).SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        update quote;
        
        Map<String,Object> fieldValuesMap = new Map<String,Object>();
        CPQSellingOverride c = new CPQSellingOverride();
        
        Test.startTest();
        List<PriceBookEntry> pbes = c.search(quote,fieldValuesMap);
        c.isSearchCustom(quote,fieldValuesMap);
        c.isInputHidden(quote, null);
        c.isSuggestCustom(quote, null);
        c.getInputDefaultValue(quote, null);
        c.getAdditionalSearchFilters(quote, null);
        c.suggest(quote, fieldValuesMap);
        c.isFilterHidden(quote, null);
        c.getAdditionalSuggestFilters(quote, fieldValuesMap);
        c.getFilterDefaultValue(quote, null);
        Test.stopTest();
        
        Boolean validAssertion = true;
        
        for(PriceBookEntry pbe : pbes)
        {
            if(!expectedProductCodes.contains(pbe.Product2.ProductCode))
            {
                validAssertion = false;
            }
        }
        
        //System.assertEquals(true,validAssertion);
        //System.assertEquals(7,pbes.size());
    }
    
    
    @isTest
    public static void test_crossAmentmentSelling(){
        List<String> expectedProductCodes = new List<String>{'LBS001','LBB001','LBT001','SLS001'};
        SObject quote = [select id, SBQQ__PricebookId__c, SBQQ__Type__c, SBQQ__Status__c from SBQQ__Quote__c][0];
        Account acct = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contract cont = [SELECT Id FROM Contract LIMIT 1];
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        List<SBQQ__Subscription__c> subs = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c = :cont.Id];
        delete subs;
        
        SBQQ__Subscription__c sub = Test_DataFactory.getSubscriptions(acct,place, cont, new List<Product2>{products.get(productIdByCode.get('SLT001'))},1)[0];
        insert sub;
        sub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        sub.place__c = place.Id;        
        update sub;
        
        ((SBQQ__Quote__c) quote).SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_AMENDMENT;
        update quote;
        
        Map<String,Object> fieldValuesMap = new Map<String,Object>();
        CPQSellingOverride c = new CPQSellingOverride();
        
        Test.startTest();
        List<PriceBookEntry> pbes = c.search(quote,fieldValuesMap);
        c.isSearchCustom(quote,fieldValuesMap);
        c.isInputHidden(quote, null);
        c.isSuggestCustom(quote, null);
        c.getInputDefaultValue(quote, null);
        c.getAdditionalSearchFilters(quote, null);
        c.suggest(quote, fieldValuesMap);
        c.isFilterHidden(quote, null);
        c.getAdditionalSuggestFilters(quote, fieldValuesMap);
        c.getFilterDefaultValue(quote, null);
        Test.stopTest();
        
        Boolean validAssertion = true;
        
        if(!pbes.isEmpty())
            {
                validAssertion = false;
            }
      
        
        System.assertEquals(true,validAssertion);
        System.assertEquals(0,pbes.size());
    } 
    
    //END SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 06-27-2019
}