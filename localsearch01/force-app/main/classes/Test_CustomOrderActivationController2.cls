@isTest
public class Test_CustomOrderActivationController2 {
	
    @testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow; 
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Product2 product = generateProduct_myCOCKPITBasic();
        insert product;
        
        Product2 myCOCKPITStandard_product = generateProduct_myCOCKPITStandard();
        insert myCOCKPITStandard_product;
        
        List<Account> acc = Test_DataFactory.generateAccounts('Account test name',1,false);
        insert acc;     
        List<Contact> contacts = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last name test', 1);
        insert contacts;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
    }
    @isTest
    public static void test_activateUpgradeTermOrder(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
		List<String> fieldNamesContact = new List<String>(  Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
        String queryContact = 'SELECT ' +String.join( fieldNamesContact, ',' ) +' FROM Contact LIMIT 1';
        Contact contact = Database.query(queryContact);
        
        //Create Opportunity with Quote + QuoteLine (Type = New)
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Opportunity Test Name', account.Id);
        insert opportunity;
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        List<String> fieldNamesProduct = new List<String>(  Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProduct = 'SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2 WHERE ProductCode = \'MCOBASIC001\' LIMIT 1';
        Product2 myCOCKPITBasic_product = Database.query(queryProduct);
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, myCOCKPITBasic_product.Id, Date.today() , null, null, null, 'Annual');
        insert quoteLine;
        
        //Create activation Order + Order Item in Fulfilled Status
        Order activationOrder = generateActivationOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        insert activationOrder;
        OrderItem activationOrderItem = Test_DataFactory.generateFatherOrderItem(activationOrder.Id, quoteLine);
        activationOrderItem.Quantity = 1;
        activationOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        insert activationOrderItem;
        
        //Create an Active contract to upgrade
        Contract contract = Test_DataFactory.generateContractFromOrder(activationOrder, false);
        insert contract;
        SBQQ__Subscription__c subscription = Test_DataFactory.generateFatherSubFromOI(contract, activationOrderItem);
        subscription.Subscription_Term__c = quoteLine.Subscription_Term__c;
        subscription.Termination_Date__c = Date.today();
        subscription.In_Termination__c = true;
        insert subscription;
        
        //Activate contract/order
        activationOrderItem.SBQQ__Subscription__c = subscription.Id;
        update activationOrderItem;
        
        activationOrder.Contract__c = contract.Id;
        activationOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        activationOrder.SBQQ__Contracted__c = true;
        update activationOrder;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        //Create Opportunity + Quote (Type = Upgrade)
        Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
        upgradeOpportunity.CloseDate = Date.today().addDays(1);
		upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
		upgradeOpportunity.Upgrade__c = true;     
		upgradeOpportunity.Downgrade__c = false;
		insert upgradeOpportunity;
        
        String upgradeOpportunityId = upgradeOpportunity.Id;
        String queryUpgradeQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :upgradeOpportunityId LIMIT 1';
        SBQQ__Quote__c upgradeQuote = Database.query(queryUpgradeQuote);
        
        String queryProduct_myCOCKPITStandard = 'SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2 WHERE ProductCode = \'MCOSTANDARD001\' LIMIT 1';
        Product2 myCOCKPITStandard_product = Database.query(queryProduct_myCOCKPITStandard);
        SBQQ__QuoteLine__c upgradeQuoteLine = Test_DataFactory.generateQuoteLine(upgradeQuote.Id, myCOCKPITStandard_product.Id, Date.today(), contact.Id, null, null, 'Annual');
        insert upgradeQuoteLine;
        
        //Create Upgrade - Activation Order
        Map<String,String> upgradeOrderMap = new Map<String,String>();
        upgradeOrderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        upgradeOrderMap.put('Type',ConstantsUtil.ORDER_TYPE_UPGRADE);
        upgradeOrderMap.put('EffectiveDate',String.valueOf(Date.today()));
        upgradeOrderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        upgradeOrderMap.put('AccountId',account.Id);
        upgradeOrderMap.put('SBQQ__Quote__c',upgradeQuote.Id);
        Order upgradeOrder = Test_DataFactory.generateOrder(upgradeOrderMap);
        upgradeOrder.OpportunityId = upgradeOpportunity.Id;
        insert upgradeOrder;
        
        OrderItem upgradeOrderItem = Test_DataFactory.generateFatherOrderItem(upgradeOrder.Id, upgradeQuoteLine);
        upgradeOrderItem.Subscription_To_Terminate__c = subscription.Id;
        insert upgradeOrderItem;
        
        Test.startTest();
		//Create Upgrade - Termination Order
		Order upgradeTerminationOrder = generateTerminationOrder(contract);
        upgradeTerminationOrder.Parent_Order__c = upgradeOrder.Id;
        insert upgradeTerminationOrder;
        
        OrderItem upgradeTerminationOrderItem = generateFatherOrderItem(subscription, upgradeTerminationOrder, quoteLine.SBQQ__PricebookEntryId__c);
        upgradeTerminationOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert upgradeTerminationOrderItem;
        
        CustomOrderActivationController.activateAllOrder(upgradeTerminationOrder.Id, Label.Confirm_for_Termination);
        Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    public static Product2 generateProduct_myCOCKPITBasic(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generateProduct_myCOCKPITStandard(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Standard');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCS');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','20');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOSTANDARD001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
   	public static Order generateActivationOrder(String accountId, String quoteId, String orderType, String customType){
        Map<String,String> fieldApiNameToValueOrder = new Map<String,String>();
        fieldApiNameToValueOrder.put('Custom_Type__c', customType);
        fieldApiNameToValueOrder.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValueOrder.put('EndDate', String.valueOf(Date.today().addDays(10)));
        fieldApiNameToValueOrder.put('AccountId', accountId);
        fieldApiNameToValueOrder.put('SBQQ__Quote__c', quoteId);
        fieldApiNameToValueOrder.put('Type', orderType);
        Order ord = Test_DataFactory.generateOrder(fieldApiNameToValueOrder);
        return ord;
    }
    public static Order generateTerminationOrder(Contract c){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        order.Type = ConstantsUtil.ORDER_TYPE_UPGRADE;
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_TERMINATION;
        if(c.TerminateDate__c == null){
            order.EffectiveDate = Date.today();
        }
        else{
            order.EffectiveDate = c.TerminateDate__c;
        }
		order.ContractId = c.id;
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today();
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('FATHER OI ---> '+JSON.serializePretty(fatherOrderItem));
        return fatherOrderItem; 
    }
}