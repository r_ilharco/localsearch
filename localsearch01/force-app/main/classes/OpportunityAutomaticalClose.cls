public class OpportunityAutomaticalClose {
    
    
    
    @InvocableMethod(label='closeOpportunity' description='Tries to Accept the Quote and Close Opportunity')
    public static void closeOpportunity(List<SBQQ__Quote__c> quotes){	
        
        if(!quotes.isEmpty()){
            List<Opportunity> opportunityList = new List<Opportunity>();
            List<SBQQ__Quote__c> allquotes = new List<SBQQ__Quote__c>();
            List<Opportunity> all_opp = new List<Opportunity>(); 
            CustomNotificationType customNotification = [SELECT Id, CustomNotifTypeName, DeveloperName from CustomNotificationType WHERE DeveloperName = :ConstantsUtil.AUTOMATIC_CLOSE_OPP_NOTIFICATION LIMIT 1];

            allquotes.add(SEL_Quote.getQuoteById(quotes[0].Id));
            system.debug('allquotes ::  '+allquotes);
            
            all_opp.add(SEL_Opportunity.getOpportunityById(quotes[0].SBQQ__Opportunity2__c));
            
            system.debug('all_opp ::  '+all_opp);
            for(SBQQ__Quote__c quote : allquotes){
                quote.SBQQ__Status__c=ConstantsUtil.Quote_StageName_Accepted;
            }
            
            List<Database.SaveResult> resultQuoteUpdate = Database.update(allquotes, false);
            
            System.debug('UPDATE - QUOTE');
            System.debug('Approved_By_Process__c : '+allquotes[0].Approved_By_Process__c);
            for (Database.SaveResult sr_quote : resultQuoteUpdate) {
                if (sr_quote.isSuccess()) {
                    System.debug('Successfully Updated QUOTE: ' + sr_quote.getId());
                    for(Opportunity opp : all_opp){
                        if(opp.SBQQ__PrimaryQuote__c == sr_quote.getId()){
                            opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
                            opportunityList.add(opp);
                        }
                    }
                    if(!opportunityList.isEmpty()){
                        List<Database.SaveResult> resultOpportunityUpdate = Database.update(opportunityList, false);
                        if(allquotes[0].Approved_By_Process__c == 'ApprovalProcessForSubstitution2'){
                          for (Database.SaveResult sr_opportunity : resultOpportunityUpdate) {
                            if (sr_opportunity.isSuccess()) {
                               System.debug('Successfully Updated OPP: ' + sr_opportunity.getId());
                                List<String> fillers = new List<String>{allquotes[0].Name,allquotes[0].SBQQ__Opportunity2__r.Name};
                                sendNotifications(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Success, fillers), allquotes[0].Id, customNotification.Id , new set<String>{allquotes[0].SBQQ__SalesRep__c});

                            }else{
                                System.debug('NOT Successfully Updated OPP');
                                List<String> fillers = new List<String>{allquotes[0].Name};
                                sendNotifications(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Error, fillers), allquotes[0].Id, customNotification.Id , new set<String>{allquotes[0].SBQQ__SalesRep__c});
                                sendEmail(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Error, fillers),allquotes[0]);
                            }
                        }  
                        }
                        
                    }
                }else{ 
                    if(allquotes[0].Approved_By_Process__c == 'ApprovalProcessForSubstitution2'){
                        System.debug('NOT Successfully Updated QUOTE');
                        List<String> fillers = new List<String>{allquotes[0].Name};
                        sendNotifications(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Missing_Steps, fillers), allquotes[0].Id, customNotification.Id , new set<String>{allquotes[0].SBQQ__SalesRep__c});
                   		sendEmail(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Missing_Steps, fillers),allquotes[0]);
                    }
                }
            }
        
        
        
        }
        
    }
    
  
    public static void sendEmail(String subject,String strBody,SBQQ__Quote__c quote) {
	
    
   		if(strBody.contains(quote.Name)) strBody = strBody.replace(quote.Name, '<br><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+quote.Id+'">'+quote.Name+'</a>');
        
        
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        	system.debug('quote.SBQQ__SalesRep__r.Email :: '+quote.SBQQ__SalesRep__r.Email);
        	message.toAddresses = new String[] { quote.SBQQ__SalesRep__r.Email };
            message.subject = subject;        
        	message.plainTextBody = strBody;
			message.setHtmlBody(strBody); 
        	Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages); 
        
        if (results[0].success){
            System.debug('The email was sent successfully.');
        }else{
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
    
    
    public static void sendNotifications(String strTitle,String strBody, String strTargetId, String strNotificationId, set<String> setUserIds) {
       
   	Messaging.CustomNotification obj = new Messaging.CustomNotification();
        obj.setNotificationTypeId(strNotificationId);
        obj.setTargetId(strTargetId);
        obj.setTitle(strTitle);
        obj.setBody(strBody);
    obj.send(setUserIds);
    }
}