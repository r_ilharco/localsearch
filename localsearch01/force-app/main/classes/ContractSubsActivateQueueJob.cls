global without sharing class ContractSubsActivateQueueJob implements Queueable {

	private Set<ID> vsetContract;
   
    public ContractSubsActivateQueueJob(Set<ID> setContract) {
        vsetContract= setContract;       
    }    
  global void execute(QueueableContext SC) {
	 ContractUtility.ActiveSubscripJob(vsetContract);
  }
}