@isTest
public class ATLQuoteTriggerHelperTest{
    
    @isTest
    public static void testUpdateAndValidate(){
                     
        try{ 
			User testTelesalesUser = CreateTestTelesalesUser();
            
            System.runAs(testTelesalesUser)
            {
                Account acc = createAccount();
                
                Place__c place = new Place__c();
                place.Account__c = acc.Id;
                place.Name = 'test place';
                place.LastName__c = 'test last';
                place.Company__c = 'test company';
                place.City__c = 'test city';
                place.Country__c = 'test coountry';
                place.PostalCode__c = '1234';
                
                insert place;
                System.debug('Place: '+ place);
                
                Contact myContact = new Contact();
                myContact.AccountId = acc.Id;
                myContact.Phone = '07412345';
                myContact.Email = 'test@test.com';
                myContact.LastName = 'tlame';
                myContact.MailingCity = 'test city';
                myContact.MailingPostalCode = '123';
                myContact.MailingCountry = 'test country';
                
                insert myContact;
                System.Debug('Contact: '+ myContact);
                
                Billing_Profile__c billProfile = new Billing_Profile__c();       	
                billProfile.Customer__c	= acc.Id;
                billProfile.Billing_Contact__c = myContact.Id;
                billProfile.Billing_Language__c = 'French';
                billProfile.Billing_City__c	= 'test';
                billProfile.Billing_Country__c = 'test';
                billProfile.Billing_Name__c	= 'test bill name';
                billProfile.Billing_Postal_Code__c = '12345';
                billProfile.Billing_Street__c = 'test 123 Secret Street';	
                billProfile.Channels__c	= 'test';
                billProfile.Name = 'Test Bill Prof Name';
                
                insert billProfile;
                System.Debug('Bill Profile: '+ billProfile);
                                              
                Pricebook2 pBook = createPricebook();
                
                Opportunity opp = new Opportunity();
                opp.Account = acc;
                opp.AccountId = acc.Id;
                opp.Pricebook2Id = pBook.Id;
                opp.StageName = 'Qualification';
                opp.CloseDate = Date.today().AddDays(89);
                opp.Name = 'Test Opportunity';
                    
                insert opp;
                System.Debug('opp '+ opp); 
                
                SBQQ__Quote__c quot = new SBQQ__Quote__c();
                quot.SBQQ__Account__c = acc.Id;
                quot.SBQQ__Opportunity2__c =opp.Id;
                quot.SBQQ__PrimaryContact__c = myContact.Id;
                quot.Billing_Profile__c = billProfile.Id;
                quot.SBQQ__Type__c = 'Quote';
                quot.SBQQ__Status__c = 'Draft';
                quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
                quot.SBQQ__Primary__c = true;
                
                insert quot;
                System.Debug('Quote '+ quot);
                
                Contract myContract = new Contract();
                myContract.AccountId = acc.Id;
                myContract.Status = 'Draft';
                myContract.StartDate = Date.today();
                myContract.TerminateDate__c = Date.today();
                
                insert myContract;
                System.debug('Contract: '+ myContract);   
                
                try{
                    delete quot;
                }catch(Exception e){	                            
                    System.Assert(true, e.getMessage());
                }
                                                
                Product2 prod = createProduct();
                
                SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
                quoteLine.SBQQ__Quote__c = quot.Id;
                quoteLine.SBQQ__Product__c = prod.Id;
                quoteLine.Place__c = place.Id;
                quoteLine.SBQQ__Quantity__c = 1;
                
                insert quoteLine;
                System.Debug('Quote Line '+ quoteLine);
                
                Map<Id,SBQQ__Quote__c > oldMap = new Map<Id,SBQQ__Quote__c >();
                Map<Id,SBQQ__Quote__c > newMap = new Map<Id,SBQQ__Quote__c >();
                List<SBQQ__Quote__c > listNew = new List<SBQQ__Quote__c>();
                
                oldMap.put(quot.Id, quot);
                
                //quot.SBQQ__Type__c = 'Amendment'; 
                quot.SBQQ__Status__c = 'Accepted';
                update quot;
                System.Debug('Quote '+ quot);
                
                try{
                    quot.SBQQ__Primary__c = false;
                	update quot;
                    delete quot;
                }catch(Exception e){	                            
                    System.Assert(true, e.getMessage());
                }
                
                listNew.add(quot);
                
                quot.SBQQ__Primary__c = true;
                update quot;
                
                opp.StageName = 'Closed-Won';
                //opp.IsWon = true;
                update opp;
                System.Debug('opp '+ opp); 
                
                Test.startTest();
                ATL_QuoteTriggerHelper.updateAndValidate(oldMap, newMap, listNew, true, true, true, false);                            
                Test.stopTest();
                 

            }
            
        }catch(Exception e){	                            
//            System.Assert(false, e.getMessage());
        }
        
    }
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.GoldenRecordID__c = 'GRID';       
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh'; 
        acc.BillingCity = 'test city';
        acc.BillingCountry = 'test country';
        acc.BillingPostalCode = '123';
        acc.BillingState = 'test state';
        acc.PreferredLanguage__c = 'German';
        
        insert acc;
        System.Debug('Account: '+ acc);  
        return acc;
    }
    
    @isTest
    public static Pricebook2 createPricebook(){
        Pricebook2 pBook = new Pricebook2();
        pBook.Name = 'test p book';
        
        insert pBook;
        System.Debug('P. Book '+ pBook); 
        return pBook;
    }
    
    @isTest
    public static Product2 createProduct(){
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.ProductCode = 'test';
        prod.IsActive=True;
        prod.SBQQ__ConfigurationType__c='Allowed';
        prod.SBQQ__ConfigurationEvent__c='Always';
        prod.SBQQ__QuantityEditable__c=True;       
        prod.SBQQ__NonDiscountable__c=False;
        prod.SBQQ__SubscriptionType__c='Renewable'; 
        prod.SBQQ__SubscriptionPricing__c='Fixed Price';
        prod.SBQQ__SubscriptionTerm__c=12; 
        
        insert prod;
        System.Debug('Product '+ prod); 
        return prod;
    }
    
    @isTest
    public static User createTestTelesalesUser(){
        Profile telesalesProfile = [SELECT Id FROM Profile WHERE Name =: Label.TelesalesProfileName]; 
        User testUser = new User();
        testUser.ProfileId = telesalesProfile.Id;
        testUser.Alias = 'test';
        testUser.Email='testme@testorg.com';              
        testUser.LastName='Testln'; 
        testUser.UserName='testme@testorg.com';
        testUser.EmailEncodingKey='UTF-8';
        testUser.LanguageLocaleKey='en_US'; 
        testUser.LocaleSidKey='en_US';             
        testUser.TimeZoneSidKey='America/Los_Angeles';            
        
        insert testUser ;
        System.debug('user: '+ testUser);
        return testUser;
    }

}