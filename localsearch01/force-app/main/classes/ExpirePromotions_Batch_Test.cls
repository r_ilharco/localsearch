@IsTest public class ExpirePromotions_Batch_Test {
    
    Public Static void testSetupBeforeEachTest() {
        insert new GlobalConfiguration_cs__c(ExpirePromotionNrOfDaysBeforeEnd__c = 1);
        //        
        List<Product2> pr = new List<Product2>{ Test_DataFactory.createProduct2() };
        insert pr;
        //
        Campaign ca = Test_DataFactory.createCampaign();
        insert ca;
        //
        // loose coupled
        Date startedAt = Date.today().addMonths(-2); // must be more than one month in the past
        Promotions__c pm = Test_DataFactory.createPromotion(pr[0],ca,startedAt,30);
        insert pm;
        //
        Account ac = Test_DataFactory.createAccounts('AccountTest', 1)[0];
        insert ac;
        //
        Place__c pl = Test_DataFactory.createPlaces('TestPlaceName',1)[0]; pl.Account__c = ac.id;
        insert pl;
        //
        Opportunity op = Test_DataFactory.createOpportunities('Opp Test','Draft',ac.Id,1)[0];
        op.CampaignId = ca.Id;
        // for future use: op.OwnerId = Userinfo.getuserid();
        // for future use: op.Pricebook2Id = Test.getStandardPricebookId();
        insert op;
        //
        SBQQ__Quote__c qu = Test_DataFactory.createQuotes('Draft', op, 1)[0];
        qu.SBQQ__Account__c = ac.Id;
        qu.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_QUOTE;
        qu.SBQQ__Primary__c = true;
        // note: qu.Campaign__c = Formula: SBQQ__Opportunity2__c.CampaignId
        // for future use: qu.SBQQ__PrimaryContact__c = myContact.Id;
        // for future use: qu.Billing_Profile__c = billProfile.Id;
        // for future use: qu.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
        insert qu;
        //
        List<SBQQ__QuoteLine__c> ql = Test_DataFactory.createQuoteLinesWithPlace(qu, pl, pr);
        ql[0].SBQQ__Discount__c = 15;
        ql[0].Campaign_Id__c = ca.Id;
        insert ql;
        //
        Contract co = Test_DataFactory.getContracts(ac, ConstantsUtil.CONTRACT_STATUS_DRAFT, 1)[0];
        co.StartDate = startedAt;
        co.In_Termination__c = false;
        co.SBQQ__Evergreen__c = true;
        co.EndDate = Date.today().addMonths(12);
        co.SBQQ__Quote__c = qu.Id;
        co.SBQQ__Opportunity__c = op.Id;
        // for future use: co.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        // for future use: co.nextRenewalDate__c = Date.today().addDays(1); # take one in future
        insert co;
        //
        {
            Integer nr = 4;
            List<SBQQ__Subscription__c> su = Test_DataFactory.getSubscriptions(ac,pl,co,pr,nr);
            for(Integer i = 0; i <nr; i++){
                Date d; if( i < nr/2 ){ d = co.StartDate; }else{ d = Date.today().addDays(1); } // some are in future which should not expired
                su[i].Subsctiption_Status__c = 'Active';
                su[i].SBQQ__Discount__c = 10; // must be greater than 0
                su[i].SBQQ__SubscriptionStartDate__c = d;
            }
            insert su;
        }
        System.assertEquals(4,[SELECT COUNT() FROM SBQQ__Subscription__c],'Nr of subscriptions created.');
        System.assertEquals(1,[SELECT Id, Product__c, Campaign__c, Days__c FROM Promotions__c ORDER BY LastModifiedDate DESC LIMIT 50000].size(),'NrOfPromotions');
        System.assertEquals(2,
            [SELECT Id, SBQQ__Product__c, SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c, SBQQ__Discount__c, SBQQ__SubscriptionStartDate__c 
                FROM SBQQ__Subscription__c 
                WHERE Subsctiption_Status__c = 'Active'
                  AND SBQQ__SubscriptionStartDate__c <= TODAY
                  AND SBQQ__Discount__c != null AND SBQQ__Discount__c > 0 
                  AND SBQQ__Product__c != null
                  AND SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c != null  // takes SBQQ__Opportunity2__c.CampaignId
                  ].size(),'NrOfSubscrHavingDiscount');
        Integer nfOfDaysBeforeEnd = (Integer)GlobalConfiguration_cs__c.getOrgDefaults().ExpirePromotionNrOfDaysBeforeEnd__c;
        System.assertEquals(1,nfOfDaysBeforeEnd,'nfOfDaysBeforeEnd');
        System.debug('ExpirePromotions_Batch_Test.testSetupBeforeEachTest: Created Product, Campaign, Promotions, Account, Place, Opp, Quote, Contract and Subscriptions.'); 
    }
    
    static testmethod void testBatch() {
        testSetupBeforeEachTest();
        Test.startTest();
        ExpirePromotions_Batch p = new ExpirePromotions_Batch();
        Id batchId = Database.executeBatch(p);
        Test.stopTest();
        System.assertEquals(4,[SELECT COUNT() FROM SBQQ__Subscription__c],'Nr of subscriptions created.');
        System.assertEquals(2,[SELECT COUNT() FROM SBQQ__Subscription__c WHERE Technical_Info__c != null],'Nr of subscriptions changed.');
    }
    
    static testmethod void testSchedule() {
        testSetupBeforeEachTest();
        Test.startTest();
        String CronExpDummyEvenRunAtStopTest = '0 0 23 * * ? *';
        Integer nrOfJobsBeforeCallSchedule = [SELECT COUNT() FROM AsyncApexJob];
        ExpirePromotions_Batch p = new ExpirePromotions_Batch();
        String jobId = System.schedule('ExpirePromotions_Batch_Test', CronExpDummyEvenRunAtStopTest, p);
        Test.stopTest();
        Integer nrOfJobsAfterCallSchedule = [SELECT COUNT() FROM AsyncApexJob];
        Integer nrOfJobsDiff = nrOfJobsAfterCallSchedule - nrOfJobsBeforeCallSchedule;
        System.debug('ExpirePromotions_Batch_Test.testSchedule done. Note: processing of batch is still async ongoing and we cannot wait for end of it. '
                    +'nrOfJobsBeforeCallSchedule=' + nrOfJobsBeforeCallSchedule + ' nrOfJobsAfterCallSchedule=' + nrOfJobsAfterCallSchedule); 
        System.assertEquals(4,[SELECT COUNT() FROM SBQQ__Subscription__c],'Nr of subscriptions created.');
        System.assert(nrOfJobsDiff >= 2,'Difference=' + nrOfJobsDiff + ' of nrOfJobsAfterCallSchedule minus nrOfJobsBeforeCallSchedule expected >= 2.');
    }
}