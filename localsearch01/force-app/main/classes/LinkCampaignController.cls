public class LinkCampaignController {
    
    private static CurrentUserInfo currentUserInfo = new CurrentUserInfo();
    
    
    @AuraEnabled
    public static response getActiveCampaingsByType(Id campaingId, string recordId){
        response res = new response();
        res.isEmpty = true;
        res.readOnly = false;
        res.isUserAllowed = true;
        res.errorMessages ='';
        List<Campaign> campaignsFinalList = new List<Campaign>();
        List<Campaign> campaigns = new List<Campaign>();
        string query = 'select id, Name,StartDate, EndDate, Sales_Channel__c, Type, Status from Campaign ';
        string whereClause = '';
        system.debug('currentUserInfo ::'+currentUserInfo);
        
        List<SBQQ__Quote__c> lstQuotes = [Select Id,SBQQ__Primary__c,SBQQ__Type__c from SBQQ__Quote__c where SBQQ__Opportunity2__c = :recordId and SBQQ__Primary__c = true and SBQQ__Type__c = :ConstantsUtil.QUOTE_TYPE_QUOTE];
        Boolean isUserAllowed = currentUserInfo.dmcUser || currentUserInfo.adminUser || currentUserInfo.digitalAgencyUser || currentUserInfo.teleSalesUser || currentUserInfo.pdfUser ||currentUserInfo.salesUser;
        if(!isUserAllowed){
            res.isUserAllowed = false;
            res.errorMessages = Label.Namirial_UI_ErrorMessage_NoSufficientPermissions;
        }else if(lstQuotes.isEmpty()){
            res.isUserAllowed = false;
            res.errorMessages = Label.CampLink_QuoteTypeVal;
        }else{
            if(!currentUserInfo.userType.isEmpty()){
                string srtQueryfilter;
                string innerstr = '';
                for(string s :currentUserInfo.userType){
                    innerstr += '\''+s+'\',';
                }
                if(innerstr.endsWith(',')) innerstr = innerstr.removeEnd(',');
                
                system.debug('innerstr ::: '+innerstr);
                whereClause = ' Where IsActive = True And Sales_Channel__c includes ('+innerstr+')';
                
            }
            
            campaigns = database.query(query+whereClause);
            system.debug('campaigns ::'+campaigns.size());
            
            if(campaingId != null){
                List<Campaign> selectedCamp = [Select Id, Name,StartDate, EndDate, Sales_Channel__c, Type, Status from Campaign where Id =:campaingId Limit 1];
                if(!selectedCamp.isEmpty() /*&& !campaigns.isEmpty()*/){
                    system.debug('campaigns.contains(selectedCamp[0]) :::'+campaigns.contains(selectedCamp[0]));
                    if(!campaigns.contains(selectedCamp[0])){
                        if(campaigns.isEmpty()){
                            res.readOnly = true;
                        }
                        campaigns.add(selectedCamp[0]);
                        
                    }
                }
            }
            
            if(!campaigns.isEmpty()){
                res.isEmpty = false;
                system.debug('campaigns ::'+campaigns); 
                res.campaignList = campaigns;
            }
            
        }
        
        return res;
    }
    
    
    @AuraEnabled
    public static response removeQLPromotions(string recordId){
        response res = new response();
        res.success = true;
        boolean updateSuccess = true;
        if(!string.isBlank(recordId)){
            try{
             List<SBQQ__QuoteLine__c> qlItems = new List<SBQQ__QuoteLine__c>();   
            List<SBQQ__Quote__c>   primaryQuotes = [Select Id,Promotion_Applied__c,IsProcessPromotionActivated__c, 
                                                    	(Select Id,Promotion_Applied__c, Promotion_Type__c, SBQQ__Discount__c,Total_Discount_Amt_Promotions__c from SBQQ__LineItems__r 
                                            				where Promotion_Applied__c = True)  
                                                    from SBQQ__Quote__c where Id = :recordId Limit 1 ]; 
                
                
                if(!primaryQuotes.isEmpty()){
                    for(SBQQ__Quote__c quote : primaryQuotes ){
                        if(quote.IsProcessPromotionActivated__c){
                            res.success = false;
							res.errorMessages = Label.Generic_Error;
                            return res;
                        }else if(!quote.SBQQ__LineItems__r.isEmpty()){
							quote.Promotion_Applied__c = false;
                             for(SBQQ__QuoteLine__c ql : quote.SBQQ__LineItems__r){
                    			ql.Promotion_Applied__c = false;
                    			ql.Promotion_Type__c = null;
                    			ql.SBQQ__Discount__c = null;
                    			if(ql.Total_Discount_Amt_Promotions__c>0) ql.Total_Discount_Amt_Promotions__c = 0;
                            	qlItems.add(ql);
                			}
                            
                        }
                       
                       
                    } 
                   
                    if(!qlItems.isEmpty()){
                        QuoteLineTriggerHandler.disableTrigger = true;
                        QuoteTriggerHandler.disableTrigger = true;
                		Database.SaveResult[] srList = Database.update(qlItems, true);                        
                        update primaryQuotes;
                        QuoteTriggerHandler.disableTrigger = false;
                		QuoteLineTriggerHandler.disableTrigger = false;    
                    }
                    
                    
                    
                }     
                
 
            }catch(Exception ex){
                system.debug('ex.Message :: '+ex.getMessage());
                res.success = false;
                //res.errorMessages = Label.Generic_Error;
                res.errorMessages = ex.getMessage();
            }
            
        }
        system.debug('res :: '+res);
    	return res;
    }
    
    
    public class response{ 
        @AuraEnabled public Boolean success;
        @AuraEnabled public Boolean isUserAllowed; 
        @AuraEnabled public Boolean isEmpty;
        @AuraEnabled public Boolean readOnly;
        @AuraEnabled public string errorMessages;
        @AuraEnabled public List<Campaign> campaignList;
    }   
    
    public class CurrentUserInfo{
        
        @AuraEnabled public Boolean dmcUser;
        @AuraEnabled public Boolean teleSalesUser;
        @AuraEnabled public Boolean salesUser;
        @AuraEnabled public Boolean adminUser;
        @AuraEnabled public Boolean digitalAgencyUser;
        @AuraEnabled public Boolean pdfUser;
        @AuraEnabled public List<string> userType;
        
        CurrentUserInfo(){
            User currentUser = [SELECT Id,Code__c, Name, Profile.Name, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Label IN (:ConstantsUtil.DMCPERMISSIONSET_LABEL)) FROM User WHERE Id = :UserInfo.getUserId()];
            List<String> userType= new List<string>();
            this.dmcUser = false;
            this.teleSalesUser = false;
        	this.salesUser = false;
        	this.adminUser = false;
            this.digitalAgencyUser = false;
             this.pdfUser = false;
            if(ConstantsUtil.TELESALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
                system.debug('1');
                this.teleSalesUser = true;
                userType.add('Telesales'); // to be changed
            }else if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) || ConstantsUtil.SYSADMINCODE.equalsIgnoreCase(currentUser.Code__c)){
                userType.add('DMC');
                userType.add('Telesales'); 
                userType.add('Digital Agency');
                userType.add(' ');
                this.adminUser = true;
            }else if(ConstantsUtil.SALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
                system.debug('2');
                this.salesUser = true;
                userType.add('DMC');// to be changed
            }else if(ConstantsUtil.DIGITALAGENCYPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                system.debug('3');
                this.digitalAgencyUser = true;
                userType.add('Digital Agency');// to be changed
            }else if(checkPublicGroupMembership(UserInfo.getUserId())){
                system.debug('4');
                this.pdfUser = true;
                userType.add('DMC');
                userType.add('Telesales'); 
                userType.add('Digital Agency');
            }
            
            if(currentUser.PermissionSetAssignments.size() > 0){
                this.dmcUser = true;
                userType.add('DMC');// to be changed
            }
            this.userType = userType;
        }
    }
    
    
    
    
    public static boolean checkPublicGroupMembership(Id userId){
        system.debug('userId :: '+userId);
        boolean pdfGroupMember = false;
        
        
        List<GroupMember> groupMembers = new List<GroupMember>();
        Map<Id,User>  userMembers = new Map<Id,User> ();
        List<Group> groupRoleMembers = new List<Group>();
        Set<Id> userIds= new Set<Id>();
        

        userMembers = new Map<Id, User>([select Id from User where Id in (select UserOrGroupId from GroupMember 
                                                                          WHERE Group.DeveloperName=:ConstantsUtil.PDF_FACTORY) 
                                         and isActive = true]);
        
        groupRoleMembers = [select id, type, RelatedId from Group 
                            where Id in (select UserOrGroupId 
                                         from GroupMember where Group.DeveloperName=:ConstantsUtil.PDF_FACTORY) 
                            and type in ('Role','RoleAndSubordinatesInternal')];
        
        Set<Id> roleIds = new Set<Id>();
        Set<Id> roleandSubId = new Set<Id>();
        for(Group gr : groupRoleMembers){
            roleIds.add(gr.RelatedId); 
            if(gr.type == 'RoleAndSubordinatesInternal'){
                roleandSubId.add(gr.RelatedId); 
            }
        }
        
        if(!userMembers.isEmpty()){
            userIds.addAll(userMembers.keySet());
        }
        if(!roleandSubId.isEmpty()){
            userIds.addAll(RoleUtility.getRoleSubordinateUsers(roleandSubId));
        }
        if(!roleIds.isEmpty()){
            userIds.addAll(RoleUtility.getRoleUsers(roleIds));
        }
        
        
        System.debug('userIds: '+userIds);
        System.debug('userMembers: '+userMembers);
        System.debug('roleIds: '+roleIds);
        System.debug('roleandSubId: '+roleandSubId);
        
        if(userIds.contains(userId)) pdfGroupMember= true;
        system.debug('pdfGroupMember' + pdfGroupMember);    
        return pdfGroupMember;    
    }
    
}