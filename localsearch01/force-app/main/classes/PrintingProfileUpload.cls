public class PrintingProfileUpload {
    
    public static List<Printing_Profile__c> printingProfileInsert () {
        
        List<Printing_Profile__c> printProfList = new List<Printing_Profile__c>();
        final string pp1Name = 'Printing Profile FAK_DE_MIG';
        final string pp2Name = 'Printing Profile FAK_IT_MIG';
        final string pp3Name = 'Printing Profile FAK_FR_MIG';
        final string pp4Name = 'Printing Profile GUT_DE_MIG';
        final string pp5Name = 'Printing Profile GUT_IT_MIG';
        final string pp6Name = 'Printing Profile GUT_FR_MIG';
        final string pp7Name = 'Printing Profile FAK_DE_STD';
        final string pp8Name = 'Printing Profile FAK_IT_STD';
        final string pp9Name = 'Printing Profile FAK_FR_STD';
        final string pp10Name = 'Printing Profile GUT_DE_STD';
        final string pp11Name = 'Printing Profile GUT_IT_STD';
        final string pp12Name = 'Printing Profile GUT_FR_STD';
        
       
        

        Printing_Profile__c pp1 = new Printing_Profile__C (
        	Name = pp1Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Mit SWISS LIST ändern sich die <b>Rechnungsmodalitäten</b>. Die Rechnung für eine <b>Vertragsdauer</b> von <b>12 Monaten</b> wird Ihnen ab Vertragsbeginn zugestellt. Den von uns im Jahr 2019 in Rechnung gestellten Betrag für Ihr durch SWISS LIST abgelöstes Eintragsprodukt haben wir Ihnen <b>anteilsmässig als Gutschrift</b> angerechnet. Falls Sie die Rechnung bis anhin mit der Swisscom-Rechnung erhalten haben, erhalten Sie ab jetzt die <b>Rechnung direkt von localsearch</b>.</p>',
            Language__c = 'German',
            Sender__c = 'Swisscom Directories AG, Förrlibuckstrasse 62, Postfach, 8021 Zürich 1 UID: CHE-102.951.148 MWST',
            Title__c = 'Rechnung',
            Web_Site__c = 'swisslist.ch',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
       // Database.insert(pp1);
        printProfList.add(pp1);
        
        Printing_Profile__c pp2 = new Printing_Profile__C (
        	Name = pp2Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Totale</p>',
            Credit_Note_Grouping_Footer__c = '<p>Totale</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Con SWISS LIST cambiano le modalità di fatturazione.  La fattura per un contratto della durata di 12 mesi sarà emessa a partire dall’inizio del contratto. Per quanto riguarda l’importo da noi fatturato nel 2019 per il prodotto di registrazione sostituito da SWISS LIST, nella fattura troverà un accredito proporzionale. Inoltre, se fino ad adesso riceveva la fattura insieme a quella di Swisscom, da oggi riceverà la fattura direttamente da localsearch.</p>',
            Language__c = 'Italian',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, casella Postale, 8021 Zurigo 1 IDI: CHE-102.951.148 MWST',
            Title__c = 'Fattura',
            Web_Site__c = 'swisslist.ch',            
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
       // Database.insert(pp2);
        printProfList.add(pp2);
        
        Printing_Profile__c pp3 = new Printing_Profile__C (
        	Name = pp3Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Les <b>modalités de facturation</b> changent avec SWISS LIST. Pour une <b>durée de contrat</b> de <b>12 mois</b>, la facture vous sera délivrée au début du contrat. </p>',
            Language__c = 'French',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, CP, 8021 Zurich 1 UID: CHE-102.951.148 TVA',
            Title__c = 'Facture',
            Web_Site__c = 'swisslist.ch',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
       // Database.insert(pp3);
        printProfList.add(pp3);
        
        Printing_Profile__c pp4 = new Printing_Profile__C (
        	Name = pp4Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Mit SWISS LIST ändern sich die <b>Rechnungsmodalitäten</b>. Die Rechnung für eine <b>Vertragsdauer</b> von <b>12 Monaten</b> wird Ihnen ab Vertragsbeginn zugestellt. Den von uns im Jahr 2019 in Rechnung gestellten Betrag für Ihr durch SWISS LIST abgelöstes Eintragsprodukt haben wir Ihnen <b>anteilsmässig als Gutschrift</b> angerechnet. Falls Sie die Rechnung bis anhin mit der Swisscom-Rechnung erhalten haben, erhalten Sie ab jetzt die <b>Rechnung direkt von localsearch</b>.</p>',
            Language__c = 'German',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, casella Postale, 8021 Zurigo 1 IDI: CHE-102.951.148 MWST',
            Title__c = 'Rechnung',
            Web_Site__c = 'swisslist.ch',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp4);
        printProfList.add(pp4);
        
        
        Printing_Profile__c pp5 = new Printing_Profile__C (
        	Name = pp5Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Totale</p>',
            Credit_Note_Grouping_Footer__c = '<p>Totale</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Con SWISS LIST cambiano le <b>modalità di fatturazione</b>.  La fattura per un <b>contratto</b> della durata di <b>12 mesi</b> sarà emessa a partire dall’inizio del contratto. Per quanto riguarda l’importo da noi fatturato nel 2019 per il prodotto di registrazione sostituito da SWISS LIST, nella fattura troverà un <b>accredito proporzionale</b>. Inoltre, se fino ad adesso riceveva la fattura insieme a quella di Swisscom, da oggi riceverà la <b>fattura direttamente da localsearch</b>.</p>',
            Language__c = 'Italian',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, casella Postale, 8021 Zurigo 1 IDI: CHE-102.951.148 MWST',
            Title__c = 'Fattura',
            Web_Site__c = 'swisslist.ch',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp5);
        printProfList.add(pp5);
        
        
        Printing_Profile__c pp6 = new Printing_Profile__C (
        	Name = pp6Name,
            Process_Mode__c = 'ModeSwissList',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'swisslist@localsearch.ch',
            Contact_Phone__c = '0800 888 810',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '<p>Les <b>modalités de facturation</b> changent avec SWISS LIST. Pour une <b>durée de contrat</b> de <b>12 mois</b>, la facture vous sera délivrée au début du contrat. Nous avons appliqué le montant que nous facturons pour l’année 2019 pour l’inscription remplacée par SWISS LIST <b>sous forme de crédit proportionnel</b>. Si vous avez reçu jusqu’à présent la facture avec celle de Swisscom, vous recevrez désormais <b>la facture directement de la part de localsearch</b>.</p>',
            Language__c = 'French',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, CP, 8021 Zurich 1 UID: CHE-102.951.148 TVA',
            Title__c = 'Facture',
            Web_Site__c = 'swisslist.ch',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp6);
        printProfList.add(pp6);
        
        Printing_Profile__c pp7 = new Printing_Profile__C (
        	Name = pp7Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'German',
            Sender__c = 'Swisscom Directories AG, Förrlibuckstrasse 62, Postfach, 8021 Zürich 1 UID: CHE-102.951.148 MWST',
            Title__c = 'Rechnung',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp7);
        printProfList.add(pp7);
        
        Printing_Profile__c pp8 = new Printing_Profile__C (
        	Name = pp8Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Totale</p>',
            Credit_Note_Grouping_Footer__c = '<p>Totale</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'Italian',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, casella Postale, 8021 Zurigo 1 IDI: CHE-102.951.148 MWST',
            Title__c = 'Fattura',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp8);
        printProfList.add(pp8);
        
        
        Printing_Profile__c pp9 = new Printing_Profile__C (
        	Name = pp9Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'FAK',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'French',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, CP, 8021 Zurich 1 UID: CHE-102.951.148 TVA',
            Title__c = 'Facture',
            To_Date__c = Date.newInstance(2019, 12, 31)         
        );
        //Database.insert(pp9);
        printProfList.add(pp9);
        
        Printing_Profile__c pp10 = new Printing_Profile__C (
        	Name = pp10Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'German',
            Sender__c = 'Swisscom Directories AG, Förrlibuckstrasse 62, Postfach, 8021 Zürich 1 UID: CHE-102.951.148 MWST',
            Title__c = 'Rechnung',
            To_Date__c = Date.newInstance(2019, 12, 31)         
        );
        //Database.insert(pp10);
        printProfList.add(pp10);
        
        
        Printing_Profile__c pp11 = new Printing_Profile__C (
        	Name = pp11Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Totale</p>',
            Credit_Note_Grouping_Footer__c = '<p>Totale</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'Italian',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, casella Postale, 8021 Zurigo 1 IDI: CHE-102.951.148 MWST',
            Title__c = 'Fattura',
            To_Date__c = Date.newInstance(2019, 12, 31)          
        );
        //Database.insert(pp11);
        printProfList.add(pp11);
        
        Printing_Profile__c pp12 = new Printing_Profile__C (
        	Name = pp12Name,
            Process_Mode__c = 'Standard0',
            Bill_Type__c = 'PaymentSlip',
            Contact_Mail__c = 'customercare@localsearch.ch',
            Contact_Phone__c = '0800 86 80 86',
            Contract_Grouping_Footer__c = '<p>Total</p>',
            Credit_Note_Grouping_Footer__c = '<p>Total</p>',
            Doc_Type__c = 'GUT',
            From_Date__c= Date.newInstance(2019, 01, 01),
            Header__c = '',
            Language__c = 'French',
            Sender__c = 'Swisscom Directories SA, Förrlibuckstrasse 62, CP, 8021 Zurich 1 UID: CHE-102.951.148 TVA',
            Title__c = 'Facture',
            To_Date__c = Date.newInstance(2019, 12, 31)         
        );
        //Database.insert(pp12);
        printProfList.add(pp12);
        Database.insert(printProfList);
        PrintingProfileUpload.printingProfileFooterInsert(pp1, pp2, pp3, pp4, pp5, pp6, pp7, pp8, pp9, pp10, pp11, pp12);
        return printProfList;
        
    }
        public static List<Printing_Profile_Footer__c> printingProfileFooterInsert (Printing_Profile__c pp1, Printing_Profile__c pp2, Printing_Profile__c pp3, Printing_Profile__c pp4, 
		Printing_Profile__c pp5, Printing_Profile__c pp6, Printing_Profile__c pp7, Printing_Profile__c pp8,Printing_Profile__c pp9, Printing_Profile__c pp10, Printing_Profile__c pp11, Printing_Profile__c pp12) {
            
            

            id pp1Id = pp1.Id;
            id pp2Id = pp2.Id;
            id pp3Id = pp3.Id;
            id pp4Id = pp4.Id;
            id pp5Id = pp5.Id;
            id pp6Id = pp6.Id;
            id pp7Id = pp7.Id;
            id pp8Id = pp8.Id;
            id pp9Id = pp9.Id;
        	id pp10Id = pp10.Id;
            id pp11Id = pp11.Id;
            id pp12Id = pp12.Id;
            
            
        	List<Printing_Profile_Footer__c> printProfFootList = new List<Printing_Profile_Footer__c>();
			Printing_Profile_Footer__c ppf1 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_IT_STD',
				Text__c = '<p>Printing Profile Footer GUT_IT_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp1Id
			);

			printProfFootList.add( ppf1);

			Printing_Profile_Footer__c ppf2 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_FR_MIG',
				Text__c = '<p>Printing Profile Footer GUT_FR_MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp2Id
			);
            printProfFootList.add( ppf2);
			


			Printing_Profile_Footer__c ppf3 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_FR_STD',
				Text__c = '<p>Printing Profile Footer GUT_FR_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp3Id
			);
            printProfFootList.add( ppf3);
			

			Printing_Profile_Footer__c ppf4 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_FR MIG',
				Text__c = '<p>Printing Profile Footer FAK_FR MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp4Id
			);
             printProfFootList.add( ppf4);
			

			Printing_Profile_Footer__c ppf5 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_IT_STD',
				Text__c = '<p>Printing Profile Footer FAK_IT_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp5Id
			);
             printProfFootList.add( ppf5);
			

			Printing_Profile_Footer__c ppf6 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_IT_MIG',
				Text__c = '<p>Printing Profile Footer FAK_IT_MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp6Id
			);
             printProfFootList.add( ppf6);
			

			Printing_Profile_Footer__c ppf7 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_DE_STD',
				Text__c = '<p>Printing Profile Footer GUT_DE_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp7Id
			);
             printProfFootList.add( ppf7);

			Printing_Profile_Footer__c ppf8 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_FR_STD',
				Text__c = '<p>Printing Profile Footer FAK_FR_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp8Id
			);
            printProfFootList.add( ppf8);

			Printing_Profile_Footer__c ppf9 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_DE_STD',
				Text__c = '<p>Printing Profile Footer FAK_DE_STD</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp9Id
			);
             printProfFootList.add( ppf9);


			Printing_Profile_Footer__c ppf10 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer FAK_DE_MIG',
				Text__c = '<p>Printing Profile Footer FAK_DE_MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp10Id
			);
            printProfFootList.add( ppf10);

			Printing_Profile_Footer__c ppf11 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_DE_MIG',
				Text__c = '<p>Printing Profile Footer GUT_DE_MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp11Id
			);
            printProfFootList.add( ppf11);

			Printing_Profile_Footer__c ppf12 = new Printing_Profile_Footer__c (
				Name = 'Printing Profile Footer GUT_IT_MIG',
				Text__c = '<p>Printing Profile Footer GUT_IT_MIG</p>',
				Type__c = 'Text',
				Printing_Profile__c = pp12Id
			);
            printProfFootList.add( ppf12);
			insert printProfFootList;        
        	return printProfFootList;
	}       
    
}