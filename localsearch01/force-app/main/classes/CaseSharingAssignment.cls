/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* After the case is routed to the correct queue, the creator losts its visibility. This class
* provides methods to create a sharing rule (Read Only mode) for the case creator.
*
* The main method of this class is invoked by *NOME_PB* Process Builder.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-07-03
* @systemLayer    Invocation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class CaseSharingAssignment {
    
    public class CaseParams{
        @InvocableVariable(required=true)
        public Id caseId;
        @InvocableVariable(required=true)
        public Id createdById;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method creates a sharing rule (Read only mode) for the case creator. Before the sharing
    * rule is created, a check is made to ensure that a sharing rule for the current case creator 
    * does not already exists
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    caseParams	List of instances of object "CaseParams" containing case id and 
    * 								its creator id
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @InvocableMethod(label='Share Case' description='Share Case in RO mode to its creator')
    public static void shareCase(List<CaseParams> caseParams){
        
        List<CaseParams> casesToShare = new List<CaseParams>();
        
        Set<Id> casesToShareIds = new Set<Id>();
        Set<Id> createdByIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        Set<Id> profileIds = new Set<Id>();
        Map<Id, Profile> profilesMap;
        
        for(CaseParams currentCase : caseParams){
            userIds.add(currentCase.createdById);
        }
        
        Map<Id, User> usersMap = SEL_User.getUsersById(userIds);
        if(usersMap != NULL && usersMap.size() > 0){
            for(User u : usersMap.values())
                profileIds.add(u.ProfileId);
            
            if(profileIds != NULL && profileIds.size() > 0)
                profilesMap = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Id IN :profileIds]);
        }
        
        if(profilesMap != NULL){
            for(CaseParams currentCase : caseParams){
                if(usersMap != NULL && usersMap.containsKey(currentCase.createdById) && profilesMap.containsKey(usersMap.get(currentCase.createdById).ProfileId)){
                    casesToShareIds.add(currentCase.caseId);
                    createdByIds.add(currentCase.createdById);
                }
            }
        }
        
        List<CaseShare> caseSharing = [SELECT CaseId, UserOrGroupId FROM CaseShare WHERE CaseId IN :casesToShareIds AND UserOrGroupId IN :createdByIds];
        
        System.debug(caseSharing);
        
        if(!caseSharing.isEmpty()){
            
            Map<Id, Set<Id>> caseToUsers = new Map<Id, Set<Id>>();
            
            createMap(caseToUsers, caseSharing);
            
            casesToShare = getCaseToShare(caseToUsers, caseParams);
            
        }
        else casesToShare.addAll(caseParams);
    	
        List<CaseShare> sharingToInsert = new List<CaseShare>();
        if(profilesMap != NULL){
            for(CaseParams currentCase : casesToShare){
                if(usersMap != NULL && usersMap.containsKey(currentCase.createdById) && profilesMap.containsKey(usersMap.get(currentCase.createdById).ProfileId)){
                    sharingToInsert.add(createSharing(currentCase.caseId, currentCase.createdById));
                }
            }
        }
        
        if(!sharingToInsert.isEmpty()) insert sharingToInsert;
    }
    
    public static void createMap(Map<Id, Set<Id>> caseToUsers, List<CaseShare> caseSharing){
        for(CaseShare currentShare : caseSharing){
            if(!caseToUsers.containsKey(currentShare.CaseId)) caseToUsers.put(currentShare.CaseId, new Set<Id>{currentShare.UserOrGroupId});
            else caseToUsers.get(currentShare.CaseId).add(currentShare.UserOrGroupId);
        }
    }
    
    public static List<CaseParams> getCaseToShare(Map<Id, Set<Id>> caseToUsers, List<CaseParams> caseParams){
        
        List<CaseParams> casesToShare = new List<CaseParams>();
        
        for(CaseParams currentCase : caseParams){
            if(caseToUsers.containsKey(currentCase.caseId)){
                if(!caseToUsers.get(currentCase.caseId).contains(currentCase.createdById)) casesToShare.add(currentCase);
            }
            else casesToShare.add(currentCase);
        }
        
        return casesToShare;
        
    }
    
    public static CaseShare createSharing(Id caseId, Id userId){
        CaseShare share = new CaseShare();
        share.CaseAccessLevel = 'Read';
        share.UserOrGroupId = userId;
        share.CaseId = caseId;
        share.RowCause = 'Manual';
        return share;
    }

}