@IsTest
public class migraOppTest {
    
    @TestSetup
    static void initData(){
        list<Account>  accList= TestSetupUtilClass.createAccounts(1);
       	        
        Place__c pl = new Place__c();
        pl.LastName__c  = 'XXX ';
        pl.GoldenRecordID__c = 'GRID';
        pl.City__c = 'dh';
        pl.State__c  = 'bd'; 
        pl.Company__c  = 'ITL';
        pl.Street__c  = 'utt';
        pl.PostalCode__c  = '1230';
        pl.Country__c  = 'bd';  
        pl.PlaceID__c ='PL00';
        pl.Quote_Migration_Batch__c = 'B1';
        pl.Product_Information__c = 'SLS001';
        pl.SLmig_AmountCategory__c = 1;
        pl.SLmig_AmountLocation__c = 1;
        pl.SLmig_StartDate__c = DateTime.now();
        pl.Account__c = accList.get(0).id;
        insert pl;
        Account a = [SELECT id,Name,GoldenRecordID__c FROM Account WHERE id =: accList.get(0).id ];
        insert TestSetupUtilClass.ContactGenaratorByAccount(a);
        insert TestSetupUtilClass.BillingProfileGeneratorByAccount(a); 
        
        Swisslist_CPQ.config();
    }
    
   /* 
	@Istest
    static void testCreateOpp() {
		Test.startTest();
        migraOpp op = new migraOpp('name','B1',Date.today());
        database.executeBatch(op,1);
        Test.stopTest();

        
    }   
    
    @Istest
    static void testCreateQL() {
        
		List<Account> lstA = [Select id,Name,GoldenRecordID__c from Account limit 1 ];
       	insert TestSetupUtilClass.OpportunityGeneratorByAccount(lstA.get(0),Date.today(),Test.getStandardPricebookId());
		List<SBQQ__Quote__c> lstQ = [SELECT id,SBQQ__Account__c,SBQQ__StartDate__c FROM SBQQ__Quote__c limit 1 ];
        Contact c = [SELECT id from Contact limit 1];
        Billing_Profile__c b = [SELECT id from Billing_Profile__c limit 1];
        lstQ.get(0).SL_Migration__c = true;
        lstQ.get(0).SBQQ__PrimaryContact__c = c.id;
        lstQ.get(0).Billing_Profile__c = b.id;
        lstQ.get(0).Quote_Migration_Batch__c = 'B1';
        update lstQ.get(0);
       	database.executeBatch(new migraQuote2('B1')); 
        
    } 
    
     @Istest
    static void testCreateBP() {
        Account a = [Select id,Name from Account limit 1 ];
       	insert TestSetupUtilClass.OpportunityGeneratorByAccount(a,Date.today(),Test.getStandardPricebookId());     
       	database.executeBatch(new migraBPContact('B1')); 

    } 
    
     @Istest
    static void testchangeQuoteDates() {
        Account a = [Select id,Name from Account limit 1 ];
       	insert TestSetupUtilClass.OpportunityGeneratorByAccount(a,Date.today(),Test.getStandardPricebookId());     
       	database.executeBatch(new migraQuoteDates('B1',Date.today())); 

    } 
    
    @Istest
    static void testContract() {  
       	database.executeBatch(new migraContract('B1',false)); 

    } 
    
    */
    
   
    
}