global without sharing class ContractTerminationQueueJob implements Queueable {

	private List<Contract> vlistContract;
   
    public ContractTerminationQueueJob(List<Contract> listContract) {
        vlistContract= listContract;       
    }    
  global void execute(QueueableContext SC) {
	 ContractUtility.TerminateContractsJob(vlistContract);
  }
}