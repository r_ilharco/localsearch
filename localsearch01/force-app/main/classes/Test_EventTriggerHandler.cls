@isTest
public class Test_EventTriggerHandler {
	@TestSetup
    public static void setup(){
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        List<Contact> contact = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'test last name', 1);
        insert contact;
        Opportunity oppo = test_DataFactory.generateOpportunity('Test Oppo', acc[0].Id);
        insert oppo;
    }
    
    @isTest
    public static void EventTriggerHandler(){
        Contact cont = [select Id, IsDeleted, MasterRecordId, AccountId, LastName, FirstName, Salutation, MiddleName, Suffix, Name, RecordTypeId, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, OtherLatitude, OtherLongitude, OtherGeocodeAccuracy, OtherAddress, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingLatitude, MailingLongitude, MailingGeocodeAccuracy, MailingAddress, Phone, Fax, MobilePhone, HomePhone, OtherPhone, AssistantPhone, ReportsToId, Email, Title, Department, AssistantName, LeadSource, Birthdate, Description, OwnerId, HasOptedOutOfEmail, HasOptedOutOfFax, DoNotCall, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastCURequestDate, LastCUUpdateDate, LastViewedDate, LastReferencedDate, EmailBouncedReason, EmailBouncedDate, IsEmailBounced, PhotoUrl, Jigsaw, JigsawContactId, IndividualId, Language__c, Position__c, Benutzername__c, Personalnummer__c, Regionaldirektor__c, Regionalleiter__c, RD__c, RL__c, DataSource__c, GoldenRecordID__c, MailingAddressValidationDate__c, MailingAddressValidationStatus__c, PO_BoxCity__c, PO_BoxZip_PostalCode__c, PO_Box__c, Title__c, ZIPExtension__c, c_o__c, Primary__c, Business_Phone__c, Contact_Personal_Function__c, GoldenrecordId_Migration__c, Mobile_Phone__c from Contact LIMIT 1]; 
       	Opportunity oppo = [select Id, IsDeleted, AccountId, IsPrivate, Name, Description, StageName, Amount, Probability, ExpectedRevenue, TotalOpportunityQuantity, CloseDate, Type, NextStep, LeadSource, IsClosed, IsWon, ForecastCategory, ForecastCategoryName, CampaignId, HasOpportunityLineItem, Pricebook2Id, OwnerId, Territory2Id, IsExcludedFromTerritory2Filter, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, FiscalQuarter, FiscalYear, Fiscal, ContactId, LastViewedDate, LastReferencedDate, SyncedQuoteId, ContractId, HasOpenActivity, HasOverdueTask, Budget_Confirmed__c, Discovery_Completed__c, ROI_Analysis_Completed__c, SBQQ__AmendedContract__c, Loss_Reason__c, SBQQ__Contracted__c, SBQQ__CreateContractedPrices__c, SBQQ__OrderGroupID__c, SBQQ__Ordered__c, SBQQ__PrimaryQuote__c, SBQQ__QuotePricebookId__c, SBQQ__Renewal__c, SBQQ__RenewedContract__c, BulkCreation__c, ExternalOPTY__c, Quote_Migration_Batch__c, Opportunity_reason__c, PushCount__c, Amount__c, Cluster_Id__c, Downgrade__c, SambaMigration__c, UpgradeDowngrade_Contract__c, Upgrade__c from Opportunity LIMIT 1];
        Event ev = new Event();
        ev.DurationInMinutes = 5;
        ev.ActivityDateTime = Datetime.now();
        ev.Subject = 'Call';
        ev.WhoId = cont.Id;
        ev.WhatId = oppo.Id;
        List<Event> listEvent = new List<Event>();
        listEvent.add(ev); 
        
        Test.startTest();
        insert listEvent;
        listEvent[0].DurationInMinutes = 10;
        update listEvent;
        Test.stopTest();
    }
    
}