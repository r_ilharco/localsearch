/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author      : Mara Mazzarella <mamazzarella@deloitte.it>
* Date	      : 2019-11-21
* Sprint      : Sprint 11
* Work item   : Samba Migration - Phase 2
* Testclass   :
* Package     : 
* Description : 
* Changelog   :
*/

global without sharing class createContractfromMigrated implements Database.Batchable<sObject>, Schedulable{
    
    global void execute(SchedulableContext sc) {
        createContractfromMigrated contracts = new createContractfromMigrated(); 
        Database.executeBatch(contracts, 1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
       return Database.getQueryLocator('SELECT Id, SambaIsStartConfirmed__c,SBQQ__Contracted__c from Order where'+
                                      ' SambaIsStartConfirmed__c  = TRUE'+
                                      ' AND SBQQ__Contracted__c = FALSE'+
                                      ' AND Type  = \'' + ConstantsUtil.ORDER_TYPE_NEW + '\''+
                                      ' AND Status  = \'' + ConstantsUtil.ORDER_STATUS_ACTIVATED + '\' LIMIT 50000');
    }
    global String log = '';
    global void execute(Database.BatchableContext info, List<Order> scope){
         try{
            System.debug('scope '+scope);
            Set<Id> opp = new Set<Id>();
            for(Order o: scope){
                log += o.Id+',';
                o.SBQQ__Contracted__c = true;
            }
            update scope;
        }
        catch(Exception e){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'createContractfromMigrated', 'createContractfromMigrated.execute', e, ErrorHandler.ErrorCode.E_DML_FAILED, 'OrderIds :'+log, null, null, null, null, false);
        }
    }
    global void finish(Database.BatchableContext context) {
        
    }

}