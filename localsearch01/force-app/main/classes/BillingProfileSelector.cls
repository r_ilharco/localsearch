/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-08-2019
 * Sprint      : 1
 * Work item   : 25
 * Testclass   :
 * Package     : 
 * Description : Selector Object for Billing Profile 
 * Changelog   : 
 */

public class BillingProfileSelector implements IObjectSelector {

    /**
	 * Gennaro Casola | Sprint 1 | Date 04-08-2019
	 * Description It retrieves a map of billing profiles set as default related to accounts associated for each element on bplist.
	 *
	 * @param bplist A list of Billing Profiles 
	 *
	 * @return Map<Id, Billing_Profile__c>
	 */
    public static Map<Id, Billing_Profile__c> getBPDefaultsNotIn(Map<Id, Billing_Profile__c> bpMap)
    {
        Map<Id, Billing_Profile__c> mapToReturn;
        Set<Id> accts = new Set<Id>();
        for(Billing_Profile__c bp : bpMap.values())
        {
            accts.add(bp.Customer__c);
        }

         mapToReturn = new Map<Id, Billing_Profile__c>([SELECT Id FROM Billing_Profile__c WHERE Customer__c IN :accts AND Is_Default__c = true AND Id NOT IN :bpMap.keySet()]);

         /*for(Billing_Profile__c bp : bplist)
         {
             mapToReturn.remove(bp.Id);
         }*/

         return mapToReturn; 
    }
}