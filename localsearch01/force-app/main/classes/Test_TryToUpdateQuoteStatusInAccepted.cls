@isTest
public class Test_TryToUpdateQuoteStatusInAccepted {
		
    	@testSetup static void setupData(){
		
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Opportunity Management,Quote Management';
		insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Map<String, Id> usersByUsernameMap = new Map<String,Id>();
        Test_DataFactory.namirial_insertFutureUsers();
        Map<Id, User> users = new Map<Id,User>([SELECT Id, Username FROM User]);
        if(users != NULL && users.size() > 0)
        {
            for(User u : users.values())
            {
            	usersByUsernameMap.put(u.Username, u.Id);
            }
        }
        
        Pricebook2 pbDMC = new Pricebook2();
        pbDMC.ExternalId__c = 'PB3';
        pbDMC.Name = 'DMC';
        pbDMC.IsActive = true;
        insert pbDMC;
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        
        List<Account> accounts = Test_DataFactory.createAccounts('AccountTest01',1);
 		insert accounts;
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0],1);
        contacts[0].FirstName = 'FirstName';
        insert contacts;
        
        List<Opportunity> opps = Test_DataFactory.createOpportunities('OppName01','Draft',accounts[0].Id,1);
        opps[0].Name = 'OppName01';
        opps[0].Pricebook2Id = pbDMC.Id;
        insert opps;
        List<SBQQ__Quote__c> quotes = Test_DataFactory.createQuotes('New', opps[0], 1);
        quotes[0].SBQQ__SalesRep__c = usersByUsernameMap.get('dmctester@test.com');
        quotes[0].Filtered_Primary_Contact__c = contacts[0].Id;
        quotes[0].SBQQ__PrimaryContact__c = contacts[0].Id;
        quotes[0].SBQQ__PriceBook__c = pbDMC.Id;
        insert quotes;

    }
    
        @isTest static void test_TryToUpdateQuoteStatusInAccepted(){
        List<SBQQ__Quote__c> quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];

        Test.startTest();
        	TryToUpdateQuoteStatusInAccepted.tryToUpdateQuoteStatusInAccepted(quote);
        Test.stopTest();        
    	}
    
    
    
    
}