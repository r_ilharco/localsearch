/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Mara Mazzarella <mamazzarella@deloitte.it>
 * Date	       : 2019-11-06
 * Sprint      : Sprint 7
 * Work item   : SF2-300, Sprint7, Order Management.
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */

global without sharing class activeMigratedOrder implements Database.Batchable<sObject>, Schedulable{
    global List<String> staticValues = new List<String>();

    global activeMigratedOrder(List<String> val){
        this.staticValues = val;
    }
	global void execute(SchedulableContext sc) {
		Integration_Config__mdt config = [SELECT Id, Active__c,Method__c  FROM Integration_Config__mdt WHERE MasterLabel = 'TriggerMigration'][0];
		Integer sizeBatch = Integer.valueOf(config.Method__c);
		
    	activeMigratedOrder a = new activeMigratedOrder(this.staticValues); 
    	Database.executeBatch(a, sizeBatch);
   	}
    global Database.QueryLocator start(Database.BatchableContext bc){    
      /*Id ids = '8011X000000r2MHQAY';
       return Database.getQueryLocator('SELECT Id, SBQQ__Contracted__c,EffectiveDate from Order where'+
                                      ' (SBQQ__Quote__r.ContractSAMBA_Key__c != null or ContractSAMBA_Key__c != null )'+
                                      ' AND Type  = \'' + ConstantsUtil.ORDER_TYPE_NEW + '\''+
                                      ' AND id= \'' + ids + '\'');*/
     
       return Database.getQueryLocator('SELECT Id, SBQQ__Quote__c, SBQQ__Contracted__c,EffectiveDate from Order where'+
                                      ' (SBQQ__Quote__r.ContractSAMBA_Key__c != null or ContractSAMBA_Key__c != null )'+
                                      ' AND Type  = \'' + ConstantsUtil.ORDER_TYPE_NEW + '\' and SBQQ__Contracted__c = false and Processed__c = false '+
                                      ' AND Status  != \'' + ConstantsUtil.ORDER_STATUS_ACTIVATED + '\'and Account.Cluster_Id__c in: staticValues LIMIT 10000');
   	}
    
    global void execute(Database.BatchableContext info, List<Order> scope){
        try{
            Set<Id> ids = new Set<Id>();
            Boolean updateflag = false;
            Map <Id,Boolean> sambaisbilled = new Map<Id,Boolean>();
            Map<Id,Boolean> sambaisconfirmed = new Map<Id,Boolean>();
			//Map<Id, Order> quoteToOrder = new Map<Id, Order>();
			
			/*for(Order o : scope){
				
				if(!quoteToOrder.containsKey(o.SBQQ__Quote__c)){					
					quoteToOrder.put(o.SBQQ__Quote__c, o);				
				}	
			}*/
					
            for(Order o: scope){ 

                sambaisbilled.put(o.Id,false);
                sambaisconfirmed.put(o.Id,false);
            }
          // 	List<OrderItem> ois = SEL_OrderItem.getParentOrderItemsByOrderId(ids);
            
                for(OrderItem oi : SEL_OrderItem.getParentOrderItemByOrderIds(sambaisbilled.keySet())){
                    if(oi.SambaIsBilled__c == true)
                       sambaisbilled.put(oi.OrderId,true);
                    if(oi.SambaIsStartConfirmed__c == true)
                       sambaisconfirmed.put(oi.OrderId,true);
                }
                for(Order o: scope){
                    o.Processed__c= true;
                    o.SambaIsBilled__c = sambaisbilled.get(o.Id);
                    o.SambaIsStartConfirmed__c = sambaisconfirmed.get(o.Id);
                    if(o.SambaIsStartConfirmed__c ){
                        o.Status  = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                        o.SBQQ__Contracted__c = true;
                        if(o.EffectiveDate >date.today()){
                            o.Status  = ConstantsUtil.ORDER_STATUS_PUBLICATION;
                        	o.SBQQ__Contracted__c = false;
                        }
                    }
                }
            //update quoteToOrder.values();
            List<Database.SaveResult> resultsOrder = Database.update(scope, false);
			for (Database.SaveResult sr : resultsOrder) {
                if (!sr.isSuccess()) {              
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Order fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        catch(Exception e){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'activeMigratedOrder', 'activeMigratedOrder.execute', e, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);
        }
    }
    
    global void finish(Database.BatchableContext context) {}
    
}