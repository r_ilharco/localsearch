/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to fields listed in SPIII 1057 removed
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-03                      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class Test_QuoteTerritoryChangedBatch {
    static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
            insert byPassTrigger;
        }
    
    @TestSetup
    public static void setup(){
        insertBypassFlowNames();
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        List<UserTerritory2Association> uT2As = [SELECT ID, territory2.Name, territory2.DeveloperName FROM UserTerritory2Association];
       /* for(UserTerritory2Association uT2A : uT2As){
            if(uT2A.territory2.DeveloperName.contains('Area')){
                acc[0].Area_Code__c = uT2A.territory2.Name;
                break;
            }
        }*/
        insert acc;
        Opportunity oppo =  Test_DataFactory.generateOpportunity('Test Opportunity', acc[0].Id);
        oppo.Pricebook2Id = Test.getStandardPricebookId();
        insert oppo;
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last Name Test', 1);
        insert cont;
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(oppo.Id, acc[0].Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        insert quote1;
    }
    
    @isTest
    public static void test_QuoteTerritoryChangedBatchIF(){  
        
        List<Account> listAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                                     BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, ShippingCity, 
                                     ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, ShippingAddress,
                                     Phone, Fax, AccountNumber, Website, PhotoUrl, Sic, Industry, AnnualRevenue, NumberOfEmployees, Ownership, TickerSymbol, Description,
                                     Rating, Site, OwnerId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate,
                                     LastReferencedDate, IsExcludedFromRealign, IsCustomerPortal, AccountSource, SBQQ__AssetQuantitiesCombined__c, 
                                     SBQQ__CoTermedContractsCombined__c, SBQQ__CoTerminationEvent__c, SBQQ__ContractCoTermination__c, SBQQ__DefaultOpportunity__c,
                                     SBQQ__IgnoreParentContractedPrices__c, SBQQ__PreserveBundle__c, SBQQ__PriceHoldEnd__c, SBQQ__RenewalModel__c,
                                     SBQQ__RenewalPricingMethod__c, SBQQ__TaxExempt__c, Account_Fax__c, Account_Phone__c, Account_Rating__c, Active_Contracts__c,
                                     Amount_At_Risk__c, BillingAddressValidationDate__c, BillingAddressValidationStatus__c, 
                                     ChurnReason__c, ClientRating__c, 
                                     Company_Since__c, Created_Date__c, CreditStatus__c, Customer_Number__c, DataSource__c, Email__c, GoldenRecordID__c, 
                                     LCM_NX_MergeIDs__c, LCM_SAMBA_MergeIDs__c, LegalEntity__c, Mobile_Phone__c, 
                                     POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, RegionalDirector__c,
                                     RegionalLeader__c, SalesChannel__c, Salutation__c, ShippingAddressValidationDate__c, ShippingAddressValidationStatus__c,
                                     ShippingPO_BoxCity__c, ShippingPO_BoxZip_PostalCode__c, ShippingPO_Box__c, Special__c, Status__c, 
                                     Swiss_List_Candidate__c, UID__c, 
                                     Language_Short_Form__c, Reason__c, Contact_Method__c, Contact_Method_i__c, Activation_Date__c,
                                     AddressValidated__c, AddressValidationDate__c, Address_Integration__c, Allowed_communication_channel__c, ApprovalStep__c,
                                     Approval_Requester_Email__c, Assigned_Territory__c, Cluster_Id__c, Communication_Channel__c, Communication_stop__c,
                                     Converted__c, Customer_Type__c, Digital_Score__c, Employees__c,
                                     Foundation_Date__c, Full_Address__c, HRID__c, 
                                     Last_Inactive_Date__c, Legal_Address_equals_to_Shipping_Address__c, Legal_Form__c, Lock_Account__c,
                                     MarkedAsNotDuplicated__c, New_Customer__c, Onwer_Formula__c, Owner_Changed__c, Process_Type__c,
                                     Product_of_interest__c, Reassigned__c, Resubmission_date__c, Resubmission_reason__c, 
                                     Sales_Status__c, Sales_manager_substitute__c,  
                                     Tech_ZipCode__c, Territory_Changed__c, VAT_ID__c, 
                                     isDuplicated__c FROM Account LIMIT 1];
        List<Id> ListAccountId = new List<Id>();
        ListAccountId.add(listAccount[0].Id);
  
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                            Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        QuoteTerritoryChangedBatch tsc = new QuoteTerritoryChangedBatch(ListAccountId);
        Database.executeBatch(tsc,100);
        Test.stopTest(); 
    }
    
    @isTest
    public static void test_QuoteTerritoryChangedBatchELSE(){  
        
        List<Account> listAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, BillingCity, BillingState, BillingPostalCode, 
                                     BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, ShippingCity, 
                                     ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, 
                                     ShippingAddress, Phone, Fax, AccountNumber, Website, PhotoUrl, Sic, Industry, AnnualRevenue, NumberOfEmployees, Ownership, 
                                     TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp,
                                     LastActivityDate, LastViewedDate, LastReferencedDate, IsExcludedFromRealign, IsCustomerPortal, AccountSource,
                                     SBQQ__AssetQuantitiesCombined__c, SBQQ__CoTermedContractsCombined__c, SBQQ__CoTerminationEvent__c,
                                     SBQQ__ContractCoTermination__c, SBQQ__DefaultOpportunity__c, SBQQ__IgnoreParentContractedPrices__c, SBQQ__PreserveBundle__c, 
                                     SBQQ__PriceHoldEnd__c, SBQQ__RenewalModel__c, SBQQ__RenewalPricingMethod__c, SBQQ__TaxExempt__c, Account_Fax__c, 
                                     Account_Phone__c, Account_Rating__c, Active_Contracts__c, Amount_At_Risk__c, BillingAddressValidationDate__c,
                                     BillingAddressValidationStatus__c, 
                                     ChurnReason__c, ClientRating__c, Company_Since__c, Created_Date__c, CreditStatus__c,
                                     Customer_Number__c, DataSource__c, Email__c, GoldenRecordID__c, LCM_NX_MergeIDs__c, 
                                     LCM_SAMBA_MergeIDs__c, LegalEntity__c, Mobile_Phone__c, POBox__c, P_O_Box_City__c, 
                                     P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, RegionalDirector__c, RegionalLeader__c,
                                     SalesChannel__c, Salutation__c, ShippingAddressValidationDate__c, ShippingAddressValidationStatus__c, 
                                     ShippingPO_BoxCity__c, ShippingPO_BoxZip_PostalCode__c, ShippingPO_Box__c, Special__c, Status__c,
                                     Swiss_List_Candidate__c, UID__c, Language_Short_Form__c, Reason__c, Contact_Method__c, Contact_Method_i__c, 
                                     Activation_Date__c, AddressValidated__c, AddressValidationDate__c, Address_Integration__c, Allowed_communication_channel__c,
                                     ApprovalStep__c, Approval_Requester_Email__c, Assigned_Territory__c, Cluster_Id__c, Communication_Channel__c,
                                     Communication_stop__c, Converted__c, Customer_Type__c, Digital_Score__c, 
                                     Employees__c, Foundation_Date__c,
                                     Full_Address__c, HRID__c, Last_Inactive_Date__c, Legal_Address_equals_to_Shipping_Address__c, 
                                     Legal_Form__c, Lock_Account__c, MarkedAsNotDuplicated__c, New_Customer__c,
                                     Onwer_Formula__c, Owner_Changed__c, Process_Type__c, Product_of_interest__c, Reassigned__c, Resubmission_date__c,
                                     Resubmission_reason__c, Sales_Status__c, Sales_manager_substitute__c,   
                                     Tech_ZipCode__c, Territory_Changed__c, VAT_ID__c, isDuplicated__c FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                            Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        update listAccount;
        SBQQ.TriggerControl.enable();
        Territory2 terr = [Select Id FROM Territory2 Limit 1];
        ObjectTerritory2Association oTa = new ObjectTerritory2Association();
        oTa.ObjectId = listAccount[0].Id;
        oTa.Territory2Id = terr.Id;
        oTa.AssociationCause = 'Territory2Manual';
        insert oTa;
        AccountTriggerHandler.disableTrigger = false;
        List<Account> listAccount2 = [SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                                      BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, ShippingCity,
                                      ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, 
                                      ShippingAddress, Phone, Fax, AccountNumber, Website, PhotoUrl, Sic, Industry, AnnualRevenue, NumberOfEmployees,
                                      Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById, LastModifiedDate,
                                      LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, IsExcludedFromRealign,
                                      IsCustomerPortal, AccountSource, SBQQ__AssetQuantitiesCombined__c, SBQQ__CoTermedContractsCombined__c,
                                      SBQQ__CoTerminationEvent__c, SBQQ__ContractCoTermination__c, SBQQ__DefaultOpportunity__c, 
                                      SBQQ__IgnoreParentContractedPrices__c, SBQQ__PreserveBundle__c, SBQQ__PriceHoldEnd__c, SBQQ__RenewalModel__c,
                                      SBQQ__RenewalPricingMethod__c, SBQQ__TaxExempt__c, Account_Fax__c, Account_Phone__c, Account_Rating__c, 
                                      Active_Contracts__c, Amount_At_Risk__c, BillingAddressValidationDate__c,
                                      BillingAddressValidationStatus__c, 
                                      ChurnReason__c, ClientRating__c, Company_Since__c,
                                      Created_Date__c, CreditStatus__c, Customer_Number__c, DataSource__c, Email__c,
                                      GoldenRecordID__c, LCM_NX_MergeIDs__c, LCM_SAMBA_MergeIDs__c, LegalEntity__c,
                                      Mobile_Phone__c, POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c,
                                      PreferredLanguage__c, RegionalDirector__c, RegionalLeader__c, SalesChannel__c,
                                      Salutation__c, ShippingAddressValidationDate__c, ShippingAddressValidationStatus__c, 
                                      ShippingPO_BoxCity__c, ShippingPO_BoxZip_PostalCode__c, ShippingPO_Box__c, Special__c,
                                      Status__c, Swiss_List_Candidate__c, UID__c,  
                                      Language_Short_Form__c, Reason__c, 
                                      Contact_Method__c, Contact_Method_i__c, Activation_Date__c, AddressValidated__c, AddressValidationDate__c,
                                      Address_Integration__c, Allowed_communication_channel__c, ApprovalStep__c, Approval_Requester_Email__c,
                                       Assigned_Territory__c, Cluster_Id__c, Communication_Channel__c, Communication_stop__c, Converted__c,
                                      Customer_Type__c, Digital_Score__c, Employees__c,
                                      Foundation_Date__c, Full_Address__c,
                                      HRID__c, Last_Inactive_Date__c, Legal_Address_equals_to_Shipping_Address__c, Legal_Form__c,
                                      Lock_Account__c, MarkedAsNotDuplicated__c, New_Customer__c,
                                      Onwer_Formula__c, Owner_Changed__c, Process_Type__c, Product_of_interest__c, Reassigned__c, Resubmission_date__c,
                                      Resubmission_reason__c, Sales_Status__c, Sales_manager_substitute__c,
                                       
                                      Tech_ZipCode__c, Territory_Changed__c, VAT_ID__c, 
                                      isDuplicated__c FROM Account LIMIT 1];
        List<Id> ListAccountId2 = new List<Id>();
        ListAccountId2.add(listAccount2[0].Id);
        for(UserTerritory2Association ua : [SELECT ID, territory2.Name FROM UserTerritory2Association]){
            system.debug('NAMEEEE:' + ua.territory2.Name);
        }
        
        Test.startTest();      
        QuoteTerritoryChangedBatch tsc2 = new QuoteTerritoryChangedBatch(ListAccountId2);
        Database.executeBatch(tsc2,100);
        Database.BatchableContext db = null;
        QuoteOwnerRecalculationBatch tsc = new QuoteOwnerRecalculationBatch(ListAccountId2);
        tsc.execute(db, new list<SBQQ__Quote__c>{quote});
        tsc.finish(db);
        Test.stopTest(); 
    }
}