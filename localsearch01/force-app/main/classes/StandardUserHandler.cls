/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* In this class is located the ADFS User Territory Management 
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created		  2019-12-11
* @systemLayer    Trigger
* @TestClass 	  Test_StandardUserHandler

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Gennaro Casola
* 2019-12-11      SF2-422
*
* @modifiedby      Gennaro Casola
* 2020-06-19      SPIII-1183 - Modify ADFS Logic based on Territory Model - 
*				  They have been cutted off the third-level territories from the territory model, 
*				  and so It have been included the DMCs, with the same logics of the SMAs, 
*				  in the Automatic User Territory Management throught the ADFS.
*				  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class StandardUserHandler implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
    private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {
        SAMLResponse samlResponse = new SAMLResponse(u.Id, null, null, null, federationIdentifier, attributes, null);
        
        if(/*create &&*/ attributes.containsKey('user.username')) {
            u.Username = attributes.get('user.username');
        }
        //if(create) {
            if(attributes.containsKey('user.federationidentifier')) {
                u.FederationIdentifier = attributes.get('user.federationidentifier');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
        //}

        if(attributes.containsKey('user.code__c')) {
            String code = NULL;
            if(String.isNotBlank(attributes.get('user.code__c')))
            {
                Map<Id,UserConfiguration__mdt> utcs = new Map<Id,UserConfiguration__mdt>([SELECT Id, Service__c, Role__c, Profile__c, PermissionSets__c, LiveAgent__c, Code__c FROM UserConfiguration__mdt WHERE Code__c = :attributes.get('user.code__c')]);
                if(utcs != NULL && utcs.size() == 1)
            		code = attributes.get('user.code__c');
            }
            
            u.Code__c = code;
        }
        
        if(attributes.containsKey('user.phone')) {
            u.Phone = attributes.get('user.phone');
        }
        
        if(attributes.containsKey('user.email')) {
            u.Email = attributes.get('user.email');
        }
        
        if(attributes.containsKey('user.firstname')) {
            u.FirstName = attributes.get('user.firstname');
        }
        
        if(attributes.containsKey('user.lastname')) {
            u.LastName = attributes.get('user.lastname');
        }
        
        if(create && attributes.containsKey('user.languagelocalekey')) {
            String[] chars = attributes.get('user.languagelocalekey').split('');
            chars[0] = chars[0].toLowerCase(); 
            chars[1] = chars[1].toLowerCase();
            u.LanguageLocaleKey = String.join(chars, '');
            
        }
        
        if(create && attributes.containsKey('user.localesidkey')) {
            String[] chars = attributes.get('user.localesidkey').split('');
            chars[0] = chars[0].toLowerCase(); 
            chars[1] = chars[1].toLowerCase();
            u.LocaleSidKey = String.join(chars, '');
        }
        
        if(create && attributes.containsKey('user.alias')) {
            u.Alias = (attributes.get('user.alias').length()>5)?attributes.get('user.alias').substring(0,5):attributes.get('user.alias');
            u.CommunityNickname = (attributes.get('user.alias').length()>40)?attributes.get('user.alias').substring(0,40):attributes.get('user.alias');
        }
        
        if(/*create && */ attributes.containsKey('user.timezonesidkey')) {
            u.TimeZoneSidKey = attributes.get('user.timezonesidkey');
        }
     
        if(/*create &&*/ attributes.containsKey('user.emailencodingkey')) {
            u.EmailEncodingKey = attributes.get('user.emailencodingkey');
        }    
        
        
        if(attributes.containsKey('user.federationidentifier')) {
           u.FederationIdentifier = attributes.get('user.federationidentifier');
        }
        
        if(attributes.containsKey('user.employeekey'))
        {
            u.ExternalID_localsearch_SAM__c = attributes.get('user.employeekey');
        }
        
        if(/*create &&*/ attributes.containsKey('user.manager') && String.isNotBlank(attributes.get('user.manager')))
        {
            String managerFederationId = attributes.get('user.manager');
            Map<Id, User> users = SEL_User.getUsersByFederationId(new Set<String>{managerFederationId});
            
            if(users != NULL && users.size() == 1)
            	u.ManagerId = users.values().get(0).Id;
        }else
        {
            u.ManagerId = NULL;
        }
        
        if(/*create &&*/ attributes.containsKey('user.departmentnumber'))
        {
            if(!create){
            	Map<Id, UserTerritory2Association> ut2associationsMap = new Map<Id, UserTerritory2Association>([SELECT Id, UserId, Territory2Id, Territory2.Name, Territory2.DeveloperName 
                                                                                                                FROM UserTerritory2Association 
																												WHERE UserId = :u.Id
                                                                                                                AND IsActive = true
                                                                                                                AND RoleInTerritory2 = :u.Code__c]);
                if((ut2associationsMap != NULL && ut2associationsMap.size() == 1))
                    u.AssignedTerritory__c = ut2associationsMap.values().get(0).Territory2.Name;
                else
                	u.AssignedTerritory__c = NULL;
            }
            
            String departmentNumber = NULL;
            if(String.isNotBlank(attributes.get('user.departmentnumber')))
				departmentNumber = attributes.get('user.departmentnumber');
				
			u.DepartmentNumber__c = departmentNumber;
        }else
            u.AssignedTerritory__c = NULL;
        
        try{
            
            if(!create)
                update(u);
            else
                Database.SaveResult sr = Database.insert(u);
            
        } catch(Exception e){// temporal solution to catch errors between SF and ADFS
            saveADFSLog(u.Id, null, null, null, federationIdentifier,attributes, null , false, null);
            //saveADFSLog(null, samlResponse, true, e.getMessage());
        	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(new string[] {'josearmando.santas@localsearch.ch'});
            email.setSubject('Error SSO');
            email.setHtmlBody(e.getMessage());
            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }

    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        if(communityId == null && portalId == null) {
            handleUser(create, u, attributes, federationIdentifier, true);
        } else {
            throw new JitException('Not supported communities & portal');
        }
        
    }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        SAMLResponse samlResponse = new SAMLResponse(null, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        saveADFSLog(null, samlSsoProviderId, communityId, portalId, federationIdentifier,attributes, assertion , true, null);
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
        return u;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
            
            if(userId != NULL || String.isNotBlank(federationIdentifier))
            {
                String query = 'SELECT Id FROM User WHERE ';
                
                if(userId != NULL)
                    query += 'Id  = \'' + userId + '\'';
                
                if(userId != NULL && String.isNotBlank(federationIdentifier))
                    query += ' AND ';
                
                if(String.isNotBlank(federationIdentifier))
                    query += 'FederationIdentifier = \'' + federationIdentifier + '\'';
                
                //User u = [SELECT Id FROM User WHERE Id=:userId];
                List<User> users = Database.query(query);
                if(users != NULL && users.size() == 1)
                {
                    saveADFSLog(users[0].Id, samlSsoProviderId, communityId, portalId, federationIdentifier,attributes, assertion , false, null);
                    handleJit(false, users[0], samlSsoProviderId, communityId, portalId,
                    federationIdentifier, attributes, assertion);
                }
            }
    }
    
    @Future
    public static void saveADFSLog(Id userId, Id samlSsoProviderId,  Id communityId,  Id portalId,  String federationIdentifier,  Map<String, String> attributes,  String assertion, Boolean create, String exMessage){
        StandardUserHandler.SAMLResponse samlResponse = new StandardUserHandler.SAMLResponse(userId,samlSsoProviderId,communityId, portalId, federationIdentifier, attributes, assertion);
        Log__c l = new Log__c();
        l.Name = 'ADFS SSO ' + ConstantsUtil.LOG_BP_USER_MANAGEMENT +  ' ' + ConstantsUtil.RESPONSE+ ' '+string.valueOfGmt(Datetime.now());
        l.Communication_Ok__c = true;
        l.User__c =  userId;
        
        l.Body_Message__c = JSON.serialize(samlResponse);
        
        l.Business_Process__c = ConstantsUtil.LOG_BP_USER_MANAGEMENT;
        l.External_system__c = ConstantsUtil.LOG_ES_USER_MANAGEMENT;
        l.Correlation_ID__c =  '';
        
        
        
        if(String.isNotBlank(exMessage))
        {
            l.Type_of_event__c = ConstantsUtil.LOG_EVENT_TYPE_ERROR;
            l.Result__c = ConstantsUtil.LOG_RESULT_ERROR;
            l.Error__c = exMessage;
            l.Response_Body__c = ((create)?'CREATE':'UPDATE') + ' USERID: ' + userId + ' / FEDERATIONID: ' + samlResponse.federationIdentifier;
        }else
        {
            l.Type_of_event__c = ConstantsUtil.LOG_EVENT_TYPE_LOG;
            l.Result__c = ConstantsUtil.LOG_RESULT_SUCCESS;
            l.Response_Body__c = ((create)?'CREATE':'UPDATE') + 'USERID: ' + userId + ' FEDERATIONID: ' + samlResponse.federationIdentifier;
            l.Error__c = '';
        }
        
        insert l;
    }

    public class SAMLResponse{
        public Id userId;
        public Id samlSsoProviderId;
        public Id communityId;
        public Id portalId;
        public String federationIdentifier;
        public Map<String, String> attributes;
        public String assertion;
        
        public SAMLResponse(Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion)
        {
            this.userId = userId;
        	this.samlSsoProviderId = samlSsoProviderId;
        	this.communityId = communityId;
        	this.portalId = portalId;
        	this.federationIdentifier = federationIdentifier;
        	this.attributes = attributes;
        	this.assertion = assertion;
        }
    }
}