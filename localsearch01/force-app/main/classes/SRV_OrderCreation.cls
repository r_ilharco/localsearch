/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class contains util methods to create orders.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-08-12
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mara Mazzarella   <mamazzarella@deloitte.it>
* @changes        2021-02-25
*                 SPIII-5754 fix in method groupSubscription in order to make possible the 
*                  contract cancellation after Amendment 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘*/
public class SRV_OrderCreation {
	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Generate order.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    orderWrapper	   Wrapper class containing informations about order being created
    * @return         Order			   Order to insert
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Order generateOrder(OrderWrapper orderWrapper){
        Order order = new Order();
        order.AccountId = orderWrapper.accountId;
        order.SBQQ__Quote__c = orderWrapper.quoteId;
        order.Pricebook2Id = orderWrapper.pricebookId;
        order.Status = orderWrapper.Status;
        order.Type = orderWrapper.orderType;
        order.Custom_Type__c = orderWrapper.customType;
		//order.No_Clawback__c = noClawback; 
        order.Contract__c = orderWrapper.contractId;
        return order;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Generate order items.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    subscriptions	   	List of subscription from which the order items will be created
    * @input param    orderId	   	   	Order id
    * @input param    subIdToOrderItem	Util map to set RequiredBy field on children OrderItems
    * @return         OrderItem			OrderItems to insert
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static List<OrderItem> generateOrderItems(List<SBQQ__Subscription__c> subscriptions, Id orderId, Map<Id, OrderItem> subIdToOrderItem){

        List<OrderItem> orderItems = new List<orderItem>();
        
        for(SBQQ__Subscription__c currentSub : subscriptions){
            OrderItem oi = new OrderItem();
            oi.ServiceDate = currentSub.Termination_Date__c;
            oi.TLPaket_Region__c = currentSub.TLPaket_Region__c;
            oi.CategoryID__c = currentSub.CategoryID__c;
            oi.LocationID__c = currentSub.LocationID__c;
            oi.Edition__c = currentSub.SBQQ__QuoteLine__r.Edition__c;
            oi.Quantity = currentSub.SBQQ__Quantity__c;
            oi.Product2Id = currentSub.SBQQ__Product__c;
            if(currentSub.SBQQ__ListPrice__c == NULL) oi.UnitPrice = currentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            else oi.UnitPrice = currentSub.SBQQ__ListPrice__c;
            oi.PricebookEntryId = currentSub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c;
            oi.SBQQ__Contract__c = currentSub.SBQQ__Contract__c;
            oi.SBQQ__QuoteLine__c =currentSub.SBQQ__QuoteLine__c; 
            oi.SBQQ__Subscription__c = currentSub.Id;
            oi.OrderId = orderId;
            if(String.isNotBlank(currentSub.Campaign_Id__c)) oi.Campaign_Id__c = currentSub.Campaign_Id__c;
            if(String.isBlank(currentSub.RequiredBy__c)) subIdToOrderItem.put(currentSub.Id, oi);
            else if(subIdToOrderItem.containsKey(currentSub.RequiredBy__c)) oi.SBQQ__RequiredBy__c = subIdToOrderItem.get(currentSub.RequiredBy__c).Id;
            orderItems.add(oi);
        }

        return orderItems; 
    }
    
    /**
    * ────────────────────────────────────────────────────────────────────────────────────────────────┐
    * Creates a structure that groups subscriptions by contract and for each contract groups 
    * subscriptions by product group. For each product group of each contract an order must be created.
    * ─────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    masterSubscriptions					   	List of master subscriptions to group
    * @input param	  childrenSubscriptions					   	List of children subscriptions to group
    * @return         Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>>	Subscription 
    * 																				grouped by Product
    * 																				Group and Contract
    * ────────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>> groupSubscriptions(Map<Id, SBQQ__Subscription__c> masterSubscriptions, Map<Id, SBQQ__Subscription__c> childrenSubscriptions){
        
        Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>> subscriptionsHierarchyGrouped = new Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>>();
        
        for(SBQQ__Subscription__c currentSubscription : masterSubscriptions.values()){
            if(!subscriptionsHierarchyGrouped.containsKey(currentSubscription.SBQQ__Contract__c)){
                subscriptionsHierarchyGrouped.put(currentSubscription.SBQQ__Contract__c, new Map<String, Map<Id, List<SBQQ__Subscription__c>>>{currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c => new Map<Id, List<SBQQ__Subscription__c>>{currentSubscription.Id => new List<SBQQ__Subscription__c>()}});
            }
            else{
                if(subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).containsKey(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c)){
                	subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c).put(currentSubscription.Id, new List<SBQQ__Subscription__c>());
            	}
                else subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).put(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c, new Map<Id, List<SBQQ__Subscription__c>>{currentSubscription.Id => new List<SBQQ__Subscription__c>()});
        	}
        }
		
        if(!childrenSubscriptions.isEmpty()){
            for(SBQQ__Subscription__c currentSubscription : childrenSubscriptions.values()){
                if(!subscriptionsHierarchyGrouped.isEmpty()) {
                    if(subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).containsKey(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c))
                    	subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c).get(currentSubscription.RequiredBy__c).add(currentSubscription);
                	else 
                        subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.RequiredBy__r.SBQQ__QuoteLine__r.OrderGroup__c).get(currentSubscription.RequiredBy__c).add(currentSubscription);
                }
                else if(subscriptionsHierarchyGrouped.isEmpty()){
                    if(!subscriptionsHierarchyGrouped.containsKey(currentSubscription.SBQQ__Contract__c)){
                        subscriptionsHierarchyGrouped.put(currentSubscription.SBQQ__Contract__c, new Map<String, Map<Id, List<SBQQ__Subscription__c>>>{currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c => new Map<Id, List<SBQQ__Subscription__c>>{currentSubscription.RequiredBy__c => new List<SBQQ__Subscription__c>{currentSubscription}}});
                    }
                    else{
                        if(subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).containsKey(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c)){
                            if(subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c).containsKey(currentSubscription.RequiredBy__c)){
                                subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c).get(currentSubscription.RequiredBy__c).add(currentSubscription);
                            }
                            else subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).get(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c).put(currentSubscription.RequiredBy__c, new List<SBQQ__Subscription__c>{currentSubscription});
                        }
                        else subscriptionsHierarchyGrouped.get(currentSubscription.SBQQ__Contract__c).put(currentSubscription.SBQQ__QuoteLine__r.OrderGroup__c, new Map<Id, List<SBQQ__Subscription__c>>{currentSubscription.RequiredBy__c => new List<SBQQ__Subscription__c>{currentSubscription}});
                    }
                }
            }
        }
                
        return subscriptionsHierarchyGrouped;
    }
    
    
    public class CompositeOrderValue{
        public Id contractId {get; set;}
        public String orderGroup {get; set;}
    }
    
    public class OrderWrapper {
        public String orderType {get; set;}
        public String customType {get; set;}
        public String status {get; set;}
        public Id quoteId {get; set;}
        public Id accountId {get; set;}
        public Id contractId {get; set;}
        public Id opportunityId {get; set;}
        public Id pricebookId {get; set;}
        public Boolean noClawback {get;set;}
    }
}