/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*	This is an invocable method used in "Quote Management" Process Builder 
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Clarissa De Feo   <cdefeo@deloitte.it>
* @created        2020-08-07
* @systemLayer    Invocation
* @TestClass	Test_TryToUpdateQuoteStatusInAccepted
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Name LastName
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class TryToUpdateQuoteStatusInAccepted {
    @InvocableMethod(label='tryToUpdateQuoteStatusInAccepted' description='It tries to update the quote status in "Accepted"')
    public static void tryToUpdateQuoteStatusInAccepted(List<SBQQ__Quote__c> quotes)
    {	
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
        
        System.debug('quotes ---> '+JSON.serialize(quotes));
        
        if(quotes != NULL && quotes.size() > 0){
            for(SBQQ__Quote__c quote : quotes){
                SBQQ__Quote__c quoteToUpdate = new SBQQ__Quote__c();
                quoteToUpdate.Id = quote.Id;
                quoteToUpdate.SBQQ__Status__c = ConstantsUtil.Quote_Status_Accepted;
                quotesToUpdate.add(quoteToUpdate);
            }
        }
        
        System.debug('quotes 2 ---> '+JSON.serialize(quotes));
        
        List<Database.SaveResult> srs;
        
        if(quotesToUpdate.size() > 0)
            srs = Database.update(quotesToUpdate, false);
        
        System.debug('result quote list---> '+ srs[0].isSuccess());    
    }
    
}