/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-20-2019
 * Sprint      : 3
 * Work item   : SF2-88 Contract Renewal - US_65 Contract automatically renewed at subscription end date (implicit)
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

@isTest
private class Test_UpdateNextRenewalDate {
    
	@testSetup
    static void setup() {
        Account acct = Test_DataFactory.createAccounts('AccountTest', 1)[0];
        insert acct;
        
        Opportunity oppt = Test_DataFactory.createOpportunities('OppTest','Qualification',acct.Id,1)[0];
        oppt.Pricebook2Id = Test.getStandardPricebookId();
        insert oppt;
        
        List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',2);
        places[0].Account__c = acct.id;
        places[1].Account__c = acct.id;
        
        insert places;
        
        Swisslist_CPQ.config();
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        Contract cont = Test_DataFactory.getContracts(acct, ConstantsUtil.CONTRACT_STATUS_DRAFT, 1)[0];
        cont.StartDate = Date.today();
        cont.SBQQ__Evergreen__c = true;
        cont.In_Termination__c = false;
        cont.nextRenewalDate__c = Date.today().addDays(1);
        cont.SBQQ__Opportunity__c = oppt.Id;
        insert cont;
        
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        SBQQ__Subscription__c subStarter = Test_DataFactory.getSubscriptions(acct, places[0], cont, new List<Product2>{products.get(productIdByCode.get('SLT001'))}, 1)[0];
        subStarter.Subscription_Term__c = products.get(productIdByCode.get('SLT001')).Subscription_Term__c;
        subStarter.SBQQ__Quantity__c = 1;
        subStarter.In_Termination__c = false;
        subStarter.Status__c = 'Active';
        
        SBQQ__Subscription__c subStandard = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('SLS001'))}, 1)[0];
        subStandard.Subscription_Term__c = products.get(productIdByCode.get('SLS001')).Subscription_Term__c;
        subStandard.SBQQ__Quantity__c = 1;
        subStandard.In_Termination__c = false;
        subStandard.Status__c = 'Active';
        
        subs.add(subStarter);
        subs.add(subStandard);
        
        insert subs;
        subs.clear();
        
        SBQQ__Subscription__c subA1 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('ONAP001'))}, 1)[0];
        subA1.Subscription_Term__c = products.get(productIdByCode.get('ONAP001')).Subscription_Term__c;
        subA1.SBQQ__Quantity__c = 1;
        subA1.In_Termination__c = false;
        subA1.SBQQ__RequiredById__c = subStandard.Id;
        subA1.Status__c = 'Active';
        
        subs.add(subA1);
        
        SBQQ__Subscription__c subA2 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('ORER001'))}, 1)[0];
        subA2.Subscription_Term__c = products.get(productIdByCode.get('ORER001')).Subscription_Term__c;
        subA2.SBQQ__Quantity__c = 1;
        subA2.In_Termination__c = false;
        subA2.SBQQ__RequiredById__c = subStandard.Id;
        subA2.Status__c = 'Active';
        
        subs.add(subA2);
        
        SBQQ__Subscription__c subA3 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('OURL001'))}, 1)[0];
        subA3.Subscription_Term__c = products.get(productIdByCode.get('OURL001')).Subscription_Term__c;
        subA3.SBQQ__Quantity__c = 1;
        subA3.In_Termination__c = false;
        subA3.SBQQ__RequiredById__c = subStandard.Id;
        subA3.Status__c = 'Active';
        
        subs.add(subA3);
        
        SBQQ__Subscription__c subA4 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('OREV001'))}, 1)[0];
        subA4.Subscription_Term__c = products.get(productIdByCode.get('OREV001')).Subscription_Term__c;
        subA4.SBQQ__Quantity__c = 1;
        subA4.In_Termination__c = false;
        subA4.SBQQ__RequiredById__c = subStandard.Id;
        subA4.Status__c = 'Active';
        
        subs.add(subA4);
        
        SBQQ__Subscription__c subA5 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('OPMBASIC001'))}, 1)[0];
        subA5.Subscription_Term__c = products.get(productIdByCode.get('OPMBASIC001')).Subscription_Term__c;
        subA5.SBQQ__Quantity__c = 1;
        subA5.In_Termination__c = false;
        subA5.SBQQ__RequiredById__c = subStandard.Id;
        subA5.Status__c = 'Active';
        
        subs.add(subA5);
        
        SBQQ__Subscription__c subA6 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('ONAP001'))}, 1)[0];
        subA6.Subscription_Term__c = products.get(productIdByCode.get('ONAP001')).Subscription_Term__c;
        subA6.SBQQ__Quantity__c = 1;
        subA6.In_Termination__c = false;
        subA6.SBQQ__RequiredById__c = subStarter.Id;
        subA6.Status__c = 'Active';
        
        subs.add(subA6);
        
        SBQQ__Subscription__c subA7 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('ORER001'))}, 1)[0];
        subA7.Subscription_Term__c = products.get(productIdByCode.get('ORER001')).Subscription_Term__c;
        subA7.SBQQ__Quantity__c = 1;
        subA7.In_Termination__c = false;
        subA7.SBQQ__RequiredById__c = subStarter.Id;
        subA7.Status__c = 'Active';
        
        subs.add(subA7);
		
		SBQQ__Subscription__c subA8 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('OREV001'))}, 1)[0];
        subA8.Subscription_Term__c = products.get(productIdByCode.get('OREV001')).Subscription_Term__c;
        subA8.SBQQ__Quantity__c = 1;
        subA8.In_Termination__c = false;
        subA8.SBQQ__RequiredById__c = subStarter.Id;
        subA8.Status__c = 'Active';
        
        subs.add(subA8);
        
        SBQQ__Subscription__c subA9 = Test_DataFactory.getSubscriptions(acct, places[1], cont, new List<Product2>{products.get(productIdByCode.get('OPMBASICLOW001'))}, 1)[0];
        subA9.Subscription_Term__c = products.get(productIdByCode.get('OPMBASICLOW001')).Subscription_Term__c;
        subA9.SBQQ__Quantity__c = 1;
        subA9.In_Termination__c = false;
        subA9.SBQQ__RequiredById__c = subStarter.Id;
        subA9.Status__c = 'Active';
        
        subs.add(subA9);
        
        insert subs;
        
        cont.Status = 'Active';
        update cont;
    }
    
    static testmethod void test_UpdateNextRenewalDate(){
        Test.startTest();
        	Contract cont = [SELECT Id, Call_API__c FROM Contract LIMIT 1];
        	cont.Call_API__c = true;
        	update cont;
        Test.stopTest();
        
        Contract afterUpdContract = [SELECT Id, StartDate, nextRenewalDate__c FROM Contract LIMIT 1];
        SBQQ__Subscription__c sub = [SELECT Id, Subscription_Term__c FROM SBQQ__Subscription__c WHERE Place__c != NULL LIMIT 1];
        System.assertEquals(afterUpdContract.StartDate.addMonths(Integer.valueOf(sub.Subscription_Term__c)), afterUpdContract.nextRenewalDate__c);
        
    }
}