@isTest
public class PlaceProductInfoAccountsPBTest {

     @isTest
    static void testProductInfoSLT(){
        try{
        Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='Dhaka';    
                       
            insert acc;
        System.debug('Account' +acc);
        

        	Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'Dhaka';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '101';
            place.Account__c = acc.Id;

        	Place.Product_Information__c='SLT001';
            insert place;

            System.debug('Place: '+ place);

            Account acc1 = [Select Swiss_List_Candidate__c from Account];
           system.debug('Swiss' +acc1);
    }
        catch(Exception e)
        {
            System.assert(false, e.getMessage());
        }
        
    }
    
    @isTest
    static void testProductInfoSLS(){
        try{
        Account acc2 = new Account();
            acc2.Name = 'test acc2';
            acc2.GoldenRecordID__c = 'GRID2';       
            acc2.POBox__c = '102';
            acc2.P_O_Box_Zip_Postal_Code__c ='101';
            acc2.P_O_Box_City__c ='Dhaka2';    
                       
            insert acc2;
        System.debug('Account' +acc2);
        

        	Place__c place2 = new Place__c();
            place2.Name = 'test place2';
            place2.LastName__c = 'test last2';
            place2.Company__c = 'test company2';
            place2.City__c = 'Dhaka2';
            place2.Country__c = 'test coountry2';
            place2.PostalCode__c = '1012';
            place2.Account__c = acc2.Id;

        	Place2.Product_Information__c='SLS001';
            insert place2;
            System.debug('Place2: '+ place2);
            Account acc3 = [Select Swiss_List_Candidate__c from Account limit 1];
           system.debug('Swiss2' +acc3);
    }
        catch(Exception e)
        {
            System.assert(false, e.getMessage());
        }
        
    }
    
     @isTest
    static void testProductInfoNull(){
        try{
        Account acc4 = new Account();
            acc4.Name = 'test acc3';
            acc4.GoldenRecordID__c = 'GRID3';       
            acc4.POBox__c = '103';
            acc4.P_O_Box_Zip_Postal_Code__c ='1013';
            acc4.P_O_Box_City__c ='Dhaka23';    
                      
            insert acc4;
        System.debug('Account' +acc4);
        

        	Place__c place4 = new Place__c();
            place4.Name = 'test place23';
            place4.LastName__c = 'test last23';
            place4.Company__c = 'test company23';
            place4.City__c = 'Dhaka23';
            place4.Country__c = 'test coountry23';
            place4.PostalCode__c = '1012';
            place4.Account__c = acc4.Id;

            insert place4;
            System.debug('Place4: '+ place4);
            Account acc5 = [Select Swiss_List_Candidate__c from Account limit 1];
           system.debug('Swiss3' +acc5);
    }
        catch(Exception e)
        {
            System.assert(false, e.getMessage());
        }
        
    }
    
}