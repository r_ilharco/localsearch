global class Service_Duplicate_Management {
@future
	    public static void deleteOpportunity(List<Id> opportunityId)
	    {  
            List<Opportunity> oppList = new List<Opportunity>();
            oppList =  [select id from Opportunity where id in :opportunityId];
            if(oppList.size()>0){
                delete oppList;
            }
	    }
}