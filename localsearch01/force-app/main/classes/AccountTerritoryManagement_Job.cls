global class AccountTerritoryManagement_Job implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(system.now().minute()+1);
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'AccountTerritoryManagement_Batch%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('AccountTerritoryManagement_Batch0')){
                AccountTerritoryManagement_Batch b0 = new AccountTerritoryManagement_Batch(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('AccountTerritoryManagement_Batch0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch1')){ 
                AccountTerritoryManagement_Batch b1 = new AccountTerritoryManagement_Batch(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch2')){
                AccountTerritoryManagement_Batch b2 = new AccountTerritoryManagement_Batch(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch3')){
                AccountTerritoryManagement_Batch b3 = new AccountTerritoryManagement_Batch(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch4')){
                AccountTerritoryManagement_Batch b4 = new AccountTerritoryManagement_Batch(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch5')){
                AccountTerritoryManagement_Batch b5 = new AccountTerritoryManagement_Batch(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch6')){
                AccountTerritoryManagement_Batch b6 = new AccountTerritoryManagement_Batch(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch7')){
                AccountTerritoryManagement_Batch b7 = new AccountTerritoryManagement_Batch(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch8')){
                AccountTerritoryManagement_Batch b8 = new AccountTerritoryManagement_Batch(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('AccountTerritoryManagement_Batch9')){
                AccountTerritoryManagement_Batch b9 = new AccountTerritoryManagement_Batch(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('AccountTerritoryManagement_Batch9', strSchedule,b9);
            }
        }
    }
}