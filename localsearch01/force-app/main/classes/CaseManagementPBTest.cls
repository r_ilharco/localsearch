@isTest
public class CaseManagementPBTest {
    
    @isTest
    static void testCaseManagemnt(){
        
        Group grp= new Group();
        grp.Name='Controlling';
        //grp.DeveloperName='Controlling';
        grp.Type='Queue';
        
        insert grp;
        
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue = new QueueSObject(QueueID = grp.id, SObjectType = 'Case' );
            
            insert testQueue;
        }
        
        
        Contact contactt= CreateContact();
        
        
        Case cs =  new Case();
        cs.RecordTypeId = '0121r000000xEFIAA2';
        cs.SuppliedEmail = 'iqbal.rocky@arollotech.com';
        cs.Origin = 'Web';
        cs.OwnerId= grp.Id; 
        cs.Topic__c= 'Sales Controlling';
        insert cs;
        update cs;
        
        
        System.debug('case: '+cs);	
        
    }
    
    
    @isTest
    static void testCaseNewReq(){
        
        Contact contactt= CreateContact();
        Contact contacttrd= CreateContactRD();
        
        
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.RL__c=contactt.Id;
        contactt1.RD__c=contacttrd.Id; 
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        Case cs1= new Case();
        cs1.RecordTypeId = '0121r000000xEFIAA2';
        cs1.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs1.Origin = 'Web';
        cs1.Topic__c= 'Sales Controlling';
        cs1.RL_in_CC__c = True;	
        cs1.RD_in_CC__c = true;
        cs1.ContactId=contactt1.Id;
        
        insert cs1;
        system.debug('Case1' +cs1);
        
        
    }
    
    
    
    @isTest
    static void testAssignToCSC(){
        
        Case cs3= new Case();
        cs3.RecordTypeId = '0121r000000xEFIAA2';
        cs3.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs3.Topic__c= 'Billing';
        cs3.Sub_Topic__c ='Billing Request';
        
        insert cs3;
        system.debug('Case2' +cs3);
        
    }
    
    
    @isTest
    static void testAssignCSCOwner(){
        
        Group grp1= new Group();
        grp1.Name='Controlling';
        //grp1.DeveloperName='Controlling1';
        grp1.Type='Queue';
        
        insert grp1;
        system.debug('Group' +grp1);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue1 = new QueueSObject(QueueID = grp1.id, SObjectType = 'Case' );
            
            insert testQueue1;
        }
        
        
        Group grp2= new Group();
        grp2.Name='CSC'; 
        //grp2.DeveloperName='CSC';
        grp2.Type='Queue';
        
        insert grp2;
        system.debug('Group' +grp2);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue2 = new QueueSObject(QueueID = grp2.Id, SObjectType = 'Case' );
            
            insert testQueue2;
        }
        
        
        
        Case cs4= new Case();
        cs4.RecordTypeId = '0121r000000xEFIAA2';
        cs4.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs4.OwnerId=grp1.Id;
        //cs4.Topic__c= 'Billing';
        
        //cs4.Sub_Topic__c ='Billing Request';
        
        insert cs4;
        
        system.debug('Case4' +cs4);
        cs4.OwnerId=grp2.Id;
        update cs4;
        //case ccc= [Select Status from case];
        // system.debug('Status:::::' +ccc);
        
    }
    
    
    @isTest
    static void testAssignSalseChatCR()
    {
        Case cs5= new Case();
        cs5.RecordTypeId = '0121r000000xEFIAA2';
        cs5.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs5.Topic_Sales__c='Contract_Request';
        
        insert cs5;
        
        system.debug('Case5' +cs5);
        
        
    }
    
    @isTest
    static void testAssignSalseChatCCR()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic_Sales__c='Customer_Center_Request';
        
        insert cs6;
        system.debug('Case6' +cs6);
        
        
        
    }
    
    @isTest
    static void testAssignSalseChatPR()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic_Sales__c='Product_Request';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    }
    
    @isTest
    static void testAssignSalseChatTWMR()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic_Sales__c='Tools_Web_Mobile_Request';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    }
    
    @isTest
    static void testAssignSalseChatOther()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic_Sales__c='Other';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    }
    
    @isTest
    static void testAssignSalseChatCRSales()
    {
        Case cs5= new Case();
        cs5.RecordTypeId = '0121r000000xEFIAA2';
        cs5.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs5.Topic__c='Contract_Request';
        
        insert cs5;
        
        system.debug('Case5' +cs5);
        
        
    }
    
    @isTest
    static void testAssignSalseChatCCRSales()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Customer_Center_Request';
        
        insert cs6;
        system.debug('Case6' +cs6);
        
        
        
    }
    
    @isTest
    static void testAssignSalseChatPRSales()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Product_Request';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    }
    
    @isTest
    static void testAssignSalseChatTWMRSales()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Tools_Web_Mobile_Request';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    }
    
    @isTest
    static void testAssignSalseChatOtherSales()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Other';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    }  
    
    @isTest
    static void testAssignSalseChatLastOneSales()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Client Base edit';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    } 
    
    @isTest
    static void testAssignKAM()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.Topic__c='Client Base edit';
        cs6.KAM_Customer__c='YES';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        

    } 
    
    @isTest
    static void testGEWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='German';
 
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
   
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
  
    } 
    
    @isTest
    static void testGENoWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='German';
        
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    } 
    
    @isTest
    static void testGEKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='German';
        
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        cs6.Topic__c='Client Base edit';
        cs6.KAM_Customer__c='YES';
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    } 
    
    
    @isTest
    static void testFRWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='French';
        
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    } 
    
    @isTest
    static void testFRNoWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='French';
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    } 
    
    @isTest
    static void testFRKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='French';
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        cs6.Topic__c='Client Base edit';
        cs6.KAM_Customer__c='YES';
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    } 
    
    
    
    @isTest
    static void testITWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='Italian';
        
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    } 
    
    @isTest
    static void testITNoWorkNotKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='Italian';
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    } 
    
    @isTest
    static void testITKAM()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='Italian';
        
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        
        
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs6.ContactId= contactt1.Id;
        cs6.Topic__c='Client Base edit';
        cs6.KAM_Customer__c='YES';
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
        
    } 
    
    
    @isTest
    static void testGeOwnerChanged()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='German';
        //contactt1.RL__c=contactt.Id;
        //contactt1.RD__c=contacttrd.Id; 
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        Group grp1= new Group();
        grp1.Name='Controlling';
        //grp1.DeveloperName='Controlling1';
        grp1.Type='Queue';
        
        insert grp1;
        system.debug('Group' +grp1);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue1 = new QueueSObject(QueueID = grp1.id, SObjectType = 'Case' );
            
            insert testQueue1;
        }
        
        
        Group grp2= new Group();
        grp2.Name='CSC'; 
        //grp2.DeveloperName='CSC';
        grp2.Type='Queue';
        
        insert grp2;
        system.debug('Group' +grp2);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue2 = new QueueSObject(QueueID = grp2.Id, SObjectType = 'Case' );
            
            insert testQueue2;
        }
        
        
        Case cs4= new Case();
        cs4.RecordTypeId = '0121r000000xEFIAA2';
        cs4.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs4.OwnerId=grp1.Id;
        
        
        insert cs4;
        system.debug('Case4' +cs4);
        
        
        
        Case cs= new Case();
        cs.RecordTypeId = '0121r000000xEFIAA2';
        cs.SuppliedEmail = 'iqbal@gmail.com';
        //cs.OwnerId=grp1.Id;
        
        
        insert cs;
        system.debug('Case' +cs);
        
        cs4.OwnerId=grp2.Id;
        update cs4;
        
        cs4.ContactId= contactt1.Id;
        update cs4;
        
        
    }
    
    
    @isTest
    static void testFROwnerChanged()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='French';
        //contactt1.RL__c=contactt.Id;
        //contactt1.RD__c=contacttrd.Id; 
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        Group grp1= new Group();
        grp1.Name='Controlling';
        //grp1.DeveloperName='Controlling1';
        grp1.Type='Queue';
        
        insert grp1;
        system.debug('Group' +grp1);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue1 = new QueueSObject(QueueID = grp1.id, SObjectType = 'Case' );
            
            insert testQueue1;
        }
        
        
        Group grp2= new Group();
        grp2.Name='CSC'; 
        //grp2.DeveloperName='CSC';
        grp2.Type='Queue';
        
        insert grp2;
        system.debug('Group' +grp2);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue2 = new QueueSObject(QueueID = grp2.Id, SObjectType = 'Case' );
            
            insert testQueue2;
        }
        
        
        Case cs4= new Case();
        cs4.RecordTypeId = '0121r000000xEFIAA2';
        cs4.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs4.OwnerId=grp1.Id;
        
        
        insert cs4;
        system.debug('Case4' +cs4);
        
        
        
        Case cs= new Case();
        cs.RecordTypeId = '0121r000000xEFIAA2';
        cs.SuppliedEmail = 'iqbal@gmail.com';
        //cs.OwnerId=grp1.Id;
        
        
        insert cs;
        system.debug('Case' +cs);
        
        cs4.OwnerId=grp2.Id;
        update cs4;
        
        cs4.ContactId= contactt1.Id;
        update cs4;
        
        
    }
    
    @isTest
    static void testITOwnerChanged()
    {
        Contact contactt1= New Contact();
        
        contactt1.Phone='01711111111';
        contactt1.Email='iqbalbdrocky@gmail.com';
        //contactt.AccountId= accountt.Id;
        contactt1.PO_Box__c= 'Ramna';
        contactt1.PO_BoxCity__c= 'Dhaka'; 
        contactt1.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt1.LastName = 'Test';
        contactt1.FirstName='Rock';
        contactt1.Language__c='Italian';
        
        insert contactt1;  
        System.debug('contact1: '+contactt1);	
        
        Group grp1= new Group();
        grp1.Name='Controlling';
        grp1.Type='Queue';
        
        insert grp1;
        system.debug('Group' +grp1);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue1 = new QueueSObject(QueueID = grp1.id, SObjectType = 'Case' );
            
            insert testQueue1;
        }
        
        
        Group grp2= new Group();
        grp2.Name='CSC'; 
        grp2.Type='Queue';
        
        insert grp2;
        system.debug('Group' +grp2);
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueueSObject testQueue2 = new QueueSObject(QueueID = grp2.Id, SObjectType = 'Case' );
            
            insert testQueue2;
        }
        
        
        Case cs4= new Case();
        cs4.RecordTypeId = '0121r000000xEFIAA2';
        cs4.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        cs4.OwnerId=grp1.Id;
        
        
        insert cs4;
        system.debug('Case4' +cs4);
        
        
        
        Case cs= new Case();
        cs.RecordTypeId = '0121r000000xEFIAA2';
        cs.SuppliedEmail = 'iqbal@gmail.com';
        
        
        insert cs;
        system.debug('Case' +cs);
        
        cs4.OwnerId=grp2.Id;
        update cs4;
        
        cs4.ContactId= contactt1.Id;
        update cs4;
        
        
    }
    
    @isTest
    static void testAssignFeedback()
    {
        Case cs6= new Case();
        cs6.RecordTypeId = '0121r000000xEFIAA2';
        cs6.SuppliedEmail = 'iqbalbdrocky@gmail.com';
        
        cs6.Topic__c='Feedback Service Cloud';
        
        
        insert cs6;
        
        system.debug('Case6' +cs6);
        
        
    }     
    
    
    
    @isTest
    static Contact CreateContact(){
        
        Contact contactt= New Contact();
        
        contactt.Phone='01700000000';
        contactt.Email='iqbal.rocky@arollotech.com';
        contactt.PO_Box__c= 'Ramna';
        contactt.PO_BoxCity__c= 'Dhaka'; 
        contactt.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contactt.LastName = 'Test';
        contactt.FirstName='Chu';
        contactt.Email='iqbalrocky91@gmail.com';
        contactt.Position__c= 'Regionalleiter';
        
        
        
        insert contactt;  
        System.debug('contact: '+contactt);	
        return contactt;
        
    }
    
    
    @isTest
    static Contact CreateContactRD(){
        
        Contact contacttrd= New Contact();
        
        contacttrd.Phone='01722222222';
        contacttrd.Email='iqbal.rocky@arollotech.com';
        contacttrd.PO_Box__c= 'Ramna';
        contacttrd.PO_BoxCity__c= 'Dhaka'; 
        contacttrd.PO_BoxZip_PostalCode__c	='Dhaka-1000';
        contacttrd.LastName = 'Test';
        contacttrd.FirstName='Churl';
        contacttrd.Email='iqbalrocky91@gmail.com';
        contacttrd.Position__c= 'Regionaldirektor';
        
        
        
        insert contacttrd;  
        System.debug('contacttrd: '+contacttrd);	
        return contacttrd;
        
    }
    
}