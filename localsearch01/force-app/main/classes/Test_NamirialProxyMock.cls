@isTest
global class Test_NamirialProxyMock {
	public static HttpResponseMockProxy buildMockProxy() {
        Map<String,StaticResource> staticResourcesMap = new Map<String,StaticResource>();
        List<String> staticResourceNamesToquery = new List<String>{ConstantsUtil.GETENVELOPEBYID_RESPONSE_NAMIRIAL_JSON_SR,
        																			ConstantsUtil.DOWNLOADCOMPLETEDDOCUMENT_RESPONSE_NAMIRIAL_JSON_SR,
        																			ConstantsUtil.UPLOADTEMPORARYFILE_AND_PREPARESENDENVELOPE_RESPONSE_NAMIRIAL_JSON_SR,
        																			ConstantsUtil.SENDENVELOPE_RESPONSE_NAMIRIAL_JSON_SR};
                                                                                        
        List<StaticResource> staticResources = new List<StaticResource>();
        staticResources = [SELECT Id, Name, Body FROM StaticResource WHERE Name IN :staticResourceNamesToquery];
        
        if(staticResources.size() > 0)
        {
            for(StaticResource sr : staticResources){
                System.debug('Name: ' + sr.Name + 'Content: ' + sr.Body.toString());
                staticResourcesMap.put(sr.Name, sr);
            }
        }
                                                                                        
        HttpResponseMockProxy proxy = new HttpResponseMockProxy();
        proxy.mocks = new Map<String, GenericMock> {
            'digital-signature/v2/envelopes' => new GenericMock(new Map<Integer, Map<String,String>> {
                200 => new Map<String,String>{ 'Generic' => '{"envelope_id" : "envelopeId", "workstep_redirection_url" : "redirectionUrl"}'},
                401 => new Map<String,String>{ 'Generic' => 'Unauthorized' }
            })
        };
        return proxy;
    }
    
	public class HttpResponseMockProxy implements HttpCalloutMock {
        //public Map<string, GenericMock> mocks;
        public Map<String, GenericMock> mocks;
        
        public HTTPResponse respond(HTTPRequest req) {
            List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];

            Mashery_Setting__c c2;
            if(!p.isEmpty()){
                c2 = Mashery_Setting__c.getInstance(p[0].Id);    
            } else{
                c2 = Mashery_Setting__c.getOrgDefaults();
            }
            
            system.debug('REQ: '+ req);
            String methodName = req.getEndpoint().substring(req.getEndpoint().indexOfIgnoreCase(Test_NamirialUtility.endpoint)+Test_NamirialUtility.endpoint.length());
			system.debug('methodName: '+ methodName);
            GenericMock mock;
            if(mocks != null) {
                mock = mocks.get(methodName);
            } 
            if(mock == null) {
                HttpResponse res = new HttpResponse();
                res.setStatusCode(404);
                return res;
            }
            return mock.respond(req);
        }
    }

    public class GenericMock implements HttpCalloutMock {
        public integer mockedResponse = 200;
        public String responseType = 'Generic';
        
        public Map<Integer, Map<String,String>> responses;
        
        public GenericMock(Map<Integer, Map<String,String>> responses) {
            this.responses = responses;
        }
        
        public virtual HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HTTPResponse();
            string responseBody = responses.get(mockedResponse).get(responseType);
            if(responseBody == null) {
                res.setStatusCode(404);
                return res;
            }
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(mockedResponse);
            res.setBody(responseBody);
            return res;
        }
    }
}