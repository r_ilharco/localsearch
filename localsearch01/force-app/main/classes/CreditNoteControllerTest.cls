@isTest
public class CreditNoteControllerTest {
    
    @isTest
    public static void testGetDefaultCreditNoteValues(){
        
        try{
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            
            insert place;
            System.debug('Place: '+ place);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
                       
            insert acc;
            System.Debug('Account: '+ acc); 
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
            
            Billing_Profile__c billProfile = new Billing_Profile__c();
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'test';   
            billProfile.Name = 'Test Bill Prof Name';
    
            insert billProfile;
            System.Debug('Bill Profile: '+ billProfile);
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            
            insert myContract;
            System.debug('Contract: '+ myContract);       
            
            Invoice__c invoice = new Invoice__c();
            invoice.Customer__c = acc.Id;
            invoice.Billing_Profile__c = billProfile.Id;
            invoice.Invoice_Code__c = 'FAK';
            invoice.Process_Mode__c = 'Standard0';
            invoice.Status__c = 'Collected';
            invoice.Amount__c = 230;
            
            insert invoice;
            System.debug('invoice: '+ invoice);
            
            Invoice_Order__c invoiceOrder = new Invoice_Order__c();
            invoiceOrder.Invoice__c = invoice.Id;
            invoiceOrder.Contract__c = myContract.Id;
            
            insert invoiceOrder;
            System.debug('Invoice Order: '+ invoiceOrder);
                                  
            SBQQ__Subscription__c subs = new SBQQ__Subscription__c();
            subs.SBQQ__Account__c = acc.Id;
            subs.Place__c =place.Id;
            subs.SBQQ__Contract__c = myContract.Id;
            subs.SBQQ__Quantity__c = 2;
            
            insert subs;
            System.debug('Subs: '+ subs);
            
            Credit_Note__c creditNote = new Credit_Note__c();
            creditNote.Account__c = acc.Id;
            creditNote.Reimbursed_Invoice__c = invoice.Id;
            creditNote.Contract__c = myContract.Id;
            creditNote.Value__c = 100;
            creditNote.Execution_Date__c = Date.today();
            
            insert creditNote;
            System.debug('Credit Note: '+ creditNote);
            
            Test.startTest();
            
            CreditNoteController.getDefaultCreditNoteValues(invoice.id);
                       
            invoice.Invoice_Code__c = 'GUT';
            update invoice;
            CreditNoteController.getDefaultCreditNoteValues(invoice.id);
            
            invoice.Invoice_Code__c = 'FAK';
            invoice.Process_Mode__c = 'ModeSwissList';
            update invoice;
            CreditNoteController.getDefaultCreditNoteValues(invoice.id);
            
            invoice.Invoice_Code__c = 'FAK';
            invoice.Process_Mode__c = 'Standard0';
            invoice.Status__c = 'Draft';
            update invoice;
            CreditNoteController.getDefaultCreditNoteValues(invoice.id);
            
            invoice.Billing_Profile__c = null;
            update invoice;
            CreditNoteController.getDefaultCreditNoteValues(invoice.id);
            
            Test.stopTest();
            
        }catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
                     
    }

}