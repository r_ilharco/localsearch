/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Package     : 
 * Description : TestClass for Lead Trigger (Handler and Helper)
 * Changelog   : 
 */

@isTest
public class Test_LeadTrigger {
	
    @testSetup
    public static void test_setupData(){
       
        LeadTriggerHandler.disableTrigger = true;      
       
        Territory2 territory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id != NULL LIMIT 1];
        
        UserTerritory2Association userTerritoryAssociation = new UserTerritory2Association();
        userTerritoryAssociation.Territory2Id = territory.Id;
        userTerritoryAssociation.UserId= UserInfo.getUserId();
		userTerritoryAssociation.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
		insert userTerritoryAssociation;
        
        LeadTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_leadTrigger(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        List<Lead> leadsToInsert = Test_DataFactory.generateLeads('Lead First Name', 'Lead Last Name', 2);
        leadsToInsert[0].LeadSource = ConstantsUtil.LEAD_SOURCE_MANUAL_Sales;
        leadsToInsert[0].Rating = ConstantsUtil.LEAD_RATING_COLD;
        leadsToInsert[0].UID__c = 'CH-123456';
        leadsToInsert[0].Phone = '020304050';
        leadsToInsert[0].Email = 'testmail@leadtrigger.com';
        leadsToInsert[1].LeadSource = ConstantsUtil.LEAD_SOURCE_MANUAL_Sales; 
        leadsToInsert[1].Rating = ConstantsUtil.LEAD_RATING_HOT;

        insert leadsToInsert;
        
        leadsToInsert[0].PostalCode = '8011';
        leadsToInsert[0].AddressValidationStatus__c = ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED;
        update leadsToInsert[0];
        
        delete leadsToInsert;
        undelete leadsToInsert;
        Test.stopTest();
    }
}