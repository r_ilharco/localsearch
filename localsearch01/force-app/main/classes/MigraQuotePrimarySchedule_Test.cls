@isTest
public class MigraQuotePrimarySchedule_Test {
    
    @isTest
	public static void MigraQuotePrimarySchedule_test()
    { 
    	Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        String jobId = System.schedule('Sample_Heading', CRON_EXP, new MigraQuotePrimarySchedule('B1',1) );   
        Test.stopTest();
    }
}