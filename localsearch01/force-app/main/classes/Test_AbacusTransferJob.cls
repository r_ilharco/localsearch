@isTest
public class Test_AbacusTransferJob{
    @testSetup
    public static void setup(){
        Vat_Schedule__c schedule = new Vat_Schedule__c(Start_Date__c = null, Value__c = 7.7, End_Date__c = null, Tax_Code__c = 'Standard');
		insert schedule;
        List<Account> customers = Test_DataFactory.generateAccounts('testCustomer', 3,false);
        insert customers;
        List<Invoice__c> invoices = Test_DataFactory.createInvoices(3, 'collected', 'standard0', customers);
        for(Invoice__c inv : invoices){
            inv.Total__c = 200;
            inv.Booking_Status__c = ConstantsUtil.ABACUS_STATUS_DRAFT;
        }
        insert invoices;
        List<Invoice_Order__c> invoiceOrders = new List<Invoice_Order__c>();
        for(Invoice__c invoice : invoices){
            invoiceOrders.addAll(Test_DataFactory.createInvoiceOrders(5,invoice.id));
        }
        insert invoiceOrders;
        List<Invoice_Item__c> invoiceItems = new List<Invoice_Item__c>();
        for(Invoice_Order__c invoiceOrder : invoiceOrders){
        	invoiceItems.addAll(Test_DataFactory.createInvoiceItems(2, invoiceOrder.Id));
        }
        for(Invoice_Item__c item : invoiceItems){
            item.Tax_Code__c = 'Standard';
            item.Accrual_To__c = Date.today().addDays(1);
            item.Accrual_From__c = Date.today().addDays(3);
            item.Total__c = 50;
            item.Amount__c = 1;
        }
        insert invoiceItems;
    }

    @isTest
    public static void test_SEL_Inv(){
        Test.startTest();        
        Invoice__c inv = [SELECT Id FROM Invoice__c LIMIT 1];
        SEL_Invoices.getInvoicesByContractIDsAndStus(new Set<Id>{inv.Id}, new List<String>{'Active'});
        SEL_Invoices.getInvoicesById(new Set<Id>{inv.Id});
        Test.stopTest();
    }
    
	@isTest
    public static void test_abacusTransferJobInvoiceCallout(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AbacusTransferHttpCalloutMock()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock
        Test.startTest();
        AbacusTransferJob atj = new AbacusTransferJob();
        Id batchId = Database.executeBatch(atj);
        Test.stopTest();
    }
    
    @isTest
    public static void test_SchedAbacusTransferJobInvoiceCallout(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AbacusTransferHttpCalloutMock()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock
        Test.startTest();
        AbacusTransferJob atj = new AbacusTransferJob();
        String CRON_EXP = '0 0 0 3 9 ? 2032';
        String jobId = System.schedule('BorrarAseguradosTest', CRON_EXP, new AbacusTransferJob());
        
        
        Test.stopTest();
    }
    
    @isTest
    public static void test_SRV_ABACUS(){
        Test.startTest();
        AbacusHttpResponseMock jsonResponse = new AbacusHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        
        SRV_Abacus.postCallout('test','test','test');
        Test.stopTest();
    }
    
    public class AbacusHttpResponseMock implements HttpCalloutMock {
        public string invoiceDocId = 'invoice-id';
       
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            if(req.getEndpoint().contains('token')){
                Map<string, object> responseMap = new Map<string, object> {
                    'access_token' => '123456789',
                        'expires_in' => '3600',
                        'token_type' => 'bearer'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
            }else{
                Map<string, object> responseMap = new Map<string, object> {
                    'open_amount' => '15',
                        'total_amount_incl_tax' => '15'
                        };
                            res.setHeader('Content-Type', 'application/json');
                res.setBody(JSON.serialize(responseMap));
                res.setStatusCode(200);
            }
            
            
            
                        
            return res;
        }
    }
}