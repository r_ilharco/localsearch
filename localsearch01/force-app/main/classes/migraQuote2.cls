global class migraQuote2 implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    public Map<ID, List<Place__c>> plAcc;
    public Map<ID, Boolean> plUsed;
    public Map<String, Id> prodIds;
    public String priceId;
	public Map<ID, Integer> plExtraRub;
    public Map<ID, Integer> plExtraZeh;
    public Map<String, Id> pfId;
    public Map<String, Id> pOppId;
    public Map<String, Id> pPbeId;
    public string batch;
    
    
    public migraQuote2(string batchName){
        plUsed = new Map<ID, Boolean>();
        prodIds = new Map<String, Id>();
        plAcc = new Map<ID, List<Place__c>>();
        plExtraRub = new Map<ID, Integer>();
        plExtraZeh = new Map<ID, Integer>();
        pfId = new Map<String, Id>();
        pOppId = new Map<String, Id>();
        pPbeId = new Map<String, Id>();
		batch = batchName;

            
       	List<SBQQ__ProductFeature__c> pf = [Select id,Name,SBQQ__ConfiguredSKU__c FROM SBQQ__ProductFeature__c limit 100];
        
        for(SBQQ__ProductFeature__c p : pf){
            pfId.put(p.SBQQ__ConfiguredSKU__c + p.Name, p.id);
        } 
        
        List<Product2> products = [Select id,ProductCode FROM Product2 limit 100];
        
        for(Product2 p : products){
            prodIds.put(p.ProductCode, p.id);
        } 
        
        List<SBQQ__ProductOption__c> lstPop = [SELECT id,SBQQ__ConfiguredSKU__c,SBQQ__ProductCode__c FROM SBQQ__ProductOption__c limit 100 ];
        
		for(SBQQ__ProductOption__c pOpp : lstPop){
            pOppId.put(pOpp.SBQQ__ConfiguredSKU__c + pOpp.SBQQ__ProductCode__c, pOpp.id);
        } 
        
        
        List<PricebookEntry> lstPbe = [select Id, ProductCode from PricebookEntry limit 100];
        for(PricebookEntry pbe : lstPbe){
            pPbeId.put(pbe.ProductCode, pbe.id);
        } 
        
        
        Id priceId = '01s1r000003jhYBAAY'; //[Select id FROM Pricebook2 limit 1];
                
        for (Place__c a : [Select id, Account__c, Product_Information__c, SLmig_AmountCategory__c, SLmig_AmountLocation__c  from Place__c WHERE Quote_Migration_Batch__c =: batch and status__c != 'disabled']){ 
            if(plAcc.get(a.Account__c) == null){
                List<Place__c> lstPlaces = new List<Place__c>();
                lstPlaces.add(a);
                plAcc.put(a.Account__c, lstPlaces);
            } else {
                List<Place__c> lstPlaces = plAcc.get(a.Account__c);
				lstPlaces.add(a);                
            }
               
        } 
                
    }
	
	global Database.QueryLocator start(Database.BatchableContext BC) { //Id='a0q1X000000jBzzQAE' AND  
        return Database.getQueryLocator([select id,SBQQ__Account__c from SBQQ__Quote__c where Quote_Migration_Batch__c =: batch and id not in (Select SBQQ__Quote__c  from SBQQ__Quoteline__c where SBQQ__Quote__r.Quote_Migration_Batch__c =: batch) ]);
		//return Database.getQueryLocator([SELECT Id, SBQQ__Account__c  FROM SBQQ__Quote__c WHERE SL_Migration__c = true and Quote_Migration_Batch__c =: batch]);
	}
    
	global void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope) {
        
        List<SBQQ__QuoteLine__c> lstQL = new List<SBQQ__QuoteLine__c>();
               
		for(SBQQ__Quote__c quote : scope) {
            List<String> lstStr = new List<String>();
            List<Place__c> lstPlaces = new List<Place__c>(plAcc.get(quote.SBQQ__Account__c));
            List<Id> plIds = new List<Id>();
            
            
            
            Integer i = 1;
            for(Place__c pl : lstPlaces) {
                
                plExtraRub.put(pl.id, (Integer) pl.SLmig_AmountCategory__c);
                plExtraZeh.put(pl.id, (Integer) pl.SLmig_AmountLocation__c);
                
                String prodId = prodIds.get(pl.Product_Information__c);
                
                SBQQ__QuoteLine__c QL;
                String pbEntry;
                                      
                if ( pl.Product_Information__c == 'SLS001'){
                    pbEntry = pPbeId.get('SLS001');
					QL = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.id,SBQQ__AllowAssetRefund__c=false,
                    SBQQ__BillingFrequency__c='Annual',SBQQ__Bundle__c=true,SBQQ__Bundled__c=false,SBQQ__CarryoverLine__c=false,SBQQ__ComponentCost__c=0,
                    SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentListTotal__c=100,SBQQ__ComponentTotal__c=100,
                    SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=390,
                    SBQQ__DefaultSubscriptionTerm__c=1,SBQQ__Existing__c=false,
                    SBQQ__Hidden__c=false,SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=390,SBQQ__NetPrice__c=390,
                    SBQQ__Number__c= i,SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=390,
                    SBQQ__PartnerPrice__c=390,SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,
                    SBQQ__PricingMethodEditable__c=false,SBQQ__PricingMethod__c='List',
                    SBQQ__Product__c = prodId ,SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=390,
                    SBQQ__ProratedPrice__c=390,SBQQ__Quantity__c=1,SBQQ__RegularPrice__c=390,SBQQ__Renewal__c=false,SBQQ__SpecialPrice__c=390,
                    SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',SBQQ__SubscriptionTerm__c=1,
                    SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,SBQQ__Uplift__c=0,
                    SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',Place__c=pl.id,Quote__c=quote.id,
                    Status__c='Draft',TrialStatus__c='Draft',Trial__c=false);
                    
                    lstQL.add(QL);
                    
                } else if (pl.Product_Information__c == 'SLT001'){
                    pbEntry = pPbeId.get('SLT001');
                    QL = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.id,SBQQ__AllowAssetRefund__c=false,
                    SBQQ__BillingFrequency__c='Annual',SBQQ__Bundle__c=true,SBQQ__Bundled__c=false,SBQQ__CarryoverLine__c=false,SBQQ__ComponentCost__c=0,
                    SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentListTotal__c=100,SBQQ__ComponentTotal__c=100,
                    SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=59.9,
                    SBQQ__DefaultSubscriptionTerm__c=1,SBQQ__Existing__c=false,
                    SBQQ__Hidden__c=false,SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=59.9,SBQQ__NetPrice__c=59.9,
                    SBQQ__Number__c= i,SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=59.9,
                    SBQQ__PartnerPrice__c=59.9,SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,
                    SBQQ__PricingMethodEditable__c=false,SBQQ__PricingMethod__c='List',
                    SBQQ__Product__c = prodId ,SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=59.9,
                    SBQQ__ProratedPrice__c=59.9,SBQQ__Quantity__c=1,SBQQ__RegularPrice__c=59.9,SBQQ__Renewal__c=false,SBQQ__SpecialPrice__c=59.9,
                    SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',SBQQ__SubscriptionTerm__c=1,
                    SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,SBQQ__Uplift__c=0,
                    SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',Place__c=pl.id,Quote__c=quote.id,
                    Status__c='Draft',TrialStatus__c='Draft',Trial__c=false);
                    
                    lstQL.add(QL);
                }
                if(pl.SLmig_AmountCategory__c > 0){
                    i++;
                }
                
                if(pl.SLmig_AmountLocation__c > 0){
                    i++;
                }
                
                i = i + 5;
               
            }
            
            
            
		}
        
        insert lstQL;
        System.debug('##ID?'+lstQL);
        Integer order;
        String oldId = '';
        List<SBQQ__QuoteLine__c> lstQLprod = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c qlBundle : lstQL) {
            
            String qid = '' + qlBundle.SBQQ__Quote__c;
            
            if (oldId != qid || oldId == ''){
            	order = 2;
            }
            	
            	String prod1 = prodIds.get('ONAP001'); 
            	String pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'ONAP001');
                String po = pfId.get(qlBundle.SBQQ__Product__c+'Basic Content');
            	String pbEntry = pPbeId.get('ONAP001');
                
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c= qlBundle.SBQQ__Quote__c ,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,
                                                    SBQQ__CarryoverLine__c=false,SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,
                                                    SBQQ__ConfigurationRequired__c=false,
                                                    SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,SBQQ__DefaultSubscriptionTerm__c=1,SBQQ__DynamicOptionId__c = po,
                                                    SBQQ__Existing__c=false,SBQQ__Hidden__c=false,SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,
                                                    SBQQ__NonDiscountable__c=true,SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                    SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,
                                                    SBQQ__OriginalPrice__c=0,SBQQ__PartnerPrice__c=0,SBQQ__PriceEditable__c=false,
                                                    SBQQ__PricebookEntryId__c= pbEntry,SBQQ__PricingMethodEditable__c=false,SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c= pOpp,
                                                    SBQQ__Product__c= prod1, SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,SBQQ__Quantity__c=1,
                                                    SBQQ__RegularPrice__c=0,SBQQ__Renewal__c=false,
                                                    SBQQ__RequiredBy__c= qlBundle.id,SBQQ__SpecialPrice__c=0,SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                    SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                    SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',
                                                    SBQQ__Uplift__c=0));
                
                
            	order++;
            	pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'ORER001');
                String prod2 = prodIds.get('ORER001');    
            	pbEntry = pPbeId.get('ORER001');
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c ,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,
                                                     SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=po,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,
                                                     SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,SBQQ__NonDiscountable__c=true,
                                                     SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,SBQQ__OptionType__c='Component',
                                                     SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=0,SBQQ__PartnerPrice__c=0,SBQQ__PriceEditable__c=false,
                                                     SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,SBQQ__PricingMethod__c='List',
                                                     SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=prod2,SBQQ__ProrateMultiplier__c=1,
                                                     SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,SBQQ__Quantity__c=1,SBQQ__RegularPrice__c=0,
                                                     SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=0,SBQQ__SubscriptionBase__c='List',
                                                     SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',SBQQ__SubscriptionTerm__c=1,
                                                     SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                     SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));   
                
                order++;
            	pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'OREV001');
                String prod4 = prodIds.get('OREV001');
            	pbEntry = pPbeId.get('OREV001');
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,
                                                     SBQQ__ConfigurationRequired__c=false,SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,
                                                     SBQQ__DefaultSubscriptionTerm__c=1,SBQQ__DynamicOptionId__c=po,
                                                     SBQQ__Existing__c=false,SBQQ__Hidden__c=false,SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,
                                                     SBQQ__NonDiscountable__c=true,SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                     SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=0,SBQQ__PartnerPrice__c=0,
                                                     SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,
                                                     SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=prod4,
                                                     SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,SBQQ__Quantity__c=1,
                                                     SBQQ__RegularPrice__c=0,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=0,
                                                     SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                     SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                     SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
            
            
            
            if(qlBundle.SBQQ__Product__c == prodIds.get('SLS001')){
                
                order++;
            	pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'OURL001');
                String prod3 = prodIds.get('OURL001'); 
            	pbEntry = pPbeId.get('OURL001');
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,
                                                     SBQQ__ConfigurationRequired__c=false,SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=po,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,SBQQ__Incomplete__c=false,
                                                     SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,SBQQ__NonDiscountable__c=true,SBQQ__NonPartnerDiscountable__c=false,
                                                     SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=0,
                                                     SBQQ__PartnerPrice__c=0,SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,
                                                     SBQQ__PricingMethodEditable__c=false,SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c= pOpp,
                                                     SBQQ__Product__c=prod3,SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,
                                                     SBQQ__Quantity__c=1,SBQQ__RegularPrice__c=0,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,
                                                     SBQQ__SpecialPrice__c=0,SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',
                                                     SBQQ__SubscriptionScope__c='Quote',SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,
                                                     SBQQ__UpliftAmount__c=0,SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
                
            	order++;
               	String po1 = pfId.get(qlBundle.SBQQ__Product__c+'Region Reached');
				pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'OPMBASIC001');
                String prod5 = prodIds.get('OPMBASIC001'); 
                pbEntry = pPbeId.get('OPMBASIC001');
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,
                                                     SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=po1,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,
                                                     SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,SBQQ__NonDiscountable__c=true,
                                                     SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                     SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=0,SBQQ__PartnerPrice__c=0,
                                                     SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,
                                                     SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=prod5,
                                                     SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,SBQQ__Quantity__c=1,
                                                     SBQQ__RegularPrice__c=0,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=0,
                                                     SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                     SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                     SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
                
                
                
                
				                
            } else if(qlBundle.SBQQ__Product__c == prodIds.get('SLT001')) {
                order++;
                String po1 = pfId.get(qlBundle.SBQQ__Product__c+'Region Reached');
                pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'OPMBASICLOW001');
                String prod5 = prodIds.get('OPMBASICLOW001'); 
                pbEntry = pPbeId.get('OPMBASICLOW001');
                lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=1,SBQQ__Bundled__c=true,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,
                                                     SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=0,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=po1,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,
                                                     SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=0,SBQQ__NetPrice__c=0,SBQQ__NonDiscountable__c=true,
                                                     SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                     SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=0,SBQQ__PartnerPrice__c=0,
                                                     SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,
                                                     SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=prod5,
                                                     SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=0,SBQQ__ProratedPrice__c=0,SBQQ__Quantity__c=1,
                                                     SBQQ__RegularPrice__c=0,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=0,
                                                     SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                     SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                     SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
                
                
            }
            
            Integer ExtraRub = plExtraRub.get(qlBundle.Place__c);
            Integer ExtraZub = plExtraZeh.get(qlBundle.Place__c);
            Integer priceAdd = 50;
            Integer total = 0;
            
            if(ExtraRub > 0){
               order++;
               total = priceAdd * ExtraRub;
               String poadd = pfId.get(qlBundle.SBQQ__Product__c+'Basic Content Add');
               pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'PORER001');
               String add1 = prodIds.get('PORER001'); 
               pbEntry = pPbeId.get('PORER001');
               lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=ExtraRub,SBQQ__Bundled__c=false,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,
                                                     SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=priceAdd,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=poadd,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,
                                                     SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=priceAdd,SBQQ__NetPrice__c=priceAdd,SBQQ__NonDiscountable__c=true,
                                                     SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                     SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=priceAdd,SBQQ__PartnerPrice__c=priceAdd,
                                                     SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,
                                                     SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=add1,
                                                     SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=priceAdd,SBQQ__ProratedPrice__c=priceAdd,SBQQ__Quantity__c=ExtraRub,
                                                     SBQQ__RegularPrice__c=priceAdd,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=priceAdd,
                                                     SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                     SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                    SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
            
            }
            
            if(ExtraZub > 0){
               order++;
               total = priceAdd * ExtraRub;
               String poadd = pfId.get(qlBundle.SBQQ__Product__c+'Basic Content Add');
               pOpp = pOppId.get(qlBundle.SBQQ__Product__c+'POREV001');
               String add2 = prodIds.get('POREV001'); 
               pbEntry = pPbeId.get('POREV001');
               lstQLprod.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=qlBundle.SBQQ__Quote__c,SBQQ__AllowAssetRefund__c=false,SBQQ__BillingFrequency__c='Annual',
                                                     SBQQ__Bundle__c=false,SBQQ__BundledQuantity__c=ExtraZub,SBQQ__Bundled__c=false,SBQQ__CarryoverLine__c=false,
                                                     SBQQ__ComponentDiscountedByPackage__c=false,SBQQ__ComponentUpliftedByPackage__c=false,SBQQ__ConfigurationRequired__c=false,
                                                     SBQQ__CostEditable__c=false,SBQQ__CustomerPrice__c=priceAdd,SBQQ__DefaultSubscriptionTerm__c=1,
                                                     SBQQ__DynamicOptionId__c=poadd,SBQQ__Existing__c=false,SBQQ__Hidden__c=false,
                                                     SBQQ__Incomplete__c=false,SBQQ__ListPrice__c=priceAdd,SBQQ__NetPrice__c=priceAdd,SBQQ__NonDiscountable__c=true,
                                                     SBQQ__NonPartnerDiscountable__c=false,SBQQ__Number__c=order,SBQQ__OptionLevel__c=1,
                                                     SBQQ__OptionType__c='Component',SBQQ__Optional__c=false,SBQQ__OriginalPrice__c=priceAdd,SBQQ__PartnerPrice__c=priceAdd,
                                                     SBQQ__PriceEditable__c=false,SBQQ__PricebookEntryId__c=pbEntry,SBQQ__PricingMethodEditable__c=false,
                                                     SBQQ__PricingMethod__c='List',SBQQ__ProductOption__c=pOpp,SBQQ__Product__c=add2,
                                                     SBQQ__ProrateMultiplier__c=1,SBQQ__ProratedListPrice__c=priceAdd,SBQQ__ProratedPrice__c=priceAdd,SBQQ__Quantity__c=ExtraZub,
                                                     SBQQ__RegularPrice__c=priceAdd,SBQQ__Renewal__c=false,SBQQ__RequiredBy__c=qlBundle.id,SBQQ__SpecialPrice__c=priceAdd,
                                                     SBQQ__SubscriptionBase__c='List',SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionScope__c='Quote',
                                                     SBQQ__SubscriptionTerm__c=1,SBQQ__TaxCode__c='Standard',SBQQ__Taxable__c=false,SBQQ__UpliftAmount__c=0,
                                                    SBQQ__ProductSubscriptionType__c='Evergreen',SBQQ__SubscriptionType__c='Evergreen',SBQQ__Uplift__c=0));
                
            }
            
            order = order + 2;
            oldId = qlBundle.SBQQ__Quote__c;
        }
        
        
		insert lstQLprod;        
        
	}
	
	global void finish(Database.BatchableContext BC) {
        
    }
    
}