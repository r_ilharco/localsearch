/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date		   : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : This class contains some useful methods for AbacusTransferJob: The implemented methods deal with the calculation logic that concerns Invoices
 * Changelog   : 
 */

public class SRV_Abacus {
    public static AbacusJsonWrapper getPayload(Map<Id,List<Invoice_Order__c>> invoiceOrdersMap, List<Invoice__c> invoices){
        List<AbacusJsonWrapper.MyInvoice> myInvoices = new List<AbacusJsonWrapper.MyInvoice>();
        
        for(Invoice__c invoice : invoices) {
            List<Invoice_Order__c> invoiceOrders = invoiceOrdersMap.get ( invoice.id );
            List<AbacusJsonWrapper.MyOrder> myOrders = new List<AbacusJsonWrapper.MyOrder>();
            
            for(Invoice_Order__c invoiceOrder : invoiceOrders){
                List<AbacusJsonWrapper.MyItem> myItems = new List<AbacusJsonWrapper.MyItem>();
                
                for(Invoice_Item__c invoiceItem : invoiceOrder.Invoice_Items__r){
					AbacusJsonWrapper.MyCreditNote credit_note = new AbacusJsonWrapper.MyCreditNote( invoiceItem);
					AbacusJsonWrapper.MySubscription subscription = new AbacusJsonWrapper.MySubscription( invoiceItem);
                    AbacusJsonWrapper.MyProduct product = new AbacusJsonWrapper.MyProduct( invoiceItem);
                    //TAXITEM   
                    List<TaxHelper.VatListItem> vatTaxItems = new List<TaxHelper.VatListItem>();
                    if(invoiceItem.Credit_Note__c == null){
                    	vatTaxItems = TaxHelper.getVatItems(invoiceItem.Tax_Code__c, invoiceItem.Accrual_From__c, invoiceItem.Accrual_To__c, invoiceItem.Amount__c, Date.today());
                    }else{
                        system.debug('here for credit note - abacus sending');
                    	vatTaxItems = TaxHelper.getVatItemsForCreditNoteToAbacus(invoiceItem.Tax_Code__c, invoiceItem.Accrual_From__c, invoiceItem.Accrual_To__c, invoiceItem.Amount__c, Date.today());                        
                    }
					List<AbacusJsonWrapper.MyTaxItem> myTaxItems = new List<AbacusJsonWrapper.MyTaxItem>();
                    for(TaxHelper.VatListItem vti : vatTaxItems){
                        myTaxItems.add(new AbacusJsonWrapper.MyTaxItem(vti));
                    }
                    system.debug('invoiceItem: ' + invoiceItem);
                    AbacusJsonWrapper.MyItem item = new AbacusJsonWrapper.MyItem (myTaxItems, invoiceItem, product, subscription, credit_note);
                    system.debug('item: ' + item);
                	myItems.add(item);
                }
				AbacusJsonWrapper.MyContract contract = new AbacusJsonWrapper.MyContract( invoiceOrder);
				AbacusJsonWrapper.MyOrder order = new AbacusJsonWrapper.MyOrder( invoiceOrder, contract, myItems);
            	myOrders.add(order);
            }
            AbacusJsonWrapper.MyBillingProfile billing_profile = new AbacusJsonWrapper.MyBillingProfile ( invoice);
            AbacusJsonWrapper.MyCustomer customer = new AbacusJsonWrapper.MyCustomer( invoice);
            AbacusJsonWrapper.MyInvoice myInvoice = new AbacusJsonWrapper.MyInvoice( invoice, customer, billing_profile, myOrders);
            AbacusJsonWrapper.MyInvoice checkedInvoice = checkTheTotalForAbacus(myOrders, myInvoice);
            myInvoices.add(myInvoice);
        }
        AbacusJsonWrapper wrapper = new AbacusJsonWrapper ( myInvoices );
        return wrapper;
    }
    
    public static List<Invoice__c> updateInvoices (List<Invoice__c> invoices) {
        
        List<Invoice__c> updatedInvoices = new List<Invoice__c> ();
        for (Invoice__c invoice : invoices) {
            updatedInvoices.add(new Invoice__c (Id = invoice.id, Booking_Status__c = ConstantsUtil.ABACUS_STATUS_SENT));
        }
        
        return updatedInvoices;
    }
    
    public static String formatCorrection(String jsonPayload){
        Map<String,String> correctValues = new Map<String,String>();
        correctValues.put('invoice_date', 'date');
        correctValues.put('customer_number', 'number');
        correctValues.put('contract_number', 'number');
        //correctValues.put('null', '');
        for(String s : correctValues.keySet()){
            jsonPayload = jsonPayload.replace(s, correctValues.get(s));
        }
        return jsonPayload;
    }

     public static HttpResponse postCallout(String requestJson, String correlationId, String methodName){
       UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2 ;
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else{
            c2 = Mashery_Setting__c.getOrgDefaults();
        }
        String endpoint = c2.Endpoint__c + methodName;
        system.debug('endpoint: ' + endpoint);
        Decimal cached_millisecs;
        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        Boolean IsNewToken=False;
        if( String.isBlank(c2.Token__c) || ( (cached_millisecs/1000).round() >= (Integer) c2.Expires__c)){
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        } else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        System.debug('token '+token.access_token);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setBody(requestJson);
        System.debug('REQUEST BODY: ************** '+requestJson);
        request.setHeader('Content-Type','application/json');
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('POST');
            /*HttpRequest req = new HttpRequest();
            req.setEndpoint(BillingHelper.billingEndpoint + '/jobs/transfer');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, correlationId);
            req.setHeader(ConstantsUtil.TRACE_HEAD_BP_NAME, 'SendToSwissbilling');
            req.setHeader(ConstantsUtil.TRACE_HEAD_INITIATOR, 'Salesforce');
            req.setHeader(ConstantsUtil.TRACE_HEAD_CALLING_APP, 'Billing');
            req.setHeader(ConstantsUtil.TRACE_HEAD_BO_NAME, 'Invoice__c');
            req.setHeader(ConstantsUtil.TRACE_HEAD_BO_ID, invoice.Id);             
            req.setBody(documentJson);
            req.setTimeout(30000);*/
        request.setTimeout(60000);
        HttpResponse response = http.send(request);
        system.debug('response: ' + response);
        return response;
    }
    
    public static AbacusJsonWrapper.MyInvoice checkTheTotalForAbacus(List<AbacusJsonWrapper.MyOrder> myOrders, AbacusJsonWrapper.MyInvoice myInvoice){
        Decimal totalLinesAmount = 0;
        for(AbacusJsonWrapper.MyOrder currentOO : myOrders){
            for(AbacusJsonWrapper.MyItem currentII : currentOO.items){
                system.debug('currentII: ' + currentII);
                	totalLinesAmount += Decimal.valueOf(currentII.total);
                	system.debug('totalLinesAmount: ' + totalLinesAmount);
                /*for(AbacusJsonWrapper.MyTaxItem currentTaxItem : currentII.tax_items){
                    totalLinesAmount += Decimal.valueOf(currentTaxItem.total);
                    system.debug('totalLinesAmount: ' + totalLinesAmount);
                }*/
            }
        }
        system.debug('totalLinesAmount: ' + totalLinesAmount);
        system.debug('myInvoice.total: ' + myInvoice.total);
        myInvoice.rounding = String.valueOf(Decimal.valueOf(myInvoice.total) - totalLinesAmount);
        return myInvoice;
    }

}