global class SubscriptionGroupingCheckBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts {
    private String sSQL = null;
    public SubscriptionGroupingCheckBatch(){
    }
    
    global database.querylocator start(Database.BatchableContext bc){ 
        
        String contractStatusActive = ConstantsUtil.CONTRACT_STATUS_ACTIVE;     
        String subStatusActive = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        String contratStatusTermination = ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION;
        sSQL = 'SELECT SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c FROM SBQQ__Subscription__c WHERE (Subsctiption_Status__c = :subStatusActive AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c  != null '+
               'AND (SBQQ__Contract__r.Status = :contractStatusActive OR (SBQQ__Contract__r.Status = :contratStatusTermination AND SBQQ__Contract__r.TerminateDate__c > TODAY )) '+
               'AND SBQQ__NetPrice__c != 0 AND SBQQ__QuoteLine__c != NULL AND (SBQQ__EndDate__c = NULL OR SBQQ__EndDate__c > TODAY ) AND (Next_Invoice_Date__c = NULL OR Next_Invoice_Date__c <= TODAY ) AND In_Termination__c = false)';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope){
        Set<Id> billingProfileIds = new Set<Id>();
        for(SBQQ__Subscription__c sss : scope){
            billingProfileIds.add(sss.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c);
        }
        Date todayDate = Date.today();
        Date firstDayOfTheNextMonth = todayDate.addMonths(1).toStartOfMonth();
        system.debug('date: ' + firstDayOfTheNextMonth);
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, Next_Invoice_Date__c, Grouping_Check__c,
                                                     SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__r.Grouping_Mode__c 
                                                     FROM SBQQ__Subscription__c 
                                                     WHERE Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE 
                                                        AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c  != null
                                                        AND (SBQQ__Contract__r.Status = :ConstantsUtil.CONTRACT_STATUS_ACTIVE
                                                        OR (SBQQ__Contract__r.Status = :ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION 
                                                            AND SBQQ__Contract__r.TerminateDate__c > :Date.today()))
                                                        AND SBQQ__NetPrice__c != 0 
                                                  		AND SBQQ__QuoteLine__c != NULL 
                                                        AND (SBQQ__EndDate__c = NULL OR SBQQ__EndDate__c > :Date.today())
                                                        AND (Next_Invoice_Date__c = NULL OR Next_Invoice_Date__c < :firstDayOfTheNextMonth)
                                                     	AND One_time_Fee_Billed__c = false
                                                     	AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c IN :billingProfileIds
                                                     ORDER BY Next_Invoice_Date__c desc
                                                     LIMIT 50000];
        Map<Id, List<SBQQ__Subscription__c>> billingProfileSubsMap = new Map<Id, List<SBQQ__Subscription__c>>();
        Map<Id, List<SBQQ__Subscription__c>> billingProfileNoGroupingMap = new Map<Id, List<SBQQ__Subscription__c>>();
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c s : subscriptions){
            if(s.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__r.Grouping_Mode__c == ConstantsUtil.BILLING_GROUP_MODE_STANDARD){
                if(!billingProfileSubsMap.containsKey(s.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c)){
                    billingProfileSubsMap.put(s.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, new List<SBQQ__Subscription__c>{s});
                }else{
                    billingProfileSubsMap.get(s.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c).add(s);
                }
            }else if(s.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__r.Grouping_Mode__c == ConstantsUtil.BILLING_GROUP_MODE_NO_GROUPING){
                if(s.Grouping_Check__c){
					s.Grouping_Check__c = false;
                    subsToUpdate.add(s);
                }
            }
        }
        for(Id bfId : billingProfileSubsMap.keySet()){
            Date checkDate;
            for(SBQQ__Subscription__c currentSub : billingProfileSubsMap.get(bfId)){
                if(currentSub.Next_Invoice_Date__c != null){
					checkDate = currentSub.Next_Invoice_Date__c;
                    break;
                }
            }
            for(SBQQ__Subscription__c currentSub : billingProfileSubsMap.get(bfId)){
                system.debug('checkDate: ' + checkDate);
                system.debug('currentSub: ' + currentSub);
                system.debug('Next_Invoice_Date__c: ' + currentSub.Next_Invoice_Date__c);
                if(checkDate != null){
                    if(currentSub.Next_Invoice_Date__c != null){
                        if(currentSub.Next_Invoice_Date__c < checkDate && checkDate > Date.today()){
                            currentSub.Grouping_Check__c = true;
                        }else{
                            currentSub.Grouping_Check__c = false;
                        }
                    }else{
                        if(checkDate > Date.today()){
                            currentSub.Grouping_Check__c = true;
                        }else{
                            currentSub.Grouping_Check__c = false;
                        }
                    }   
                }else{
                    currentSub.Grouping_Check__c = false;
                }
                subsToUpdate.add(currentSub);
            }
        }
        if(subsToUpdate.size() > 0){
            update subsToUpdate;
        }
    }
    global void finish(Database.BatchableContext bc){        
    }
    
    global void execute(SchedulableContext sc){
        SubscriptionGroupingCheckBatch b = new SubscriptionGroupingCheckBatch(); 
        database.executebatch(b); 
    }
}