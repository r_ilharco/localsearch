public without sharing class SRV_EnhancedExtProdConfiguration {
    
    public static Mashery_Setting__c csToUpdate {get;set;}
    
    public static HttpResponse callout (Map<String,String> parameters, String correlationId, String methodName){
        
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        
        Mashery_Setting__c c2;
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else c2 = Mashery_Setting__c.getOrgDefaults();

        String endpoint = c2.Endpoint__c + methodName;

        if(!parameters.isEmpty()){
            Boolean afterFirst = false;
            endpoint += '?';
            for(String s : parameters.keySet()){
                if(afterFirst){
                    endpoint += '&';
                }                  
                endpoint += s + '=' + parameters.get(s);
                afterFirst = true;
            }
        }

        Decimal cached_millisecs;

        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }

        Boolean IsNewToken=False;
        if( String.isBlank(c2.Token__c) || ( (cached_millisecs/1000).round() >= (Integer) c2.Expires__c)){
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        } else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        System.debug('token '+token.access_token);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
                
        request.setEndpoint(endpoint);
        System.debug('endpoint '+endpoint);
        
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        //request.setHeader('X-LS-Tracing-BPName', '');
		//request.setHeader('X-LS-Tracing-Initiator', '');
        //request.setHeader('X-LS-Tracing-CallingApp', '');
        //request.setHeader('X-LS-Tracing-BOIdName', '');
        //request.setHeader('X-LS-Tracing-BOIdValue', '');
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('GET');
        request.setTimeout(60000);
        
        HttpResponse response = http.send(request);
        
        Integer statusCode = response.getStatusCode();
        
        /*if(String.valueOf(statusCode).equalsIgnoreCase('401')){
            
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        	
            request.setHeader('Authorization', 'Bearer '+token.access_token);
            
            response = http.send(request);
        }*/
        
        if(IsNewToken==True && !Test.isRunningTest()){
            System.debug('Is new token true to update cs');
            c2.Token__c = token.access_token; 
            c2.Expires__c = token.expires_in; 
            csToUpdate = c2;        
        }
        return response;
    }
    
    public static HttpResponse postCallout (String requestJson, String correlationId, String methodName){
        
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        List<Profile> p = [select Id from profile where name = :ConstantsUtil.SysAdmin limit 1];
        
        Mashery_Setting__c c2;

        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else{
            c2 = Mashery_Setting__c.getOrgDefaults();
        }
        decimal cached_millisecs;
        if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        Boolean IsNewToken=False;
        if(c2.Token__c == null || c2.Token__c == '' ||((cached_millisecs/1000).round()>=(Integer)c2.Expires__c)){
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        } else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        
        String endpoint = c2.Endpoint__c;
        System.debug('token '+token.access_token);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint + methodName);
        request.setBody(requestJson);
        System.debug('REQUEST BODY: ************** '+requestJson);
        request.setHeader('Content-Type','application/json');
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        //request.setHeader('X-LS-Tracing-BPName', '');
		//request.setHeader('X-LS-Tracing-Initiator', '');
        //request.setHeader('X-LS-Tracing-CallingApp', '');
        //request.setHeader('X-LS-Tracing-BOIdName', '');
        //request.setHeader('X-LS-Tracing-BOIdValue', '');
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('POST');
        request.setTimeout(60000);
        
        HttpResponse response = http.send(request);
        
        Integer statusCode = response.getStatusCode();
        
        if(String.valueOf(statusCode).equalsIgnoreCase('401')){
            
            IsNewToken=True;
            token = UtilityToken.refreshToken();
        	
            request.setHeader('Authorization', 'Bearer '+token.access_token);
            
            response = http.send(request);
        }
        
        if(IsNewToken==True && !Test.isRunningTest()){
            c2.Token__c = token.access_token; 
            c2.Expires__c = token.expires_in; 
            csToUpdate = c2;       
        }
        return response;
    }
    
    
    //https://apicloud-test.localsearch.tech/reservation?localid={localid}&category={category}
    /*public static ReservationResponse VipReservation (String placeId, Map<String,String> parameters, String method){
        ReservationResponse returnType = new ReservationResponse();
           
        String correlationId = generateCorrelationId(placeId);
        ReservationRequest requestBody = new ReservationRequest();
        requestBody.localid =  parameters.get('localid');
        requestBody.category =  parameters.get('category');
        
        String jsonRequest = JSON.serialize(requestBody);
        
        try{
            HttpResponse response = postCallout(jsonRequest, correlationId, method);
            System.debug('response '+response);         
            if (response.getStatusCode() == 200) {
                returnType = (ReservationResponse)JSON.deserialize(response.getBody(), ReservationResponse.class); 
            }
            else {
                throw new CustomException();
            }  
        }
        catch(CustomException ce){
        //createIntegrationLog(correlationId, endpoint, ce.getMessage(), placeId);
        }
        catch(Exception e){
        //createIntegrationLog(correlationId, endpoint, e.getMessage(), placeId);
        }
        return returnType;
    }*/
    
    //https://apicloud-test.localsearch.tech/v1/toplink-regions?localid={localid}
    public static TopLinkRegionsResponseWrapper TopLinklocationCallout(String quoteId, Map<String,String> parameters){
        
        TopLinkRegionsResponseWrapper returnType = new TopLinkRegionsResponseWrapper();
        
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.METHOD_TOP_LINK_REGIONS);
        String correlationId = generateCorrelationId(quoteId);
        
        try{
        	HttpResponse response = callout(parameters,correlationId,methodName);        
            if (response.getStatusCode() == 200) {
                System.debug('VLAUDATO RE: '+response.getBody());
                returnType = (TopLinkRegionsResponseWrapper)JSON.deserialize(response.getBody(), TopLinkRegionsResponseWrapper.class); 
                //createIntegrationLog(correlationId, endpoint, null, quoteId);
            }
            else {
                throw new CustomException();
            }  
        }
        catch(CustomException ce){
            //createIntegrationLog(correlationId, endpoint, ce.getMessage(), quoteId);
        }
        catch(Exception e){
            //createIntegrationLog(correlationId, endpoint, e.getMessage(), quoteId);
        }
        return returnType;
    }
    
    //SYSTEM : Local.ch
    public static CategoryLocationResponse categoryCallout(String quoteId, Map<String,String> parameters){
        
        CategoryLocationResponse returnType = new CategoryLocationResponse();
        
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.METHOD_CATEGORY_SERVICE);
        String correlationId = generateCorrelationId(quoteId);
        try{
           	HttpResponse response = new HttpResponse();
            response= callout(parameters, correlationId, methodName);
            if (response.getStatusCode() == 200) {
                returnType = (CategoryLocationResponse)JSON.deserialize(response.getBody(), CategoryLocationResponse.class); 
                //createIntegrationLog(correlationId, endpoint, null, quoteId);
            }
            else {
                throw new CustomException();
            }  
        }
        catch(CustomException ce){
            //createIntegrationLog(correlationId, endpoint, ce.getMessage(), quoteId);
        }
        catch(Exception e){
            //createIntegrationLog(correlationId, endpoint, e.getMessage(), quoteId);
        }

        return returnType;
    }
    
    //SYSTEM : search.ch
    public static CategorySearchResponse searchCategoryCallout(String quoteId, Map<String,String> parameters){
        
        CategorySearchResponse returnType = new CategorySearchResponse();
        
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.METHOD_AVAILABILITY);
        String correlationId = generateCorrelationId(quoteId);        
        try{
           	HttpResponse response = new HttpResponse();
            response= callout(parameters, correlationId, methodName);
            System.debug('response '+response);
            System.debug('response BODY CAT '+response.getBody());
            if (response.getStatusCode() == 200) {
                returnType = (CategorySearchResponse)JSON.deserialize(response.getBody(), CategorySearchResponse.class); 
                //createIntegrationLog(correlationId, endpoint, null, quoteId);
            }
            else {
                throw new CustomException();
            }  
        }
        catch(CustomException ce){
            //createIntegrationLog(correlationId, endpoint, ce.getMessage(), quoteId);
        }
        catch(Exception e){
            //createIntegrationLog(correlationId, endpoint, e.getMessage(), quoteId);
        }
		
        return returnType;
    }

    //location-service/locations/?area_types=LTVSalesRegion
    public static LocationResponse locationCallout(String quoteId, Map<String,String> parameters){
        LocationResponse returnType = new LocationResponse();
        
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.METHOD_ONLINE_DIS_REGIONS);
        String correlationId = generateCorrelationId(quoteId);

        try{
        	HttpResponse response = callout(parameters, correlationId, methodName);
            System.debug('response '+response);
            if (response.getStatusCode() == 200) {
                returnType = (LocationResponse)JSON.deserialize(response.getBody(), LocationResponse.class); 
            }
            else {
                throw new CustomException();
            }  
        }
        catch(CustomException ce){

        }
        catch(Exception e){
			
        }
        
        return returnType;
    }
    
    public static String generateQLineExternalId(){
        String hashString = String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        return EncodingUtil.convertToHex(hash);
    }
    
    public static String generateCorrelationId(String productId){
        String hashString = productId + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        return EncodingUtil.convertToHex(hash);
    }
    
    public static CasaAdContextResponse adContextCallout(String quoteId, Map<String,String> parameters){
        
        CasaAdContextResponse casaResponse;
        String correlationId;
        
        CasaAdContextRequest requestBody = new CasaAdContextRequest();
        requestBody.category = new CategoryLocation();
        requestBody.category.href = parameters.get('category');
        if(parameters.get('region') != null){
            requestBody.region = new CategoryLocation();
        	requestBody.region.href = parameters.get('region');
        }
        requestBody.start_date = parameters.get('start_date');
        requestBody.end_date = parameters.get('end_date');
        requestBody.expiration_date = parameters.get('expiration_date');
        requestBody.slot = parameters.get('slot');
        requestBody.product = parameters.get('product_code');
        if(!ConstantsUtil.CASA_PRODUCTS_LANGUAGE_AGNOSTIC.contains(parameters.get('product_code'))){
            requestBody.language = parameters.get('language');
        }
        
        String jsonRequest = JSON.serialize(requestBody, true);
        
        if(quoteId != null) correlationId = generateCorrelationId(quoteId);
        else correlationId = generateQLineExternalId();
        
        HttpResponse response;
        String methodName;
        
        try{
            methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.AD_CONTEXT_ALLOCATION);
            methodName += '?dry_run=false';
            
            response = postCallout(jsonRequest, correlationId, methodName);
            if (response.getStatusCode() == 200) {
                casaResponse = (CasaAdContextResponse)JSON.deserialize(response.getBody(), CasaAdContextResponse.class);
                insert LogUtility.saveCasaReservationLog(methodName, response, jsonRequest, correlationId, quoteId, null);
                return casaResponse;
            }
            else {
                throw new CustomException();
            }
        }
        catch(CustomException ce){
            insert LogUtility.saveCasaReservationLog(methodName, response, jsonRequest, correlationId, quoteId, null);
        }
        catch(Exception e){
            insert LogUtility.saveCasaReservationLog(methodName, response, jsonRequest, correlationId, quoteId, e);
        }
        return casaResponse;
    }
    public class TokenResponseCIS{
        public String access_token {get;set;}
    }
    
    @future @testvisible
    private static void createIntegrationLog(String correlationId, String json, String errorMessage, Id quoteId){
        Log__c log = new Log__c(Name = correlationId +' '+ConstantsUtil.REQUEST, 
                                Message__c = errorMessage, 
                                Body_Message__c = json, 
                                Quote__c = quoteId,
                                Correlation_ID__c = correlationId);
        database.insert(log, false);
    }

    
    public class CasaAdContextResponse extends CasaAdContextRequest{
        
        public List<CasaAdContextRequest> alternative_allocations {get;set;}
        
    }
    
    public virtual class CasaAdContextRequest{
        
        /*public CasaAdContextRequest(){
            alternative_allocations = new List<AlternativeAllocation>();
            conflicting_allocations = new List<HrefOnlyClass>();
        }*/
        
        public List<AlternativeAllocation> alternative_allocations {get;set;}
        public HrefOnlyClass assets {get;set;}
        public CategoryLocation category {get;set;}
        public List<HrefOnlyClass> conflicting_allocations {get;set;}
        public Boolean disabled {get;set;}
        public String end_date {get;set;}
        public String expiration_date {get;set;}
        public HrefOnlyClass expired_allocation {get;set;}
		public String href {get;set;}
        public String id {get;set;}
        public String language {get;set;}
        public String product {get;set;}
        public CategoryLocation region {get;set;}
        public HrefOnlyClass renewal {get;set;}
        public String slot {get;set;}
        public String start_date {get;set;}
    }
    
    public class AlternativeAllocation{}
    
    public class HrefOnlyClass{
        public String href {get;set;}
    }
    
    public class CategorySearchResponse{
        public List<Availability> availability {get;set;}
    }
    
    public class CategoryLocation{
        public String href {get;set;}
        public String classification {get;set;}
    }
    
    public class Availability{
        public String category {get;set;}
        public CategoryName category_name {get;set;}
        public String state {get;set;}
        public List<Blocker> blocker {get;set;}
        public Double price {get;set;}
        public Boolean bookable {get;set;}
    }
    
    public class CategoryName{
        public String de;
        public String fr;
        public String it;
        public String en;
    }
	
    public class Blocker{
        public String localid;
        public String name;
        public String start_date;
        public String end_date;
        public Boolean reservation;
    }
  
    public class LocationResponse {
        public List<LocationItem> items {get;set;}
        public String total {get;set;}
    }
    public class LocationItem{    
        public String href {get;set;}
        public String id {get;set;}
        public String code {get;set;}
        public String name {get;set;}
    }  
    /*public class Location{
        public String all  {get;set;}
    }*/
    public class CategoryLocationResponse{    
        public List<Item> items {get;set;}
        public String total {get;set;}
    }  
    public class Item {
        public String id {get;set;}
        public CategoryLocationItem category_locales {get;set;}
    }
    public class CategoryLocationItem{
        public LanguageItem it {get;set;}
        public LanguageItem fr {get;set;}
        public LanguageItem en {get;set;}
        public LanguageItem de {get;set;}
    }
    public class LanguageItem{
        public String name {get;set;}
    }
    
    public class ReservationRequest{
        public String localid {get;set;}
        public String category {get;set;}
    }
    public class ReservationResponse{
        public Boolean success {get;set;}
        public String request {get;set;}
        public String eof {get;set;}
    }
    public class CustomException extends Exception{}
    
}