@isTest
private class StandardUserHandlerTest {
    static testMethod void testCreateAndUpdateUser() {
        try{
        StandardUserHandler handler = new StandardUserHandler();
        
        Map<string,String> tempUser=  new Map<String, String>
        {'User.language' => 'en_US' ,
            'user.username'=>'username@email.com.prod' ,
            'user.lastname'=>'last name',
            'user.email'=>'email@email.com',
            'user.alias'=>'alias',
            'user.timezonesidkey'=>'Europe/Berlin',
            'user.localesidkey'=>'en_US',
            'user.emailencodingkey'=>'ISO-8859-1',
            'user.code__c' => 'SYSADM',
            'user.languagelocalekey'=>'en_US',
            'user.communitynickname' => 'nickname'}; 
           
            User u = handler.createUser(null,null,null,null, tempUser ,null);
            profile prof = [select id,name from profile where name = 'System Administrator' limit 1];
            u.ProfileId = prof.id;
            insert u; 
        	tempUser.put('user.phone', '4654654');        	
        	tempUser.put('user.firstname', 'Test');
        	User updatedUser = [SELECT lastName,LanguageLocaleKey,email FROM user WHERE email='email@email.com'];
        	
        	handler.updateUser(updatedUser.id,null,null,null, '', tempUser,null);
        		try{
                tempUser.put('user.phone', '4654654asd+s+dsd+sd');
                handler.createUser(null,null,null,null, tempUser ,null);
                }catch(Exception e){                                
                    System.Assert(true, e.getMessage());
                }
			}catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
    }
}