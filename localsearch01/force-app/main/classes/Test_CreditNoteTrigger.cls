/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Credit Note Trigger (Handler and Helper)
 * Changelog   : 
 */

@isTest
public class Test_CreditNoteTrigger{
    
    @testSetup
    static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        CreditNoteTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        Invoice__c invoice = new Invoice__c();
        invoice.Name = 'InvoiceTest';
		invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
		invoice.Customer__c = account.Id;
        invoice.Amount__c = 500;
        insert invoice;
        
        Invoice_Order__c invoiceOrder = new Invoice_Order__c();
        invoiceOrder.Invoice__c = invoice.Id;
        invoiceOrder.Contract__c = contract.Id;
        insert invoiceOrder;
        
        Invoice_Item__c invoiceItem = new Invoice_Item__c();
        invoiceItem.Invoice_Order__c = invoiceOrder.Id;
		invoiceItem.Subscription__c = fatherSub.Id;
		invoiceItem.Amount__c = 150;
		invoiceItem.Accrual_From__c = Date.today();
        insert invoiceItem;
        		        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        CreditNoteTriggerHandler.disableTrigger = false;
    }
    
    @isTest 
    static void test_insertUpdate(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
        
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id AND SBQQ__RequiredById__c = null LIMIT 1];
        
        Credit_Note__c creditNote = new Credit_Note__c();
        creditNote.Account__c = account.Id;
        creditNote.Contract__c = contract.Id;
        creditNote.Execution_Date__c = System.today();
        creditNote.Value__c = 105;
        creditNote.Manual__c = true;
        creditNote.Subscription__c = sub.Id;
		creditNote.Reimbursed_Invoice__c = invoice.Id;
        creditNote.Period_From__c = Date.today();
        creditNote.Period_To__c = Date.today().addDays(1);
        insert creditNote;
        
        Invoice_Item__c invoiceItem = [SELECT Id, Credit_Note__c FROM Invoice_Item__c LIMIT 1];
        invoiceItem.Credit_Note__c = creditNote.Id;
        update invoiceItem;
        
        creditNote.Description__c = 'Test Description';
        creditNote.Period_From__c = Date.today().addDays(10);
        
        Test.startTest();
		update creditNote;
        Test.stopTest();
    }
    
    @isTest public static void test_delete(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
        
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id AND SBQQ__RequiredById__c = null LIMIT 1];
        
        Credit_Note__c creditNote = new Credit_Note__c();
        creditNote.Account__c = account.Id;
        creditNote.Contract__c = contract.Id;
        creditNote.Execution_Date__c = System.today();
        creditNote.Value__c = 105;
        creditNote.Manual__c = true;
        creditNote.Subscription__c = sub.Id;
		creditNote.Reimbursed_Invoice__c = invoice.Id;
        creditNote.Period_From__c = Date.today();
        creditNote.Period_To__c = Date.today().addDays(1);
        insert creditNote;
        
        Test.startTest();
        delete creditNote;
        undelete creditNote;
        Test.stopTest();
    }

    @isTest
    static void test_updateTerritoryInformation(){
        
        List<Credit_Note__c> newItems = new List<Credit_Note__c>();
        
        UserTerritory2Association ut2a = [SELECT Id, Territory2Id, UserId, RoleInTerritory2
                                          FROM UserTerritory2Association
                                          WHERE userId != null
                                          LIMIT 1];
        
        Account a = [SELECT Id, OwnerId FROM Account LIMIT 1][0];
        a.OwnerId = ut2a.UserId;
        update a;
        
        Contract c = [SELECT Id FROM Contract LIMIT 1][0];
        Credit_Note__c cn = new Credit_Note__c(Account__c=a.Id, AccountOwner__c = ut2a.UserId, Contract__c=c.Id, Execution_Date__c=System.today(), Value__c=105);
        newItems.add(cn);
        
        Test.startTest();
        insert newItems;
        Test.stopTest();
    }
}