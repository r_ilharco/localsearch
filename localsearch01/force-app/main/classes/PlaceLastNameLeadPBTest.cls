@isTest
public class PlaceLastNameLeadPBTest {

     @isTest
    static void testPlaceLastName(){
        try{
        Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='Dhaka';    
                       
            insert acc;
        System.debug('Account' +acc);

        
        Lead ld=new Lead();

       		ld.LastName='Doe';
            ld.FirstName='John';
            ld.Company='test company';
            ld.Status='New';
        	ld.P_O_Box__c='10';
        	ld.P_O_Box_City__c='Dhaka';
       		ld.P_O_Box_Zip_Postal_Code__c='101';
                	
       test.startTest();
        insert ld;  
       //update ld;
       
        
        System.debug('Lead' +ld);
        

        	Place__c place = new Place__c();
            place.Name = 'test place';
            place.Company__c = 'test company';
            place.City__c = 'Dhaka';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '101';
            place.Account__c = acc.Id;
        	place.Lead__c= ld.Id;
        	
            insert place;
            //update place;
            System.debug('Place: '+ place);
        test.stopTest();
            
           
    }
        catch(Exception e)
        {
            System.assert(false, e.getMessage());
        }
        
    }
}