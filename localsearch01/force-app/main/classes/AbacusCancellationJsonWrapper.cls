public with sharing class AbacusCancellationJsonWrapper {
    String original_document_number {get;set;}
    String doc_date {get;set;}

    public AbacusCancellationJsonWrapper(String doc_number, Date doc_date) {
        this.original_document_number = doc_number;
        this.doc_date = String.valueOf(doc_date);
    }
}