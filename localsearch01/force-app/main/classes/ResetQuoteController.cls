public without sharing class ResetQuoteController {
	
    @AuraEnabled
    public static CheckValidityWrapper checkValidity(String opportunityId){
        
        CheckValidityWrapper checkValidityWrapper = new CheckValidityWrapper();
        
        List<Opportunity> opportunity = [SELECT Id, StageName FROM Opportunity WHERE Id = :opportunityId];
        
        if(!opportunity.isEmpty()){
            Boolean isClosedLost = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST.equalsIgnoreCase(opportunity[0].StageName);
            Boolean isClosedWon = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON.equalsIgnoreCase(opportunity[0].StageName);
            if(isClosedLost || isClosedWon) checkValidityWrapper.optyClosed = true;
            else{
                List<SBQQ__Quote__c> primaryQuote = [SELECT Id, SBQQ__Status__c, SBQQ__ExpirationDate__c FROM SBQQ__Quote__c WHERE SBQQ__Primary__c = TRUE AND SBQQ__Opportunity2__c = :opportunityId LIMIT 1];
                
                if(!primaryQuote.isEmpty()){
                    Boolean isAccepted = ConstantsUtil.Quote_StageName_Accepted.equalsIgnoreCase(primaryQuote[0].SBQQ__Status__c);
                    if(primaryQuote[0].SBQQ__ExpirationDate__c < Date.today()) checkValidityWrapper.isExpired = true;
                    else if(!isAccepted) checkValidityWrapper.quoteAccepted = false;
                }
            }
        }
        
        return checkValidityWrapper;
    }
    
    @AuraEnabled
    public static Boolean resetQuote(String opportunityId){
        
        Boolean isSuccess = false;
        
        List<SBQQ__Quote__c> primaryQuote = [SELECT Id, SBQQ__Status__c, SBQQ__ExpirationDate__c, Tech_Bypass_Validation__c FROM SBQQ__Quote__c WHERE SBQQ__Primary__c = TRUE AND SBQQ__Opportunity2__c = :opportunityId LIMIT 1];
        
        if(!primaryQuote.isEmpty()){
            Id quoteId = primaryQuote[0].Id;
            List<SBQQ__QuoteDocument__c> quoteDocuments = [SELECT Id, SBQQ__SignatureStatus__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__c = :quoteId];
            try{
                
                QuoteTriggerHandler.disableTrigger = true;
                primaryQuote[0].Tech_Bypass_Validation__c = true;
                primaryQuote[0].SBQQ__Status__c = ConstantsUtil.Quote_Status_Draft;
                update primaryQuote;
                
                if(!quoteDocuments.isEmpty()){
                    for(SBQQ__QuoteDocument__c currentDocument : quoteDocuments) currentDocument.SBQQ__SignatureStatus__c = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_REVOKED;
                    update quoteDocuments;
                }
                
                QuoteTriggerHandler.disableTrigger = false;
                primaryQuote[0].Tech_Bypass_Validation__c = false;
                update primaryQuote;
                
                isSuccess = true;
            }
            catch(Exception e){
                Error_Log__c error = LogUtility.generateErrorLog('ResetQuoteController', 'resetQuote', e, null, quoteId, null);
                insert error;
            }
        }
        
        return isSuccess;
    }
    
    public class CheckValidityWrapper{
        @AuraEnabled
        public Boolean quoteAccepted {get;set;}
        @AuraEnabled
        public Boolean optyClosed {get;set;}
        @AuraEnabled
        public Boolean isExpired {get;set;}
        
        public CheckValidityWrapper(){
            quoteAccepted = true;
            optyClosed = false;
            isExpired = false;
        }
    }
}