public without sharing class  QuoteManager {
    @InvocableMethod (label='Update Quote' description='Update Quote')
    public static Void updateQuote(List<SBQQ__Quote__c > listNew) {
        String VquoteId;
        String VStatus;
        Boolean VOrdered;
        
        for(SBQQ__Quote__c q :listNew){
            VquoteId=q.Id; 
            VStatus=q.SBQQ__Status__c;
            VOrdered=q.SBQQ__Ordered__c;
        }
        updateQuoteFuture(VquoteId,VStatus,VOrdered);        
        
    }
    //@Future    
    public static void updateQuoteFuture(String VquoteId,String VStatus, Boolean VOrdered) {
        List<SBQQ__QuoteLine__c> listQLinesToUpdate = new List<SBQQ__QuoteLine__c>();
        //String VquoteId='';
        //System.debug('VStatus '+VStatus+'--- VOrdered '+VOrdered);
        // update to Trial Request
        try {           
            if(VStatus==Label.Quote_Status_for_Trial_Request){
                for(SBQQ__QuoteLine__c ql : [SELECT Place__c,TrialStartDate__c,TrialEndDate__c,TrialStatus__c,TrialRequestedAt__c, SBQQ__Bundled__c  FROM SBQQ__QuoteLine__c 
                                             WHERE SBQQ__Quote__c = :VquoteId AND TrialStatus__c!=:ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED] ) { 
                                                 
                                                 ql.TrialStatus__c=Label.QuoteLine_Trial_Requested;                   
                                                 //if(ql.Place__c!=null && ql.TrialStartDate__c==null) ql.TrialStartDate__c = Date.today();
                                                 //if(ql.Place__c!=null && ql.TrialEndDate__c==null) ql.TrialEndDate__c = ql.TrialStartDate__c+Integer.valueOf(Label.Trial_end_prior_days);
                                                 
                                                 //SBQQ__Bundled__c == false means this Quote Line for either main product or optional product                             
                                                 if(ql.SBQQ__Bundled__c == false && ql.TrialStartDate__c==null) ql.TrialStartDate__c = Date.today();
                                                 if(ql.SBQQ__Bundled__c == false && ql.TrialEndDate__c==null) ql.TrialEndDate__c = ql.TrialStartDate__c+Integer.valueOf(Label.Trial_end_prior_days);
                                                 
                                                 ql.TrialRequestedAt__c =datetime.now();                            
                                                 listQLinesToUpdate.add(ql);
                                                 
                                             }
            }
            else if(VStatus=='Draft'){ //back to Draft if TrialStatus moved to Draft from previous Staus
                for(SBQQ__QuoteLine__c ql : [SELECT TrialStatus__c  FROM SBQQ__QuoteLine__c 
                                             WHERE SBQQ__Quote__c = :VquoteId AND TrialStatus__c!=:'Draft'] ) {
                                                 ql.TrialStatus__c='Draft';           
                                                 // ql.Status__c='Draft';    
                                                 
                                                 listQLinesToUpdate.add(ql);                                                           
                                             }
            }
            else if(VStatus=='Rejected'){ //Rejected
                for(SBQQ__QuoteLine__c ql : [SELECT TrialStatus__c,id  FROM SBQQ__QuoteLine__c 
                                             WHERE SBQQ__Quote__c = :VquoteId AND TrialStatus__c!=:'Rejected'] ) {
                                                 ql.TrialStatus__c='Rejected';
                                                 //    ql.Status__c='Draft';                            
                                                 listQLinesToUpdate.add(ql);                                                           
                                             }
            }
            if(listQLinesToUpdate.size() > 0){
                update listQLinesToUpdate; 
            }
            
        }catch (Exception ex) {
            ErrorHandler.logInfo('QuoteManager', '', ex.getMessage());
        }         
        /*                
Task nTask = new Task(
Subject     ='Trial Activation', 
OwnerId     = UserInfo.getUserId(), 
ActivityDate = Date.today()+5, 
WhatId      = VquoteId,
Status = 'Offen' // Open
);

insert nTask; 
*/        
    }
    //@Future
    public static void convertTrial(String VquoteId) {
        // when trial quote accepted to OPP won and contract activated     
        List<SBQQ__QuoteLine__c> listQLinesToUpdate = new List<SBQQ__QuoteLine__c>();         
        try {              
            for(SBQQ__QuoteLine__c ql : [SELECT TrialStatus__c,id  FROM SBQQ__QuoteLine__c 
                                         WHERE SBQQ__Quote__c = :VquoteId AND TrialStatus__c!=:'Draft'] ) {
                                             ql.TrialStatus__c='Converted';
                                             //    ql.Status__c='Converted';    
                                             listQLinesToUpdate.add(ql);              
                                         }            
            if(listQLinesToUpdate.size() > 0){
                update listQLinesToUpdate; 
            }                    
        }catch (Exception ex) {
            ErrorHandler.logInfo('QuoteManager', '', ex.getMessage());
        }                
    }
    
    @AuraEnabled
    public static MyQuote CalculateQuoteDiff(Id quoteId, boolean updateQuote){
        
        system.debug('CalculateQuoteDiff::'+quoteId+' updateQuote::'+updateQuote);
        Set<string> quoteStatus = new Set<string>{ConstantsUtil.QUOTE_STATUS_PRESENTED, ConstantsUtil.QUOTE_STATUS_APPROVED, ConstantsUtil.QUOTE_STATUS_REJECTED};
            Set<string> quoteTypes = new Set<string>{ConstantsUtil.QUOTE_TYPE_REPLACEMENT, ConstantsUtil.QUOTE_TYPE_UPGRADE };
                List<SBQQ__Quote__c> quotes = [select id, New_EW__c, Old_EW__c, SBQQ__Type__c, Difference_EW__c,SBQQ__Status__c,
                                            //   (select id ,SBQQ__Quote__r.SBQQ__Type__c, SBQQ__ProductName__c, Total__c,Subscription_Term__c,
                                            //    SBQQ__Quote__c, SBQQ__Product__r.One_time_Fee__c, Subscription_to_Terminate__c, SBQQ__RequiredBy__c,SBQQ__Product__r.Base_term_Renewal_term__c, SBQQ__StartDate__c  from SBQQ__LineItems__r ),
                                               Total_Net_Amount__c, SBQQ__CustomerDiscount__c, Net_Order_Value__c, Signed_Document_URL__c,
                                               Total_Sum_Line__c, SBQQ__StartDate__c 
                                               from SBQQ__Quote__c 
                                               where id= :quoteId 
                                               and SBQQ__Type__c in :quoteTypes];
        system.debug('CalculateQuoteDiff::quotes'+quotes.size());
        if(quotes.size() != 1) return null;
        SBQQ__Quote__c quote = quotes[0];
        
        //Map<id,SBQQ__QuoteLine__c> qls = new Map<id,SBQQ__QuoteLine__c>(quote.SBQQ__LineItems__r);
         Map<id,SBQQ__QuoteLine__c> qls = new Map<id,SBQQ__QuoteLine__c>([select id ,SBQQ__Quote__r.SBQQ__Type__c, SBQQ__ProductName__c, Total__c,Subscription_Term__c,
                                                SBQQ__Quote__c, SBQQ__Product__r.One_time_Fee__c, Subscription_to_Terminate__c, SBQQ__RequiredBy__c,SBQQ__Product__r.Base_term_Renewal_term__c, SBQQ__StartDate__c  
                                                                               from SBQQ__Quoteline__c
                                                                          where SBQQ__Quote__c= :quoteId ]);
        if(qls.size() == 0){
            Formula.recalculateFormulas(new List<SBQQ__Quote__c>{quote});
            return new MyQuote(quote);
        }
        
        Map<Id, SBQQ__Subscription__c>  subs = new Map<Id, SBQQ__Subscription__c>();
        if(quote.SBQQ__Type__c == ConstantsUtil.QUOTE_TYPE_REPLACEMENT){
            subs = SEL_Subscription.getReplacedSubscriptionsByQuoteId(qls.keySet());  
        } else{
            set<id> subsIds = new set<id>();
            for(SBQQ__QuoteLine__c ql : qls.values()){
                if(ql.Subscription_to_Terminate__c!= null){
                    subsIds.add(ql.Subscription_to_Terminate__c);
                }
            }
            subs = SEL_Subscription.getTerminatedSubscriptionsById(subsIds);
        }
        
        decimal oldTotal = 0;
        decimal oldNetTotal = 0;
        for(SBQQ__Subscription__c s :subs.values()){
            if(!s.SBQQ__Product__r.One_time_Fee__c){
                try{
                    decimal term = decimal.valueof(s.Subscription_Term__c);
                    oldTotal += (s.SBQQ__QuoteLine__r.Total__c / term) * (term < 12?term:12);
                } catch(exception ex){}
            }
            integer baseTermRenew = integer.valueOf(s.RequiredBy__c == null?s.SBQQ__Product__r.Base_term_Renewal_term__c : subs.get(s.RequiredBy__c).SBQQ__Product__r.Base_term_Renewal_term__c);
            if(quote.SBQQ__Type__c == ConstantsUtil.QUOTE_TYPE_REPLACEMENT){
                Date serviceDate = (s.RequiredBy__c == null?s.Replaced_by_Product__r.SBQQ__StartDate__c:subs.get(s.RequiredBy__c).Replaced_by_Product__r.SBQQ__StartDate__c) -1;
                system.debug('baseTermRenew: '+baseTermRenew+' serviceDate: '+serviceDate+' sub: '+s);
                oldNetTotal += OrderProductTriggerHelper.calculateOrderItemTotalSingle(s,serviceDate, baseTermRenew);
            } else{
                Date serviceDate = getServiceDate(s,qls.values());
                system.debug('baseTermRenew: '+baseTermRenew+' serviceDate: '+serviceDate+' sub: '+s);
                oldNetTotal += OrderProductTriggerHelper.calculateOrderItemTotalSingle(s, serviceDate, baseTermRenew);
            }
        }
        decimal newTotal = 0;
        for(SBQQ__QuoteLine__c s :qls.values()){
            if(!s.SBQQ__Product__r.One_time_Fee__c){
                try{
                    decimal term = decimal.valueof(s.Subscription_Term__c);
                    newTotal += (s.Total__c / term) * (term < 12?term:12);
                } catch(exception ex){}
            }
        }
        
        quote.New_EW__c = newTotal.setScale(2);
        quote.Old_EW__c = oldTotal.setScale(2);
        system.debug('oldNetTotal::'+oldNetTotal);
        quote.Net_Order_Value__c = quote.Total_Net_Amount__c + oldNetTotal;// + because oldNetTotal is negative value
        
        if(updateQuote){
            update quote;
        }
        
        //Formula.recalculateFormulas(new List<SBQQ__Quote__c>{quote});
        quote.recalculateFormulas();
        return new MyQuote(quote);
    }
    
    private static Date getServiceDate(SBQQ__Subscription__c s,List<SBQQ__QuoteLine__c> qls){
        for(SBQQ__QuoteLine__c ql : qls){
            if(ql.Subscription_to_Terminate__c == s.Id || ql.Subscription_to_Terminate__c == s.RequiredBy__c){
                return ql.SBQQ__StartDate__c - 1;
            }
        }
        return Date.today();
    }
    
    public class MyQuote{
        Set<string> QuoteStatus = new Set<string>{ConstantsUtil.QUOTE_STATUS_PRESENTED, ConstantsUtil.QUOTE_STATUS_APPROVED, ConstantsUtil.QUOTE_STATUS_REJECTED};
        @AuraEnabled public SBQQ__Quote__c Quote {get; set;}
        @AuraEnabled public boolean AllowRefresh { get {
            if(this.Quote == null) return false;
            if(QuoteStatus.contains(this.Quote.SBQQ__Status__c)) return false;
            return true;
        }
                                                 }
        public MyQuote(SBQQ__Quote__c quote){
            this.Quote = quote;
            system.debug('Quote::'+quote);
        }
    }
}