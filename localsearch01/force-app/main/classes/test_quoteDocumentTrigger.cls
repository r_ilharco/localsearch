@istest 
public class test_quoteDocumentTrigger {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
    }
    
    @testsetup static void setup(){
        insertbypassflownames();
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        insert acc;
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
        insert cont;
        
        Pricebook2 pbDMC = new Pricebook2();
        pbDMC.ExternalId__c = 'PB2';
        pbDMC.Name = 'Telesales';
        pbDMC.IsActive = true;
        insert pbDMC;
        
        opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
        opp.Pricebook2Id = pbDMC.Id;
        insert opp;
        
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote.SBQQ__PriceBook__c = pbDMC.Id;
        insert quote;
        document doc = new document();
        doc.name = 'testname';
        doc.Body = Blob.valueOf('testbody');
        doc.FolderId = UserInfo.getUserId();
        insert doc;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'Telesales Template 2020 v1.0 - IT';
        insert template;
        
        SBQQ__QuoteDocument__c document = test_dataFactory.generateQuoteDocument(quote.id);
        document.SBQQ__QuoteTemplate__c = template.id;
        document.SBQQ__DocumentId__c = doc.id;
        insert document;
        
        SBQQ__QuoteDocument__c document1 = test_dataFactory.generateQuoteDocument(quote.id);
        document1.SBQQ__DocumentId__c = doc.id;
        document1.SBQQ__Template__c = template.id;
        insert document1;
        
        System.debug('--->document:' + JSON.serialize(document));
        System.debug('--->document1:' + JSON.serialize(document1));
        
        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = document.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        cdlList.add(cdl);
        
        ContentDocumentLink cdl2 = new ContentDocumentLink();
        cdl2.LinkedEntityId = document1.Id;
        cdl2.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl2.shareType = 'V';
        
        cdlList.add(cdl2);
        
        insert cdlList;
    }   

    @istest static void test_createcontentversion(){
        list<sbqq__quoteDocument__c> documents = [select id,SBQQ__DocumentId__c,SBQQ__QuoteTemplate__c from sbqq__quoteDocument__c];
        map<id,sbqq__quoteDocument__c> mapdoc = new map<id,sbqq__quoteDocument__c>(documents);
        test.startTest();
        QuoteDocumentTriggerHelper.createContentVersion(mapdoc);
        test.stopTest();
    }
    
    @istest static void test_qdthandler(){
        list<sbqq__quoteDocument__c> documents = [select id,SBQQ__DocumentId__c,SBQQ__QuoteTemplate__c, SBQQ__Quote__c from sbqq__quoteDocument__c];
        map<id,sbqq__quotedocument__c> mapd = new map<id,sbqq__quotedocument__c>(documents);
        quotedocumenttriggerhandler qdth = new quotedocumenttriggerhandler();
        qdth.beforeinsert(documents);
        qdth.beforedelete(mapd);
        qdth.afterdelete(mapd);
        qdth.afterundelete(mapd);
    }
    
    @IsTest
    static void test_QuoteDocumentTriggerHandler(){
        QuoteDocumentTriggerHandler.triggerName = 'QuoteDocumentTriggerHandler';
        System.debug(QuoteDocumentTriggerHandler.triggerName);
        QuoteDocumentTriggerHandler.disableTrigger = true;
        System.debug(QuoteDocumentTriggerHandler.disableTrigger);
        QuoteDocumentTriggerHandler.disableTrigger = NULL;
    }
    
    @IsTest
    static void test_updateQuotesStatusOfExpiredORRevokedDocuments()
    {
        Map<Id, SBQQ__QuoteDocument__c> newItems = new Map<Id, SBQQ__QuoteDocument__c>();
        Map<Id, SBQQ__QuoteDocument__c> oldItems = new Map<Id, SBQQ__QuoteDocument__c>();
        List<SBQQ__QuoteDocument__c> documents = [select id,SBQQ__DocumentId__c,SBQQ__QuoteTemplate__c, SBQQ__Quote__c from SBQQ__QuoteDocument__c];
        SBQQ__QuoteDocument__c newQD = documents[0];
        SBQQ__QuoteDocument__c oldQD = documents[0].clone(true, true, false, false);
        
        newQD.SBQQ__SignatureStatus__c = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_REVOKED;
        oldQD.SBQQ__SignatureStatus__c = NULL;
        
        newItems.put(newQD.Id, newQD);
        oldItems.put(oldQD.Id, oldQD);
        
        Test.startTest();
        	QuoteDocumentTriggerHelper.updateQuotesStatusOfExpiredORRevokedDocuments(newItems, oldItems);
        Test.stopTest();
    }
    
    @IsTest
    static void test_updateQuotesOfManuallySignedDocuments()
    {
        List<SBQQ__QuoteDocument__c> documents = [select id,SBQQ__DocumentId__c,SBQQ__QuoteTemplate__c, SBQQ__Quote__c, SBQQ__SignatureStatus__c from SBQQ__QuoteDocument__c];
        SBQQ__QuoteDocument__c newQD = documents[0];
        System.debug('--->' + JSON.serialize(newQD));
        newQD.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        newQD.ManualProcess__c = false;
        newQD.NamirialSignature_ProcessType__c = NULL;
        
        update newQD;
        
        Test.startTest();
            newQD.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
            newQD.ManualProcess__c = true;
            newQD.NamirialSignature_ProcessType__c = NULL;
        	update newQD;
        Test.stopTest();
    }
    
    @IsTest
    static void test_updateSignedDocuments()
    {
        List<SBQQ__QuoteDocument__c> documents = [select id,SBQQ__DocumentId__c,SBQQ__QuoteTemplate__c, SBQQ__Quote__c, SBQQ__SignatureStatus__c from SBQQ__QuoteDocument__c];
        Test.startTest();
        	SBQQ__QuoteDocument__c newQD = documents[0];
        	newQD.ManualProcess__c = false;
            newQD.SBQQ__SignatureStatus__c = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_IN_PROGRESS;
        	update newQD;
        	newQD.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        	update newQD;
            newQD.SBQQ__SignatureStatus__c = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_FAILED;
        	update newQD;
        Test.stopTest();
    }

    @IsTest
    static void test_updateTerritoryInformationOnQuote()
    {
        Map<Id, SBQQ__QuoteDocument__c> newItems = new Map<Id, SBQQ__QuoteDocument__c>();
        Map<Id, SBQQ__QuoteDocument__c> oldItems = new Map<Id, SBQQ__QuoteDocument__c>();
        List<SBQQ__QuoteDocument__c> documents = [select id, Account_Owner__c, SBQQ__Quote__c from  SBQQ__QuoteDocument__c];
        system.debug(documents);
        SBQQ__QuoteDocument__c newQD = documents[0];
        SBQQ__QuoteDocument__c oldQD = documents[0].clone(true, true, false, false);
        
        newQD.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        oldQD.SBQQ__SignatureStatus__c = NULL;

        newItems.put(newQD.Id, newQD);
        oldItems.put(oldQD.Id, oldQD);

        Test.startTest();
        	QuoteDocumentTriggerHelper.updateTerritoryInformationOnQuote(newItems, oldItems);
        Test.stopTest();
    }

    @IsTest
    static void test_updateTerritoryInformationOnQuote()
    {
        Map<Id, SBQQ__QuoteDocument__c> newItems = new Map<Id, SBQQ__QuoteDocument__c>();
        Map<Id, SBQQ__QuoteDocument__c> oldItems = new Map<Id, SBQQ__QuoteDocument__c>();
        List<SBQQ__QuoteDocument__c> documents = [select id, Account_Owner__c, SBQQ__Quote__c from  SBQQ__QuoteDocument__c];
        system.debug(documents);
        SBQQ__QuoteDocument__c newQD = documents[0];
        SBQQ__QuoteDocument__c oldQD = documents[0].clone(true, true, false, false);
        
        newQD.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        oldQD.SBQQ__SignatureStatus__c = NULL;

        newItems.put(newQD.Id, newQD);
        oldItems.put(oldQD.Id, oldQD);

        Test.startTest();
        	QuoteDocumentTriggerHelper.updateTerritoryInformationOnQuote(newItems, oldItems);
        Test.stopTest();
    }
    
}