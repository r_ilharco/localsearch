@isTest
public with sharing class OrderTriggerTest {
    
    @TestSetup
    static void makeData(){
        
        
    }

    @isTest
	public static void testmethod1()
    {          
        try{
            
            Test.startTest();
            List<Account> accList = Test_DataFactory.createAccounts('superman',1);
            insert accList;
            List<Contract> contrList = Test_DataFactory.createContracts(accList[0].Id, 1);
            insert contrList;
            List<Order> ordList = Test_DataFactory.createOrders(accList[0], contrList[0], ConstantsUtil.ORDER_TYPE_TERMINATION, 1);
            insert ordList;
            for(Order ord: ordList){
                ord.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                ord.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
                ord.Type = ConstantsUtil.ORDER_TYPE_UPGRADE;
            }
            Order childOrd = new Order();
            childOrd.Name = String.valueOf((Date.Today()));
			childOrd.AccountId = ordList[0].AccountId;
			childOrd.EffectiveDate = Date.Today();
			childOrd.ContractId = ordList[0].ContractId;
			childOrd.Type = ConstantsUtil.ORDER_TYPE_UPGRADE;
			childOrd.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
			childOrd.Custom_type__c = ConstantsUtil.ORDER_TYPE_TERMINATION;
            childOrd.Parent_Order__c = ordList[0].Id;
            insert childOrd;
            
            Test.stopTest();
            update ordList;
        }catch(Exception e){	                            
           	system.debug('error: '+e.getMessage()+' at '+e.getLineNumber());
        }
    }
}