@isTest
public class BillingProfileNamePBTest {

    @isTest 
    static void testUpdateBillingProfile (){
        
        Account accountt= new Account(Name='BillingTestAccount',
                        				PreferredLanguage__c = 'German',
                          				POBox__c='Ramna'  ,
                           				P_O_Box_City__c	='Dhaka',          
                           				P_O_Box_Zip_Postal_Code__c='Dhaka-1000'	          
                        );
    				
					insert accountt;
    				System.debug('Acc: '+accountt);
      
     
        Contact contactt= New Contact(
          								Phone='01700000000',
                                    	Email='i@gmail.com',
            							AccountId= accountt.Id,
        								PO_Box__c= 'Ramna',
         								PO_BoxCity__c= 'Dhaka', 
         								PO_BoxZip_PostalCode__c	='Dhaka-1000',
         								LastName = 'Test',
         								FirstName='Chu'
       							 	);
        
        			insert contactt;  
      				System.debug('contact: '+contactt);
       
        
        
        
    	Billing_Profile__c billingProfile = new Billing_Profile__c(    
        															Billing_Name__c = 	accountt.Name + ' ' + 'Billing Profile' ,
        															Customer__c = accountt.Id,
    																P_O_Box__c= 'Ramna',
        															P_O_Box_City__c	= 'Dhaka',
        															P_O_Box_Zip_Postal_Code__c='Dhaka-1000',	
    																Billing_Contact__c =contactt.Id ,
        															Billing_Language__c='German'
    															);
       
        
        				test.startTest();
       					insert billingProfile;
        				
      					
        				System.debug('billingProfile: '+billingProfile);
       					
        				
        				update billingProfile;
        				test.stopTest();
						
        				System.debug('billingProfile: '+billingProfile);
      
        		string billingName = billingProfile.Billing_Name__c;
        		System.assertEquals('BillingTestAccount Billing Profile' , billingName);
 
    }  
}