@isTest
global class AbacusTransferHttpCalloutMock implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request){
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
//      response.setBody('{"provatype": ["provacontent"]}');
        response.setStatusCode(401); 
        return response; 
    }
}