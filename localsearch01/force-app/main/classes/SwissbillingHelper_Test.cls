@isTest
public class SwissbillingHelper_Test {

    private static Id priceBookId;
    private static List<Product2> products;
    private static List<PricebookEntry> pricebookEntries;
	private static List<Account> accounts;
    private static List<Billing_Profile__c> billingProfiles;
	private static List<Contact> contacts;
	private static List<Opportunity> opportunities;
	private static List<SBQQ__Quote__c> quotes;
	private static List<SBQQ__QuoteLine__c> quoteLines;
	private static List<Contract> contracts;
	private static List<SBQQ__Subscription__c> subscriptions;
    private static List<Invoice__c> invoices;
        
    private static void createPricebook() {
        if(Test.isRunningTest()) {
            priceBookId = Test.getStandardPricebookId();
        } else {
            priceBookId = [SELECT Id from PriceBook2 WHERE IsActive=TRUE and IsStandard=TRUE LIMIT 1].Id;
        }        
        products = new List<Product2>();
        products.add(new Product2(ProductCode='P-001', Description='Product 001 Description', Name='Product 001', AutomaticRenew__c=TRUE, 
            BillingChannel__c='Mail;Print', SBQQ__BillingFrequency__c='Annual', SBQQ__Component__c=FALSE, KTRCode__c='KRT-001', MultiplePlaceIDAllowed__c=FALSE,
            PlaceIDRequired__c = 'Yes', SBQQ__SubscriptionType__c='Renewable', SBQQ__Taxable__c=TRUE, SBQQ__TaxCode__c='Standard', ExternalId='PROD-001'));
        products.add(new Product2(ProductCode='P-002', Description='Product 002 Description', Name='Product 002', AutomaticRenew__c=TRUE, 
            BillingChannel__c='Mail;Print', SBQQ__BillingFrequency__c='Annual', SBQQ__Component__c=FALSE, KTRCode__c='KRT-002', MultiplePlaceIDAllowed__c=FALSE,
            PlaceIDRequired__c = 'Yes', SBQQ__SubscriptionType__c='Renewable', SBQQ__Taxable__c=TRUE, SBQQ__TaxCode__c='Standard', ExternalId='PROD-002'));
        insert products;
        
        pricebookEntries = new List<PricebookEntry>();
        pricebookEntries.add(new PricebookEntry(IsActive=TRUE, UnitPrice=300, UseStandardPrice=FALSE, 
            PriceBook2Id=priceBookId, Product2Id=products[0].Id));
        
        pricebookEntries.add(new PricebookEntry(IsActive=TRUE, UnitPrice=500, UseStandardPrice=FALSE, 
            PriceBook2Id=priceBookId, Product2Id=products[1].Id));        
        insert pricebookEntries;
    }
    
    private static void createAccounts() {
        accounts = new List<Account>();
    	accounts.add(new Account(Name='Test 1', BillingStreet='Cape Cannaveral Blvd, 10', BillingState='TX', BillingCity='Texas', BillingPostalCode = '10001',
             BillingCountry='US', GoldenRecordID__c='GR-001', Type='Kunde', Customer_number__c = '123456789'));
		insert accounts;
        contacts = new List<Contact>();
        contacts.add(new Contact(FirstName='Test', LastName='Contact', email='ssineriz@seashellinformatics.com', GoldenRecordID__c='GR-C-001', 
            	Language__c='Italian', AccountId = accounts[0].Id, MailingCity='Turin', MailingCountry='Italy', MailingPostalCode='11000', MailingState='TO', MailingStreet='Piazzale Lagosta, 2'));
        insert contacts;
        billingProfiles = new List<Billing_Profile__c>();
        billingProfiles.add(new Billing_Profile__c (Name='BP-Print-Test 1', Billing_City__c='Texas', Billing_Contact__c = contacts[0].Id,
                Billing_Country__c = 'IT', Billing_Language__c='Italian', Billing_Name__c='B Test Contact', Billing_Postal_Code__c='B-10001',
                Billing_Street__c = 'B-Cape Cannaveral Blvd, 10', Channels__c = 'Print;Mail', Grouping_Mode__c = '1 - Standard (Monthly)', Is_Default__c = TRUE,
                Mail_address__c = 'ssineriz@seashellinformatics.com', Phone__c = '123-555-B', Process_Mode__c = '0 - Standard', Customer__c=accounts[0].Id));
        insert billingProfiles;
        
    } 
    
    private static void createVatSchedules() {
        insert new List<Vat_Schedule__c> {
            new Vat_Schedule__c(Tax_Code__c = 'Standard', Start_Date__c=null, End_Date__c=Date.newInstance(2020, 2, 15), Value__c=7.7, Name='Standard 7.7%'),
    		new Vat_Schedule__c(Tax_Code__c = 'Standard', Start_Date__c=Date.newInstance(2020, 2, 16), End_Date__c=null, Value__c=7.9, Name='Standard 7.9%'),
            new Vat_Schedule__c(Tax_Code__c = 'Reduced', Start_Date__c=null, End_Date__c=null, Value__c=2.5, Name='Reduced 2.5%')
        };
	}
    
    private static void createPrintingProfiles() {
        Printing_profile__c pp1 = new Printing_profile__c(
            Contact_Mail__c = 'billing@localsearch.ch', Contact_Person__c='Mr. Billing', Contact_Phone__c='058 262 77 70',
			Contract_Grouping_Footer__c = null,
			Contract_Grouping_Header__c = 'Das ist ein freier Multiline-Header-Text für den Vertrag, der den Vertrag beschreibt oder sonstige wichtige Infos zu den Produkten im Vertrag beinhaltet. Was auch immer sinnvoll und zweckmässig ist, damit der Kunde die Rechnung besser verstehen kann.',
			Credit_Note_Grouping_Footer__c = null,
			Credit_Note_Grouping_Header__c ='Das ist ein freier Multiline-Header-Text für den Credit Note, der den Vertrag beschreibt oder sonstige wichtige Infos zu den Produkten im Vertrag beinhaltet. Was auch immer sinnvoll und zweckmässig ist, damit der Kunde die Rechnung besser verstehen kann.',
			Doc_Type__c = 'FAK',
			Footer__c = 'Sie können auf tel.search.ch Ihren Eintrag jederzeit ändern. Das Passwort dazu lautet http://tel.search.ch/kirchberg/hauptstrasse-11/liechti-optik-ag: 52a19478',
			From_Date__c = null,
			Header__c = 'Wir danken für Ihren geschätzten Auftrag und verrechnen gerne unsere Leistungen wie folgt:',
			Header_Logo__c = '/files/localsearch_logo.jpg',
			Language__c = 'Italian', Name = 'Printing profile FAK-IT', Sender__c = 'Address Line 1\nAddress Line 2\nAddress Line 3',
            Web_Site__c = 'http://localsearch.ch',
            Title__c = 'Rechnung ', To_Date__c = null, Notify_To__c = 'a@b.c\r\nb@b.c\r\nc@b.c'
        );
        insert pp1;
        List<Printing_Profile_Footer__c> footers = new List<Printing_Profile_Footer__c>();
        footers.add(new Printing_Profile_Footer__c (
                Position__c = 1, Printing_Profile__c = pp1.id,
                Name = 'Printing profile footer FAK-IT - 1', Text__c = null, Type__c = 'Image')
        );
        footers.add(new Printing_Profile_Footer__c (
                Image_Url__c = '/files/localsearch_logo.jpg', Position__c = 2, Printing_Profile__c = pp1.id,
                Name = 'Printing profile footer FAK-IT - 2', Text__c = null, Type__c = 'Image')
        );
        footers.add(new Printing_Profile_Footer__c (
                Image_Url__c = null, Position__c = 3, Printing_Profile__c = pp1.id,
                Name = 'Printing profile footer FAK-IT - 3', Text__c = 'Swissbilling SA, Rue du Caudray 4, 1020 Renens', Type__c = 'Text')
        );
        insert footers;

        List<ContentVersion> contentVersions = new List<ContentVersion>();
        contentVersions.Add(new ContentVersion(
            Title = 'Test PrintingProfile',
            PathOnClient = 'Test.pdf',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        ));
        contentVersions.Add(new ContentVersion(
            Title = 'Test PrintingProfileFooter',
            PathOnClient = 'Logo.png',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        ));
        insert contentVersions;
        List<ContentDocument> docs = [SELECT Id FROM ContentDocument];
        
        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
        docLinks.add(new ContentDocumentLink(LinkedEntityId = pp1.id, ContentDocumentId = docs[0].Id, shareType = 'V'));
        docLinks.add(new ContentDocumentLink(LinkedEntityId = footers[0].id, ContentDocumentId = docs[1].Id, shareType = 'V'));
        insert docLinks;        
    }
    
    private static void createOpportunities() {
        /*
        Contract con = new Contract(AccountId=accounts[0].id, StartDate=Date.Today() , Status='Draft');
        insert con;
		*/
        opportunities = new List<Opportunity>();
        opportunities.add(new Opportunity(Name='Test Opportunity', CloseDate=Date.today() /*Date.newInstance(2018, 10, 1)*/, StageName='Proposal', SBQQ__Contracted__c=TRUE,
            SBQQ__QuotePricebookId__c=priceBookId, PriceBook2Id=priceBookId, Account=accounts[0], AccountId = accounts[0].Id));
        insert opportunities;
    }
    
    private static void createSettings() {
        Numbering_cs__c nc = Numbering_cs__c.getOrgDefaults();
        nc.SetupOwnerId = Userinfo.getOrganizationId();
        nc.Next_Invoice_Number__c = 400000000L;
        upsert nc;
        
        Customer_Number_cs__c cn = Customer_Number_cs__c.getOrgDefaults();
        cn.SetupOwnerId = Userinfo.getOrganizationId();
        cn.Next_Customer_Number__c = 200000000L;
        upsert cn;            
        
        Bypass_Triggers__c th = Bypass_Triggers__c.getOrgDefaults();
        th.SetupOwnerId = Userinfo.getOrganizationId();
        th.Trigger_Name__c = 'ProduceInvoiceTemporaryCreditNoteTrigger, ProduceInvoiceTemporarySBQQSubscriptionTrigger, QuoteTrigger, QuoteLineTrigger';
        upsert th;      
    }
    
    private static void createQuotes() {
        Place__c place = new Place__c(
			Account__c = accounts[0].Id,
			GoldenRecordID__c = accounts[0].GoldenRecordID__c,
			Nx_Amount__c = 27.15,
			Nx_RefundStatus__c = 'not used',
            Name = accounts[0].Name
        );
		insert place;
        Place__c place2 = new Place__c(
			Account__c = accounts[0].Id,
			GoldenRecordID__c = accounts[0].GoldenRecordID__c,
            Name = 'Place 2 ' + accounts[0].Name
        );
		insert place2;        
        quotes = new List<SBQQ__Quote__c>();
        quotes.add(new SBQQ__Quote__c(Billing_Channel__c='Print', SBQQ__BillingFrequency__c='Annual', Billing_Profile__c=billingProfiles[0].Id, 
            SBQQ__ContractingMethod__c='By Subscription End Date', SBQQ__Key__c='Q-001', SBQQ__Opportunity2__c=opportunities[0].Id, SBQQ__Primary__c=TRUE,
            SBQQ__PrimaryContact__c = contacts[0].Id, SBQQ__QuoteLanguage__c='Italian', Sales_Channel__c='Telesales', SBQQ__StartDate__c=Date.newInstance(2018, 10, 15),
            SBQQ__Status__c='Draft', SBQQ__SubscriptionTerm__c=12, SBQQ__Type__c='Quote',
        	SBQQ__PriceBook__c=priceBookId, SBQQ__Account__c=accounts[0].Id));
        insert quotes;
		quoteLines = new List<SBQQ__QuoteLine__c>();
        quoteLines.add(new SBQQ__QuoteLine__c(SBQQ__BillingFrequency__c='Annual', SBQQ__Bundled__c=FALSE,
        	SBQQ__Number__c = 1, SBQQ__Bundle__c = FALSE, SBQQ__SubscriptionScope__c='Group', SBQQ__PricebookEntryId__c=pricebookEntries[0].Id,
			SBQQ__ListPrice__c= 300, SBQQ__NetPrice__c= 300, place__c = place.Id,
            SBQQ__Product__c = pricebookEntries[0].Product2Id, SBQQ__Quantity__c=1, SBQQ__StartDate__c=Date.newInstance(2018, 10, 15),
            SBQQ__EndDate__c = null, SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionTerm__c=12,
            SBQQ__Taxable__c=TRUE, SBQQ__TaxCode__c='Standard', SBQQ__ChargeType__c='Recurring', SBQQ__BillingType__c='Advance',
            SBQQ__Quote__c=quotes[0].Id));
		quoteLines.add(new SBQQ__QuoteLine__c(SBQQ__BillingFrequency__c='Annual', SBQQ__Bundled__c=FALSE,
            SBQQ__Number__c = 2, SBQQ__Bundle__c = FALSE, SBQQ__SubscriptionScope__c='Group', SBQQ__PricebookEntryId__c=pricebookEntries[1].Id,
            SBQQ__ListPrice__c= 500, SBQQ__NetPrice__c= 500, place__c = place2.Id,
            SBQQ__Product__c = pricebookEntries[1].Product2Id, SBQQ__Quantity__c=1, SBQQ__StartDate__c=Date.newInstance(2018, 10, 15),
            SBQQ__EndDate__c = null, SBQQ__SubscriptionPricing__c='Fixed Price',SBQQ__SubscriptionTerm__c=12,
            SBQQ__Taxable__c=TRUE, SBQQ__TaxCode__c='Standard', SBQQ__ChargeType__c='Recurring', SBQQ__BillingType__c='Advance',
            SBQQ__Quote__c=quotes[0].Id));
		insert quoteLines;
        quotes[0].SBQQ__Status__c='Accepted';
        update quotes[0];
        
        opportunities[0].SBQQ__PrimaryQuote__c = quotes[0].id;
        opportunities[0].StageName = 'Closed Won';
        update opportunities;
    }
    
    private static void createContracts() {
        contracts = new List<Contract>();
        contracts.add(new Contract(StartDate = Date.newInstance(2018, 10, 15), ContractTerm=12, Status='Draft', SBQQ__SubscriptionQuantitiesCombined__c=TRUE,
            SBQQ__Opportunity__c = opportunities[0].Id, SBQQ__PreserveBundleStructureUponRenewals__c=TRUE, SBQQ__RenewalQuoted__c=FALSE,
        	SBQQ__Quote__c = quotes[0].Id, Pricebook2Id = priceBookId, AccountId=accounts[0].Id, CustomerSignedId = contacts[0].Id, Call_API__c = false
        ));
        insert contracts;
        contracts[0].Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE; // can't insert directly with status activated 
        update contracts;
        subscriptions = new List<SBQQ__Subscription__c>();
        subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c='Annual', SBQQ__BillingType__c='Advance', SBQQ__Bundled__c=FALSE,
            SBQQ__ChargeType__c='Recurring', SBQQ__CustomerPrice__c=300, SBQQ__Discount__c=0, SBQQ__ListPrice__c=300, SBQQ__NetPrice__c=300, SBQQ__Number__c=1,
            SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c=FALSE, SBQQ__Product__c=products[0].Id, SBQQ__Quantity__c=1, SBQQ__RenewalQuantity__c=1, SBQQ__QuoteLine__c=quoteLines[0].Id, 
            Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.newInstance(2018, 10, 15), SBQQ__SubscriptionEndDate__c=null,
        	SBQQ__Contract__c=contracts[0].Id, SBQQ__Account__c=accounts[0].Id, Status__c = 'Draft'
        	));
        subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c='Annual', SBQQ__BillingType__c='Advance', SBQQ__Bundled__c=FALSE,
            SBQQ__ChargeType__c='Recurring', SBQQ__CustomerPrice__c=500, SBQQ__Discount__c=0, SBQQ__ListPrice__c=500, SBQQ__NetPrice__c=500, SBQQ__Number__c=2,
            SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c=FALSE, SBQQ__Product__c=products[1].Id, SBQQ__Quantity__c=1, SBQQ__RenewalQuantity__c=1, SBQQ__QuoteLine__c=quoteLines[1].id, 
            Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.newInstance(2018, 10, 15), SBQQ__SubscriptionEndDate__c=null,
        	SBQQ__Contract__c=contracts[0].Id, SBQQ__Account__c=accounts[0].Id, Status__c = 'Draft'
        	));
        insert subscriptions;
    }
    
    private static void createAll() {
        createPricebook();
        createAccounts();
        createVatSchedules();
        createPrintingProfiles();
        createOpportunities();
        createSettings();
        createQuotes();
        createContracts();
    }
    
    static testmethod void test_functions() {
        boolean exceptionThrown = false;
        Date startDate = Date.newInstance(2018, 1, 1);
        Date stopDate = Date.newInstance(2018, 10, 31);
        
        System.assertEquals('2018-10-05 20:45:32+02:00', SwissbillingHelper.toISO8601String(DateTime.valueOf('2018-10-05 20:45:32+02:00')));
        System.assertEquals('2018-10-05', SwissbillingHelper.toISO8601String(Date.newInstance(2018, 10, 5)));
        System.assertEquals(null, SwissbillingHelper.toISO8601String((DateTime) null));
        System.assertEquals(null, SwissbillingHelper.toISO8601String((Date) null));
        System.assertEquals(121232343, SwissbillingHelper.getCustomerNumber('121\'232\'343'));
        try {
            long custNumber = SwissbillingHelper.getCustomerNumber('NOT A NUMBER');
        } catch (Exception ex) {
            System.assertEquals('Invalid long', ex.getMessage().substring(0,12));
            exceptionThrown = true;
        }
        System.assert(exceptionThrown, 'Expected an exception');
        exceptionThrown = false;
        
        System.assertEquals('Invoice', SwissbillingHelper.getDocTypeByCode('FAK'));
        System.assertEquals('CreditNote', SwissbillingHelper.getDocTypeByCode('GUT'));
        System.assertEquals(null, SwissbillingHelper.getDocTypeByCode('NONE'));
        
        System.assertEquals('mail', SwissbillingHelper.getBillingChannelValue('Mail'));
        System.assertEquals('print', SwissbillingHelper.getBillingChannelValue('Print'));
        System.assertEquals(null, SwissbillingHelper.getBillingChannelValue('NONE'));
        
        System.assertEquals(new List<string> {null, null}, SwissbillingHelper.get2LinesAddress(null, null));
        System.assertEquals(new List<string> {'Street Name, 20', null}, SwissbillingHelper.get2LinesAddress('Street Name, 20', null));
        System.assertEquals(new List<string> {'Street Name, 20', 'Baltimore'}, SwissbillingHelper.get2LinesAddress('Street Name, 20', 'Baltimore'));
        System.assertEquals(new List<string> {'Street Name, 20', 'Lake building'}, SwissbillingHelper.get2LinesAddress('Street Name, 20\nLake building', null));
        System.assertEquals(new List<string> {'Street Name, 20', 'Lake building Baltimore'}, SwissbillingHelper.get2LinesAddress('Street Name, 20\nLake building\nBaltimore', null));
        System.assertEquals(new List<string> {'Street Name, 20', 'Lake building Baltimore'}, SwissbillingHelper.get2LinesAddress('Street Name, 20\nLake building', 'Baltimore'));
        System.assertEquals(new List<string> {'Street Name, 20', 'Lake building Baltimore Arizona'}, SwissbillingHelper.get2LinesAddress('Street Name, 20\nLake building\nBaltimore', 'Arizona'));
        System.assertEquals('Some rich text', SwissbillingHelper.formatRichText('Some rich text'));
        System.assertEquals(null, SwissbillingHelper.getFullName(null, null));
        System.assertEquals('First', SwissbillingHelper.getFullName('First', null));
        System.assertEquals('Last', SwissbillingHelper.getFullName(null, 'Last'));
        System.assertEquals('First Last', SwissbillingHelper.getFullName('First', 'Last'));

        System.assertEquals(Date.newInstance(2018, 12, 31), SwissbillingHelper.getPeriodEndDate('Annual', startDate, null));
        System.assertEquals(Date.newInstance(2018, 6, 30), SwissbillingHelper.getPeriodEndDate('Semiannual', startDate, null));
        System.assertEquals(Date.newInstance(2018, 3, 31), SwissbillingHelper.getPeriodEndDate('Quarterly', startDate, null));
        System.assertEquals(Date.newInstance(2018, 1, 31), SwissbillingHelper.getPeriodEndDate('Monthly', startDate, null));
        System.assertEquals(stopDate, SwissbillingHelper.getPeriodEndDate('Annual', startDate, stopDate));
        try {
            Date endDate = SwissbillingHelper.getPeriodEndDate('Invoice Plan', startDate, null);
        } catch (ErrorHandler.GenericException ex) {
            System.assertEquals(ErrorHandler.ErrorCode.E_NOT_IMPLEMENTED.name(), ex.ErrorCode);
            System.assertEquals('Invoice Plan not implemented', ex.getMessage());
            exceptionThrown = true;
        }
        System.assert(exceptionThrown, 'Expected an ErrorHandler.GenericException');
        exceptionThrown = false;
        System.assertEquals(Date.newInstance(2018, 12, 31), SwissbillingHelper.getPeriodEndDate('INVALID', startDate, null));
        
        SwissbillingHelper.PeriodData data = new SwissbillingHelper.PeriodData(Date.newInstance(2019, 10, 15), Date.newInstance(2020, 10, 14), 1);
        System.assertEquals(Date.newInstance(2020, 9, 30), data.getAccrualEndDate());
        data = new SwissbillingHelper.PeriodData(Date.newInstance(2019, 10, 1), Date.newInstance(2020, 9, 30), 1);
        System.assertEquals(Date.newInstance(2020, 9, 30), data.getAccrualEndDate());
    }
    
    /*
    static testmethod void test_getPrintingProfiles() {
        createPrintingProfiles();
        boolean exceptionThrown = false;
        Printing_Profile__c pp = SwissbillingHelper.getPrintingProfile('Italian', 'FAK', 'PaymentSlip', 'Standard0', Date.today());
        System.assertEquals('Printing profile FAK-IT', pp.Name);
        try {
            pp = SwissbillingHelper.getPrintingProfile('ES', 'FAK', 'PaymentSlip', 'Standard0', Date.today());
        } catch (Exception ex) {
            System.assert(ex instanceOf SwissbillingHelper.SwissbillingHelperException, 'Invalid Exception Class');
            System.assertEquals(ErrorHandler.ErrorCode.E_NO_PRINTING_PROFILE.name(), ((SwissbillingHelper.SwissbillingHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        System.assert(exceptionThrown, 'Expected an exception');
        exceptionThrown = false;
        System.assertEquals(new List<string> {'a@b.c', 'b@b.c', 'c@b.c'}, 
                            SwissbillingHelper.getNotificationList(pp));
        System.assertEquals(new Map<string,string> { 'line1' => 'Address Line 1', 'line2' => 'Address Line 2', 'line3' => 'Address Line 3'}, 
                            SwissbillingHelper.getDocSender(pp));        
    }
    */
    /*
    static testmethod void test_InstallmentPeriods() {
        createAll();
		SBQQ__Subscription__c refSubscription = [SELECT SBQQ__StartDate__c, SBQQ__BillingFrequency__c, SBQQ__EndDate__c from SBQQ__Subscription__c where id=:subscriptions[0].Id];
        SwissbillingHelper.PeriodData data = SwissbillingHelper.getLatestPeriod(refSubscription, Date.newInstance(2018, 10, 15));
        System.assertEquals('15/10/2018 - 14/10/2019', data.toPeriodString());
        System.assertEquals(1, data.installment);
        System.assertEquals(Date.newInstance(2019, 9, 30), data.getAccrualEndDate());
        data = SwissbillingHelper.getLatestPeriod(refSubscription, null);
        data = SwissbillingHelper.getLatestPeriod(refSubscription, Date.newInstance(2019, 10, 15));
        System.assertEquals('15/10/2019 - 14/10/2020', data.toPeriodString());
        System.assertEquals(2, data.installment);
    }
	*/

    // static testmethod void test_PublicLinks() {
    //     //createAll();
    //     SwissbillingHelper.CheckPrintingProfilePublicLinks();
    // }
    
    /*
    static testMethod void test_SwissbillingPreparation() {
        createAll();
        List<SBQQ__Subscription__c> billableSubscriptions = Billing.getBillableSubscriptions();
        Billing.generateInvoices(billableSubscriptions);
        
    }
    */
}