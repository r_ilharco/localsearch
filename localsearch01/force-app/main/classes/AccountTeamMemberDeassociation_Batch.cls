global class AccountTeamMemberDeassociation_Batch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    
    global database.querylocator start(Database.BatchableContext bc){     
        Date today = Date.today();
        String sSQL = 'SELECT id, Expiration_Date__c, AccountId FROM AccountTeamMember WHERE Expiration_Date__c <= :today';
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<AccountTeamMember> scope){
        system.debug('scope: ' + scope);
        List<AccountTeamMember> teamMembersToDelete = new List<AccountTeamMember>();
        if(scope.size() > 0){
            for(AccountTeamMember teamMember : scope){
                teamMembersToDelete.add(teamMember);
            }
        }
        if(teamMembersToDelete.size() > 0){
            delete teamMembersToDelete;
        }
    }
    
    global void finish(Database.BatchableContext bc){     
        
    }
    
    global void execute(SchedulableContext sc){
        AccountTeamMemberDeassociation_Batch accountTeamMemberBatch = new AccountTeamMemberDeassociation_Batch(); 
       	Database.executebatch(accountTeamMemberBatch, 200);
    }
}