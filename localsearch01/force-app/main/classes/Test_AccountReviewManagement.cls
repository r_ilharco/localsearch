@isTest
public class Test_AccountReviewManagement {
    
    @testSetup
    public static void test_setupData(){
        
        User dmcUser = Test_DataFactory.createUsers('Sales', 1)[0];
        insert dmcUser;
        
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
		List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get(ConstantsUtil.PERMISSION_CREATEACC_LABEL);
        psain.AssigneeId = dmcUser.Id;
        psas.add(psain);
        insert psas;
    }
    
    @isTest
    public static void createAccountAsSales(){
        
        User dmcUser = [SELECT ID, Name, Profile_Name__c, isActive 
                        FROM User 
                        WHERE Profile_Name__c = :ConstantsUtil.SALESPROFILE_LABEL
                        AND isActive = true
                        LIMIT 1];
        
        Account dmcAccount = Test_DataFactory.createAccounts('Test Account', 1)[0];
        
        Test.startTest();
        insert dmcAccount;
        Test.stopTest();
        
    }
}