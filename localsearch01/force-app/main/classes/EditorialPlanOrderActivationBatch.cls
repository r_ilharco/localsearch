/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 25-11-2019
 * Sprint      : 
 * User Story  : 
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

global class EditorialPlanOrderActivationBatch implements Database.batchable<sObject>{
    
    private String editionId;
    
    public EditorialPlanOrderActivationBatch(String edId){
        this.editionId = edId;
    }
    
    global List<Order> start(Database.BatchableContext BC){
        List<Order> orders = [SELECT Id, status, SBQQ__Contracted__c 
                                  FROM Order 
                                  WHERE status = :ConstantsUtil.ORDER_STATUS_PUBLICATION AND Id IN (SELECT OrderId 
                                                                                                    FROM OrderItem 
                                                                                                    WHERE Edition__c = :editionId)];
        return orders;
    }
    
    global void execute(Database.BatchableContext info, List<Order> scope){
        List<Order> ordersToActivate = new List<Order>();
        for(Order o : scope){
            o.status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
            o.SBQQ__Contracted__c = true;
            ordersToActivate.add(o);
        }
        if(ordersToActivate.size() > 0){
        	update ordersToActivate;
        }
    }
    
    global void finish(Database.BatchableContext info){
        //...     
    }
}