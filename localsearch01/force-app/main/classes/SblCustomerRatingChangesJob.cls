public class SblCustomerRatingChangesJob implements Queueable, Database.AllowsCallouts {
    private DateTime startDate;
    private DateTime endDate;
    
    public SblCustomerRatingChangesJob() {
        // Without parameters, will check for last successful run, and fetch from that date.
        String className = String.valueOf(this).substring(0, String.valueOf(this).indexOf(':'));
        List<AsyncApexJob> lastJobs = [SELECT CreatedDate from AsyncApexJob where Status='Completed' 
                              and ApexClassId in (SELECT Id FROM ApexClass WHERE name=:className) 
                              Order By CreatedDate desc LIMIT 1];
        if(lastJobs.size() > 0) { this.startDate = lastJobs[0].CreatedDate; }
    }
    
    public SblCustomerRatingChangesJob(DateTime startDate, DateTime endDate) {
    	this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public void execute(QueueableContext context) {
		//Vincenzo Laudato 2019-06-14 *** AS-46 START
        BillingResults.CustomerRatingReport report = BillingResults.getCustomerRatingChanges(this.startDate, this.EndDate);
        
        if(report != null){
            Map<String, BillingResults.CustomerRatingUpdate> resultMap = new Map<String, BillingResults.CustomerRatingUpdate>();

            for(BillingResults.CustomerRatingUpdate rating : report.customer_ratings) {
                BillingResults.CustomerRatingUpdate storedRating = resultMap.get(rating.client_id);
                if(storedRating != null)
                    storedRating.mergeRating(rating);
                else
                    resultMap.put(rating.client_id, rating);
            }
            if(!resultMap.isEmpty()){
                Database.executeBatch(new SblCustomerRatingChangesBatch(resultMap), 500);
            }
        }
        //Vincenzo Laudato 2019-06-14 *** AS-46 END
    }
}