/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date        : 11-12-2019
 * Sprint      : 13
 * Work item   : SF2-562
 * Testclass   : Test_LeadChangeOwnerController
 * Package     : 
 * Description : Apex controller for LeadChangeOwner component
 * Changelog   : 
                
 */

public without sharing class LeadChangeOwnerController {
    
    /*A.L. Marotta 09-12-19 Start
    SF2-562, Wave E, Sprint 13 - change owner scenario for Lead: a Sales Manager can assign a Lead to a DMC*/
    @AuraEnabled
    public static ResponseHasUserGrant hasUserGrant_Lead()
    {
        ResponseHasUserGrant result = new ResponseHasUserGrant();
        Boolean okProfile = false;
        Boolean okPermissionSetConfiguration = false;
        List<String> validPermissionSets = new List<String>{ConstantsUtil.DMCPERMISSIONSET_DEVNAME, ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME};
        User currentUser = [SELECT Id, Name, Profile.Name, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Name IN :validPermissionSets) FROM User WHERE Id = :UserInfo.getUserId()];
   		
        System.debug('>>> currentUser.Profile.Name: ' + currentUser.Profile.Name);
        System.debug('>>> currentUser.PermissionSetAssignments.size(): ' + currentUser.PermissionSetAssignments.size());
        
        if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
        {
            okProfile = true;
        }
        
        if(currentUser.PermissionSetAssignments.size() > 0)
        {
            for(PermissionSetAssignment psa: currentUser.PermissionSetAssignments)
            {
                System.debug('>>>currentUser.PermissionSetAssignments.Name: ' + psa.PermissionSet.Name + ' Label: ' + psa.PermissionSet.Label);
                if( ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && (ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME.equalsIgnoreCase(psa.PermissionSet.Name) || (ConstantsUtil.DMCPERMISSIONSET_DEVNAME.equalsIgnoreCase(psa.PermissionSet.Name))))
                {
                    okPermissionSetConfiguration = true;
                } 
            }
        }else if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
        {
            okPermissionSetConfiguration = true;
        }
        
        result.allowed = okProfile && okPermissionSetConfiguration;
        System.debug('okProfile && okPermissionSetConfiguration = '+result.allowed);
        return result;
    }

    //A.L. Marotta 09-12-19 Wave E, Sprint 13 - updates lead owner during Change Owner process on lead record page
    @AuraEnabled
    public static ResponseHasUserGrant updateLeadOwner(String leadId)
    {

        Lead lead = [select id, OwnerId, New_Owner__c from Lead where id =: leadId];
        //A.L. Marotta 13-01-2020 - result variable for apex sharing result
        Boolean sharingToCurrentUserSucceeded = false;
        ResponseHasUserGrant response = new ResponseHasUserGrant();
        response.allowed = false;
        response.errorMessage = '';
        
        System.debug('lead: '+lead+', lead.OwnerId: '+lead.OwnerId+', lead.New_Owner__c: '+lead.New_Owner__c);
        if(lead != null && lead.OwnerId != null && lead.New_Owner__c != null){
        	
            List<GroupMember> currentUserGroupMembers = [select id, Group.Name, UserOrGroupId from GroupMember where UserOrGroupId =: UserInfo.getUserId()
                                                      and (Group.Name like 'VB %' OR Group.Name like 'VG %')];
            List<GroupMember> newOwnerGroupMembers = [select id, Group.Name, UserOrGroupId from GroupMember where UserOrGroupId =: lead.New_Owner__c
                                                     and (Group.Name like 'VB %' OR Group.Name like 'VG %')];
            List<PermissionSetAssignment> newOwnerPermissionSets = [select id, AssigneeId, PermissionSet.Name from PermissionSetAssignment where AssigneeId =: lead.New_Owner__c and PermissionSet.Name =: 'Digital_Marketing_Consultant' ];
            
            System.debug('NEW OWNER PERMISSION SETS: '+newOwnerPermissionSets);
            System.debug('NEW OWNER GROUP MEMBERS: '+newOwnerGroupMembers);
            System.debug('CURRENT USER GROUP MEMBERS: '+currentUserGroupMembers);
            if(newOwnerGroupMembers.isEmpty() || newOwnerPermissionSets.isEmpty()){
                response.allowed = false; //NEW OWNER IS NOT A DMC OR HE IS NOT IN ANY TERRITORY-RELATED GROUP
                response.errorMessage = Label.New_Owner_not_a_DMC_or_not_in_territory;
            }else if(!currentUserGroupMembers.isEmpty() && !newOwnerGroupMembers.isEmpty() && !newOwnerPermissionSets.isEmpty()){
                
                Boolean sameGroupName = false;
                for(GroupMember gmOld : currentUserGroupMembers){
                    String oldGroupName = gmOld.Group.Name;
                    for(GroupMember gmNew : newOwnerGroupMembers){
                        String newGroupName = gmNew.Group.Name;
                        if(newGroupName.equalsIgnoreCase(oldGroupName)){
                            sameGroupName = true;
                            break;
                        }
                    }
                    if(sameGroupName){
                        break;
                    }
                }
                
                if(!sameGroupName){
                    response.allowed = false;
                    response.errorMessage = Label.New_Owner_not_in_same_territory;
                }else{
                    lead.OwnerId = lead.New_Owner__c;
                    lead.New_Owner__c = null;
                    try{
                        update lead;
                        response.allowed = true;
                        response.errorMessage = '';
                        //A.L. Marotta 13-01-2020 - extend record editability to current user, that is, the Sales Manager
                        sharingToCurrentUserSucceeded = SharingUtility.shareLeadManually(lead.Id, Userinfo.getUserId(), 'Edit');
                    }catch(Exception e){
                        System.debug('Error: '+e.getMessage()+e.getStackTraceString());
                        response.allowed = false;
                        response.errorMessage = e.getMessage();
                    }
                    System.debug('**** APEX SHARING RESULT: '+sharingToCurrentUserSucceeded+' ****');
                }
                
            }
            
            
            
        }else{
            response.allowed = false;
            response.errorMessage = Label.Lead_owner_not_updated;
        }
        
        System.debug('RESPONSE IN APEX IS: '+response);
        return response;

    }
    
    public class ResponseHasUserGrant{
        @AuraEnabled
        public Boolean allowed;
        @AuraEnabled
        public String errorMessage;
    }
    
}