@isTest
public class OppTakeInChargeHandler_Test {
	
    @isTEst
    public static void changeOwner_test(){
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        
        insert account;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        
        
        
       	Map<String, Id> profilesMap = Test_DataFactory.getProfilesMap(); 
        List<User> userList = new List<User>();
        User telesalesUser = Test_DataFactory.generateUser();
        telesalesUser.ProfileId = profilesMap.get(ConstantsUtil.TELESALESPROFILE_LABEL);
        telesalesUser.Username = 'telesalestestuser@ls.ch.test';
        telesalesUser.Code__c = 'DMC';
        userList.add(telesalesUser);
        User salesUser = Test_DataFactory.generateUser();
        salesUser.ProfileId = profilesMap.get('Sales');
        salesUser.Username = 'salesUsertestuser@ls.ch.test';
        salesUser.Code__c = 'DMC';
 		userList.add(salesUser);
        UserTriggerHandler.disableTrigger = true;
        insert userList;
        UserTriggerHandler.disableTrigger = false;
        
        OpportunityTriggerHandler.disableTrigger = true;
        opportunity.OwnerId = telesalesUser.Id;
        insert opportunity;
        OpportunityTriggerHandler.disableTrigger = false;
        test.startTest();
        
        OppTakeInChargeHandler.changeOwner(opportunity.id);
        
        system.runAs(telesalesUser){
            OppTakeInChargeHandler.changeOwner(opportunity.id);
        }
        system.runAs(salesUser){
            OppTakeInChargeHandler.changeOwner(opportunity.id);
        }
        
        opportunity.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS;
        OpportunityTriggerHandler.disableTrigger = true;
        update opportunity;
		OppTakeInChargeHandler.changeOwner(opportunity.id);
        OpportunityTriggerHandler.disableTrigger = false;
        test.stopTest();
        

    }
}