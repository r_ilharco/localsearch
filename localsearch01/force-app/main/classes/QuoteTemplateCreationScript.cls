public class QuoteTemplateCreationScript {
    public void createQuoteTemplate(){
        SBQQ__QuoteTemplate__c qt = createTheTemplate();
        
        SBQQ__TemplateContent__c sr = createHeaderContent();
        SBQQ__TemplateSection__c ts = createSection('Header', qt.Id, sr.Id, 10);
        
		sr = createLineContent();
        ts = createSection('Items', qt.Id, sr.Id, 20);
        
        sr = createAfterQuoteLineContent();
        ts = createSection('After Quote Line', qt.Id, sr.Id, 30);
        
        sr = createTermContent();
        ts = createSection('Terms', qt.Id, sr.Id, 40);
        
		sr = createFooterContent();
        ts = createSection('Footer_Sign', qt.Id, sr.Id, 50);
    }
    
    public SBQQ__QuoteTemplate__c createTheTemplate() {
        SBQQ__QuoteTemplate__c qt = new SBQQ__QuoteTemplate__c (
        	Name = 'Quote German',
            SBQQ__DeploymentStatus__c = 'Deployed',
            SBQQ__PageHeight__c = 11.00,
            SBQQ__PageWidth__c  = 8.50,
            SBQQ__TopMargin__c = 1,
            SBQQ__BottomMargin__c = 1,
            SBQQ__LeftMargin__c = 0.5,
            SBQQ__RightMargin__c = 0.5,
            SBQQ__FooterHeight__c = 15,
            SBQQ__CompanyName__c  = 'localsearch Swisscom Directories AG',
            SBQQ__CompanyStreet__c = 'Förrlibuckstrasse 62',
            SBQQ__CompanyCity__c  = 'Zurich',
            SBQQ__CompanyPhone__c = '0800 86 80 86',
            SBQQ__CompanyPostalCode__c = '8005',
            SBQQ__CompanyCountry__c = 'Switzerland',
            SBQQ__FontFamily__c = 'Roboto',
            SBQQ__FontSize__c = 7.0,
            SBQQ__BorderColor__c = '000000',
            SBQQ__ShadingColor__c = 'FFFFFF',
            SBQQ__TermBodyIndent__c  = 15,
            LocalSearch_Logo__c = '', // <p><img src="https://energy-agility-5649-dev-ed--c.documentforce.com/servlet/rtaImage?eid=a0o9E000001BElL&amp;feoid=00N9E000002FzoK&amp;refid=0EM9E000000Cn1J" alt="rtaImage.jpg"></img></p>
            SBQQ__GroupGap__c = 15
        );
        insert qt;
        return qt;
    }
    
    public SBQQ__TemplateContent__c createHeaderContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'LS_QT_Header' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'LS_Header',
            SBQQ__Type__c = 'HTML',
            SBQQ__FontSize__c = 8,
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createLineContent() {
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'LS_Quote_Lines',
            SBQQ__Type__c = 'Line Items',
            SBQQ__TableStyle__c = 'Standard'
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createAfterQuoteLineContent() {
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'After_Quote_Line',
            SBQQ__Type__c = 'HTML',
            SBQQ__FontSize__c = 10,
            SBQQ__RawMarkup__c = '<p>Je nach Inseratetyp werden wir Ihnen vor der Publikation einen Probeabzug (Gut zum Druck/ Gut zur Aufschaltung) zustellen,damit Sie die M&#246;glichkeit einer Kontrolle haben.</p>'+
                '<p>&#160;</p>'+
                '<p><br />'+
                'Freundliche Gr&#252;sse<br />'+
                'Swisscom Directories AG</p>'+
                '<p>&#160;</p>'+
                '<p>&#160;</p>'+
                '<p><br />'+
                '<span style="font-size: 8px;"><span style="color: rgb(169, 169, 169);">(1) Auflage Online (local.ch): 4&#39;625&#39;000 Unique Clients (Stand Januar 2017, Quelle NET-Metrix Audit).<br />'+
                '(2) Aufschaltung sp&#228;testens 30 Tage nach Unterschrift.</span></span></p>');
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createTermContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'LS_QT_Terms' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'LS_Quote_Terms',
            SBQQ__Type__c = 'HTML',
            SBQQ__FontSize__c = 7,
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }

    public SBQQ__TemplateContent__c createFooterContent() {
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'LS_Quote_Footer',
            SBQQ__Type__c = 'Quote Terms'
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateSection__c createSection(String name, Id idTemp, Id idContent, Integer order) {
        SBQQ__TemplateSection__c ts = new SBQQ__TemplateSection__c (
        	Name = name,
            SBQQ__Content__c = idContent,
            SBQQ__Template__c = idTemp,
            SBQQ__DisplayOrder__c = order
        );
        insert ts;
        return ts;
    }
    
}