/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 09-17-2019
 * Sprint      : 9
 * Work item   : SF2-345, Sprint 9, Wave E - Update of Quote and Contract Template
 * Testclass   :
 * Package     : 
 * Description : Selector Class for Vat_Schedule__c
 * Changelog   : 
 */

public class SEL_VatSchedule {
	public static List<Vat_Schedule__c> getValidVATSchedules()
    {
        return [
                SELECT Id, Name, End_Date__c, Start_Date__c, Tax_Code__c, Value__c 
                FROM Vat_Schedule__c 
                WHERE (Start_Date__c <= TODAY AND End_Date__c >= TODAY) 
                OR (Start_Date__c = NULL AND End_Date__c = NULL) 
                OR (Start_Date__c = NULL AND End_Date__c >= TODAY) 
                OR (Start_Date__c <= TODAY AND End_Date__c = NULL)  
                ORDER BY End_Date__c DESC NULLS LAST,
                Start_Date__c DESC NULLS LAST 
            ];
    }
}