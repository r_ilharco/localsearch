global class Phase1QuoteAlignment implements Database.Batchable<AggregateResult>, Schedulable{
    
    global void execute(SchedulableContext sc) {
    	Phase1QuoteAlignment a = new Phase1QuoteAlignment(); 
    	Database.executeBatch(a, 200);
   	}
    
	global Iterable<AggregateResult> start(Database.BatchableContext BC){  
	
		List<AggregateResult> aggrRest = [SELECT SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.Phase1Source__c = true AND LastModifiedDate < YESTERDAY AND SBQQ__Quote__r.SBQQ__Status__c != 'Rejected' GROUP BY SBQQ__Quote__c LIMIT 2000];
		return  aggrRest;
	
	}
	
    /*global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.Phase1Source__c = true AND LastModifiedDate < YESTERDAY AND SBQQ__Quote__r.SBQQ__Status__c != \'' + ConstantsUtil.QUOTE_STATUS_REJECTED + '\' GROUP BY SBQQ__Quote__c LIMIT 2000');

    }*/
    
    global void execute(Database.BatchableContext info, List<AggregateResult> scope){
        
        Set<Id> quoteIds = new Set<Id>();
        for(AggregateResult quoteLine : scope){
            quoteIds.add((Id) quoteLine.get('SBQQ__Quote__c'));
        }
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT 	Id, 
                                               			Total__c, 
                                               			Package_Total__c, 
                                               			Subscription_Term__c, 
                                               			SBQQ__Quantity__c, 
                                               			SBQQ__ListPrice__c,
                                        				Base_term_Renewal_term__c, 
                                               			SBQQ__RequiredBy__c, 
                                               			SBQQ__RequiredBy__r.Base_term_Renewal_term__c,
                                        				SBQQ__Discount__c, 
                                               			SBQQ__NonDiscountable__c, 
                                               			SBQQ__AdditionalDiscountAmount__c, 
                                               			SBQQ__Quote__c,
                                        				SBQQ__Quote__r.SBQQ__CustomerDiscount__c 
                                              	FROM 	SBQQ__QuoteLine__c 
                                               	WHERE 	SBQQ__Quote__c IN :quoteIds];
        
        Map<Id, SBQQ__QuoteLine__c> qls = new Map<Id, SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c currentQuoteLine : quoteLines){
            qls.put(currentQuoteLine.Id, currentQuoteLine);
        }
        
        Map<Id, Set<SBQQ__QuoteLine__c>> hierachicalQlsMap = new Map<Id, Set<SBQQ__QuoteLine__c>>();
        
        for(SBQQ__QuoteLine__c currentQuoteLine : qls.values()){
            //In production environment the Subscription Term is always 12 months
            currentQuoteLine.Subscription_Term__c = '12';
            
            //Setting the base term on each Quote Line
            Decimal baseTerm;
            if(currentQuoteLine.Base_term_Renewal_term__c != null) baseTerm = currentQuoteLine.Base_term_Renewal_term__c;
            else baseTerm = currentQuoteLine.SBQQ__RequiredBy__r.Base_term_Renewal_term__c;
            
            //Calculate total and discount
            Decimal subTerm = Decimal.valueOf(currentQuoteLine.Subscription_Term__c);
            Double total = (Double)(currentQuoteLine.SBQQ__Quantity__c*currentQuoteLine.SBQQ__ListPrice__c*(subTerm/baseTerm));
            if(currentQuoteLine.SBQQ__Discount__c != null && !currentQuoteLine.SBQQ__NonDiscountable__c){
                Double discountAmount = (Double)(total/100)*currentQuoteLine.SBQQ__Discount__c;
                currentQuoteLine.Total__c = total-discountAmount;
            }
            else if(currentQuoteLine.SBQQ__AdditionalDiscountAmount__c != null && !currentQuoteLine.SBQQ__NonDiscountable__c) currentQuoteLine.Total__c = (Double) total-currentQuoteLine.SBQQ__AdditionalDiscountAmount__c;
            else if(currentQuoteLine.SBQQ__Quote__r.SBQQ__CustomerDiscount__c != null && !currentQuoteLine.SBQQ__NonDiscountable__c){
                Double discountAmount = (Double)(total/100)*currentQuoteLine.SBQQ__Quote__r.SBQQ__CustomerDiscount__c;
                currentQuoteLine.Total__c = total-discountAmount;
            }
            else{
                currentQuoteLine.Total__c = total;
            }
            
            if(String.isNotBlank(currentQuoteLine.SBQQ__RequiredBy__c)){
                if(!hierachicalQlsMap.containsKey(currentQuoteLine.SBQQ__RequiredBy__c)){
                    hierachicalQlsMap.put(currentQuoteLine.SBQQ__RequiredBy__c, new Set<SBQQ__QuoteLine__c>{currentQuoteLine});
                }
                else{
                    hierachicalQlsMap.get(currentQuoteLine.SBQQ__RequiredBy__c).add(currentQuoteLine);
                }
            }
        }
        
        for(Id currentKey : hierachicalQlsMap.keySet()){
            Double packageTotal = qls.get(currentKey).Total__c;
            for(SBQQ__QuoteLine__c currentQL : hierachicalQlsMap.get(currentKey)){
                packageTotal += currentQL.Total__c;
            }
            qls.get(currentKey).Package_Total__c = packageTotal;
        }
        
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;
        update qls.values();
        QuoteTriggerHandler.disableTrigger = false;
		QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    global void finish(Database.BatchableContext info){}
}