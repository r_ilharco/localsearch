@isTest
public class Test_InvoiceTrigger {
	
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        //Insert Account 
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;   
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_StatusUpdates(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Total__c = 500,
                                           Customer__c = account.Id);
        insert invoice;        
        Test.startTest();
        invoice.Payment_Status__c = '0';
        invoice.Open_Amount__c = 200;
        update invoice;
        invoice.Payment_Status__c = '1';
        update invoice;
        invoice.Payment_Status__c = 'C1';
        update invoice;
        invoice.Payment_Status__c = '2';
        update invoice;
        InvoiceTriggerHandler.disableTrigger = false;
        Test.stopTest();
    }
    @isTest
    public static void test_delete(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Total__c = 500,
                                           Customer__c = account.Id);
        insert invoice;   
        delete invoice;
    }
}