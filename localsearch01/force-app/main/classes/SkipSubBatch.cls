public class SkipSubBatch implements Database.Batchable<SObject>, Schedulable{
    private static final String BILLING_SETTINGS_METADATA_LABEL = ConstantsUtil.BILLING_SETTINGS_METADATA_LABEL;
    private static final String CONTRACT_STATUS_ACTIVE = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
    private static final String SUBSCRIPTION_STATUS_ACTIVE =  ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
    private static final String ORDER_TYPE_AMENDMENT = ConstantsUtil.ORDER_TYPE_AMENDMENT;

    public Database.Querylocator start(Database.BatchableContext bc) {
        Integer daysForBilling = 0;
		List<Billing_Setting__mdt> settingSngList = new List<Billing_Setting__mdt>([SELECT Days_Until_Billing__c FROM Billing_Setting__mdt WHERE Label = :BILLING_SETTINGS_METADATA_LABEL]);
        if(settingSngList.size() > 0 && settingSngList[0].Days_Until_Billing__c != null){
            daysForBilling = (Integer)settingSngList[0].Days_Until_Billing__c;
        }
        Date dateToCheck = Date.today().addDays(-daysForBilling).addDays(+1);
        String subscriptionQiery = 'SELECT Id FROM SBQQ__Subscription__c WHERE Subsctiption_Status__c = :SUBSCRIPTION_STATUS_ACTIVE AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c  != null '+
               'AND SBQQ__Contract__r.Status = :CONTRACT_STATUS_ACTIVE AND (SBQQ__EndDate__c = NULL OR SBQQ__EndDate__c > TODAY ) AND One_time_Fee_Billed__c = false '+
               'AND SBQQ__NetPrice__c != 0 AND SBQQ__QuoteLine__c != NULL AND (SBQQ__TerminatedDate__c = NULL OR SBQQ__TerminatedDate__c > TODAY ) AND Total__c != 0 ' +
               'AND ((Next_Invoice_Date__c = NULL AND SBQQ__StartDate__c <= :dateToCheck) OR (Next_Invoice_Date__c != NULL AND Next_Invoice_Date__c < :dateToCheck)) ' +
               'AND Amount_Checked__c = false AND SBQQ__OrderProduct__r.Order.Type != :ORDER_TYPE_AMENDMENT';
        return Database.getQueryLocator(subscriptionQiery);
    }

    public void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> scope) {
        SkipSubBatchHandler.processRecords(scope);
    }

    public void execute(SchedulableContext sc){
        SkipSubBatch skipSubBatch = new SkipSubBatch(); 
        database.executebatch(skipSubBatch, 150);
    }

    public void finish(Database.BatchableContext bc){
        
    }
}
