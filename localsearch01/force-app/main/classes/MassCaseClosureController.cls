public class MassCaseClosureController {
    public List<string> casesIds {get;set;}
    public integer countCases {get;set;}
    
    public MassCaseClosureController(ApexPages.StandardSetController controller){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error Message.'));
        List<Case> caseList = New List<Case>();
        List<string> caseIdsTemp = new List<Id>();
        set<Id> caseIdSet = new set<Id>();
        caseList = controller.getSelected();
        system.debug('caseList: ' + caseList);
        for(Case cases : caseList){
            caseIdsTemp.add(cases.id);
            caseIdSet.add(cases.id);
        }
        this.casesIds = caseIdsTemp;
        this.countCases = [Select count() from Case where IsClosed = false and Id in :caseIdSet];
        system.debug('countCases: ' + countCases);
        system.debug('caseIdsTemp: ' + caseIdsTemp);
        system.debug('this.casesIds: ' + this.casesIds);
    }
    
    
    @AuraEnabled
    public static List<PicklistOption> getPicklistOptions( String objectName, String fieldName ) {
        List<PicklistOption> options = new List<PicklistOption>();
        options.add(new PicklistOption('--NONE SELECTED--', ''));
        for ( PicklistEntry entry : Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap().get( fieldName ).getDescribe().getPicklistValues() ) {
            options.add( new PicklistOption( entry.getLabel(), entry.getValue() ) );
        }
        
        
        System.debug( 'options=' + options );
        
        return options;
    }
    
    @AuraEnabled
    public static result closeCaseshandler(List<string> caseIdList, string caseStatus, string caseReason){
        result res = new result();
        system.debug('res ::'+res);
       res.isSuccess = true;
       res.partialSucces = false;
       res.errorMessages = '';
       res.message = '';
       
        try{
            Set<String> caseIdSet = new Set<String>();
            for(string s : caseIdList){
                system.debug('s :: '+s);
                caseIdSet.add(s.trim());
            }
            
            system.debug('caseIdSet :: '+caseIdSet);
            List<case> caseList = new List<case>();
            Set<string> errorsSet = new set<string>();
            system.debug('caseIdSet.size()' + caseIdSet.size());
            integer successResults = 0;
            for(List<case> listofCases : [Select Id,Status,Closure_Reason__c,IsClosed from Case where Id in :caseIdSet and IsClosed = false]){
                for(case cases : listofCases ){
                    cases.Status = caseStatus;
                    cases.Closure_Reason__c = caseReason;
                 
                }
              	List<Database.SaveResult> resultCaseClosure = Database.update(listofCases, false);
                for (Database.SaveResult sr_case : resultCaseClosure) {
                    if (sr_case.isSuccess()) {
                        successResults ++;
                    }else{
                        for(Database.Error err : sr_case.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            
							errorsSet.add(err.getMessage());
                        }
                        
                    }
                }
            }
            system.debug('errorsSet.size() :: '+errorsSet.size());
             system.debug('successResults :: '+successResults);
            integer errorSize = errorsSet.size() > 20 ? 20 : errorsSet.size();
            List<string> errorList = new List<string>();
            errorList.addAll(errorsSet);
            
            if(successResults > 0 && !errorList.isEmpty()){
                res.message = String.format(Label.MassCaseClosure_SuccessMesage ,new List<String>{string.valueOf(successResults)});
                res.partialSucces = true;
                
                for( integer i = 0 ; i< errorSize ; i++){
                    res.errorMessages +=+'- '+ errorList[i]+'; ';  
                }

               
            }else if(successResults == 0 && !errorList.isEmpty()){
                res.isSuccess = false;
                res.partialSucces = false;
                //res.message = Label.MassCaseClosure_GenericFailedMessage; // 'Some records failled due: ';
            	for( integer i = 0 ; i< errorSize ; i++){
                   res.message += +'<br/>'+'- '+ errorList[i];  
                  
                }
            }else{
               res.message = String.format(Label.MassCaseClosure_SuccessMesage ,new List<String>{string.valueOf(successResults)}); 
            }
            
            
        }catch(Exception e){
            res.isSuccess = false;
            res.message = Label.MassCaseClosure_GenericFailedMessage ;
            res.message = +'<br/>'+'- '+ e.getMessage();
            System.debug('EX : '+e.getMessage()+'::'+e.getLineNumber());
        }
        
        return res;
    }
    
    
    @RemoteAction
    public static string isAllowed(){
        Boolean isUserAllowed = false;
        string result = '';
        User currentUser = [SELECT Id, Name, Profile.Name, Code__c, AssignedTerritory__c 
                            FROM User WHERE Id = :UserInfo.getUserId()];
        
        List<Mass_Case_Closure__c> accessList = [Select Id,  System_Administrator__c, Telesales_Agent__c, Sales__c, Product_Manager__c, Digital_Agency__c 
                                                 from Mass_Case_Closure__c ];
        if(!accessList.isEmpty()){
            if(((ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) || ConstantsUtil.SYSADMINCODE.equalsIgnoreCase(currentUser.Code__c)) && accessList[0].System_Administrator__c)){
                isUserAllowed = true;
            }else if(ConstantsUtil.DIGITALAGENCYPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && accessList[0].Digital_Agency__c){
                isUserAllowed = true;
            }else if(ConstantsUtil.TELESALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && accessList[0].Telesales_Agent__c){
                isUserAllowed = true;
            }else if(ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && accessList[0].Sales__c){
                isUserAllowed = true;
            }   
        }
        
        
        if(!isUserAllowed || accessList.isEmpty()) result = Label.MassCaseClosure_NoPermission;
        
        return result;
    }
    
    public class result {
        @AuraEnabled public boolean isSuccess {get; set;}
        @AuraEnabled public String message {get; set;}
        @AuraEnabled public boolean partialSucces {get; set;}
        @AuraEnabled public String errorMessages {get; set;}

    }
    
    
    public class PicklistOption {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String value {get; set;}
        
        public PicklistOption(String label,String value) {
            this.label = label;
            this.value = value;
        }
        
    }
    
    
    
}