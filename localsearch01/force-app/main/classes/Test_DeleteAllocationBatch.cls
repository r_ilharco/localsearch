/*    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SPIII-1678 API Name changed 
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-07              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@Istest
private class Test_DeleteAllocationBatch {

static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
}

@Testsetup  static void setup(){
   insertBypassFlowNames();
   Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
   insert acc;
   opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
   insert opp;
   List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
   insert cont;
   SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote1.Allocation_not_empty__c = true;
        quote1.Ad_Allocation_Ids__c = '5de7a446b919f40001888947';
        quote1.SBQQ__ExpirationDate__c = date.today();
        insert quote1;
   Product2 product1 = generateProduct();
   insert product1;

SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c 
                                            FROM SBQQ__Quote__c LIMIT 1];
     Product2 product = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                                 ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                                 Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                                 Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                                 Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                                    FROM Product2 LIMIT 1 ];
    SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(-1), null, null, null, 'Annual');
    quoteLine.Samba_Migration__c = true;
    test.startTest();
    Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
    insert quoteLine;
    Test.stopTest(); 
}

@isTest static void test_SRV(){
SBQQ__Quote__c quote = [Select id,Ad_Allocation_Ids__c from SBQQ__Quote__c Limit 1][0];
string currentIDtoDeallocate = quote.Ad_Allocation_Ids__c;
Map<String,String> parameters = new Map<String,String>();
String correlationId = SRV_DeleteAllocationCallout.generateCorrelationId(currentIdToDeallocate);
parameters.put('ad_context_allocation', correlationId);
test.startTest();
Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
SRV_DeleteAllocationCallout.deleteAllocationCallout(parameters, correlationId, quote.id);
test.stopTest();
}  

@istest static void test_batch(){
SBQQ__Quote__c QuoteTest = [Select id,Ad_Allocation_Ids__c from SBQQ__Quote__c][0];
test.startTest();
Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
DeleteAllocationBatch dab = new DeleteAllocationBatch();
dab.execute(null,new List<SBQQ__Quote__c>{QuoteTest});
dab.execute(null);
test.stopTest();   
}

@istest static void test_expirebatch(){
SBQQ__Quoteline__c QuotelineTest = [Select id,Ad_Context_Allocation__c,SBQQ__Quote__c from SBQQ__Quoteline__c][0];
test.startTest();
Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
DeleteAllocationonexpireBatch daoeb = new DeleteAllocationonexpireBatch();
daoeb.execute(null, new list<SBQQ__Quoteline__c>{QuoteLineTest});
test.stopTest();
    
}

public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}