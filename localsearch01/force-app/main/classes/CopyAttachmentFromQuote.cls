/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* All the attachment of the Quote are created on the related Order. Fix the bug SPIII-3503
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-10-08
* @systemLayer    Invocation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class CopyAttachmentFromQuote{
    
    public class OrderParam{
        @InvocableVariable(required=true)
        public Id orderId;
        @InvocableVariable(required=true)
        public Id quoteId;
        @InvocableVariable(required=true)
        public Boolean migrated;
    }
    
    @InvocableMethod
    public static void copyAttachmentsFromQuote(List<OrderParam> orders){
        
        Map<Id,Set<Id>> quoteToOrderMap = new Map<Id,Set<Id>>();
        
        for(OrderParam currentOrder : orders){
            if(String.isNotBlank(currentOrder.quoteId) && !currentOrder.migrated){
                if(!quoteToOrderMap.containsKey(currentOrder.quoteId)){
                    quoteToOrderMap.put(currentOrder.quoteId, new Set<Id>{currentOrder.orderId});
                }
                else quoteToOrderMap.get(currentOrder.quoteId).add(currentOrder.orderId);
            }
        }
        if(!quoteToOrderMap.isEmpty()){
            List<ContentDocumentLink> attachments = new List<ContentDocumentLink>();
            List<ContentDocumentLink> attachmentsToInsert = new List<ContentDocumentLink>();
            Set<Id> quotesId = quoteToOrderMap.keySet();
            
            List<String> fieldNames = new List<String>(  Schema.getGlobalDescribe().get('ContentDocumentLink').getDescribe().fields.getMap().keySet() );
            String query =' SELECT ' +String.join( fieldNames, ',' ) +' FROM ContentDocumentLink WHERE LinkedEntityId IN :quotesId';
            attachments = Database.query(query);
            if(!attachments.isEmpty()){
                for(ContentDocumentLink currentAttachment: attachments){
                    for(Id currentOrderId : quoteToOrderMap.get(currentAttachment.LinkedEntityId)){
                        ContentDocumentLink clonedAttachment = currentAttachment.clone();
                        clonedAttachment.LinkedEntityId = currentOrderId;
                        attachmentsToInsert.add(clonedAttachment);
                    }
                }
            }
            if(!attachmentsToInsert.isEmpty()){
                insert attachmentsToInsert;
            }
        }
        
    }
}