public class SendPDFController {
/*
----------------------------------------------------------------------
-- - Class Name   : SendPDFController
-- - Author        : Fabio Giuliani(Soprasteria)
-- - Date         : 27/01/2019
-- - Description  : 
-- - Version      : 1.0
----------------------------------------------------------------------
*/
    public String accountId {get;set;}
//ACCOUNT
    public String accountName {get;set;}
    public String accountStatus {get;set;}
    public String accountMobilePhone {get;set;}
    public String accountEmail {get;set;}



    public List<Contact> contactList {get;set;}
    public List<Place__c> placeList {get;set;}
    public List<Contract> contractList {get;set;}
    public List<Billing_Profile__c> billingProfileList {get;set;}
    public List<Opportunity> opportunityList {get;set;}
    public List<Case> caseList {get;set;}
    public List<SBQQ__Quote__c> quoteList {get;set;}
    public List<SBQQ__Subscription__c> subscriptionList {get;set;}
    public List<Invoice__c> invoiceList {get;set;}
    public List<Order> orderList {get;set;}
    public List<Task> taskList {get;set;}
    public SObject account;

    public SendPDFController(apexpages.StandardController stdController){

        system.debug('----------------------------------------------------------------------');
        system.debug('-- - Class Name   : SendPDFController                                 '); 
        system.debug('----------------------------------------------------------------------');

	    account = stdController.getRecord();
        accountId = account.id;
    
        system.debug('idAcc -> ' + accountId);
        List <Account> accountList = new List<Account>([SELECT Name, Status__c, Email__c, Mobile_Phone__c,
                                  (SELECT FirstName, LastName, Title__c, Email, MobilePhone FROM Contacts),
                                  (SELECT Name, SearchField_Address__c,Phone__c, Email__c FROM Places__r),
                                  (SELECT ContractNumber, Name, StartDate, EndDate, Status FROM Contracts),
                                  (SELECT Name, Billing_Contact__c, Channels__c, Billing_Language__c FROM Billing_Profiles__r),
                                  (SELECT CaseNumber, Status, Priority, Type, Origin FROM Cases),
                                  (SELECT Name, StageName, Amount, CloseDate FROM Opportunities),
                                  (SELECT Name, SBQQ__Status__c, SBQQ__NetAmount__c, SBQQ__PrimaryContact__c, SBQQ__Primary__c FROM SBQQ__Quotes__r),
                                  (SELECT SBQQ__Number__c, SBQQ__ProductName__c, Place_Name__c, Status__c, SBQQ__TerminatedDate__c, Subsctiption_Status__c, SBQQ__SubscriptionStartDate__c, SBQQ__Quantity__c FROM SBQQ__Subscriptions__r),
                                  (SELECT Name, Invoice_Date__c, Amount__c, Status__c, Billing_Profile__c FROM Invoices__r),
                                  (SELECT OrderNumber, Status, EffectiveDate, Order.Contract.ContractNumber, TotalAmount FROM Orders),
                                  (SELECT Subject, TaskSubtype, ActivityDate, Status, OwnerId FROM Tasks)

                                  FROM Account WHERE id=:accountId LIMIT 1]);

        system.debug('SendPDFController : listAcc -> '+accountList);
        if (accountList.isEmpty()){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Account Not Found'));
              return ;
        }
        accountName = accountList[0].Name;
        accountStatus = accountList[0].Status__c;
        accountMobilePhone = accountList[0].Mobile_Phone__c;
        accountEmail = accountList[0].Email__c;
		
        contactList = new List<Contact>();
    	placeList = new List<Place__c>();
    	contractList = new List<Contract>();
    	billingProfileList = new List<Billing_Profile__c>();
    	opportunityList = new List<Opportunity>();
    	caseList = new List<Case>();
    	quoteList = new List<SBQQ__Quote__c>();
    	subscriptionList = new List<SBQQ__Subscription__c>();
    	invoiceList = new List<Invoice__c>();
    	orderList = new List<Order>();
    	taskList = new List<Task>();
        
        for (Account a : accountList){
            for (Contact con : a.Contacts){
                contactList.add(con);
            }
            for (Place__c pla : a.Places__r){
                placeList.add(pla);
            }
            for (Contract contr : a.Contracts){
                contractList.add(contr);
            }
            for (Billing_Profile__c bill : a.Billing_Profiles__r){
                billingProfileList.add(bill);
            }
            for (Opportunity opp : a.Opportunities){
                opportunityList.add(opp);
            }
            for (Case cas : a.Cases){
                caseList.add(cas);
            }
            for (SBQQ__Quote__c quo : a.SBQQ__Quotes__r){
                quoteList.add(quo);
            }
            for (SBQQ__Subscription__c sub : a.SBQQ__Subscriptions__r){
                subscriptionList.add(sub);
            }
            for (Invoice__c inv : a.Invoices__r){
                invoiceList.add(inv);
            }
            for (Order ord : a.Orders){
                orderList.add(ord);
            }
            for (Task tas : a.Tasks){
                taskList.add(tas);
            }
        }

			system.debug('contactList -> '+contactList);
			system.debug('placeList -> '+placeList);
			system.debug('contractList -> '+contractList);
			system.debug('billingProfileList -> '+billingProfileList);
			system.debug('opportunityList -> '+opportunityList);
			system.debug('caseList -> '+caseList);
			system.debug('quoteList -> '+quoteList);
			system.debug('subscriptionList -> '+subscriptionList);
			system.debug('invoiceList -> '+invoiceList);
			system.debug('orderList -> '+orderList);
			system.debug('taskList -> '+taskList);

    }
}