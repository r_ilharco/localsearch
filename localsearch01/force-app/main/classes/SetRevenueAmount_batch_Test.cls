@isTest
public class SetRevenueAmount_batch_Test {
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
    }
    
    @TestSetup
    public static void setup(){
        insertBypassFlowNames();
        SubscriptionTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        AccountTriggerHandler.disableTrigger = true;
        Test_Billing.createAccount();
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
        }
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        Test_Billing.createOpp(accounts[0].id);
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c LIMIT 1];
        system.debug('quote: ' + q);
        Test_Billing.generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        Test_Billing.generateContract(q.Id, accounts[0].id, opp.Id);
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        c.Status = 'Active';
        c.Phase1Source__c = true;
        update c;
        generateSubs(c, qls);
        SubscriptionTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        AccountTriggerHandler.disableTrigger = false;
    }
    public static void generateSubs(Contract contr, List<SBQQ__QuoteLine__c> quoteLines){
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, SBQQ__BillingType__c='Advance',
                                                        SBQQ__ChargeType__c = 'Recurring', SBQQ__Discount__c=0.0, SBQQ__ListPrice__c=quoteLine.SBQQ__ListPrice__c, 
                                                        SBQQ__Number__c = quoteLine.SBQQ__Number__c, SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c = FALSE, Total__c = 490,
                                                        SBQQ__Product__c = quoteLine.SBQQ__Product__c, SBQQ__Quantity__c = quoteLine.SBQQ__Quantity__c, SBQQ__QuoteLine__c = quoteLine.Id, 
                                                        Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.today(), SBQQ__SubscriptionEndDate__c=null,
                                                        SBQQ__Contract__c = contr.Id, SBQQ__Account__c = contr.AccountId, SBQQ__Bundled__c = FALSE, Status__c = 'Active', Subscription_term__c = '12'));
        }
        insert subscriptions;
    }
    
    public static void generateQuoteLine(String quoteId, String productId, Date startDate, String requiredById, String billingFreq){
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__StartDate__c = startDate;
        quoteLine.CategoryID__c = 'testcategoryid';
        quoteLine.Category__c = 'Category Name';
        quoteLine.LocationID__c = 'testlocationid';
        quoteLine.Location__c = 'Location Name';
        quoteLine.SBQQ__Quantity__c= 1;
        quoteLine.SBQQ__ListPrice__c = 1290;
        quoteLine.Total__c = 490;
        quoteLine.Ad_Context_Allocation__c = 'testadcontextallocationid';
        quoteLine.Campaign_Id__c = 'testcampaignid';
        quoteLine.Contract_Ref_Id__c = 'testcontractrefid';
        quoteLine.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
        quoteLine.SBQQ__RequiredBy__c = requiredById;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.SBQQ__BillingFrequency__c =  billingFreq;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.Subscription_Term__c = '12';
        insert quoteLine;
    }
    
    @isTest
    public static void test_SetRevenueAmount_batch(){  
        
        List<SBQQ__Subscription__c> subList = [select id, Subsctiption_Status__c, SBQQ__Account__c,Total__c, SBQQ__Account__r.Revenue_Amount__c 
                                               from SBQQ__Subscription__c where Total__c != null and Total__c >0  
                                               and Subsctiption_Status__c =:ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE 
                                               order by SBQQ__Account__c, Subsctiption_Status__c];
        
        Test.startTest();
        List<String> values = new List<String>{'q','Q','r','R','k','K','m','M','n','N','j','J','g','G','h','H','y','Y','c','C','d','D','x','X','w','W','u','U','v','V','z','Z','8','9','s','S','t','T','6','7','o','O','p','P','4','5','I','i','l','L','2','3','e','E','f','F','0','1','a','A','b','B'};
        SetRevenueAmount_batch accBatch = new SetRevenueAmount_batch(values); 
        Database.executeBatch(accBatch, 100);
        accBatch.execute(null);
        Test.stopTest(); 
    }
}