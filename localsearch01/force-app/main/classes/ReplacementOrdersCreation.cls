/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is invoked by Opportunity Handler Process Builder when an Opportunity related to a
* Quote of type "Replacement" is set to Closed Won. Termination orders for subscriptions to replace
* are generated.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-08-12
* @systemLayer    Invocation
* @testClass      Test_ReplacementOrdersCreation
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-4634-Replacement creates cancellations instead of terminations 
				  and calculates wrong amount
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-12-11
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ReplacementOrdersCreation {

    public class OpportunityParams{
        @InvocableVariable(required=true)
        public Id primaryReplacementQuoteId;
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Creates termination orders grouped by Contract and OrderGroup__c.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    opportunityParams		List of instances of object "OpportunityParams" containing 
    * 										primary quote id.
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @InvocableMethod(label='Create Replacement Orders' description='Create Replacement Termination Orders')
    public static void createTerminationOrders(List<OpportunityParams> opportunityParams){
        
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();        
        Set<Id> replacementQuoteIds = new Set<Id>();
        for(OpportunityParams currentOptyParam : opportunityParams) replacementQuoteIds.add(currentOptyParam.primaryReplacementQuoteId);
        
        if(!replacementQuoteIds.isEmpty()){
            Map<Id, SBQQ__Quote__c> quotesMap = SEL_Quote.getQuoteById(replacementQuoteIds);
            SBQQ__Quote__c replacementQuote;
            if(quotesMap != NULL && quotesMap.size() == 1)
                replacementQuote = quotesMap.values()[0];
            Map<Id, SBQQ__Subscription__c> masterSubscriptionsToReplace = SEL_Subscription.getSubsInvolvedInSubstitutionProcessByQuoteIds(replacementQuoteIds);
            
            if(!masterSubscriptionsToReplace.isEmpty()){
                
                List<SBQQ__Subscription__c> allSubscriptions = new List<SBQQ__Subscription__c>();
                allSubscriptions.addAll(masterSubscriptionsToReplace.values());
                
                Map<Id, SBQQ__Subscription__c> childrenSubscriptionsToReplace = SEL_Subscription.getChildrenSubscriptionForReplacement(masterSubscriptionsToReplace.keySet());
                if(!childrenSubscriptionsToReplace.isEmpty()) allSubscriptions.addAll(childrenSubscriptionsToReplace.values());
                
                updateSubscriptions(allSubscriptions);

                Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>> subscriptionsHierarchyGrouped = SRV_OrderCreation.groupSubscriptions(masterSubscriptionsToReplace, childrenSubscriptionsToReplace);
				
                if(!subscriptionsHierarchyGrouped.isEmpty()){
                    
                    Map<Id, Contract> contracts = SEL_Contract.getContractsByIds(subscriptionsHierarchyGrouped.keySet());
                    
                    Map<Integer, SRV_OrderCreation.CompositeOrderValue> indexToComposite = new Map<Integer, SRV_OrderCreation.CompositeOrderValue>();
                    Map<Integer, Order> indexToOrder = new Map<Integer, Order>();
                    Map<Id, OrderItem> subIdToOrderItem = new Map<Id, OrderItem>();
                    List<OrderItem> fatherOrderItems = new List<OrderItem>();
                    List<OrderItem> childrenOrderItems = new List<OrderItem>();
                    
                    Integer orderIndex = 0;
                    for(Id currentContractId : subscriptionsHierarchyGrouped.keySet()){
                        for(String currentGroup : subscriptionsHierarchyGrouped.get(currentContractId).keySet()){
                            
                            SRV_OrderCreation.CompositeOrderValue compositeOrderValue = new SRV_OrderCreation.CompositeOrderValue();
                            compositeOrderValue.contractId = currentContractId;
                            compositeOrderValue.orderGroup = currentGroup;
                            
                            Order terminationCancellationOrder = SRV_OrderCreation.generateOrder(generateOrderWrapperInstance(contracts.get(currentContractId), replacementQuote,subscriptionsHierarchyGrouped.get(currentContractId).get(currentGroup).keySet(),masterSubscriptionsToReplace));
                        	indexToComposite.put(orderIndex, compositeOrderValue);
                            indexToOrder.put(orderIndex, terminationCancellationOrder);
                            orderIndex++;
                        }
                    }
                                        
                    insert indexToOrder.values();
                    
                    for(Integer index : indexToOrder.keySet()){    
                        List<SBQQ__Subscription__c> masterSubscriptions = new List<SBQQ__Subscription__c>();
                        for(Id masterSubscriptionId : subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).keySet()) masterSubscriptions.add(masterSubscriptionsToReplace.get(masterSubscriptionId));
                        fatherOrderItems.addAll(SRV_OrderCreation.generateOrderItems(masterSubscriptions, indexToOrder.get(index).Id, subIdToOrderItem));
                    }
                    
                    if(!fatherOrderItems.isEmpty()){
                        insert fatherOrderItems;
                                                
                        for(Integer index : indexToOrder.keySet()){    
                            List<SBQQ__Subscription__c> masterSubscriptions = new List<SBQQ__Subscription__c>();
                            for(Id masterSubscriptionId : subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).keySet()){
                                List<SBQQ__Subscription__c> childrenSubscriptions = subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).get(masterSubscriptionId);
                            	if(!childrenSubscriptions.isEmpty()) childrenOrderItems.addAll(SRV_OrderCreation.generateOrderItems(childrenSubscriptions, indexToOrder.get(index).Id, subIdToOrderItem));
                            }
                        }
                        
                        if(!childrenOrderItems.isEmpty()) insert childrenOrderItems;
                    }
                }
            }
        }
    }
    
    public static SRV_OrderCreation.OrderWrapper generateOrderWrapperInstance(Contract contract, SBQQ__Quote__c replacementQuote, Set<Id> replacedsubs, Map<Id,SBQQ__Subscription__c> masterSubscriptionsToReplace){
        
        SRV_OrderCreation.OrderWrapper orderWrapper = new SRV_OrderCreation.OrderWrapper();
        orderWrapper.orderType = ConstantsUtil.ORDER_TYPE_REPLACEMENT;
        Boolean cancellation = false;
        for(Id subId : replacedsubs){
            if(masterSubscriptionsToReplace.get(subId).SBQQ__StartDate__C >=masterSubscriptionsToReplace.get(subId).Replaced_by_Product__r.SBQQ__StartDate__c){
            	cancellation = true;   
            }
        }
        if(cancellation) {
            orderWrapper.customType = ConstantsUtil.ORDER_TYPE_CANCELLATION;
            contract.In_Cancellation__c = true;
            update contract;
        }
        else orderWrapper.customType = ConstantsUtil.ORDER_TYPE_TERMINATION;
        orderWrapper.status = ConstantsUtil.ORDER_STATUS_DRAFT;
        orderWrapper.quoteId = replacementQuote.Id;
        orderWrapper.accountId = replacementQuote.SBQQ__Account__c;
        orderWrapper.contractId = contract.Id;
        orderWrapper.opportunityId = replacementQuote.SBQQ__Opportunity2__c;
        orderWrapper.pricebookId = contract.SBQQ__Quote__r.SBQQ__PriceBook__c;	

        return orderWrapper;
    }
    
    public static void updateSubscriptions(List<SBQQ__Subscription__c> subscriptions){
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            Boolean isEvergreen = ConstantsUtil.PRODUCT2_SUBSCRIPTIONTYPE_EVERGREEN.equalsIgnoreCase(currentSubscription.SBQQ__SubscriptionType__c);
            Boolean hasReplacedByProduct = currentSubscription.Replaced_by_Product__c != null;
            
            currentSubscription.In_Termination__c = TRUE;
            currentSubscription.Termination_Generate_Credit_Note__c = TRUE;
            
            if(hasReplacedByProduct){
                if(isEvergreen && currentSubscription.End_Date__c < currentSubscription.Replaced_by_Product__r.SBQQ__StartDate__c){
                    currentSubscription.Termination_Date__c = currentSubscription.End_Date__c;
					currentSubscription.SBQQ__TerminatedDate__c = currentSubscription.End_Date__c;
                }
                else{
                    currentSubscription.Termination_Date__c = currentSubscription.Replaced_by_Product__r.SBQQ__StartDate__c.adddays(-1);
					currentSubscription.SBQQ__TerminatedDate__c = currentSubscription.Replaced_by_Product__r.SBQQ__StartDate__c.adddays(-1);
                }
            }
            else{
                if(isEvergreen && currentSubscription.End_Date__c < currentSubscription.RequiredBy__r.Replaced_by_Product__r.SBQQ__StartDate__c){
                    currentSubscription.Termination_Date__c = currentSubscription.End_Date__c;
					currentSubscription.SBQQ__TerminatedDate__c = currentSubscription.End_Date__c;
                }
                else{
                    currentSubscription.Termination_Date__c = currentSubscription.RequiredBy__r.Replaced_by_Product__r.SBQQ__StartDate__c.adddays(-1);
                	currentSubscription.SBQQ__TerminatedDate__c = currentSubscription.RequiredBy__r.Replaced_by_Product__r.SBQQ__StartDate__c.adddays(-1);
                }
            }
        }
        update subscriptions;
    }
}