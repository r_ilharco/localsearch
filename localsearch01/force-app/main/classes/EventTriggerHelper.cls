/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Giovanni Prota <gprota@deloitte.it>
 * Date        : 05-12-2019
 * Sprint      : 2
 * Work item   : #655
 * Testclass   : Test_EventTrigger
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public without sharing class EventTriggerHelper {
    
    public static void checkContactisFromAccount(List<Event> NewEvent){
        
        Map<Id,List<Event>> mapEventToCheck = new Map<Id,List<Event>>();
        Set<Id> quoteIds = new Set<Id>();
        Set<Id> opportunityIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        
        Map<Id, SBQQ__Quote__c> quotesMap;
        Map<Id, Opportunity> opportunitiesMap;
        Map<Id, Contact> contactsMap;
        
        for(Event e:NewEvent){
            if (e.WhoId != null && e.WhatId != null)
            {
                if(e.WhoId.getSObjectType().getDescribe().getName().equals('Contact'))
                {
                    contactIds.add(e.WhoId);
                    
                    if(e.WhatId.getSObjectType().getDescribe().getName().equals('SBQQ__Quote__c'))
                    {
                        quoteIds.add(e.WhatId);
                    }else if(e.WhatId.getSObjectType().getDescribe().getName().equals('Opportunity'))
                    {
                        opportunityIds.add(e.WhatId);
                    }
                }
            }
        }
        
        if(contactIds.size() > 0)
        	contactsMap = SEL_Contact.getContactsById(contactIds);
        
        if(quoteIds.size() > 0)
        	quotesMap = SEL_Quote.getQuoteById(quoteIds);
        
        if(opportunityIds.size() > 0)
        	opportunitiesMap = SEL_Opportunity.getOpportunitiesById(opportunityIds);
        
        if(contactsMap != NULL && contactsMap.size() > 0)
        {
            for(Event e: NewEvent)
            {
                if(contactsMap.containsKey(e.WhoId))
                {
                    if(e.WhatId.getSObjectType().getDescribe().getName().equalsIgnoreCase('Account'))
                    {
                        if(e.WhatId != contactsMap.get(e.WhoId).AccountId)
                        {
                            if(!Test.isRunningTest()) e.addError(Label.Event_Account_Contact_Check);
                        }
                    }else if(e.WhatId.getSObjectType().getDescribe().getName().equalsIgnoreCase('SBQQ__Quote__c') && (quotesMap != NULL && quotesMap.containsKey(e.WhatId)))
                    {
                        if(contactsMap.get(e.WhoId).AccountId != quotesMap.get(e.WhatId).SBQQ__Account__c)
                        {
                            if(!Test.isRunningTest()) e.addError(Label.Event_Quote_Contact_Check);
                        }
                    }else if(e.WhatId.getSObjectType().getDescribe().getName().equalsIgnoreCase('Opportunity') && (opportunitiesMap != NULL && opportunitiesMap.containsKey(e.WhatId)))
                    {
                        if(contactsMap.get(e.WhoId).AccountId != opportunitiesMap.get(e.WhatId).AccountId)
                        {
                            if(!Test.isRunningTest()) e.addError(Label.Event_Opportunity_Contact_Check);
                        }
                    }
                }
            }
        }
    }
    
    public static void setTypeBasedOnSubjectContent(List<Event> events)
    {/* 2021-03-17/mn: Type__c is now obsolete
     	for(Event e : events)
        {
            if(String.isNotBlank(e.Subject))
            {
                Integer startPosFirstMatch = e.Subject.length();
                e.Type__c = '';
                for(Schema.PicklistEntry pe : Event.Subject.getDescribe().getPicklistValues())
                {
                    if( e.Subject.containsIgnoreCase(pe.getLabel()) && e.Subject.indexOfIgnoreCase(pe.getLabel()) < startPosFirstMatch)
                    {
                        startPosFirstMatch = e.Subject.indexOfIgnoreCase(pe.getLabel());
                        e.Type__c = pe.getLabel();
                    }
                }
            }
        }*/
    }
    
    /*public static void sendNotifications(List<Event> events){
        List<FeedItem> feedItemsToPost  = new List<FeedItem>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        Map<Id, Contact> contacts = new Map<Id, Contact>();
        Map<Id, User> users = new Map<Id, User>();
        
        Set<Id> contactIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        
        for(Event e : events)
        {
            if(e.WhoId != NULL && e.WhoId.getSObjectType().getDescribe().getName().equals('Contact'))
            {
                if(e.WhoId != NULL)
                	contactIds.add(e.WhoId);
                
                if(e.OwnerId != NULL)
                	userIds.add(e.OwnerId);
            }
        }
        
		contacts = SEL_Contact.getContactsById(contactIds);
        users = SEL_User.getUsersById(userIds);
        
        for(Event e : events)
        {
            if(e.OwnerId != NULL)
            {
                FeedItem post = new FeedItem();
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                
                post.ParentId = e.OwnerId;
                post.Body = 'An appointment has been scheduled';
                
                if(e.ActivityDate!=NULL)
                {
                    post.Body += ' on the ' + e.ActivityDate.format();
                }
                
                if(e.ActivityDateTime != NULL)
                {
                    post.Body += ' at ' + e.ActivityDateTime.format('HH:mm');
                }
                
                if(String.isNotBlank(e.Location))
                {
                    post.Body += ' in ' + e.Location;
                }
                
                if(contacts.containsKey(e.WhoId))
                {
                    if(String.isNotBlank(contacts.get(e.WhoId).Email))
                    {
                        Id templateId = NULL;
                        Map<Id, EmailTemplate> emailTemplatesMap = new Map<Id, EmailTemplate>();
                        Map<String, Id> emailTemplateIdsByLanguage = new Map<String, Id>();
                        String emailTemplateLIKE = ConstantsUtil.EMAIL_TEMPLATE_EVENT_CREATION + '%';
        				emailTemplatesMap = new Map<Id, EmailTemplate>([SELECT Id, Name, Subject, Body, DeveloperName FROM EmailTemplate WHERE DeveloperName LIKE :emailTemplateLIKE]);
                        
                        String subject = 'localsearch - An appointment has been scheduled';
                        String body = '';
                        String emailTemplateBody = '';
                        String language = contacts.get(e.WhoId).Language__c;
                        
                        if(emailTemplatesMap != NULL && emailTemplatesMap.size() > 0)
                        {
                            for(EmailTemplate et: emailTemplatesMap.values())
                            {
                                if(et.DeveloperName.containsIgnoreCase('de') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('de'))
                                {
                                    emailTemplateIdsByLanguage.put('de',et.Id);
                                }else if(et.DeveloperName.containsIgnoreCase('it') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('it'))
                                {
                                    emailTemplateIdsByLanguage.put('it',et.Id);
                                }else if(et.DeveloperName.containsIgnoreCase('fr') && emailTemplateIdsByLanguage != NULL && !emailTemplateIdsByLanguage.containsKey('fr'))
                                {
                                    emailTemplateIdsByLanguage.put('fr',et.Id);
                                }
                            }
                        }
                        
                        
                        body += post.Body;
                        if(String.isNotBlank(language))
                        {
                            switch on language.toLowerCase() {
                                when 'german'{
                                    templateId = emailTemplateIdsByLanguage.get('de');
                                }
                                when 'french'{
                                    templateId = emailTemplateIdsByLanguage.get('fr');
                                }
                                when 'italian'{
                                    templateId = emailTemplateIdsByLanguage.get('it');
                                } when else {
                                    templateId = emailTemplateIdsByLanguage.get('de');
                                }
                            }
                        }else
                        {
                            templateId = emailTemplateIdsByLanguage.get('de');
                        }
                        
                        //System.debug('>templateId: ' + templateId);
                        //System.debug('>language.toLowerCase(): ' + language.toLowerCase());
                        
                        if(templateId != NULL && emailTemplatesMap.containsKey(templateId))
                        {
                            subject = emailTemplatesMap.get(templateId).Subject;
                            body = emailTemplatesMap.get(templateId).Body;
                            
                            if(emailTemplatesMap.get(templateId).DeveloperName.containsIgnoreCase('fr'))
                            {
                                if(e.ActivityDate!=NULL)
                				{
                                    body = body.replace('{EventDate}', ' le ' + e.ActivityDate.format());
                                }else
                                {
                                    body = body.replace('{EventDate}','');
                                }
                                
                                if(e.ActivityDateTime != NULL)
                				{
                                    body = body.replace('{EventTime}', ' à ' + e.ActivityDateTime.format('HH:mm'));
                                }else
                                {
                                    body = body.replace('{EventTime}','');
                                }
                                
                                if(String.isNotBlank(e.Location))
                				{
                                    body = body.replace('{EventLocation}', ' en ' + e.Location);
                                }else
                                {
                                    body = body.replace('{EventLocation}','');
                                }
                                
                                if(users.containsKey(e.OwnerId))
                        		{
                                    String ownerName = '';
                                    if(String.isNotBlank(users.get(e.OwnerId).FirstName))
                                    {
                                    	ownerName += users.get(e.OwnerId).FirstName + ' ';
                                    }
                                    
                                    if(String.isNotBlank(users.get(e.OwnerId).LastName))
                                    {
                                    	ownerName += users.get(e.OwnerId).LastName;
                                    }
                                    
                                    if(String.isNotBlank(ownerName))
                                    {
                                    	body = body.replace('{EventContact}',' avec ' + ownerName + ' ');
                                    }else
                                    {
                                        body = body.replace('{EventContact}','');
                                    }
                                    
                                }else
                                {
                                    body = body.replace('{EventContact}','');
                                }
                                
                            }else if(emailTemplatesMap.get(templateId).DeveloperName.containsIgnoreCase('it'))
                            {
                                
                                if(e.ActivityDate!=NULL)
                				{
                                    body = body.replace('{EventDate}', ' il ' + e.ActivityDate.format());
                                }else
                                {
                                    body = body.replace('{EventDate}','');
                                }
                                
                                if(e.ActivityDateTime != NULL)
                				{
                                    body = body.replace('{EventTime}', ' alle ' + e.ActivityDateTime.format('HH:mm'));
                                }else
                                {
                                    body = body.replace('{EventTime}','');
                                }
                                
                                if(String.isNotBlank(e.Location))
                				{
                                    body = body.replace('{EventLocation}', ' in ' + e.Location);
                                }else
                                {
                                    body = body.replace('{EventLocation}','');
                                }
                                
                                if(users.containsKey(e.OwnerId))
                        		{
                                    String ownerName = '';
                                    if(String.isNotBlank(users.get(e.OwnerId).FirstName))
                                    {
                                    	ownerName += users.get(e.OwnerId).FirstName + ' ';
                                    }
                                    
                                    if(String.isNotBlank(users.get(e.OwnerId).LastName))
                                    {
                                    	ownerName += users.get(e.OwnerId).LastName;
                                    }
                                    
                                    if(String.isNotBlank(ownerName))
                                    {
                                    	body = body.replace('{EventContact}',' con ' + ownerName + ' ');
                                    }else
                                    {
                                        body = body.replace('{EventContact}','');
                                    }
                                    
                                }else
                                {
                                    body = body.replace('{EventContact}','');
                                }
                                
                            }else if(emailTemplatesMap.get(templateId).DeveloperName.containsIgnoreCase('de'))
                            {
                                if(e.ActivityDate!=NULL)
                				{
                                    body = body.replace('{EventDate}', ' für den ' + e.ActivityDate.format());
                                }else
                                {
                                    body = body.replace('{EventDate}','');
                                }
                                
                                if(e.ActivityDateTime != NULL)
                				{
                                    body = body.replace('{EventTime}', ' um ' + e.ActivityDateTime.format('HH:mm') + ' Uhr ');
                                }else
                                {
                                    body = body.replace('{EventTime}','');
                                }
                                
                                if(String.isNotBlank(e.Location))
                				{
                                    body = body.replace('{EventLocation}', ' im ' + e.Location);
                                }else
                                {
                                    body = body.replace('{EventLocation}','');
                                }
                                
                                if(users.containsKey(e.OwnerId))
                        		{
                                    String ownerName = '';
                                    if(String.isNotBlank(users.get(e.OwnerId).FirstName))
                                    {
                                    	ownerName += users.get(e.OwnerId).FirstName + ' ';
                                    }
                                    
                                    if(String.isNotBlank(users.get(e.OwnerId).LastName))
                                    {
                                    	ownerName += users.get(e.OwnerId).LastName;
                                    }
                                    
                                    if(String.isNotBlank(ownerName))
                                    {
                                    	body = body.replace('{EventContact}',' mit ' + ownerName + ' ');
                                    }else
                                    {
                                        body = body.replace('{EventContact}','');
                                    }
                                    
                                }else
                                {
                                    body = body.replace('{EventContact}','');
                                }                                
                            }
                        }
                        
                   		message.setToAddresses(new String[] {contacts.get(e.WhoId).Email});
                        if(users.containsKey(e.OwnerId) && String.isNotBlank(users.get(e.OwnerId).Email))
                        {
                        	message.setCcAddresses(new String[] {users.get(e.OwnerId).Email});
                        }
                        
                        if(templateId == NULL)
                            body += ' with ' + users.get(e.OwnerId).FirstName + ' ' + users.get(e.OwnerId).LastName + '.';
                        
                        body = body.replaceAll('\\s+\\.','.');
                        
                        message.setOptOutPolicy('FILTER');
                		message.setSubject(subject);
                		message.plainTextBody = body;
                        messages.add(message);
                    }
                    
                    post.Body += ' with ' + contacts.get(e.WhoId).FirstName + ' ' + contacts.get(e.WhoId).LastName;
                }
                
                if(e.WhoCount > 1)
                {
                    post.Body += ', ... ';
                }else if(e.WhoCount == 1)
                {
                    post.Body += '.';
                }
                    
                post.Title = 'An event has been scheduled';
                //post.RelatedRecordId = quoteDocument.Id;
                post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ e.Id;
                
                if(e.OwnerId.getSObjectType().getDescribe().getName().equals('User'))
                	feedItemsToPost.add(post);
            }
    	}
        
        if(messages != NULL && messages.size() > 0)
        {
        	Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
        
        if(feedItemsToPost != NULL || feedItemsToPost.size() > 0)
        	insert feedItemsToPost;
    }*/
    
}