global class ExpirePromotions_Batch implements Database.Batchable<sObject>, Schedulable, Database.Stateful
{
    Map<String, Promotions__c> mapPromotionByProductAndCampaign = new Map<String, Promotions__c>();

    // specifies the number of days before the end of the promotion at that the discount of the subscription
    // will be reset to zero to prepare the discount value before the next billing run.
    Integer nrOfDaysBeforeEnd = (Integer)GlobalConfiguration_cs__c.getOrgDefaults().ExpirePromotionNrOfDaysBeforeEnd__c;
    
    Integer nrOfSubscriptionsChanged = 0;
    
    void fillPromotions(){
        // Fill map; 2020-07: has about 10 records; 
        // We work with 15-char IDs because SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c resulted in that.
        mapPromotionByProductAndCampaign.clear();
        // Only the fields in the SELECT clause can later be requested without reloading object.
        for( Promotions__c i : [SELECT Id, Product__c, Campaign__c, Days__c, Name, Discount__c, Start_date__c, End_date__c 
                                FROM Promotions__c ORDER BY LastModifiedDate DESC LIMIT 50000] ){
            String key = String.valueOf(i.Product__c).Substring(0,15) + ',' + String.valueOf(i.Campaign__c).Substring(0,15);
            System.debug(String.valueOf(this).split(':')[0]+': fillPromotions: Key='+key);
            mapPromotionByProductAndCampaign.put(key,i);
        }
    }
   
    global Database.QueryLocator start(Database.BatchableContext bc) {
        fillPromotions();
        System.debug(String.valueOf(this).split(':')[0]+': NrOfPromotions=' 
                     + mapPromotionByProductAndCampaign.size() + '; NrOfDaysBeforeEnd=' + nrOfDaysBeforeEnd);
        // 2020-07: 5500 rows.
        return Database.getQueryLocator( 
            [SELECT Id, SBQQ__Product__c, SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c, SBQQ__Discount__c, SBQQ__SubscriptionStartDate__c 
                FROM SBQQ__Subscription__c 
                WHERE Subsctiption_Status__c = 'Active'
                  AND SBQQ__SubscriptionStartDate__c <= TODAY
                  AND SBQQ__Discount__c != null AND SBQQ__Discount__c > 0 
                  AND SBQQ__Product__c != null
                  AND SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c != null  // takes SBQQ__Opportunity2__c.CampaignId
    		]);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Subscription__c> records){
        // Promotions means that some subscriptions specified by a campaign, a product and a date-range 
        // get a discount for a number of days (ex: 365) beginning at the start of the subscription.
        System.assertEquals(4,[SELECT COUNT() FROM SBQQ__Subscription__c],'A1.'); 
        Date today =  Date.today();
        List<SBQQ__Subscription__c> changedItems = new List<SBQQ__Subscription__c>();
        System.assertEquals(2,records.size(),'A2.'); 
        for( String i : mapPromotionByProductAndCampaign.keySet() ){ System.debug('ExpirePromotions_Batch_Execute Key: '+i); }
        for( SBQQ__Subscription__c i : records ){
            String key = String.valueOf(i.SBQQ__Product__c).Substring(0,15) + ',' 
                + String.valueOf(i.SBQQ__Contract__r.SBQQ__Quote__r.Campaign__c).Substring(0,15);
            Promotions__c p = mapPromotionByProductAndCampaign.get(key); // null if not found
            System.assert(p != null,'A3.'); 
            System.debug(String.valueOf(this).split(':')[0]+': '+key);
            if( p != null ){
                Date expiresAt = i.SBQQ__SubscriptionStartDate__c.addDays( ((Integer)p.Days__c) - nrOfDaysBeforeEnd);
                System.debug(String.valueOf(this).split(':')[0]+': Check expiresAt='+expiresAt+' today='+today);
                if( expiresAt <= today ){
                    String msg = 'Promotion(Name=' + p.Name + ',Days=' + p.Days__c + ',nrOfDaysBeforeEnd=' + nrOfDaysBeforeEnd + ') expired at '
                        + expiresAt + ' (=SubscrStartDate=' + i.SBQQ__SubscriptionStartDate__c
                        + ' plus PromotionDays minus nrOfDaysBeforeEnd) by setting discount from ' + i.SBQQ__Discount__c + ' to 0.';
                    i.Technical_Info__c = today.format() + ' ' + msg;
                    i.SBQQ__Discount__c = 0; // reset discount
                    changedItems.add(i);
                    nrOfSubscriptionsChanged++;
                    System.debug(String.valueOf(this).split(':')[0]+': '+msg);
                }
            }
        }
        update changedItems;
    }
    
    global void finish(Database.BatchableContext bc){
        System.debug(String.valueOf(this).split(':')[0]+': NrOfSubscriptionsChanged='+nrOfSubscriptionsChanged);
        ErrorHandler.logInfo('ExpirePromotions_Batch','','NrOfSubscriptionsChanged='+nrOfSubscriptionsChanged);
    }
    
    global void execute(SchedulableContext ctx) {
        System.debug(String.valueOf(this).split(':')[0]+': execute(SchedulableContext) begin, calling batch asynchronously');
        Id batchId = Database.executeBatch(this);
        System.debug(String.valueOf(this).split(':')[0]+': execute(SchedulableContext) end');
    } 
}