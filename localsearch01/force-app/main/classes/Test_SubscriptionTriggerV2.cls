/**
 * Author      : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date        : 2020-06-03
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Subscription Trigger (Handler and Helper)
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
global class Test_SubscriptionTriggerV2 {
    
    @testSetup
    public static void test_setupData(){
        SubscriptionTriggerHandler.disableTrigger = true;
        Territory2 territory = [SELECT Id FROM Territory2 WHERE Name LIKE '%2.%' LIMIT 1];
        //Test.setMock(HttpCalloutMock.class, new UtilityTokenTest());
        
        UserTerritory2Association userTerritoryAssociation = new UserTerritory2Association();
        userTerritoryAssociation.Territory2Id = territory.Id;
        userTerritoryAssociation.UserId= UserInfo.getUserId();
        userTerritoryAssociation.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        insert userTerritoryAssociation;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insertUpdatetoCancel(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_SubscriptionTriggerV2.deleteAllocationCalloutMock());
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        fatherSub.SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_MONTHLY;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        fatherSub.SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_QUARTERLY;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        delete subsToUpdate;
        undelete subsToUpdate;
        
        Invoice__c invoice = new Invoice__c();
        invoice.Name = 'InvoiceTest';
        invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
        invoice.Customer__c = account.Id;
        invoice.Amount__c = 500;
        insert invoice;
        
        Invoice_Order__c invoiceOrder = new Invoice_Order__c();
        invoiceOrder.Invoice__c = invoice.Id;
        invoiceOrder.Contract__c = contract.Id;
        insert invoiceOrder;
        
        Invoice_Item__c invoiceItem = new Invoice_Item__c();
        invoiceItem.Invoice_Order__c = invoiceOrder.Id;
        invoiceItem.Subscription__c = fatherSub.Id;
        invoiceItem.Amount__c = 150;
        invoiceItem.Accrual_From__c = Date.today();
        insert invoiceItem;
        
        List<SBQQ__Subscription__c> subsToTerminate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
        fatherSub.Termination_Generate_Credit_Note__c = true;
        fatherSub.Next_Invoice_Date__c = Date.today().addDays(2);
        fatherSub.Termination_Date__c = Date.today();
        fatherSub.Ad_Context_Allocation__c = 'ABCDE12345';
        subsToTerminate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
            currentSub.Termination_Generate_Credit_Note__c = true;
            currentSub.Termination_Date__c = Date.today();
            currentSub.Ad_Context_Allocation__c = 'ABCDE12345';
            subsToTerminate.add(currentSub);
        }
        update subsToTerminate;
      
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
    }
	@isTest
    public static void test_insertUpdateToTerminate(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_SubscriptionTriggerV2.deleteAllocationCalloutMock());
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        delete subsToUpdate;
        undelete subsToUpdate;
        
        Invoice__c invoice = new Invoice__c();
        invoice.Name = 'InvoiceTest';
        invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
        invoice.Customer__c = account.Id;
        invoice.Amount__c = 500;
        invoice.Open_Amount__c = 500;
        insert invoice;
        
        Invoice_Order__c invoiceOrder = new Invoice_Order__c();
        invoiceOrder.Invoice__c = invoice.Id;
        invoiceOrder.Contract__c = contract.Id;
        insert invoiceOrder;
        
        Invoice_Item__c invoiceItem = new Invoice_Item__c();
        invoiceItem.Invoice_Order__c = invoiceOrder.Id;
        invoiceItem.Subscription__c = fatherSub.Id;
        invoiceItem.Amount__c = 150;
        invoiceItem.Accrual_From__c = Date.today();
        insert invoiceItem;
        
        List<SBQQ__Subscription__c> subsToTerminate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
        fatherSub.Termination_Generate_Credit_Note__c = true;
        fatherSub.Next_Invoice_Date__c = Date.today().addDays(2);
        fatherSub.Termination_Date__c = Date.today();
        fatherSub.Ad_Context_Allocation__c = 'ABCDE12345';
        subsToTerminate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
            currentSub.Termination_Generate_Credit_Note__c = true;
            currentSub.Termination_Date__c = Date.today();
            currentSub.Ad_Context_Allocation__c = 'ABCDE12345';
            subsToTerminate.add(currentSub);
        }
        update subsToTerminate;
      
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
    }
    
    global class deleteAllocationCalloutMock implements HttpCalloutMock{
        global HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            res.setBody('{"example":"test"}');
            return res;
        }
    }
}