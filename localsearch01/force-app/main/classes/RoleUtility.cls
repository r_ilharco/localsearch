/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Mara Mazzarella <mamazzarella@deloitte.it>
* @created		  2020-07-29
* @systemLayer    Utility
* @TestClass 	  RoleUtility_Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public class RoleUtility {
    public static Set<ID> getRoleSubordinateUsers(set<Id> roleIds) {
        // To get all sub roles.
        Set<Id> allSubRoleIds = getAllSubRoleIds(roleIds);
        Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where IsActive = True AND UserRoleId IN :allSubRoleIds]);
        System.debug('users '+users.keySet());
        return users.keySet();
    }
    public static Set<ID> getRoleUsers(Set<Id> roleIds) {
        Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where IsActive = True AND UserRoleId in :roleIds]);
        return users.keySet();
    }
    
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        // Get all the roles underneath the passed roles.
        for(UserRole userRole :[select Id, Name from UserRole where ParentRoleId IN :roleIds AND ParentRoleID != null]){
            currentRoleIds.add(userRole.Id);
        }
        if(currentRoleIds.size() > 0){
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        return currentRoleIds;
    }
}