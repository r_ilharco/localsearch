/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class CreateTerminationOrderMigration implements Database.Batchable<sObject>, Schedulable{
    
    global List<String> staticValues = new List<String>();

    global CreateTerminationOrderMigration(List<String> val){
        this.staticValues = val;
    }
    global void execute(SchedulableContext sc) {
        Integration_Config__mdt config = [SELECT Id, Active__c,Method__c  FROM Integration_Config__mdt WHERE MasterLabel = 'TriggerMigration'][0];
		Integer sizeBatch = Integer.valueOf(config.Method__c);
    	CreateTerminationOrderMigration a = new CreateTerminationOrderMigration(this.staticValues); 
    	Database.executeBatch(a, sizeBatch);
   	}
    global Database.QueryLocator start(Database.BatchableContext BC){     
        String query = 'select Id from Contract WHERE Id in (select SBQQ__Contract__c from SBQQ__Subscription__c where SBQQ__OrderProduct__r.Order.ContractSAMBA_Key__c !=null'+ 
                                        ' AND  In_Termination__c = true AND Subsctiption_Status__c=\'Active\' )'+
                                        ' AND Id NOT IN (SELECT Contract__c FROM Order WHERE Custom_Type__c=\'Termination\' '+
                                        ' AND Contract__r.SBQQ__Order__r.ContractSAMBA_Key__c !=null) '+
            							' AND Cluster_Id__c in: staticValues';
        System.debug('query: '+query);
        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        Set<Id> contractIds = new Set<Id>();
        for(Contract contracts : scope){
            contractIds.add(contracts.id);
        } 
        List<SBQQ__Subscription__c> subscriptions = [SELECT SBQQ__Product__r.Production__c,Campaign_Id__c, CategoryId__c,LocationId__c,TLPaket_Region__c,Contract_Ref_Id__c, RequiredBy__r.Contract_Ref_Id__c, Id,Name,Total__c,SBQQ__EndDate__c,In_Termination__c,Termination_Generate_Credit_Note__c,SBQQ__ProductName__c, SBQQ__Contract__c, Subsctiption_Status__c,SBQQ__QuoteLine__r.position_state__c, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.position_state__c, SBQQ__StartDate__c, SBQQ__TerminatedDate__c,Termination_Date__c, End_Date__c, External_Id__c,SBQQ__ListPrice__c,SBQQ__Quantity__c,PricebookEntryId__c,RequiredBy__c,SBQQ__Product__c,SBQQ__QuoteLine__c,SBQQ__Contract__r.ActivatedDate,SBQQ__Contract__r.SBQQ__Evergreen__c,Place__c,SBQQ__Product__r.SBQQ__SubscriptionTerm__c,SBQQ__Product__r.Subscription_Term__c,SBQQ__Product__r.ProductCode,RequiredBy__r.SBQQ__Product__r.ProductCode, Partial_Termination__c, Subscription_Term__c,SBQQ__Contract__r.AccountId,SBQQ__Contract__r.Account.PreferredLanguage__c,SBQQ__Contract__r.EndDate,SBQQ__Contract__r.Termination_Reason__c,SBQQ__Product__r.Upselling_Disable__c,SBQQ__Product__r.Downselling_Disable__c,SBQQ__Product__r.One_time_Fee__c,SBQQ__QuoteLine__r.Termination_Date__c,Manual_Activation__c,SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c,SBQQ__Product__r.Manual_Activation__c,SBQQ__RequiredById__c FROM SBQQ__Subscription__c WHERE SBQQ__OrderProduct__r.Order.ContractSAMBA_Key__c !=null and  In_Termination__c = true AND Subsctiption_Status__c='Active' AND SBQQ__Contract__c NOT IN (SELECT Contract__c FROM Order WHERE Type='Termination' AND Custom_Type__c='Termination' AND Contract__r.SBQQ__Order__r.ContractSAMBA_Key__c !=null) and SBQQ__Contract__c in:contractIds order by SBQQ__Contract__c ];
        
        Map<Id,List<SBQQ__Subscription__c>> mapContSubs = new Map<Id,List<SBQQ__Subscription__c>>();
        for(SBQQ__Subscription__c sub : subscriptions){ 
            if(!mapContSubs.containsKey(sub.SBQQ__Contract__c)){
                mapContSubs.put(sub.SBQQ__Contract__c,new List<SBQQ__Subscription__c>{sub});
            }
            else{
                mapContSubs.get(sub.SBQQ__Contract__c).add(sub);
            }
        }
        for (contract c : [select Id,SBQQ__Evergreen__c, Status,SBQQ__Order__r.SambaIsStartConfirmed__c,nextRenewalDate__c,AccountId,Account.PreferredLanguage__c,Termination_Reason__c,SBQQ__Order__r.SambaIsBilled__c, StartDate,SBQQ__Quote__r.SBQQ__StartDate__c,SBQQ__Quote__r.SBQQ__SalesRep__c,Name, CallId__c, OwnerId,EndDate, SBQQ__Quote__r.SBQQ__PriceBook__c,SBQQ__OpportunityPricebookId__c,
                          Pricebook2Id, SBQQ__Quote__c,TerminateDate__c,In_Termination__c,SBQQ__Quote__r.Contract_Signed_Date__c 
                           from contract where id in: mapContSubs.Keyset()]){
            ContractUtility.createOrderWithOrderItems(c,mapContSubs.get(c.Id),ConstantsUtil.ORDER_TYPE_TERMINATION, false, true,false);
        }
    }
    global void finish(Database.BatchableContext info){}
}