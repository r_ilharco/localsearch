@isTest
public class LeadManagemnetPBTest {

    @isTest
    static void testLeadManagement(){
       
        try{
        	Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='Dhaka';    
                       
            insert acc;
        	System.debug('Account' +acc);

        
        	Lead ld=new Lead();

       		ld.LastName='Doe';
            ld.FirstName='John';
            ld.Company='test company';
            ld.Status='New';
        	ld.P_O_Box__c='10';
        	ld.P_O_Box_City__c='Dhaka';
       		ld.P_O_Box_Zip_Postal_Code__c='101';
                	
       		test.startTest();
        	insert ld;  
       		//update ld;
       
        
        	System.debug('Lead' +ld);
        

        	Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'Dhaka';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '101';
            place.Account__c = acc.Id;
        	
            insert place;
            //update place;
            System.debug('Place: '+ place);
            
            ld.Status='Converted';
            update ld;
  			place.Lead__c=ld.Id;
            update place;
            
            
            User u = new user();
            u.LastName = 'Test Code';
            u.Email = 'iqbal.rocky@arollotech.com';
            u.Alias = 'Tcode';
            u.Username = 'iqbal.roc@arollo.com';
            u.CommunityNickname = 'test12';
            u.LocaleSidKey = 'en_US';
            u.TimeZoneSidKey = 'GMT';
            u.ProfileID = '00e1r0000027zBpAAI';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';

			insert u;
            system.debug('User' +u);
            
            Lead lds=new Lead();

       		lds.LastName='bb';
            lds.FirstName='cc';
            lds.Company='test company2';
            lds.Status='Qualification';
        	lds.P_O_Box__c='101';
        	lds.P_O_Box_City__c='Dhaka1';
       		lds.P_O_Box_Zip_Postal_Code__c='1011'; 
            
            
            insert lds;
            
            ld.OwnerId= u.Id;
            update ld;
            
        	test.stopTest();
       }
       catch(Exception e)
      {
           system.assert(false, e.getMessage());
       }
    }
      
}