@isTest
public class Test_AccountTeamMemberDeas_Batch {
    @testSetup
    public static void testSetup(){
        List<Account> accounts = Test_DataFactory.generateAccounts('TestAcc', 1, false);
        insert accounts;
        AccountTeamMember accTeamMember = new AccountTeamMember();
        accTeamMember.UserId = UserInfo.getUserId();
        accTeamMember.Expiration_Date__c = Date.today();
        accTeamMember.AccountId = accounts[0].Id;
        insert accTeamMember;
    }
    
    @isTest
    public static void batchExecutionTest(){
        test.startTest();
        AccountTeamMemberDeassociation_Batch batch = new AccountTeamMemberDeassociation_Batch();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        Database.executebatch(batch, 200);
        test.stopTest(); 
    }
    
    @isTest
    public static void schedulableExecutionTest(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        String year = String.valueOf(Date.today().year() + 1);
        String CRON_EXP = '0 0 0 3 9 ? ' + year;
       	String jobId = System.schedule('AccountTeamMemberDeassociation_Batch_Test', CRON_EXP, new AccountTeamMemberDeassociation_Batch());        
        test.stopTest(); 
    }
    
}