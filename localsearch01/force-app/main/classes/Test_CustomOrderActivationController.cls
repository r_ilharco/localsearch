/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author	   : Antonio Esposito <antesposito@deloitte.it>
* Date		   : 02-12-2019
* Sprint      : 
* Work item   : SF2-242
* Testclass   : 
* Package     : 
* Description : 
* Changelog   : 
#1: Create the test class
*/

@isTest
public class Test_CustomOrderActivationController {
    @testSetup
    public static void setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow; 
        
        AccountTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Product2 product = generateProduct();
        insert product;
        
        List<Account> acc = Test_DataFactory.generateAccounts('Account test name',1,false);
        insert acc;     
        List<Contact> contacts = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last name test', 1);
        Opportunity oppo = Test_DataFactory.generateOpportunity('Opportunity test name', acc[0].Id);
        insert oppo;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = oppo.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today() , null, null, null, 'Annual');
        insert quoteLine;
        
        AccountTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_allMethods(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        order.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert order;
        
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert fatherOrderItem;
        
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        
        CustomOrderActivationController.getOrderItems(order.Id);
        CustomOrderActivationController.activateAllOrder(order.Id, Label.Confirm_for_Cancellation);
        
        CustomOrderActivationWrapper orderWrapper = new CustomOrderActivationWrapper();
        String serializedFather = '[{\"fieldLabel\":\"Quantity\",\"fieldName\":\"Quantity\",\"fieldType\":\"DOUBLE\",\"isEditable\":false,\"value\":\"1.00\"},{\"fieldLabel\":\"Start Date\",\"fieldName\":\"ServiceDate\",\"fieldType\":\"DATE\",\"helpText\":\"You can select the real activation date for the service\",\"isEditable\":true,\"value\":\"'+Date.today()+'\"},{\"fieldLabel\":\"Merchant ID\",\"fieldName\":\"Merchant_ID__c\",\"fieldType\":\"STRING\",\"isEditable\":true,\"value\":\"12312\"}]';
        String serializedChild = '[{\"activateLater\":false,\"childId\":\"'+childOrderItem.Id+'\",\"hideItem\":false}]';
        orderWrapper.hideChild = false;
        orderWrapper.isFullActivation = false;
        orderWrapper.isSecondActivation = false;
        orderWrapper.status = fatherOrderItem.SBQQ__Status__c;
        orderWrapper.ordItemId = fatherOrderItem.Id;
		orderWrapper.serializedordItemElement = serializedFather;
        orderWrapper.serializedChildElement = serializedChild;
        System.debug('JSON --> '+JSON.serializePretty(orderWrapper));
        String orderItemsSerialized = JSON.serialize(new List<CustomOrderActivationWrapper>{orderWrapper});
        CustomOrderActivationController.activateOrder(order.Id, orderItemsSerialized);
        Test.stopTest();
    }
    @isTest
    public static void test_getOrderItems(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        insert order;
        
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_DRAFT;
        insert fatherOrderItem;
        
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_DRAFT;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        
        CustomOrderActivationController.getOrderItems(order.Id);
        Test.stopTest();
    }
    @isTest
    public static void test_getOrderItems_2(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_AMENDMENT, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        order.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert order;
        
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert fatherOrderItem;
        
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        
        CustomOrderActivationController.getOrderItems(order.Id);
        Test.stopTest();
    }
    @isTest
    public static void test_activateOrder(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        order.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert order;
        
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert fatherOrderItem;
        
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        
        CustomOrderActivationController.getOrderItems(order.Id);
        CustomOrderActivationController.activateAllOrder(order.Id, Label.Confirm_for_Cancellation);
        
        CustomOrderActivationWrapper orderWrapper = new CustomOrderActivationWrapper();
        String serializedFather = '[{\"fieldLabel\":\"Quantity\",\"fieldName\":\"Quantity\",\"fieldType\":\"DOUBLE\",\"isEditable\":false,\"value\":\"1.00\"},{\"fieldLabel\":\"Start Date\",\"fieldName\":\"ServiceDate\",\"fieldType\":\"DATE\",\"helpText\":\"You can select the real activation date for the service\",\"isEditable\":true,\"value\":\"'+Date.today()+'\"},{\"fieldLabel\":\"Merchant ID\",\"fieldName\":\"Merchant_ID__c\",\"fieldType\":\"STRING\",\"isEditable\":true,\"value\":null}]';
        String serializedChild = '[{\"activateLater\":false,\"childId\":\"'+childOrderItem.Id+'\",\"hideItem\":false}]';
        orderWrapper.hideChild = false;
        orderWrapper.isFullActivation = false;
        orderWrapper.isSecondActivation = false;
        orderWrapper.status = fatherOrderItem.SBQQ__Status__c;
        orderWrapper.ordItemId = fatherOrderItem.Id;
		orderWrapper.serializedordItemElement = serializedFather;
        orderWrapper.serializedChildElement = serializedChild;
        System.debug('JSON --> '+JSON.serializePretty(orderWrapper));
        String orderItemsSerialized = JSON.serialize(new List<CustomOrderActivationWrapper>{orderWrapper});
        CustomOrderActivationController.activateOrder(order.Id, orderItemsSerialized);
        Test.stopTest();
    }
    @isTest
    public static void test_activateTerminationOrder(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
		
		SBQQ.TriggerControl.disable();         
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        insert order;
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        insert fatherOrderItem;
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItem);
        fatherSub.In_Termination__c = true;
        fatherSub.Subscription_Term__c = quoteLine.Subscription_Term__c;
        fatherSub.Termination_Date__c = Date.today();
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, new List<OrderItem>{childOrderItem}, fatherSub.Id);
        childrenSubs[0].In_Termination__c = true;
        childrenSubs[0].Termination_Date__c = Date.today();
        insert childrenSubs;
        fatherOrderItem.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOrderItem;
        childOrderItem.SBQQ__Subscription__c = childrenSubs[0].Id;
        update childOrderItem;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        Test.startTest();
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        terminationOrder.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert terminationOrder;
        OrderItem fatherOI = generateFatherOrderItem(fatherSub, terminationOrder, quoteLine.SBQQ__PricebookEntryId__c);
        fatherOI.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert fatherOI;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOI.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        childrenOrderItems[0].SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert childrenOrderItems;

        CustomOrderActivationController.activateAllOrder(terminationOrder.Id, Label.Confirm_for_Termination);
        SBQQ.TriggerControl.enable();         
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        Test.stopTest();
    }
    @isTest
    public static void test_activateCancellationOrder(){
        
        List<String> fieldNamesAccount = new List<String>(  Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount = 'SELECT ' +String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
		
		SBQQ.TriggerControl.disable();         
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        
        Order order = generateOrder(account.Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        insert order;
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(order.Id, quoteLine);
        fatherOrderItem.Quantity = 1;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        insert fatherOrderItem;
        OrderItem childOrderItem = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{quoteLine} , fatherOrderItem.Id)[0];
        childOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        childOrderItem.Quantity = 1;
        insert childOrderItem;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItem);
        fatherSub.In_Termination__c = true;
        fatherSub.Subscription_Term__c = quoteLine.Subscription_Term__c;
        fatherSub.Termination_Date__c = Date.today();
        insert fatherSub;
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, new List<OrderItem>{childOrderItem}, fatherSub.Id);
        childrenSubs[0].In_Termination__c = true;
        childrenSubs[0].Termination_Date__c = Date.today();
        insert childrenSubs;
        fatherOrderItem.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOrderItem;
        childOrderItem.SBQQ__Subscription__c = childrenSubs[0].Id;
        update childOrderItem;
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        Test.startTest();
        Order cancellationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_CANCELLATION);
        cancellationOrder.Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert cancellationOrder;
        OrderItem fatherOI = generateFatherOrderItem(fatherSub, cancellationOrder, quoteLine.SBQQ__PricebookEntryId__c);
        fatherOI.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert fatherOI;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,cancellationOrder,fatherOI.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        childrenOrderItems[0].SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        insert childrenOrderItems;

        CustomOrderActivationController.activateAllOrder(cancellationOrder.Id, Label.Confirm_for_Cancellation);
        SBQQ.TriggerControl.enable();         
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        Test.stopTest();
    }
	    
    public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Order generateOrder(String accountId, String quoteId, String orderType, String customType){
        Map<String,String> fieldApiNameToValueOrder = new Map<String,String>();
        fieldApiNameToValueOrder.put('Custom_Type__c', customType);
        fieldApiNameToValueOrder.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValueOrder.put('EndDate', String.valueOf(Date.today().addDays(10)));
        fieldApiNameToValueOrder.put('AccountId', accountId);
        fieldApiNameToValueOrder.put('SBQQ__Quote__c', quoteId);
        fieldApiNameToValueOrder.put('Type', orderType);
        Order ord = Test_DataFactory.generateOrder(fieldApiNameToValueOrder);
        return ord;
    }
    public static Order generateOrder(Contract c, String orderType){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.Type = orderType;
        order.Custom_Type__c = orderType;
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null){
                c.Cancel_Date__c = Date.today();
                order.EffectiveDate = Date.today();
            }
        }
        else{
            
            if(c.TerminateDate__c == null){
                c.TerminateDate__c = Date.today();
                order.EffectiveDate = Date.today();
            }
            else{
                order.EffectiveDate = c.TerminateDate__c;
            }
            order.ContractId = c.id;
        }
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today();
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('FATHER OI ---> '+JSON.serializePretty(fatherOrderItem));
        return fatherOrderItem; 
    }
    public static OrderItem generateChildrenOrderItem(SBQQ__Subscription__c subscription, Order order, String requiredBy, String pbeId){
        OrderItem oi = new OrderItem();
        oi.ServiceDate = Date.today();
        oi.Quantity = subscription.SBQQ__Quantity__c;
        oi.UnitPrice = subscription.SBQQ__ListPrice__c;   
        oi.Product2Id = subscription.SBQQ__Product__c;
        oi.PricebookEntryId = pbeId;
        oi.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        oi.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c;
        oi.SBQQ__Subscription__c = subscription.Id;
        oi.OrderId = order.Id;
        oi.SBQQ__RequiredBy__c = requiredBy;  
        oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('CHILD OI ---> '+JSON.serializePretty(oi));
        return oi;   
    }
}