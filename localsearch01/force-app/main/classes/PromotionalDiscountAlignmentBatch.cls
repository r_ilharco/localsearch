/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2021-03-22
 * Sprint      : Sprint 14
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : Subscriptions alignment for promotional discount logic
 * Changelog   : 
 */

global class PromotionalDiscountAlignmentBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    private List<String> subscriptionStatusList;
    
    public PromotionalDiscountAlignmentBatch(List<String> statusList) {
        this.subscriptionStatusList = new List<String>(statusList);
        system.debug('statusList in constructor: ' + statusList);
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext){
        system.debug('subscriptionStatusList: '+ this.subscriptionStatusList);
        String query = 'SELECT Id, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c, SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c, SBQQ__Discount__c, Promotion_Type__c ' + 
            'FROM SBQQ__Subscription__c '+
            'WHERE Subsctiption_Status__c Not IN :subscriptionStatusList AND SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null AND SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c >0 AND SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c = false ';
        return Database.getQueryLocator(query);
                                     
    }

    public void execute(Database.BatchableContext batchableContext, List<SBQQ__Subscription__c> scope) {
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        system.debug('scope: ' + scope);
        for(SBQQ__Subscription__c sub : scope){
            sub.SBQQ__Discount__c = sub.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c;
            sub.Promotion_Type__c = ConstantsUtil.QUOTE_DISCOUNT;
            subscriptionsToUpdate.add(sub);
        }
		if(subscriptionsToUpdate.size()> 0) {
            update subscriptionsToUpdate;
        }
	}

    public void finish(Database.BatchableContext batchableContext){
        
    }
}
