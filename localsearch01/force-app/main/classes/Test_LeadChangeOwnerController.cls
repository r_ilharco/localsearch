@isTest
public class Test_LeadChangeOwnerController {

    
    @testSetup
    public static void setup(){
       
		User sysAdmin = Test_DataFactory.createUsers(ConstantsUtil.SYSADMINPROFILE_LABEL, 1)[0];
        sysAdmin.Email = 'testAdmin=@email.org';
        User salesManager = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL, 1)[0];
        salesManager.Email = 'testSalesMan=@email.org';
        List<User> dmcUsers = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL, 2);
        dmcUsers.get(0).Email = 'testDMC1=@email.org';
        dmcUsers.get(1).Email = 'testDMC2=@email.org';
        
        List<User> users = new List<User>();
        users.add(sysAdmin); 
        users.add(salesManager);
        users.addAll(dmcUsers);
        UserTriggerHandler.disableTrigger= true;
        insert users;
        UserTriggerHandler.disableTrigger= false;
        
        PermissionSet salesmanagerPS = [select id from PermissionSet where Name =: ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME][0];
        PermissionSet digital_marketing_consultant = [select id from PermissionSet where Name =: ConstantsUtil.DMCPERMISSIONSET_DEVNAME][0];
        
        PermissionSetAssignment salesManagerAssignment = new PermissionSetAssignment();
        salesManagerAssignment.PermissionSetId = salesmanagerPS.Id;
        salesManagerAssignment.AssigneeId = salesManager.Id;
        
        PermissionSetAssignment dmcAssignment;
        List<PermissionSetAssignment> dmcAssignments = new List<PermissionSetAssignment>();
        
        for(User u : dmcUsers){
            dmcAssignment = new PermissionSetAssignment();
            dmcAssignment.PermissionSetId = digital_marketing_consultant.Id;
        	dmcAssignment.AssigneeId = u.Id;
            dmcAssignments.add(dmcAssignment);
        }
            
        List<PermissionSetAssignment> pmassignments = new List<PermissionSetAssignment>();
        pmassignments.add(salesManagerAssignment);
        pmassignments.addAll(dmcAssignments);
        insert pmassignments;
        
        List<Group> groups = Test_DataFactory.createGroups(new List<String>{'VB Test'});
        insert groups;
        
        List<GroupMember> groupMembers = Test_DataFactory.createGroupMembers(groups.get(0), users);
        System.debug('TEST GROUP MEMBERS: '+groupMembers);
        insert groupMembers;
        
    }
    
    @isTest
    public static void test_hasUserGrant_Lead(){
        User sysAdmin = [select id from User where Profile.Name =: ConstantsUtil.SYSADMINPROFILE_LABEL and Email = 'testAdmin=@email.org'];
        User salesManager = [select id from User where Profile.Name =: ConstantsUtil.SALESPROFILE_LABEL and Id in (select AssigneeId from PermissionSetAssignment where PermissionSet.Name =: ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME) and Email = 'testSalesMan=@email.org'];
    	
        Test.startTest();
        System.runAs(sysAdmin){
            LeadChangeOwnerController.ResponseHasUserGrant response = LeadChangeOwnerController.hasUserGrant_Lead();
            System.assertEquals(true, response.allowed);
        }
        System.runAs(salesManager){
            LeadChangeOwnerController.ResponseHasUserGrant response = LeadChangeOwnerController.hasUserGrant_Lead();
            System.assertEquals(true, response.allowed);
        }
        Test.stopTest();
    }
    
    @isTest
    public static void test_updateLeadOwner(){
    	
        List<User> dmcUsers = [select id from User where Profile.Name =: ConstantsUtil.SALESPROFILE_LABEL and Id in (select AssigneeId from PermissionSetAssignment where PermissionSet.Name =: ConstantsUtil.DMCPERMISSIONSET_DEVNAME) and Email like '%testDMC%'];
        
        Test.startTest();
        System.runAs(dmcUsers.get(0)){
        	Lead lead = Test_DataFactory.createLeads('FirstName', 'LastName', 1)[0];
            lead.New_Owner__c = dmcUsers.get(1).Id;
            insert lead;
            LeadChangeOwnerController.ResponseHasUserGrant response = LeadChangeOwnerController.updateLeadOwner(lead.Id);
            System.assertEquals(true, response.allowed);
        }
        Test.stopTest();
        
    }
    
    
    
}