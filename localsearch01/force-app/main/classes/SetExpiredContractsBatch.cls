/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 02-08-2019
 * Sprint      : 3
 * Work item   : SF2-205 Order Management - Renewal - Contract automatically expires
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

global class SetExpiredContractsBatch implements Database.Batchable<sObject>, Schedulable {
    
    global void execute(SchedulableContext sc) {
        SetExpiredContractsBatch setExpiredContractB = new SetExpiredContractsBatch(); 
        database.executebatch(setExpiredContractB, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){     
        String contractStatus = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        return Database.getQueryLocator('SELECT Id, Status ' +
                                        'FROM Contract ' + 
                                        'WHERE EndDate <= TODAY ' +
                                        'AND Status = :contractStatus AND SBQQ__Evergreen__c = false AND In_Termination__c = false AND In_Cancellation__c = false '
                                       );
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        Set<string> excludeStatus = new Set<string>{ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED,ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED};
        Set<Id> contractIdsToSkip = new Set<Id>();
        Map<Id, Contract> contracts = new Map<Id, Contract>(scope);
        Map<Id,SBQQ__Subscription__c> subs = new Map<Id,SBQQ__Subscription__c>([SELECT Id, Subsctiption_Status__c, In_Termination__c, SBQQ__Contract__c
                                                                               	FROM SBQQ__Subscription__c
                                                                               	WHERE SBQQ__Contract__c IN :contracts.keySet()
                                                                                and Subsctiption_Status__c not in :excludeStatus]);
		List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c s : subs.values()){
            if(!s.In_Termination__c){
                s.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_EXPIRED;
                subsToUpdate.add(s);
            }else{
                contractIdsToSkip.add(s.SBQQ__Contract__c);
            }
        }
        List<Contract> contractsToUpdate = new List<Contract>();
        for(Contract c : contracts.values()) {
            if(!contractIdsToSkip.contains(c.Id)){
                c.Status = ConstantsUtil.CONTRACT_STATUS_EXPIRED;
            	contractsToUpdate.add(c);
            }
        }
        if(subsToUpdate.size() > 0){
            update subsToUpdate;
        }
        if(contractsToUpdate.size() > 0){
            update contractsToUpdate;
        }
    }
    
    public void finish(Database.BatchableContext context) {}
}