/**
* Author	   : Pasquale Di Marzo <pdimarzo@deloitte.it>
* Date		   : 11-03-2020
* Sprint      : 
* Work item   : 
* Package     : 
* Description : Test class for ContractTriggerHandler,ContractTriggerHelper
* Changelog   : 
*/

@isTest
private class Test_ContractTriggerHelper {
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    
    
    @TestSetup
    static void setup(){
        
        insertBypassFlowNames();
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        AccountTriggerHandler.disableTrigger = true;
        insert acc;
        AccountTriggerHandler.disableTrigger = false;
        Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
        test.startTest();
        cntrct.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update cntrct;
        test.stopTest();
        lead Tlead = Test_DataFactory.createleads('Test','Lead',1)[0];
        insert Tlead;
        
        Bypass_Triggers__c profilewideBypass = new Bypass_Triggers__c();
        profilewideBypass.SetupOwnerId = UserInfo.getProfileId();
        profilewideBypass.Trigger_Name__c = 'NomeTest';
        insert profilewideBypass;
        Bypass_Triggers__c setting = new Bypass_Triggers__c();
        setting.Trigger_Name__c = 'Trying';
        insert setting;
    }
    
    /*@isTest static void testOpenAmountCalculation() {
    Contract c = [SELECT Id FROM Contract limit 1][0];
    Map<Id, Contract> testmap = new Map<Id, Contract>();
    testmap.put(c.id,c);
    PermissionSetAssignment PSA = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
    string UserId = PSA.AssigneeId;
    user u = [select id from user where id=: UserId][0];
    system.runas(u){
    SBQQ__Subscription__c sbqq = new SBQQ__Subscription__c();
    sbqq.SBQQ__Contract__c = c.id;
    sbqq.SBQQ__Quantity__c = 1;
    sbqq.SBQQ__NetPrice__c = 10;
    insert sbqq;
    ContractTriggerHelper.openAmountCalculation(testmap);
    }
    }*/
    
    
    @isTest static void Test_disableTrigger() {
        ContractTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    static void testAfterUndelete(){
        Map<Id, contract> Tcontract = new Map<Id, contract>([select id from contract]);
        
        Test.startTest();
        delete Tcontract.values();
        undelete Tcontract.values();
        Test.stopTest();
    }
    
    @isTest
    static void creditNoteCheck(){
        Test_Billing.createAccount();
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
        }
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        Test_Billing.createOpp(accounts[0].id);
        Opportunity opp = [SELECT Id, Upgrade__c FROM Opportunity LIMIT 1];
        opp.Upgrade__c = true;
        update opp;
        SBQQ__Quote__c q = Test_DataFactory.generateQuote((String)opp.id, (String)accounts[0].id, (String)contacts[0].id, (String)profiles[0].id);
        insert q;
        system.debug('quote: ' + q);
        Test_Billing.generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        QuoteLineTriggerHandler.disableTrigger = true;
        Test_Billing.generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        Test_Billing.generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        QuoteLineTriggerHandler.disableTrigger = false;
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        Test_Billing.generateContract(q.Id, accounts[0].id, opp.Id);
        Map<Id,Contract> c = new Map<Id,Contract>([SELECT Id, Status, AccountId FROM Contract WHERE SBQQ__Opportunity__r.Upgrade__c = TRUE AND SBQQ__Opportunity__c = :opp.Id LIMIT 1]);
        Map<Id,Contract> c2 = new Map<Id, Contract>([SELECT Id, Status, AccountId FROM Contract WHERE SBQQ__Opportunity__r.Upgrade__c = TRUE AND SBQQ__Opportunity__c = :opp.Id LIMIT 1]);
        for(Contract con : c.values()){
            con.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        }
        Test.startTest();
        ContractTriggerHelper.upgradedContractCreditNotesCheck(c, c2);
        Test.stopTest();
    }
}
