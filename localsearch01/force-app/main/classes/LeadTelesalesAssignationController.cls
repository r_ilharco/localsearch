/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Alessandro L. Marotta <amarotta@deloitte.it>
 * Date		   : 04-12-2019
 * Sprint      : Sprint 13
 * Work item   : SF2-538, Wave E, Sprint 13 - manual assignation of lead to Telesales queue on click
 * Testclass   : Test_LeadTelesalesAssignationController
 * Package     : 
 * Description : 
 * Changelog   :
 * 				
 */

public without sharing class LeadTelesalesAssignationController {

    @AuraEnabled
    public static ResponseWrapper assignLeadToTelesalesQueue(Id leadId){

        ResponseWrapper response = new ResponseWrapper();

        System.debug('In apex method');
        QueueSObject allTelesalesUsers = [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName =: Label.Telesales_Queue limit 1];
        Lead lead = [select id, OwnerId from Lead where id =: leadId limit 1];
        //A.L. Marotta 19-12-19 Start, Wave E, Sprint 13 - manual lead sharing
        Id oldOwnerId;
        Boolean sharingResult = false;

        if (lead.id != null && allTelesalesUsers.Queue.Id != null){
            if(lead.OwnerId != null){
                oldOwnerId = lead.OwnerId;
            }
            lead.OwnerId = allTelesalesUsers.Queue.Id;
            System.debug('Lead owner changed');

        }
        //A.L. Marotta 19-12-19 End
        //save updated record
        try{
            update lead;
            System.debug('Lead owner updated successfully');
            //A. L. Marotta 19-12-19 Wave E, Sprint 13 - share lead manually to old owner
            Boolean oldUserIsDMC = userIsDMC(oldOwnerId);
            if(oldOwnerId != null && oldUserIsDMC){
                sharingResult = SharingUtility.shareLeadManually(lead.Id, oldOwnerId, 'Edit');
                response.oldOwnerIsDMC = true;
            }
            System.debug('Manual lead sharing result: '+sharingResult);
            ListView myLeads = [select id from ListView where DeveloperName = 'My_Leads'];
            response.listViewId = myLeads.Id;
            
            return response;

        }
        catch(Exception e){
            System.debug('Exception: ' + e);
            return null;
        }

    }


    //A.L. Marotta 19-12-19 Wave E, Sprint 13 - check if user is a DMC or an Admin
    private static Boolean userIsDMC(Id userId)
    {
        Boolean okProfile = false;
        Boolean okPermissionSetConfiguration = false;
        User currentUser = [SELECT Id, Name, Profile.Name, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Name =: ConstantsUtil.DMCPERMISSIONSET_DEVNAME) FROM User WHERE Id = :userId];
   		
        System.debug('>>> currentUser.Profile.Name: ' + currentUser.Profile.Name);
        System.debug('>>> currentUser.PermissionSetAssignments.size(): ' + currentUser.PermissionSetAssignments.size());
        
        if(ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
        {
            okProfile = true;
        }
        
        if(currentUser.PermissionSetAssignments.size() > 0)
        {
            for(PermissionSetAssignment psa: currentUser.PermissionSetAssignments)
            {
                System.debug('>>>currentUser.PermissionSetAssignments.Name: ' + psa.PermissionSet.Name + ' Label: ' + psa.PermissionSet.Label);
                if(ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && (ConstantsUtil.DMCPERMISSIONSET_DEVNAME).equalsIgnoreCase(psa.PermissionSet.Name))
                {
                    okPermissionSetConfiguration = true;
                } 
            }
        }
        /*else if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
        {
            okPermissionSetConfiguration = true;
        }*/
        
        System.debug('okProfile && okPermissionSetConfiguration: '+ (okProfile && okPermissionSetConfiguration));
        return okProfile && okPermissionSetConfiguration;
    }

    public class ResponseWrapper{
        @AuraEnabled
        public Boolean oldOwnerIsDMC;
        @AuraEnabled
        public String listViewId;

        public ResponseWrapper(){
            oldOwnerIsDMC = false;
            listViewId = null;
        }
    }

}