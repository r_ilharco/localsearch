@isTest
public class Test_InvoicesCancellationBatch {
	@testSetup
    public static void testSetup(){
        createAccount();
        List<Account> accounts = [SELECT Id FROM Account];
        List<Invoice__c> invoices = Test_DataFactory.createInvoices(1 , 'Collected', 'Standard1' , accounts);
        insert invoices;
    }
    
    private static testmethod void executeBatch(){
        List<Invoice__c> invoices = [SELECT Id, Correlation_Id__c, Name, Payment_Status__c, Status__c, Customer__c, Customer__r.Name, Customer__r.Customer_Number__c, Open_Amount__c, Total__c  FROM Invoice__c];
        List<String> statusList = new List<String>();
        statusList.add('Collected');
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        InvoicesCancellationBatch batch = new InvoicesCancellationBatch(statusList);
        batch.start(null);
        batch.execute(null,invoices);
        batch.finish(null);
        test.stopTest(); 
    }
    
    public static void createAccount(){
        Account ac = new Account();
        ac.Name = 'TestNameAccount';
        ac.AddressValidated__c = 'validated_address';
        ac.BillingCity = 'Zürich';
        ac.BillingCountry = 'Switzerland';
        ac.BillingPostalCode = '8005';
        ac.BillingStreet = 'Förrlibuckstrasse 62';
        ac.Customer_Number__c = '2000258872';
        ac.LegalEntity__c = 'AG';
        ac.P_O_Box_City__c = 'Buchberg';
        ac.P_O_Box_Zip_Postal_Code__c = '8454';
        ac.PreferredLanguage__c = 'German';
        ac.Status__c = 'New';
        insert ac;
    } 
}