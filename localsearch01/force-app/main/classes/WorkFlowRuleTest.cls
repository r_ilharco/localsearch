@isTest
public class WorkFlowRuleTest {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    
    @isTest
    static void testWorkFlowRule()
    {     
        
        try{ 
            insertBypassFlowNames();
            
            Account acc = createAccount();
            
            Place__c place = new Place__c();
            place.Account__c = acc.Id;
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            
            insert place;
            System.debug('Place: '+ place);
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
            
            Billing_Profile__c billProfile = new Billing_Profile__c();       	
            billProfile.Customer__c	= acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c	= 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c	= 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';	
            billProfile.Channels__c	= 'test';
            billProfile.Name = 'Test Bill Prof Name';
            
            insert billProfile;
            System.Debug('Bill Profile: '+ billProfile);
            
            Pricebook2 pBook = createPricebook();
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.Pricebook2Id = pBook.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
            
            insert opp;
            System.Debug('opp '+ opp); 
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__PrimaryContact__c = myContact.Id;
            quot.Billing_Profile__c = billProfile.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(30);
            quot.SBQQ__Primary__c = true;
            quot.SBQQ__BillingCity__c= 'test';
            quot.SBQQ__BillingCountry__c='test';
            quot.SBQQ__BillingState__c='test';
            quot.SBQQ__BillingPostalCode__c='Test';
            quot.SBQQ__StartDate__c = date.today();
            
            
            insert quot;
            System.Debug('Quote '+ quot);
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Quote__c= quot.Id;
        
            insert myContract;
            System.debug('Contract: '+ myContract);   
  
            Product2 prod = createProduct();
            
            SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
            quoteLine.SBQQ__Quote__c = quot.Id;
            quoteLine.SBQQ__Product__c = prod.Id;
            quoteLine.Place__c = place.Id;
            quoteLine.SBQQ__Quantity__c = 1;
            
            insert quoteLine;
            System.Debug('Quote Line '+ quoteLine);
            
            quot.SBQQ__Status__c = 'Accepted';
            update quot;
            System.Debug('Quote '+ quot);
            
            
            quot.SBQQ__Primary__c = true;
            update quot;
            
            test.startTest();
            opp.StageName = 'Closed-Won';
            //opp.IsWon = true;
            update opp;
            System.Debug('opp '+ opp); 
            test.stopTest();
       
            
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }
  
    }
    
    @isTest
    static void testWorkFlow(){
        
        try{
            insertBypassFlowNames();
            
            User u = new user();
            u.LastName = 'Test Code';
            u.Email = 'iqbal.rocky@arollotech.com';
            u.Alias = 'Tcode';
            u.Username = 'iqbal.roc@arollo.com';
            u.CommunityNickname = 'test12';
            u.LocaleSidKey = 'en_US';
            u.TimeZoneSidKey = 'GMT';
            u.ProfileID = '00e1r0000027zBpAAI';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
            
            insert u;
            system.debug('User' +u);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
            
            insert acc;
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
            
            insert opp;
            System.Debug('opp '+ opp); 
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
            
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            
            insert quot;
            System.Debug('Quote '+ quot);
            
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Opportunity__c= opp.Id;
            myContract.SBQQ__Quote__c= quot.Id;
            myContract.OwnerId= u.Id;
            
            test.startTest();
            insert myContract;
            System.debug('Contract: '+ myContract); 
            
            myContract.Status='Active';
            update myContract;
            myContract.Status='Cancelled';
            update myContract;              
            
            test.stopTest();
        }
        catch(Exception e)
        {
            system.assert(false, e.getMessage());
        }
        
    }
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.GoldenRecordID__c = 'GRID';       
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh'; 
        acc.BillingCity = 'test city';
        acc.BillingCountry = 'test country';
        acc.BillingPostalCode = '123';
        acc.BillingState = 'test state';
        acc.PreferredLanguage__c = 'German';
        acc.ShippingCity='test';
        
        insert acc;
        System.Debug('Account: '+ acc);  
        return acc;
    }
    
    @isTest
    public static Pricebook2 createPricebook(){
        Pricebook2 pBook = new Pricebook2();
        pBook.Name = 'test p book';
        
        insert pBook;
        System.Debug('P. Book '+ pBook); 
        return pBook;
    }
    
    @isTest
    public static Product2 createProduct(){
        Product2 prod = new Product2();
        prod.Name = 'test product';
        prod.ProductCode = 'test';
        prod.IsActive=True;
        prod.SBQQ__ConfigurationType__c='Allowed';
        prod.SBQQ__ConfigurationEvent__c='Always';
        prod.SBQQ__QuantityEditable__c=True;       
        prod.SBQQ__NonDiscountable__c=False;
        //prod.SBQQ__SubscriptionType__c='Evergreen'; 
        prod.SBQQ__SubscriptionPricing__c='Fixed Price';
        prod.SBQQ__SubscriptionTerm__c=12; 
        
        insert prod;
        System.Debug('Product '+ prod); 
        return prod;
    }
    
}