@istest 
public class Test_RunTerritoryRulesInvocable {
	@testsetup static void setup(){
        list<account> accounts = test_dataFactory.createAccounts('testAcc',1);
        User u = [select id from User where Code__c =: ConstantsUtil.ROLE_IN_TERRITORY_SMA and isActive = true limit 1];
        accounts[0].OldApproverId__c = u.id;
        insert accounts;
     }
    @IsTest
    public static void test_RunTerritoryRules(){
        List<Account> accs = [select id,RegionalLeader__c,OldApproverId__c,OwnerId from Account];
        List<String> accIds = new List<String>();
       
        for(Account acc : accs ){ accIds.add(acc.Id);}
        Test.startTest();
        	RunTerritoryRulesInvocable.RunTerritoryRules(accIds);
        Test.stopTest();
    }
}
