@isTest
 public class TrialActivation_BatchSchedule_Test {
 
  	 static testMethod void testTrialActivation_BatchSchedule()
     {
         List<SBQQ__Quote__c> listQuotes = new List<SBQQ__Quote__c>();
 
          Test.startTest();
         TrialActivation_BatchSchedule obj = new TrialActivation_BatchSchedule();
 
          try{
             DataBase.executeBatch(obj);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }
 
          Database.BatchableContext bc;
         try{
         	obj.execute(bc,listQuotes);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }
 
          SchedulableContext sc;
         Try{
         	obj.execute(sc);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }       
         Test.stopTest();
     }
 }