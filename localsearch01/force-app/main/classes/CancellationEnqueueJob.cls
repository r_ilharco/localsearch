public class CancellationEnqueueJob implements Queueable, Database.AllowsCallouts {
    private Invoice__c invoice;
    public CancellationEnqueueJob(Invoice__c invoice) {
        this.invoice = invoice;
    }
    public void execute(QueueableContext context) {
        SwissbillingHelper.cancelInvoice(this.invoice);
    }
}