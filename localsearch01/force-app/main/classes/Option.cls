public class Option{
    @AuraEnabled public String name {get; set;}
    @AuraEnabled public String optionCode {get; set;}
    @AuraEnabled public String fields {get; set;}
    @AuraEnabled public String externalConfigurationId {get; set;}
    @AuraEnabled public String selectedLocation {get; set;}
    @AuraEnabled public String selectedCategory {get; set;}
    @AuraEnabled public String selectedLocationId {get; set;}
    @AuraEnabled public String selectedCategoryId {get; set;}
    @AuraEnabled public Map<String,List<List<SelectOptionLightning>>> mapValuesOption {get; set;}
    @AuraEnabled public Map<String,List<List<String>>> mapValuesString {get; set;}
    @AuraEnabled public Map<String,List<String>> mapText {get; set;}
    @AuraEnabled public String mandatoryFields {get;set;}
    @AuraEnabled public String fieldsToShow {get;set;}
    @AuraEnabled public Boolean isYellowPage {get;set;}
    @AuraEnabled public Boolean isWhitePage {get;set;}
    @AuraEnabled public Boolean isCover {get;set;}
    @AuraEnabled public Boolean isEditorialPart {get;set;}
    
    public Option() {
        mapValuesOption = new Map<String,List<List<SelectOptionLightning>>>();
        mapValuesString = new Map<String,List<List<String>>>();
        mapText = new Map<String,List<String>>();
    } 
}