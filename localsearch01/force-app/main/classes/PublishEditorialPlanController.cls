/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-1714 Object "Edition" Publication "Date cannot be in Past" 
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-08              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class PublishEditorialPlanController {
    
    @AuraEnabled
    public static String dateCheckAndUpdates(String editionId){
        String isOk = '';
        Editorial_Plan__c edition = [SELECT Id, Publication_Date__c, Local_Guide_Number__c, Name, Planned_Publication_Date__c  
                                                  FROM Editorial_Plan__c 
                                                  WHERE Id = :editionId LIMIT 1];
        if(//edition.Publication_Date__c >= Date.today() &&
           edition.Publication_Date__c >= edition.Planned_Publication_Date__c ){
            updateOrders(edition.Id);
            updateSubscriptions(edition.Publication_Date__c, edition.Name, edition.Local_Guide_Number__c);
            isOk = 'ok';
        }
        /*else if(edition.Publication_Date__c < Date.today()){
            isOk = 'Date cannot be in the past!';
        }*/
        else if(edition.Publication_Date__c < edition.Planned_Publication_Date__c){
            isOk = Label.EditionController_Error; //'Publication Date cannot be before the Planned Publication Date';
        }
        system.debug('isOk: ' + isOk);
        return isOk;
    }
    
    public static void updateOrders(String editionId){
        try{
            EditorialPlanOrderActivationBatch orderActivationJob = new EditorialPlanOrderActivationBatch(editionId);
            Database.executeBatch(orderActivationJob,1);
        }catch(Exception ex){
            system.debug('ex: ' + ex.getMessage() + '/n at line: ' + ex.getStackTraceString());
        }
    }
    
    public static void updateSubscriptions(Date publicationDate, String editionName, Decimal editionNumber){
        try{
            EditorialPlanInvoiceDateBatch invoiceDateCalculationJob = new EditorialPlanInvoiceDateBatch(publicationDate, editionName, editionNumber);
            Database.executeBatch(invoiceDateCalculationJob, 200);
        }catch(Exception ex){
            system.debug('ex: ' + ex.getMessage() + '/n at line: ' + ex.getStackTraceString());
        }
    }
}