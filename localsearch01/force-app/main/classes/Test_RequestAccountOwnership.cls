@isTest
public class Test_RequestAccountOwnership {
	
    @testSetup
    public static void test_setupData(){
        AccountTriggerHandler.disableTrigger = true;
        Account account = Test_DataFactory.generateAccounts('Account Name Test', 1, false)[0];
        account.RegionalLeader__c = UserInfo.getUserId();
        insert account;
    	AccountTriggerHandler.disableTrigger = false;
        
        Profile assignedProfile = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SALESPROFILE_LABEL LIMIT 1];
        User user = new User(ProfileId = assignedProfile.Id,
                             LastName = 'last',
                             Email = 'testclass@user.com',
                             Username = 'testclass@user.com'+System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'test',
                             Alias = 'test',
                             TimeZoneSidKey='America/Los_Angeles',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_US'
                        );
        UserTriggerHandler.disableTrigger = true;
        insert user;
        UserTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_requestAccountOwnershipNoManager(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];
		User user = [SELECT Id FROM User WHERE Email = 'testclass@user.com'];
        user.Code__c = ConstantsUtil.DMC_CODE_USERCONFIGURATION_MDT;
        UserTriggerHandler.disableTrigger = true;
        update user;
        UserTriggerHandler.disableTrigger = false;

        Test.startTest();
        System.runAs(user){
			RequestAccountOwnershipController.requestAccountOwnership(account.Id);
        }
        Test.stopTest();
    }
    
    @isTest
    public static void test_requestAccountOwnership(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];
		User user = [SELECT Id FROM User WHERE Email = 'testclass@user.com'];
        
        user.ManagerId = UserInfo.getUserId();
        user.Code__c = ConstantsUtil.DMC_CODE_USERCONFIGURATION_MDT;
        update user;
        
        Test.startTest();
        System.runAs(user){
			RequestAccountOwnershipController.requestAccountOwnership(account.Id);
            TeamMembershipController.AccountParam accParam = new TeamMembershipController.AccountParam();
            accParam.accountId = account.Id;
        	accParam.submitterId = UserInfo.getUserId();
            RequestAccountOwnershipController.requestAccountTeamMembership(account.Id);
            TeamMembershipController.addTeamMember(new List<TeamMembershipController.AccountParam>{accParam});
        }
        Test.stopTest();
        
    }
    
    @isTest
    public static void test_requestAccountOwnershipAlreadyOwner(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];

        Test.startTest();
        RequestAccountOwnershipController.requestAccountOwnership(account.Id);
        RequestAccountOwnershipController.validateUserAndRecord(account.Id);
        Test.stopTest();

    }
    
    @isTest
    public static void test_requestAccountOwnershipNoDMC(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];
		User user = [SELECT Id FROM User WHERE Email = 'testclass@user.com'];
        user.ManagerId = UserInfo.getUserId();
        update user;
        
        Test.startTest();
        System.runAs(user){
			RequestAccountOwnershipController.requestAccountOwnership(account.Id);
        }
        Test.stopTest();
    }
}