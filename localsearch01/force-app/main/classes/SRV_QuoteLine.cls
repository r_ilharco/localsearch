/**
* Author	   : Gennaro Casola <gcasola@deloitte.it>
* Date		   : 05-29-2019
* Sprint      : 1
* Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
* Testclass   : Test_ProductSubstitutionCmpController
* Description : Service Class for SBQQ__QuoteLine__c
* Changelog   : 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella
* 2020-09-30      SPIII-3455 - onePRESENCE Replacement Process - blockProductSubstituionByPlace
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class SRV_QuoteLine {
    
    public static  Map<String, Map<SBQQ__Quote__c, List<SBQQ__QuoteLine__c>>> constructQuoteLineMapByQuoteType(List<SBQQ__QuoteLine__c > listNew)
    {
        Map<String, Map<SBQQ__Quote__c, List<SBQQ__QuoteLine__c>>> result = new Map<String, Map<SBQQ__Quote__c, List<SBQQ__QuoteLine__c>>>();
        Set<Id> quoteIds = new Set<Id>();
        Map<Id, SBQQ__Quote__c> quoteMap = new Map<Id, SBQQ__Quote__c>();
        for(SBQQ__QuoteLine__c ql : listNew)
        {
            quoteIds.add(ql.SBQQ__Quote__c);
        }
        if(!quoteIds.isEmpty()){
            quoteMap = SEL_Quote.getQuoteById(quoteIds);
            for(SBQQ__QuoteLine__c ql: listNew){
                if(quoteMap.get(ql.SBQQ__Quote__c)!= null){
                    if(!result.containsKey(quoteMap.get(ql.SBQQ__Quote__c).SBQQ__Type__c))
                    {
                        result.put(quoteMap.get(ql.SBQQ__Quote__c).SBQQ__Type__c, new Map<SBQQ__Quote__c, List<SBQQ__QuoteLine__c>>());
                    }
                    
                    if(!result.get(quoteMap.get(ql.SBQQ__Quote__c).SBQQ__Type__c).containsKey(quoteMap.get(ql.SBQQ__Quote__c)))
                    {
                        result.get(quoteMap.get(ql.SBQQ__Quote__c).SBQQ__Type__c).put(quoteMap.get(ql.SBQQ__Quote__c), new List<SBQQ__QuoteLine__c>());
                    }
                    
                    result.get(quoteMap.get(ql.SBQQ__Quote__c).SBQQ__Type__c).get(quoteMap.get(ql.SBQQ__Quote__c)).add(ql);
                }
            }
        }
        return result;
    }
    
    
    public static Map<Id, Map<Id,String>> blockProductSubstituionByPlace (Id quoteId){
        Map<Id,Id> idsMapError = new  Map<Id,Id> ();
        Map<Id, Map<Id,String>> idsErrorMapmsg = new Map<Id, Map<Id,String>>();
        String quoteType;
        String errorMsg = '';
        List<SBQQ__QuoteLine__c> newQuoteLines = [SELECT Id, SBQQ__Quote__c, Product_Group__c, SBQQ__Quote__r.SBQQ__Type__c,Place_Name__c,
                                                  Place__c, SBQQ__Number__c, SBQQ__StartDate__c,Quote_Type__c, Account__c 
                                                  FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:quoteId AND Place__c != null];
        
        if(!newQuoteLines.isEmpty()){
            quoteType = newQuoteLines[0].SBQQ__Quote__r.SBQQ__Type__c;
            Map<Id, List<SBQQ__QuoteLine__c>> placeIdToCurrentConfiguredQuoteLines = new Map<Id, List<SBQQ__QuoteLine__c>>();
            Place_Process__mdt DownSellingValidation_config = [SELECT Id, QuoteStatus__c, ContractStatus__c, SubscriptionStatus__c FROM Place_Process__mdt WHERE MasterLabel = :ConstantsUtil.DOWNSELLING_VALIDATION_PLACE_PROCESS][0];
            Map<String, List<String>> productGroupToProductGroupsToCheck = new Map<String, List<String>>();
            List<Product_on_Place_Validation_Setting__mdt> placeValidationSettings = [SELECT Product_Group__c, Product_Group_to_Check__c FROM Product_on_Place_Validation_Setting__mdt];
            for(Product_on_Place_Validation_Setting__mdt currentSetting : placeValidationSettings) productGroupToProductGroupsToCheck.put(currentSetting.Product_Group__c, currentSetting.Product_Group_to_Check__c.split(';'));
            
            Set<String> subscriptionStatuses = new Set<String>(DownSellingValidation_config.SubscriptionStatus__c.split(';'));
            for(SBQQ__QuoteLine__c currentQuoteLine: newQuoteLines){
                if(String.isNotBlank(currentQuoteLine.Place__c)){
                    if(!placeIdToCurrentConfiguredQuoteLines.containsKey(currentQuoteLine.Place__c)) placeIdToCurrentConfiguredQuoteLines.put(currentQuoteLine.Place__c, new List<SBQQ__QuoteLine__c>{currentQuoteLine});
                    else placeIdToCurrentConfiguredQuoteLines.get(currentQuoteLine.Place__c).add(currentQuoteLine);
                }
            }
            
            List<SBQQ__Subscription__c> subscriptionsToCheck = SEL_Subscription.getSubscriptionByPlaceProductAndStatus(placeIdToCurrentConfiguredQuoteLines.keySet(), subscriptionStatuses);
            if(!subscriptionsToCheck.isEmpty()){
                Map<Id, List<SBQQ__Subscription__c>> placeIdToActiveSubscriptions = new Map<Id, List<SBQQ__Subscription__c>>();
                for(SBQQ__Subscription__c currentSubscription : subscriptionsToCheck){
                    if(String.isNotBlank(currentSubscription.Place__c) && String.isNotBlank(currentSubscription.SBQQ__Product__r.Product_Group__c)){
                        if(!placeIdToActiveSubscriptions.containsKey(currentSubscription.Place__c)) placeIdToActiveSubscriptions.put(currentSubscription.Place__c, new List<SBQQ__Subscription__c>{currentSubscription});
                        else placeIdToActiveSubscriptions.get(currentSubscription.Place__c).add(currentSubscription);
                    }
                }
                for(Id currentConfiguredPlaceId : placeIdToCurrentConfiguredQuoteLines.keySet()){
                    if(placeIdToActiveSubscriptions.containsKey(currentConfiguredPlaceId)){
                        for(SBQQ__QuoteLine__c currentQuoteLine : placeIdToCurrentConfiguredQuoteLines.get(currentConfiguredPlaceId)){
                            List<String> blockedBy = productGroupToProductGroupsToCheck.get(currentQuoteLine.Product_Group__c);
                            for(SBQQ__Subscription__c currentSubscription : placeIdToActiveSubscriptions.get(currentConfiguredPlaceId)){
                                if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quoteType)){
                                    if(blockedBy != null && blockedBy.contains(currentSubscription.SBQQ__Product__r.Product_Group__c) ){
                                        if(currentSubscription.In_Termination__c && currentSubscription.SBQQ__TerminatedDate__c >= currentQuoteLine.SBQQ__StartDate__c){
                                            errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                            if(currentSubscription.SBQQ__Account__c !=currentQuoteLine.Account__c){
                                            	errorMsg = Label.ActiveSubsOnSamePlaceDifferentAccount.replace('{AccountName}',currentSubscription.SBQQ__Account__r.Name).replace('{PlaceName}', currentQuoteLine.Place_Name__c);
                                            }
                                            idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                        }
                                        else if(!currentSubscription.In_Termination__c && currentSubscription.SBQQ__Contract__r.SBQQ__Evergreen__c){
                                            errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                            if(currentSubscription.SBQQ__Account__c !=currentQuoteLine.Account__c){
												errorMsg = Label.ActiveSubsOnSamePlaceDifferentAccount.replace('{AccountName}',currentSubscription.SBQQ__Account__r.Name).replace('{PlaceName}', currentQuoteLine.Place_Name__c);
                                            }
                                            idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                        }
                                        else if(!currentSubscription.In_Termination__c && !currentSubscription.SBQQ__Contract__r.SBQQ__Evergreen__c){
                                            if(currentQuoteLine.SBQQ__StartDate__c <= currentSubscription.SBQQ__EndDate__c){
												errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                                if(currentSubscription.SBQQ__Account__c !=currentQuoteLine.Account__c){
                                                	errorMsg = Label.ActiveSubsOnSamePlaceDifferentAccount.replace('{AccountName}',currentSubscription.SBQQ__Account__r.Name).replace('{PlaceName}', currentQuoteLine.Place_Name__c);
                                                }
                                               	idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                            }
                                        }
                                    }
                                }
                                else{
                                    if(blockedBy != null && blockedBy.contains(currentSubscription.SBQQ__Product__r.Product_Group__c) && !ConstantsUtil.PRODUCTCODE_SUBSTITION_EXCEPTION.contains(currentSubscription.SBQQ__Product__r.ProductCode)){
                                        if(currentSubscription.In_Termination__c && currentSubscription.SBQQ__TerminatedDate__c >= currentQuoteLine.SBQQ__StartDate__c){
                                            idsMapError.put(currentSubscription.Id,currentQuoteLine.Id);
                                            errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                            idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                        }
                                        else if(!currentSubscription.In_Termination__c && currentSubscription.SBQQ__Contract__r.SBQQ__Evergreen__c){
                                            idsMapError.put(currentSubscription.Id,currentQuoteLine.Id);
                                            errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                            idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                        }
                                        else if(!currentSubscription.In_Termination__c && !currentSubscription.SBQQ__Contract__r.SBQQ__Evergreen__c){
                                            if(currentQuoteLine.SBQQ__StartDate__c <= currentSubscription.SBQQ__EndDate__c){
                                                idsMapError.put(currentSubscription.Id,currentQuoteLine.Id);
                                                errorMsg = Label.ActiveSubsOnSamePlace.replace('{PlaceName}',currentQuoteLine.Place_Name__c);
                                            	idsErrorMapmsg.put(currentSubscription.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(idsMapError.isEmpty() && !ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quoteType)){
                Set<String> orderStatuses = new Set<String>{ConstantsUtil.ORDER_STATUS_ACTIVATED, ConstantsUtil.ORDER_STATUS_CANCELLED};
                    List<OrderItem> orderItemsToCheck = SEL_OrderItem.getOrderItemsByPlaceProductAndStatus(placeIdToCurrentConfiguredQuoteLines.keySet(), orderStatuses);
                Map<Id,String> placeToOrderNumber = new Map<Id,String>();
                if(!orderItemsToCheck.isEmpty()){
                    Map<Id, List<OrderItem>> placeIdToPendingOrderItems = new Map<Id, List<OrderItem>>();
                    
                    for(OrderItem currentOrderItem : orderItemsToCheck){
                        if(String.isNotBlank(currentOrderItem.Place__c) && String.isNotBlank(currentOrderItem.Product2.Product_Group__c)){
                            if(!placeIdToPendingOrderItems.containsKey(currentOrderItem.Place__c)) placeIdToPendingOrderItems.put(currentOrderItem.Place__c, new List<OrderItem>{currentOrderItem});
                            else placeIdToPendingOrderItems.get(currentOrderItem.Place__c).add(currentOrderItem); 
                        }
                    }
                    for(Id currentConfiguredPlaceId : placeIdToCurrentConfiguredQuoteLines.keySet()){
                        if(placeIdToPendingOrderItems.containsKey(currentConfiguredPlaceId)){
                            for(SBQQ__QuoteLine__c currentQuoteLine : placeIdToCurrentConfiguredQuoteLines.get(currentConfiguredPlaceId)){
                                List<String> blockedBy = productGroupToProductGroupsToCheck.get(currentQuoteLine.Product_Group__c);
                                for(OrderItem currentOrderItem : placeIdToPendingOrderItems.get(currentConfiguredPlaceId)){
                                    if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(currentQuoteLine.Quote_Type__c)){
                                        if(blockedBy != null && blockedBy.contains(currentOrderItem.Product2.Product_Group__c)){
                                            idsMapError.put(currentOrderItem.Id,currentQuoteLine.Id);
                            				errorMsg = Label.onePRESENCE_Upgrade_Orders_In_Progress.replace('{PlaceName}',currentQuoteLine.Place_Name__c ).replace('{OrderNumber}',currentOrderItem.Order.OrderNumber);
                                            idsErrorMapmsg.put(currentOrderItem.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(idsMapError.isEmpty() && !ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quoteType)){
                    Set<String> quoteStatus = new Set<String>(DownSellingValidation_config.QuoteStatus__c.split(';'));
                    List<SBQQ__QuoteLine__c> existingQuoteLinesToCheck = SEL_SBQQQuoteLine.getQLinesByPlaceProductAndStatus(placeIdToCurrentConfiguredQuoteLines.keySet());
                    if(!existingQuoteLinesToCheck.isEmpty()){
                        Map<Id, List<SBQQ__QuoteLine__c>> placeIdToPendingQuoteLines = new Map<Id, List<SBQQ__QuoteLine__c>>();
                        for(SBQQ__QuoteLine__c currentPendingQuoteLine : existingQuoteLinesToCheck){
                            if(String.isNotBlank(currentPendingQuoteLine.Place__c) && String.isNotBlank(currentPendingQuoteLine.Product_Group__c)){
                                if(!placeIdToPendingQuoteLines.containsKey(currentPendingQuoteLine.Place__c)) placeIdToPendingQuoteLines.put(currentPendingQuoteLine.Place__c, new List<SBQQ__QuoteLine__c>{currentPendingQuoteLine});
                                else placeIdToPendingQuoteLines.get(currentPendingQuoteLine.Place__c).add(currentPendingQuoteLine);
                            }
                        }
                        for(Id currentConfiguredPlaceId : placeIdToCurrentConfiguredQuoteLines.keySet()){
                            if(placeIdToPendingQuoteLines.containsKey(currentConfiguredPlaceId)){
                                for(SBQQ__QuoteLine__c currentQuoteLine : placeIdToCurrentConfiguredQuoteLines.get(currentConfiguredPlaceId)){
                                    List<String> blockedBy = productGroupToProductGroupsToCheck.get(currentQuoteLine.Product_Group__c);
                                    for(SBQQ__QuoteLine__c currentPendingQuoteLine : placeIdToPendingQuoteLines.get(currentConfiguredPlaceId)){
                                        if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(currentQuoteLine.Quote_Type__c)){
                                            if(blockedBy != null && blockedBy.contains(currentPendingQuoteLine.Product_Group__c)){
                                                idsMapError.put(currentPendingQuoteLine.Id,currentQuoteLine.Id);
												errorMsg = Label.onePRESENCE_Upgrade_Quote_Presented.replace('{PlaceName}',currentQuoteLine.Place_Name__c).replace('{Quote Number}',currentPendingQuoteLine.SBQQ__Quote__r.Name);
                                            	idsErrorMapmsg.put(currentPendingQuoteLine.Id, new Map<Id,String>{currentQuoteLine.Id => errorMsg});
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return idsErrorMapmsg;
    }
}