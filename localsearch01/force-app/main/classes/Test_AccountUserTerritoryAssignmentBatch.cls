/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* TestClass of AccountUserTerritoryAssignmentBatch
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created		  2020-06-05
* @systemLayer    Test
* @TestClass 	  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest 
public class Test_AccountUserTerritoryAssignmentBatch {
    
    @TestSetup 
    static void setup(){
        delete [SELECT Id FROM ObjectTerritory2Association];
        UserTerritoryAssignmentTriggerHandler.disableTrigger = true;
        delete [SELECT Id FROM UserTerritory2Association];
        UserTerritoryAssignmentTriggerHandler.disableTrigger = false;
        Test_DataFactory.insertFutureUsers();
        Territory2 firstLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1];
        Territory2 secondLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = :firstLevelTerritory.Id AND Territory2Model.State = 'Active' LIMIT 1];
        //Territory2 thirdLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = :secondLevelTerritory.Id AND Territory2Model.State = 'Active' LIMIT 1];
        
        List<Account> accountsList = Test_DataFactory.createAccounts('Test', 3);
        insert accountsList;
        
        List<ObjectTerritory2Association> ot2aToInsert = new List<ObjectTerritory2Association>();
        
        ObjectTerritory2Association ot2a = new ObjectTerritory2Association();
        ot2a.objectid = accountsList[0].id;
        ot2a.Territory2Id = firstLevelTerritory.Id;
        ot2a.AssociationCause = 'Territory2Manual';
        ot2aToInsert.add(ot2a);
        
        ObjectTerritory2Association ot2a2 = new ObjectTerritory2Association();
        ot2a2.objectid = accountsList[1].id;
        ot2a2.Territory2Id = secondLevelTerritory.Id;
        ot2a2.AssociationCause = 'Territory2Manual';
        ot2aToInsert.add(ot2a2);
        
        /*ObjectTerritory2Association ot2a3 = new ObjectTerritory2Association();
        ot2a3.objectid = accountsList[2].id;
        ot2a3.Territory2Id = thirdLevelTerritory.Id;
        ot2a3.AssociationCause = 'Territory2Manual';
        ot2aToInsert.add(ot2a3);*/
        
        insert ot2aToInsert;
    }

	@IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_defaultConstructor(){
        Test.startTest();
        	Database.executeBatch(new AccountUserTerritoryAssignmentBatch(),2000);
        Test.stopTest();
    }
    
    /*@IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_executeSchedulable(){
        Test.startTest();
        	AccountUserTerritoryAssignmentBatch accountUserTerritoryAssignmentBatch = new AccountUserTerritoryAssignmentBatch();
            String sch = '20 30 8 10 2 ?';
            String jobID = System.schedule('AccountUserTerritoryAssignmentBatch', sch, accountUserTerritoryAssignmentBatch);
        Test.stopTest();
    }*/
    
    @IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_insertInfo(){
        Territory2 firstLevelTerritory;
        Territory2 secondLevelTerritory;
        Territory2 thirdLevelTerritory;
        Map<Id, ObjectTerritory2Association> ot2aMap = new Map<Id, ObjectTerritory2Association>([SELECT Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN (SELECT Id FROM Account)]);
        Set<Id> territoryIds = new Set<Id>();
        for(ObjectTerritory2Association ot2a : ot2aMap.values())
            territoryIds.add(ot2a.Territory2Id);
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id, ParentTerritory2.ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoryIds and Territory2.Territory2Model.State='Active']);
        
        for(Territory2 t : territoriesMap.values()){
            if(t.ParentTerritory2Id == NULL)
                firstLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id == NULL)
                secondLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                thirdLevelTerritory = t;
        }
        
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, Code__c, Username, FederationIdentifier FROM User WHERE Username IN ('dmctester@test.com','managertester@test.com','smdtester@test.com','sdrtester@test.com','dmc2tester@test.com','manager2tester@test.com','smd2tester@test.com','sdr2tester@test.com')]);
        Map<String,Id> usernameToUserIdMap = new Map<String,Id>();
        
        for(User u : usersMap.values())
            usernameToUserIdMap.put(u.Username, u.Id);
        
        List<UserTerritory2Association> ut2aListToInsert = new List<UserTerritory2Association>();
        
     /*   UserTerritory2Association ut2a = new UserTerritory2Association();
        ut2a.Territory2Id = firstLevelTerritory.Id;
        ut2a.UserId = usernameToUserIdMap.get('sdrtester@test.com');
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a);*/
        
       /* UserTerritory2Association ut2a2 = new UserTerritory2Association();
        ut2a2.Territory2Id = firstLevelTerritory.Id;
        ut2a2.UserId = usernameToUserIdMap.get('managertester@test.com');
        ut2a2.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a2);*/
        
    /*   UserTerritory2Association ut2a3 = new UserTerritory2Association();
        ut2a3.Territory2Id = firstLevelTerritory.Id;
        ut2a3.UserId = usernameToUserIdMap.get('smdtester@test.com');
        ut2a3.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a3);*/
        
        UserTerritory2Association ut2a4 = new UserTerritory2Association();
       // ut2a4.Territory2Id = thirdLevelTerritory.Id;
       	ut2a4.Territory2Id = secondLevelTerritory.Id;
        ut2a4.UserId = usernameToUserIdMap.get('dmctester@test.com');
        ut2a4.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a4);
        
        UserTerritory2Association ut2a12 = new UserTerritory2Association();
        ut2a12.Territory2Id = firstLevelTerritory.Id;
        ut2a12.UserId = usernameToUserIdMap.get('sdr2tester@test.com');
        ut2a12.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a12);
        
        UserTerritory2Association ut2a22 = new UserTerritory2Association();
        ut2a22.Territory2Id = firstLevelTerritory.Id;
        ut2a22.UserId = usernameToUserIdMap.get('manager2tester@test.com');
        ut2a22.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a22);
        
        UserTerritory2Association ut2a32 = new UserTerritory2Association();
        ut2a32.Territory2Id = firstLevelTerritory.Id;
        ut2a32.UserId = usernameToUserIdMap.get('smd2tester@test.com');
        ut2a32.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a32);
        
        UserTerritory2Association ut2a42 = new UserTerritory2Association();
       // ut2a42.Territory2Id = thirdLevelTerritory.Id;
        ut2a42.Territory2Id = secondLevelTerritory.Id;
        ut2a42.UserId = usernameToUserIdMap.get('dmc2tester@test.com');
        ut2a42.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a42);
        
        insert ut2aListToInsert;
        
        Test.startTest();
        	Database.executeBatch(new AccountUserTerritoryAssignmentBatch(90),2000);
        Test.stopTest();
        
    }
    
    @IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_updateInfo(){
        Territory2 firstLevelTerritory;
        Territory2 secondLevelTerritory;
        Territory2 thirdLevelTerritory;
        Map<Id, ObjectTerritory2Association> ot2aMap = new Map<Id, ObjectTerritory2Association>([SELECT Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN (SELECT Id FROM Account)]);
        Set<Id> territoryIds = new Set<Id>();
        for(ObjectTerritory2Association ot2a : ot2aMap.values())
            territoryIds.add(ot2a.Territory2Id);
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id, ParentTerritory2.ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoryIds and Territory2.Territory2Model.State='Active']);
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, RegionalDirector__c, RegionalLeader__c, Sales_manager_substitute__c FROM Account WHERE Id IN (SELECT ObjectId FROM ObjectTerritory2Association)]);        
        
        for(Territory2 t : territoriesMap.values()){
            if(t.ParentTerritory2Id == NULL)
                firstLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id == NULL)
                secondLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                thirdLevelTerritory = t;
        }
        
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, Code__c, Username, FederationIdentifier FROM User WHERE Username IN ('dmctester@test.com','managertester@test.com','smdtester@test.com','sdrtester@test.com','dmc2tester@test.com','manager2tester@test.com','smd2tester@test.com','sdr2tester@test.com')]);
        Map<String,Id> usernameToUserIdMap = new Map<String,Id>();
        
        for(User u : usersMap.values())
            usernameToUserIdMap.put(u.Username, u.Id);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            for(Account acc : accountsMap.values()){
                acc.RegionalDirector__c = usernameToUserIdMap.get('sdrtester@test.com');
                acc.RegionalLeader__c = usernameToUserIdMap.get('managertester@test.com');
                acc.Sales_manager_substitute__c = usernameToUserIdMap.get('smdtester@test.com');
            }
            AccountTriggerHandler.disableTrigger = true;
            update accountsMap.values();
            AccountTriggerHandler.disableTrigger = false;
        }
        
        List<UserTerritory2Association> ut2aListToInsert = new List<UserTerritory2Association>();
        
        UserTerritory2Association ut2a = new UserTerritory2Association();
        ut2a.Territory2Id = firstLevelTerritory.Id;
        ut2a.UserId = usernameToUserIdMap.get('sdrtester@test.com');
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a);
        
        UserTerritory2Association ut2a2 = new UserTerritory2Association();
        ut2a2.Territory2Id = secondLevelTerritory.Id;
        ut2a2.UserId = usernameToUserIdMap.get('managertester@test.com');
        ut2a2.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a2);
        
        UserTerritory2Association ut2a3 = new UserTerritory2Association();
        ut2a3.Territory2Id = secondLevelTerritory.Id;
        ut2a3.UserId = usernameToUserIdMap.get('smdtester@test.com');
        ut2a3.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a3);
        
        UserTerritory2Association ut2a4 = new UserTerritory2Association();
        //ut2a4.Territory2Id = thirdLevelTerritory.Id;
        ut2a4.Territory2Id = secondLevelTerritory.Id;
        ut2a4.UserId = usernameToUserIdMap.get('dmctester@test.com');
        ut2a4.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a4);
        
        /*UserTerritory2Association ut2a12 = new UserTerritory2Association();
        ut2a12.Territory2Id = firstLevelTerritory.Id;
        ut2a12.UserId = usernameToUserIdMap.get('sdr2tester@test.com');
        ut2a12.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a12);*/
        
      /*  UserTerritory2Association ut2a22 = new UserTerritory2Association();
        ut2a22.Territory2Id = secondLevelTerritory.Id;
        ut2a22.UserId = usernameToUserIdMap.get('manager2tester@test.com');
        ut2a22.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a22);*/
        
      /*  UserTerritory2Association ut2a32 = new UserTerritory2Association();
        ut2a32.Territory2Id = secondLevelTerritory.Id;
        ut2a32.UserId = usernameToUserIdMap.get('smd2tester@test.com');
        ut2a32.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a32);*/
        
        UserTerritory2Association ut2a42 = new UserTerritory2Association();
        //ut2a42.Territory2Id = thirdLevelTerritory.Id;
        ut2a42.Territory2Id = secondLevelTerritory.Id;
        ut2a42.UserId = usernameToUserIdMap.get('dmc2tester@test.com');
        ut2a42.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a42);
        
        insert ut2aListToInsert;
        
        Test.startTest();
        	Database.executeBatch(new AccountUserTerritoryAssignmentBatch(90),2000);
        Test.stopTest();
        
    }
    
    @IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_updateInfo2(){
        Territory2 firstLevelTerritory;
        Territory2 secondLevelTerritory;
        Territory2 thirdLevelTerritory;
        Map<Id, ObjectTerritory2Association> ot2aMap = new Map<Id, ObjectTerritory2Association>([SELECT Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN (SELECT Id FROM Account)]);
        Set<Id> territoryIds = new Set<Id>();
        for(ObjectTerritory2Association ot2a : ot2aMap.values())
            territoryIds.add(ot2a.Territory2Id);
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id, ParentTerritory2.ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoryIds and Territory2.Territory2Model.State='Active']);
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, RegionalDirector__c, RegionalLeader__c, Sales_manager_substitute__c FROM Account WHERE Id IN (SELECT ObjectId FROM ObjectTerritory2Association)]);        
        
        for(Territory2 t : territoriesMap.values()){
            if(t.ParentTerritory2Id == NULL)
                firstLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id == NULL)
                secondLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                thirdLevelTerritory = t;
        }
        
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, Code__c, Username, FederationIdentifier FROM User WHERE Username IN ('dmctester@test.com','managertester@test.com','smdtester@test.com','sdrtester@test.com','dmc2tester@test.com','manager2tester@test.com','smd2tester@test.com','sdr2tester@test.com')]);
        Map<String,Id> usernameToUserIdMap = new Map<String,Id>();
        
        for(User u : usersMap.values())
            usernameToUserIdMap.put(u.Username, u.Id);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            for(Account acc : accountsMap.values()){
                acc.RegionalDirector__c = usernameToUserIdMap.get('sdr2tester@test.com');
                acc.RegionalLeader__c = usernameToUserIdMap.get('manager2tester@test.com');
                acc.Sales_manager_substitute__c = usernameToUserIdMap.get('smd2tester@test.com');
            }
            AccountTriggerHandler.disableTrigger = true;
            update accountsMap.values();
            AccountTriggerHandler.disableTrigger = false;
        }
        
        List<UserTerritory2Association> ut2aListToInsert = new List<UserTerritory2Association>();
        
     /*   UserTerritory2Association ut2a = new UserTerritory2Association();
        ut2a.Territory2Id = firstLevelTerritory.Id;
        ut2a.UserId = usernameToUserIdMap.get('sdrtester@test.com');
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a);*/
        
       /* UserTerritory2Association ut2a2 = new UserTerritory2Association();
        ut2a2.Territory2Id = secondLevelTerritory.Id;
        ut2a2.UserId = usernameToUserIdMap.get('managertester@test.com');
        ut2a2.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a2);*/
        
       /* UserTerritory2Association ut2a3 = new UserTerritory2Association();
        ut2a3.Territory2Id = secondLevelTerritory.Id;
        ut2a3.UserId = usernameToUserIdMap.get('smdtester@test.com');
        ut2a3.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a3);*/
        
        UserTerritory2Association ut2a4 = new UserTerritory2Association();
        //ut2a4.Territory2Id = thirdLevelTerritory.Id;
        ut2a4.Territory2Id = secondLevelTerritory.Id;
        ut2a4.UserId = usernameToUserIdMap.get('dmctester@test.com');
        ut2a4.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a4);
        
        UserTerritory2Association ut2a12 = new UserTerritory2Association();
        ut2a12.Territory2Id = firstLevelTerritory.Id;
        ut2a12.UserId = usernameToUserIdMap.get('sdr2tester@test.com');
        ut2a12.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a12);
        
        UserTerritory2Association ut2a22 = new UserTerritory2Association();
        ut2a22.Territory2Id = secondLevelTerritory.Id;
        ut2a22.UserId = usernameToUserIdMap.get('manager2tester@test.com');
        ut2a22.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a22);
        
        UserTerritory2Association ut2a32 = new UserTerritory2Association();
        ut2a32.Territory2Id = secondLevelTerritory.Id;
        ut2a32.UserId = usernameToUserIdMap.get('smd2tester@test.com');
        ut2a32.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a32);
        
        UserTerritory2Association ut2a42 = new UserTerritory2Association();
       // ut2a42.Territory2Id = thirdLevelTerritory.Id;
       	ut2a42.Territory2Id = secondLevelTerritory.Id;
        ut2a42.UserId = usernameToUserIdMap.get('dmc2tester@test.com');
        ut2a42.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a42);
        
        insert ut2aListToInsert;
        List<UserTerritory2Association> ut2aListToDelete = new List<UserTerritory2Association>();
        ut2aListToDelete.add(ut2a12);
        ut2aListToDelete.add(ut2a22);
        ut2aListToDelete.add(ut2a32);
        ut2aListToDelete.add(ut2a42);
        delete ut2aListToDelete;
        
        Test.startTest();
        	Database.executeBatch(new AccountUserTerritoryAssignmentBatch(-1),2000);
        Test.stopTest();
        
    }
    
    @IsTest
    public static void test_AccountUserTerritoryAssignmentBatch_deleteInfo(){
        Territory2 firstLevelTerritory;
        Territory2 secondLevelTerritory;
        Territory2 thirdLevelTerritory;
        Map<Id, ObjectTerritory2Association> ot2aMap = new Map<Id, ObjectTerritory2Association>([SELECT Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN (SELECT Id FROM Account)]);
        Set<Id> territoryIds = new Set<Id>();
        for(ObjectTerritory2Association ot2a : ot2aMap.values())
            territoryIds.add(ot2a.Territory2Id);
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id, ParentTerritory2.ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoryIds and Territory2.Territory2Model.State='Active']);
        Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, Name, RegionalDirector__c, RegionalLeader__c, Sales_manager_substitute__c FROM Account WHERE Id IN (SELECT ObjectId FROM ObjectTerritory2Association)]);
        
        for(Territory2 t : territoriesMap.values()){
            if(t.ParentTerritory2Id == NULL)
                firstLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id == NULL)
                secondLevelTerritory = t;
            else if(t.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2Id != NULL && t.ParentTerritory2.ParentTerritory2.ParentTerritory2Id == NULL)
                thirdLevelTerritory = t;
        }
        
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, Code__c, Username, FederationIdentifier FROM User WHERE Username IN ('dmctester@test.com','managertester@test.com','smdtester@test.com','sdrtester@test.com','dmc2tester@test.com','manager2tester@test.com','smd2tester@test.com','sdr2tester@test.com')]);
        Map<String,Id> usernameToUserIdMap = new Map<String,Id>();
        
        for(User u : usersMap.values())
            usernameToUserIdMap.put(u.Username, u.Id);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            for(Account acc : accountsMap.values()){
                acc.RegionalDirector__c = usernameToUserIdMap.get('sdrtester@test.com');
                acc.RegionalLeader__c = usernameToUserIdMap.get('managertester@test.com');
                acc.Sales_manager_substitute__c = usernameToUserIdMap.get('smdtester@test.com');
            }
            AccountTriggerHandler.disableTrigger = true;
            update accountsMap.values();
            AccountTriggerHandler.disableTrigger = false;
        }
        
        List<UserTerritory2Association> ut2aListToInsert = new List<UserTerritory2Association>();
        
        /*UserTerritory2Association ut2a = new UserTerritory2Association();
        ut2a.Territory2Id = firstLevelTerritory.Id;
        ut2a.UserId = usernameToUserIdMap.get('sdrtester@test.com');
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a);*/
        
        UserTerritory2Association ut2a2 = new UserTerritory2Association();
        ut2a2.Territory2Id = secondLevelTerritory.Id;
        ut2a2.UserId = usernameToUserIdMap.get('managertester@test.com');
        ut2a2.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a2);
        
       /* UserTerritory2Association ut2a3 = new UserTerritory2Association();
        ut2a3.Territory2Id = secondLevelTerritory.Id;
        ut2a3.UserId = usernameToUserIdMap.get('smdtester@test.com');
        ut2a3.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a3);*/
       
        
        UserTerritory2Association ut2a4 = new UserTerritory2Association();
        ut2a4.Territory2Id = secondLevelTerritory.Id;
        ut2a4.UserId = usernameToUserIdMap.get('dmctester@test.com');
        ut2a4.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a4);
        
        UserTerritory2Association ut2a12 = new UserTerritory2Association();
        ut2a12.Territory2Id = firstLevelTerritory.Id;
        ut2a12.UserId = usernameToUserIdMap.get('sdr2tester@test.com');
        ut2a12.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SDI;
        ut2aListToInsert.add(ut2a12);
        
        /*UserTerritory2Association ut2a22 = new UserTerritory2Association();
        ut2a22.Territory2Id = secondLevelTerritory.Id;
        ut2a22.UserId = usernameToUserIdMap.get('manager2tester@test.com');
        ut2a22.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        ut2aListToInsert.add(ut2a22);*/
        
        UserTerritory2Association ut2a32 = new UserTerritory2Association();
        ut2a32.Territory2Id = secondLevelTerritory.Id;
        ut2a32.UserId = usernameToUserIdMap.get('smd2tester@test.com');
        ut2a32.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_SMD;
        ut2aListToInsert.add(ut2a32);
        
        UserTerritory2Association ut2a42 = new UserTerritory2Association();
        ut2a42.Territory2Id = secondLevelTerritory.Id;
        // ut2a42.Territory2Id = thirdLevelTerritory.Id;
        ut2a42.UserId = usernameToUserIdMap.get('dmc2tester@test.com');
        ut2a42.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        ut2aListToInsert.add(ut2a42);
        
        insert ut2aListToInsert;
        delete ut2aListToInsert;
        
        Test.startTest();
        	Database.executeBatch(new AccountUserTerritoryAssignmentBatch(90),2000);
        Test.stopTest();
        
    }
    
    @Future 
    static void updateAccounts(List<Id> accountIds, Id regionalDirector, Id regionalLeader, Id salesManagerSubstitute, Id salesUser){
        List<Account> accountsToUpdateList = new List<Account>();
        for(Id accountId : accountIds){
            Account acc = new Account();
            acc.Id = accountId;
            acc.RegionalDirector__c = regionalDirector;
            acc.RegionalLeader__c = regionalLeader;
            acc.Sales_manager_substitute__c = salesManagerSubstitute;
            accountsToUpdateList.add(acc);
        }
        update accountsToUpdateList;
    }
    
}