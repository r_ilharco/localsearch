@isTest
public class WhenPlaceChangedToPrivatePBTest {
    
    @isTest
    static void testPlaceChangedToPrivate(){
        
        try{
            
            
            
            Account acc = createAccount();
            
            Place__c place = new Place__c();
            place.Account__c = acc.Id;
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            place.Place_Type__c= 'Private';
            //place.QuoteItemId__c= null;
            //place.QuoteItemId__c= quot.Id;
            
            
            test.startTest();
            insert place;
            //update place;
            System.debug('Place: '+ place);
            //place.Place_Type__c= 'Private';
            //update place;
            
            Place__c place1 = new Place__c();
            //place1.Account__c = acc.Id;
            place1.Name = 'test place1';
            place1.LastName__c = 'test last1';
            place1.Company__c = 'test company1';
            place1.City__c = 'test city1';
            place1.Country__c = 'test coountry1';
            place1.PostalCode__c = '1234';
            //place1.Place_Type__c= 'Private';
            //place1.QuoteItemId__c= null;
            
            
            
            
            insert place1;
            place1.Place_Type__c= 'Private';
            update place1;
            //update place1;
            System.debug('Place1: '+ place1);
            test.stopTest();
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
            
            
            
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }   
        
        
        
    }
    
    @isTest
    public static Account CreateAccount(){
        User u = CreateUser();
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.GoldenRecordID__c = 'GRID';       
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh'; 
        acc.BillingCity = 'test city';
        acc.BillingCountry = 'test country';
        acc.BillingPostalCode = '123';
        acc.BillingState = 'test state';
        acc.PreferredLanguage__c = 'German';
        acc.OwnerId = u.Id;        
        
        insert acc;
        System.Debug('Account: '+ acc);  
        return acc;
    }
    
    @isTest
    static User CreateUser(){
        
        User u = new user();
        u.LastName = 'Test Code';
        u.Email = 'iqbal.rocky@arollotech.com';
        u.Alias = 'Tcode';
        u.Username = 'iqbal.roc@arollo.com';
        u.CommunityNickname = 'test12';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = '00e1r0000027zBpAAI';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        
        insert u;
        system.debug('User:' +u);
        return u;
        
    }
    
}