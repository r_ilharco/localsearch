@isTest
public class Test_SetExpiredContractsBatch {
    @testSetup
    public static void testSetup(){
        Test_Billing.testSetup();
        List<Contract> contracts = [SELECT Id, EndDate FROM Contract];
        for(Contract c : contracts){
            c.EndDate = Date.today().addDays(-1);
        }
        update contracts;
    }
    
    private static testmethod void groupingBatch(){
        List<Contract> contracts = [SELECT Id FROM Contract];
        Date todayDate = Date.today();
        Date firstDayOfTheNextMonth = todayDate.addMonths(1).toStartOfMonth();        
        test.startTest();
        SetExpiredContractsBatch obj = new SetExpiredContractsBatch();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        obj.execute(null,contracts);
        obj.execute(null);
        test.stopTest();   
    }
    
    private static testmethod void testTerminatedSubscriptions(){
        test.startTest();
        List<Contract> contracts = [SELECT Id FROM Contract];
        Map<id, Contract> keys = new Map<id, Contract>(contracts);
        List<SBQQ__Subscription__c> subs = [SELECT Id, Subsctiption_Status__c, In_Termination__c 
         FROM SBQQ__Subscription__c
         WHERE SBQQ__Contract__c IN :keys.keySet()];
        
        subs[0].Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
        update subs[0];
        Date todayDate = Date.today();
        Date firstDayOfTheNextMonth = todayDate.addMonths(1).toStartOfMonth();        
        
        SetExpiredContractsBatch obj = new SetExpiredContractsBatch();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        obj.execute(null,contracts);
        obj.execute(null);
        test.stopTest();   
        
        for( SBQQ__Subscription__c subs2 : [SELECT Id, Subsctiption_Status__c, In_Termination__c 
         									FROM SBQQ__Subscription__c
                                            WHERE SBQQ__Contract__c IN :keys.keySet()]){
                                                if(subs[0].id == subs2.id){
           											system.assertEquals(ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED, subs2.Subsctiption_Status__c);
                                                } else {
                                                    system.assertEquals(ConstantsUtil.SUBSCRIPTION_STATUS_EXPIRED, subs2.Subsctiption_Status__c);
                                                }
                                            }

    }
    
}