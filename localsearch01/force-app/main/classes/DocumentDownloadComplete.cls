/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Invoked when the document is succesfully uploaded on Salesforce by TIBCO after the Digital
* Signature process is completed.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-09-23
* @systemLayer    Invocation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@RestResource(urlMapping='/document-download-complete')
global without sharing class DocumentDownloadComplete{
    
    @HttpPost
    global static void signedDocumentReceived(){
        
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;
        
        try{
			DocumentDownloadCompleteRequest documentDownloadCompleteRequest = (DocumentDownloadCompleteRequest)JSON.deserialize(request.requestBody.toString(), DocumentDownloadCompleteRequest.class);
            
            String validationError = SRV_DocumentDownloadComplete.requestValidation(documentDownloadCompleteRequest);
           	if(!String.isEmpty(validationError)) throw new MissingFieldsException(validationError);
            else{
                
                SBQQ__Quote__c quote = SEL_Quote.getQuoteById(documentDownloadCompleteRequest.quoteId);
                
                if(quote != NULL){
					SRV_DocumentDownloadComplete.updateQuoteDocument(Id.valueOf(documentDownloadCompleteRequest.quoteDocumentId), documentDownloadCompleteRequest.signatureDate);
                	SRV_DocumentDownloadComplete.updateQuote(quote, documentDownloadCompleteRequest.signatureDate, documentDownloadCompleteRequest.distributionPublicUrl);
                	SRV_DocumentDownloadComplete.sendNotificationToSales(quote, Id.valueOf(documentDownloadCompleteRequest.quoteDocumentId));
                }
            }
        }
        catch(MissingFieldsException ce){
            response.responseBody = Blob.valueOf(ce.getMessage());
            response.statusCode = 400;
        }
        catch(Exception e){
            response.responseBody = Blob.valueOf(e.getMessage());
            response.statusCode = 500;
        }
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Wrapper class for REST request
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public class DocumentDownloadCompleteRequest{

        public String quoteId {get; set;}
        public String quoteDocumentId {get; set;}
        public String signatureDate {get; set;}
        public String distributionPublicUrl {get; set;}
        
        public DocumentDownloadCompleteRequest(){}
        
        public DocumentDownloadCompleteRequest(String quoteId, String quoteDocumentId, String signatureDate, String distributionPublicUrl){
            this.quoteId = quoteId;
            this.quoteDocumentId = quoteDocumentId;
            this.signatureDate = signatureDate;
            this.distributionPublicUrl = distributionPublicUrl;
        }
    }
    
    public class MissingFieldsException extends Exception {} 
}