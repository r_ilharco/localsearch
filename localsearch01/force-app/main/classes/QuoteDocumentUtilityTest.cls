@isTest
public class QuoteDocumentUtilityTest implements HttpCalloutMock{
    
    public HttpResponse respond(HTTPRequest req){
        String body = '{"test": "test"}';
        HttpResponse resp = new HttpResponse();
        resp.setStatus('OK');
        resp.setStatusCode(Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS));
        resp.setBody(body);
        return resp;
    }
    
    @isTest
	public static void testProcessQuoteDocument()
    {                
        Try{
            Account acc = createAccount();  
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
                
            insert opp;
            System.Debug('opp '+ opp); 
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            
            insert quot;
            System.Debug('Quote '+ quot);
                        
            List<Id> quoteIdList = new List<Id>();
            quoteIdList.add(quot.Id);
            
            QuoteTemplateCreationScript qt = new QuoteTemplateCreationScript();
         	qt.createQuoteTemplate();
                	
            Test.setMock(HttpCalloutMock.class, new QuoteDocumentUtilityTest());
            Test.startTest();  
            
            try{
                QuoteDocumentUtility.processQuoteDocument(null);
            }catch(Exception e){	                            
                System.Assert(true, e.getMessage());
            }
            
            try{
                QuoteDocumentUtility.processQuoteDocument(quoteIdList);
            }catch(Exception e){	
                System.Assert(true, e.getMessage());
            }
            
            try{
                QuoteDocumentUtility.generateAndSaveQuoteDocument(null, null);
            }catch(Exception e){	
                System.Assert(true, e.getMessage());
            }
            
            try{
                QuoteDocumentUtility.getQuoteDocumentName(null, null);
            }catch(Exception e){	                            
                System.Assert(true, e.getMessage());
            }
            
            try{
                QuoteDocumentUtility.getQuoteTemplateId(null);
            }catch(Exception e){	                            
                System.Assert(true, e.getMessage());
            }
            
            Test.stopTest();
            
        }catch(Exception e){	                            
           System.Assert(false, e.getMessage());
        }
    }
    
    
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'XXX ';
        acc.GoldenRecordID__c = 'GRID';
        
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh';    
                   
        insert acc;
        System.Debug('Account '+ acc); 
              
        return acc;
    }

}