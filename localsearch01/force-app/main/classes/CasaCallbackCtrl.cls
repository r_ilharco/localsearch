public without sharing class CasaCallbackCtrl{
    
    public boolean isError {set; get;}
    
    public CasaCallbackCtrl(){
        isError = false;
    }
    
    public void init(){
        
        Map<String,String> inputParams = new Map<String,String>();
        SRV_CasaCallback.CasaParams casaParams = SRV_CasaCallback.getInputParamsFromPage(ApexPages.currentPage());
        
        String formattedStartDate; 
        String formattedEndDate; 
        String formattedExpDate;
        
        if(casaParams.start_date != null){
            formattedStartDate = SRV_CasaCallback.formatDate(casaParams.start_date);
        }
        if(casaParams.end_date != null){
            formattedEndDate = SRV_CasaCallback.formatDate(casaParams.end_date);
        }
        if(casaParams.expiration_date != null){
            formattedExpDate = SRV_CasaCallback.formatDate(casaParams.expiration_date);
        }
        
        //POST callout to CASA
        String baseCatgory = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_CATEGORY);
        String baseLocation = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_LOCATION);
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('category', baseCatgory+casaParams.category);
        if(casaParams.region != null && casaParams.region != 'all'){
            parameters.put('region', baseLocation+casaParams.region); 
        }
        parameters.put('start_date', formattedStartDate);
        parameters.put('end_date', formattedEndDate);
        parameters.put('expiration_date', formattedExpDate);
        parameters.put('slot', casaParams.slot);
        parameters.put('product_code', casaParams.product_code);
        parameters.put('language', casaParams.language);
        
        SRV_EnhancedExtProdConfiguration.CasaAdContextResponse casaResponse = SRV_EnhancedExtProdConfiguration.adContextCallout(casaParams.quote_item, parameters);
        if(casaResponse != null){
            casaParams.ad_context_allocation = casaResponse.id;
            if(!SRV_CasaCallback.publishPlatformEvent(casaParams)){
                /*RestResponse res = RestContext.response;
                res.responseBody = Blob.valueOf('Internal Server Error');
                res.statusCode = 500;*/
                isError = true;
            }
        }
        else{
            if(!SRV_CasaCallback.publishErrorPlatformEvent()){
                /*RestResponse res = RestContext.response;
                res.responseBody = Blob.valueOf('Internal Server Error');
                res.statusCode = 500;*/
            }
        }
    }
}