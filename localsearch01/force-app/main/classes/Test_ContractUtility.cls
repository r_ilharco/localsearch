/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-11-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-11-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class Test_ContractUtility {
	
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        		        
        SBQQ.TriggerControl.enable();
        String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        
    }
    
    @isTest
    public static void test_cancellationContract(){
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        ContractUtility.cancellationContract(contract.Id);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Cancellation__c, Cancel_Date__c FROM Contract LIMIT 1];
        System.assertEquals(true, afterTestContract.In_Cancellation__c);
        System.assertEquals(Date.today(), afterTestContract.Cancel_Date__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(true, currentSubscription.In_Termination__c);
            System.assertEquals(Date.today(), currentSubscription.Termination_Date__c);
        }
    }
    
    @isTest
    public static void test_cancellationContractInvalidStatus(){
        
        Contract contract = [SELECT Id, Status FROM Contract LIMIT 1];
        contract.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
        update contract;
        
        Test.startTest();
        ContractUtility.cancellationContract(contract.Id);
        Test.stopTest();
        
        Contract afterTestContract = [SELECT Id, In_Cancellation__c, Cancel_Date__c FROM Contract LIMIT 1];
        System.assertEquals(false, afterTestContract.In_Cancellation__c);
        System.assertEquals(null, afterTestContract.Cancel_Date__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(false, currentSubscription.In_Termination__c);
            System.assertEquals(null, currentSubscription.Termination_Date__c);
        }
    }
    
    @isTest
    public static void test_cancellationContractInvalidEndDate(){
        
        Contract contract = [SELECT Id, EndDate FROM Contract LIMIT 1];
        contract.EndDate = Date.today();
        update contract;
        
        Test.startTest();
        ContractUtility.cancellationContract(contract.Id);
        Test.stopTest();
        
        Contract afterTestContract = [SELECT Id, In_Cancellation__c, Cancel_Date__c FROM Contract LIMIT 1];
        System.assertEquals(false, afterTestContract.In_Cancellation__c);
        System.assertEquals(null, afterTestContract.Cancel_Date__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(false, currentSubscription.In_Termination__c);
            System.assertEquals(null, currentSubscription.Termination_Date__c);
        }
    }
    
    @isTest
    public static void test_cancellationContractSamba(){
        
        Order order = [SELECT Id, SambaIsStartConfirmed__c, SambaIsBilled__c FROM Order LIMIT 1];
        order.SambaIsStartConfirmed__c = true;
        order.SambaIsBilled__c = true;
        update order;
        Contract contract = [SELECT Id, EndDate FROM Contract LIMIT 1];
        
        Test.startTest();
        ContractUtility.cancellationContract(contract.Id);
        Test.stopTest();
        
        Contract afterTestContract = [SELECT Id, In_Cancellation__c, Cancel_Date__c FROM Contract LIMIT 1];
        System.assertEquals(true, afterTestContract.In_Cancellation__c);
        System.assertEquals(Date.today(), afterTestContract.Cancel_Date__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(true, currentSubscription.In_Termination__c);
            System.assertEquals(Date.today(), currentSubscription.Termination_Date__c);
        }
        
        List<Case> sambaCase = [SELECT Id FROM Case];
        System.assertEquals(0, sambaCase.size());
    }
    
    @isTest
    public static void test_terminationContract(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        Test.startTest();
        ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today()), 'Contract replacement', false, true, false, false);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Termination__c, TerminateDate__c FROM Contract LIMIT 1];
        System.assertEquals(true, afterTestContract.In_Termination__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(true, currentSubscription.In_Termination__c);
        }
    }
    
    @isTest
    public static void test_terminationContractDraftInvoice(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_DRAFT,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        Test.startTest();
        ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today()), 'Contract replacement', false, false, false, false);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Termination__c, TerminateDate__c FROM Contract LIMIT 1];
        System.assertEquals(false, afterTestContract.In_Termination__c);
        System.assertEquals(null, afterTestContract.TerminateDate__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(false, currentSubscription.In_Termination__c);
            System.assertEquals(null, currentSubscription.Termination_Date__c);
            System.assertEquals(null, currentSubscription.SBQQ__TerminatedDate__c);
        }
    }
    
    @isTest
    public static void test_terminationContractNoInvoice(){
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today()), 'Contract replacement', false, false, false, false);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Termination__c, TerminateDate__c FROM Contract LIMIT 1];
        System.assertEquals(false, afterTestContract.In_Termination__c);
        System.assertEquals(null, afterTestContract.TerminateDate__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(false, currentSubscription.In_Termination__c);
            System.assertEquals(null, currentSubscription.Termination_Date__c);
            System.assertEquals(null, currentSubscription.SBQQ__TerminatedDate__c);
        }
    }
    
    // start ticket# 376
    @isTest
    public static void test_terminationContractNoInvoiceSamba(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id, SambaIsBilled__c FROM Contract LIMIT 1];
        
        contract.SambaIsBilled__c = true;
        contract.AccountId = account.Id;
        update contract;

        ContractUtility.ContractResponse response = new ContractUtility.ContractResponse();
        Test.startTest();
        response = ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today()), 'Contract replacement', true, true, false, false);
        Test.stopTest();
        
        System.assertEquals(true, response.valid);
    }
    // end ticket# 376
    
    @isTest
    public static void test_terminationSubscription(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        List<Id> subsToTerminate = new List<Id>();
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, SBQQ__Product__r.ProductCode FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c sub : subscriptions){
            if(sub.SBQQ__Product__r.ProductCode.equals('POREV001')){
                subsToTerminate.add(sub.Id);
            }
        }
        Test.startTest();
        ContractUtility.terminationSubscriptions(new List<Id>{contract.Id}, subsToTerminate, String.valueOf(Date.today()), 'Contract replacement', false, 1, true, false);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Termination__c, TerminateDate__c FROM Contract LIMIT 1];
        System.assertEquals(false, afterTestContract.In_Termination__c);
        
        List<SBQQ__Subscription__c> afterTestSubscriptions = [SELECT Id, In_Termination__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c WHERE SBQQ__Product__r.ProductCode = 'POREV001'];
        for(SBQQ__Subscription__c currentSubscription : afterTestSubscriptions){
            System.assertEquals(true, currentSubscription.In_Termination__c);
        }

    }
    
    @isTest
    public static void test_terminationSubscriptionNoInvoiceSamba(){
        
        Contract contract = [SELECT Id, SambaIsBilled__c FROM Contract LIMIT 1];
        contract.SambaIsBilled__c = true;
        update contract;
        
        List<Id> subsToTerminate = new List<Id>();
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, SBQQ__Product__r.ProductCode FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c sub : subscriptions){
            if(sub.SBQQ__Product__r.ProductCode.equals('POREV001')){
                subsToTerminate.add(sub.Id);
            }
        }
        
        Test.startTest();
		ContractUtility.terminationSubscriptions(new List<Id>{contract.Id}, subsToTerminate, String.valueOf(Date.today()), 'Contract replacement', false, 1, true, false);
        Test.stopTest();
    }
    
    @isTest 
    public static void test_getContractAndSubscriptionAndReasons(){
        
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        SBQQ__Subscription__c subscription = [SELECT Id FROM SBQQ__Subscription__c LIMIT 1];
        
        Test.startTest();
        ContractUtility.getContractAndSubscriptionAndReasons(contract.Id, true);        
        ContractUtility.getContractAndSubscriptionAndReasons(contract.Id, false);
        ContractUtility.getContractAndSubscriptionAndReasons(subscription.Id, false);
        Test.stopTest();
    }
    
	@isTest
    public static void test_getSubscriptionByContract() {
        
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        
        Test.startTest();
        ContractUtility.SubscriptionsResponse subscriptionResponse = ContractUtility.getSubscriptionByContract(contract.id);
        System.assert(subscriptionResponse.valid);
        Test.stopTest();
    }
    
    @isTest
    public static void test_getQuoteProducts(){
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Type__c = :ConstantsUtil.QUOTE_TYPE_AMENDMENT LIMIT 1];
        
        Test.startTest();
        ContractUtility.GetProductsReplacementResponse getProductsReplacementResponse = ContractUtility.getQuoteProducts(quote.id);
        Test.stopTest();
    }
    
    @isTest
    public static void test_replaceProducts() {
        
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Type__c != :ConstantsUtil.QUOTE_TYPE_AMENDMENT LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__Product__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.SBQQ__Type__c != :ConstantsUtil.QUOTE_TYPE_AMENDMENT LIMIT 1];

        List<String> optionsList = new List<String>();
        for(SBQQ__QuoteLine__c ql: quoteLines){
            optionsList.add(ql.Id + '-' + ql.SBQQ__Product__c + '-' +'false');
        }

        Test.startTest();
        ContractUtility.replaceProducts(quote.Id, optionsList);
        Test.stopTest();
    }
    
    @isTest
    public static void test_otherMethods(){
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        Test.startTest();
        ContractUtility.hasUserGrant('Test');
        ContractUtility.getProfileName();
        ContractUtility.isRenewableContract(contract.Id);
        SRV_Contract.getEvergreenSubTermByContractId(new Set<Id>{contract.Id});
        SRV_Contract.terminateContractsForInvoices(new Set<Id>{invoice.Id});
        Test.stopTest();
    }
    
    // start ticket# 376
    @isTest 
    public static void test_getContractCancellationInformation(){
        
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        
        Test.startTest();
        ContractUtility.getContractCancellationInformation(contract.Id);  
        Test.stopTest();
    }
    
    @isTest
    public static void test_cancellationContractAction(){
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Test.startTest();
        ContractUtility.cancellationContractAction(contract.Id, 'Field Sales Error', true, true);
        Test.stopTest();
        Contract afterTestContract = [SELECT Id, In_Cancellation__c, Cancel_Date__c FROM Contract LIMIT 1];
        System.assertEquals(true, afterTestContract.In_Cancellation__c);
        System.assertEquals(Date.today(), afterTestContract.Cancel_Date__c);
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(true, currentSubscription.In_Termination__c);
            System.assertEquals(Date.today(), currentSubscription.Termination_Date__c);
        }
    }
    
    @isTest
    public static void test_terminationContractNotPermittedTelesalesUser(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id, SambaIsBilled__c FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile prof = [SELECT Id FROM Profile WHERE Name=:Label.TelesalesProfileName];
        User usr = new User(
            Alias = 'test-al', 
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', 
            LastName='Testing', 
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', 
            ProfileId = prof.Id,
            TimeZoneSidKey='America/Los_Angeles',
            UserName=uniqueUserName
        );
 
        ContractUtility.ContractResponse response = new ContractUtility.ContractResponse();
        System.runAs(usr) {
            Test.startTest();
            response = ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today()), 'Contract replacement', true, true, false, false);
            Test.stopTest();
        }
        
        System.assertEquals(false, response.valid);
    }

    
    // end ticket# 376

    /*@isTest
    public static void test_terminationContractGenerateFinalInvoice(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id, EndDate FROM Contract LIMIT 1];
        contract.EndDate = Date.today().addYears(1);
        update contract;
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        
        Test.startTest();
        List<SBQQ__Subscription__c> subsList = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c subsItem : [SELECT Id, In_Termination__c, Generate_Final_Invoice__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c]) {
            subsItem.Next_Invoice_Date__c = Date.today().addMonths(2);
            subsList.add(subsItem);
        }
        update subsList;
        ContractUtility.terminationContract(contract.Id, String.valueOf(Date.today().addDays(1)), 'Contract replacement', false, true, false, false);
        Test.stopTest(); 
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, In_Termination__c, Generate_Final_Invoice__c, Termination_Date__c, SBQQ__TerminatedDate__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            System.assertEquals(true, currentSubscription.Generate_Final_Invoice__c);
        }
    }*/
}