/*
CHANGELOG:
            #1 - SF2-218, Wave C, Sprint 6, Logging - Error Handling - 07-24-2019 gcasola
            #2 - SF2-772 - gcasola - Address Validation Review
*/
public without sharing class BillingProfileController {
    
    @AuraEnabled
    public static Map<String,List<sObject>> getQuoteAndSubscription(Id billingProfileId){
        
        Set<Id> bpIds = new Set<Id>();
        bpIds.add(billingProfileId);
        
        Map<String,List<sObject>> returnedObjects = new Map<String,List<sObject>>();
        
        returnedObjects.put('Quote',new List<sObject>());
        returnedObjects.put('Contract', new List<sObject>());
        
		Map<Id,SBQQ__Quote__c> quoteToDisplay = new Map<Id,SBQQ__Quote__c>();
        quoteToDisplay = SEL_SBQQQuote.getQuoteByBPId(bpIds);
        
        Map<Id,Contract> subToDisplay = new Map<Id,Contract>();
        subToDisplay = SEL_Contract.getContractByBillingProfile(bpIds);
        
        if(!quoteToDisplay.isEmpty()){
            
            returnedObjects.get('Quote').addAll(quoteToDisplay.values());
            
        }
        
        if(!subToDisplay.isEmpty()){
            
            returnedObjects.get('Contract').addAll(subToDisplay.values());
            
        }
        
        System.debug('returned values '+returnedObjects);
        
        return returnedObjects;
        
    }
    
    @AuraEnabled        
    public static Map<String, object> getDefaultBPValues(Id relatedId) {
        Map<String, object> ret;
        Schema.SObjectType ty = relatedId.getSobjectType();
        string objName = ty.getDescribe().getName();
        List<Account> accounts = new List<Account>();
        List<Billing_Profile__c> billingProfiles = new List<Billing_Profile__c>();

        System.debug('[BillingProfileController.getDefaultBPValues] objName: ' + objName);
        System.debug('[BillingProfileController.getDefaultBPValues] relatedId: ' + relatedId);
        switch on (objName) {
            when 'Billing_Profile__c'
            {
                billingProfiles = [
                    SELECT 	Id, Phone__c, Mail_address__c, P_O_Box_Zip_Postal_Code__c, P_O_Box_City__c ,
                     P_O_Box__c, Name, Billing_Postal_Code__c,Billing_Country__c,Billing_City__c,
                     Billing_State__c,Billing_Street__c,Is_Default__c ,Process_Mode__c, Grouping_Mode__c ,
                     Billing_Language__c , Billing_Name__c, Channels__c, Billing_Contact__c, Customer__c
                    FROM Billing_Profile__c 
                    WHERE Id = :relatedId
                ];

                List<Billing_Profile__c> bpdefault = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :billingProfiles[0].Customer__c AND Is_Default__c = true];
				System.debug('[BillingProfileController.getDefaultBPValues] billingProfiles[0].Name: ' + billingProfiles[0].Name);
                ret = getValuesMap(billingProfiles[0]);
                if(bpdefault.size() > 0)
                ret.put('IdBPDefault', (bpdefault[0].Id!=null)?bpdefault[0].Id:'');
                /*for(String s: ret.keySet())
                {
                    System.debug('[BillingProfileController.getDefaultBPValues] ret-> ' + s + ' : ' + String.valueOf(ret.get(s)));
                }*/
                System.debug('[BillingProfileController.getDefaultBPValues] BillingProfiles.size(): ' + billingProfiles.size());
                System.debug('[BillingProfileController.getDefaultBPValues] ret.size(): ' + ret.size());
            }
            when 'Account' {
				accounts = [Select Id, Name, BillingStreet, BillingPostalCode, BillingCountry, 
                                  BillingCity, POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, 
                                  (Select Id, Name, Email, Phone FROM Contacts LIMIT 1), (Select Id, Is_Default__c FROM Billing_Profiles__r ORDER BY Is_Default__c DESC) 
                                  FROM Account where Id =:relatedId LIMIT 1];                
            }
            when 'SBQQ__Quote__c' {
				accounts = [Select Id, Name, BillingStreet, BillingPostalCode, BillingCountry, 
                                  BillingCity, POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, 
                                  (Select Id, Name, Email, Phone FROM Contacts LIMIT 1), (Select Id, Is_Default__c FROM Billing_Profiles__r ORDER BY Is_Default__c DESC) 
                                  FROM Account where Id in (SELECT SBQQ__Account__c FROM SBQQ__Quote__c where Id=:relatedId) LIMIT 1];
            }
            when else {
                return null;
            }
        }

        System.debug('[BillingProfileController.getDefaultBPValues]: ' + accounts.size());

        if(accounts.size() == 0 && billingProfiles.size() == 0) {
            System.debug('[BillingProfileController.getDefaultBPValues] return null');
			return null;    
        }else if( accounts.size() > 0) {
            System.debug('[BillingProfileController.getDefaultBPValues] I\'m here');
            Account account = accounts[0];
            ret = getValuesMap(account);
            ret.put('Id', account.Id);
        }
        

        System.debug('[BillingProfileController.getDefaultBPValues] before return BillingProfiles.size(): ' + billingProfiles.size());
        System.debug('[BillingProfileController.getDefaultBPValues] before return accounts.size(): ' + accounts.size());
        System.debug('[BillingProfileController.getDefaultBPValues] before return ret.size(): ' + ret.size());
        return ret;
    }
    
    @AuraEnabled
    public static Map<string, object> getAccountBPValues(string accountId) {
        List<Account> accounts = [Select Id, Name, BillingStreet, BillingPostalCode, BillingCountry, 
                                  BillingCity, POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, 
                                  (Select Id, Name, Email, Phone FROM Contacts LIMIT 1), (Select Is_Default__c FROM Billing_Profiles__r Order By Is_Default__c DESC) 
                                  FROM Account where Id = :accountId LIMIT 1];
        if(accounts.size() == 0) {
			return null;
        }
        return getValuesMap(accounts[0]);        
    }
    
    @AuraEnabled
	public static Map<String, object> getContactBPValues(string accountId, string contactId) {
		List<Contact> contacts = [Select Id, Name, Email, Phone FROM Contact where AccountId = :accountId and Id = :contactId LIMIT 1];
        if(contacts.size() == 0) {
			return null;
        }        
        Contact c = contacts[0];
        Map<String, object> ret = new Map<String, Object>();
            ret.put('Billing_Name__c', c.Name);
            ret.put('Mail_address__c', c.Email);
            ret.put('Phone__c', c.Phone);    	
        return ret;
    }

    @AuraEnabled
    public static boolean checkDefault(Id bpId, Id relatedId) {
        List<Account> accts = [SELECT Id, (Select Id, Is_Default__c from Billing_Profiles__r) FROM Account WHERE Id in (SELECT Customer__c FROM Billing_Profile__c where Id=:bpId)];
		if(accts.size() != 1) return false; // should never happen
        System.debug('[BillingProfileController.checkDefault] bpId: ' + bpId);
        System.debug('[BillingProfileController.checkDefault] relatedId: ' + relatedId);
        // set quote billing profile
        if(relatedId != null) {
        	Schema.SObjectType ty = relatedId.getSobjectType();
        	string objName = ty.getDescribe().getName();
            System.debug('[BillingProfileController.checkDefault] pointA');
            if(objName == 'SBQQ__Quote__c') {
                System.debug('[BillingProfileController.checkDefault] pointB');
                SBQQ__Quote__c quote = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c WHERE Id = :relatedId ];
                System.debug('Quote:'+quote);
                quote.Billing_Profile__c = bpId;
                //update quote;
                System.debug('Quote:'+quote);
                Database.SaveResult dbSaveResult = Database.update(quote);
                if(dbSaveResult.isSuccess())
                {
                    System.debug('[BillingProfileController.checkDefault] postsuccess - bpId: ' + bpId);
        			System.debug('[BillingProfileController.checkDefault] postsuccess - relatedId: ' + relatedId);
                    System.debug('[BillingProfileController.checkDefault] dbSaveResult.isSuccess() - Success');
                	System.debug('returned quote after update'+[SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c WHERE Id=: relatedId]);
                }
            	//update new SBQQ__Quote__c(Id=relatedId, Billing_Profile__c=bpId);
                System.debug('[BillingProfileController.checkDefault] pointC');
            }
        }
        
        return true;
        
        /*
        boolean isCurrentDefault = false;
        List<Id> othersDefault = new List<Id>();
        for(Billing_Profile__c bp : accts[0].Billing_Profiles__r) {
            if(bp.Is_Default__c == true) {
                if(bp.Id == bpId)
                    isCurrentDefault = true;
                else
                    othersDefault.add(bp.Id);
            }
        }
        if(isCurrentDefault) {
            if(othersDefault.size() == 0) return false;
            List<Billing_Profile__c> updates = new List<Billing_Profile__c> ();
            for(Id id : othersDefault) {
                updates.add(new Billing_Profile__c(Id=id, Is_Default__c = false));
            }
            update updates;
            return false;
        } else {
            if(othersDefault.size() > 0) return false;
            update new Billing_Profile__c(Id=bpId, Is_Default__c = true);
            return true;
        }
		*/
    }

    private static Map<String, object> getValuesMap(Billing_Profile__c bp) {
        Map<String, object> ret = new Map<String, Object>();
        ret.put('Id', bp.Customer__c);
		ret.put('Name', (bp.Name==null)?'':bp.Name);
		ret.put('Is_Default__c', bp.Is_Default__c);
        ret.put('IdBPDefault', bp.Is_Default__c);
		ret.put('Billing_Language__c', (bp.Billing_Language__c==null)?'':bp.Billing_Language__c);
        ret.put('Billing_Contact__c', (bp.Billing_Contact__c==null)?'':bp.Billing_Contact__c);
        ret.put('Billing_Name__c', (bp.Billing_Name__c==null)?'':bp.Billing_Name__c);
        ret.put('Mail_address__c', (bp.Mail_address__c==null)?'':bp.Mail_address__c);
        ret.put('Phone__c', (bp.Phone__c==null)?'':bp.Phone__c);
        ret.put('Billing_Street__c', (bp.Billing_Street__c==null)?'':bp.Billing_Street__c);
        ret.put('Billing_Postal_Code__c', (bp.Billing_Postal_Code__c==null)?'':bp.Billing_Postal_Code__c);
        ret.put('Billing_Country__c', (bp.Billing_Country__c==null)?'':bp.Billing_Country__c);
        ret.put('Billing_City__c', (bp.Billing_City__c==null)?'':bp.Billing_City__c);
        ret.put('P_O_Box__c', (bp.P_O_Box__c==null)?'':bp.P_O_Box__c);
        ret.put('P_O_Box_City__c', (bp.P_O_Box_City__c==null)?'':bp.P_O_Box_City__c);
        ret.put('P_O_Box_Zip_Postal_Code__c', (bp.P_O_Box_Zip_Postal_Code__c==null)?'':bp.P_O_Box_Zip_Postal_Code__c);
        System.debug('[BillingProfileController.getValuesMap(Billing_Profile__c bp)] bp.Name: ' + ret.get('Name'));
        return ret;
    }
    
    private static Map<String, object> getValuesMap(Account a) {
        string bpSuff = '';
        boolean isDefault = true;
        Map<String, object> ret = new Map<String, Object>();
        if(a.Billing_Profiles__r.size() > 0) {
            bpSuff = ' N.' + (a.Billing_Profiles__r.size() + 1);
            if(a.Billing_Profiles__r[0].Is_Default__c == true) {
                isDefault = false;
            }
            //START GK5
            ret.put('IdBPDefault', a.Billing_Profiles__r[0].Id);
            //END GK5
        }
		ret.put('Name', a.Name.left(58) + ' Billing Profile' + bpSuff); // max 80 chars for name
		ret.put('Is_Default__c', isDefault);
		ret.put('Billing_Language__c', a.PreferredLanguage__c);
        if(a.Contacts != null && a.Contacts.size() >0) {
            ret.put('Billing_Contact__c', a.Contacts[0].Id);
            ret.put('Billing_Name__c', a.Contacts[0].Name);
            ret.put('Mail_address__c', a.Contacts[0].Email);
            ret.put('Phone__c', a.Contacts[0].Phone);
        }
        ret.put('Billing_Street__c', a.BillingStreet);
        ret.put('Billing_Postal_Code__c', a.BillingPostalCode);
        ret.put('Billing_Country__c', a.BillingCountry);
        ret.put('Billing_City__c', a.BillingCity);
        ret.put('P_O_Box__c', a.POBox__c);
        ret.put('P_O_Box_City__c', a.P_O_Box_City__c);
        ret.put('P_O_Box_Zip_Postal_Code__c', a.P_O_Box_Zip_Postal_Code__c);
        ret.put('AccountName', a.Name);
        return ret;
    }

    //A. L. Marotta 27-05-19 Start
    //US #93, Wave 1, Sprint 2 - returns boolean result of the billing profile's legal addres validation
    @AuraEnabled
    public static String checkBillingProfileLegalAddress(String recordId, String companyId, String street, String postalCode, String city, Boolean editMode){
        SRV_IntegrationOutbound.AddressValidationResult avResult = new SRV_IntegrationOutbound.AddressValidationResult();
        if(!SRV_IntegrationOutbound.getSkipAddressValidationFlag(companyId)){
            String result = '';
            System.debug('************* ID COMPANY: '+companyId);
            Account acc = SEL_Account.getAccountById(companyId);
            ValidateAddressRequestWrapper avRequest = new ValidateAddressRequestWrapper();
            avRequest.company = acc.Name;
            avRequest.street = street;
            avRequest.zip = postalCode;
            //avRequest.uid = uid;
            //avRequest.email = email;
            //avRequest.phone = phone;
            avRequest.location = city;
            
            System.debug('>>>>>avRequest:' + JSON.serialize(avRequest));
            
            //SRV_IntegrationOutbound.AddressValidationResult avResult = SRV_IntegrationOutbound.syncValidateAddress(recordId, avRequest);
            if(editMode && recordId != NULL && String.isNotBlank(recordId))
                avResult = SRV_IntegrationOutbound.syncValidateAddress(Id.valueOf(recordId), avRequest);
            else
                avResult = SRV_IntegrationOutbound.syncValidateAddress(avRequest);
            
            System.debug('>>>avResult: ' + JSON.serialize(avResult));
            
            if(avResult != NULL)
            {
                result = avResult.result;
                if(ConstantsUtil.ADDRESS_VALIDATION_KO.equalsIgnoreCase(avResult.result) && String.isNotBlank(avResult.error.error_code)){
                    result = avResult.error.error_code;
                }
            }
            
            System.debug('>>>result: ' + result);
        }else
            avResult.result = ConstantsUtil.ADDRESS_VALIDATION_OK;
        
        System.debug('>>>avResult: ' + avResult);
        
        return JSON.serialize(avResult);
    }
    //A. L. Marotta 27-05-19 End
    
    //Vincenzo Laudato US 2019-05-29 #92 - US #93 Sprint 2, Wave 1 *** START
    @AuraEnabled
    public static Boolean checkIntegrationStatus(){
        Integration_Config__mdt integrationConfig = SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
        if(integrationConfig != null)
            return integrationConfig.Active__c;
        else return null;
    }
    
    //SF2-900 - Billing Profile - Edit on Validation Status - gcasola - 20200317
    @AuraEnabled
    public static FieldsAttributes getFieldsProperties(Id recordId, List<String> fieldAPINames)
    {
        FieldsAttributes fieldsAttributes = new FieldsAttributes();
        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(currentUser != NULL)
        {
            if(!ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
            {
                fieldsAttributes.fieldsAttributesMap.put('AddressValidated__c', new InputFieldAttributes(false, true, true));
            }else
            {
                fieldsAttributes.fieldsAttributesMap.put('AddressValidated__c', new InputFieldAttributes(false, false, false));
            }
        }else
        {
            fieldsAttributes.fieldsAttributesMap.put('AddressValidated__c', new InputFieldAttributes(false, true, true));
        }
        
        return fieldsAttributes;
    }
    
    public class FieldsAttributes{
        @AuraEnabled public Map<String, InputFieldAttributes> fieldsAttributesMap;
        
        public FieldsAttributes()
        {
            fieldsAttributesMap = new Map<String, InputFieldAttributes>();
        }
    }
    
    public class InputFieldAttributes{
        @AuraEnabled public Boolean required;
        @AuraEnabled public Boolean disabled;
        @AuraEnabled public Boolean readOnly;
        
        public InputFieldAttributes()
        {
            this.required = false;
            this.disabled = false;
            this.readOnly = false;
        }
        
        InputFieldAttributes(Boolean required, Boolean disabled, Boolean readOnly)
        {
            this.required = required;
            this.disabled = disabled;
            this.readOnly = readOnly;
        }
    }
}