public class accountReviewManagement {
    
    @InvocableMethod(label='accountReviewManagement' description='Creates cases for DAM to review accounts created by DMC and Telesales')
    public static void createCaseReview(List<Id> recordIds){
        
        Id internalSupportId = [SELECT Id, Name FROM RecordType WHERE Name = :constantsUtil.CASE_RTYPE_INTERNAL_SUP][0].id;
        Id damQueueId = [SELECT Id, Name FROM Group WHERE Name = :ConstantsUtil.DATA_MANAGEMENT_QUEUE][0].id;
        Set<Id> accountIds = new Set<Id>();
        Map<Id, List<Contact>> contactsMap = new Map<Id, List<Contact>>();
        List<Case> casesToInsert = new List<Case>();
        List<Account> accReviewList = [SELECT Id, Name, CreatedById, Manual_validation_required__c, PreferredLanguage__c
                                       FROM Account
                                       WHERE Manual_validation_required__c = true
                                       AND Id IN :recordIds];
                
        for(Account acc : accReviewList){
            accountIds.add(acc.Id);
        }
        
        List<Contact> contactList = [SELECT Id, Primary__c, AccountId FROM Contact WHERE Primary__c = True AND AccountId IN :accountIds];
        
        for(Contact cc : contactList){
            if(!contactsMap.containsKey(cc.AccountId)){
                contactsMap.put(cc.AccountId, new List<Contact>{cc});
            }else{
                contactsMap.get(cc.AccountId).add(cc);
            	}
        }
        
        if(accReviewList.size() > 0){
            for(Account acc : accReviewList){
                Case reviewCase = new Case();
                reviewCase.RecordTypeId = internalSupportId;
                reviewCase.Subject = ConstantsUtil.CASE_SUBJ_VALIDATE_NEW_ACCOUNT;
                reviewCase.Topic__c = 'New Account';
                reviewCase.Language__c = acc.PreferredLanguage__c;
                reviewCase.Employee__c = acc.CreatedById;
                reviewCase.AccountId = acc.Id;
                reviewCase.Description = label.Validate_new_Account_Description;
                reviewCase.Origin = ConstantsUtil.CASE_ORIGIN_MANUAL2;
                reviewCase.Status = ConstantsUtil.CASE_NEW_STATUS;
                reviewCase.OwnerId = damQueueId;
                reviewCase.ContactId = contactsMap.containsKey(acc.Id)? contactsMap.get(acc.Id)[0].Id : null;
                casesToInsert.add(reviewCase);
                system.debug('casesToInsert ' +casesToInsert);
            }
        }
        
        if(casesToInsert.size() > 0){
        insert casesToInsert;
        }   
    }
}