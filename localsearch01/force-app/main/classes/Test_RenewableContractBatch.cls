@isTest
public class Test_RenewableContractBatch {
    
	@testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.SBQQ__Evergreen__c = true;
        contract.SBQQ__Opportunity__c = opportunity.Id;
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        fatherSub.SBQQ__SubscriptionStartDate__c = Date.today().addYears(-1).addDays(-1);
        fatherSub.End_Date__c = Date.today();
        fatherSub.Next_Renewal_Date__c = Date.today().addDays(1);
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        for(SBQQ__Subscription__c sub : childrenSubs){
            sub.SBQQ__RequiredByProduct__c = productCodeToProduct.get('SLT001').Id;
            sub.SBQQ__SubscriptionStartDate__c = Date.today().addYears(-1).addDays(-1);
            sub.End_Date__c = Date.today();
            sub.Next_Renewal_Date__c = Date.today().addDays(1);
        }
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        		        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        
    }
    
    @isTest
    public static void test_renewableContractBatch(){
        List<Contract> contracts = [SELECT Id, AccountId, SBQQ__OpportunityPricebookId__c FROM Contract WHERE Id IN 
                                    (SELECT SBQQ__Contract__c FROM SBQQ__Subscription__c WHERE In_Termination__c = FALSE 
                                                                                         AND Next_Renewal_Date__c <= TOMORROW 
                                                                                         AND SBQQ__Contract__r.SBQQ__Evergreen__c = TRUE 
                                                                                         AND Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE)];
        Test.startTest();
        RenewableContractBatch b = new RenewableContractBatch(); 
       	b.execute(null, contracts);
        Test.stopTest();
    }
    
}