@isTest
public class SetAccountStatusBatch_Test {
    
    @testSetup
    public static void test_setupData(){ 
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Quote Management,Send Owner Notification';
        insert byPassFlow;
        AccountTriggerHandler.disableTrigger = true;
        List<Account> account = Test_DataFactory.generateAccounts('Test Account Name', 1, false);
        insert account;
        Account accountMerge = new Account( Name= 'MergeMerge',
                                            BillingCity = 'Zürich',
            								BillingCountry = 'Switzerland',
            								BillingStreet = '62 Förrlibuckstrasse',
            								BillingPostalCode = '8001',
                                            UID__c = 'CHX-1234567',
            								Phone = '1234567810',
            								Email__c = 'testmerge@test.com',
            								PreferredLanguage__c  = ConstantsUtil.TST_PREF_LANGUAGE,
            								AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_VALIDATED_ADDRESSONLY,
            								LegalEntity__c = ConstantsUtil.ACCOUNT_LEGAL_ENTITY_FOUNDATION);
        insert accountMerge;
        account[0].Active_DMC_Contracts__c = null;
        update account;
        
        Pricebook2 dmcPricebook = new Pricebook2();
        dmcPricebook.externalId__c = 'PB3';
        dmcPricebook.Name = 'PB3';
        insert dmcPricebook;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account[0].Id);
        opportunity.StageName= ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        opportunity.Pricebook2Id = dmcPricebook.id;
        insert opportunity;
        
        Opportunity opportunity2 = Test_DataFactory.generateOpportunity('Test Opportunity2 Name', account[0].Id);
        opportunity2.StageName= ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        opportunity2.Pricebook2Id = dmcPricebook.id;
        insert opportunity2;
        
        Opportunity opportunitymigra = Test_DataFactory.generateOpportunity('Migrated Opportunity Name', account[0].Id);
        opportunitymigra.StageName= ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        opportunitymigra.ExternalOpty__c ='12345';
        opportunitymigra.Pricebook2Id = dmcPricebook.id;
        insert opportunitymigra;
        
        Opportunity opportunitymigra2 = Test_DataFactory.generateOpportunity('Migrated 2 Opportunity Name', account[0].Id);
        opportunitymigra2.StageName= ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        opportunitymigra2.ExternalOpty__c ='12347';
        opportunitymigra2.Pricebook2Id = dmcPricebook.id;
        insert opportunitymigra2;
        AccountTriggerHandler.disableTrigger = false;

    }
    @isTest 
    public static void shouldBeNew() {
        List<String> staticValues = new list<String>();
        for (Account acc : [select id, cluster_id__c from Account where id not in (Select accountid from Opportunity)]){
            staticValues.add(acc.cluster_id__c);
        }
        
        SetAccountStatusBatch a = new SetAccountStatusBatch(staticValues);
        a.execute(null);
    }
    @isTest 
    public static void shouldBeProspect() {   
        List<String> staticValues = new list<String>();
        for (Account acc : [select id, cluster_id__c from Account where id in (select accountid from Opportunity)]){
            staticValues.add(acc.cluster_id__c);
        }
        SetAccountStatusBatch a = new SetAccountStatusBatch(staticValues);
        a.execute(null);
    }
    
     @isTest 
    public static void shouldBeMerged() {   
        AccountTriggerHandler.disableTrigger = true;
        Account tomerge = [select id from account where Email__c = 'testmerge@test.com' limit 1];
        List<Account> acc = [select id, cluster_id__c from Account where id in (select accountid from Opportunity)];
        merge  tomerge acc[0];
        List<SBQQ__QUOTE__c> quotes = [select id,SBQQ__Type__c,SBQQ__Opportunity2__r.ExternalOpty__c,SBQQ__Opportunity2__r.Pricebook2.ExternalId__c,SignatureDate__c from SBQQ__QUOTE__c];
        Integer days = 1;
        for(SBQQ__QUOTE__c quote: quotes){
            if(quote.SBQQ__Opportunity2__r.ExternalOpty__c !=null) {
                quote.ContractSAMBA_Key__c = quote.SBQQ__Opportunity2__r.ExternalOpty__c;
                quote.Contract_Signed_Date__c = date.today().addDays(-days);
            }
            else 
            	quote.SignatureDate__c = date.today().addDays(-days);
            days ++;
        }
        update quotes;
        
        QuoteDocumentTriggerHandler.disableTrigger = true;
        document doc = new document();
        doc.name = 'testname';
        doc.Body = Blob.valueOf('testbody');
        doc.FolderId = UserInfo.getUserId();
        insert doc;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'Telesales Template 2020 v1.0 - IT';
        insert template;
        
        SBQQ__QuoteDocument__c document = test_dataFactory.generateQuoteDocument(quotes[0].id);
        document.SBQQ__QuoteTemplate__c = template.id;
        document.SBQQ__DocumentId__c = doc.id;
        document.SignatureDate__c = date.today().addDays(-days);
        insert document;
		QuoteDocumentTriggerHandler.disableTrigger = false;
        
        Order order = new Order ();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.AccountId = tomerge.id;
        order.SBQQ__Quote__c = quotes[0].id;
		Pricebook2 dmcPricebook = [select id from Pricebook2 where externalId__c = 'PB3' limit 1];
        order.Pricebook2Id = dmcPricebook.id;
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
        order.Type = ConstantsUtil.ORDER_TYPE_NEW;
        insert order;
        
        Test.startTest();
        NewCustomerMergeAccount_Batch a = new NewCustomerMergeAccount_Batch();
        a.execute(null);
        AccountTriggerHandler.disableTrigger = false;
        Test.stopTest();
    }
    
    @isTest 
    public static void shouldBeMerged_v2() {   
        AccountTriggerHandler.disableTrigger = true;
        Account tomerge = [select id from account where Email__c = 'testmerge@test.com' limit 1];
        List<Account> acc = [select id, cluster_id__c from Account where id in (select accountid from Opportunity)];
        merge  tomerge acc[0];
        List<SBQQ__QUOTE__c> quotes = [select id,SBQQ__Type__c,SBQQ__Opportunity2__r.ExternalOpty__c,SBQQ__Opportunity2__r.Pricebook2.ExternalId__c,SignatureDate__c from SBQQ__QUOTE__c];
        Integer days = 1;
        for(SBQQ__QUOTE__c quote: quotes){
            if(quote.SBQQ__Opportunity2__r.ExternalOpty__c !=null) {
                quote.ContractSAMBA_Key__c = quote.SBQQ__Opportunity2__r.ExternalOpty__c;
                quote.Contract_Signed_Date__c = date.today().addDays(-days);
            }
            else 
            	quote.SignatureDate__c = date.today().addDays(-days);
            days++;
        }
        update quotes;
        
        QuoteDocumentTriggerHandler.disableTrigger = true;
        document doc = new document();
        doc.name = 'testname';
        doc.Body = Blob.valueOf('testbody');
        doc.FolderId = UserInfo.getUserId();
        insert doc;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'Telesales Template 2020 v1.0 - IT';
        insert template;
        
        SBQQ__QuoteDocument__c document = test_dataFactory.generateQuoteDocument(quotes[0].id);
        document.SBQQ__QuoteTemplate__c = template.id;
        document.SBQQ__DocumentId__c = doc.id;
        document.SignatureDate__c = date.today().addDays(-days);
        insert document;
		QuoteDocumentTriggerHandler.disableTrigger = false;
        
        Order order = new Order ();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.AccountId = tomerge.id;
        order.SBQQ__Quote__c = quotes[0].id;
		Pricebook2 dmcPricebook = [select id from Pricebook2 where externalId__c = 'PB3' limit 1];
        order.Pricebook2Id = dmcPricebook.id;
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
        order.Type = ConstantsUtil.ORDER_TYPE_NEW;
        insert order;
        
        ContractTriggerHandler.disableTrigger = true;             
        Contract c = test_dataFactory.generateContractFromOrder(order,false);
        insert c;
        ContractTriggerHandler.disableTrigger = false;
        Test.startTest();
        c.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
        c.Terminatedate__c = date.today().addMonths(-13);
        update c;
        update tomerge;
        
        NewCustomerMergeAccount_Batch a = new NewCustomerMergeAccount_Batch();
        a.execute(null);
        AccountTriggerHandler.disableTrigger = false;
        Test.stopTest();
    }
    
    
    @isTest 
    public static void shouldBeProspectwithTerminatedContract() {   
        List<String> staticValues = new list<String>();
        Order order = new Order ();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        List<Account> acc =  [select id,cluster_id__c,Last_Inactive_Date__c from Account where id in (Select accountid from opportunity)];
        order.AccountId = acc[0].id;
        staticValues.add(acc[0].cluster_id__c);
        Pricebook2 dmcPricebook = [select id from Pricebook2 where externalId__c = 'PB3' limit 1];
        order.Pricebook2Id = dmcPricebook.id;
        Contract contract  = test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        contract.status= constantsUtil.CONTRACT_STATUS_TERMINATED;
        contract.TerminateDate__c = Date.today();
        contract.AccountId= acc[0].id;
        update contract;
        for (Account account : [select id, cluster_id__c from Account where id in (Select accountid from Contract)]){
            staticValues.add(account.cluster_id__c);
        }
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', acc[0].Id);
        opportunity.Pricebook2Id = dmcPricebook.id;
        insert opportunity;
        SetAccountStatusBatch a = new SetAccountStatusBatch(staticValues);
        AccountStatusUpdateScript.getLastInactiveDateWhenNull(Date.today(),null,null);
        AccountStatusUpdateScript.getLastInactiveDateWhenNotNull(Date.today(),null,null,null);
        a.execute(null);
    }
    @isTest 
    public static void  shouldBeActive() {
        Order order = new Order ();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        SBQQ__QUOTE__c quote = [select id,SBQQ__Type__c, SBQQ__Opportunity2__r.Pricebook2.ExternalId__c from SBQQ__QUOTE__c limit 1];
        List<Account> acc =  [select id,cluster_id__c from Account where id in (select accountid from Opportunity)];
        order.AccountId = acc[0].id;
        order.SBQQ__Quote__c = quote.id;
		Pricebook2 dmcPricebook = [select id from Pricebook2 where externalId__c = 'PB3' limit 1];
        order.Pricebook2Id = dmcPricebook.id;
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
        order.Type = ConstantsUtil.ORDER_TYPE_NEW;
        insert order;
        Contract contract  = test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        ContractTriggerHandler.disableTrigger = true;
        contract.status = constantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        ContractTriggerHandler.disableTrigger = false;
        List<String> staticValues = new list<String>();
        for (Account account : [select id, cluster_id__c from Account]){
            staticValues.add(account.cluster_id__c);
        }
        acc[0].Activation_Date__c = null;
        acc[0].Active_Contracts__c = 0;
        acc[0].Active_DMC_Contracts__c = 0;
        update acc;
        SetAccountStatusBatch a = new SetAccountStatusBatch(staticValues);
        a.execute(null);
    }
    @isTest 
    public static void shouldBeInactive() {
        Order order = new Order ();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        List<Account> acc =  [select id,cluster_id__c,Last_Inactive_Date__c from Account where id in (Select accountid from opportunity)];
        order.AccountId = acc[0].id;
        Pricebook2 dmcPricebook = [select id from Pricebook2 where externalId__c = 'PB3' limit 1];
        order.Pricebook2Id = dmcPricebook.id;
        Contract contract  = test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        contract.status= constantsUtil.CONTRACT_STATUS_TERMINATED;
        contract.TerminateDate__c = Date.today();
        contract.AccountId= acc[0].id;
        update contract;
        List<String> staticValues = new list<String>();
        for (Account account : [select id, cluster_id__c from Account where id in (Select accountid from Contract)]){
            staticValues.add(account.cluster_id__c);
        }
        SetAccountStatusBatch a = new SetAccountStatusBatch(staticValues);
        a.execute(null);
        acc[0].Last_Inactive_Date__c = date.today() -1;
        
        update acc;
        a.execute(null);
    }
}