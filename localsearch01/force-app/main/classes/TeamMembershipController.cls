/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* After a request to sell by a DMC is approved by SMA, SMD or SDI, the DMC is added to the Account
* Team.
*
* The main method of this class is invoked by Account Change Owner Management Process Builder.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-07-28
* @systemLayer    Invocation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class TeamMembershipController{

    public class AccountParam{
        
        @InvocableVariable(required=true)
        public Id accountId;
        @InvocableVariable(required=true)
        public Id submitterId;
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * This method adds the DMC as a Team Member of the Account on which a request to sell was made 
    * through the button "Make a Request".
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    caseParams	List of instances of object "AccountParam" containing account id 
    * 								and user to add as team memeber id.
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @InvocableMethod(label='Add Team Member' description='Add the requester to Account Team')
    public static void addTeamMember(List<AccountParam> accountParams){
        
        List<AccountTeamMember> teamMembers = new List<AccountTeamMember>();
        
        for(AccountParam accountParam : accountParams){
            
            AccountTeamMember teamMember = new AccountTeamMember();
            teamMember.AccountAccessLevel = 'Edit';
            teamMember.AccountId = accountParam.accountId;
            teamMember.UserId = accountParam.submitterId;
            teamMember.OpportunityAccessLevel = 'None';
            teamMember.CaseAccessLevel = 'Read';
            teamMember.ContactAccessLevel = 'Read';
            teamMembers.add(teamMember);
            
        }
        
        if(!teamMembers.isEmpty()) insert teamMembers;
        
    }
}