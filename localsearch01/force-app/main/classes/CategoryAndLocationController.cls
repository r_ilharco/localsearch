/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-15-2019
 * Sprint      : 1
 * Work item   : US_19
 * Testclass   : Test_CategoryAndLocationController
 * Package     : 
 * Description : Apex Controller for the Lightning Component "CategoriesAndLocationsRelatedList"
 * Changelog   : 
 */

public with sharing class CategoryAndLocationController {

    /**
	 * Gennaro Casola | Sprint 1 | Date 05-15-2019
	 * Description
	 *
	 * @param obj | Id  | Place__c's ID or PrivatePlace__c's ID depending on the record's page where the component is located.
	 * @param sObjName | String | the sObject name
     * @param locale | String | User's Language Locale (ex. "en_US", "it", "de", "fr")
	 *
	 * @return Map<String, List<String>> | the key values are 'C' for Categories and 'L' for Locations and the list of strings cointains the respective values in user's language locale.
	 */
    @AuraEnabled
    public static Map<String, List<String>> getCategoriesAndLocationByPlace(Id obj, String sObjName, String locale)
    {
        Map<String,List<String>> categoriesAndLocations = new Map<String,List<String>>();
        categoriesAndLocations.put('C', new List<String>());
        categoriesAndLocations.put('L', new List<String>());
        List<Category_Location__c> categories = new List<Category_Location__c>();
        List<Category_Location__c> locations = new List<Category_Location__c>();
        
        //In this point of source code It is retrieved the metadata configuration for the correct mapping of Field API Names based on User's language locale.
        List<Category_Location_Mapping__mdt> cat_loc_config = [SELECT FieldApiName__c, RTDeveloperName__c, Language__c FROM Category_Location_Mapping__mdt WHERE Language__c = :locale];
        String categoryAPIName = '';
        String locationAPIName = '';

        for(Category_Location_Mapping__mdt rec : cat_loc_config)
        {
            if(ConstantsUtil.CATEGORY_RT_DEVELOPERNAME.equals(rec.RTDeveloperName__c))
            {
                categoryAPIName = rec.FieldApiName__c;
            }else if(ConstantsUtil.LOCATION_RT_DEVELOPERNAME.equals(rec.RTDeveloperName__c))
            {
                locationAPIName = rec.FieldApiName__c;
            }
        }
        
        if(sObjName.equals('Place__c'))
        {
            categories = SEL_CategoryAndLocation.getCategoriesByPlace(obj);
            locations = SEL_CategoryAndLocation.getLocationsByPlace(obj);
        }
        else if (sObjName.equals('PlacePrivate__c'))
        {
            categories = SEL_CategoryAndLocation.getCategoriesByPrivatePlace(obj);
            locations = SEL_CategoryAndLocation.getLocationsByPrivatePlace(obj);
        }

        for(Category_Location__c category : categories)
        {
            categoriesAndLocations.get('C').add((String)category.getSobject('Category_Location__r').get(categoryAPIName));
        }

        for(Category_Location__c location: locations)
        {
            if(location.mainLocation__c)
            {
                List<String> temp = categoriesAndLocations.get('L').clone();
                categoriesAndLocations.get('L').clear();
                categoriesAndLocations.get('L').add((String)location.getSobject('Category_Location__r').get(locationAPIName));
                categoriesAndLocations.get('L').addAll(temp);
            }else
            {
            	categoriesAndLocations.get('L').add((String)location.getSobject('Category_Location__r').get(locationAPIName));
            }
        }

        
        return categoriesAndLocations;
    }
}