/*
 * * ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@IsTest public class Test_OnboardingProductsManagement {

static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Send Owner Notification';
        insert byPassTrigger;
}

    public static testmethod void OnboardingProductsManagementTest1() {
        insertBypassFlowNames();
        List<Product2> prList = new List<Product2>();
        List<SBQQ__Subscription__c> subList = new List<SBQQ__Subscription__c>();

        Account customerAcc = new Account(Name='customerAccount');
        customerAcc.Type = 'Customer';
        customerAcc.PreferredLanguage__c= 'German';
        customerAcc.Status__c = 'New';
        customerAcc.BillingCity = 'Zürich';
        customerAcc.BillingCountry = 'Switzerland';
        customerAcc.BillingPostalCode = '8005';
        customerAcc.BillingStreet = 'Förrlibuckstrasse, 62';
        customerAcc.Phone = '+39999999';
        customerAcc.Email__c = 'oggi@ciao.com.net';
        insert customerAcc;

        Contact primaryCont = new Contact(LastName='primaryContact');
        primaryCont.MailingCity = 'Zürich';
        primaryCont.MailingCountry = 'Switzerland';
        primaryCont.MailingPostalCode = '8005';
        primaryCont.MailingStreet = 'Förrlibuckstrasse, 62';
        primaryCont.AccountId = customerAcc.Id;
        primaryCont.Primary__c = true;
        insert primaryCont;

        Product2 prod1 = new Product2();
        prod1.Name = 'myPresence';
        prod1.ProductCode = 'ONEPRESENCE_001';
        prod1.IsActive = true;
        prod1.SBQQ__ConfigurationType__c = 'Allowed';
        prod1.SBQQ__ConfigurationEvent__c = 'Always';
        prod1.SBQQ__QuantityEditable__c = true;
        prod1.ExternalKey_ProductCode__c = 'SLS001';
        prod1.SBQQ__NonDiscountable__c = false;
        prod1.SBQQ__SubscriptionType__c = 'Renewable'; 
        prod1.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        prod1.SBQQ__SubscriptionTerm__c = 12;
        prod1.Base_term_Renewal_term__c = '12';
        prList.add(prod1);

        Product2 prod2 = new Product2();
        prod2.Name = 'MyCashStarter';
        prod2.ProductCode = 'O_ONEP_AGENCYSERVICE_S_001';
        prod2.IsActive = true;
        prod2.SBQQ__ConfigurationType__c = 'Allowed';
        prod2.SBQQ__ConfigurationEvent__c = 'Always';
        prod2.SBQQ__QuantityEditable__c = true;
        prod2.ExternalKey_ProductCode__c = 'SLS002';
        prod2.SBQQ__NonDiscountable__c = false;
        prod2.SBQQ__SubscriptionType__c = 'Renewable'; 
        prod2.SBQQ__SubscriptionPricing__c = 'Fixed Price';
        prod2.SBQQ__SubscriptionTerm__c = 12;
        prod2.Base_term_Renewal_term__c = '12';
        prList.add(prod2);

        insert prList;

        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;

        User u = new user();
        u.LastName = 'mylastName';
        u.Email = 'testclass@test.class';
        u.Alias = 'tclass';
        u.Username = 'testclass@test.class';
        u.CommunityNickname = 'testclass@test.class';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u; 

        Opportunity oppty = new Opportunity();
        oppty.Name = 'oppty test';
        oppty.AccountId = customerAcc.Id;
        oppty.CloseDate = Date.today();
        oppty.StageName = 'Quote Definition';
        insert oppty;

        SBQQ__Quote__c quot = new SBQQ__Quote__c();
        quot.SBQQ__Account__c = customerAcc.Id;
        quot.SBQQ__Opportunity2__c = oppty.Id;
        quot.SBQQ__Status__c = 'Draft';
        quot.SBQQ__SalesRep__c = u.Id;
        insert quot;

        QuoteLineTriggerHandler.disableTrigger = true;
        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c ql1 =  new SBQQ__QuoteLine__c  (
            SBQQ__Quote__c = quot.Id
            , SBQQ__Product__c = prod1.id
            , SBQQ__Quantity__c = 1
            , Subscription_Term__c = '12'
        );
        quoteLines.Add(ql1);  
        
         SBQQ__QuoteLine__c ql2 =  new SBQQ__QuoteLine__c  (
            SBQQ__Quote__c = quot.Id
            , SBQQ__Product__c = prod2.id
            , SBQQ__Quantity__c = 1
            , Subscription_Term__c = '12'
        );
        quoteLines.Add(ql2);  

        insert quoteLines;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        Order ord = new Order();
        ord.SambaIsStartConfirmed__c  = false;
        ord.Status = 'Draft';
        ord.EffectiveDate = Date.today();
        ord.AccountId = customerAcc.Id;
        insert ord;

        //Contract contr = [SELECT Id FROM Contract WHERE SBQQ__Order__c = ord.Id];

        Contract contr = new Contract ();
        contr.AccountId = customerAcc.Id;
        contr.StartDate = Date.today();
        contr.Status ='Draft';
        contr.SBQQ__Quote__c = quot.Id;
        contr.SBQQ__Order__c = ord.Id;
        insert contr;

        //contr.Status ='Active';
        //update contr;

        Place__c place1 = new Place__c();
        place1.Account__c = customerAcc.Id;
        place1.Company__c = 'TestPlace';
        place1.Name = 'TestPlace';
        place1.PlaceID__c = 'TestPlaceAAABBBCCC';
        place1.Place_Type__c = 'Business';        
        insert place1;

        test.startTest();
        
            SBQQ__Subscription__c subMyPres = new SBQQ__Subscription__c();
            subMyPres.SBQQ__Account__c = customerAcc.Id;
            subMyPres.SBQQ__Contract__c = contr.Id;
            subMyPres.SBQQ__Product__c = prod1.Id;
            subMyPres.Subsctiption_Status__c = 'Active';
            subMyPres.SBQQ__Quantity__c = 1.0;
            subMyPres.Place__c = place1.Id;
        	subMyPres.Total__c = 500;
        	subMyPres.Subscription_Term__c = '12';
        	subMyPres.SBQQ__QuoteLine__c = ql1.id;
            subList.add(subMyPres);

            SBQQ__Subscription__c subMyCash = new SBQQ__Subscription__c();
            subMyCash.SBQQ__Account__c = customerAcc.Id;
            subMyCash.SBQQ__Contract__c = contr.Id;
            subMyCash.SBQQ__Product__c = prod2.Id;
            subMyCash.Subsctiption_Status__c = 'Active';
            subMyCash.SBQQ__Quantity__c = 1.0;
        	subMyCash.Total__c = 500;
        	subMyCash.Subscription_Term__c = '12';
        	subMyCash.SBQQ__QuoteLine__c = ql2.id;
            subList.add(subMyCash);

            insert subList;
        
        	contr.Status ='Active';
        	update contr;
        test.stopTest();
    }
}