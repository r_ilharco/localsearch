/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Account Team Member Trigger Handler
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-08-10
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class AccountTeamMemberTriggerHandler implements ITriggerHandler {
	
    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }

    public void BeforeInsert(List<SObject> newItems) {
        List<AccountTeamMember> newItemlist = (List<AccountTeamMember>) newItems;
        Map<Id,AccountTeamMember> newItemMap = new Map<Id,AccountTeamMember>();
        for(AccountTeamMember item: newItemlist){
            newItemMap.put(item.id,item);
        }
        AccountTeamMemberTriggerHelper.checkPermissions(newItemMap);
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        AccountTeamMemberTriggerHelper.checkPermissions((Map<Id, AccountTeamMember>)newItems);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems){
        AccountTeamMemberTriggerHelper.checkPermissions((Map<Id, AccountTeamMember>) oldItems);
    }
    public void AfterInsert(Map<Id, SObject> newItems){}
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}
    public void AfterDelete(Map<Id, SObject> oldItems){}
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}