/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.      
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-01-2019
 * Sprint      : 1
 * Work item   : 19
 * Testclass   :
 * Package     : 
 * Description : Apex Class Controller for HierachicalQuoteLines.cmp, It retrieves the hierarchy of Quote Lines based on the argument passed(QuoteId) in order to show them into the TreeGrid Component
 * Changelog   : 
 */


public without sharing class HierarchicalQuoteLinesController {

    /**
	 * Gennaro Casola | Sprint 1 | Date  01/04/2019
	 * Description
     *** Server-Side Apex Controller for HierarchicalQuoteLines.cmp ***
     *
     * It gets all the quote lines relative to a quote based on the quote id. 
	 * Then it's gonna be constructed a hierarchy map by scanning the obtained elements, 
     * and selecting them as parent or child depending on SBQQ__RequiredBy__c.
     * Finally the data structure it's somministrated to the JSONWriter 
     * for getting the JSON Format Structure of Quote Lines.
     *
	 * @param id | Id  | An Id of a SBQQ__Quote__c
	 *
	 * @return String | A JSON Structure which contains the Quote Lines of a Quote to be submitted to a treegrid lightning component
	 */
    @AuraEnabled
    public static String getQuoteLinesByQuoteId(Id id){
        Map<Id, SBQQ__QuoteLine__c> quoteLinesMap = QuoteLineSelector.getMapRecordsByQuoteId(id);
        system.debug('quoteLinesMap ::: '+quoteLinesMap);
        
        
        //Remove non price relevant products from the list
        Map<String,Boolean> featureToggle = ReadFeatureToggle();
        system.debug('featureToggle.get' +featureToggle.get('Hide_Non_Price_Relevant_Items'));
        if(featureToggle.containsKey('Hide_Non_Price_Relevant_Items') && featureToggle.get('Hide_Non_Price_Relevant_Items')){
            for(Id quoteLineId : quoteLinesMap.keySet())
            {
                SBQQ__QuoteLine__c quoteLine = (SBQQ__QuoteLine__c)quoteLinesMap.get(quoteLineId);
                if(quoteLine.NonPriceRelevant__c)  quoteLinesMap.remove(quoteLineId);
                
            }
        }


                Map<SBQQ__QuoteLine__c, List<SBQQ__QuoteLine__c>> quoteLinesHierarchyMap = QuoteLineSelector.getHierarchyMapRecordsByQuoteLinesMap(quoteLinesMap);
        
        Map<Id, Map<String, String>> translatedValues = new Map<Id, Map<String, String>>();
        
        Map<String, String> keyPairs = new Map<String,String>{ 'Id' => 'Id',
                                                            'Name' => 'Name',
                                                            'Place__c' => 'Place__c',
                                                            'Place_Name__c' => 'Place_Name__c',
                                                            'SBQQ__Product__c' => 'SBQQ__Product__c',
                                                            'SBQQ__ProductName__c' => 'SBQQ__ProductName__c',
                                                            'SBQQ__Quantity__c' => 'SBQQ__Quantity__c',
                        									'SBQQ__AdditionalDiscount__c' => 'SBQQ__AdditionalDiscount__c',
            												'Total__c' => 'Total__c',
                                                            'Base_term_Renewal_term__c' => 'Base_term_Renewal_term__c',
            												'ASAP_Activation__c' =>'ASAP_Activation__c',
                                                            'SBQQ__StartDate__c' => 'SBQQ__StartDate__c'
                                                            
            };
                
        Set<Id> productIds = new Set<Id>();
        String language = UserInfo.getLanguage();
		     
        for (SBQQ__QuoteLine__c q: quoteLinesMap.values()) {
            productIds.add( q.SBQQ__Product__c);
            
        }	
        
		List<SBQQ__Localization__c> localizations = [SELECT ID, SBQQ__Product__c, SBQQ__Language__c, SBQQ__Text__c FROM SBQQ__Localization__c WHERE SBQQ__Product__c IN :productIds AND SBQQ__Label__c = :ConstantsUtil.TRANSLATION_LABEL_PRODUCT_NAME];      
        System.debug ('localications ->' + localizations);
        Map<Id, Map<String, String>> productLanguageTranslation = new Map<ID, Map<String, String>>();
        for(SBQQ__Localization__c loc : localizations){
            if(!String.isBlank(loc.SBQQ__Text__c)){
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                    productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                }else{
                    productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                }
            }
        }
        
        for(SBQQ__QuoteLine__c ql : quoteLinesMap.values())
        {
            if(productLanguageTranslation.containsKey(ql.SBQQ__Product__c) && productLanguageTranslation.get(ql.SBQQ__Product__c).containsKey(language))
            {
                if(!translatedValues.containsKey(ql.Id))
                {
                    translatedValues.put(ql.Id, new Map<String,String>());
                }
                
                translatedValues.get(ql.Id).put('SBQQ__ProductName__c', productLanguageTranslation.get(ql.SBQQ__Product__c).get(language));
            }
        }
        
        System.debug ('Translated Valude map -> ' + translatedValues);
        return JSONWriter.getJSONbyHierarchyMap(quoteLinesHierarchyMap, keyPairs, translatedValues);
        
    }
    
    
    //A.L. Marotta 28-10-19 Start
    //Wave F, Sprint 12 - returns the logged in user Profile name
    @AuraEnabled
    public static String getProfileName(){
        Id profileId = userinfo.getProfileId();
		String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        List<PermissionSetAssignment> pSAssign = [SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId =:Userinfo.getUserId()];
        for(PermissionSetAssignment ps : pSAssign){
            profileName +=';'+ps.PermissionSet.Name;
        }
        return profileName;
    }

    //Query All Feature_Toggle__mdt 
    public static Map<String,Boolean> ReadFeatureToggle(){
        Map<String, Boolean> features = new Map<String, Boolean>();
        for (Feature_Toggle__mdt  feature : [SELECT DeveloperName, Active__c FROM Feature_Toggle__mdt ]) {
            features.put(feature.DeveloperName, feature.Active__c);
            system.debug('features:'+feature);
        }

        return features;
    }
}