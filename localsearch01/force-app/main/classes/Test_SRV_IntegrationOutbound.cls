@IsTest
public class Test_SRV_IntegrationOutbound {
	@TestSetup
    static void setupData(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        
        List<Account> accs = Test_DataFactory.createAccounts('test acc', 1);
        insert accs;
        List<Contact> conts = Test_DataFactory.createContacts(accs.get(0), 1);
        insert conts;
        List<Billing_Profile__c> bps = Test_DataFactory.createBillingProfiles(accs, conts, 1);
        insert bps;
        list<Lead> lds = Test_DataFactory.createleads('Test','Lead',1);
        insert lds;
    }
    
    @IsTest 
    static void test_ALL_Account_OK()
    {
        String customRequestJSON = '{"zip":null,"uid":null,"street":null,"phone":null,"params":null,"location":null,"email":null,"company":null}';
        SRV_IntegrationOutbound.AddressValidationResult avr = new SRV_IntegrationOutbound.AddressValidationResult(new SRV_IntegrationOutbound.AddressValidationResult());
        SRV_IntegrationOutbound.AddressValidationData avd = new SRV_IntegrationOutbound.AddressValidationData(new SRV_IntegrationOutbound.AddressValidationData());
        ValidateAddressRequestWrapper customRequest = new ValidateAddressRequestWrapper();
        SRV_IntegrationOutbound.AddressValidationResult avResult = new SRV_IntegrationOutbound.AddressValidationResult();
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
        	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id);
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id);
        	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id, customRequestJSON);
        	SRV_IntegrationOutbound.syncValidateAddress(customRequest);
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id, customRequest);
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id, customRequest, true);
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id, customRequest, avResult, true);
        Test.stopTest();
    }
    
    /*@IsTest 
    static void test_syncValidateAddress_Account_DUPLICATE_DETECTED()
    {
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Account duplicatedAccount = new Account();
        duplicatedAccount.Name = 'Name';
        duplicatedAccount.BillingStreet = 'BillingStreet';
        duplicatedAccount.BillingCity = 'BillingCity';
        duplicatedAccount.BillingCountry = 'BillingCountry';
        duplicatedAccount.BillingPostalCode = '1005';
        duplicatedAccount.UID__c = 'UIDXYZ';
        duplicatedAccount.HRID__c = 'HRIDXYZ';
        duplicatedAccount.Phone = '123456';
        duplicatedAccount.VAT_ID__c = 'VATIDXYZ';
        duplicatedAccount.Mobile_Phone__c = '123454';
        duplicatedAccount.Fax = '3332411433';
        
        AccountTriggerHandler.disableTrigger = true;
        insert duplicatedAccount;
        AccountTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
            SRV_IntegrationOutbound.syncValidateAddress(acc.Id);
        	SRV_IntegrationOutbound.syncValidateAddress(duplicatedAccount.Id);
        Test.stopTest();
    }*/
    
    @IsTest 
    static void test_asyncValidateAddress_Account_OK()
    {
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
        	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_asyncValidateAddress_BillingProfile_OK()
    {
        Billing_Profile__c bp = [SELECT Id, Customer__c FROM Billing_Profile__c LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(bp.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{bp});
        	SRV_IntegrationOutbound.asyncValidateAddress(bp.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_asyncValidateAddress_Lead_OK()
    {
        Lead ld = [SELECT Id FROM Lead LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(ld.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{ld});
        	SRV_IntegrationOutbound.asyncValidateAddress(ld.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_Account_OK()
    {
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_BillingProfile_OK()
    {
        Billing_Profile__c bp = [SELECT Id, Customer__c FROM Billing_Profile__c LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(bp.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{bp});
        	SRV_IntegrationOutbound.syncValidateAddress(bp.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_Lead_OK()
    {
        Lead ld = [SELECT Id FROM Lead LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(ld.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{ld});
        	SRV_IntegrationOutbound.syncValidateAddress(ld.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_asyncValidateAddress_Account_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
        	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_asyncValidateAddress_BillingProfile_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Billing_Profile__c bp = [SELECT Id, Customer__c FROM Billing_Profile__c LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(bp.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{bp});
        	SRV_IntegrationOutbound.asyncValidateAddress(bp.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_asyncValidateAddress_Lead_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Lead ld = [SELECT Id FROM Lead LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(ld.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{ld});
        	SRV_IntegrationOutbound.asyncValidateAddress(ld.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_Account_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Account acc = [SELECT Id, SkipAddressValidation__c FROM Account LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(acc.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{acc});
        	SRV_IntegrationOutbound.syncValidateAddress(acc.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_BillingProfile_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Billing_Profile__c bp = [SELECT Id, Customer__c FROM Billing_Profile__c LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(bp.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{bp});
        	SRV_IntegrationOutbound.syncValidateAddress(bp.Id);
        Test.stopTest();
    }
    
    @IsTest 
    static void test_syncValidateAddress_Lead_KO()
    {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO) );
        Lead ld = [SELECT Id FROM Lead LIMIT 1];
        Test.startTest();
        	Boolean skipAddressValidationFlag = SRV_IntegrationOutbound.getSkipAddressValidationFlag(ld.Id);
        	Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(new List<SObject>{ld});
        	SRV_IntegrationOutbound.syncValidateAddress(ld.Id);
        Test.stopTest();
    }
    
}