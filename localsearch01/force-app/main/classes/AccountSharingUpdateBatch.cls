global class AccountSharingUpdateBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    private String sSQL = null;
    private List<Id> accountTerritoryIds;
    private List<Id> accountOwnerIds;
    private List<String> staticValues;
    
    public AccountSharingUpdateBatch(){
        accountTerritoryIds = new List<Id>();
        accountOwnerIds = new List<Id>();
    }

    public AccountSharingUpdateBatch(List<String> val){
        accountTerritoryIds = new List<Id>();
        accountOwnerIds = new List<Id>();
        staticValues = new List<String>(val);
    }
    
    global database.querylocator start(Database.BatchableContext bc){      
        sSQL = 'SELECT Id, Territory_Changed__c, Owner_Changed__c FROM Account WHERE (Territory_Changed__c = true OR Owner_Changed__c = true)';
        if(!(staticValues.isEmpty())) sSQL += ' AND Cluster_Id__c IN :staticValues';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
        system.debug('scope: ' + scope);
        List<Account> accountsToUpdate = new List<Account>();
        for(Account ac : scope){
            if(ac.Territory_Changed__c){
                accountTerritoryIds.add(ac.Id);
            }else if(ac.Owner_Changed__c){
                accountOwnerIds.add(ac.Id);
            }
            ac.Territory_Changed__c = false;
            ac.Owner_Changed__c = false;
            accountsToUpdate.add(ac);
        }
        if(accountsToUpdate.size() > 0){
            update accountsToUpdate;
        }
    }
    global void finish(Database.BatchableContext bc){
        if(accountTerritoryIds.size() > 0){
            system.debug('batchs for territory recalculation --> queued');
            system.debug('accountTerritoryIds: ' + accountTerritoryIds);
            QuoteTerritoryChangedBatch quoteTerritoryBatch = new QuoteTerritoryChangedBatch(accountTerritoryIds);
            Database.executeBatch(quoteTerritoryBatch, 200); 
            SubscriptionTerritoryChangedBatch subTerritoryBatch = new SubscriptionTerritoryChangedBatch(accountTerritoryIds);
            Database.executeBatch(subTerritoryBatch, 200);
        }
        if(accountOwnerIds.size() > 0){
            system.debug('batchs for owner recalculation --> queued');
            system.debug('accountOwnerIds: ' + accountOwnerIds);
            QuoteOwnerChangedBatch quoteOwnerBatch = new QuoteOwnerChangedBatch(accountOwnerIds);
            Database.executeBatch(quoteOwnerBatch, 200); 
            SubscriptionOwnerChangedBatch subOwnerBatch = new SubscriptionOwnerChangedBatch(accountOwnerIds);
            Database.executeBatch(subOwnerBatch, 200);
        }
    }
    
    global void execute(SchedulableContext sc){
        AccountSharingUpdateBatch b = new AccountSharingUpdateBatch(); 
        database.executebatch(b,150);
    }
}