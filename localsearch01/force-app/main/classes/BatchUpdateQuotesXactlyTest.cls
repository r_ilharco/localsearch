@isTest
public class BatchUpdateQuotesXactlyTest {  
    @Testsetup  
    static void setup(){
        integer i = 0;
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
       
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generateFatherProduct());


        insert productsToInsert;
        
        system.debug('productsToInsert[0].Create_automatic_renewal_opportunity__c' +productsToInsert[0].Create_automatic_renewal_opportunity__c);
        system.debug('productsToInsert[0].Create_automatic_renewal_opportunity__c' +productsToInsert[0].Id);
        
        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;
        system.debug('debug:::'+i++);
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
        system.debug('debug:::'+i++);
        //Billing_Profile__c billingProfile = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :account.Id LIMIT 1];
		system.debug('debug:::'+i++);
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        insert opportunity;
        
        SBQQ__Quote__c quote = [SELECT Id,Replacement__c,SBQQ__Opportunity2__c,SBQQ__Account__c FROM SBQQ__Quote__c Where SBQQ__Opportunity2__c = :opportunity.Id LIMIT 1];
       	
        
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
		system.debug('QUERY ::'+[select Id,Create_automatic_renewal_opportunity__c from  Product2 where Id =:fatherQl.SBQQ__Product__c].Create_automatic_renewal_opportunity__c);
        system.debug('fatherQL.Create_automatic_renewal_opportunity__c' +fatherQl.SBQQ__Product__r.Create_automatic_renewal_opportunity__c);
        system.debug('fatherQl.SBQQ__Product__c' +fatherQl.SBQQ__Product__c);
        insert fatherQL;        
        
        QuoteTriggerHandler.disableTrigger = false;
        SBQQ__QuoteDocument__c quoteDocument1 = Test_DataFactory.generateQuoteDocument(quote.Id);
        insert quoteDocument1;
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        QuoteTriggerHandler.disableTrigger = true;

        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        

        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.StartDate = Date.today().addMonths(-3);
        contract.EndDate = Date.today().addMonths(12);
        
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        system.debug('fatherSub ::: '+ fatherSub.SBQQ__Product__r.Create_automatic_renewal_opportunity__c);
        insert fatherSub;
        
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        update subsToUpdate;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract; 
      	system.debug('Contract statuts ::: '+contract.Status);
            
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;       
    }
    
    @istest 
    static void test_Batch(){
        List<user> ul = [select id from User where isactive = true];
        system.debug('ul ::'+ul);
        Datetime d = Datetime.now();
        set<string> cStatus = new set<string>{ConstantsUtil.CONTRACT_STATUS_CANCELLED , ConstantsUtil.CONTRACT_STATUS_EXPIRED ,ConstantsUtil.CONTRACT_STATUS_TERMINATED};
        List<contract> c = [select AccountId,Account.OwnerId, CustomerSignedDate, OwnerAtAutorenewal__c, OwnerOnSignature__c, SBQQ__Quote__c, SBQQ__Quote__r.Contract_Signed_Date__c, SBQQ__Quote__r.SBQQ__Account__c 
         from Contract 
         where Status not in :cStatus
         and SBQQ__Quote__c != null and SBQQ__Quote__r.AccountOwner__c = null];
        system.debug('c1 ::'+c);
        system.debug('c1 ::'+c.size());
        contract c1= c[0];
        
        Account a = [select id, ownerId from account where id  = :c1.AccountId limit 1];
        system.debug('a.ownerId '+a.ownerId);
        a.ownerId = ul[ul.size()-1].Id;
        system.debug('a.ownerId '+a.ownerId);
        
        
        test.startTest();
        update a;
        system.debug('c1 ::'+c1);
        BatchUpdateQuotesXactly bat = new BatchUpdateQuotesXactly();
        bat = new BatchUpdateQuotesXactly(1);
		bat = new BatchUpdateQuotesXactly(new List<id>{c1.id});
        bat.execute(null);  

        BatchUpdateQuotesXactly_Schedulable x = new BatchUpdateQuotesXactly_Schedulable();
        x.execute(null);
        
        test.stopTest();   
        //system.assert([Select Id,StageName From Opportunity where Type = :ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS and Id =:opplist[0].Id].StageName == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST);
    }

    
    public static Product2 generateFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        
        product.Create_automatic_renewal_opportunity__c = true;
        product.Opportunity_Start_Date_DMC__c = 365;
        product.Opportunity_Expiration_Date_DMC__c = 91;
        product.Opportunity_Expiration_Date_Telesales__c = 0;
        
        return product;        
    }
}