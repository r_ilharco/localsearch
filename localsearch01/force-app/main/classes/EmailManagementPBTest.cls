@isTest
public class EmailManagementPBTest {

    @isTest
    static void testEmailManagement()
    {
        try{
        
            User u = new user();
            u.LastName = 'Test Code';
            u.Email = 'iqbal.rocky@arollotech.com';
            u.Alias = 'Tcode';
            u.Username = 'iqbal.roc@arollo.com';
            u.CommunityNickname = 'test12';
            u.LocaleSidKey = 'en_US';
            u.TimeZoneSidKey = 'GMT';
            u.ProfileID = '00e1r0000027zBpAAI';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
        
            insert u;
        
       		Case cs =  new Case();
            cs.RecordTypeId = '0121r000000xEFIAA2';
            cs.SuppliedEmail = 'iqbal.rocky@arollotech.com';
            cs.Origin = 'Web';
            cs.OwnerId= u.Id; 
            cs.Status='Closed';

            insert cs;
            system.debug('case' +cs);
            
            
            EmailMessage em= new EmailMessage();
            em.Incoming=true;
            em.ParentId= cs.Id;
            
            test.startTest();
            insert em;
            test.stopTest();
        }
        catch(Exception e)
        {
            system.assert(false,e.getMessage());
        }
    }
}