public class QuoteContractTemplateCreationScript {
    
    public void createQuoteTemplate(){
        SBQQ__TemplateContent__c headerContent = createHeader();
        SBQQ__TemplateContent__c footerContent = createFooter();
        
        SBQQ__QuoteTemplate__c qt = createTheTemplate(headerContent.Id, footerContent.Id);
        
        SBQQ__TemplateContent__c sr = createHeaderContent();
        SBQQ__TemplateSection__c ts = createSection('Contract Body', qt.Id, sr.Id, 10, '', 'Auto');
        
		sr = createLineContent();
        ts = createSection('Contract Items', qt.Id, sr.Id, 20, 'FFFFFF', 'Always');
        
        sr = createTotalCountContent();
        ts = createSection('Contract Total', qt.Id, sr.Id, 30, 'ECEBEB', 'Always');
        
        sr = createAfterQuoteLineContent();
        ts = createSection('Contract After Quote Line', qt.Id, sr.Id, 40, '', 'Always');
        
        sr = createTermContent();
        ts = createSection('Contract Quote Term1', qt.Id, sr.Id, 50, '', 'Always');
        
		sr = createSecondTermContent();
        ts = createSection('Contract Quote Term2', qt.Id, sr.Id, 60, '', 'Always');
    }
    
    public SBQQ__QuoteTemplate__c createTheTemplate(Id headerId, Id footerId) {
        SBQQ__QuoteTemplate__c qt = new SBQQ__QuoteTemplate__c (
        	Name = 'swisslist_telesales_vertrag_de',
            SBQQ__DeploymentStatus__c = 'Deployed',
            SBQQ__PageHeight__c = 12.00,
            SBQQ__PageWidth__c  = 8.50,
            SBQQ__TopMargin__c = 0.50,
            SBQQ__BottomMargin__c = 0.50,
            SBQQ__LeftMargin__c = 0.50,
            SBQQ__RightMargin__c = 0.50,
            SBQQ__HeaderContent__c = headerId,
            SBQQ__FooterContent__c = footerId,
            SBQQ__HeaderHeight__c = 45.000,
            SBQQ__FooterHeight__c = 10.000,
            SBQQ__CompanyName__c  = 'Swisscom Directories AG',
            SBQQ__CompanyStreet__c = 'Förrlibuckstrasse 62',
            SBQQ__CompanyCity__c  = 'Zurich',
            SBQQ__CompanyPhone__c = '0800 888 810',
            SBQQ__CompanyPostalCode__c = '8021',
            SBQQ__CompanyCountry__c = 'Switzerland',
            SBQQ__FontFamily__c = 'Titillium Web',
            SBQQ__FontSize__c = 7.0,
            SBQQ__BorderColor__c = '000000',
            SBQQ__ShadingColor__c = 'FFFFFF',
            SBQQ__TermBodyIndent__c  = 15,
            LocalSearch_Logo__c = '',
            Footer_Logo__c = '',
            SBQQ__GroupGap__c = 15
        );
        insert qt;
        return qt;
    }
    
    public SBQQ__TemplateContent__c createHeader() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Header' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test Logo';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Header',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createFooter() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Footer' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test Footer';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Footer',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createHeaderContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Contract_Body' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Contract_Body',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createLineContent() {
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Quote_Line_Items',
            SBQQ__Type__c = 'Line Items',
            SBQQ__FontFamily__c = 'Titillium Web',
            SBQQ__FontSize__c = 11.00,
            SBQQ__TableStyle__c = 'Standard'
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createTotalCountContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Total_Count' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Total_Count',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createAfterQuoteLineContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Contract_After_Quote_Line' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Contract_After_Quote_Line',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateContent__c createTermContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Quote_Terms1' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Quote_Terms1',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }

    public SBQQ__TemplateContent__c createSecondTermContent() {
        String body = '';
        if(!Test.isRunningTest()) {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'Quote_Terms2' LIMIT 1];
            body = sr.Body.toString();
        } else {
            body = 'Test HTML';
        }
        SBQQ__TemplateContent__c tc = new SBQQ__TemplateContent__c (
        	Name = 'Quote_Terms2',
            SBQQ__Type__c = 'HTML',
            SBQQ__RawMarkup__c = body
        );
        insert tc;
        return tc;
    }
    
    public SBQQ__TemplateSection__c createSection(String name, Id idTemp, Id idContent, Integer order, String borderColor, String keepTogether) {
        SBQQ__TemplateSection__c ts = new SBQQ__TemplateSection__c (
        	Name = name,
            SBQQ__Content__c = idContent,
            SBQQ__Template__c = idTemp,
            SBQQ__DisplayOrder__c = order,
            SBQQ__BorderColor__c = borderColor,
            SBQQ__KeepTogether__c = keepTogether
        );
        insert ts;
        return ts;
    }
    
}