/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Gennaro Casola <gcasola@deloitte.it>
 * Date        : 11-12-2019
 * Sprint      :  
 * Work item   : SF2-422
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

@RestResource(urlMapping='/users/deactivate')
global  with sharing class UserManagementRESTService {
    public static Integer FEDERATION_ID_SIZE = 512;
	
    @HttpPost
    global static void deactivateUser(String FederationId, Boolean IsActive){
        RestResponse res = RestContext.response;
        res.statusCode = 200;
        String fedId = (FederationId.length()>UserManagementRESTService.FEDERATION_ID_SIZE?FederationId.subString(0,UserManagementRESTService.FEDERATION_ID_SIZE-1):FederationId);
        Map<Id, User> users;
        Id userId;
        
        Database.SaveResult updateSR;
        
        try
        {
            users = SEL_User.getUsersByFederationId(new Set<String>{fedId});
            
            if(users != NULL && users.size() == 1)
            {
                userId = users.values().get(0).Id;
            }
            
            if(users != NULL && users.size() == 1)
            {
                if(IsActive == false)
                {
                    if(users.values().get(0).IsActive == true)
                    {
                        Map<Id, PermissionSetAssignment> userPermissionSet = SEL_UserPackageLicense.getUserPermissionSetByUserId(new Set<Id>{users.values().get(0).Id});
                            if(userPermissionSet != NULL && userPermissionSet.size() > 0)
                                delete userPermissionSet.values();
                        Map<Id, PermissionSetLicenseAssign> userPackageLicenses = SEL_UserPackageLicense.getUserPackageLicensesByUserId(new Set<Id>{users.values().get(0).Id});
                            if(userPackageLicenses != NULL && userPackageLicenses.size() > 0)
                                delete userPackageLicenses.values();
                       
                        users.values().get(0).IsActive = false;
                        updateSR = Database.update(users.values().get(0));
                    
                        if(!updateSR.isSuccess())
                        {
                            res.statusCode = 400;
                            res.responseBody = Blob.valueOf('The user hasn\'t been deactivated.');
                        }
                    }else
                    {
                      	res.statusCode = 400;
                    	res.responseBody = Blob.valueOf('The user has been already deactivated.');  
                    }
                }else
                {
                    res.statusCode = 400;
                    res.responseBody = Blob.valueOf('The user can\'t be activated.');  
                }
                
            }else
            {
                res.statusCode = 400;
            }
            //LogUtility.saveLogUserManagementDeactivation(RestContext.request, res, userId, null);
            LogUtility.saveLogUserManagementDeactivation(RestContext.request.requestURI, Json.serialize(RestContext.request.params), res.statusCode, String.valueOf(res.responseBody), userId, null);
        }catch(Exception ex)
        {
            res.statusCode = 500;
            //LogUtility.saveLogUserManagementDeactivation(RestContext.request, res, userId, ex);
            LogUtility.saveLogUserManagementDeactivation(RestContext.request.requestURI, Json.serialize(RestContext.request.params), res.statusCode, String.valueOf(res.responseBody), userId, ex.getMessage());
        } 
        
    }
}