public class SLMigrationMassiveContractCreation_Batch implements Database.Batchable<sObject> {
    list <id> WonOpties = new list<id>();
    public integer offset =10;
    public integer batchSize = 50;
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([select id from opportunity where id in:WonOpties]);
    }
    public  void execute(Database.BatchableContext BC, List<opportunity> scope) { 
         list<opportunity> opt = new list<opportunity>();
         for (opportunity o:  scope){
             o.SBQQ__Contracted__c=true;      
             opt.add(o);       
         }
         update opt;
    }
     public void finish(Database.BatchableContext BC) {   
        system.scheduleBatch(new   ContractBeforeActive_BatchSch(), 'ContractBeforeActive', offset, batchSize);
        system.debug('JOB ENDED');
    }
    public SLMigrationMassiveContractCreation_Batch   (list<id> ids){
      WonOpties=ids; 
   } 
}