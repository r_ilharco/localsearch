public class AbacusTransferJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable  {
    private string abaToken;
    private string soapNS = ConstantsUtil.XMLNS_SOAP;
    private string xsi = ConstantsUtil.XMLNS_SCHEMA_INSTANCE;
    private string abaTypesNS = ConstantsUtil.XMLNS_ABA_CONNECT_TYPES;
    private string abaDocNS = ConstantsUtil.XMLNS_ABA_DEBI_DOCUMENT;
    private string abaDocTypeNS = ConstantsUtil.XMLNS_ABA_DOCUMENT_TYPES;
        
    public AbacusTransferJob() {
      //  this.abaToken = AbacusHelper.abacusLoginToken();
    }
    /*
    public AbacusTransferJob(string abaToken) {
        this.abaToken = abaToken;
    }*/
    
    public List<sObject> start(Database.BatchableContext context) {
        // prepare data
        // Select invoices and related data with invoice status 'Collected' && 	Registered = false        
    	return SEL_Invoices.getInvoices(DOMC_Invoice.INVOICE_PROCESS_MODE_SWISS_LIST,
                                        DOMC_Invoice.INVOCE_IS_PARTIALLY_PAYED_TRUE,
                                        DOMC_Invoice.INVOCE_REGISTERED_FALSE,
                                        DOMC_Invoice.INVOICE_STATUS_COLLECTED,
                                        DOMC_Invoice.INVOICE_STATUS_CANCELLED,
                                        DOMC_Invoice.PAYMENT_STATUS_C1,
                                        DOMC_Invoice.PAYMENT_STATUS_C2,
                                        DOMC_Invoice.PAYMENT_STATUS_C3,
                                        DOMC_Invoice.INVOICE_STATUS_OVERDUE,
                                        DOMC_Invoice.INVOICE_STATUS_PAYED
                                       );
    }

    public void execute(SchedulableContext sc){
        AbacusTransferJob job = new AbacusTransferJob();
        List<Billing_Setting__mdt> settingSngl = new List<Billing_Setting__mdt>();
        Integer batchSize = 5;
        settingSngl = [select AbacusSize__c from Billing_Setting__mdt where Label =: ConstantsUtil.BILLING_SETTINGS_METADATA_LABEL];
        if(settingSngl != null && settingSngl[0].AbacusSize__c != null){
            batchSize = (Integer)settingSngl[0].AbacusSize__c;
        }
        system.debug('batchSize: ' + batchSize);
        Database.executeBatch(job,batchSize);
    }
    
    public void execute(Database.BatchableContext context, List<sObject> dataset) {
        //#179, Sprint 1, Luciano di Martino / Sara Dubbioso, previous implementation (SOAP CALL) has been commented 
        //REST Callout has been implemented applying the Enterprise Pattern
		List<Invoice__c> invoices = (List<Invoice__c>)dataset;
        List<Invoice_Order__c> invoiceOrders = SEL_InvoiceOrders.getInvoiceOrders(invoices);
        List<Invoice__c> updatedInvoices = new List<Invoice__c> ();
        List<Error_Log__c> errors = new List<Error_Log__c> ();
        Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = SRV_InvoiceOrder.setMap(invoiceOrders);
        
        String correlationId = ContractUtility.getUniqueId(String.valueOf(DateTime.now().getTime()));
        
        AbacusJsonWrapper invoiceWrapper = SRV_Abacus.getPayload(invoiceOrdersMap, invoices);
        String jsonPayload =  SRV_Abacus.formatCorrection(JSON.serializePretty(invoiceWrapper, true));
        System.debug('jsonPayload: ' + jsonPayload);
        try {
            SRV_REST_Abacus restAbacus = new SRV_REST_Abacus (DOMC_Abacus.REST_ABACUS_ENDPOINT, correlationId, jsonPayload);
            HttpResponse response = restAbacus.restCall();

            integer statusCode = response.getStatusCode();
            errors.add(restAbacus.checkStatus(response)); 
            
            updatedInvoices = SRV_Abacus.updateInvoices(invoices);
            
               
        } catch ( Exception ex ) {
            errors.add(ErrorHandler.createLogError('Billing', 'Billing.sendToAbacus', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, jsonPayload, correlationId));
        }
        
        try {
             if(updatedInvoices!=null && updatedInvoices.size() > 0) {
            	update updatedInvoices;
            }
            if(errors.size()> 0) {
                insert errors;
            }
        } catch ( Exception ex ) {
            ErrorHandler.log(System.LoggingLevel.ERROR, 'Billing', 'Billing.sendToAbacus', ex, ErrorHandler.ErrorCode.E_DML_FAILED, null, ex.getStackTraceString() +'/n'+ ex.getMessage(), null, null, null, false);    
        }
        
        
        
        /*************************************************************************************************************
		List<Invoice__c> invoiceUpdates = new List<Invoice__c>();
        List<Error_Log__c> errors = new List<Error_Log__c>();
        for(Invoice__c invoice : invoices) {
            try {
                if(!BillingHelper.checkMinTotalAllowed(invoice)) {
	                errors.add(ErrorHandler.createLogWarning('Billing', 'Billing.sendToAbacus', ErrorHandler.WarningCode.W_BILL_TOTAL_BELOW_LIMIT, 'Bill total below limit', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));
                    invoiceUpdates.add(new Invoice__c(Id=invoice.Id, Status__c = ConstantsUtil.INVOICE_STATUS_IN_COLLECTION));
                    continue;
                }
                Dom.Document doc = new Dom.Document();
                Dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'SOAP-ENV');
                envelope.setNamespace('xsi', xsi);
                envelope.setNamespace('ns1', abaTypesNS);
                envelope.setNamespace('ns2', abaDocNS);
                Dom.XmlNode body = envelope.addChildElement('Body', soapNS, 'SOAP-ENV');
                List<Invoice_Order__c> invoiceGroups = invoiceOrdersMap.get(invoice.Id);
                
                Dom.XmlNode saveRequest = body.addChildElement('SaveRequest', abaDocNS, 'ns2');
                Dom.XmlNode connectParam = saveRequest.addChildElement('AbaConnectParam', abaDocNS, 'ns2');
                // connectParam.setNamespace('ns1', abaTypesNS);
                connectParam.addChildElement('Login', abaTypesNS, 'ns1').addChildElement('LoginToken', abaTypesNS, 'ns1').addTextNode(abaToken);  
                connectParam.addChildElement('Revision', abaTypesNS, 'ns1').addTextNode('0');
                connectParam.addChildElement('Level', abaTypesNS, 'ns1').addTextNode('Warning');
                
                Dom.XmlNode data = saveRequest.addChildElement('Data', abaDocNS, 'ns2');
                Dom.XmlNode document = data.addChildElement('Document', abaDocTypeNS, '');
                // document.setNamespace('', abaDocTypeNS);
                document.setAttribute('mode', 'SAVE');
                putTextElement(document, 'DocumentCode', invoice.Invoice_Code__c);
                putTextElement(document, 'CustomerNumber', AbacusHelper.getAbaCustomer(invoice));
                putTextElement(document, 'Number', invoice.Name.replaceAll('[^0-9]', ''));
                putTextElement(document, 'Reference', invoice.Correlation_Id__c);
                putTextElement(document, 'AccountReceivableDate', AbacusHelper.toISO8601String(AbacusHelper.validTaxMonth(invoice.Invoice_Date__c)));
                putTextElement(document, 'Currency', 'CHF');
                putTextElement(document, 'Amount', invoice.Total__c);
                putTextElement(document, 'KeyAmount', invoice.Total__c);
                putTextElement(document, 'ReminderProcedure', ConstantsUtil.ABACUS_REMINDER_PROCEDURE);
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                    putTextElement(document, 'GroupNumber1', invoice.Name.replaceAll('[^0-9]', ''));
                } else {
                    // Should not group Credit Notes, so the only item is related to the generating Credit Note, which points to the reimbursed invoice
                    Credit_Note__c cn = invoiceGroups[0].Invoice_Items__r[0].Credit_Note__r;
                    if(cn != null && cn.Reimbursed_Invoice__r !=null) {
                    	putTextElement(document, 'GroupNumber1', cn.Reimbursed_Invoice__r.Name.replaceAll('[^0-9]', ''));
                    } else {
                        // TODO: Appropriate error. Check also invoice total >= cn total
                    	throw new AbacusHelper.AbacusHelperException('Credit note without reference to Invoice', ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                    }
                }
                putTextElement(document, 'GroupNumber2', '0');
                putTextElement(document, 'GroupNumber3', '0');
                putTextElement(document, 'PaymentCentre', '0');
                putTextElement(document, 'PaymentOrderProcedure', ConstantsUtil.ABACUS_PAYMENT_ORDER_PROCEDURE);
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                	putTextElement(document, 'PaymentReferenceLineType', ConstantsUtil.ABACUS_PAYMENT_REFERENCE_LINE_TYPE);
                	putTextElement(document, 'PaymentReferenceLine', invoice.Payment_Reference__c);
                	putTextElement(document, 'CollectiveAccount', ConstantsUtil.ABACUS_COLLECTIVE_ACCOUNT);
                }
                
                integer lineCount = 1;
                Dom.XmlNode extendedFields;
                for(Invoice_Order__c invoiceGroup : invoiceGroups) {
                    for(Invoice_Item__c invoiceItem : invoiceGroup.Invoice_Items__r) {
                        // split per tax schedule periods. Need: amount (gross), tax, tax from/to (TaxHelper: split period)
                        List<TaxHelper.VatListItem> taxItems = TaxHelper.getVatItems(invoiceItem.Tax_Code__c, invoiceItem.Accrual_From__c, invoiceItem.Accrual_To__c, invoiceItem.Amount__c, Date.today());
                        decimal runTax = 0;
                        decimal runTotal = 0;
                        for(integer taxLine = 0; taxLine < taxItems.size(); taxLine++) {
                            TaxHelper.VatListItem taxItem = taxItems[taxLine];
                            decimal total = (taxItem.total * 100.0).round(RoundingMode.HALF_EVEN) / 100.0;
                            decimal tax = (taxItem.vat * 100.0).round(RoundingMode.HALF_EVEN) / 100.0;
                            // Last row absorbs rounding errors
                            if(taxLine == taxItems.size() -1 ) {
                                total = invoiceItem.Total__c - runTotal;
                                tax = invoiceItem.Tax__c - runTax;
                            } else {
                                runTotal += total;
                             	runTax += tax;   
                            }                        
                            Dom.XmlNode line = document.addChildElement('LineItem', abaDocTypeNS, '');
                            line.setAttribute('mode', 'SAVE');
                            putTextElement(line, 'Number', lineCount /* invoiceItem.Line_Number__c *///);
        					/******************
                            putTextElement(line, 'Amount', total);
                            putTextElement(line, 'KeyAmount', total);
                            putTextElement(line, 'CreditAccount', invoiceItem.Credit_Account__c);
                            // Phase 2
                            /*  
                            line.addChildElement('Project', abaDocTypeNS, '').addTextNode('Phone Book Entry');
                            */
      					    /******************
                            putTextElement(line, 'CreditCostCentre1', invoiceItem.KSTCode__c);
                            putTextElement(line, 'CreditCostCentre2', invoiceItem.KTRCode__c);
    
                            // Tax code in product: 31 or 32 (High vat, Low vat). 30 for vat excluded
                            if(invoice.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_STANDARD) {
                                putTextElement(line, 'TaxCode', AbacusHelper.convertTaxCode(taxItem.vatCode));
                            } else if(invoice.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                                putTextElement(line, 'TaxCode', '30');
                            }
                            putTextElement(line, 'TaxIncluded', ConstantsUtil.ABACUS_TAX_INCLUDED);
                            putTextElement(line, 'TaxDateValidFrom', AbacusHelper.toISO8601String(taxItem.dateFrom));
                            putTextElement(line, 'TaxAmount', -tax);
                            putTextElement(line, 'KeyTaxAmount', -tax);
                            putTextElement(line, 'Text', invoiceItem.Description__c.mid(0,80));
                            if(taxItem.hasAccrual) {
                                putTextElement(line, 'AccrualNumber', ConstantsUtil.ABACUS_ACCRUAL_NUMBER);
                                putTextElement(line, 'AccrualFrom', AbacusHelper.toISO8601String(taxItem.dateFrom));
                                putTextElement(line, 'AccrualTo', AbacusHelper.toISO8601String(taxItem.dateTo));
                            }
                            
                            extendedFields = line.addChildElement('ExtendedFields', abaDocTypeNS, '');
                            if(invoiceItem.Product__r != null) {
                                putTextElement(extendedFields, 'StringData', invoiceItem.Product__r.ProductCode, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD6'});
                                putTextElement(extendedFields, 'StringData', invoiceItem.Product__r.Name, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD7'});
                            }
                            // putTextElement(extendedFields, 'StringData', 'Book Entry Edition', new Map<string, string> {'Name' => '_USERFIELD9'});
                            putTextElement(extendedFields, 'StringData', invoiceItem.Sales_Channel__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD10'});
                            if(invoiceGroup.Contract__r != null) {
                                putTextElement(extendedFields, 'StringData', invoiceGroup.Contract__r.ContractNumber, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD14'});
                            }
                            putTextElement(extendedFields, 'StringData', invoiceItem.Line_Number__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD15'});
                            if(invoiceItem.Subscription__r != null) {
                                putTextElement(extendedFields, 'StringData', AbacusHelper.toISO8601String(invoiceItem.Subscription__r.SBQQ__StartDate__c), abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD16'});
                            }                        
                            lineCount++;
                        }
                    }
                }
                
                if(invoice.Rounding__c != null && invoice.Rounding__c != 0) {
                    Dom.XmlNode line = document.addChildElement('LineItem', abaDocTypeNS, '');
                    line.setAttribute('mode', 'SAVE');
                    putTextElement(line, 'Number', lineCount);
                    putTextElement(line, 'Amount', invoice.Rounding__c);
                    putTextElement(line, 'KeyAmount', invoice.Rounding__c);
                    putTextElement(line, 'CreditAccount', ConstantsUtil.ABACUS_ROUNDING_CREDIT_ACCOUNT);
                    putTextElement(line, 'CreditCostCentre1', ConstantsUtil.ABACUS_ROUNDING_CREDIT_COST_CENTER);
                    putTextElement(line, 'TaxCode', '29');
                    putTextElement(line, 'TaxIncluded', ConstantsUtil.ABACUS_TAX_INCLUDED);
                    putTextElement(line, 'TaxDateValidFrom', AbacusHelper.toISO8601String(invoice.Invoice_Date__c));
                    putTextElement(line, 'TaxAmount', '0');
                    putTextElement(line, 'KeyTaxAmount', '0');
                    putTextElement(line, 'Text', ConstantsUtil.ABACUS_ROUNDING_DESCRIPTION);
                    extendedFields = line.addChildElement('ExtendedFields', abaDocTypeNS, '');
                    putTextElement(extendedFields, 'StringData', ConstantsUtil.ABACUS_ROUNDING_DESCRIPTION, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD7'});
                }
                Dom.XmlNode paymentTerm = document.addChildElement('PaymentTerm', abaDocTypeNS, '');
                paymentTerm.setAttribute('mode', 'SAVE');
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                    putTextElement(paymentTerm, 'Number', '60');
                } else if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_GUT) {
                    putTextElement(paymentTerm, 'Number', '1');
                }
                putTextElement(paymentTerm, 'CopyFromTable', 'true');
                putTextElement(paymentTerm, 'Type', '0');
                extendedFields = document.addChildElement('ExtendedFields', abaDocTypeNS, '');
                putTextElement(extendedFields, 'LongData', invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''), abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD14'});
                putTextElement(extendedFields, 'StringData', invoice.Customer__r.Name, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD15'});
                putTextElement(extendedFields, 'StringData', invoice.Billing_Profile__r.Billing_Street__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD16'});
                // putTextElement(extendedFields, 'StringData', invoice.Billing_Profile__r.Billing_State__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD17'});
                putTextElement(extendedFields, 'StringData', invoice.Billing_Profile__r.Billing_Country__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD18'});
                putTextElement(extendedFields, 'StringData', invoice.Billing_Profile__r.Billing_Postal_Code__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD19'});
                putTextElement(extendedFields, 'StringData', invoice.Billing_Profile__r.Billing_City__c, abaTypesNS, new Map<string, string> {'Name' => '_USERFIELD20'});
					
                HttpRequest req = new HttpRequest();
                req.setEndpoint(BillingHelper.abaSaveEndpoint);
                req.setMethod('POST');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, invoice.Correlation_Id__c);
                req.setHeader(ConstantsUtil.TRACE_HEAD_BP_NAME, 'SendToAbacus');
                req.setHeader(ConstantsUtil.TRACE_HEAD_INITIATOR, 'Salesforce');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CALLING_APP, 'Billing');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_NAME, 'Invoice__c');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_ID, invoice.Id);
                req.setHeader('Content-Type', 'text/xml');
                req.setHeader('SOAPAction', 'save');
System.debug(doc.toXmlString());                
                req.setBodyDocument(doc);
                HttpResponse res = httpClient.send(req);
                
                integer statusCode = res.getStatusCode();
                
                if(statusCode == 200) {
                    Dom.Document resDoc = AbacusHelper.getDocumentFromMultipartMime(res);
                    envelope = resDoc.getRootElement();
                    Dom.XmlNode xmlBody = envelope.getChildElement('Body', soapNS);
                    AbacusHelper.AbacusHelperException fault = AbacusHelper.checkFaults(xmlBody);
                    if(fault != null) {
                        
                        if(fault.FaultType == AbacusHelper.FaultType.ABACUS || fault.FaultCode == '8040') {
                            // seems the error when a task is pending. Future post will fail until job completed and retrieved
                            System.abortJob(context.getJobId());
                        }
						
                        throw fault;
                    }
                    Dom.XmlNode responseMessage = xmlBody.getChildElement('SaveResponse', abaDocNS)
                        .getChildElement('ResponseMessage', abaDocNS);
                    string isFinished = responseMessage.getChildElement('IsFinished', abaTypesNS).getText();
                    if(isFinished == 'false') {
                        string abaRequestId = responseMessage.getChildElement('RequestID', abaTypesNS).getText();
                       	// TODO: pause job until got response for "abaRequestId" Job. Timeout after 30 min., then logout/logon
                    } 
                    // TODO: get some confirmation
                } else {
                    /* Handle other specific errors if necessary */
     			    /***************
                    // If aba token expired, System.enqueueJob(new AbacusLogin());
                    throw new AbacusHelper.AbacusHelperException('Abacus save action returned ' + statusCode, ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                }                
                invoiceUpdates.add(new Invoice__c(Id=invoice.Id, Registered__c=true));
                errors.add(ErrorHandler.createLogInfo('Billing', 'Billing.sendToAbacus', 'Bill sent to Abacus', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));
            } catch (Exception ex) {
                errors.add(ErrorHandler.createLogError('Billing', 'Billing.sendToAbacus', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice));
            }
		}
       
        

        Savepoint savePt;
        if(!Test.isRunningTest()) { savePt = Database.setSavepoint(); }
        try {
            if(invoiceUpdates!=null && invoiceUpdates.size() > 0) {
            	update invoiceUpdates;
            }
            if(errors.size()> 0) {
                insert errors;
            }
        } catch (Exception ex) {
            if(!Test.isRunningTest()) { Database.rollback(savePt); }
            ErrorHandler.log(System.LoggingLevel.ERROR, 'Billing', 'Billing.sendToAbacus', ex, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);                
        }     
		*****************************************************************************************************************+*****/
        
        
    }
	
    public void finish(Database.BatchableContext context) {
		//if(!Test.isRunningTest()) AbacusHelper.abacusLogout(abaToken);
    }
    /*********************************************************************************************************
    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content) {
        if(parent == null || string.isBlank(elementName)) {
            return null;
        }        
        return putTextElement(parent, elementName, content, parent.getNamespace());
    }

    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content, string namespace) {
    	return putTextElement(parent, elementName, content, namespace, null);
    }
    
    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content, string namespace, Map<string, string> attributes) {
        if(parent == null || string.isBlank(elementName)) {
            return null;
        }
        string text;
        if(content == null) {
            text = '';
        } else {
          	text = string.valueOf(content);
        }
        Dom.XmlNode child = parent.addChildElement(elementName, namespace, parent.getPrefixFor(namespace));
		child.addTextNode(text);
        if(attributes != null && attributes.size() > 0) {
            for(string key : attributes.keySet()) {
                child.setAttribute(key, attributes.get(key));
            }
        }
        return child;
    }**************************************************************************************************************/
}