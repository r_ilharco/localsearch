public class TrialActivationManager {

    public static void sendTrialRequest(List<SBQQ__Quote__c> quotes, List<String> setQLId) {
		
        Set<String> setQuoteLinesId = new Set<String>(setQLId);
        
        List<SBQQ__QuoteLine__c> listQLinesToUpdate = new List<SBQQ__QuoteLine__c>();
        Set<String> queryFields = new Set<String>{  'Id','SBQQ__Quote__c','SBQQ__RequiredBy__c','SBQQ__Quantity__c','TrialEndDate__c',
                                                    'SBQQ__Product__r.ProductCode','SBQQ__Product__c','TrialStartDate__c',
                                                    'Place__c','Place__r.PlaceID__c','SBQQ__RequiredBy__r.Place__c',
                                                    'SBQQ__RequiredBy__r.Place__r.PlaceID__c', 'External_Id__c'   
                                                };
        Set<String> productCodes = new Set<String>();

        Map<Id,Set<SBQQ__QuoteLine__c>> quoteToQuoteLines = new Map<Id,Set<SBQQ__QuoteLine__c>>();
        Map<String, List<Product_Activation_JSON__mdt>> productToJsonConfigMdt = new Map<String, List<Product_Activation_JSON__mdt>>();
        
        Set<Id> quoteIds = new Set<Id>();
        for(SBQQ__Quote__c currentQuote : quotes){
            quoteIds.add(currentQuote.Id);
        }
        if(!quoteIds.isEmpty()){
            List<SBQQ__QuoteLine__c> quoteLines = SEL_SBQQQuoteLine.getQuoteLinesById(setQuoteLinesId, queryFields);
            for(SBQQ__QuoteLine__c currentQL : quoteLines){
                productCodes.add(currentQL.SBQQ__Product__r.ProductCode);
            }
        } 

        if(!productCodes.isEmpty()){
            
            List<Product_Activation_JSON__mdt> productAdditionalAttributes = SRV_OrderUtil.getJsonConfigMetadata(productCodes);
        
            for(Product_Activation_JSON__mdt currentMdt : productAdditionalAttributes){
                SRV_TrialActivationUtil.generateListMdtValueMap(productToJsonConfigMdt, currentMdt.Product_Code__c, currentMdt);
                SRV_TrialActivationUtil.addQueryFields(currentMdt,queryFields);
            }

            List<SBQQ__QuoteLine__c> quoteLines = SEL_SBQQQuoteLine.getQuoteLinesById(setQuoteLinesId,queryFields);
            listQLinesToUpdate = SRV_TrialActivationUtil.setTrialOnQuoteLines(quoteLines);
            for(SBQQ__QuoteLine__c currentQL : quoteLines){
                if(!quoteToQuoteLines.containsKey(currentQL.SBQQ__Quote__c)){
                    quoteToQuoteLines.put(currentQL.SBQQ__Quote__c, new Set<SBQQ__QuoteLine__c>{currentQL});
                }
                else{
                    quoteToQuoteLines.get(currentQL.SBQQ__Quote__c).add(currentQL);
                }
            }
            if(!quoteToQuoteLines.isEmpty()){
                for(SBQQ__Quote__c currentQuote : quotes){
                    SRV_TrialActivationUtil.QuoteTrialActivationPayload quoteTrialPayload = SRV_TrialActivationUtil.getTrialJSON(currentQuote, quoteToQuoteLines.get(currentQuote.Id), productToJsonConfigMdt);
                    String jsonBody = JSON.serialize(quoteTrialPayload);
                    
                    SRV_TrialActivationUtil.sendTrialInformation(jsonBody, currentQuote.Id, currentQuote.Trial_CallId__c);
                }

                if(!listQLinesToUpdate.isEmpty()) update listQLinesToUpdate;
            }
        }  
    }
}