/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date		   : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : This class contains some useful methods for AbacusTransferJob: The implemented methods deal with the calculation logic that concerns InvoiceOrder
 * Changelog   : 
 */

public class SRV_InvoiceOrder {
  
    public static Map<Id, List<Invoice_Order__c>> setMap (List<Invoice_Order__c> invoiceOrders) {
        // Build a map for lower levels, instead of calling SOQL for each invoice (2 level subqueries aren't allowed)
        Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = new Map<Id, List<Invoice_Order__c>>();
        for(Invoice_Order__c invoiceOrder : invoiceOrders) {
            List<Invoice_Order__c> invoiceOrderList;
            if(invoiceOrdersMap.containsKey(invoiceOrder.Invoice__c)) {
                invoiceOrderList = invoiceOrdersMap.get(invoiceOrder.Invoice__c);
            } else {
                invoiceOrderList = new List<Invoice_Order__c>();
                invoiceOrdersMap.put(invoiceOrder.Invoice__c, invoiceOrderList);                
            }
            invoiceOrderList.Add(invoiceOrder);
        }
        return invoiceOrdersMap;
     }
    
}