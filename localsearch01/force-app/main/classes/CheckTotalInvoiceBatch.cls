public without sharing class CheckTotalInvoiceBatch implements Database.Batchable<SObject>{
    
    public Database.QueryLocator start(Database.BatchableContext batchableContext){
        List<String> invoiceStatusList = new List<String>{'Collected'};
        String query = 'SELECT Id, Invoice_wrongly_generated__c FROM Invoice__c WHERE Status__c IN :invoiceStatusList';
        return Database.getQueryLocator(query);
                                     
    }

    public void execute(Database.BatchableContext batchableContext, List<Invoice__c> scope) {
        CheckTotalInvoiceHandler.processRecords(scope);
    }

    public void finish(Database.BatchableContext batchableContext){
        
    }
}