public class BatchUpdateQuotesXactly implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    private List<Id> contractIds = new List<Id>();
    private integer limitContracts = -1;
    private List<String> staticValues = new List<String>();
    
    public BatchUpdateQuotesXactly(){}
    public BatchUpdateQuotesXactly(List<String> val){ this(val, 10000); }
    public BatchUpdateQuotesXactly(List<String> val, integer limits){
        this.staticValues = val;
        this.limitContracts = limits;
    }
    public BatchUpdateQuotesXactly(integer limitContracts){ this.limitContracts = limitContracts; }
    public BatchUpdateQuotesXactly(List<Id> contractIds){ this.contractIds = contractIds; }
    
    public database.querylocator start(Database.BatchableContext bc) {
        List<Id> ids = this.contractIds;
        string query = ' select AccountId,Account.OwnerId, CustomerSignedDate, OwnerAtAutorenewal__c, OwnerOnSignature__c, SBQQ__Quote__c, SBQQ__Quote__r.Contract_Signed_Date__c, SBQQ__Quote__r.SBQQ__Account__c from Contract where Status not in (\'' + ConstantsUtil.CONTRACT_STATUS_CANCELLED + '\',\'' + ConstantsUtil.CONTRACT_STATUS_EXPIRED + '\',\'' + ConstantsUtil.CONTRACT_STATUS_TERMINATED + '\' )';
        query 		+= ' and SBQQ__Quote__c != null and SBQQ__Quote__r.AccountOwner__c = null ';
        query		+= ids.isEmpty()?'':' and id in :ids ';
        query		+= staticValues.isEmpty()?'':' and Account.Cluster_Id__c in: staticValues'; //use account to group contracts by account
        query		+= limitContracts > 0?' limit '+limitContracts:'';
        system.debug('Query ::'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Contract> scope){
        system.debug('scope :: '+scope.size());
        if(scope.isEmpty()) return;
        
        //colect AccountIds to grab history Account
        set<id> AccountIds = new Set<Id>();
        set<id> quoteIds = new set<id>();
        set<id> contractIds = new set<id>();
        Map<id,Contract> quoteContract = new Map<id,Contract>(); // map quoteid to contract
        for(Contract c : scope) {
            AccountIds.add(c.AccountId);
            quoteIds.add(c.SBQQ__Quote__c);
            contractIds.add(c.id);
            quoteContract.put(c.SBQQ__Quote__c, c);
            system.debug('contractID: '+c.Id+ ' QuoteId: '+c.SBQQ__Quote__c+' AccountId: '+c.AccountId);
        }
        
        
        AggregateResult[] groupedResults = [select Order.Contract__c cc , max(ServiceDate) sd
                                            from OrderItem  
                                            where Order.Contract__c in :contractIds and Order.Type = 'Implicit Renewal' and order.Status = 'Activated' and SBQQ__RequiredBy__c = null
                                            group by Order.Contract__c];
        
        map<string, date> renewal = new map<string, date>();
        for (AggregateResult ar : groupedResults)  { renewal.put(string.valueOf(ar.get('cc')), Date.valueOf(ar.get('sd')));}
        
        system.debug('renewal: '+renewal);
        
        map<id, list<AccountHistory>> owMap = new map<id, list<AccountHistory>>();
        for(AccountHistory ah : [select AccountId, CreatedDate, Field, OldValue, NewValue from AccountHistory where Field in ('Owner') and AccountId in :AccountIds  order by AccountId, Field, CreatedDate]){
            string oldVal = string.valueOf(ah.OldValue);
            string newVal = string.valueOf(ah.NewValue);
            if(ah.Field.equals('Owner') && ((string.isNotBlank(oldVal) && oldVal.startsWith('005')) || (string.isNotBlank(newVal) && newVal.startsWith('005')))){
                list<AccountHistory> l = owMap.get(ah.AccountId);
                if(l == null) l = new list<AccountHistory>();
                l.add(ah);
                owMap.put(ah.AccountId, l);
            }
        }
        
        system.debug('AccountHistory: '+owMap);
        
        Map<id,SBQQ__Quote__c> quotesToUpdate = new Map<id,SBQQ__Quote__c>();
        List<Contract> contractsUpdate = new List<Contract>();
        for(SBQQ__Quote__c sub : [select id, Contract_Signed_Date__c, AccountOwner__c, SBQQ__Account__r.OwnerId from SBQQ__Quote__c where id in :quoteIds]) {
            if(sub.Contract_Signed_Date__c == null){
                sub.Contract_Signed_Date__c = quoteContract.get(sub.Id).CustomerSignedDate;
            }
            datetime signatureDate = sub.Contract_Signed_Date__c;
            system.debug('signatureDate: '+signatureDate+' quoteid: '+sub.id);
            if(signatureDate != null){
                sub.AccountOwner__c = grabByDate(owMap.get(sub.SBQQ__Account__c),signatureDate, sub.SBQQ__Account__r.OwnerId);
                quotesToUpdate.put(sub.id, sub);
                system.debug('AccountOwner__c: '+sub.AccountOwner__c);
            }
        }
        
        system.debug('quotesToUpdate: '+quotesToUpdate);
        if(!quotesToUpdate.isEmpty()){
            QuoteTriggerHandler.disableTrigger = true;
            update quotesToUpdate.values();
            QuoteTriggerHandler.disableTrigger = false;
        }
        
        for(Contract c : scope) {
            if(quotesToUpdate.containsKey(c.SBQQ__Quote__c) && c.OwnerOnSignature__c == null){
                c.OwnerOnSignature__c = quotesToUpdate.get(c.SBQQ__Quote__c).AccountOwner__c;
            }
            if(renewal.containsKey(c.Id) && c.OwnerAtAutorenewal__c == null){
                c.OwnerAtAutorenewal__c = grabByDate(owMap.get(c.AccountId), renewal.get(c.Id), c.Account.OwnerId);
            }
            contractsUpdate.add(c);
            system.debug('Contract: '+c.Id+' OwnerOnSignature__c: '+c.OwnerOnSignature__c+' OwnerAtAutorenewal__c:'+c.OwnerAtAutorenewal__c);
        }   
        
        system.debug('contractsUpdate: '+contractsUpdate);
        if(!contractsUpdate.isEmpty()){
            ContractTriggerHandler.disableTrigger = true;
            update contractsUpdate;
            ContractTriggerHandler.disableTrigger = false;
        }
    }
    
    public static string grabByDate(list<AccountHistory> l, datetime signatureDate, string defaultval){
         system.debug('signatureDate::'+signatureDate);
        if(signatureDate != null && l != null && !l.isEmpty()){
            AccountHistory beforeSignature = null;
            AccountHistory afterSignature = null;
            for(AccountHistory a: l){
                if(DateOnly(signatureDate) >= DateOnly(a.CreatedDate) && (beforeSignature == null || beforeSignature.CreatedDate < a.CreatedDate)){beforeSignature = a; } 
                if(DateOnly(signatureDate) < DateOnly(a.CreatedDate) && (afterSignature == null || afterSignature.CreatedDate > a.CreatedDate)){ afterSignature = a; }
            }
            if(beforeSignature != null /*&& string.isNotBlank(beforeSignature.NewValue)*/){ return string.valueOf(beforeSignature.NewValue);
            } else if(afterSignature != null){ return string.valueOf(afterSignature.OldValue); }
        }
        return defaultval;
    }
    
    private static Date DateOnly(Datetime mydate){
        return Date.newInstance(mydate.year(), mydate.month(), mydate.day());
    }
    
    public void finish(Database.BatchableContext bc){}  
    
    public void execute(SchedulableContext sc) { database.executebatch(this, 200); }
    
    /*public static String sched = '0 0 1 * * ?';
    public static string scheduleMe() {
        string name = 'BatchUpdateQuotesXactly';
        if (Test.isRunningTest() ) { name = name + system.now(); }
        return scheduleMe(name, sched) ;      
    }
    
    public static string scheduleMe(string name,String schedule) {
        return System.schedule(name, schedule, new BatchUpdateQuotesXactly());       
    }*/
}