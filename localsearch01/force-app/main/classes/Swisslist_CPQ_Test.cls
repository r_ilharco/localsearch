@isTest
private class Swisslist_CPQ_Test {

    @isTest
    static void SwisslistCPQconfigTest() {

        Test.startTest();

        Swisslist_CPQ.config();

        List <Product2> lstProd =
            [SELECT Id                              ,
                    AutomaticRenew__c               ,
                    Family                          ,
                    IsActive                        ,
                    KTRCode__c                      ,
                    KTRDesignation__c               ,
                    L1_Business__c                  ,
                    L2_ProductRange__c              ,
                    L3_ProductLine__c               ,
                    MultiplePlaceIDAllowed__c       ,
                    Name                            ,
                    PlaceIDRequired__c              ,
                    ProductCode                     ,
                    SBQQ__Component__c              ,
                    SBQQ__ConfigurationEvent__c     ,
                    SBQQ__ConfigurationType__c      ,
                    SBQQ__DefaultQuantity__c        ,
                    SBQQ__NonDiscountable__c        ,
                    SBQQ__Optional__c               ,
                    SBQQ__OptionSelectionMethod__c  ,
                    SBQQ__PriceEditable__c          ,
                    SBQQ__PricingMethodEditable__c  ,
                    SBQQ__PricingMethod__c          ,
                    SBQQ__QuantityEditable__c       ,
                    SBQQ__SubscriptionBase__c       ,
                    SBQQ__SubscriptionPricing__c    ,
                    SBQQ__SubscriptionTerm__c       ,
                    SBQQ__SubscriptionType__c       ,
                    SBQQ__BillingFrequency__c       ,
                    KSTCode__c                      ,
                    SBQQ__TaxCode__c
               FROM Product2
              WHERE ProductCode IN ('SLT001',
                                    'OPMBASICLOW001',
                                    'OPMBASIC001'   ,
                                    'SLS001'        )];

        System.assertEquals(4,lstProd.size());

        List <PricebookEntry> lstPriceEntry =
            [SELECT IsActive        ,
                    Pricebook2Id    ,
                    Product2Id      ,
                    UnitPrice       ,
                    UseStandardPrice
               FROM PricebookEntry
              WHERE Product2Id IN (:lstProd[0].Id,   // 'SLT001'
                                   :lstProd[3].Id)]; // 'SLS001'

        System.assertEquals(2,lstPriceEntry.size());

        List <SBQQ__ProductFeature__c> lstProductFeacture =
            [SELECT Id                     ,
                    Name                   ,
                    SBQQ__ConfiguredSKU__c ,
                    SBQQ__MaxOptionCount__c,
                    SBQQ__MinOptionCount__c,
                    SBQQ__Number__c
               FROM SBQQ__ProductFeature__c
              WHERE SBQQ__ConfiguredSKU__c IN (:lstProd[0].Id, // 'SLT001'
                                               :lstProd[3].Id) // 'SLS001'
                AND Name = 'Region Reached'];

        System.assertEquals(2,lstProductFeacture.size());

        List <SBQQ__ProductOption__c> lstProductOption =
            [SELECT SBQQ__Bundled__c         ,
                    SBQQ__ConfiguredSKU__c   ,
                    SBQQ__Feature__c         ,
                    SBQQ__MaxQuantity__c     ,
                    SBQQ__MinQuantity__c     ,
                    SBQQ__Number__c          ,
                    SBQQ__OptionalSKU__c     ,
                    SBQQ__QuantityEditable__c,
                    SBQQ__Quantity__c        ,
                    SBQQ__Required__c        ,
                    SBQQ__Selected__c        ,
                    SBQQ__Type__c
               FROM SBQQ__ProductOption__c
              WHERE SBQQ__ConfiguredSKU__c IN (:lstProd[0].Id,      // 'SLT001'
                                               :lstProd[3].Id)      // 'SLS001'
                AND SBQQ__Feature__c IN (:lstProductFeacture[0].Id, // 'SLT001' and 'Region Reached'
                                         :lstProductFeacture[1].Id) // 'SLS001' and 'Region Reached'
                AND SBQQ__OptionalSKU__c IN (:lstProd[1].Id,        // 'OPMBASICLOW001'
                                             :lstProd[2].Id)];      // 'OPMBASIC001'

        System.assertEquals(2,lstProductOption.size());

        List <Product_Replacement__c> lstReplace =
            [SELECT Base_Product__c   ,
                    Updated_product__c
               FROM Product_Replacement__c
              WHERE Base_Product__c = :lstProd[0].Id      // 'SLT001'
                AND Updated_product__c = :lstProd[3].Id]; // 'SLS001'

        System.assertEquals(1,lstReplace.size());

        Test.stopTest();
    }

    @isTest
    static void SwisslistCPQdelTest() {

        Test.startTest();

        Swisslist_CPQ.config();
        Swisslist_CPQ.del();

        List<Product2> lstProd = [select Id from Product2];
        System.assertEquals(0,lstProd.size());

        List<PricebookEntry> lstPriceEntry = [select Id from PricebookEntry];
        System.assertEquals(0,lstPriceEntry.size());

        List<SBQQ__ProductFeature__c> lstProductFeacture = [select Id from SBQQ__ProductFeature__c];
        System.assertEquals(0,lstProductFeacture.size());

        List<SBQQ__ProductOption__c> lstProductOption = [select Id from SBQQ__ProductOption__c];
        System.assertEquals(0,lstProductOption.size());

        List<Product_Replacement__c> lstReplace = [select Id from Product_Replacement__c];
        System.assertEquals(0,lstReplace.size());

        Test.stopTest();
    }
}