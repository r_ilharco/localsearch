global class migraBPContact implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    private Map<ID, List<SBQQ__Quote__c>> MapAccQuote;
    public string batch;
    
    public migraBPContact(string batchName){
        MapAccQuote = new Map<ID, List<SBQQ__Quote__c>>();
        batch = batchName;

        List<SBQQ__Quote__c> lstQuote = new List<SBQQ__Quote__c>();
		lstQuote = [select id,SBQQ__Account__c,Billing_Profile__c  from SBQQ__Quote__c where Billing_Profile__c = null and Quote_Migration_Batch__c =: batch];
        
        for(SBQQ__Quote__c q: lstQuote){
            if(q.SBQQ__Account__c != null){
                if(MapAccQuote.get(q.SBQQ__Account__c) == null){         
                    List<SBQQ__Quote__c> lstq = new List<SBQQ__Quote__c>();
                    lstq.add(q);
                    MapAccQuote.put(q.SBQQ__Account__c, lstq);
                } else {
                    List<SBQQ__Quote__c> lstq = MapAccQuote.get(q.SBQQ__Account__c);
                    lstq.add(q);                
                }
            }
        }
        
    }


    global Database.QueryLocator start(Database.BatchableContext BC) { //Id='a0q1X000000jBzzQAE' AND  
		return Database.getQueryLocator([select Customer__c,Billing_Contact__c from Billing_Profile__c]);
	}
    
	global void execute(Database.BatchableContext BC, List<Billing_Profile__c> scope) {
        List<SBQQ__Quote__c> lstQ = new List<SBQQ__Quote__c>();
               
        for(Billing_Profile__c bp : scope) {
			
            if(MapAccQuote.get(bp.Customer__c) != null){
                List<SBQQ__Quote__c> quotes = MapAccQuote.get(bp.Customer__c);
                for(SBQQ__Quote__c q : quotes) {
                    q.Billing_Profile__c = bp.id;
                    if(bp.Billing_Contact__c != null){
                        q.SBQQ__PrimaryContact__c = bp.Billing_Contact__c;
                    }
                    lstQ.add(q);
                }
                
            }   
        }
        
       update lstQ;
     
    }   
    
   	global void finish(Database.BatchableContext BC) {
        
    }
    
}