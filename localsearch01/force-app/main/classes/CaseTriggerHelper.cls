/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2020-02-05
 * Sprint      : 
 * Work item   : BUG-115
 * Testclass   :
 * Package     : 
 * Description :
 * Changelog   : 
 */


public class CaseTriggerHelper {
    /*ldimartino, BUG 115 - 05/05/2020, START*/
    public static void pendingStatusChangedCheck(Map<Id, Case> oldCasesMap, Map<Id, Case> newCasesMap){
        for(Case oldCase : oldCasesMap.values()){
            if(oldCase.Status == 'Pending' && oldCase.Status != newCasesMap.get(oldCase.Id).Status){
                newCasesMap.get(oldCase.Id).Pending_Reason__c = '';
            }
        }
    }
    /*ldimartino, BUG 115 - 05/05/2020, END*/
    
    public static void forceTriggerAssigmentRules(Map<Id, Case> newCasesMap){
        List<Case> lstCase = new List<Case>();
        for(Case nCase : newCasesMap.values()){
            if(nCase.Run_Assignment__c == true && (nCase.Origin == 'Manually' || nCase.Origin =='Manual')){
                lstCase.add(nCase);
                
            }
        }
        
        //To force Assigment Rules needs to be in different transaction
        List<Case> cases = new List<Case>();
        for(Case c : [Select Id from Case where Id in :lstCase])
        {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            c.setOptions(dmo);
            cases.add(c);
        }

        Database.upsert(cases);
    }
    
    /*ldimartino, SFRO-159 START*/
    public static void duplicateAccountsCheck(Map<Id, Case> oldCasesMap, Map<Id, Case> newCasesMap){
        Set<Id> accIds = new Set<Id>();
        Map<Id, Id> caseToAccount = new Map<Id, Id>();
        Map<Id, Boolean> accountToDuplicate = new Map<Id, Boolean>();
        Set<Id> caseIds = oldCasesMap.keySet();
        Map<Id,Case> cases = new Map<Id,Case>([SELECT Id, RecordType.Name FROM Case WHERE Id IN :caseIds]);
        //select id, Legal_City__c, Legal_Entity__c, Legal_Street__c, Company_Name__c, RecordType.DeveloperName, Status, Closure_Reason__c, AccountId from case
        for(Id cId : caseIds){
            if(oldCasesMap.get(cId).Status != newCasesMap.get(cId).Status && newCasesMap.get(cId).Status == 'Closed' && newCasesMap.get(cId).Closure_Reason__c == 'Solved' 
               && newCasesMap.get(cId).AccountId != null && cases.get(cId).RecordType.Name == ConstantsUtil.CASE_RECORD_TYPE_CHANGE_CUSTOMER_DATA){
                accIds.add(newCasesMap.get(cId).AccountId);
                caseToAccount.put(cId, newCasesMap.get(cId).AccountId);
            }
        }
        Map<Id, Account> acctList = new Map<Id, Account>([SELECT Id, Name, BillingCity, LegalEntity__c, BillingStreet ,UID__c 
                                                          FROM Account 
                                                          WHERE Id IN :accIds]);
        
        for(Id caseId : caseToAccount.keySet()){
            if(String.isNotBlank(newCasesMap.get(caseId).Legal_City__c)) acctList.get(caseToAccount.get(caseId)).BillingCity = newCasesMap.get(caseId).Legal_City__c;
            
            if(String.isNotBlank(newCasesMap.get(caseId).Legal_Entity__c)) acctList.get(caseToAccount.get(caseId)).LegalEntity__c = newCasesMap.get(caseId).Legal_Entity__c;
            
            if(String.isNotBlank(newCasesMap.get(caseId).Legal_Street__c)) acctList.get(caseToAccount.get(caseId)).BillingStreet = newCasesMap.get(caseId).Legal_Street__c;
            
            if(String.isNotBlank(newCasesMap.get(caseId).Company_Name__c)) acctList.get(caseToAccount.get(caseId)).Name = newCasesMap.get(caseId).Company_Name__c;
            
        }
        Integer index = 0;
        if(acctList.values().size() > 0){
            system.debug('acctList: ' + acctList.values());
            Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(acctList.values());
            for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                    system.debug('dupeResult: ' + dupeResult);
                    for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                        system.debug('matchResult: ' + matchResult);
                        if(matchResult.getSize()>0 /*&& String.valueOf(dupeResult).containsIgnoreCase('block')*/){
                            system.debug('matchResult.getSize: ' + matchResult.getSize());
                            accountToDuplicate.put(acctList.values().get(index).Id, false);
                        }
                    }
                }
                index++;
            }
        }
        if(accountToDuplicate.values().size() > 0){
            for(Id cId : caseIds){
                if(accountToDuplicate.containsKey(caseToAccount.get(cId))){
                    if(!Test.isRunningTest())newCasesMap.get(cId).addError('The information you are updating on the Account will create a duplicate');
                }
            }
        }
    }
    /*ldimartino, SFRO-159 END*/
}