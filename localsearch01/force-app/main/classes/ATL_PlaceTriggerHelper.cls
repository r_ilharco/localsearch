public without sharing class ATL_PlaceTriggerHelper{

    public static void managePlace_Lead (List<Place__c> newPlace,Map<ID, Place__c> oldPlaceMap){
        try{
            System.debug('Inside Trigger Action Helper');
            
            List<Place__c> lstPlace = new List<Place__c>();
            //Boolean isUpdate = false;

            //Vincenzo Laudato 2019-06-10 *** START
            //Queue Management
            Id queueId;
            List<Group> queueIds = [ SELECT Id 
                                FROM Group 
                                WHERE DeveloperName = :ConstantsUtil.TELESALES_PLACEDB_QUEUE 
                                AND Type = 'Queue' 
                                LIMIT 1];

            if(queueIds.isEmpty()) throw new NoQueueException('No Queue found');
            else queueId = queueIds[0].Id;

            //Map used to create a new lead and associate it to the correct place
            Map<Id, Place__c> id2Place = new Map<Id,Place__c>();

            //Maps used to check if there are existing leads or account with the GoldenRecordId given in input
            Map<String, Id> goldenRecordId2AccountId = new Map<String,Id>();
            Map<String, Id> goldenRecordId2LeadId = new Map<String,Id>();

            Set<String> newPlacesGoldenRecordId = new Set<String>();
            for(Place__c currentPlace : newPlace){
                //Vincenzo Laudato AS-76 *** START
                if(String.isNotBlank(currentPlace.GoldenRecordId__c) && String.isBlank(currentPlace.Account__c) && String.isBlank(currentPlace.Lead__c))
                    newPlacesGoldenRecordId.add(currentPlace.GoldenRecordId__c);
            }
            if(!newPlacesGoldenRecordId.isEmpty()){
                List<Account> accounts = [  SELECT Id, GoldenRecordId__c FROM Account WHERE GoldenRecordId__c IN :newPlacesGoldenRecordId];
            
                List<Lead> leads = [SELECT Id, GoldenRecordId__c FROM Lead WHERE GoldenRecordId__c IN :newPlacesGoldenRecordId];

                for(Account currentAccount : accounts){
                    goldenRecordId2AccountId.put(currentAccount.GoldenRecordId__c, currentAccount.Id);
                }
                
                for(Lead currentLead : leads){
                    goldenRecordId2LeadId.put(currentLead.GoldenRecordId__c, currentLead.Id);
                }
                //Vincenzo Laudato 2019-06-10 *** END
            }
            //Vincenzo Laudato AS-76 *** END
            
            for(Place__c pl : newPlace) {
                System.Debug('Place Id '+ pl.Id);            
                System.Debug('Lead__c Id '+ pl.Lead__c);   
                System.Debug('Account__c Id '+ pl.Account__c);     
                Place__c placeToUpdate = new Place__c(Id = pl.Id);

                if(String.isNotBlank(pl.GoldenRecordID__c)){
                    //Vincenzo Laudato 2019-06-10 *** START
                    Id accountId = goldenRecordId2AccountId.get(pl.GoldenRecordId__c);
                    if(accountId != null){
                        System.Debug('Account Id '+ accountId);
                        placeToUpdate.Account__c = accountId;
                        lstPlace.add(placeToUpdate);
                        //isUpdate = true;
                    }
                    else{
                        Id leadId = goldenRecordId2LeadId.get(pl.GoldenRecordId__c);
                        if(leadId != null){
                            System.Debug('Lead Id '+ leadId);
                            placeToUpdate.Lead__c = leadId;
                            lstPlace.add(placeToUpdate);
                            //isUpdate = true;
                        }
                        else{ //When GoldenRecordID__c not matched with either Account/Lead then Create new lead and associate that lead to place
                            System.Debug('no Golden Rec found so will create a lead');
                            List<String> sources = new List<String>();
                            for(Place_Setting__mdt setting : [select id, Place_Source__c from Place_Setting__mdt]){
                                sources.add(setting.Place_Source__c);
                            }
                            if(sources.contains(pl.SubSource__c)){
                                 System.Debug(pl.SubSource__c +' so will create a lead');
                                 id2Place.put(pl.Id, pl);
                            }
                        }
                    }   
                }
                else{//When GoldenRecordID__c NULL then Create new lead and associate that lead to place
                    id2Place.put(pl.Id, pl);
                }
            }

            if(!id2Place.isEmpty()){
                upsertLeadAndUpdatePlace(id2Place, queueId);
            }

            if(!lstPlace.isEmpty()){
                update lstPlace;
            }
        }
        catch(NoQueueException noQueueException){
            System.debug(noQueueException.getMessage());
        }
        catch(Exception e){
            System.debug('Exception in ATL_PlaceTriggerHelper.managePlace_Lead '+e.getMessage());
        }
        //Vincenzo Laudato 2019-06-10 *** END
    }

    //Vincenzo Laudato 2019-06-10 *** START
    @TestVisible private static void upsertLeadAndUpdatePlace(Map<Id, Place__c> id2Place, Id queueId){

        Map<Id, Lead> placeId2Lead = new Map<Id, Lead>();

        try{
            for(Id currentPlaceId : id2Place.keySet()){
                Place__c place = id2Place.get(currentPlaceId);
                
                String placeType = place.Place_Type__c != null ? place.Place_Type__c : '';
                String placeStatus = place.Status__c != null ? place.Status__c : '';
                
                if( String.isBlank(place.Lead__c) && 
                    placeType != Label.Place_Type_private.toLowercase() && 
                    placeStatus != Label.Place_Status_disabled
                ){
                    Lead theLead = new Lead();
                    theLead.FirstName =  place.FirstName__c;
                    theLead.LastName = String.IsEmpty(place.LastName__c) ? place.Company__c : place.LastName__c;//pl.LastName__c;
                    theLead.City = place.City__c;
                    theLead.State = place.State__c;
                    theLead.Company = String.isEmpty(place.Company__c) ? 'unknown / unbekannt / inconnu / sconosciuto' : place.Company__c ;//pl.Company__c;
                    theLead.Street = place.Street__c;
                    theLead.PostalCode = place.PostalCode__c;
                    theLead.Country = place.Country__c;
                    theLead.GoldenRecordID__c = place.GoldenRecordID__c;
                    theLead.Title = place.Title__c;
                    theLead.Email = place.Email__c;
                    theLead.MobilePhone = place.MobilePhone__c;
                    theLead.Phone = place.Phone__c;
                    theLead.Website = place.Website__c;
                    theLead.LeadSource = 'PlaceDB';
                    //theLead.OwnerId = queueId;
                    theLead.P_O_Box__c = place.PO_Box__c;
                    theLead.P_O_Box_City__c = place.PO_Box_City__c;
                    theLead.P_O_Box_Zip_Postal_Code__c = place.PO_Box_Zip_Postal_Code__c;
                    // Mohammed Soliman 2019-10-04 *** START
                    theLead.Rating = ConstantsUtil.LEAD_RATING_COLD;
                    //Status is new by default 
                    placeId2Lead.put(place.Id, theLead);
                }
            }
            if(!placeId2Lead.isEmpty()){
                upsert placeId2Lead.values();
                List<Place__c> placesToUpdate = [SELECT Id, Lead__c FROM Place__c WHERE Id IN :placeId2Lead.keySet()];
                for(Place__c currentPlace : placesToUpdate){
                    String leadId = placeId2Lead.get(currentPlace.Id).Id;
                    currentPlace.Lead__c = leadId;
                }
                update placesToUpdate;
            }
        }
        catch(Exception e){
            System.debug('Exception in ATL_PLaceTriggerHelper.upsertLeadAndUpdatePlace: '+e.getMessage());
        }
    }
    //Vincenzo Laudato 2019-06-10 *** EN

    public static void checkPlace ( Map <Id, Place__c> newItems , Map<ID, Place__c> oldItems ) {

        Set<Id> changedPlaceIds = new Set<Id> ();

        for (ID id : newItems.keySet()) {
            if ( (newItems.get(id).New_Account__c != oldItems.get(id).New_Account__c) && newItems.get(id).New_Account__c != null ){

                newItems.get(id).Account_Changed__c = true;
                newItems.get(id).Lead_Changed__c = false;
                
                changedPlaceIds.add(id);
                
            System.debug('Account Changed ' + newItems.get(id).Account__c +  ' ->' + oldItems.get(id).Account__c);

            }
            else if ( (newItems.get(id).Lead__c != oldItems.get(id).Lead__c)
                     && newItems.get(id).Lead__c != null ){

                newItems.get(id).OldReference__c = oldItems.get(id).Lead__c;
                newItems.get(id).Lead_Old__c = oldItems.get(id).Lead__c;
                newItems.get(id).New_Lead__c = newItems.get(id).Lead__c;
                newItems.get(id).Lead_Changed__c = true;
                newItems.get(id).Account_Changed__c = false;
                changedPlaceIds.add(id);
                
                System.debug('Lead Changed ' + newItems.get(id).Lead__c +  ' -> ' + oldItems.get(id).Lead__c);

            }
        }  
        
        /*if(!changedPlaceIds.isEmpty()){
            Set<Id> placeNotValid = SRV_Place.checkQuoteAndContract(changedPlaceIds);
            
            if( !placeNotValid.isEmpty()) {
                
                System.debug('placeNot valid is not empty');
                
                for(Id i : placeNotValid){
                    System.debug ('newItems.get(i) ' + newItems.get(i));
                    newItems.get(i).addError(System.Label.addErrorCheckPlace);  
                }
            }
        }*/
    }
    
    
    public static void checkApproval (Map <Id, Place__c> newItems, Map <Id, Place__c> oldItems ) {
        
        for(Id i : newItems.keySet()) {
            
            if (newItems.get(i).Approval_Status__c != oldItems.get(i).Approval_Status__c  && newItems.get(i).Approval_Status__c == 'Rejected') {
                
                System.debug ('checkApproval, Approval Status Rejected');
                
                if (newItems.get(i).Account_Changed__c && !newItems.get(i).Lead_Changed__c) {
                    
                    newItems.get(i).Account_Changed__c = false;
                    newItems.get(i).Approval_Status__c = 'None';
                    //newItems.get(i).Account__c = newItems.get(i).New_Account__c ;
                    newItems.get(i).New_Account__c = null;
                    System.debug ('checkApproval, Account Changed');
                }
                else if (!newItems.get(i).Account_Changed__c && newItems.get(i).Lead_Changed__c) {
                    
                    newItems.get(i).Lead_Changed__c = false;
                    newItems.get(i).Approval_Status__c = 'None';
                    newItems.get(i).Lead__c = newItems.get(i).OldReference__c ;
                    newItems.get(i).OldReference__c = null;
                    System.debug ('checkApproval, Lead Changed');
                }
                
            }
            else if (newItems.get(i).Approval_Status__c != oldItems.get(i).Approval_Status__c  && newItems.get(i).Approval_Status__c == 'Approved') {
                if (newItems.get(i).Account_Changed__c && !newItems.get(i).Lead_Changed__c) {
                    newItems.get(i).Account__c = newItems.get(i).New_Account__c;
                }
                newItems.get(i).Approval_Status__c = 'None';
            }
                
        }
        
    }
    
    public static void placeCreationNotification(List<Place__c> newPlace){
        /*for(Place__c currentPlace : newPlace){
            if(currentPlace.OwnerId != null){
                String feedBody = Label.Chatter_Feed_New_Place.replace('%name%',currentPlace.Name);
				ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, currentPlace.OwnerId, ConnectApi.FeedElementType.FeedItem, feedBody);
            }
        }*/
   } 
        

    public class NoQueueException extends Exception {}
}