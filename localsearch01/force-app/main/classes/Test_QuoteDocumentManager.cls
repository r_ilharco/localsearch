@istest 
public class Test_QuoteDocumentManager {
 static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
    }
    
    @testsetup static void setup(){
        insertbypassflownames();
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        insert acc;
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
        insert cont;
        
        Pricebook2 pbDMC = new Pricebook2();
        pbDMC.ExternalId__c = 'PB2';
        pbDMC.Name = 'Telesales';
        pbDMC.IsActive = true;
        insert pbDMC;
        
        opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
        opp.Pricebook2Id = pbDMC.Id;
        insert opp;
        
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote.SBQQ__PriceBook__c = pbDMC.Id;
        insert quote;
        document doc = new document();
        doc.name = 'testname';
        doc.Body = Blob.valueOf('testbody');
        doc.FolderId = UserInfo.getUserId();
        insert doc;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'Telesales Template 2020 v1.0 - IT';
        insert template;
        
        SBQQ__QuoteDocument__c document = test_dataFactory.generateQuoteDocument(quote.id);
        document.SBQQ__QuoteTemplate__c = template.id;
        document.SBQQ__DocumentId__c = doc.id;
        insert document;
        
        SBQQ__QuoteDocument__c document1 = test_dataFactory.generateQuoteDocument(quote.id);
        document1.SBQQ__DocumentId__c = doc.id;
        document1.SBQQ__Template__c = template.id;
        insert document1;
        
        System.debug('--->document:' + JSON.serialize(document));
        System.debug('--->document1:' + JSON.serialize(document1));
        
        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = document.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        cdlList.add(cdl);
        
        ContentDocumentLink cdl2 = new ContentDocumentLink();
        cdl2.LinkedEntityId = document1.Id;
        cdl2.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl2.shareType = 'V';
        
        cdlList.add(cdl2);
        
        insert cdlList;
    }
    
    @istest static void test_updateQuoteDocumentWindreamStatus(){
		List<Opportunity> opp = [select id from Opportunity];
        test.startTest();
        QuoteDocumentManager.updateQuoteDocumentWindreamStatus(new List<String>{opp[0].id}); 
        test.stopTest();
    } 
}