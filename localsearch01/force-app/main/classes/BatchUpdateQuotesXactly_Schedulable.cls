public class BatchUpdateQuotesXactly_Schedulable implements Schedulable{
public void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(system.now().minute()+1);
        String year = string.valueOf(system.now().year());
    	String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty() || Test.isRunningTest()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'BatchUpdateQuotesXactly%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('BatchUpdateQuotesXactly0')){
                orderAcceptedOpportunityForMigration b0 = new orderAcceptedOpportunityForMigration(new List<String>{'0','1','a','A','b','B','q','Q','r','R','k','K'});
                System.schedule('BatchUpdateQuotesXactly0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('BatchUpdateQuotesXactly1')){ 
                orderAcceptedOpportunityForMigration b1 = new orderAcceptedOpportunityForMigration(new List<String>{'2','3','e','E','f','F','m','M','n','N','j','J'});
                System.schedule('BatchUpdateQuotesXactly1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('BatchUpdateQuotesXactly2')){
                orderAcceptedOpportunityForMigration b2 = new orderAcceptedOpportunityForMigration(new List<String>{'4','5','I','i','l','L','g','G','h','H','y','Y'});
                System.schedule('BatchUpdateQuotesXactly2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('BatchUpdateQuotesXactly3')){
                orderAcceptedOpportunityForMigration b3 = new orderAcceptedOpportunityForMigration(new List<String>{'6','7','o','O','p','P','c','C','d','D','x','X','w','W'});
                System.schedule('BatchUpdateQuotesXactly3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('BatchUpdateQuotesXactly4')){
                orderAcceptedOpportunityForMigration b4 = new orderAcceptedOpportunityForMigration(new List<String>{'8','9','s','S','t','T','u','U','v','V','z','Z'});
                System.schedule('BatchUpdateQuotesXactly4', strSchedule,b4);
            }
        }
    }
}