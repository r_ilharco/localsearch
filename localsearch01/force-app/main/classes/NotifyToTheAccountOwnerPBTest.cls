@isTest
public class NotifyToTheAccountOwnerPBTest {

    @isTest
    public static void testUpdateOpportunityStage(){
        try{
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh'; 
            acc.BillingCity = 'test city';
            acc.BillingCountry = 'test country';
            acc.BillingPostalCode = '123';
            acc.BillingState = 'test state';
            acc.PreferredLanguage__c = 'German';       
            insert acc;
            System.Debug('Account: '+ acc); 
            
            Place__c place = new Place__c();
            place.Account__c = acc.Id;
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';
            place.Title__c = 'Test Title';
            insert place;
            System.debug('Place: '+ place);
            
            place.Title__c = 'Test Title update';
            update place;
            System.debug('Place update: '+ place);
            
            
        }catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
    }
    
    
}