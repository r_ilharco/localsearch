public class OpportunityOwnerChangeController {
 public List<Id> oppIds {get;set;}

	public OpportunityOwnerChangeController(ApexPages.StandardSetController controller){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error Message.'));
    	List<Opportunity> opportunities = New List<Opportunity>();
        List<Id> oppIdsTemp = new List<Id>();
    	opportunities = controller.getSelected();
        system.debug('opportunities: ' + opportunities);
        for(Opportunity opp : opportunities){
            oppIdsTemp.add(opp.id);
        }
        this.oppIds = oppIdsTemp;
        system.debug('oppIdsTemp: ' + oppIdsTemp);
        system.debug('this.oppIds: ' + this.oppIds);
    }
    
    @RemoteAction
    public static responseWrapper changeOpportunityOwner(List<String> opportunityIds){
        responseWrapper changeOwnerResponse = new responseWrapper();
        changeOwnerResponse.success = true;

        String result = '';
        System.debug('opportunityIds -> ' + JSON.serialize(opportunityIds));
        if(opportunityIds != NULL){
            for(Integer i = 0; i < opportunityIds.size(); i++){
                opportunityIds[i] = opportunityIds[i].deleteWhitespace();
            }
        }
		User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
        Set<String> recordIds = new Set<String>(opportunityIds);
        System.debug('recordIds -> ' + JSON.serialize(recordIds));
        List<Opportunity> oppList = [Select Id,Name,StageName,Type,OwnerId,CloseDate,AccountId from Opportunity where Id IN :recordIds];
         List<Opportunity> listToUpdate = new List<Opportunity>();
        for(Opportunity opp : oppList){
          		if(opp.OwnerId == currentUser.Id){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorCount += 1;
                    changeOwnerResponse.errorMessage.add(opp.Name+': '+Label.RequestOpportunityOwnership_AlreadyOwner);
                }else if(currentUser.Profile.Name!= ConstantsUtil.SysAdmin && currentUser.Profile.Name != ConstantsUtil.TELESALESPROFILE_LABEL){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorCount += 1;
                    changeOwnerResponse.errorMessage.add(opp.Name+': '+Label.Automated_Opportunity_Profile_authorization);
                      
                }else if(opp.Type != ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS || (opp.StageName != ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION && opp.StageName != ConstantsUtil.OPPORTUNITY_STAGE_VALIDATION)){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorCount += 1;
                    changeOwnerResponse.errorMessage.add(opp.Name+': '+Label.Automated_Opportunity_Type_Authorization);
                }else{
                   listToUpdate.add(opp); 
                }
            
        }
        if(!listToUpdate.isEmpty()){
            changeOwner(listToUpdate,currentUser.Id,changeOwnerResponse);
        }
       	if(changeOwnerResponse.successCount > 0){
          //result= 'Update Ownership on : '+changeOwnerResponse.successCount+' records\n';
          List<String> fillers = new List<String>{string.valueOf(changeOwnerResponse.successCount)};
          changeOwnerResponse.resultCount = String.format(Label.Successfully_updated_Records, fillers)+'.\n';
       	}
        if(changeOwnerResponse.errorCount > 0){
            //result += 'Failed to Update : '+changeOwnerResponse.errorCount +' records \n';
            List<String> fillers = new List<String>{string.valueOf(changeOwnerResponse.errorCount)};
                if(changeOwnerResponse.successCount > 0){
                    changeOwnerResponse.resultCount += String.format(Label.Failed_to_Update_Records, fillers)+':';
                }else{
                   changeOwnerResponse.resultCount = String.format(Label.Failed_to_Update_Records, fillers)+':'; 
                }
          	
            //changeOwnerResponse.resultCount = 'Failed to Update : '+changeOwnerResponse. +' records \n';//
            for(string errorstring :changeOwnerResponse.errorMessage){
                result += '-'+errorstring+'\n';
            }
        }
        changeOwnerResponse.resultMessage = result;
        
        return changeOwnerResponse;
    }
    
    public static responseWrapper changeOwner(List<Opportunity> listToUpdate,string currentUserId,responseWrapper changeOwnerResponse){
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
        List<SBQQ__Quote__c> quotesList = new List<SBQQ__Quote__c>();
        for(Opportunity opp : listToUpdate){
            opp.OwnerId = currentUserId;
            oppMap.put(opp.Id, opp);
        }
        Database.SaveResult[]  resultoppupdate=  Database.update(listToUpdate,false);
        for (Database.SaveResult sr_opp : resultoppupdate) {
            system.debug('sr_opp.isSuccess() :: '+sr_opp.isSuccess());
            if(sr_opp.isSuccess()) {
                System.debug('Successfully Created OPPORTUNITY: ' + sr_opp.getId());
                changeOwnerResponse.successCount += 1;
                if(oppMap.containsKey(sr_opp.getId())){
                    SBQQ__Quote__c quote = new SBQQ__Quote__c();
               		quote.SBQQ__ExpirationDate__c  = oppMap.get(sr_opp.getId()).CloseDate;
                	quote.SBQQ__Opportunity2__c = oppMap.get(sr_opp.getId()).Id;
                	quote.SBQQ__Account__c = oppMap.get(sr_opp.getId()).AccountId;
                    quotesList.add(quote);
                }
               
            }else{
                for(Database.Error err : sr_opp.getErrors()) { 
                    System.debug('Error: '+ err.getStatusCode() + ' *****' + err.getMessage());
                }  
            }   
        }
        
        if(!quotesList.isEmpty()){
            insert quotesList;
        }
        return changeOwnerResponse;
    }
    
    
    public class responseWrapper {
        public Boolean success {get;set;}
        public integer errorCount {get;set;}
        public integer successCount {get;set;}
        public string resultMessage {get;set;}
         public string resultCount {get;set;}
        public List<string> errorMessage {get;set;}
        public responseWrapper(){
            errorMessage = new List<string>();
            errorCount = 0;
            successCount = 0;
        }
    }
}