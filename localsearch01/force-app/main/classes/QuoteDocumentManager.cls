/* SPIII 2438 Windream Manual management
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mara Mazzrellla   <mamazzalla@deloitte.it>
* @created        2020-09-07
* @systemLayer    Invocable
* @testClass      Test_QuoteDocumentManager
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public class QuoteDocumentManager {

    @invocableMethod (label='Update Windream Status' description='Update Windream Status')
    public static void updateQuoteDocumentWindreamStatus(List<String> opportunityIds){     
        List<SBQQ__QuoteDocument__c> documentList = new  List<SBQQ__QuoteDocument__c> ();
        if(opportunityIds.size() > 0){
        	documentList = [select id,WindreamUploadStatus__c from SBQQ__QuoteDocument__c 
                            where SBQQ__Quote__r.SBQQ__Opportunity2__c in:opportunityIds
                            and SBQQ__SignatureStatus__c =: ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED
                            and SignatureDate__c != null 
                            and WindreamUploadStatus__c !=: ConstantsUtil.WINDREAM_UPLOAD_SUCCESS_STATUS];
            for(SBQQ__QuoteDocument__c doc : documentList){
                doc.WindreamUploadStatus__c = ConstantsUtil.WINDREAM_UPLOAD_READY_STATUS;
            }
            update documentList;
        }      
    }
}