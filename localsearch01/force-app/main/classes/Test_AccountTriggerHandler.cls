/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 29-05-2019
 * Sprint      : 2
 * Work item   : #90 *** #91
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
                #1: SF2-264 - Legal Address Validation - gcasola - 07-19-2019 - Address_Validated changed Type from Checkbox to Picklist
                #2: SF2-253 - Validate Account and Billing Address on Quote Creation
                    SF2-264 - Legal Address Validation
                    gcasola 07-22-2019
 */

@isTest
public class Test_AccountTriggerHandler {
	
    @testSetup
    public static void setupData(){
        List<Account> acc = Test_DataFactory.createAccounts('Test',1);
        insert acc;
    }
    
    @isTest
    public static void addressValidationOK(){
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        acc[0].BillingPostalCode = '81021';
        Test.startTest();
        update acc[0];
        //System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_VALIDATED, acc[0].AddressValidated__c);
        Test.stopTest();
    }
    
    @isTest
    public static void addressValidationKO(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        acc[0].BillingPostalCode = '81021';
        Test.startTest();
        update acc[0];
        //System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED, acc[0].AddressValidated__c);
        Test.stopTest();
    }
    
    @isTest
    public static void addressValidationCatch(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_KO));
        List<Account> acc = [SELECT Id, 
                             		BillingCity, 
                             		BillingStreet, 
                             		BillingCountry, 
                             		BillingPostalCode, 
                             		UID__c, 
                             		Phone, 
                             		Email__c, 
                             		AddressValidated__c
                              FROM Account
                              LIMIT 1];
        
        acc[0].BillingPostalCode = 'ABCDE';
        Test.startTest();
        ValidateAddressResponseWrapper rw = new ValidateAddressResponseWrapper();
        update acc[0];
        /*Wrapper not fully covered even if the deserialize instantiate every class member*/
        ValidateAddressResponseWrapper wr = new ValidateAddressResponseWrapper();
        wr.attributes = new ValidateAddressResponseWrapper.AttributesObj();
        wr.attributes.hrid = 'Test';
        wr.attributes.vatid = '12345678912';
        wr.attributes.legal_form = '0101';
        wr.attributes.uid = '121AS2';
        wr.attributes.address = new ValidateAddressResponseWrapper.AddressObj();
        wr.attributes.address.street = 'Street';
        wr.attributes.address.city = 'City';
        wr.attributes.address.location = 'Location';
        /**/
        Test.stopTest();
    }
    
}