/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Orlando Aversano <oaversano@deloitte.it>
 * Date		   : 14-10-2019
 * Sprint      : 11 Wave F
 * Work item   : OrderProductTriggerTest
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   : 
 */
@isTest
public with sharing class OrderProductTriggerTest {

    @isTest
	public static void testmethod1()
    {          
        try{
            
            Test.startTest();
            List<Account> accList = Test_DataFactory.createAccounts('OA61769',1);
            insert accList;
            List<Contract> contrList = Test_DataFactory.createContracts(accList[0].Id, 1);
            insert contrList;
            SYSTEM.DEBUG('contrList: '+contrList);
            List<Product2> prdList = Test_DataFactory.createProducts('testPrd',1);
            prdList[0].Base_term_Renewal_term__c = '12';
            insert prdList;
            List<Place__c> placeList = Test_DataFactory.createPlaces('testPlace',1);
            insert placeList;
            
            List<SBQQ__Subscription__c> subscrList = Test_DataFactory.getSubscriptions(accList[0], placeList[0], contrList[0], prdList, 1);
            subscrList[0].SBQQ__ListPrice__c = 2;
            insert subscrList;
            List<Order> ordList = Test_DataFactory.createOrders(accList[0], contrList[0], ConstantsUtil.ORDER_TYPE_TERMINATION, 1);
            insert ordList;
            
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry();
            standardPrice.Pricebook2Id = pricebookId;
            standardPrice.Product2Id = prdList[0].Id;
            standardPrice.UnitPrice = 1;
            standardPrice.IsActive = true;
            standardPrice.UseStandardPrice = false;
            insert standardPrice ;
            ordlist[0].Pricebook2Id = pricebookId;
            update ordlist;
            List<OrderItem> ordItmList = Test_DataFactory.createOrderItems(ordList[0], prdList[0], standardPrice, subscrList);
            OrdItmList[0].SBQQ__Contract__c = contrList[0].Id;
            insert ordItmList;
            Test.stopTest();
        }catch(Exception e){	                            
           	system.debug('error: '+e.getMessage()+' at '+e.getLineNumber());
        }
    }
}