@isTest
public class Test_SubsBatch {
    @testSetup
    public static void testSetup(){
        Test_Billing.testSetup();
    }
    
    private static testmethod void groupingBatch(){
        Set<Id> billingProfileIds = new Set<Id>();
        list<sbqq__subscription__c> allsubs = [select id,SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c from sbqq__subscription__c];
        for(SBQQ__Subscription__c sss : allsubs){
            billingProfileIds.add(sss.SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c);
        }
        Date todayDate = Date.today();
        Date firstDayOfTheNextMonth = todayDate.addMonths(1).toStartOfMonth();
        list<sbqq__subscription__c> newtest = new list<sbqq__subscription__c>();
        list<SBQQ__Subscription__c> subTest = [SELECT Id, SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, Next_Invoice_Date__c, Grouping_Check__c,
                                               SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__r.Grouping_Mode__c 
                                               FROM SBQQ__Subscription__c 
                                               WHERE Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE 
                                               AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c  != null
                                               AND (SBQQ__Contract__r.Status = :ConstantsUtil.CONTRACT_STATUS_ACTIVE
                                                    OR (SBQQ__Contract__r.Status = :ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION 
                                                        AND SBQQ__Contract__r.TerminateDate__c > :Date.today()))
                                               AND SBQQ__NetPrice__c != 0 
                                               AND SBQQ__QuoteLine__c != NULL 
                                               AND (SBQQ__EndDate__c = NULL OR SBQQ__EndDate__c > :Date.today())
                                               AND (Next_Invoice_Date__c = NULL OR Next_Invoice_Date__c < :firstDayOfTheNextMonth)
                                               AND One_time_Fee_Billed__c = false
                                               AND SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c IN :billingProfileIds
                                               ORDER BY Next_Invoice_Date__c desc
                                               LIMIT 50000];
        
        test.startTest();
        subscriptionGroupingCheckBatch obj = new subscriptiongroupingCheckBatch();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        obj.execute(null,subtest);
        obj.execute(null);
        test.stopTest();   
    }
    
}