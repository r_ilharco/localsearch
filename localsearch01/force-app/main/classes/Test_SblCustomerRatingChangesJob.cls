@isTest
public class Test_SblCustomerRatingChangesJob {

    @testSetup
    public static void setup() {
        
    }
    
    @isTest
    public static void test_runSblCustomerRatingChangesBatch(){
        Test.startTest();
        Test.setMock( HttpCalloutMock.class,new Test_RatingChangesJobMock());
        //SblCustomerRatingChangesJob sblRCJ1 = new SblCustomerRatingChangesJob();
        System.enqueueJob(new SblCustomerRatingChangesJob(Date.today(),Date.today().addDays(2)));
        Test.stopTest(); 
    }
    
    @isTest
    public static void test_runSblCustomerRatingChangesBatch1(){
        Test.startTest();
        SblCustomerRatingChangesJob sblRCJ1 = new SblCustomerRatingChangesJob();
        Test.stopTest(); 
    }
}