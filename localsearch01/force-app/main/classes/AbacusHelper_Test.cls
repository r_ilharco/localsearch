@IsTest
public class AbacusHelper_Test {

    private static final string abaResponseContentType = 'multipart/related; boundary="MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82"; type="application/xop+xml"; start="<0.42c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82@apache.org>"; start-info="text/xml"';

    private static final string xmlAbaResponse = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + 
        	'<soapenv:Body>' + 
        		'<act:LoginResponse xmlns:act="http://www.abacus.ch/abaconnect/2007.10/core/AbaConnectTypes">' + 
        			'<act:LoginToken>0d3d834349c80afb64ef6e6dd30b5d52a6873c6d</act:LoginToken>' + 
        			'<act:Code>0</act:Code>' + 
        			'<act:Message />' +
        		'</act:LoginResponse>' + 
        	'</soapenv:Body>' +
        '</soapenv:Envelope>';

    private static final string xmlAbaLogoutResponse = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + 
        	'<soapenv:Body>' + 
        		'<act:LoginResponse xmlns:act="http://www.abacus.ch/abaconnect/2007.10/core/AbaConnectTypes">' + 
        			'<act:LoginToken>0d3d834349c80afb64ef6e6dd30b5d52a6873c6d</act:LoginToken>' + 
        			'<act:Code>0</act:Code>' + 
        			'<act:Message />' +
        		'</act:LoginResponse>' + 
        	'</soapenv:Body>' +
        '</soapenv:Envelope>';
    
    private static final string xmlAbaFaultResponse = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + 
        	'<soapenv:Body>' + 
                '<soapenv:Fault>' +
                    '<faultcode>soapenv:Server</faultcode>' +
                    '<faultstring>AbaConnectFaultException</faultstring>' +
                    '<detail>' +
                        '<act:AbaConnectFault xmlns:act="http://www.abacus.ch/abaconnect/2007.10/core/AbaConnectTypes">' +
                            '<act:Code>8020</act:Code>' +
                            '<act:Message>Login failed the account for the user: acag8 has been disabled. Code from Login ACCOUNTDISABLED</act:Message>' +
                            '<act:Cause>Login failed the account for the user: acag8 has been disabled. Code from Login ACCOUNTDISABLED</act:Cause>' +
                        '</act:AbaConnectFault>' +
                    '</detail>' +
                '</soapenv:Fault>' +
        	'</soapenv:Body>' +
        '</soapenv:Envelope>';    

    private static final string xmlSoapFaultResponse = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + 
        	'<soapenv:Body>' + 
                '<soapenv:Fault>' +
                    '<faultcode>0001</faultcode>' +
                    '<faultstring>Invalid Soap Error</faultstring>' +
                    '<detail>' +
						'Some details about the error' +
                    '</detail>' +
                '</soapenv:Fault>' +
        	'</soapenv:Body>' +
        '</soapenv:Envelope>';
    
    private static string getMimeMessage(string content) {
        return '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82\n' +
			'Content-Type: application/xop+xml; charset=UTF-8; type="text/xml"\n' +
			'Content-Transfer-Encoding: binary\n' + 
			'\n' +
			content + '\n' +
            '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82--\n';
    }
    
    private static void createSettings() {
        Numbering_cs__c nc = Numbering_cs__c.getOrgDefaults();
        if(nc==null) {
            insert new Numbering_cs__c(Next_Invoice_Number__c = 400000000L);
        }
    }
    
    static testmethod void test_esr() {
        System.assertEquals(new List<integer> {0, 9, 4, 6, 8, 2, 7, 1, 3, 5}, AbacusHelper.EsrMod10);
        System.assertEquals(ConstantsUtil.BILLING_ESR_PREFIX + '01999999990123456789' + '0', AbacusHelper.getESRValue('199999999', '123456789'));
    }
    
    static testmethod void test_isoDate() {
    	System.assertEquals('2018-10-15', AbacusHelper.toISO8601String(Date.newInstance(2018, 10, 15)));
        System.assertEquals(null, AbacusHelper.toISO8601String(null), 'Null date should return null Iso string');
    }

    static testmethod void test_getElementText() {
        System.assertEquals('', AbacusHelper.getElementText(null, null, null));
        Dom.Document xdoc = new Dom.Document();
        Dom.XmlNode xn = xdoc.createRootElement('root', null, null);
        System.assertEquals('', AbacusHelper.getElementText(xn, 'child', null));
    }
    
    static testmethod void test_MultipartStrip() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', abaResponseContentType);
        res.setBody(getMimeMessage(xmlAbaResponse));
        Dom.Document xDocActual = AbacusHelper.getDocumentFromMultipartMime(res);
        System.assertEquals(xmlAbaResponse, xDocActual.toXmlString());
    }

    static testmethod void test_InvalidMultipartStrip() {
        // Missing content type header
        HttpResponse res = new HttpResponse();
        res.setBody(getMimeMessage(xmlAbaResponse));
        boolean exceptionThrown = false;
        try {
        	Dom.Document xDocActual = AbacusHelper.getDocumentFromMultipartMime(res);
        } catch (Exception ex) {
            System.assert(ex instanceOf AbacusHelper.AbacusHelperException, 'Invalid exception type');
            System.assertEquals(ErrorHandler.ErrorCode.E_UNKNOWN.name(), ((AbacusHelper.AbacusHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        System.assertEquals(true, exceptionThrown, 'No exception thrown on invalid http response');        
    }

    static testmethod void test_NonMultipart() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setBody(xmlAbaResponse);
        Dom.Document xDocActual = AbacusHelper.getDocumentFromMultipartMime(res);
        System.assertEquals(xmlAbaResponse, xDocActual.toXmlString());
    }
    
    static testMethod void test_convertTaxCode() {
        System.assertEquals('31', AbacusHelper.convertTaxCode('31'));
        System.assertEquals('31', AbacusHelper.convertTaxCode('Standard'));
        System.assertEquals('32', AbacusHelper.convertTaxCode('Reduced'));
        System.assertEquals('31', AbacusHelper.convertTaxCode(null));
    }
    
    static testmethod void test_LoginToken() {
        createSettings();
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlAbaResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        string token = AbacusHelper.abacusLoginToken();
        Test.stopTest();
        System.assertEquals('0d3d834349c80afb64ef6e6dd30b5d52a6873c6d', token);
    }

    static testmethod void test_Login() {
        createSettings();
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlAbaResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        AbacusHelper.abacusLoginToken();
        Test.stopTest();
    }

    static testmethod void test_FailedLoginToken() {
        createSettings();
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.answerType = 400;
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        boolean exceptionThrown = false;
        try {
        	string token = AbacusHelper.abacusLoginToken();
        } catch (Exception ex) {
            System.assert(ex instanceOf AbacusHelper.AbacusHelperException, 'Invalid exception type');
            System.assertEquals(ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE.name(), ((AbacusHelper.AbacusHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionThrown, 'No exception thrown on invalid http response');
    }

    static testmethod void test_AbaFault() {
        createSettings();
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlAbaFaultResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        boolean thrownException = false;
        try {
        	string token = AbacusHelper.abacusLoginToken();
        } catch (AbacusHelper.AbacusHelperException ex) {
            System.assertEquals(ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT.name(), ex.ErrorCode);
            thrownException = true;
        }
        Test.stopTest();
        System.assertEquals(true, thrownException, 'AbacusHelperException was expected');
    }

    static testmethod void test_SoapFault() {
        createSettings();
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlSoapFaultResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        boolean thrownException = false;
        try {
        	string token = AbacusHelper.abacusLoginToken();
        } catch (AbacusHelper.AbacusHelperException ex) {
            System.assertEquals(ErrorHandler.ErrorCode.E_SOAP_FAULT.name(), ex.ErrorCode);
            thrownException = true;
        }
        Test.stopTest();
        System.assertEquals(true, thrownException, 'AbacusHelperException was expected');
    }    

    static testmethod void test_abacusLogout() {
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlAbaLogoutResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);        
        AbacusHelper.abacusLogout('fakeToken');
    }

    static testmethod void test_Logout() {
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlAbaLogoutResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);        
    	AbacusHelper.abacusLogout('faketoken');
    }

    static testmethod void test_FailedLogout() {
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.answerType = 400;
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        boolean exceptionThrown = false;
        try {
        	AbacusHelper.abacusLogout('fakeToken');
        } catch (Exception ex) {
            System.assert(ex instanceOf AbacusHelper.AbacusHelperException, 'Invalid exception type');
            System.assertEquals(ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE.name(), ((AbacusHelper.AbacusHelperException)ex).ErrorCode);
            exceptionThrown = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionThrown, 'No exception thrown on invalid http response');
    }
    
    static testmethod void test_LogoutSoapFault() {
        Test.startTest();
        AbacusHttpResponseMock xmlResponse = new AbacusHttpResponseMock();
        xmlResponse.fullBody = getMimeMessage(xmlSoapFaultResponse);
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        boolean thrownException = false;
        try {
        	AbacusHelper.abacusLogout('fakeToken');
        } catch (AbacusHelper.AbacusHelperException ex) {
            System.assertEquals(ErrorHandler.ErrorCode.E_SOAP_FAULT.name(), ex.ErrorCode);
            thrownException = true;
        }
        Test.stopTest();
        System.assertEquals(true, thrownException, 'AbacusHelperException was expected');
    }
    
    public class AbacusHttpResponseMock implements HttpCalloutMock {
        public integer answerType = 200;
        public string fullBody;
	   
        public HTTPResponse respond(HTTPRequest req) {
            // Assert body and headers are as expected
            HttpResponse res = new HttpResponse();
            switch on answerType {
                when 200 {
                    res.setHeader('Content-Type', abaResponseContentType);
                    res.setBody(fullBody);
                    res.setStatusCode(200);                
                }
                when 400 {
                    res.setBody('<h1>400 - Bad Request</h1>');
                    res.setStatusCode(400);                     
                }
                when 401 {
                    res.setBody('<h1>401 - Unauthorized</h1>');
                    res.setStatusCode(401);                     
                }
            }
            return res;
        }
    }

}