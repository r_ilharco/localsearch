/**
* Author      : Alessandro Luigi Marotta <amarotta@deloitte.it>
* Date        : 24-05-2019
* Sprint      : 2
* Work item   : 
* Testclass   : Test_AccountTrigger
* Package     : 
* Description : 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to fields listed in SPIII 1057 removed
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-03              
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reassign owner if territory changes (SPIII 1214)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-07-09  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        "Parent Account" field on Account (SPIII 1461)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-07-17  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Set default account owner (SPIII 2050)
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-20  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Review AS IS Request assignment process performed by DMC (SPIII 2025)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-07-24
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Added logic to reset Submitter field after the Approval Request is approved.
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-07-28
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-4843 -  IsDuplicated flag remaining true after Account merge
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2021-01-21
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class ATL_AccountTriggerHelper {

    
    //all the static methods you need for the AccountTriggerHandler class
    //all the calls to the inbound methods in the SRV_IntegrationOutbound class
    
    public static void checkNewAccountsLegalAddress(List<Account> newItems){
        System.debug('***************************************+BEFORE INSERT CHECk');
        
        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
		      
        if(Trigger.operationType == System.TriggerOperation.BEFORE_INSERT)
        {
            for(Account acc: newItems){
                if(addressValidationMDT.Active__c){
                    acc.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                    acc.AddressValidationDate__c = NULL;
                    acc.Address_Integration__c = true;
                }
            }
        }else if(Trigger.operationType == System.TriggerOperation.AFTER_INSERT)
        {
            Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(newItems);
            Set<Id> accountIdsToUpdate = new Set<Id>();
            for(Account acc: newItems){
                if(addressValidationMDT.Active__c){
                    try{
                        if(!skipAddressValidationFlagsMap.get(acc.Id))
                        	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id);
                    }catch(Exception e)
                    {
                        accountIdsToUpdate.add(acc.Id);
                    }
                }
            }
            
            if(accountIdsToUpdate.size() > 0)
            {
                List<Account> accountsToUpdate = SEL_Account.getAccountsById(accountIdsToUpdate).values();
                for(Account acct : accountsToUpdate)
                {
                    acct.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                    acct.AddressValidationDate__c = NULL;
                    acct.Address_Integration__c = false;
                }
                
                update accountsToUpdate;
            }
        }
        
    }
    
    //SF2-438 - Localina Activation Integration Check - mamazzarella@deloitte.it
    public static void insertCustomerNumber(List<Account> newItems){
        System.debug('**insertCustomerNumber');
        boolean customerNumberChanged = false;
        Customer_Number_cs__c customerNumber = Customer_Number_cs__c.getOrgDefaults();
        System.debug('**customerNumber: '+customerNumber);
        
        try {
            for(Account acc: newItems){
                if(String.isBlank(acc.Customer_Number__c) && acc.AccountSource != ConstantsUtil.ACC_SOURCE_MANUAL_LCM){
                    long nextValue = customerNumber.Next_Customer_Number__c.longValue();
                    customerNumber.Next_Customer_Number__c = nextValue + 1;
                    customerNumberChanged = true;
                    acc.Customer_Number__c = String.valueOf(nextValue).replaceAll('[^0-9]', '');
                }
            }
            if(customerNumberChanged) {
                update customerNumber;
            }
        }
        catch (Exception ex) {
            
        } 
       
    }
    
    public static void checkAccountsLegalAddress(Map<Id, Account> NewAccounts, Map<Id, Account> OldAccounts){
        
        System.debug('***************************************+BEFORE UPDATE CHECk' + NewAccounts);
        System.debug('***************************************+BEFORE UPDATE CHECk' + OldAccounts);
        
        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
        Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(NewAccounts.values());
        System.debug('addressValidationMDT' + addressValidationMDT);
        for(Account acc: NewAccounts.values()){
            Account oldAcc = OldAccounts.get(acc.Id);
            System.debug ('oldAcc ' + oldAcc);
            if(addressValidationMDT.Active__c){
                System.debug('addressValidationMDT.Active__c ' + addressValidationMDT.Active__c);
                if(((acc.BillingCity != null && oldAcc.BillingCity != null && !oldAcc.BillingCity.equalsIgnoreCase(acc.BillingCity))||
                    (acc.BillingStreet != null && oldAcc.BillingStreet != null && !oldAcc.BillingStreet.equalsIgnoreCase(acc.BillingStreet))||
                    (acc.BillingCountry != null && oldAcc.BillingCountry != null && !oldAcc.BillingCountry.equalsIgnoreCase(acc.BillingCountry))||
                    (acc.BillingPostalCode != null && oldAcc.BillingPostalCode != null && !oldAcc.BillingPostalCode.equalsIgnoreCase(acc.BillingPostalCode))||
                    (acc.UID__c != null && oldAcc.UID__c != null && !oldAcc.UID__c.equalsIgnoreCase(acc.UID__c))||
                    (acc.Phone != null && oldAcc.Phone != null && !oldAcc.Phone.equalsIgnoreCase(acc.Phone))||
                    (acc.Email__c != null && oldAcc.Email__c != null && !oldAcc.Email__c.equalsIgnoreCase(acc.Email__c)))
                  ){         
                      if(!ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED.equals(acc.AddressValidated__c))
                      {
                          acc.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                          acc.AddressValidationDate__c = NULL;
                      }
                      
                      if(!skipAddressValidationFlagsMap.get(acc.Id))
                      	SRV_IntegrationOutbound.asyncValidateAddress(acc.Id);
                      acc.Address_Integration__c = true;
                  }
            }
        }
        
    }
    
    public static void tipInsertAccountTechicalZipCode(List<Account> NewAccount){
        
        for(Account acc: NewAccount){
            try{
                if(acc.BillingPostalCode != null){
                    acc.Tech_ZipCode__c=decimal.valueOf(acc.BillingPostalCode);
                }
            }
            catch (Exception e){
                /**** Sprint 8  - SF2 - 333 Validation on Zip Code ****/
                acc.Tech_ZipCode__c.addError(System.Label.Zip_Code_Validation);
            }              
        } 
    }
    
    public static void manageTerritoryAssignmentRules(List<SObject> newItems, Map<Id, SObject> oldItems){
       	List<Account> newAccountsList = (List<Account>) newItems;
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldItems;
        
        SRV_TerritoryRules.UserTerritoryHierarchyInfo utHierarchyInfo;
        SRV_TerritoryRules.BillingPostalCodesTerritoryMappingInfo bpctmInfo;
        
        Set<Id> ownerIds = new Set<Id>();
        Boolean isOwnerChanged = false;
        Boolean isTerritoryExceptionChanged = false;
        Boolean isBillingPostalCodeChanged = false;
        
        for(Account acc : newAccountsList){
            ownerIds.add(acc.OwnerId);
        }
        
        bpctmInfo = SRV_TerritoryRules.getTerritoryDeveloperNamesByPostalCodeMap();
        utHierarchyInfo = SRV_TerritoryRules.getUserTerritoryHierarchyMap(ownerIds);
        
        for(Account acc : newAccountsList){
            Id accountId = acc.Id;
            Account newAccount = acc;
            Account oldAccount;
            String accountTerritoryDeveloperName = NULL;
            String ownerTerritoryDeveloperName = NULL;
            
            if(Trigger.operationType == System.TriggerOperation.BEFORE_UPDATE)
                oldAccount = oldAccountsMap.get(accountId);
            else
                oldAccount = new Account();
            
            if(utHierarchyInfo != NULL 
               && utHierarchyInfo.ut2aByUserId != NULL 
               && utHierarchyInfo.ut2aByUserId.containsKey(newAccount.OwnerId) 
               && utHierarchyInfo.ut2aMap != NULL
               && utHierarchyInfo.ut2aMap.containsKey(utHierarchyInfo.ut2aByUserId.get(newAccount.OwnerId)))
                ownerTerritoryDeveloperName = utHierarchyInfo.ut2aMap.get(utHierarchyInfo.ut2aByUserId.get(newAccount.OwnerId)).Territory2.DeveloperName;
            
            if(String.isNotBlank(newAccount.BillingPostalCode) 
               && bpctmInfo.territoryDeveloperNamesByPostalCodeMap != NULL 
               && bpctmInfo.territoryDeveloperNamesByPostalCodeMap.containsKey(newAccount.BillingPostalCode))
                accountTerritoryDeveloperName = bpctmInfo.territoryDeveloperNamesByPostalCodeMap.get(newAccount.BillingPostalCode);
            
            isOwnerChanged = (newAccount.OwnerId != oldAccount.OwnerId);
            isTerritoryExceptionChanged = newAccount.TerritoryException__c != oldAccount.TerritoryException__c;
            isBillingPostalCodeChanged = ((String.isNotBlank(newAccount.BillingPostalCode) && !newAccount.BillingPostalCode.equalsIgnoreCase(oldAccount.BillingPostalCode)) || (String.isNotBlank(oldAccount.BillingPostalCode) && !oldAccount.BillingPostalCode.equalsIgnoreCase(newAccount.BillingPostalCode)));
            Id territory2Id = NULL;
            Id parentTerritory2Id = NULL;
            
            if(isOwnerChanged || isTerritoryExceptionChanged || (isBillingPostalCodeChanged && !newAccount.TerritoryException__c)){
                
                if(isOwnerChanged){
                    
                    if(String.isNotBlank(ownerTerritoryDeveloperName)){
                        territory2Id = utHierarchyInfo.ut2aMap.get(utHierarchyInfo.ut2aByUserId.get(newAccount.OwnerId)).Territory2Id;
                    }else if(String.isNotBlank(accountTerritoryDeveloperName)){
                        if(utHierarchyInfo.territory2IdByDeveloperNameMap != NULL 
                           && utHierarchyInfo.territory2IdByDeveloperNameMap.containsKey(accountTerritoryDeveloperName)){
                               territory2Id = utHierarchyInfo.territory2IdByDeveloperNameMap.get(accountTerritoryDeveloperName);
                           }
                    }
                    
                }else if(isTerritoryExceptionChanged){
                    
                    if(newAccount.TerritoryException__c){
                        if(String.isNotBlank(ownerTerritoryDeveloperName)){
                            territory2Id = utHierarchyInfo.ut2aMap.get(utHierarchyInfo.ut2aByUserId.get(newAccount.OwnerId)).Territory2Id;
                        }
                    }else{
                        if(String.isNotBlank(accountTerritoryDeveloperName)){
                            if(utHierarchyInfo.territory2IdByDeveloperNameMap != NULL 
                               && utHierarchyInfo.territory2IdByDeveloperNameMap.containsKey(accountTerritoryDeveloperName)){
                                   territory2Id = utHierarchyInfo.territory2IdByDeveloperNameMap.get(accountTerritoryDeveloperName);
                               }
                        }
                    }
                    
                }else if(isBillingPostalCodeChanged){
                    if(String.isNotBlank(accountTerritoryDeveloperName)){
                        if(utHierarchyInfo.territory2IdByDeveloperNameMap != NULL 
                           && utHierarchyInfo.territory2IdByDeveloperNameMap.containsKey(accountTerritoryDeveloperName)){
                               territory2Id = utHierarchyInfo.territory2IdByDeveloperNameMap.get(accountTerritoryDeveloperName);
                           }
                    }
                }
                
                SRV_TerritoryRules.setFieldsOnAccount(newAccount, territory2Id, utHierarchyInfo, bpctmInfo);
            }
        }
    }
    
    //OAVERSANO 20191008 Check ActivationDate -- START
    /*public static void checkActivationDateNewCustomer(Map<Id, Account> NewAccounts, Map<Id, Account> OldAccounts){
        
        System.debug('***************************************+BEFORE UPDATE checkActivationDateNewCustomer' + NewAccounts);
        System.debug('***************************************+BEFORE UPDATE checkActivationDateNewCustomer' + OldAccounts);
        
        for(Account acc: NewAccounts.values()){
            Account oldAcc = OldAccounts.get(acc.Id);
            System.debug ('oldAcc ' + oldAcc);
            if(ConstantsUtil.ACCOUNT_STATUS_ACTIVE.equalsIgnoreCase(acc.Status__c) && !ConstantsUtil.ACCOUNT_STATUS_ACTIVE.equalsIgnoreCase(oldAcc.Status__c) ){
                System.debug(oldAcc.Status__c+' --> change status --> '+ acc.Status__c);
                if(acc.Activation_Date__c==null){         
                    acc.Activation_Date__c = System.now().date();
                }
            }
        }
    }*/
    //OAVERSANO 20191008 Check ActivationDate -- END
    
    public static void sendBlockedAccountNotificationToPM(Map<Id, SObject> NewAccount, Map<Id, SObject> OldAccount){
        
        List<FeedItem> newPostList = new List<FeedItem>();
        List<Account> blockedAccounts = new List<Account>();
        List<GroupMember> groupMembers = new List<GroupMember>();
        Map<Id,User>  userMembers = new Map<Id,User> ();
        List<Group> groupRoleMembers = new List<Group>();
        Set<Id> userIds= new Set<Id>();
        for(Id accId : NewAccount.keySet()) {
            // control if user is blocked 
            Account newAcc= (Account)NewAccount.get(accId);
            Account oldAcc=(Account)OldAccount.get(accId);
            if (newAcc.Status__c == ConstantsUtil.ACCOUNT_STATUS_BLOCKED 
            &&  oldAcc.Status__c != ConstantsUtil.ACCOUNT_STATUS_BLOCKED ){
                blockedAccounts.add(newAcc);
            }
        }
        
        
        if (!blockedAccounts.isEmpty()){
            // get group Members 
            //groupMembers= [SELECT Id,UserOrGroupId  FROM GroupMember WHERE Group.Name=:ConstantsUtil.PM_GROUP];
            userMembers = new Map<Id, User>([select Id from User where Id in (select UserOrGroupId from GroupMember 
                                                           WHERE Group.Name=:ConstantsUtil.PM_GROUP) 
                                                           and isActive = true]);
            groupRoleMembers = [select id, type, RelatedId from Group 
                                where Id in (select UserOrGroupId 
                                             from GroupMember where Group.Name=:ConstantsUtil.PM_GROUP) 
                                and type in ('Role','RoleAndSubordinatesInternal')];
            Set<Id> roleIds = new Set<Id>();
            Set<Id> roleandSubId = new Set<Id>();
            for(Group gr : groupRoleMembers){
                 roleIds.add(gr.RelatedId); 
                 if(gr.type == 'RoleAndSubordinatesInternal'){
                   roleandSubId.add(gr.RelatedId); 
                }
            }
            if(!userMembers.isEmpty()){
                userIds.addAll(userMembers.keySet());
            }
            if(!roleandSubId.isEmpty()){
                userIds.addAll(RoleUtility.getRoleSubordinateUsers(roleandSubId));
            }
            if(!roleIds.isEmpty()){
                userIds.addAll(RoleUtility.getRoleUsers(roleIds));
            }
          /*  if (!groupMembers.isEmpty()){
                for (GroupMember gm : groupMembers){
                    UserIds.add(gm.UserOrGroupId);
                }
            }*/
            System.debug('userIds: '+userIds);
            System.debug('roleIds: '+roleIds);
            System.debug('roleandSubId: '+roleandSubId);
            if(!userIds.IsEmpty()){
                //if (!pmGroup.isEmpty()){
                for (Id userId : userIds){
                    for (Account acc : blockedAccounts ){
                        FeedItem post = new FeedItem();
                        post.ParentId = userId;
                        post.Body = ConstantsUtil.BLOCKED_ACCOUNT_MESSAGE_1 + acc.Name  + ConstantsUtil.BLOCKED_ACCOUNT_MESSAGE_2 + acc.ClientRating__c;
                        post.Title = ConstantsUtil.BLOCKED_NOTIFICATION_TITLE;
                        newPostList.add(post);
                    }
                }
            }
        }
        
        if (!newPostList.isEmpty()){
            if(!Test.isRunningTest()) insert newPostList;
        }
    }
    //A. Esposito 20-11-19 SF2 - 414
    public static void runDuplicationRules(List<Account> NewAccount, List<Account> OldAccount){
        System.debug('validateDuplicateAccount ');
        Boolean afterInsert = Trigger.operationType == System.TriggerOperation.AFTER_INSERT;
        Set<Id>  accountToUpdate = new Set<Id>();
        List<Account>  accToAnlz = new List<Account>();
        List<Duplicate_Management__mdt> dMangmtAcc = [SELECT Active__c, Value__c, Type__c  FROM Duplicate_Management__mdt WHERE Active__c = true AND Object_API_Name__c = 'Account' AND Type__c = 'Field'];
        Map<String, List<String>> mapMetaDAcc = new Map<String, List<String>>();

        for(Duplicate_Management__mdt dm: dMangmtAcc){
            if(!mapMetaDAcc.containsKey(dm.Type__c)){
                mapMetaDAcc.put(dm.Type__c, new List<String>());
            }
            mapMetaDAcc.get(dm.Type__c).add(dm.Value__c);
        }
        List<String> fieldDR = mapMetaDAcc.get('Field');
        Boolean toAnalz = false;
        if(afterInsert == true)
            accToAnlz = NewAccount;
        else{
            for(Integer idx=0; idx < OldAccount.size(); idx++){
                if(NewAccount[idx].isDuplicated__c == false)
                    NewAccount[idx].MarkedAsNotDuplicated__c = false;
                for(String currField: fieldDR){
                    if(OldAccount[idx].get(currField) != NewAccount[idx].get(currField)){
                        toAnalz = true;
                        break;
                    }
                }
                if(toAnalz)
                    accToAnlz.add(NewAccount[idx]);
            }
        }
        try{
            for(integer i=0; i<=accToAnlz.size()/50; i++){
                List<Account> chunkaccToAnlz = new List<Account>();
                integer size = (i+1)*50 < accToAnlz.size() ? (i+1)*50 : accToAnlz.size();
                for(integer j=i*50;j<size;j++){
                    chunkaccToAnlz.add(accToAnlz[j]);       
                }
                Boolean duplicated = false;
                //A.L. Marotta 09-01-2020 Start - added check in case of chunkaccToAnlz empty
                if(!chunkaccToAnlz.isEmpty()){
                    Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(chunkaccToAnlz);
                    for(integer j = 0; j< chunkaccToAnlz.size();j++){
                        if(afterInsert == false){
                        	chunkaccToAnlz[j].isDuplicated__c = false;
                        	chunkaccToAnlz[j].MarkedAsNotDuplicated__c = false;
                        }
                        Datacloud.FindDuplicatesResult findDupeResult = results[j];   
                        for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                            for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                                if(matchResult.getSize()>0){
                                    if(afterInsert == false)
                                        chunkaccToAnlz[j].isDuplicated__c = true;
                                    accountToUpdate.add(chunkaccToAnlz[j].id);
                                }
                            }
                        }
                    }                 
                }
            }
            if(afterInsert == true && !accountToUpdate.isEmpty() ){
                AccountTriggerHandler.disableTrigger = true;
               	futureUpdateIsDuplicatedFlag(accountToUpdate,true);
                AccountTriggerHandler.disableTrigger = false;
            }
        }
        catch(Exception e) {
            for(Account acc : accToAnlz){
                acc.isDuplicated__c = false;
                acc.MarkedAsNotDuplicated__c = false;
            }
        }
    }
    
    public static void processMergedAccounts(Map<Id, Account> oldAccountsMap)
    {
        List<FeedItem> feedItemsToPost  = new List<FeedItem>();
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        Set<Id> winningAccountIds = new Set<Id>();
        for(Account acc : (List<Account>) oldAccountsMap.values())
        {
            if(acc.MasterRecordId != NULL)
            {
                System.debug('>acc.MasterRecordId: ' + acc.MasterRecordId);
                System.debug('>acc.Id' + acc.MasterRecordId);
                winningAccountIds.add(acc.MasterRecordId);
                sendNotificationForMergedAccounts(acc, feedItemsToPost, fieldMap);                
            }
        }
        
        System.debug('>winningAccountIds: ' + JSON.serialize(winningAccountIds));
        System.debug('>winningAccountIds.size(): ' + winningAccountIds.size());
        
        if(winningAccountIds != NULL && winningAccountIds.size() > 0)
        {
            Map<Id, Billing_Profile__c> billinProfilesMap = SEL_BillingProfile.getBillingProfilesByAccountIdForMergingAccounts(winningAccountIds);
            
            System.debug('>billinProfilesMap.size(): ' + billinProfilesMap.size());
            System.debug('>billinProfilesMap: ' + JSON.serialize(billinProfilesMap));
            
            for(Billing_Profile__c bp : billinProfilesMap.values())
            {
                bp.Is_Default__c = false;
            }
            
            upsert billinProfilesMap.values();
			futureUpdateIsDuplicatedFlag(winningAccountIds, false);
        }
        
        if(feedItemsToPost.size() > 0)
            insert feedItemsToPost;
    }
     @future 
    public static void futureUpdateIsDuplicatedFlag(Set<Id> winningAccountIds, boolean isDuplicated){
        List<Account> winners = [select id from Account where id in:winningAccountIds];
        for(Account a: winners){
            a.isDuplicated__c = isDuplicated;
        }
        update winners;
    }
    public static void sendNotificationForMergedAccounts(Account acc, List<FeedItem> feedItemsToPost, Map<String, Schema.SObjectField> fieldMap)
    {
        FeedItem post = new FeedItem();
        post.ParentId = acc.OwnerId;
        post.Title = Label.SendNotification_MergedAccount_Title;
        post.Body =  fieldMap.get('Name').getDescribe().getLabel() + ': ' + acc.Name + '\n';
        post.Body += (String.isNotBlank(acc.Full_Address__c)?fieldMap.get('Full_Address__c').getDescribe().getLabel() + ': ' + acc.Full_Address__c + '\n':'');
        post.Body += (String.isNotBlank(acc.Phone)?fieldMap.get('Phone').getDescribe().getLabel() + ': ' + acc.Phone + '\n':'');
        post.Body += (String.isNotBlank(acc.Email__c)?fieldMap.get('Email__c').getDescribe().getLabel() + ': ' + acc.Email__c:'');
        
        post.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ acc.MasterRecordId;
        feedItemsToPost.add(post);
    }
    
    public static void updateAddressField(List<Account> NewAccount, Map<Id, Account> OldAccountMap){
        for(Account a : NewAccount){
            if(String.isNotBlank(a.BillingStreet)){
                if(!a.BillingStreet.equalsIgnoreCase(OldAccountMap.get(a.Id).BillingStreet)){
                    getFinalAddress(a);
                }
            }
        }
    }
    
    public static void insertAddressField(List<Account> NewAccount){
        for(Account a : NewAccount){
            if(String.isNotBlank(a.BillingStreet)){
                getFinalAddress(a);
            }
        }
    }
    
    public static void getFinalAddress(Account a){
        String finalAddress;
        String textPart;
        String numericPart;
        Pattern text = Pattern.compile('[^\\d]+');
        Matcher textMatcher = text.matcher(a.BillingStreet);
        Pattern numeric = Pattern.compile('(\\d+)');
        Matcher numericMatcher = numeric.matcher(a.BillingStreet);
        if(textMatcher.find()){
            textPart =textMatcher.group(0);
        }
        if(numericMatcher.find()){
            numericPart =numericMatcher.group(0);
        }
        
        a.BillingStreet = textPart+' '+numericPart;
    }
    
    public static void checkAccountHierarcy(Map<Id, Account> NewAccounts, Map<Id, Account> OldAccounts){
        Set<Id> parentAccount= new Set<Id>();
        Set<Id> accountWithParentIds = new Set<Id>();
        for(Account newItem : NewAccounts.values()){
            if(OldAccounts != null){
                if(newItem.ParentId != OldAccounts.get(newItem.id).ParentId
                   && newItem.ParentId != null
                  ){
                       parentAccount.add(newItem.ParentId);
                }
            }
            else if (OldAccounts == null 
                     && newItem.ParentId != null){
                parentAccount.add(newItem.ParentId);
            }
        }
        if(!parentAccount.isEmpty()){
            List<Account> parentOf = [select id from Account where ParentId != null and id in: parentAccount];
            if(!parentOf.isEmpty()){
                for(Account acc : parentOf){
                    accountWithParentIds.add(acc.id);
                }
                for(Account newItem : NewAccounts.values()){
                    if(OldAccounts != null){
                        if(newItem.ParentId != OldAccounts.get(newItem.id).ParentId && newItem.ParentId != null && accountWithParentIds.contains(newItem.ParentId)
                          ){
                               newItem.addError(Label.ParentAccount_Validation);
                           }
                    }
                    else if( newItem.ParentId != null  && accountWithParentIds.contains(newItem.ParentId)){
                        newItem.addError(Label.ParentAccount_Validation);
                    }
                }
            }
        }
    }
    
    public static void resetSubmitterField(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts){
        
        List<Account> accountToUpdate = new List<Account>();
        
        for(Account currentAccount : newAccounts.values()){
            
            Boolean assignmentApprovedNew = currentAccount.Tech_Request_Assignment_Approved__c;
            Boolean assignmentApprovedOld = oldAccounts.get(currentAccount.Id).Tech_Request_Assignment_Approved__c;
            Boolean sellApprovedNew = currentAccount.Tech_Request_to_Sell_Approved__c;
            Boolean sellApprovedOld = oldAccounts.get(currentAccount.Id).Tech_Request_to_Sell_Approved__c;
            Boolean changeOwnerStatusNewNull = String.isBlank(currentAccount.ApprovalStep__c);
            Boolean changeOwnerStatusOldNotNull = String.isNotBlank(oldAccounts.get(currentAccount.Id).ApprovalStep__c);
            
            if( (!assignmentApprovedNew && assignmentApprovedOld) || (!sellApprovedNew && sellApprovedOld) || (changeOwnerStatusNewNull && changeOwnerStatusOldNotNull) ){
                Account acc = new Account();
                acc.Id = currentAccount.Id;
                acc.Tech_Submitter__c = null;
                accountToUpdate.add(acc);
            }
        }
        
        if(!accountToUpdate.isEmpty()){
            AccountTriggerHandler.disableTrigger = true;
            update accountToUpdate;
            AccountTriggerHandler.disableTrigger = false;
            
        }
    }
    
    public static void updateRelatedOpportunityQuoteOwner(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts){
        set<Id> accountId = new set<Id>();
        for(Account currentAccount : newAccounts.values()){
            Boolean isOwnerChanged = oldAccounts.get(currentAccount.Id).OwnerId != currentAccount.OwnerId;
            system.debug('isOwnerChanged :: '+isOwnerChanged);
            if(isOwnerChanged){
                accountId.add(currentAccount.Id);	
            } 
        }
        if(!accountId.isEmpty()){
            List<Opportunity> expiringContractOpp = [Select Id,StageName,Type,OwnerId,Account.OwnerId, (Select Id, OwnerId,SBQQ__Status__c,SBQQ__SalesRep__c from SBQQ__Quotes2__r where SBQQ__Status__c =:ConstantsUtil.Quote_Status_Draft ) 
                                                     from Opportunity 
                                                     where AccountId IN :accountId 
                                                     And Type= :ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS
                                                     And Automated_Opportunity_Flow__c = :ConstantsUtil.OPPORTUNITY_FLOW_DMC
                                                     And (StageName = :ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION OR StageName = :ConstantsUtil.OPPORTUNITY_STAGE_VALIDATION)];
            
            if(!expiringContractOpp.isEmpty()){
                 List<SBQQ__Quote__c> quotesList = new List<SBQQ__Quote__c>();
                for(Opportunity opp : expiringContractOpp){
                    opp.OwnerId = opp.Account.OwnerId;  
                    if(opp.SBQQ__Quotes2__r.size() > 0){
                        for(SBQQ__Quote__c  quotes : opp.SBQQ__Quotes2__r){
                            quotes.OwnerId = opp.Account.OwnerId;
                            quotes.SBQQ__SalesRep__c = opp.Account.OwnerId;
                            quotesList.add(quotes);
                        }
                    }
                }
            
            
            update expiringContractOpp;
            if(!quotesList.isEmpty()){
                update quotesList;
            }
           } 
        }
    }
    
    public static void setValuesIfCreatedBySales(List<account> newItems){
		
        //This custom permission(set) is granted to DMCs and Telesales allowing them to convert Leads & create accounts
        Boolean hasCustomPermission = FeatureManagement.checkPermission(ConstantsUtil.CUSTOM_PERMISSION_CREATEACC_LABEL);
        
        if(hasCustomPermission){ 
            for(Account salesAcc : newItems){
                salesAcc.AccountSource = ConstantsUtil.ACC_SOURCE_MANUAL_Sales; 
            }
        }
    }
    
}