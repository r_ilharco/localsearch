/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-16-2019
 * Sprint      : 1
 * Work item   : US_19
 * Testclass   : 
 * Package     : 
 * Description : Test Class for CategoryAndLocationController
 * Changelog   : 
 */

@isTest
public with sharing class Test_CategoryAndLocationController {
    
    public static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Place Name';
        insert byPassFlow;
    }
    
    @TestSetup
    static void setup(){
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        insertBypassFlowNames();
        List<Account> accounts = Test_DataFactory.createAccounts('AccountNameTest',1);
        insert accounts;
        Place__c place = Test_DataFactory.createPlaces('PNTest', 1)[0];
        place.LastName__c = 'LastName';
        place.Account__c = accounts[0].Id;
        insert place;
        
        PlacePrivate__c privatePlace = Test_DataFactory.createPrivatePlaces('PrivatePlaceNameTest', 1)[0];
        privatePlace.LastName__c = 'LastName';
        privatePlace.Account__c = accounts[0].Id;
        insert privatePlace;

        Category_Location__c category = Test_DataFactory.getCategoriesAndLocations('CategoryTest',ConstantsUtil.CATEGORY_RT_DEVELOPERNAME,1,null, null)[0];
        List<Category_Location__c> locations = Test_DataFactory.getCategoriesAndLocations('LocationTest',ConstantsUtil.LOCATION_RT_DEVELOPERNAME,2,null, null);
        insert category;
        insert locations;
        
        Category_Location__c catRelation = Test_DataFactory.getCategoriesAndLocations('CatRelationTest',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,category.Id, place.Id)[0];
        Category_Location__c locRelation = Test_DataFactory.getCategoriesAndLocations('LocRelationTest',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,locations[0].Id, place.Id)[0];
        locRelation.mainLocation__c = true;
        Category_Location__c locRelation2 = Test_DataFactory.getCategoriesAndLocations('LocRelationTest2',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,locations[1].Id, place.Id)[0];
        
        Category_Location__c catRelationP = Test_DataFactory.getCategoriesAndLocations('CatRelationTest',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,category.Id, privatePlace.Id)[0];
        Category_Location__c locRelationP = Test_DataFactory.getCategoriesAndLocations('LocRelationTest',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,locations[0].Id, privatePlace.Id)[0];
        locRelationP.mainLocation__c = true;
        Category_Location__c locRelationP2 = Test_DataFactory.getCategoriesAndLocations('LocRelationTest2',ConstantsUtil.RELATION_RT_DEVELOPERNAME,1,locations[1].Id, privatePlace.Id)[0];
        
        insert catRelation;
        insert locRelation;
        insert locRelation2;
        
        insert catRelationP;
        insert locRelationP;
        insert locRelationP2;
    }

    @IsTest
    static void test_getCategoriesAndLocationByPlace(){
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        //System.debug('Place: ' + place.Id + ' ' + place.Name);
        Map<String, List<String>> categoriesAndLocationsReturnedIT;
        Map<String, List<String>> categoriesAndLocationsReturnedDE;
        Map<String, List<String>> categoriesAndLocationsReturnedFR;
        Map<String, List<String>> categoriesAndLocationsReturnedEN;
        Map<String, List<String>> categoriesAndLocationsReturnedITprivatePlace;
        Map<String, List<String>> categoriesAndLocationsReturnedDEprivatePlace;
        Map<String, List<String>> categoriesAndLocationsReturnedFRprivatePlace;
        Map<String, List<String>> categoriesAndLocationsReturnedENprivatePlace;
        
        Test.startTest();
        categoriesAndLocationsReturnedIT = CategoryAndLocationController.getCategoriesAndLocationByPlace(place.Id, 'Place__c','it');
        categoriesAndLocationsReturnedEN = CategoryAndLocationController.getCategoriesAndLocationByPlace(place.Id, 'Place__c','en_US');
        categoriesAndLocationsReturnedFR = CategoryAndLocationController.getCategoriesAndLocationByPlace(place.Id, 'Place__c','fr');
        categoriesAndLocationsReturnedDE = CategoryAndLocationController.getCategoriesAndLocationByPlace(place.Id, 'Place__c','de');

        /*List<Category_Location__c> query = [SELECT Id, RecordTypeId, Category_de__c, Category_en__c, Category_fr__c, Category_it__c,
                                            Location_de__c, Location_en__c, Location_fr__c, Location_it__c, Category_Location__c, Place__c, PlacePrivate__c FROM Category_Location__c];
        System.debug('query: ' + JSON.serialize(query));*/
        
        //It has to be activated the 2nd version of delete_private_place flow
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        /*place.Place_Type__c = 'Private';
        
        Database.SaveResult sr = Database.update(place,dml);
        if(!sr.isSuccess())
            System.debug('Error --> ' + sr.getErrors());*/
        PlacePrivate__c privatePlace = [SELECT Id, Name FROM PlacePrivate__c LIMIT 1];
        System.debug('privatePlace: ' + privatePlace.Id + ' ' + privatePlace.Name);
        categoriesAndLocationsReturnedITprivatePlace = CategoryAndLocationController.getCategoriesAndLocationByPlace(privatePlace.Id, 'PlacePrivate__c','it');
        categoriesAndLocationsReturnedDEprivatePlace = CategoryAndLocationController.getCategoriesAndLocationByPlace(privatePlace.Id, 'PlacePrivate__c','de');
        categoriesAndLocationsReturnedFRprivatePlace = CategoryAndLocationController.getCategoriesAndLocationByPlace(privatePlace.Id, 'PlacePrivate__c','fr');
        categoriesAndLocationsReturnedENprivatePlace = CategoryAndLocationController.getCategoriesAndLocationByPlace(privatePlace.Id, 'PlacePrivate__c','en_US');
        Test.stopTest();
        
        /*List<Category_Location__c> query2 = [SELECT Id, RecordTypeId, Category_de__c, Category_en__c, Category_fr__c, Category_it__c,
                                            Location_de__c, Location_en__c, Location_fr__c, Location_it__c, Category_Location__c, Place__c, PlacePrivate__c FROM Category_Location__c];
        System.debug('query2: ' + JSON.serialize(query2));*/

        /*System.debug('categoriesAndLocationsReturnedIT.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedIT.get('C')));
        System.debug('categoriesAndLocationsReturnedEN.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedEN.get('C')));
        System.debug('categoriesAndLocationsReturnedFR.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedFR.get('C')));
        System.debug('categoriesAndLocationsReturnedDE.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedDE.get('C')));
        System.debug('categoriesAndLocationsReturnedIT.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedIT.get('L')));
        System.debug('categoriesAndLocationsReturnedEN.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedEN.get('L')));
        System.debug('categoriesAndLocationsReturnedFR.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedFR.get('L')));
        System.debug('categoriesAndLocationsReturnedDE.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedDE.get('L')));
		
        System.debug('categoriesAndLocationsReturnedITprivatePlace.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedITprivatePlace.get('C')));
        System.debug('categoriesAndLocationsReturnedENprivatePlace.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedENprivatePlace.get('C')));
        System.debug('categoriesAndLocationsReturnedFRprivatePlace.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedFRprivatePlace.get('C')));
        System.debug('categoriesAndLocationsReturnedDEprivatePlace.get(\'C\'): ' + JSON.serialize(categoriesAndLocationsReturnedDEprivatePlace.get('C')));
        System.debug('categoriesAndLocationsReturnedITprivatePlace.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedITprivatePlace.get('L')));
        System.debug('categoriesAndLocationsReturnedENprivatePlace.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedENprivatePlace.get('L')));
        System.debug('categoriesAndLocationsReturnedFRprivatePlace.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedFRprivatePlace.get('L')));
        System.debug('categoriesAndLocationsReturnedDEprivatePlace.get(\'L\'): ' + JSON.serialize(categoriesAndLocationsReturnedDEprivatePlace.get('L')));*/
        
        System.assertEquals('Category_it__c0', categoriesAndLocationsReturnedIT.get('C')[0]);
        System.assertEquals('Location_it__c0', categoriesAndLocationsReturnedIT.get('L')[0]);
        System.assertEquals('Category_en__c0', categoriesAndLocationsReturnedEN.get('C')[0]);
        System.assertEquals('Location_en__c0', categoriesAndLocationsReturnedEN.get('L')[0]);
        System.assertEquals('Category_fr__c0', categoriesAndLocationsReturnedFR.get('C')[0]);
        System.assertEquals('Location_fr__c0', categoriesAndLocationsReturnedFR.get('L')[0]);
        System.assertEquals('Category_de__c0', categoriesAndLocationsReturnedDE.get('C')[0]);
        System.assertEquals('Location_de__c0', categoriesAndLocationsReturnedDE.get('L')[0]);
        System.assertEquals('Location_it__c1', categoriesAndLocationsReturnedIT.get('L')[1]);
        System.assertEquals('Location_en__c1', categoriesAndLocationsReturnedEN.get('L')[1]);
        System.assertEquals('Location_fr__c1', categoriesAndLocationsReturnedFR.get('L')[1]);
        System.assertEquals('Location_de__c1', categoriesAndLocationsReturnedDE.get('L')[1]);
        

        System.assertEquals('Category_it__c0', categoriesAndLocationsReturnedITprivatePlace.get('C')[0]);
        System.assertEquals('Location_it__c0', categoriesAndLocationsReturnedITprivatePlace.get('L')[0]);
        System.assertEquals('Category_en__c0', categoriesAndLocationsReturnedENprivatePlace.get('C')[0]);
        System.assertEquals('Location_en__c0', categoriesAndLocationsReturnedENprivatePlace.get('L')[0]);
        System.assertEquals('Category_fr__c0', categoriesAndLocationsReturnedFRprivatePlace.get('C')[0]);
        System.assertEquals('Location_fr__c0', categoriesAndLocationsReturnedFRprivatePlace.get('L')[0]);
        System.assertEquals('Category_de__c0', categoriesAndLocationsReturnedDEprivatePlace.get('C')[0]);
        System.assertEquals('Location_de__c0', categoriesAndLocationsReturnedDEprivatePlace.get('L')[0]);
        System.assertEquals('Location_it__c1', categoriesAndLocationsReturnedITprivatePlace.get('L')[1]);
        System.assertEquals('Location_en__c1', categoriesAndLocationsReturnedENprivatePlace.get('L')[1]);
        System.assertEquals('Location_fr__c1', categoriesAndLocationsReturnedFRprivatePlace.get('L')[1]);
        System.assertEquals('Location_de__c1', categoriesAndLocationsReturnedDEprivatePlace.get('L')[1]);
    }
}