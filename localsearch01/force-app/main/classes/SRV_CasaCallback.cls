public without sharing class SRV_CasaCallback {
    /*
    * Method to retrieve params from Visualforce Callback CASA Page
    */
    public static CasaParams getInputParamsFromPage(PageReference pageRef){
       
        CasaParams casaParams = new CasaParams(pageRef.getParameters().get('category'),
                                                pageRef.getParameters().get('region'),
                                                pageRef.getParameters().get('slot'),                                                    
                                                pageRef.getParameters().get('language'),                        
                                                pageRef.getParameters().get('quote_item'),
                                                pageRef.getParameters().get('price'),       
                                                pageRef.getParameters().get('ad_context_allocation'),
                                               	pageRef.getParameters().get('product_code') 
                                                );   
        
        casaParams.start_date = (String.isNotBlank(pageRef.getParameters().get('start_date'))) ? Date.valueOf(pageRef.getParameters().get('start_date')) : null;
        casaParams.end_date = (String.isNotBlank(pageRef.getParameters().get('end_date'))) ? Date.valueOf(pageRef.getParameters().get('end_date')) : null;
        casaParams.expiration_date = (String.isNotBlank(pageRef.getParameters().get('expiration_date'))) ? Date.valueOf(pageRef.getParameters().get('expiration_date')) : null;
        System.debug('********************* '+casaParams.product_code);
        return casaParams;
    }

    public static Boolean publishPlatformEvent(CasaParams casaParams){
        
        Boolean success = true;
        
        List<Casa_Callback_Event__e> casaEvents = new List<Casa_Callback_Event__e>();
        
        casaEvents.add(new Casa_Callback_Event__e(Category__c = casaParams.category, 
                                                 Region__c = casaParams.region, 
                                                 Slot__c = casaParams.slot, 
                                                 Language__c = casaParams.language, 
                                                 Start_Date__c = casaParams.start_date, 
                                                 End_Date__c = casaParams.end_date, 
                                                 Expiration_Date__c = casaParams.expiration_date, 
                                                 Quote_Item__c = casaParams.quote_item, 
                                                 Price__c = casaParams.price,
                                                 Ad_Context_Allocation__c = casaParams.ad_context_allocation,
                                                 Success__c = true));
        
        try{
            List<Database.SaveResult> results = EventBus.publish(casaEvents);
            
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        throw new EventException('publishPlatformEvent '+err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
        catch(EventException evEx){
            ErrorHandler.logInfo('CasaCallback', '', evEx.getMessage());
            success = false;
        }
        
        return success;
    }
    
    public static Boolean publishErrorPlatformEvent(){
        
        Boolean success = true;
        
        List<Casa_Callback_Event__e> casaEvents = new List<Casa_Callback_Event__e>();
        
        casaEvents.add(new Casa_Callback_Event__e(Success__c = false));
        
        try{
            List<Database.SaveResult> results = EventBus.publish(casaEvents);
            
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        throw new EventException('publishPlatformEvent '+err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
        catch(EventException evEx){
            ErrorHandler.logInfo('CasaCallback', '', evEx.getMessage());
            success = false;
        }
        
        return success;
    }
    
    public static String formatDate(Date d) {
        return d.year() + '-' + String.valueof(d.month()+100).right(2) + '-' + String.valueof(d.day()+100).right(2);
    }
    
    public class CasaParams{
        public String category {get;set;}
        public String region {get;set;}
        public String slot {get;set;}
        public String language {get;set;}
        public Date start_date {get;set;}
        public Date end_date {get;set;}
        public Date expiration_date {get;set;}
        public String quote_item {get;set;}
        public String price {get;set;}
        public String ad_context_allocation {get;set;}
        public String product_code {get;set;}
        
        public CasaParams(String category, String region, String slot, String language, String quote_item, String price, String ad_context_allocation, String product_code){
            this.category = category;    
            this.region = region;
            this.slot = slot;
            this.language = language;
            this.quote_item = quote_item;
            this.price = price;
            this.ad_context_allocation = ad_context_allocation;
            this.product_code = product_code;
        }
    } 
    
    public class EventException extends Exception{}
}