/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2019-06-14
 * Sprint      : Hotfix 2
 * Work item   : AS-46
 * Testclass   : Billing_Test
 * Package     : 
 * Description : Avoid Too Many DML Rows exception
 * Changelog   : Used a batch to handle and reduce DML operation per Apex Invocation.
 */

public without sharing class SblInvoicesChangesBatch implements Database.Batchable<SObject>{
    
    public Map<String, BillingResults.InvoiceSummary> resultMap = new Map<String, BillingResults.InvoiceSummary>();

    public SblInvoicesChangesBatch(Map<String, BillingResults.InvoiceSummary> resultMap) {
        this.resultMap = resultMap;
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext){

        Set<String> invoiceIds = new Set<String>();
        invoiceIds.addAll(resultMap.keySet());

        String query=   'SELECT Id, '+
                        +'Correlation_Id__c, '
                        +'Open_Amount__c, '
                        +'Payment_Status__c, '+
                        +'Payment_Date__c,'+
                        +'Status__c, '+
            			+'Process_Mode__c, '+
                        +'Total__c ' +
                        +'FROM Invoice__c '+
                        +'WHERE Correlation_Id__c IN: invoiceIds' ;
        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext batchableContext, List<Invoice__c> scope) {
        system.debug('scope: ' + scope);
		Set<Id> invoiceToTerminateIds = new Set<Id>();
        for(Invoice__c invoice : scope) { 
            BillingResults.InvoiceSummary summary = resultMap.get(invoice.Correlation_Id__c);
            system.debug('Ldimartino - scope.timestamp: ' + summary.timestamp + ' scope.payment_status: ' + summary.payment_status);
            if(invoice.Payment_Status__c == summary.payment_status && invoice.Open_Amount__c == summary.open_amount) {
                continue;
            }
            invoice.Payment_Status__c = summary.payment_status;
            if(summary.payment_status == '0') {
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_PAYED;
                invoice.Payment_Date__c = summary.timestamp;
            }
            //Vincenzo Laudato 2019-06-04 AS-6 *** START
            else if(summary.payment_status.equals('1')){
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
            }
            //ldimartino, SF2-181 *** START
            else if(summary.payment_status.equals('S3') && invoice.Process_Mode__c == ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST){
                invoiceToTerminateIds.add(invoice.Id);
            }
            //ldimartino, SF2-181 *** END
            //Vincenzo Laudato 2019-06-04 *** END
            if(BillingHelper.OverduePaymentStatusList.contains(summary.payment_status)) {
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_OVERDUE;
            }
            invoice.Open_Amount__c = summary.open_amount;
            if(summary.payment_status.contains('C')){
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_CANCELLED;
            }
            if(summary.open_amount < invoice.Total__c && invoice.Status__c != ConstantsUtil.INVOICE_STATUS_CANCELLED){
                invoice.Payment_Date__c = summary.timestamp;
            }
        }
        if(invoiceToTerminateIds.size() > 0){
            SRV_Contract.terminateContractsForInvoices(invoiceToTerminateIds);
        }
        update scope;
	}

    public void finish(Database.BatchableContext batchableContext){

    }
}