@isTest
public class TrialUpdateBeforeActiveBatchSchTest {

    @isTest
    static void testTrialUpdateBeforeActive()
     {
         SBQQ__Quote__c quote = createQuoteWithQuoteLine();
         List<SBQQ__Quote__c> quoteList = new List<SBQQ__Quote__c>();
         quoteList.add(quote);
         
         List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>();
         
         for(SBQQ__QuoteLine__c ql : [SELECT TrialStartDate__c  FROM SBQQ__QuoteLine__c 
                                                  WHERE SBQQ__Quote__c = :quote.Id ] ) {
                        
                           listQLines.add(ql);                           
                    }
         
         update listQLines;
 
         Test.startTest();
         TrialUpdateBeforeActive_BatchSch obj = new TrialUpdateBeforeActive_BatchSch();
 
         try{
             DataBase.executeBatch(obj);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }
 
         Database.BatchableContext bc;
         try{
         	obj.execute(bc,listQLines);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }
 
         SchedulableContext sc;
         Try{
         	obj.execute(sc);
         }catch(Exception e){	                            
             System.Assert(False,e.getMessage());
         }       
         Test.stopTest();
         
     }
    
        @isTest
	 public static void createProducts() {
        List<Product2> listProds = new List<Product2>();
        for(Integer i = 0; i < 3; i++) {
         Product2 pr = new Product2();
           pr.Name = 'XXX ' + i;
           pr.ProductCode = 'SLT00'+i;
           pr.IsActive=True;
           pr.SBQQ__ConfigurationType__c='Allowed';
           pr.SBQQ__ConfigurationEvent__c='Always';
           pr.SBQQ__QuantityEditable__c=True;
            
           pr.SBQQ__NonDiscountable__c=False;
           pr.SBQQ__SubscriptionType__c='Renewable'; 
           pr.SBQQ__SubscriptionPricing__c='Fixed Price';
           pr.SBQQ__SubscriptionTerm__c=12; 
           
            listProds.add(pr);
        }
    insert listProds;
    }
    
    @isTest
    public static Product2 createMainProduct() {        
        createProducts();
        Product2 p = [SELECT Id, Name FROM Product2 WHERE ProductCode = 'SLT001' Limit 1];
        return p;
    }
  
    @isTest
  	public static List<Product2> createOptionProducts() {
        List<Product2> subProds = [SELECT Id, Name FROM Product2 WHERE ProductCode != 'SLT001' Limit 5];
        return subProds;
    }
    
    @isTest
    public static SBQQ__Quote__c createQuote() {
        Account acc = createAccount();
        
        Opportunity opp = new Opportunity(StageName = 'Draft', CloseDate = Date.today().AddDays(89), Account = acc, AccountId = acc.Id, Name = 'Test Opportunity xxx');
        	insert opp;
            System.Debug('opp '+ opp);
            
            opp.SBQQ__AmendedContract__c = null;           
            update opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc.Id, SBQQ__Opportunity2__c =opp.Id, SBQQ__Type__c = 'Quote', SBQQ__Status__c = 'Draft', SBQQ__ExpirationDate__c = Date.today().AddDays(89));
        insert quote;
        
        return quote;
        
    }
    
    @isTest
    public static SBQQ__Quote__c createQuoteWithQuoteLine() {
        SBQQ__Quote__c quote = createQuote();
        Product2 p = createMainProduct();
        SBQQ__QuoteLine__c mainQuoteLine = new SBQQ__QuoteLine__c(
            SBQQ__Quote__c=quote.Id, SBQQ__Product__c=p.Id, SBQQ__Quantity__c=1
        );

        insert mainQuoteLine;
        List<Product2> subProds = createOptionProducts();
        List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>();
        for(Product2 subProd : subProds) {
            listQLines.add(new SBQQ__QuoteLine__c(
                SBQQ__Quote__c=quote.Id, 
                SBQQ__Product__c=subProd.Id, 
                SBQQ__RequiredBy__c=mainQuoteLine.Id,
                SBQQ__Bundled__c = true,
                SBQQ__OptionType__c = 'Component',
                TrialStatus__c = 'Draft'
            ));
        }
        if(!listQLines.isEmpty()) insert listQLines;
        listQLines.add(mainQuoteLine);
        
        return quote;
    }
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'XXX ';
        acc.GoldenRecordID__c = 'GRID';
        
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh';    
                   
        insert acc;
        System.Debug('Account '+ acc); 
              
        return acc;
    }
}