@isTest
public class Test_PromotionalDiscountBatch {
    @testSetup
    public static void testSetup(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
        AccountTriggerHandler.disableTrigger = true;
		byPassFlow.FlowNames__c = 'Community User Creation,Set Contract Start/End date for Quote,Subscriptions Handler,Send Owner Notification';
		insert byPassFlow; 
        //SBQQ.TriggerControl.disable(); 
        createAccount();
        List<Account> accounts = [SELECT Id FROM Account where Active_Contracts__c != null];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
        }
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        createOpp(accounts[0].id);
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c LIMIT 1];
        q.Billing_Profile__c = profiles[0].Id;
        QuoteTriggerHandler.disableTrigger = true;
        update q;
        QuoteTriggerHandler.disableTrigger = false;
        system.debug('quote: ' + q);
        generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        generateLocalization(prod.Id);
        QuoteLineTriggerHandler.disableTrigger = true;
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        QuoteLineTriggerHandler.disableTrigger = false;
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        generateContract(q.Id, accounts[0].id, opp.Id);
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        c.Status = 'Active';
        update c;
        SubscriptionTriggerHandler.disableTrigger = true;
        generateSubs(c, qls);
        SubscriptionTriggerHandler.disableTrigger = false;
        List<SBQQ__Subscription__c> subs = [select Id, SBQQ__Contract__r.AccountId, SBQQ__Contract__c, Subsctiption_Status__c, Next_Invoice_Date__c, SBQQ__QuoteLine__c, SBQQ__NetPrice__c, SBQQ__Contract__r.SBQQ__Quote__r.Billing_Profile__c, SBQQ__Contract__r.SBQQ__Quote__c from SBQQ__Subscription__c ];
        system.debug('subs: ' + subs);
    }
	
    public static void createAccount(){
        Account ac = new Account();
        ac.Name = 'TestNameAccount';
        ac.AddressValidated__c = 'validated_address';
        ac.BillingCity = 'Zürich';
        ac.BillingCountry = 'Switzerland';
        ac.BillingPostalCode = '8005';
        ac.BillingStreet = 'Förrlibuckstrasse 62';
        ac.Customer_Number__c = '2000258872';
        ac.LegalEntity__c = 'AG';
        ac.P_O_Box_City__c = 'Buchberg';
        ac.P_O_Box_Zip_Postal_Code__c = '8454';
        ac.PreferredLanguage__c = 'German';
        ac.Status__c = 'New';
        ac.Active_DMC_Contracts__c =1;
        ac.Active_Contracts__c =1;
        insert ac;
    } 
    
    public static void createOpp(Id accId){
        Date today = Date.today();
        Opportunity opp = new Opportunity();
        opp.AccountId = accId;
        opp.CloseDate = today.addDays(15);
        opp.Name = 'TestNameOpp';
        opp.StageName = 'Accepted';
        insert opp;
    }
    
    public static void generateQuoteLine(String quoteId, String productId, Date startDate, String requiredById, String billingFreq){
		SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__StartDate__c = startDate;
		quoteLine.CategoryID__c = 'testcategoryid';
        quoteLine.Category__c = 'Category Name';
        quoteLine.LocationID__c = 'testlocationid';
        quoteLine.Location__c = 'Location Name';
        quoteLine.SBQQ__Quantity__c= 1;
        quoteLine.SBQQ__ListPrice__c = 1290;
        quoteLine.Total__c = 490;
        quoteLine.Ad_Context_Allocation__c = 'testadcontextallocationid';
        quoteLine.Campaign_Id__c = 'testcampaignid';
        quoteLine.Contract_Ref_Id__c = 'testcontractrefid';
        quoteLine.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
        quoteLine.SBQQ__RequiredBy__c = requiredById;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.SBQQ__BillingFrequency__c =  billingFreq;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.Subscription_Term__c = '12';
		insert quoteLine;
	}
    
    public static void generateProduct(Boolean trueOrFalseOneTime){
        Product2 product = new Product2();
        //Required fields to fill
        product.Name = 'MyCOCKPIT Basic';
        product.Product_Group__c = 'MyCockpit';
        product.ProductCode = 'MCOBASIC001';
        product.ExternalKey_ProductCode__c = 'BCB';
        product.Family = 'Tool & Services';
        product.One_time_Fee__c = trueOrFalseOneTime;
        product.SBQQ__Component__c = false;
        product.SBQQ__NonDiscountable__c = false;
        product.SBQQ__SubscriptionTerm__c = 12;
        product.SBQQ__SubscriptionType__c = 'Renewable';
        product.Billing_Group__c = 'Monthly';
        product.Configuration_Model__c = 'Auto-renewal';
        //product.Subscription_Term__c = {'12'};
        product.Base_term_Renewal_term__c = '12';
        product.Event_Type__c = 'On Boarding';
        
        //Fields needed for Order Management Process
        product.Manual_Activation__c = true;
        product.Production__c = false;
        product.Priority__c = '10';
		//if(fieldApiNameToValue.containsKey('Upselling_Disable__c')) product.Upselling_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Upselling_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Downselling_Disable__c')) product.Downselling_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Downselling_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Upgrade_Disable__c')) product.Upgrade_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Upgrade_Disable__c'));
        //if(fieldApiNameToValue.containsKey('Downgrade_Disable__c')) product.Downgrade_Disable__c = Boolean.valueOf(fieldApiNameToValue.get('Downgrade_Disable__c'));
        
        //Only for Print Products
        /*product.Adv_Width__c = fieldApiNameToValue.get('Adv_Width__c');
        product.Adv_Height__c = fieldApiNameToValue.get('Adv_Height__c');
        product.Adv_Type__c = fieldApiNameToValue.get('Adv_Type__c');
        product.Adv_Page_Type__c = fieldApiNameToValue.get('Adv_Page_Type__c');*/
        //End only for print products
        //End fields needed for Order Management Process
        
        //Standard values
        product.PlaceIDRequired__c = 'No';
        product.SBQQ__PricingMethod__c = ConstantsUtil.PRICING_METHOD;
        product.SBQQ__SubscriptionPricing__c = ConstantsUtil.SUB_PRICING;
        product.BillingChannel__c = ConstantsUtil.PROD_BILLING_CHANNEL;
        product.InvoiceCycleSet__c = ConstantsUtil.PROD_INVOICE_CYCLE_SET;
        product.SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL;
        product.SBQQ__TaxCode__c = ConstantsUtil.TAX_MODE;
        product.Period_of_notice__c = ConstantsUtil.PERIOD_NOTICE;
        product.IsActive = true;
        
        //Fields needed for billing
        product.KTRCode__c = '1202010005';
        product.KTRDesignation__c = 'MyCockpit Basic';
        product.KSTCode__c = '8932';
        product.Credit_Account__c = '334301';
        //End fields needed for billing
        
        //Needed fields for Trial
        product.Eligible_for_Trial__c = false;
        //End needed fields for Trial 
        
        insert product;
    }
    
    public static void generateContract(Id quoteId, Id AccountId, Id oppId){
        Contract c = new Contract();
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        c.SBQQ__Opportunity__c = oppId;
        c.SBQQ__Quote__c = quoteId;
        c.AccountId = AccountId;
        insert c;
    }
    
    public static void generateSubs(Contract contr, List<SBQQ__QuoteLine__c> quoteLines){
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, SBQQ__BillingType__c='Advance',
                                                        SBQQ__ChargeType__c = 'Recurring', SBQQ__Discount__c=0.0, SBQQ__ListPrice__c=quoteLine.SBQQ__ListPrice__c, 
                                                        SBQQ__Number__c = quoteLine.SBQQ__Number__c, SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c = FALSE, 
                                                        SBQQ__Product__c = quoteLine.SBQQ__Product__c, SBQQ__Quantity__c = quoteLine.SBQQ__Quantity__c, SBQQ__QuoteLine__c = quoteLine.Id, 
                                                        Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.today(), SBQQ__SubscriptionEndDate__c=null,
                                                        SBQQ__Contract__c = contr.Id, SBQQ__Account__c = contr.AccountId, SBQQ__Bundled__c = FALSE, Status__c = 'Active', Total__c = 500, Subscription_Term__c='12'));
        }
        insert subscriptions;
    }
    
    public static void generateLocalization(Id prodId){
        SBQQ__Localization__c loc = new SBQQ__Localization__c();
        loc.SBQQ__APIName__c = 'Name';
        loc.SBQQ__Label__c = 'Product Name';
        loc.SBQQ__Language__c = 'it';
        loc.SBQQ__Text__c = 'testTrad';
        loc.SBQQ__Product__c = prodId;
        insert loc;
    }
    
    private static testmethod void invoiceCreationForIdAndMultipleSubscriptions(){
        Test.startTest();
        List<SBQQ__Subscription__c> subscriptions = [SELECT Id, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c, SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c, SBQQ__Discount__c FROM SBQQ__Subscription__c];
        List<String> statusList = new List<String>();
        statusList.add('Cancelled');
        statusList.add('Expired');
		statusList.add('Terminated');
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        PromotionalDiscountAlignmentBatch batch = new PromotionalDiscountAlignmentBatch(statusList);
        batch.start(null);
        batch.execute(null,subscriptions);
        batch.finish(null);
        Test.stopTest();
    }
}