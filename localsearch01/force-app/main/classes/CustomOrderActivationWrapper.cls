/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author	  : Antonio Esposito <antesposito@deloitte.it>
* Date	  	: 07-11-2019
* Sprint      : Sprint 11
* Work item   : Feedback #95
* Testclass   :
* Package     : 
* Description : Controller for Custom Order Activation component
* Changelog   :
*
*/
public class CustomOrderActivationWrapper {
   /* @AuraEnabled
    public String ordStatus;*/
    @AuraEnabled
    public boolean isFullActivation;
    @AuraEnabled
    public String ordItemId;
    @AuraEnabled
    public OrderItem ordItem;
    @AuraEnabled
    public List<childElementType> childElement;
    @AuraEnabled
    public List<ItemElement> ordItemElement;
   /* @AuraEnabled
    public boolean isAmendment;*/
    @AuraEnabled
    public boolean isSecondActivation;
    @AuraEnabled
    public String status;
   	@AuraEnabled
    public String productCode;
    @AuraEnabled
    public String serializedChildElement;
    @AuraEnabled
    public String serializedordItemElement;
    @AuraEnabled
    public boolean hideChild;
    
    public class childElementType{
        @AuraEnabled
        public List<ItemElement> items;
        @AuraEnabled
        public Id childId;
        @AuraEnabled
        public Boolean activateLater;
        @AuraEnabled
        public Boolean hideItem;
    }
    public class ItemElement{
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String fieldLabel;
        @AuraEnabled
        public Boolean isEditable;
        @AuraEnabled
        public String fieldType;
        @AuraEnabled
        public String helpText;
    }
   
    public CustomOrderActivationWrapper(){
	//ordStatus='';
    ordItem = null;
    childElement= null;
    ordItemElement= null;
    status='';
    }
    
    public class activationResponse{
        
        @AuraEnabled
        public Boolean result;
        @AuraEnabled
        public String message;
        @AuraEnabled
        public String exceptionMessage;
        @AuraEnabled
        public String title;
    }
    
}