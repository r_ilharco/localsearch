@istest public class Test_TemplateFooterVFPageController {

static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
}

    @testsetup static void setup(){
  insertbypassflownames();
    Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
    insert acc;
    List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
    insert cont;
    opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];  
    insert opp;
    SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
    insert quote;
    SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
    template.Name = ConstantsUtil.TEMPLATENAME_SUBSTRING_TELESALES;
    template.SBQQ__TermsConditions__c = 'Die Preise werden nach Aufschaltung bzw. Publikation des jeweiligen Produkts monatlich bzw. jährlich in Rechnung gestellt. Einmalige Gebühren werden nach Vertragsabschluss in Rechnung gestellt.Es gelten die jeweils aktuellen Produktbeschreibungen auf www.localsearch.ch';
    insert template;


}

@isTest
    public static void testTemplateFooterVFPageCntrllr(){
        SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        Test.startTest();
        apexpages.currentpage().getparameters().put('qid' , quote.id);
        apexpages.currentpage().getparameters().put('tid' , template.id);
        apexpages.currentpage().getparameters().put('language' , 'fr');
        TemplateFooterVFPageController tFVFPC = new TemplateFooterVFPageController();
       
        Test.stopTest();
    }

}
