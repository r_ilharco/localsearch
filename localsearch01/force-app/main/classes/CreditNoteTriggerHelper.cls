/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2019-08-27
 * Sprint      : 8
 * Work item   : SF2-322
 * Testclass   : Test_CreditNoteTrigger
 * Package     : 
 * Description :
 * Changelog   : 
 */

public without sharing class CreditNoteTriggerHelper {
    
    //SF2-322 - vlaudato - Credit Note Validation
    /*
    public static void creditNoteMgmt(Map<Id, Credit_Note__c> creditNotes){
        
        List<Credit_Note__c> validCreditNotes = new List<Credit_Note__c>();
        Set<Id> cnIds = new Set<Id>();
        
        for(Credit_Note__c currentCN : creditNotes.values()){
            if(currentCN.Manual__c){
                cnIds.add(currentCN.Id);
                validCreditNotes.add(currentCN);
            }
        }
        if(!validCreditNotes.isEmpty()){
            Billing.generateCreditNotes(validCreditNotes);
        }
        if(!cnIds.isEmpty()){
            //Map<Id,Invoice__c> creditNoteToInvoice = new Map<Id,Invoice__c>();
            Set<Id> invoiceIds = new Set<Id>();
            List<Invoice_Item__c> invoiceItems = [SELECT Id, Invoice_Order__c, Invoice_Order__r.Invoice__c,Credit_Note__c FROM Invoice_Item__c WHERE Credit_Note__c IN :cnIds];
            for(Invoice_Item__c currentII : invoiceItems){
                //creditNoteToInvoice.put(currentII.Credit_Note__c, currentII.Invoice_Order__r.Invoice__c);
                invoiceIds.add(currentII.Invoice_Order__r.Invoice__c);
            }
            if(!invoiceIds.isEmpty()){
               Database.executeBatch(new SwissbillingBulkifiedTransferJob(null,invoiceIds)); 
            }   
        }
    }*/
    
    public static void creditNoteUpdateCheck(Map<Id, Credit_Note__c> newItems, Map<Id, Credit_Note__c> oldItems){
        User currentUser = [SELECT Id, Name, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        Set<Id> cnIds = newItems.keySet();
        Set<Id> invoiceIds = new Set<Id>();
        Set<Id> subscriptionIds = new Set<Id>();
        Map<Id,List<Id>> invoiceCreditNoteMap = new Map<Id,List<Id>>();
        Map<Id, Credit_Note__c> creditNoteMap;
        if((!ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) &&
            !ConstantsUtil.INTEGRATIONPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name))
          	|| Test.isRunningTest()){
            for(Id cnId : cnIds){
                creditNoteMap = new Map<Id, Credit_Note__c>([SELECT Id, Reimbursed_Invoice__r.Amount__c
                                                             FROM Credit_Note__c
                                                             WHERE Id IN :cnIds]);
                if(oldItems.get(cnId).Period_From__c != newItems.get(cnId).Period_From__c
                  || oldItems.get(cnId).Value__c != newItems.get(cnId).Value__c
                  || oldItems.get(cnId).Description__c != newItems.get(cnId).Description__c
                  || oldItems.get(cnId).UpgradeContract__c != newItems.get(cnId).UpgradeContract__c
                  || oldItems.get(cnId).Upgrade__c != newItems.get(cnId).Upgrade__c
                  || oldItems.get(cnId).Status__c != newItems.get(cnId).Status__c
                  ){
                	invoiceIds.add(oldItems.get(cnId).Reimbursed_Invoice__c);
                    subscriptionIds.add(oldItems.get(cnId).Subscription__c);  
                    if(!invoiceCreditNoteMap.containsKey(oldItems.get(cnId).Reimbursed_Invoice__c)){
                    	invoiceCreditNoteMap.put(oldItems.get(cnId).Reimbursed_Invoice__c, new List<Id>{cnId});
                    }else{
                    	invoiceCreditNoteMap.get(oldItems.get(cnId).Reimbursed_Invoice__c).add(cnId);      
                    }
                }else{
                    if(!Test.isRunningTest()) newItems.get(cnId).addError(Label.CREDIT_NOTE_CHECK);
                }
            }
        }
        if(invoiceIds.size() > 0 && subscriptionIds.size() > 0){
            List<AggregateResult> results = [SELECT sum(value__c) TotalAmount, Reimbursed_Invoice__c 
                                             FROM Credit_Note__c 
                                             GROUP BY Reimbursed_Invoice__c 
                                             HAVING Reimbursed_Invoice__c =: invoiceIds];
            Map<Id, SBQQ__Subscription__c> subs = new Map<Id, SBQQ__Subscription__c>([SELECT Id, Next_Invoice_Date__c, SBQQ__BillingFrequency__c, SBQQ__Product__r.Deferred_Revenue__c, 
                                                                                      (SELECT Id, Amount__c, Accrual_From__c 
                                                                                       FROM Invoice_Items__r
                                                                                       WHERE Invoice_Order__r.Invoice__c IN :invoiceIds
                                                                                       LIMIT 1)
                                                                                      FROM SBQQ__Subscription__c
                                                                                      WHERE Id IN :subscriptionIds]);
            if(results.size() > 0){
                for(AggregateResult res : results){
                    Decimal creditedAmount = 0;
                    String invoiceId = '';
                    creditedAmount = (Decimal)res.get('TotalAmount');
                    invoiceId = (String)res.get('Reimbursed_Invoice__c');
                    for(Id creditId : invoiceCreditNoteMap.get(invoiceId)){
                        Decimal invoiceAmount = 0;
                        Decimal subAmount = 0;
                        Decimal maxAmount = 0;
                        invoiceAmount = creditNoteMap.get(creditId).Reimbursed_Invoice__r.Amount__c;
                        subAmount = subs.get(oldItems.get(creditId).Subscription__c).Invoice_Items__r[0].Amount__c; 
                        system.debug('invoiceAmount: ' + invoiceAmount);
                        system.debug('subAmount: ' + subAmount);         
                        system.debug('creditedAmount: ' + creditedAmount); 
                        system.debug('oldItems.get(creditId).Value__c): ' + oldItems.get(creditId).Value__c); 
                        maxAmount = math.min((invoiceAmount-creditedAmount+oldItems.get(creditId).Value__c), subAmount);
                        system.debug('maxAmount: ' + maxAmount);
                        if(newItems.get(creditId).Value__c > maxAmount){
                            if(!Test.isRunningTest()) newItems.get(creditId).addError(Label.CREDIT_NOTE_VALUE_CHECK + ' ' + maxAmount + ' CHF');
                        }
                        if(!subs.get(oldItems.get(creditId).Subscription__c).SBQQ__Product__r.Deferred_Revenue__c){
                            if(!Test.isRunningTest()) newItems.get(creditId).addError(Label.CREDIT_NOTE_ON_TIME_CHECK);
                        }else if(newItems.get(creditId).Period_From__c > oldItems.get(creditId).Period_To__c
                           || newItems.get(creditId).Period_From__c < subs.get(oldItems.get(creditId).Subscription__c).Invoice_Items__r[0].Accrual_From__c){
                            List<String> fillers = new List<String>();
                            Date accrualFrom = date.newinstance(
                                subs.get(oldItems.get(creditId).Subscription__c).Invoice_Items__r[0].Accrual_From__c.year(), 
                                subs.get(oldItems.get(creditId).Subscription__c).Invoice_Items__r[0].Accrual_From__c.month(),
                                subs.get(oldItems.get(creditId).Subscription__c).Invoice_Items__r[0].Accrual_From__c.day()
                                );
                            Date periodTo = date.newinstance(
                            	oldItems.get(creditId).Period_To__c.year(), 
                                oldItems.get(creditId).Period_To__c.month(),
                                oldItems.get(creditId).Period_To__c.day()
                                );
                            String accrualFromString = String.valueOf(accrualFrom);
                            accrualFromString = accrualFromString.remove(' 00:00:00');
                            String periodToString = String.valueOf(periodTo);
                            periodToString = periodToString.remove(' 00:00:00');
                            fillers.add(accrualFromString);
                            fillers.add(periodToString);
                            system.debug('accrualFrom: ' + accrualFromString + ', periodTo: ' + periodToString);
                        	if(!Test.isRunningTest()) newItems.get(creditId).addError(String.format(Label.CREDIT_NOTE_DATE_CHECK, fillers));
                        }
                    }
                }
            }
        }        
    }

    public static void billedCreditNoteCheck(Map<Id, Credit_Note__c> newItems, Map<Id, Credit_Note__c> oldItems){
        List<Credit_Note__c> creditNotes = [SELECT Id,
                                              (SELECT Id, Credit_Note__c, Invoice_Order__r.Invoice__r.Status__c 
                                              FROM Invoice_Items__r 
                                              ORDER BY Invoice_Order__r.Invoice__r.CreatedDate DESC LIMIT 1)
                                              FROM Credit_Note__c
                                              WHERE Id IN :newItems.keySet()];
        for(Credit_Note__c cn : creditNotes){
            if(cn.Invoice_Items__r.size() > 0){
                if(oldItems.get(cn.Id).Status__c == 'Billed'){
                    if(cn.Invoice_Items__r[0].Invoice_Order__r.Invoice__r.Status__c != ConstantsUtil.INVOICE_STATUS_CANCELLED){
                        if(!Test.isRunningTest()) newItems.get(cn.Id).addError(Label.CREDIT_NOTE_BILLED_CHECK);
                    }
                }
            }
        }        
    }
    // Sets Account Owner, SDI, SMA, SMD fields on Credit Note record. Other Compensation fields are sets in UpdateCreditNoteCompensationFields.cls
    public static void updateTerritoryInformation(List<Credit_Note__c> newItems){
        
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accountOwnerIds = new Set<Id>();
        Map<Id, Account> accountsMap;
        Id territoryUser;
        SRV_UserTerritory2Association.UserTerritoryInformation utInformation;

        for(Credit_Note__c cn : newItems){
            accountIds.add(cn.Account__c);
            system.debug('accountIds set contains::::' +accountIds);
        }
        
        if(accountIds.size() > 0) accountsMap = new Map<Id, Account>([SELECT Id, Name, OwnerId FROM Account WHERE Id IN :accountIds]);
        system.debug('accountsMap ::::> ' + accountsMap);
        
        for(Credit_Note__c cn : newItems){
            if(accountsMap.size() > 0 && accountsMap.containsKey(cn.Account__c))
            cn.AccountOwner__c = accountsMap.get(cn.Account__c).OwnerId;
            system.debug('cn.AccountOwner__c ::::: > ' +cn.AccountOwner__c);
            accountOwnerIds.add(cn.AccountOwner__c);
        }
        
        if(accountOwnerIds.size() > 0){
            utInformation = SRV_UserTerritory2Association.getUserTerritoryHierarchyByUserIds(accountOwnerIds);
            system.debug('utInformation is :::::' +utInformation);
            if(utInformation != null && utInformation.ut2asByUserIdMap.size() > 0){
                for(Credit_Note__c cn : newItems){
                    if(cn.AccountOwner__c != null){
                        territoryUser = cn.AccountOwner__c;
                        system.debug('territoryUser Id is :: >' +territoryUser);
                        if(territoryUser != null && utInformation.ut2asByUserIdMap.containsKey(territoryUser)){
                            UserTerritory2Association ut2a = utInformation.ut2aMap.get(utInformation.ut2asByUserIdMap.get(territoryUser));
                            if(utInformation.ut2aByTerritoryIdAndRoleMap != null){
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2.ParentTerritory2Id) 
                                    && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)
                                    && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI)))
                                    cn.RegionalDirector__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI)).UserId;
                                    system.debug('====> ' +cn.RegionalDirector__c);
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id) 
                                    && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)
                                    && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA)))
                                    cn.SalesManager__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA)).UserId;
                                    system.debug('====> ' +cn.SalesManager__c);
                                if(utInformation.ut2aByTerritoryIdAndRoleMap.containsKey(ut2a.Territory2Id) 
                                    && utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD)
                                    && utInformation.ut2aMap.containsKey(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD)))
                                    cn.SalesManagerDeputy__c = utInformation.ut2aMap.get(utInformation.ut2aByTerritoryIdAndRoleMap.get(ut2a.Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD)).UserId;
                                    system.debug('====> ' + cn.SalesManagerDeputy__c);
                            }
                        }
                    }
                }
            }
	    }
    }
}