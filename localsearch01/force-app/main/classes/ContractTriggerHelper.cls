/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2019-10-01
 * Sprint      : 10
 * Work item   : SF2-336
 * Testclass   : Test_ContractTrigger
 * Package     : 
 * Description :
 * ─────────────────────────────────────────────────────────────────────────────────────────────────
@changes		  SPIII-4307 - New Customer Flag - change calculation logic
				  set as blank the Account.LastInactiveDate field in case of new activation
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-11-20
* ─────────────────────────────────────────────────────────────────────────────────────────────────
@changes		  SPIII-5901 - New Customer Flag - 
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2021-03-11
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class ContractTriggerHelper {
    /*public static void openAmountCalculation(Map<Id, Contract> contracts){
        Set<Id> contractIds = new Set<Id>();
        contractIds = contracts.keyset();
        Map<ID, List<SBQQ__Subscription__c>> contractSubscriptionsMap = new Map<ID, List<SBQQ__Subscription__c>>();
        List<Contract> contractsToUpdate = new List<Contract>();
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        subscriptions = [SELECT Id,SBQQ__Contract__c, SBQQ__Quantity__c, SBQQ__NetPrice__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c IN :contractIds];
        if(subscriptions.size()>0){
            for(SBQQ__Subscription__c currentSub : subscriptions){
                if(!contractSubscriptionsMap.containsKey(currentSub.SBQQ__Contract__c)){
                    List<SBQQ__Subscription__c> subscriptionsToAdd = new List<SBQQ__Subscription__c>{currentSub};
                    contractSubscriptionsMap.put(currentSub.SBQQ__Contract__c, subscriptionsToAdd);
                }else{
                    contractSubscriptionsMap.get(currentSub.SBQQ__Contract__c).add(currentSub);
                }
            }
        }
        for(Id contractID : contractSubscriptionsMap.keySet()){
            Decimal openAmount = 0;
            for(SBQQ__Subscription__c sub : contractSubscriptionsMap.get(contractID)){
                Decimal subAmount = 0;
                subAmount = (sub.SBQQ__Quantity__c * sub.SBQQ__NetPrice__c);
                openAmount += subAmount; 
            }
            contracts.get(contractID).Open_Amount__c = openAmount;
            contractsToUpdate.add(contracts.get(contractID));
        }
        if(contractsToUpdate.size() > 0){
            update contractsToUpdate;
        }
    }*/
	
    public static List<Contract> setFieldforMigration(List<Contract> newItems){
        for(Contract c : newItems){
            if(String.isNotBlank(c.Pricebook2Id)){
                c.SBQQ__AmendmentPricebookId__c = c.Pricebook2Id;
            }
        }
        return newItems;
    }
    
    public static void upgradedContractCreditNotesCheck(Map<Id, Contract> newItems, Map<Id, Contract> oldItems){
        system.debug('LDIMARTINO newItems: ' + newItems);
        system.debug('LDIMARTINO oldItems: ' + oldItems);
        Set<Id> contractIds = newItems.keySet();
        Set<Id> quoteIds = new Set<Id>();
        Map<Id,Id> quoteToContractMap = new Map<Id,Id>();
        Map<Id,Id> subscriptionToContractMap = new Map<Id,Id>();
        Map<Id, Contract> contracts = new Map<Id, Contract>([SELECT Id, Status, SBQQ__Opportunity__r.Upgrade__c, SBQQ__Quote__c FROM Contract WHERE Id IN :contractIds]);
        for(Contract c : contracts.values()){
            if( newItems.get(c.Id).Status != oldItems.get(c.Id).Status && newItems.get(c.Id).Status == ConstantsUtil.CONTRACT_STATUS_ACTIVE && c.SBQQ__Opportunity__r.Upgrade__c ){
                quoteIds.add(c.SBQQ__Quote__c);
                quoteToContractMap.put(c.SBQQ__Quote__c, c.Id);
            }
        }
        if(quoteIds.size() > 0){
            Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([SELECT Id, (SELECT Id, SBQQ__Quote__c, Subscription_To_Terminate__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c WHERE Id IN :quoteIds]);
            for(SBQQ__Quote__c q : quotes.values()){
                for(SBQQ__QuoteLine__c ql : q.SBQQ__LineItems__r){
                    subscriptionToContractMap.put(ql.Subscription_To_Terminate__c, quoteToContractMap.get(ql.SBQQ__Quote__c));
                }
            }
            if(subscriptionToContractMap.size() > 0){
                SRV_Contract.creditNotesUpdate(subscriptionToContractMap);
            }
        }		        
    }
    
    public static void getAccountsToIncreaseDecreaseActiveContracts(Map<Id, Contract> newItems, Map<Id, Contract> oldItems){
        
        Map<Id, List<Boolean>> accountsToIncreaseActiveContracts = new Map<Id, List<Boolean>>();
        Map<Id, List<Boolean>> accountsToDecreaseActiveContracts = new Map<Id, List<Boolean>>();
        
        for(Contract currentContract : newItems.values()){
            
            Boolean statusChangedToActive = ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(currentContract.Status) && !ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(oldItems.get(currentContract.Id).Status);
            Boolean statusChangedActiveToCancelled = ConstantsUtil.CONTRACT_STATUS_CANCELLED.equalsIgnoreCase(currentContract.Status) && ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(oldItems.get(currentContract.Id).Status);
            Boolean statusChangedActiveToTerminated = ConstantsUtil.CONTRACT_STATUS_TERMINATED.equalsIgnoreCase(currentContract.Status) && ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(oldItems.get(currentContract.Id).Status);
            Boolean statusChangedActiveToExpired = ConstantsUtil.CONTRACT_STATUS_EXPIRED.equalsIgnoreCase(currentContract.Status) && ConstantsUtil.CONTRACT_STATUS_ACTIVE.equalsIgnoreCase(oldItems.get(currentContract.Id).Status);
        	
            if(statusChangedToActive){
                if(!accountsToIncreaseActiveContracts.containsKey(currentContract.AccountId)) accountsToIncreaseActiveContracts.put(currentContract.AccountId, new List<Boolean>{currentContract.DMC_Pricebook__c});
                else accountsToIncreaseActiveContracts.get(currentContract.AccountId).add(currentContract.DMC_Pricebook__c);
            }
            else if(statusChangedActiveToCancelled || statusChangedActiveToTerminated || statusChangedActiveToExpired){
                if(!accountsToDecreaseActiveContracts.containsKey(currentContract.AccountId)) accountsToDecreaseActiveContracts.put(currentContract.AccountId, new List<Boolean>{currentContract.DMC_Pricebook__c});
                else accountsToDecreaseActiveContracts.get(currentContract.AccountId).add(currentContract.DMC_Pricebook__c);
            }
        }
        List<Account> accountsToUpdate = updateAccounts(accountsToIncreaseActiveContracts, accountsToDecreaseActiveContracts);
        if(!accountsToUpdate.isEmpty()) update accountsToUpdate;
    }
    
    @TestVisible
    private static List<Account> updateAccounts(Map<Id, List<Boolean>> accountsToIncreaseActiveContracts, Map<Id, List<Boolean>> accountsToDecreaseActiveContracts){
        
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
            
        Set<Id> accountsId = new Set<Id>();
        if(!accountsToIncreaseActiveContracts.isEmpty()) accountsId.addAll(accountsToIncreaseActiveContracts.keySet());
        if(!accountsToDecreaseActiveContracts.isEmpty()) accountsId.addAll(accountsToDecreaseActiveContracts.keySet());
        
        if(!accountsId.isEmpty()){
            List<Account> accounts = [SELECT Id, Active_Contracts__c, Active_DMC_Contracts__c FROM Account WHERE Id IN :accountsId];
            for(Account currentAccount : accounts){
                if(accountsToIncreaseActiveContracts.containsKey(currentAccount.Id)){
                    for(Boolean isDMC : accountsToIncreaseActiveContracts.get(currentAccount.Id)){
                        currentAccount.Active_Contracts__c = currentAccount.Active_Contracts__c + 1;
                        if(isDMC) currentAccount.Active_DMC_Contracts__c = currentAccount.Active_DMC_Contracts__c + 1;
                    }
                }
                if(accountsToDecreaseActiveContracts.containsKey(currentAccount.Id)){
                    for(Boolean isDMC : accountsToDecreaseActiveContracts.get(currentAccount.Id)){
                        currentAccount.Active_Contracts__c = currentAccount.Active_Contracts__c - 1;
                        if(isDMC) {
                            currentAccount.Active_DMC_Contracts__c = currentAccount.Active_DMC_Contracts__c - 1;
                            if(currentAccount.Active_DMC_Contracts__c == 0) currentAccount.Last_Inactive_Date__c = Date.today();
                            else currentAccount.Last_Inactive_Date__c = null;
                        }
                    }
                }
                accountsToUpdate.put(currentAccount.Id, currentAccount);
            }
        }
        if(!accountsToUpdate.isEmpty()) return accountsToUpdate.values();
        else return new List<Account>();
    }
}
