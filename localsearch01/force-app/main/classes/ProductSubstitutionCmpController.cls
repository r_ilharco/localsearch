/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class explaining the purpose.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-08-12
* @systemLayer    Invocation/Lightning Component Controller of ProductSubstitutionCmp
* @TestClass      Test_ProductSubstitutionCmpController
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Gennaro Casola
* 2020-08-12      SPIII-1599 - Process of Substitution
*				  SPIII-2607 - Link of old subscription and new quoteline related to same place
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mara Mazzarella
* 2020-09-30      SPIII-3455 - onePRESENCE Replacement Process - blockProductSellingOnSamePlace
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*
* @changes
* @modifiedby     Telmo Batista
* 2020-11-25      SPIII-4364 Fields for Performance calculation for Replacement
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class  ProductSubstitutionCmpController {
    private static CurrentUserInfo currentUserInfo = new CurrentUserInfo();
    
    @AuraEnabled
    public static List<SBQQ__Subscription__c> getActiveMasterSubsFromAccount(Id accountId) {
        List<SBQQ__Subscription__c> activeMasterSubs;
        if(accountId != NULL){
        	activeMasterSubs = SEL_Subscription.getActiveMasterSubsFromAccount(accountId);
        }
        
        return activeMasterSubs;
    }
    
    @AuraEnabled
    public static List<SBQQ__Subscription__c> getActiveDraftMasterSubsFromAccount(Id accountId, Date startDate, Id quoteId) {
        List<SBQQ__Subscription__c> rawMasterSubs;
        List<SBQQ__QuoteLine__c> parentQuoteLines;
        List<SBQQ__Subscription__c> masterSubsToDisplay = new List<SBQQ__Subscription__c>();
        List<Date> qlStartDateList = new List<Date>();
        Date qlStartDate;
        Date qlMinStartDate;
        
        if(accountId != null){
            rawMasterSubs = SEL_Subscription.getMasterSubsFromAccountandStatus(accountId, startDate, new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE,ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT,ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION});
            parentQuoteLines = SEL_SBQQQuoteLine.getParentQLinesByQuoteId(quoteId);
                if (rawMasterSubs.Size() > 0 && parentQuoteLines.Size() > 0){
                    // gather QL minimum start date
                    for(SBQQ__QuoteLine__c ql : parentQuoteLines){
                        if(ql.Quote_Type__c == ConstantsUtil.QUOTE_TYPE_REPLACEMENT && ql.SBQQ__StartDate__c != null)
                            qlStartDate = ql.SBQQ__StartDate__c;
                            qlStartDateList.add(qlStartDate);
                    }
                
                if(qlStartDateList.size() > 0){
                    qlStartDateList.sort();
                    qlMinStartDate = qlStartDateList.get(0);                    
                }

                    for(SBQQ__Subscription__c sub : rawMasterSubs){
                        //if((!sub.SBQQ__Contract__r.SBQQ__Evergreen__c && sub.SBQQ__EndDate__c > qlMinStartDate) || (sub.SBQQ__Contract__r.SBQQ__Evergreen__c && sub.End_Date__c > qlMinStartDate)){
                            masterSubsToDisplay.add(sub);
                        //}
                    }
                } 
        }
        
        return masterSubsToDisplay;
    }
        
    @AuraEnabled
    public static RemoteActionResponse updateReplacedSubscriptions(Map<Id, Id> qlIdsBySubscriptionIdMap,string quoteId) {
        system.debug('updateReplacedSubscription::qlIdsBySubscriptionIdMap::'+qlIdsBySubscriptionIdMap);
        system.debug('updateReplacedSubscription::quoteId::'+quoteId);
        RemoteActionResponse rar = new RemoteActionResponse();
        List<SBQQ__Subscription__c> subscriptionsToUpdateList = new List<SBQQ__Subscription__c>();
        
        System.debug('====> ' + JSON.serialize(qlIdsBySubscriptionIdMap));
        for(Id subscriptionId : qlIdsBySubscriptionIdMap.keySet()){
            SBQQ__Subscription__c sub = new SBQQ__Subscription__c();
            sub.Id = subscriptionId;
            sub.Replaced_by_Product__c = qlIdsBySubscriptionIdMap.get(subscriptionId);
            sub.Termination_Generate_Credit_Note__c = true;
            subscriptionsToUpdateList.add(sub);
        }
        
        if(subscriptionsToUpdateList != NULL && subscriptionsToUpdateList.size() > 0){
            List<Database.SaveResult> srs = Database.update(subscriptionsToUpdateList);
            if(srs != NULL && srs.size() > 0){
                rar.info.status = 'OK';
                for(Database.SaveResult sr : srs){
                    if(!sr.isSuccess()){
                        system.debug('sr::'+sr);
                        rar.info.status = 'ERROR';
                        break;
                    }
                }
                if(rar.info.status != 'ERROR'){
                	try{ 
                        QuoteManager.CalculateQuoteDiff(quoteId, true);
                    } catch(Exception ex){ rar.info.status = 'ERROR'; }
                }
            }
        }else{
            rar.info.status = 'ERROR';
        }

        return rar;
    }
    
    @AuraEnabled
    public static Map<Id, SBQQ__QuoteLine__c> getQuoteLineMapByQuoteId(Id quoteId){
        Map<Id, SBQQ__QuoteLine__c> quoteLinesMap;
        quoteLinesMap = SEL_SBQQQuoteLine.getParentQLinesByQuoteId(new Set<Id>{quoteId});
        return quoteLinesMap;
    }
    
    @AuraEnabled
    public static Map<Id, List<SBQQ__QuoteLine__c>> getQuoteLineMapByQuoteId01(string quoteId){
        String accountId;
        Date startDate;
        List<SBQQ__Quote__c> quote = new List<SBQQ__Quote__c>();
        
        if(quoteId != null){
            quote = [Select Id,SBQQ__Account__c,SBQQ__StartDate__c from SBQQ__Quote__c where Id = : quoteId ];
        }
        if(!quote.IsEmpty()){
            accountId = quote[0].SBQQ__Account__c;
            startDate= quote[0].SBQQ__StartDate__c;
        }
        
        Map<Id, SBQQ__QuoteLine__c> rawQlMap;
        List<SBQQ__Subscription__c> masterSubsToDisplay;
        Date subEndDate;
        Date evergeenSubEndDate;
        Map<Id, List<SBQQ__QuoteLine__c>> quoteLinesToDisplay = new Map<Id, List<SBQQ__QuoteLine__c>>();

        if(quoteId != null){
        rawQlMap = SEL_SBQQQuoteLine.getParentQLinesByQuoteId(new Set<Id>{quoteId});
        masterSubsToDisplay = getActiveDraftMasterSubsFromAccount(accountId, startDate, quoteId);
            if(rawQlMap.size() > 0 && masterSubsToDisplay.size() > 0){
                for(SBQQ__Subscription__c sub : masterSubsToDisplay){
                    if(!sub.SBQQ__Contract__r.SBQQ__Evergreen__c){
                        subEndDate = sub.SBQQ__EndDate__c;
                    }
                    if(sub.SBQQ__Contract__r.SBQQ__Evergreen__c){
                        evergeenSubEndDate = sub.End_Date__c;
                    }
                    List<SBQQ__QuoteLine__c> qlToDisplay = new List<SBQQ__QuoteLine__c>();
                        for(SBQQ__QuoteLine__c ql : rawQlMap.values()){
                            if(!sub.SBQQ__Contract__r.SBQQ__Evergreen__c && ql.SBQQ__StartDate__c <= subEndDate){                                
                                qlToDisplay.add(ql);
                            }
                            if(sub.SBQQ__Contract__r.SBQQ__Evergreen__c/* && ql.SBQQ__StartDate__c < evergeenSubEndDate*/){
                                qlToDisplay.add(ql);
                            }
                        }
                    if(!quoteLinesToDisplay.containsKey(sub.Id)){
                        quoteLinesToDisplay.put(sub.id, qlToDisplay);
                    }
                }
            }
        }
        return quoteLinesToDisplay;

    }
    
     @AuraEnabled
    public static Map<Id,Map<Id,String>> blockProductSellingOnSamePlace(Id quoteId){
        return SRV_QuoteLine.blockProductSubstituionByPlace(quoteId);
    }
     
    @AuraEnabled
    public static SBQQ__Quote__c getQuote(Id quoteId){
        SBQQ__Quote__c quote;
        if(quoteId != NULL){
            Map<Id, SBQQ__Quote__c> quotesMap = SEL_Quote.getQuoteById(new Set<Id>{quoteId});
            if(quotesMap != NULL && quotesMap.size() == 1)
                quote = quotesMap.values()[0];
        }
        return quote;
    }
    
    @AuraEnabled
    public static Map<Id, SBQQ__Subscription__c> getActiveSubscriptionFromAccount(Id AccountId){
        Map<Id, SBQQ__Subscription__c> subscriptionsMap;
        
        return subscriptionsMap;
    }
    
    @AuraEnabled
    public static ComponentAccessResponse getAccess(Id recordId)
    {
        ComponentAccessResponse caResponse = new ComponentAccessResponse();
        caResponse.isUserAllowed = true;
        caResponse.isComponentEnabled = true;
        Map<Id, SBQQ__Quote__c> quotesMap = SEL_Quote.getQuoteById(new Set<Id>{recordId});
        Boolean isUserAllowed = currentUserInfo.dmcUser || currentUserInfo.adminUser || currentUserInfo.digitalAgencyUser || currentUserInfo.teleSalesUser;
        
        if(!isUserAllowed){
            caResponse.isUserAllowed = false;
            caResponse.isComponentEnabled = false;
            caResponse.info.errorMessages.add(Label.Namirial_UI_ErrorMessage_NoSufficientPermissions);
        }else if(quotesMap != NULL && quotesMap.size() == 1){
            SBQQ__Quote__c quote = quotesMap.values()[0];
            Map<Id, SBQQ__QuoteLine__c> quoteLinesMap = SEL_SBQQQuoteLine.getParentQLinesByQuoteId(new Set<Id>{quote.Id});
            if(!ConstantsUtil.QUOTE_TYPE_REPLACEMENT.equalsIgnoreCase(quote.SBQQ__Type__c)){
                caResponse.isUserAllowed = false;
                caResponse.isComponentEnabled = false;
                caResponse.info.errorMessages.add(Label.ProductSubstitution_Replacement_QuoteType_Required);
            }else if(quoteLinesMap == NULL || (quoteLinesMap != NULL && quoteLinesMap.size() == 0)){
                caResponse.isUserAllowed = false;
                caResponse.isComponentEnabled = false;
                caResponse.info.errorMessages.add(Label.ProductSubstitution_AtLeast_One_QuoteLine_Needed);
            }
        }
        
        return caResponse;
    }
    
    public virtual class UIResponseMessage{
        @AuraEnabled public Info info;
        
        UIResponseMessage()
        {
            this.info = new Info();
        }
    }
    
    public class Info{
        @AuraEnabled public List<String> messages;
        @AuraEnabled public String status;
        @AuraEnabled public String errorCode;
        @AuraEnabled public List<String> errorMessages;
        
        Info()
        {
            this.messages = new List<String>();
            this.errorMessages = new List<String>();
        }
    }
    
    public class RemoteActionResponse extends UIResponseMessage{
        
    }
    
    public class ComponentAccessResponse extends UIResponseMessage{
        @AuraEnabled public Boolean isUserAllowed;
        @AuraEnabled public Boolean isComponentEnabled;
        @AuraEnabled public CurrentUserInfo currentUserInfo;
        
        ComponentAccessResponse(){
            currentUserInfo = new CurrentUserInfo();
        }
    }
    
    public class CurrentUserInfo{
        @AuraEnabled public Boolean dmcUser;
        @AuraEnabled public Boolean teleSalesUser;
        @AuraEnabled public Boolean salesUser;
        @AuraEnabled public Boolean adminUser;
        @AuraEnabled public Boolean digitalAgencyUser;
        
        CurrentUserInfo(){
            User currentUser = [SELECT Id, Name, Profile.Name, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Label IN (:ConstantsUtil.DMCPERMISSIONSET_LABEL)) FROM User WHERE Id = :UserInfo.getUserId()];
            this.dmcUser = false;
            this.teleSalesUser = false;
        	this.salesUser = false;
        	this.adminUser = false;
            this.digitalAgencyUser = false;
            
            if(ConstantsUtil.TELESALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
            	this.teleSalesUser = true;
            }else if(ConstantsUtil.SALESPROFILE_LABEL.equals(currentUser.Profile.Name)){
                this.salesUser = true;
            }else if(ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                this.adminUser = true;
            }else if(ConstantsUtil.DIGITALAGENCYPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                this.digitalAgencyUser = true;
            }
            
            if(currentUser.PermissionSetAssignments.size() > 0){
                this.dmcUser = true;
        	}
        }
    }

}