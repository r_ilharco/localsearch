global class BatchCloseExpiredOpportunities implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    
    global database.querylocator start(Database.BatchableContext bc) {
        
        Date dt_from = Date.valueOf(System.Today()) - (1);
        string query = 'Select Id,StageName,Opportunity_reason__c,AO_Contract__c,Amount,Telesales_opportunity_Expiration_Date__c,Contract_EndDate__c,AccountId,Telesales_Campaign_ID__c,SBQQ__RenewedContract__c, (Select Id from SBQQ__Quotes2__r) from Opportunity WHERE Type =\'' + ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS + '\' AND Expiration_Date__c  = '+string.valueOf(dt_from).substringBefore(' ');// AND (StageName =\'' + ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION + '\' OR StageName  =\'' + ConstantsUtil.OPPORTUNITY_STAGE_VALIDATION + '\')';
        return Database.getQueryLocator(query);  
        
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope){
        system.debug('scope :: '+scope.size());
        Map<Id,Opportunity> opportunityMap = new  Map<Id,Opportunity>();
        List<Opportunity> oppToClose = new List<Opportunity>();
        set<Id> updatedOppId = new set<Id>();
        if(!scope.isEmpty()){
            for(Opportunity opp : scope){
                if(opp.StageName == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST){
                    opportunityMap.put(opp.Id, opp);
                    updatedOppId.add(opp.Id);
                }else if((opp.SBQQ__Quotes2__r.size() == 0 && (opp.StageName == ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION || opp.StageName == ConstantsUtil.OPPORTUNITY_STAGE_VALIDATION))){
                    opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST;
                    opp.Opportunity_reason__c = ConstantsUtil.OPPORTUNITY_REASON_EXPIRED;
                    oppToClose.add(opp);
                    opportunityMap.put(opp.Id, opp); 
                }
            }
            system.debug('oppToClose ::'+oppToClose.size());  
            if(!oppToClose.isEmpty()){
                Database.SaveResult[]  resultOppUpdate =  Database.update(oppToClose,false);
                
                for (Database.SaveResult sr_opp : resultOppUpdate) {
                    system.debug('sr_opp.isSuccess() :: '+sr_opp.isSuccess());
                    if(sr_opp.isSuccess()) {
                        System.debug('Successfully Updated OPPORTUNITY: ' + sr_opp.getId());
                        updatedOppId.add(sr_opp.getId());
                    }else{
                        for(Database.Error err : sr_opp.getErrors()) { 
                            System.debug('Error: '+ err.getStatusCode() + ' *****' + err.getMessage());
                        }  
                    }   
                }    
            }
            if(!updatedOppId.isEmpty()){

                    Database.SaveResult[]  resultoppinsert =  Database.insert(createOpportunityLogic(opportunityMap,updatedOppId),false);
                    for (Database.SaveResult sr_opp : resultoppinsert) {
                        system.debug('sr_opp.isSuccess() :: '+sr_opp.isSuccess());
                        if(sr_opp.isSuccess()) {
                            System.debug('Successfully Created OPPORTUNITY: ' + sr_opp.getId());
                        }else{
                            for(Database.Error err : sr_opp.getErrors()) { 
                                System.debug('Error: '+ err.getStatusCode() + ' *****' + err.getMessage());
                            }  
                        }   
                    }
                    
                }  
            
            
        }        
        
    }
    
    public static List<Opportunity> createOpportunityLogic( Map<Id,Opportunity> opportunityMap,set<Id> updatedOppId ){
        List<Opportunity> opportunityList = new List<Opportunity>();
        Automated_Opportunity_Settings__c ao = Automated_Opportunity_Settings__c.getOrgDefaults();
        Id ownerId = (Id)ao.Owner_ID__c ;

        for(Id oppId : opportunityMap.keySet()){
            system.debug('oppId :: '+oppId);
            Opportunity opp = new Opportunity();
            opp.DMC_Opportunity_Closed_Reason__c = opportunityMap.get(oppId).Opportunity_reason__c;
            opp.CampaignId = opportunityMap.get(oppId).Telesales_Campaign_ID__c;
            opp.AccountId = opportunityMap.get(oppId).AccountId;
            opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION;
            opp.CloseDate =  (opportunityMap.get(oppId).Contract_EndDate__c - (Integer.valueOf(opportunityMap.get(oppId).Telesales_opportunity_Expiration_Date__c)));
            opp.Automated_Opportunity_Flow__c = ConstantsUtil.OPPORTUNITY_FLOW_Telesales;
            opp.SBQQ__RenewedContract__c = opportunityMap.get(oppId).SBQQ__RenewedContract__c; // SPIII-5215
            if(ownerId != null) opp.OwnerId = ownerId;
            opp.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS; 
            opp.Amount = opportunityMap.get(oppId).Amount;
            opp.Name = Label.Automated_opportunity+opportunityMap.get(oppId).AO_Contract__c;
            system.debug('Opp ::: '+opp); 
            opportunityList.add(opp);
        }
        
        
        
        
        
        return opportunityList;
    }
    
    
    global void finish(Database.BatchableContext bc){
        
        
    }  
    
    global void execute(SchedulableContext sc) {
        //BatchCloseExpiredOpportunities CloseExpiredOpportunities = new BatchCloseExpiredOpportunities(); 
        database.executebatch(this, 200);
    }
    
    public static String sched = '0 0 1 * * ?';
    global static string scheduleMe() {
        string name = 'BatchCloseExpiredOpportunities';
        if (Test.isRunningTest() ) {
            name = name + system.now();
        }
        return scheduleMe(name, sched) ;      
    }
    
    global static string scheduleMe(string name,String schedule) {
        return System.schedule(name, schedule, new BatchCloseExpiredOpportunities());       
    }
    
    
}