/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 19-07-2019
 * Sprint      : 5
 * User Story  : SF-210
 * Testclass   : 
 * Package     : 
 * Description : Order Management
 * Changelog   : 
 */

public without sharing class SEL_Order {

    public static List<Order> getChildOrder(Set<Id> orderIds,List<String> status, List<String> types, String processType){
         return  [SELECT    Id,  
                            Status, 
                            EndDate, 
                            Parent_Order__c,
                 			Custom_Type__c,
                  			Type
                    FROM    Order 
                    WHERE   Parent_Order__c in: orderIds 
                    AND     Status in: status
                  	AND     Type in: types 
                  	AND     Custom_Type__c =: processType
                    ];
    }
    
    
    public static Order getOrderByExternalId(String idCall){
        List<Order> orders = [  SELECT   
                                        Account.Name,
                              			Account.Customer_Number__c,
                                        Id, 
                                        External_Id__c, 
                                        Status, 
                                        Type, 
                                        EndDate, 
                                        SBQQ__Quote__c,
                                        SBQQ__Contracted__c,
                      			        ContractId,
                      			        Number_of_Products__c,
                                        OpportunityId,
                                        Opportunity.SBQQ__AmendedContract__c,
                                        Parent_Order__c,
                              			Custom_Type__c,
                              			AccountId,
                              			CallId__c,
                              			Contract__c
                                FROM    Order 
                                WHERE   CallId__c =: idCall 
                                LIMIT   1];
        if(!orders.isEmpty()){
            return orders[0];
        }
        else{
            return null;
        }
    }

    public static List<Order> getOrderById(Id orderId){
        return  [   SELECT  Id, 
                            External_Id__c, 
                            Status, 
                            Type, 
                            EndDate, 
                            SBQQ__Quote__c,
                            SBQQ__Contracted__c,
                            ContractId,
                            Number_of_Products__c,
                            OpportunityId,
                            Opportunity.SBQQ__AmendedContract__c,
                            OriginalOrderId,
                 			Custom_Type__c,
                 			AccountId,
                 			Account.Name,
                 			Account.Customer_Number__c,
                 			CallId__c,
                 			SBQQ__Quote__r.External_Id__c
                    FROM    Order 
                    WHERE   Id = :orderId 
                    LIMIT   1];
    }

    public static List<Order> getActivationOrdersByContractId(Set<Id> contractIds){
        return  [   SELECT  Id, 
                            External_Id__c, 
                            Status, 
                            Type, 
                            EndDate, 
                            SBQQ__Quote__c,
                            SBQQ__Contracted__c,
                            ContractId,
                            Number_of_Products__c,
                            OpportunityId,
                            Opportunity.SBQQ__AmendedContract__c,
                            OriginalOrderId,
                 			Custom_Type__c,
                 			AccountId,
                 			Account.Name,
                 			CallId__c
                    FROM    Order 
                    WHERE   Contract__c IN :contractIds
                    AND     (Custom_Type__c = :ConstantsUtil.ORDER_TYPE_ACTIVATION OR Custom_Type__c = :ConstantsUtil.ORDER_TYPE_TERMINATION OR Custom_Type__c = :ConstantsUtil.ORDER_TYPE_CANCELLATION)
                 	AND		Type != :ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL
                 	AND		Status = :ConstantsUtil.ORDER_STATUS_ACTIVATED];
    }
    
    public static List<Order> getActivationOrdersForAmendByContractId(Set<Id> orderIds){
        return  [   SELECT  Id, 
                            External_Id__c, 
                            Status, 
                            Type, 
                            EndDate, 
                            SBQQ__Quote__c,
                            SBQQ__Contracted__c,
                            ContractId,
                            Number_of_Products__c,
                            OpportunityId,
                            Opportunity.SBQQ__AmendedContract__c,
                            OriginalOrderId,
                 			Custom_Type__c,
                 			AccountId,
                 			Account.Name,
                 			CallId__c
                    FROM    Order 
                    WHERE   Contract__c IN :orderIds
                    AND     (Custom_Type__C = :ConstantsUtil.ORDER_TYPE_ACTIVATION 
                    AND 	(Type = :ConstantsUtil.ORDER_TYPE_NEW OR Type = :ConstantsUtil.ORDER_TYPE_UPGRADE))
                	AND		Manual_Amendment_Count__c < 1];
    }

    public static List<Order> getOrdersByIds(List<Id> recordIds){
        return [SELECT  Id, 
                        Custom_Type__c,
                        Manual_Activation_Roll__c,
                		Partial_Roll__c,
                		Partial_Amendment_Roll__c,
                        Type,
                		OpportunityId,
                        OrderNumber, 
                        External_Id__c,  
                        CallId__c,
                        Status, 
                        OwnerId, 
                        EffectiveDate, 
                        EndDate, 
                        AccountId,
                        Account.Name,
                        SBQQ__Quote__c,
                        Pricebook2Id,
                		SBQQ__Quote__r.Upgrade_Downgrade_Contract__r.SBQQ__OpportunityPricebookId__c, 
                        SBQQ__Quote__r.Upgrade_Downgrade_Contract__c, 
                        Call_API__c,
                        Account.Customer_Number__c,
                        Contract__c,
                		SBQQ__Quote__r.SBQQ__Opportunity2__c,
                		SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__AmendedContract__c,
                		SBQQ__Quote__r.External_Id__c,
                		SBQQ__Quote__r.Name,
                		ContractId,
                		Parent_Order__c,
                		Contract__r.Phase1Source__c,
                		Contract__r.Status,
                		SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.Phase1Source__c,
                		Partial_Termination__c,
                		Contract__r.SBQQ__Evergreen__c
                FROM    Order 
                WHERE   Id =: recordIds];
    }
    
     public static List<Order> getActivationOrdersByContractIds(Set<Id> contractIds){
        return [SELECT  Id, 
                        External_Id__c,
                		Contract__c,
                		ContractSAMBA_Key__c,
                		SambaIsStartConfirmed__c,
                		SBQQ__Quote__c,
                		SBQQ__Quote__r.Name
                FROM    Order 
                WHERE   Contract__c IN :contractIds
                AND		(Type = :ConstantsUtil.ORDER_TYPE_NEW OR Type = :ConstantsUtil.ORDER_TYPE_UPGRADE)
                AND		Custom_Type__c = :ConstantsUtil.ORDER_TYPE_ACTIVATION];
    }
    
    public static List<Order> getOrderByIds(Set<Id> orderIds){
        return  [   SELECT  Id, 
                            External_Id__c, 
                            Status, 
                            Type, 
                            EndDate, 
                            SBQQ__Quote__c,
                			SBQQ__Quote__r.Name,
                            SBQQ__Contracted__c,
                            ContractId,
                            Number_of_Products__c,
                            OpportunityId,
                            Opportunity.SBQQ__AmendedContract__c,
                            OriginalOrderId,
                 			Custom_Type__c,
                 			AccountId,
                 			Account.Name,
                 			Account.Customer_Number__c,
                 			CallId__c,
                 			SBQQ__Quote__r.External_Id__c,
                            OrderNumber, 
                            OwnerId, 
                            EffectiveDate, 
                            Pricebook2Id,
                            SBQQ__Quote__r.Upgrade_Downgrade_Contract__r.SBQQ__OpportunityPricebookId__c, 
                            SBQQ__Quote__r.Upgrade_Downgrade_Contract__c, 
                            Call_API__c,
                            Contract__c,
                            SBQQ__Quote__r.SBQQ__Opportunity2__c,
                            SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__AmendedContract__c,
                            Parent_Order__c,
                            Contract__r.Phase1Source__c,
                            SBQQ__Quote__r.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.Phase1Source__c,
                            Partial_Termination__c,
                            Contract__r.SBQQ__Evergreen__c
                    FROM    Order 
                    WHERE   Id IN :orderIds 
                    LIMIT   1];
    }
}