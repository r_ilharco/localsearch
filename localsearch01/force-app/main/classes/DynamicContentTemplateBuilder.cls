/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 08-16-2019
 * Sprint      : Sprint 7
 * Work item   : SF2-321, Sprint 7, Wave D - Quote and Contract Template
 * Testclass   : 
 * Package     : 
 * Description : Script for the generation of the Dynamic Quote/Contract PDF Template
 * Changelog   :  SF2-345, Sprint 9, Wave E - Update of Quote and Contract Template - gcasola - 09-17-2019
 */

public class DynamicContentTemplateBuilder {
	public static String nameTest = 'Quote Template 2020 v1.0';
    public static String refIdLSBanner = '0151r000007RERK';
    
	public static void start()
    {
        start(DynamicContentTemplateBuilder.nameTest);
    }
	    
    public static void start(String templateName)
    {
        String refIdLogo = '0151r000007RERF';
        Map<String, Id> contentVersionsByPDFLanguage = new Map<String,Id>{'IT' => '0681r00000JMOgLAAX', 'FR' => '0681r00000JMOgQAAX', 'DE' => '0681r00000JMOgMAAX'};
        Map<Id, ContentVersion> contentVersionsMap = new Map<Id, ContentVersion>([SELECT Id, Title, VersionData FROM ContentVersion WHERE ID IN :contentVersionsByPDFLanguage.values()]);
        SBQQ__QuoteTemplate__c dynamicContentTemplate = new SBQQ__QuoteTemplate__c();
        
        SBQQ__TemplateSection__c templateHeaderSection = new SBQQ__TemplateSection__c();
        SBQQ__TemplateSection__c templateContentQuoteLinesSection = new SBQQ__TemplateSection__c();
        SBQQ__TemplateSection__c templateFooterSection = new SBQQ__TemplateSection__c();
        SBQQ__TemplateSection__c templatePDFSection = new SBQQ__TemplateSection__c();
        
        SBQQ__TemplateContent__c logoHeader = new SBQQ__TemplateContent__c();
        SBQQ__TemplateContent__c templateHeader = new SBQQ__TemplateContent__c();
        SBQQ__TemplateContent__c templateContentQuoteLines = new SBQQ__TemplateContent__c();
        SBQQ__TemplateContent__c templateFooter = new SBQQ__TemplateContent__c();
        SBQQ__TemplateContent__c templateLSFooterBanners = new SBQQ__TemplateContent__c();
        
        logoHeader.Name = 'dynamicContent_LogoHeader';
        logoHeader.SBQQ__CustomSource__c = URL.getOrgDomainUrl().toExternalForm().split('\\.')[0] + '--c.visualforce.com/apex/' + 'LogoHeaderVFPage';
        logoHeader.SBQQ__Type__c = 'Custom';
        insert logoHeader;
        
        templateHeader.Name = 'dynamicContent_TemplateHeader';
        templateHeader.SBQQ__CustomSource__c = URL.getOrgDomainUrl().toExternalForm().split('\\.')[0] + '--c.visualforce.com/apex/' + 'TemplateHeaderVFPage';
        templateHeader.SBQQ__Type__c = 'Custom';
        insert templateHeader;
        
        templateContentQuoteLines.Name = 'dynamicContent_TemplateContentQuoteLines';
        templateContentQuoteLines.SBQQ__CustomSource__c = URL.getOrgDomainUrl().toExternalForm().split('\\.')[0] + '--c.visualforce.com/apex/' + 'TemplateContentQuoteLinesVFPage';
        templateContentQuoteLines.SBQQ__Type__c = 'Custom';
        insert templateContentQuoteLines;
        
        templateFooter.Name = 'dynamicContent_TemplateFooter';
        templateFooter.SBQQ__CustomSource__c = URL.getOrgDomainUrl().toExternalForm().split('\\.')[0] + '--c.visualforce.com/apex/' + 'TemplateFooterVFPage';
        templateFooter.SBQQ__Type__c = 'Custom';
        insert templateFooter;
        
        templateLSFooterBanners.Name = 'dynamicContent_TemplateLSFooterBanners';
        templateLSFooterBanners.SBQQ__CustomSource__c = URL.getOrgDomainUrl().toExternalForm().split('\\.')[0] + '--c.visualforce.com/apex/' + 'TemplateFooterBannersVFPage';
        templateLSFooterBanners.SBQQ__Type__c = 'Custom';
        insert templateLSFooterBanners;
        
        for(String language : contentVersionsByPDFLanguage.keySet())
        {
            dynamicContentTemplate = new SBQQ__QuoteTemplate__c();
            dynamicContentTemplate.Name = templateName + ' - ' + language;
            dynamicContentTemplate.SBQQ__HeaderContent__c  = logoHeader.Id;
            dynamicContentTemplate.SBQQ__FooterContent__c = templateLSFooterBanners.Id;
            dynamicContentTemplate.SBQQ__HeaderHeight__c  = 80;
            dynamicContentTemplate.SBQQ__TopMargin__c = 0.65;
            dynamicContentTemplate.SBQQ__LeftMargin__c = 0.87;
            dynamicContentTemplate.SBQQ__RightMargin__c = 0.87;
            dynamicContentTemplate.SBQQ__BottomMargin__c = 1;
            dynamicContentTemplate.SBQQ__PageHeight__c = 11.69;
            dynamicContentTemplate.SBQQ__PageWidth__c = 8.27;
            dynamicContentTemplate.SBQQ__PageNumberText__c = '{0}/{1}';
            dynamicContentTemplate.SBQQ__PageNumberPosition__c = 'Footer';
            dynamicContentTemplate.SBQQ__PageNumberAlignment__c = 'Right';
            dynamicContentTemplate.SBQQ__TermsConditions__c = Label.contract_template_quoteTerms;
            dynamicContentTemplate.SBQQ__FontFamily__c = ConstantsUtil.QUOTETEMPLATE_FONT;
            dynamicContentTemplate.SBQQ__GroupFontFamily__c = ConstantsUtil.QUOTETEMPLATE_FONT;
            
            if(!String.isEmpty(refIdLogo) && Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(refIdLogo).matches())
            {
                if(Database.countQuery('SELECT COUNT() FROM Document WHERE Id = \'' + refIdLogo + '\'') == 1)
                {
                    dynamicContentTemplate.SBQQ__LogoDocumentId__c = refIdLogo;
                }
            }
            
            /*CompanyInfo info = new CompanyInfo();
            info.name = 'Company Name';
            info.slogan = 'Company Slogan';
            info.phone = 'Company Phone';
            info.fax = 'Company Fax';
            info.email = 'company@email.com';
            info.street = 'Company Street';
            info.city = 'Company City';
            info.state = 'Company State';
            info.postalCode = 'Company Postal Code';
            info.country = 'Company Country';
            
            dynamicContentTemplate.SBQQ__CompanyCity__c = info.city;
            dynamicContentTemplate.SBQQ__CompanyCountry__c = info.country;
            dynamicContentTemplate.SBQQ__CompanyEmail__c = info.email;
            dynamicContentTemplate.SBQQ__CompanyFax__c = info.fax;
            dynamicContentTemplate.SBQQ__CompanyName__c = info.name;
            dynamicContentTemplate.SBQQ__CompanyPhone__c = info.phone;
            dynamicContentTemplate.SBQQ__CompanyPostalCode__c = info.postalCode;
            dynamicContentTemplate.SBQQ__CompanySlogan__c = info.slogan;
            dynamicContentTemplate.SBQQ__CompanyState__c = info.state;
            dynamicContentTemplate.SBQQ__CompanyStreet__c = info.street;*/
            
            dynamicContentTemplate.SBQQ__DeploymentStatus__c = 'Deployed';
            
            insert dynamicContentTemplate;
            
            List<SBQQ__Localization__c> quoteTermsLocalizations = new List<SBQQ__Localization__c>();
            quoteTermsLocalizations.add(new SBQQ__Localization__c(SBQQ__QuoteTemplate__c = dynamicContentTemplate.Id, SBQQ__APIName__c = 'SBQQ__TermsConditions__c', SBQQ__Language__c = 'fr', SBQQ__LongTextArea__c = Label.contract_template_quoteTerms_fr));
            quoteTermsLocalizations.add(new SBQQ__Localization__c(SBQQ__QuoteTemplate__c = dynamicContentTemplate.Id, SBQQ__APIName__c = 'SBQQ__TermsConditions__c', SBQQ__Language__c = 'de', SBQQ__LongTextArea__c = Label.contract_template_quoteTerms_de));
            quoteTermsLocalizations.add(new SBQQ__Localization__c(SBQQ__QuoteTemplate__c = dynamicContentTemplate.Id, SBQQ__APIName__c = 'SBQQ__TermsConditions__c', SBQQ__Language__c = 'it', SBQQ__LongTextArea__c = Label.contract_template_quoteTerms_it));
            insert quoteTermsLocalizations;
           	
            templateHeaderSection = new SBQQ__TemplateSection__c();
            templateHeaderSection.Name = 'TemplateHeaderSection';
            templateHeaderSection.SBQQ__Template__c = dynamicContentTemplate.Id;
            templateHeaderSection.SBQQ__Content__c = templateHeader.Id;
            templateHeaderSection.SBQQ__DisplayOrder__c = 1;
            insert templateHeaderSection;
            
            templateContentQuoteLinesSection = new SBQQ__TemplateSection__c();
            templateContentQuoteLinesSection.Name = 'TemplateContentQuoteLinesSection';
            templateContentQuoteLinesSection.SBQQ__Template__c = dynamicContentTemplate.Id;
            templateContentQuoteLinesSection.SBQQ__Content__c = templateContentQuoteLines.Id;
            templateContentQuoteLinesSection.SBQQ__DisplayOrder__c = 2;
            insert templateContentQuoteLinesSection;
            
            templateFooterSection = new SBQQ__TemplateSection__c();
            templateFooterSection.Name = 'TemplateFooterSection';
            templateFooterSection.SBQQ__Template__c = dynamicContentTemplate.Id;
            templateFooterSection.SBQQ__Content__c = templateFooter.Id;
            templateFooterSection.SBQQ__DisplayOrder__c = 3;
            insert templateFooterSection;
            
            templatePDFSection = new SBQQ__TemplateSection__c();
            templatePDFSection.Name = 'PDF_' + language;
            templatePDFSection.SBQQ__Template__c = dynamicContentTemplate.Id;
            templatePDFSection.SBQQ__DisplayOrder__c = 4;
            insert templatePDFSection;
            
            if(contentVersionsMap != NULL && contentVersionsMap.containsKey(contentVersionsByPDFLanguage.get(language)))
            {
                SBQQ__RelatedContent__c rc = new SBQQ__RelatedContent__c();
                
                Attachment att = new Attachment();
                att.Body = contentVersionsMap.get(contentVersionsByPDFLanguage.get(language)).VersionData;
                att.Name = contentVersionsMap.get(contentVersionsByPDFLanguage.get(language)).Title;
                att.ParentId = templatePDFSection.Id; 
                insert att;
                
                rc.Name = contentVersionsMap.get(contentVersionsByPDFLanguage.get(language)).Title;
                rc.SBQQ__Required__c = true;
                rc.SBQQ__ExternalId__c = att.Id;
                rc.SBQQ__TemplateSection__c = templatePDFSection.Id;
                
                insert rc;
            }
            
        }
    }
    
    public class CompanyInfo{
        public String name;
        public String slogan;
        public String phone;
        public String fax;
        public String email;
        public String street;
        public String city;
        public String state;
        public String postalCode;
        public String country;
    }
}