/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Order Management - Selector class for Order_Management__mdt records
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-06-19
* @systemLayer    Selector
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class SEL_OrderManagementMdt {
	
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Query to get Order_Management__mdt records based on product group. This method is called
    * by OrderUtility class.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    productGroup    String - Currently evaluated order's order products prod. group
    * @input param    orderStatus    String - Currently evaluated order's status
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static List<Order_Management__mdt> requestWithoutProductCode(String productGroup, String orderStatus){
        return [SELECT  Product_Group__c,
                		Product_Code__c,
                		Current_Status__c,
                		Next_Status__c,
						Send_to_Backend__c,
                		Manual_Activation__c,
                		Manual_Termination__c,
						Manual_Cancellation__c,
                		Manual_Amend__c
				FROM    Order_Management__mdt
				WHERE   Product_Group__c = :productGroup
				AND     Current_Status__c = :orderStatus
				AND     IsFeedback__c = FALSE
				AND	   	No_Manual_Step_Required__c = TRUE LIMIT   1];
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Query to get Order_Management__mdt records based on product group and product code. This method 
    * is called by OrderUtility class.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    productGroup    String - Currently evaluated order's order products prod. group
    * @input param    productCode    String - Currently evaluated order's order products prod. code
    * @input param    orderStatus    String - Currently evaluated order's status
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static List<Order_Management__mdt> requestWithProductCode(String productGroup, String productCode, String orderStatus){
        return [SELECT  Product_Group__c,
                		Product_Code__c,
                		Current_Status__c,
                		Next_Status__c,
						Send_to_Backend__c,
                		Manual_Activation__c,
                		Manual_Termination__c,
						Manual_Cancellation__c,
                		Manual_Amend__c
				FROM    Order_Management__mdt
				WHERE   Product_Group__c = :productGroup
				AND     Current_Status__c = :orderStatus
				AND 	Product_Code__c = :productCode
				AND     IsFeedback__c = FALSE
				AND	   	No_Manual_Step_Required__c = TRUE LIMIT 1];
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Query to get Order_Management__mdt records based on product group and product code. This method 
    * is called by SRV_OrderManager class.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    productGroup    String - Currently evaluated order's order products prod. group
    * @input param    productCode    String - Currently evaluated order's order products prod. code
    * @input param    orderStatus    String - Currently evaluated order's status
    * @return         void
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public static List<Order_Management__mdt> responseWithoutProductCode(String productGroup, String orderStatus){
        return [SELECT	Product_Group__c, 
						Current_Status__c,
						Next_Status__c,
						Send_to_Backend__c,
						Product_Code__c
				FROM	Order_Management__mdt
				WHERE	Product_Group__c = :productGroup
				AND		Current_Status__c = :orderStatus
				AND		IsFeedback__c = TRUE
				AND		No_Manual_Step_Required__c = TRUE
				LIMIT	1];
    }
}