@isTest
public class test_SEL{
    
    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        account.GoldenRecordId__c = 'accGoldenRecordId';
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        quote.Trial_CallId__c = 'TrialCallIdTest';
        update quote;
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Billing_Profile__c billingProfile = Test_DataFactory.generateBillingProfiles(account.Id, 'BillingNameTest', ConstantsUtil.BILLING_GROUP_MODE_STANDARD, 1)[0];
        insert billingProfile;
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.Billing_Profile__c = billingProfile.Id;
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        		        
        SBQQ.TriggerControl.enable();
        String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_SELUserTerritory2Association(){
        
        Test.startTest();
        
        Map<String, Id> profilesMap = Test_DataFactory.getProfilesMap();
        User standardUser = Test_DataFactory.generateUser();
        standardUser.ProfileId = profilesMap.get('Sales');
        standardUser.Username = 'dmctester@test.com';
        UserTriggerHandler.disableTrigger = true;
        insert standardUser;
        UserTriggerHandler.disableTrigger = false;
        
        Territory2 firstLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = NULL  AND Territory2Model.State = 'Active' LIMIT 1];
        Territory2 secondLevelTerritory = [SELECT Id FROM Territory2 WHERE ParentTerritory2Id = :firstLevelTerritory.Id AND Territory2Model.State = 'Active' LIMIT 1];
        
        UserTerritory2Association ut2a = new UserTerritory2Association();
       	ut2a.Territory2Id = secondLevelTerritory.Id;
        ut2a.UserId = standardUser.Id;
        ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        insert ut2a;
        
		Map<Id, UserTerritory2Association> uT2a_1 = SEL_UserTerritory2Association.getUserTerritory2AssocationsByUserId(new Set<Id>{standardUser.Id});
        System.assertEquals(false, uT2a_1.isEmpty());
        Map<Id, UserTerritory2Association> ut2a_2 = SEL_UserTerritory2Association.getUserTerritory2AssocationsByTerritoryIds(new Set<Id>{secondLevelTerritory.Id});
        System.assertEquals(false, ut2a_2.isEmpty());
        Test.stopTest();
    }
    
    @isTest
    public static void test_SELQuote(){
        
        Test.startTest();
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__c, SBQQ__Status__c, SBQQ__Type__c FROM SBQQ__Quote__c LIMIT 1];
        quote.Trial_CallId__c = 'TrialCallIdTest';
        update quote;
        
        Map<Id, SBQQ__Quote__c> quotes_1 = SEL_Quote.getQuoteById(new Set<Id>{quote.Id});
        System.assertEquals(false, quotes_1.isEmpty());
        
        Map<Id, SBQQ__Quote__c> quotes_2 = SEL_Quote.getQuoteWithoutPDFDocument(new Set<Id>{quote.Id});
        System.assertEquals(false, quotes_2.isEmpty());
        
        SBQQ__Quote__c quote_1 = SEL_Quote.getQuoteByCallId('TrialCallIdTest');
        System.assertEquals(quote_1.Id, quote.Id);
        
        SBQQ__Quote__c quote_2 = SEL_Quote.getQuoteByTrialCallId('TrialCallIdTest');
        System.assertEquals(quote_2.Id, quote.Id);
        
        SBQQ__Quote__c quote_3 = SEL_Quote.getQuoteById(quote.Id);
        System.assertEquals(quote_3.Id, quote.Id);
        
        Map<Id, SBQQ__Quote__c> quotes_3 = SEL_Quote.getQuotebyIdAndTypeAndStatus(new Set<Id>{quote.Id}, new List<String>{quote.SBQQ__Status__c}, new List<String>{ConstantsUtil.QUOTE_TYPE_RENEWAL});
        System.assertEquals(false, quotes_3.isEmpty());
        
        Map<Id, SBQQ__Quote__c> quotes_4 = SEL_Quote.getQuotesByOpportunityIdsAndStatuses(new Set<Id>{quote.SBQQ__Opportunity2__c}, new Set<String>{quote.SBQQ__Status__c});
        System.assertEquals(false, quotes_4.isEmpty());
        
        Test.stopTest();
    }
    
    @isTest
    public static void test_SELContract(){
        
        Account account = [SELECT Id FROM Account][0];
        Contract contract = [SELECT Id, ContractNumber FROM Contract][0];
        Billing_Profile__c billingProfile = [SELECT Id from Billing_Profile__c WHERE Name = 'BillingNameTest 0 Billing Profile'][0];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c][0];

        Map<Id, Contract> contracts_1 = SEL_Contract.getContractsByIds(new Set<Id>{contract.Id});
		System.assertEquals(false, contracts_1.isEmpty());
        
        Map<Id, Contract> contracts_2 = SEL_Contract.getContractsWithMasterSubscriptions(new Set<Id>{contract.Id});
		System.assertEquals(false, contracts_2.isEmpty());
        
        Contract contract_1 = SEL_Contract.getContract(contract.ContractNumber);
        System.assertEquals(contract_1.Id, contract.Id);
        
        Map<Id, Contract> contracts_3 = SEL_Contract.getContractsByQuoteIdsAndStatus(new Set<Id>{quote.Id}, new List<String>{ConstantsUtil.CONTRACT_STATUS_ACTIVE});
		System.assertEquals(false, contracts_3.isEmpty());
        
        Map<Id, Contract> contracts_4 = SEL_Contract.getContractByBillingProfile(new Set<Id>{billingProfile.Id});
        
        Map<Id, Contract> contracts_5 = SEL_Contract.getContractBycontractIds(new Set<Id>{contract.Id});
		System.assertEquals(false, contracts_5.isEmpty());
        
        Map<Id, Contract> contracts_6 = SEL_Contract.getAllContractBycontractIds(new Set<Id>{contract.Id});
		System.assertEquals(false, contracts_6.isEmpty());
        
        List<Contract> contracts_7 = SEL_Contract.getContractById(contract.Id);
        System.assertEquals(false, contracts_7.isEmpty());
        
        List<Contract> contracts_8 = SEL_Contract.getContractByIds(new Set<Id>{contract.Id});
        System.assertEquals(false, contracts_8.isEmpty());
        
        List<Contract> contracts_9 = SEL_Contract.getContractByStatus(ConstantsUtil.CONTRACT_STATUS_ACTIVE, ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE);
        System.assertEquals(false, contracts_9.isEmpty());
    }
    
    @IsTest
    static void test_SEL_Account(){
        Account a = [SELECT Id FROM Account WHERE GoldenRecordID__c != NULL OR Customer_Number__c != NULL LIMIT 1];
        SEL_Account.getAccount('accGoldenRecordId');
        SEL_Account.getAccountById(a.Id);
        SEL_Account.getAccountsById(new Set<Id>{a.Id});
    }
    
    @IsTest
    static void test_SEL_User(){
        SEL_User.getUsersByUsername(new Set<String>());
        SEL_User.getUsersById(new Set<Id>());
        SEL_User.getUsersByFederationId(new Set<String>());
    }
    
    @IsTest
    static void test_SEL_QuoteDocument(){
        SEL_QuoteDocument.getQDWithContentDocuments(new Set<Id>());
        SEL_QuoteDocument.getQuoteDocuments(new Set<Id>());
        SEL_QuoteDocument.getQuoteDocumentsByQuoteIds(new Set<Id>());
        SEL_QuoteDocument.getQuoteDocumentsByIdANDEnvelopeId(new Set<Id>(), new Set<String>(), new Set<String>());
    }
    
    @istest
    static void test_SelCallCenter(){
        callcenter cc = [select Id, Name, InternalName from CallCenter][0];
        
        set<string> setstring = new set<string>();
        string ccstring = cc.internalName;
        setstring.add(ccstring);
        
        test.startTest();
        SEL_Callcenter.getCallCentersByInternalName(setstring);
        test.stopTest();
        
    }   
    @istest
    static void test_Sel_CategoryAndLocation(){
        place__c pl = [select Id from place__c][0];
        set<string> setstring = new set<string>();
        set<integer> setinteger = new set<Integer>();
        setinteger.add(9);
        string newstring = pl.id;
        setstring.add(newstring);
        test.startTest();
        SEL_CategoryAndLocation.getLocationsByPrivatePlace(pl.id);
        SEL_CategoryAndLocation.getLocationsByPlace(pl.id);
        Sel_categoryAndLocation.getCategoriesByPrivatePlace(pl.id);
        Sel_categoryAndLocation.getCategoriesByPlace(pl.id);
        Sel_categoryAndLocation.getCategoriesAndLocationsByRefIds(setstring);
        Sel_categoryAndLocation.getCategoriesAndLocationsByRefIds(pl.id,setstring);
        Sel_categoryAndLocation.getCategoriesAndLocationsByhashCodes(setinteger);
        Sel_categoryAndLocation.getCategoriesAndLocationsByhashCodes(pl.id,setinteger);
        test.stopTest();
    }
    
    @istest
    static void test_Sel_Contract(){
        account acc = [select id from account][0];
        contract c = [select id,contractnumber from contract][0];
        billing_profile__c bp = [select id from billing_profile__c][0];
        set<id> setids = new set<id>();
        setids.add(c.id);
        sbqq__quote__c quote = [select id from sbqq__quote__c][0];
        list<string> status = new list<string>();
        setids.add(quote.id);
        setids.add(acc.id);
        status.add('new');
        setids.add(bp.id);
        test.startTest();
        Sel_contract.getContractsWithMasterSubscriptions(setids);
        Sel_contract.getContract(c.ContractNumber);
        Sel_contract.getContractsByQuoteIdsAndStatus(setids,status);
        Sel_contract.getContractByBillingProfile(setids);
        sel_contract.getContractBycontractIds(setids);
        sel_contract.getAllContractBycontractIds(setids);
        sel_contract.getContractById(c.id);
        sel_contract.getContractByIds(setids);
        sel_contract.getContractByStatus(null,null);
        Sel_billingProfile.getBillingProfileById(bp.id);
        Sel_billingprofile.getBillingProfilesById(setids);
        Sel_billingprofile.getBillingProfilesByAccountIdForMergingAccounts(setids);
    }
    
    @istest
    static void Sel_editorialPlan(){
        Editorial_Plan__c ep = new Editorial_Plan__c();
        insert ep;
        
        decimal numbers = ep.Local_Guide_Number__c;
        list<decimal> decimals = new list<decimal>();
        list<id> listids = new list<id>();
        string s = string.valueOfGmt(date.today());
        listids.add(ep.id);
        decimals.add(numbers);
        set<string> developernames = new set<string>();
        group g = [select id, developername from group][0];
        string devn = g.developername;
        developernames.add(devn);
        test.startTest();
        Sel_editorialPlan.getActiveEditorialPlanbyGuideNumber(decimals,s);
        Sel_editorialPlan.getEditionbyId(listids);
        Sel_group.getGroupsByDeveloperName(developernames);
        test.stopTest();
    }
    
    @istest
    static void test_sel_lead(){
        list<lead> Tlead = Test_DataFactory.generateLeads('FirstNameLead', 'LastNameLead', 10);
        insert Tlead;
        set<id> leadids = new set<id>();
        leadids.add(Tlead[0].id);
        test.startTest();
        sel_lead.getLeadById(Tlead[0].id);
        sel_lead.getLeadByIds(leadids);
        test.stoptest();
    }
    
    @istest
    static void test_sel_localization(){
        product2 p = [select id from product2][0];
        log__c log = new log__c();
        log.Correlation_ID__c = '123456';
        insert log;
        set<id> productids = new set<id>();
        productids.add(p.id);
        test.startTest();
        sel_localization.getTranslatedProductName(productids,'de');
        sel_log.getLogByCorrelationId('123456');
        sel_log.getLogById(log.Id);
        sel_localization.getProductNameTranslations(productids);
        sel_localization.getQuoteTemplateTranslations(productids, new List<String>{'field'}, 'de');
        test.stoptest();
    }
    
    @istest
    static void test_Sel_SbqqQuoteline(){
        set<id> quoteids = new set<id>();
        set<id> quotelineids = new set<id>();
        set<id> placeids = new set<id>();
        list<string> status = new list<string>();
        set<string> stringquoteids = new set<string>();
        sbqq__quote__c quote = [select id,SBQQ__Status__c from sbqq__quote__c][0];
        sbqq__quoteline__c quoteline = [select id from sbqq__quoteline__c][0];
        status.add(quote.SBQQ__Status__c);
        place__c place = [select id from place__c][0];
        placeids.add(place.id);
        quotelineids.add(quoteline.id);
        quoteids.add(quote.id);
        string quoteid = quote.id;
        stringquoteids.add(quoteid);
        set<string> listfield = new set<string>();
        listfield.add('id');
        test.startTest();
        Sel_sbqqQuoteline.retrieveQLinesByQuoteIds(quoteids);
        Sel_sbqqQuoteline.getQuoteLinesById(quotelineids); 
        sel_sbqqQuoteline.getQLinesByPlaceAndQuoteStatusNotIn(quotelineids,placeids,status);
        Sel_sbqqQuoteline.getQLinesWithoutPlaceAndProductOptionByQuoteId(quoteids);
        Sel_sbqqQuoteline.getQLinesByQuoteId(quoteids);
        Sel_sbqqQuoteline.getQLinesWithPlaceSubscriptionByQuoteId(quoteids);
        Sel_sbqqQuoteline.getQLinesByPlaceAndQuoteStatus(placeids,status);
        sel_sbqqQuoteline.getChildrenQLByParentIdAndNotIn(quotelineids,quotelineids);
        sel_sbqqQuoteline.getParentQLinesByQuoteId(quote.id);
        sel_sbqqQuoteline.getParentQLinesByQuoteIds(quoteids);
        sel_sbqqQuoteline.getActiveQuoteLineByAccountId(null);
        sel_sbqqQuoteline.getParentQLinesByQuoteIdAndIdNotIn(quoteids,quotelineids);
        sel_sbqqQuoteline.getParentQLinesByQuoteId(quoteids);
        sel_sbqqQuoteline.getDuplicateLocationbyStatusAndAccount(null,quoteids,status);
        sel_sbqqQuoteline.getDuplicateCategorybyStatusAndAccount(null,quoteids,status);
        sel_sbqqQuoteline.getQuoteLinebyProductCodeAndAccount(null,null,status);
        sel_sbqqQuoteline.getAdContextAllocatedQL(quoteids);
        sel_sbqqQuoteline.getQuoteLinesByQuoteId(quoteids,listfield);
        sel_sbqqQuoteline.getQuoteLinesById(stringquoteids,listfield);
        test.stopTest();
    }
    
    @istest
    static void test_Sel_Subscription(){
        list<id> listsubids = new list<id>();
        set<id> quotelineids = new set<id>();
        set<id> subids = new set<id>();
        set<id> placeids = new set<id>();
        set<id> contractids = new set<id>();
        set<id> accountids = new set<id>();
        list<account> listacc = [select id from account];
        for (account a : listacc){
            accountids.add(a.id);
        }
        list<string> status = new list<string>();
        contract con = [select id from contract][0];
        contractids.add(con.id);
        sbqq__quoteline__c quoteline = [select id from sbqq__quoteline__c][0];
        sbqq__subscription__c subscription = [select id,Status__c from SBQQ__Subscription__c][0];
        string sta = subscription.Status__c;
        status.add(sta);
        place__c place = [select id from place__c][0];
        placeids.add(place.id);
        subids.add(subscription.id);
        listsubids.add(subscription.id);
        quotelineids.add(quoteline.id);
        test.startTest();
        Sel_subscription.getSubs(con);
        sel_subscription.getSubscriptionsByQuoteLines(quotelineids,status);
        sel_subscription.getSubscriptionsByPlace(placeids,status);
        sel_subscription.getSubscriptionsByContractsInTerminationSetAs(contractids,true);
        sel_subscription.getMasterSubscriptionsByContractsId(contractids);
        sel_subscription.getPrimarySubscriptionsByContractId(contractids);
        sel_subscription.getMasterSubscriptionsByContracts(contractids);
        sel_subscription.getSubscriptionsByContractsAndNotInStatus(contractids,status);
        sel_subscription.getMaxTerminationEndDatebyContract(contractids);
        sel_subscription.getSubscriptionsByContractIds(contractids);
        sel_subscription.getActiveParentSubscriptionsByContractIds(contractids);
        sel_subscription.getRelatedPrimarySubscriptionsByContractId(con.id);
        sel_subscription.getSubscriptionsByContractId(con.id);
        sel_subscription.getSubscriptionsByIdOrRequiredBy(subids);
        sel_subscription.getSubscriptionsByIds(subids);
        Sel_subscription.getSubsById(subids);
        Sel_subscription.getSubsByRequiredById(listsubids); 
        Sel_subscription.getSubsByIdOrRequiredById(listsubids);
        Sel_subscription.getSubscriptionsByIdsorRequiredIds(subids);
        Sel_subscription.getSubsCountByContractIdAndNotStatus(con.id,'expired');
        SEL_Subscription.getActiveSubscriptionByProductCodeAndAccountId(accountids);
        SEL_Subscription.getSubscriptionByPlaceProductAndStatus(placeids, new Set<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE});
        SEL_Subscription.getDuplicateLocationbyStatusAndAccount(accountids, new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE}, contractids);
        SEL_Subscription.getSubscriptionbyProductCodeAndAccount(accountids, new List<String>{ConstantsUtil.MY_WEB_BASIC}, new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE}, contractids);
        SEL_Subscription.getDuplicateCategorybyStatusAndAccount(accountids, new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE}, contractids);
        SEL_Subscription.getSubscriptionForUpdateField(listsubids);
        SEL_Subscription.getSubscriptionByPlace(new Set<Id>(), new Set<String>(), new Set<String>());
        SEL_Subscription.getReplacedSubscriptionsByQuoteId(new Set<Id>());
        SEL_Subscription.getSubscriptionbyPlaceAccountStatus(null,new Set<Id>(), new Set<String>(), new Set<Id>());
        SEL_Subscription.getChildrenSubscriptionForReplacement(new Set<Id>());
        SEL_Subscription.getSubsInvolvedInSubstitutionProcessByQuoteIds(new Set<Id>());
        SEL_Subscription.terminationContract_getSubscriptionsToTerminate(new Set<Id>());
        test.stopTest();
    }
}