public class SelectOptionLightning implements Comparable{
	
    @AuraEnabled public String label {get; set;}
    @AuraEnabled public String value {get; set;}
    
    public SelectOptionLightning(String label, String value){
        this.label = label;
        this.value = value;
    }
    
    public SelectOptionLightning(){
    }
    
    public Integer compareTo(Object compareTo) {
        SelectOptionLightning compareToSO = (SelectOptionLightning)compareTo;

        Integer returnValue = 0;
        if (compareToSO.label.equalsIgnoreCase('--NONE SELECTED--')) {
            returnValue = 1;
        } 
        else{
            returnValue = -1;
        }
        
        return returnValue;       
    }
}