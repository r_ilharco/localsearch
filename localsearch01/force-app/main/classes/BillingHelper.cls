public class BillingHelper {
	
    public static final string abaSaveEndpoint = ConstantsUtil.BILLING_ESB_ABACUS_ENDPOINT + '/save';
    public static final string billingEndpoint = ConstantsUtil.BILLING_ESB_SWISSBILLING_ENDPOINT;

    private static Numbering_cs__c numberingSngl;
    private static Customer_Number_cs__c customerNumberSngl;

    private static Billing_Setting__mdt settingSngl;
    private static Map<string, BillingCaseSetting> caseSettingsSngl;    
    private static List<string> paymentStatusListSngl;
    private static List<string> overduePaymentStatusListSngl;
    private static List<string> customerRatingListSngl;
    
    public class BillingCaseSetting {
        public string label;
        public Billing_Case__mdt metadata;
        public string queueId;
        public string recordTypeId;
        
    }
    
    public static Numbering_cs__c numbering {
        get {
            if(numberingSngl == null) {
            	numberingSngl = Numbering_cs__c.getOrgDefaults();
            }
            return numberingSngl;
        }
    }

    public static Customer_Number_cs__c customerNumber {
        get {
            if(customerNumberSngl == null) {
            	customerNumberSngl = Customer_Number_cs__c.getOrgDefaults();
            }
            return customerNumberSngl;
        }
    }
    
    public static Billing_Setting__mdt setting {
        get {
            if(settingSngl == null) {
               settingSngl = [select Abacus_User__c, Abacus_Password__c, Abacus_Mandant__c, Min_Billable_Invoice__c, Min_Printable_Invoice__c from Billing_Setting__mdt where Label =: ConstantsUtil.BILLING_SETTINGS_METADATA_LABEL];
            }
            return settingSngl;
        }
    }

    public static Map<string, BillingCaseSetting> caseSettings {
        get {
            if(caseSettingsSngl == null) {
                List<string> caseTypeLabels = new List<string> {
                    ConstantsUtil.BILLING_CASE_DELIVERY_FAILURE_LABEL,
                    ConstantsUtil.BILLING_CASE_ADDRESS_CHANGE_LABEL,
                    ConstantsUtil.BILLING_CASE_SWISSBILLING_REJECT_LABEL,
                    ConstantsUtil.BILLING_CASE_ABACUS_REJECT_LABEL
                };
                caseSettingsSngl = new Map<string, BillingCaseSetting> ();
                List<Billing_Case__mdt> billingCaseMetadata = [select Label, Case_Origin__c, Case_Queue__c, Priority__c, Record_Type_Name__c, Subtype__c, Type__c
                                            from Billing_Case__mdt where Label in : caseTypeLabels];
                Set<string> recordTypeNames = new Set<string>();
                Set<string> queueNames = new Set<string>();
                
                for(Billing_Case__mdt bc : billingCaseMetadata) {
                    recordTypeNames.add(bc.Record_Type_Name__c);
                    queueNames.add(bc.Case_Queue__c);
                }
                List<RecordType> recordTypes = [Select Id, DeveloperName from RecordType where DeveloperName in :recordTypeNames];
                List<Group> queues = [Select Id, DeveloperName from Group where DeveloperName in :queueNames and Type='Queue'];
                for(Billing_Case__mdt bc : billingCaseMetadata) {
                    // quick search. few records, no need to index
                    string recordTypeId;
                    string queueId;
                    for(RecordType rt : recordTypes) {
                        if(rt.DeveloperName == bc.Record_Type_Name__c) {
                            recordTypeId = rt.Id;
                            break;
                        }
                    }
                    for(Group queue : queues) {
                        if(queue.DeveloperName == bc.Case_Queue__c) {
                            queueId = queue.Id;
                            break;
                        }
                    }
                    BillingCaseSetting bcs = new BillingCaseSetting();
					bcs.label = bc.Label;
                    bcs.metadata = bc;
                    bcs.recordTypeId = recordTypeId;
                    bcs.queueId = queueId;
                    caseSettingsSngl.put(bc.Label, bcs);
                }
            }
            return caseSettingsSngl;
        }
    }
    
    public static List<string> PaymentStatusList {
        get {
            if(paymentStatusListSngl == null) {
                paymentStatusListSngl = ConstantsUtil.BILLING_PAYMENT_STATUS.split(',\\s?');
            }
            return paymentStatusListSngl;
        }
    }

    public static List<string> OverduePaymentStatusList {
        get {
            if(overduePaymentStatusListSngl == null) {
                overduePaymentStatusListSngl = ConstantsUtil.BILLING_OVERDUE_PAYMENT_STATUS.split(',\\s?');
            }
            return overduePaymentStatusListSngl;
        }
    }
    
    public static List<string> CustomerRatingList {
        get {
            if(customerRatingListSngl == null) {
                customerRatingListSngl = ConstantsUtil.BILLING_CUSTOMER_RATING.split(',\\s?');
            }
            return customerRatingListSngl;
        }
    }
    
    // Gets Invoice's TaxMode picklist value from exemption. Tax mode may contain more than 2 values in future
    public static String getTaxModeValue(string taxExempt) {
        if(taxExempt == 'Yes') {
            return ConstantsUtil.BILLING_TAX_MODE_EXEMPT;
        } else {
        	return ConstantsUtil.BILLING_TAX_MODE_STANDARD;
        }
    }
    
    public static long generateDocumentNumber() {
        system.debug('numbering: ' + numbering);
        long nextValue = numbering.Next_Invoice_Number__c.longValue();
        numbering.Next_Invoice_Number__c = nextValue + 1;
        return nextValue;
    }

    public static void undoDocumentNumber() {
        long nextValue = numbering.Next_Invoice_Number__c.longValue();
        numbering.Next_Invoice_Number__c = nextValue - 1;
    }
    
    public static long generateCustomerNumber() {
        long nextValue = customerNumber.Next_Customer_Number__c.longValue();
        customerNumber.Next_Customer_Number__c = nextValue + 1;
        return nextValue;
    }
    
    // +/- 2 cent rounding
    public static decimal calcRounding(decimal value){
        integer mod = Math.mod(Integer.valueOf(Math.abs(value) * 100.0), 5);
        return Math.signum(value) * (5 * Math.floor(mod / 2.5) - mod) / 100.0;
    }
    
    public static string getGUID() {
        return EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(128));
    }
    
    public static boolean checkMinTotalAllowed(Invoice__c invoice) {
        if(invoice.Billing_Channel__c == ConstantsUtil.BILLING_CHANNEL_PRINT) {
            return setting.Min_Printable_Invoice__c == null
            	|| setting.Min_Printable_Invoice__c == 0
            	|| setting.Min_Printable_Invoice__c <= Math.abs(invoice.Total__c);
        } else {
            return setting.Min_Billable_Invoice__c == null
            	|| setting.Min_Billable_Invoice__c == 0
            	|| setting.Min_Billable_Invoice__c <= Math.abs(invoice.Total__c);
        }
    }
    
    public static string getFallbackChannel(string itemChannel, string profileChannels) {
        if(!string.isBlank(itemChannel)) {
            return itemChannel;
        }
        if(string.isBlank(profileChannels)) {
            return ConstantsUtil.BILLING_CHANNEL_PRINT;
        }
        List<string> channels = profileChannels.split(';');
        if(channels.size() == 1) {
            return channels[0];
        }
        if(channels.contains(ConstantsUtil.BILLING_CHANNEL_PRINT)) {
            return ConstantsUtil.BILLING_CHANNEL_PRINT;
        }
        if(channels.contains(ConstantsUtil.BILLING_CHANNEL_MAIL)) {
            return ConstantsUtil.BILLING_CHANNEL_MAIL;
        }        
        return channels[0];
    }
    
    public static string formatDocNumber(long docNumber) {
        // locale independant formatter, in style 123'456'789 (completely useless as it should be executed only in batch mode with org locale, and format() is good enough)
        if(docNumber == null) return '';
        if(docNumber == 0) return '0';
        
        //ldimartino, hotfix AS-130, START : previous implementation commented
        /*
        List<string> grps = new List<string>();
        while(docNumber != 0) {
            grps.add(Math.mod(docNumber, 1000).format().leftPad(3, '0'));
            docNumber = (long) Math.floor(docNumber / 1000);
        }
        List<string> inv = new List<string>();
        for(integer i = grps.size() - 1; i >=0; i--) {
            inv.add(grps[i]);
        }
        return string.join(inv, '\'');
        */
        //ldimartino, hotfix AS-130, END : previous implementation commented
        return String.valueOf(docNumber);
    }
}