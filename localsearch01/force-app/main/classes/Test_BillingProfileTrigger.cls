/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.      
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 2020-06-01
 * Sprint      : 
 * Work item   : 
 * Package     : 
 * Description : TestClass for Billing Profile Trigger (Handler e Helper)
 * Changelog   : 
 */


@isTest
public class Test_BillingProfileTrigger {
    
    @isTest 
    static void test_insert(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Account Status Management';
        insert byPassFlow;
        
        Test.startTest();
        Account account = Test_DataFactory.generateAccounts('Test Account', 1, false)[0];
        account.Status__c = ConstantsUtil.ACCOUNT_STATUS_NEW;
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        List<Account> accounts = new List<Account>();
		List<Contact> contacts = new List<Contact>();
		
		accounts.add(account);
		contacts.add(contact);
		
		List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accounts, contacts, 1);
		insert bp;
        
        bp[0].Billing_City__c = 'Luzern';
        bp[0].AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_NOTVALIDATED;
        update bp[0];
        
        delete bp;
        undelete bp;
        
        Test.stopTest();
    }
}