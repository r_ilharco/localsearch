/**
 * Author      : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date        : 24-05-2019
 * Sprint      : 2
 * Work item   : #90
 * Testclass   : Test_AccountTrigger
 * Package     : 
 * Description : 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  "Parent Account" field on Account (SPIII 1461)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-07-17
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Review AS IS Request assignment process performed by DMC (SPIII 2025)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-07-24
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Added logic to reset Submitter field after the Approval Request is approved.
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-07-28
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public with sharing class AccountTriggerHandler implements ITriggerHandler {
    
    //private Boolean m_isExecuting = false;
    //private Integer BatchSize = 0;
    
    public static Boolean IsFromBachJob ;
    public static Boolean isFromUploadAPI=false;
    
    /*public AccountTriggerHandler(Boolean isExecuting, Integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }*/

    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }

    public void BeforeInsert(List<account> newItems){
        ATL_AccountTriggerHelper.insertCustomerNumber(newItems);
        
        if(!System.isFuture())
            ATL_AccountTriggerHelper.checkNewAccountsLegalAddress(newItems);
        
        ATL_AccountTriggerHelper.tipInsertAccountTechicalZipCode(newItems);
        ATL_AccountTriggerHelper.insertAddressField((List<Account>)newItems);
        ATL_AccountTriggerHelper.manageTerritoryAssignmentRules(newItems, null);
        ATL_AccountTriggerHelper.setValuesIfCreatedBySales(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        ATL_AccountTriggerHelper.updateAddressField((List<Account>)newItems.values(), (Map<Id, Account>)oldItems);
        
        if(!System.isFuture())
            ATL_AccountTriggerHelper.checkAccountsLegalAddress((Map<Id, Account>)newItems, (Map<Id, Account>)oldItems);
        
        ATL_AccountTriggerHelper.manageTerritoryAssignmentRules(newItems.values(), oldItems);
        ATL_AccountTriggerHelper.runDuplicationRules((List<Account>)newItems.values(),(List<Account>)oldItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems){}
    
    public void AfterInsert(Map<Id, SObject> newItems){
        
        if(!System.isFuture())
            ATL_AccountTriggerHelper.checkNewAccountsLegalAddress((List<Account>) newItems.values());
        
        ATL_AccountTriggerHelper.checkAccountHierarcy((Map<Id, Account>)newItems, null);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        ATL_AccountTriggerHelper.sendBlockedAccountNotificationToPM(newItems,oldItems);
        ATL_AccountTriggerHelper.checkAccountHierarcy((Map<Id, Account>)newItems, (Map<Id, Account>)oldItems);
        ATL_AccountTriggerHelper.resetSubmitterField((Map<Id, Account>)newItems, (Map<Id, Account>)oldItems);
        ATL_AccountTriggerHelper.updateRelatedOpportunityQuoteOwner((Map<Id, Account>)newItems, (Map<Id, Account>)oldItems);
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems){
        ATL_AccountTriggerHelper.processMergedAccounts((Map<Id, Account>) oldItems);
    }
    
    public void AfterUndelete(Map<Id, SObject> oldItems){}

}