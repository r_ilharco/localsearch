@isTest
public class SLMigrationBatch_Test{
    @TestSetup
    static void initData(){
        list<account>  accList= TestSetupUtilClass.GenerateAccounts(7);
        insert accList;       
        accList= [select id, name,GoldenRecordID__c from account ];
        //Test.startTest();  
        TestSetupUtilClass.DatapopulationFromAccount(accList, true);
        //Test.stopTest();  
    }
   @isTest
   static void QuotecopyTest(){    
       SLMigrationQuoteCopyGen_BatchSchedule sl =new SLMigrationQuoteCopyGen_BatchSchedule(10, 100);
       SLMigrationCopyActivation_BatchSchedule qa  =  new SLMigrationCopyActivation_BatchSchedule (2, 100, 10);     
       qa.Contract_job_Prior_days =1; 
        //Test.startTest();          
       database.executeBatch(sl, 100);
       // Test.stopTest(); 
       list<quote_copy__c> insertedQuoteCopy = [select id from quote_copy__c];
       //System.assertEquals(false, insertedQuoteCopy.isEmpty() );
       database.executeBatch(qa, 100);
       map<id, opportunity>  wonOpties = new    map<id, opportunity> ( [select id from opportunity]);
       database.executeBatch( new  SLMigrationMassiveContractCreation_Batch(new list<id>(wonOpties.keyset())),100);
        string cronSett = '0 0 2 ? * SUN';
        system.schedule('copy', cronSett, sl);
        string cronDay = '0 0 5 * * ?';
        system.schedule('activation', cronDay, qa);
      
   }
}