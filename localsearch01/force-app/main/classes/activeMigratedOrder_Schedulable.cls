global class activeMigratedOrder_Schedulable implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'activeMigratedOrder%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('activeMigratedOrder0')){
                activeMigratedOrder b0 = new activeMigratedOrder(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('activeMigratedOrder0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder1')){ 
                activeMigratedOrder b1 = new activeMigratedOrder(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder2')){
                activeMigratedOrder b2 = new activeMigratedOrder(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder3')){
                activeMigratedOrder b3 = new activeMigratedOrder(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder4')){
                activeMigratedOrder b4 = new activeMigratedOrder(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder5')){
                activeMigratedOrder b5 = new activeMigratedOrder(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder6')){
                activeMigratedOrder b6 = new activeMigratedOrder(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder7')){
                activeMigratedOrder b7 = new activeMigratedOrder(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder8')){
                activeMigratedOrder b8 = new activeMigratedOrder(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('activeMigratedOrder9')){
                activeMigratedOrder b9 = new activeMigratedOrder(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('activeMigratedOrder9', strSchedule,b9);
            }
        }
    }
}