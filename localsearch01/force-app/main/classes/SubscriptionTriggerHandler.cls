/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2019-10-01
 * Sprint      : 10
 * Work item   : SF2-336
 * Testclass   : Test_SubscriptionTriggerV2
 * Package     : 
 * Description :
 * Changelog   : 
 */

public without sharing class SubscriptionTriggerHandler implements ITriggerHandler{
    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }

    public void BeforeInsert(List<sObject> newItems){
        //SubscriptionTriggerHelper.setFieldsOnSubscription((List<SBQQ__Subscription__c>)newItems);
      	//SubscriptionTriggerHelper.setfieldforMigration(newItems);
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        SubscriptionTriggerHelper.setRevenueAmountFieldOnAccount((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
        SubscriptionTriggerHelper.totalAmountToBillUpdate((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
        SubscriptionTriggerHelper.totalBillingCyclesUpdate((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems){}
    public void AfterInsert(Map<Id, SObject> newItems){
    	SubscriptionTriggerHelper.contractOpenAmountCalculation((Map<Id, SBQQ__Subscription__c>) newItems);
        SubscriptionTriggerHelper.totalAmountToBillCalculaton((Map<Id, SBQQ__Subscription__c>) newItems);
        SubscriptionTriggerHelper.setBillingFieldsOnSubs((Map<Id, SBQQ__Subscription__c>) newItems);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        Map<Id, SBQQ__Subscription__c> subscriptionToRecalculate = SubscriptionTriggerHelper.subscriptionToRecalculateCheck((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
        if(!subscriptionToRecalculate.isEmpty()){
            SubscriptionTriggerHelper.contractOpenAmountCalculation(subscriptionToRecalculate);
        }     
        SubscriptionTriggerHelper.genarateCreditNote((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
        //SubscriptionTriggerHelper.totalAmountToBillCalculatonForAmend((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
        //SubscriptionTriggerHelper.setFieldsOnSubscriptionUpdate((Map<Id, SBQQ__Subscription__c>)newItems);
        //SubscriptionTriggerHelper.deleteAllocation((Map<Id, SBQQ__Subscription__c>) newItems, (Map<Id, SBQQ__Subscription__c>) oldItems);
    }
    public void AfterDelete(Map<Id, SObject> oldItems){}
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}
