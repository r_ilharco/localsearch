@istest public class test_cancelordercontroller {

static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management';
        insert byPassTrigger;
}
@testSetup
    public static void test_setupData(){
    insertbypassflownames();
   test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
        system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
        insert psas;
        }

  Account acc = Test_DataFactory.generateAccounts('Account Name Test',1,false)[0];
        insert acc;
  list<account> accs = new list<account>();
  accs.add(acc);
  List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
   insert cont;
  List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accs, cont, 1);
   insert bp;
  Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
 opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
   opp.OwnerId = Userinfo.getuserid();
   insert opp;

SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote1.Allocation_not_empty__c = true;
        quote1.Ad_Allocation_Ids__c = '5de7a446b919f40001888947';
        quote1.SBQQ__ExpirationDate__c = date.today();
        quote1.Billing_Profile__c = bp[0].id;
        insert quote1;
 

 list<order> orders = test_dataFactory.createOrders(acc,cntrct,ConstantsUtil.ORDER_TYPE_TERMINATION,1);
 insert orders;
 
 list<order> orders2 = test_dataFactory.createOrders(acc,cntrct,ConstantsUtil.ORDER_TYPE_TERMINATION,1);
 insert orders2;

Place__c place = Test_DataFactory.generatePlaces(acc.Id, 'Place Name Test');
        place.PlaceID__c = 'externalPlaceId';
		insert place;


permissionSetAssignment psa = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
    user u = [select id from user where id=: psa.AssigneeId][0];

    List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',place.id,cntrct.id,1);
   system.runas(testuser){ 
   insert subs;
        }

Product2 product1 = generateProduct();
   product1.Tasks__c = 'Renewal Contract Reminder Task';
   insert product1;

pricebook2 pricebook = test_datafactory.createPricebook2('TestBook',1)[0];
        insert pricebook;
Id pricebookId = Test.getStandardPricebookId();
orders[0].Pricebook2Id = pricebookid;
update orders[0];
PricebookEntry standardPrice = new PricebookEntry(Pricebook2ID = pricebookid, Product2Id = product1.Id, UnitPrice = 10000, IsActive = true);
insert standardPrice;


     

 List<OrderItem> orderitems = test_datafactory.createOrderItems(orders[0], product1, standardprice, subs);
 insert orderitems;
}

@istest 
    static void test_checkorderstatus(){
   test.starttest();
   cancelordercontroller.checkorderstatus(ConstantsUtil.ORDER_STATUS_ACTIVATED);
   test.stoptest();
 }

@istest
    static void test_cancelorder(){
   order ord = [select id from order][0];
   string ordId = ord.id;
   test.startTest();
    cancelordercontroller.cancelOrder(ordId);
   test.stoptest();
}

public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }

}