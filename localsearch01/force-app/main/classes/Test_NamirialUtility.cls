@isTest
public class Test_NamirialUtility {
    
    public static String endpoint = 'www.endpointtest.it/';
    
    @testSetup static void setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Opportunity Management,Quote Management';
		insert byPassFlow;
        
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        
        Map<String, Id> usersByUsernameMap = new Map<String,Id>();
        Test_DataFactory.namirial_insertFutureUsers();
        Map<Id, User> users = new Map<Id,User>([SELECT Id, Username FROM User]);
        if(users != NULL && users.size() > 0){
            for(User u : users.values()) usersByUsernameMap.put(u.Username, u.Id);
        }
        
        Pricebook2 pbDMC = new Pricebook2();
        pbDMC.ExternalId__c = 'PB3';
        pbDMC.Name = 'DMC';
        pbDMC.IsActive = true;
        insert pbDMC;
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        
        List<Account> accounts = Test_DataFactory.createAccounts('AccountTest01',1);
 		insert accounts;
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0],1);
        contacts[0].FirstName = 'FirstName';
        insert contacts;
        Place__c place = Test_DataFactory.generatePlaces(accounts[0].Id, 'Place Name Test');
        insert place;
        
        List<Opportunity> opps = Test_DataFactory.createOpportunities('OppName01','Draft',accounts[0].Id,1);
        opps[0].Name = 'OppName01';
        opps[0].Pricebook2Id = pbDMC.Id;
        insert opps;
        List<SBQQ__Quote__c> quotes = Test_DataFactory.createQuotes('New', opps[0], 1);
        quotes[0].SBQQ__SalesRep__c = usersByUsernameMap.get('dmctester@test.com');
        quotes[0].Filtered_Primary_Contact__c = contacts[0].Id;
        quotes[0].SBQQ__PrimaryContact__c = contacts[0].Id;
        quotes[0].SBQQ__PriceBook__c = pbDMC.Id;
        insert quotes;
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quotes[0].Id, productCodeToProduct.get('SLT001').Id, Date.today(), contacts[0].Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<Document> documents = Test_DataFactory.createDocuments('Q-45445-20191126-0945.pdf',
                                                                    'provaprovaprova',
                                                                    'application/pdf',
                                                                    'CABJBBCGAd4f957419a4f4936898439621d9cf51e',
                                                                   	true,
                                                                   	UserInfo.getUserId(),
                                                                    1);
        insert documents;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'DMC Template 2020 v1.0 - IT';
        template.SBQQ__TermsConditions__c = 'Die Preise werden nach Aufschaltung bzw. Publikation des jeweiligen Produkts monatlich bzw. jährlich in Rechnung gestellt. Einmalige Gebühren werden nach Vertragsabschluss in Rechnung gestellt.Es gelten die jeweils aktuellen Produktbeschreibungen auf www.localsearch.ch';
        insert template;
        
        SBQQ__QuoteDocument__c quoteDocument =  Test_DataFactory.generateQuoteDocument(quotes[0].Id);
        quoteDocument.SBQQ__DocumentId__c = documents[0].Id;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        quoteDocument.SBQQ__QuoteTemplate__c = template.Id;
        quoteDocument.SBQQ__Quote__c = quotes[0].Id;
        insert quoteDocument;
        
        QuoteLineTriggerHandler.disableTrigger = false;
    }
 
  @isTest static void test_getQuoteDocument(){
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        SBQQ__QuoteDocument__c qdResult = NULL;
        Test.startTest();
        	qdResult = NamirialUtility.getQuoteDocument(quoteDocument.Id);
        Test.stopTest();
        
        System.assert(qdResult != NULL);
    }
    
    @isTest static void test_getAllowedSignatureMethods_DMC(){
        List<NamirialUtility.ListEntry> entries;
        User dmcUser = [SELECT Id FROM User WHERE Username = 'dmctester@test.com' LIMIT 1];
        Test.startTest();
        	System.runAs(dmcUser)
            {
                entries = NamirialUtility.getAllowedSignatureMethods();
            }
        Test.stopTest();
        
        System.assert(entries != NULL && entries.size() == 2);
    }
    
    @isTest static void test_getAllowedSignatureMethods_TeleSales(){
        List<NamirialUtility.ListEntry> entries;
        User teleSalesUser = [SELECT Id FROM User WHERE Username = 'teleSalestester@test.com' LIMIT 1];
        Test.startTest();
        	System.runAs(teleSalesUser)
            {
                entries = NamirialUtility.getAllowedSignatureMethods();
            }
        Test.stopTest();
        
        System.assert(entries != NULL && entries.size() == 1);
    }
    
    @isTest static void test_getAllowedSignatureMethods_DigitalAgency(){
        List<NamirialUtility.ListEntry> entries;
        User digitalAgencyUser = [SELECT Id FROM User WHERE Username = 'digitalAgencytester@test.com' LIMIT 1];
        Test.startTest();
        	System.runAs(digitalAgencyUser)
            {
                entries = NamirialUtility.getAllowedSignatureMethods();
            }
        Test.stopTest();
        
        System.assert(entries != NULL && entries.size() == 2);
    }
    
    @isTest static void test_getAllowedSignatureMethods_Admin(){
        List<NamirialUtility.ListEntry> entries;
        Test.startTest();
                entries = NamirialUtility.getAllowedSignatureMethods();
        Test.stopTest();
        
        System.assert(entries != NULL && entries.size() == 2);
    }
    
    @isTest static void test_getEnvelopeId(){
        String envelopeId = NULL;
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        quoteDocument.EnvelopeId__c = 'EnvelopeIDXYZW12345';
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        
        update quoteDocument;
		
        Test.startTest();
        	envelopeId = NamirialUtility.getEnvelopeId(quoteDocument.Id);
        Test.stopTest();
        
        System.assert(envelopeId != NULL && quoteDocument.EnvelopeId__c.equals(envelopeId));
    }
    
    @isTest static void test_namirialSignatureProcess_SignNow_SALESREP_MISSINGINFO_de(){
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_DE = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_FR = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_IT = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_OTHER = NULL;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Contact cont = [SELECT FirstName, LastName, Email FROM Contact WHERE Id = :quote.Filtered_Primary_Contact__c LIMIT 1];

		User u = [SELECT FirstName, LastName, Email FROM User WHERE Id = :quote.SBQQ__SalesRep__c LIMIT 1];
        u.FirstName = NULL;
        u.LanguageLocaleKey = 'de';
        UserTriggerHandler.disableTrigger = true;
        update u;
        UserTriggerHandler.disableTrigger = false;
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	namirialResponse_DE = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
        Test.stopTest();
        
        System.assert(namirialResponse_DE != NULL && namirialResponse_DE.processFailed == true);
        //System.assert(namirialResponse_FR != NULL && namirialResponse_FR.processFailed == true);
        //System.assert(namirialResponse_OTHER != NULL && namirialResponse_OTHER.processFailed == true);
    }
    
    @isTest static void test_namirialSignatureProcess_SignNow_SALESREP_MISSINGINFO_it()
    {
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_DE = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_FR = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_IT = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_OTHER = NULL;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Contact cont = [SELECT FirstName, LastName, Email FROM Contact WHERE Id = :quote.Filtered_Primary_Contact__c LIMIT 1];
        
		User u = [SELECT FirstName, LastName, Email FROM User WHERE Id = :quote.SBQQ__SalesRep__c LIMIT 1];
        u.FirstName = NULL;
        u.LanguageLocaleKey = 'it';
        UserTriggerHandler.disableTrigger = true;
        update u;
        UserTriggerHandler.disableTrigger = false;
        
        System.runAS(u){
            cont.Language__c = 'Italian';
       		update cont;
        }
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	namirialResponse_IT = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
        Test.stopTest();
        
        System.assert(namirialResponse_IT != NULL && namirialResponse_IT.processFailed == true);
        //System.assert(namirialResponse_FR != NULL && namirialResponse_FR.processFailed == true);
        //System.assert(namirialResponse_OTHER != NULL && namirialResponse_OTHER.processFailed == true);
    }
    
     @isTest static void test_namirialSignatureProcess_SignNow_SALESREP_MISSINGINFO_fr()
    {
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_DE = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_FR = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_IT = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_OTHER = NULL;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Contact cont = [SELECT FirstName, LastName, Email FROM Contact WHERE Id = :quote.Filtered_Primary_Contact__c LIMIT 1];

		User u = [SELECT FirstName, LastName, Email FROM User WHERE Id = :quote.SBQQ__SalesRep__c LIMIT 1];
        u.FirstName = NULL;
        u.LanguageLocaleKey = 'fr';
        UserTriggerHandler.disableTrigger = true;
        update u;
        UserTriggerHandler.disableTrigger = false;
        
        System.runAS(u){
            cont.Language__c = 'French';
        	update cont;
        }
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	namirialResponse_FR = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
        Test.stopTest();
        
        System.assert(namirialResponse_FR != NULL && namirialResponse_FR.processFailed == true);
    }
    
     @isTest static void test_namirialSignatureProcess_SignNow_SALESREP_MISSINGINFO_en()
    {
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_DE = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_FR = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_IT = NULL;
        NamirialUtility.NamirialSignatureProcessResponse namirialResponse_OTHER = NULL;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Contact cont = [SELECT FirstName, LastName, Email FROM Contact WHERE Id = :quote.Filtered_Primary_Contact__c LIMIT 1];

		User u = [SELECT FirstName, LastName, Email FROM User WHERE Id = :quote.SBQQ__SalesRep__c LIMIT 1];
        u.FirstName = NULL;
        u.LanguageLocaleKey = 'en_US';
        UserTriggerHandler.disableTrigger = true;
        update u;
        UserTriggerHandler.disableTrigger = false;
        
        System.runAS(u){
            cont.Language__c = '';
        	update cont;
        }
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	namirialResponse_OTHER = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
        Test.stopTest();
        
        System.assert(namirialResponse_OTHER != NULL && namirialResponse_OTHER.processFailed == true);
    }
    
    @isTest static void test_namirialSignatureProcess_InvalidSignatureMethod()
    {
        NamirialUtility.NamirialSignatureProcessResponse invalidMethodResponse = NULL;
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	invalidMethodResponse = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, 'WrongMethod');
        Test.stopTest();
        
        System.assert(invalidMethodResponse != NULL);
        System.assert(invalidMethodResponse.processFailed == true);
        System.assert(invalidMethodResponse.errorMessage.equalsIgnoreCase('Invalid Signature Method'));
        	
    }
    
    @isTest static void test_namirialSignatureProcess_SignNow()
    {
        NamirialUtility.NamirialSignatureProcessResponse signNowResponse = NULL;
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken', Endpoint__c = Test_NamirialUtility.endpoint);
        Test_NamirialProxyMock.HttpResponseMockProxy proxy =  Test_NamirialProxyMock.buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        Test.startTest();
        	signNowResponse = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
        Test.stopTest();
        	
    }
    
    @isTest static void test_namirialSignatureProcess_Exception()
    {
        NamirialUtility.NamirialSignatureProcessResponse signNowResponse = NULL;
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken', Endpoint__c = Test_NamirialUtility.endpoint);
        //Test_NamirialProxyMock.HttpResponseMockProxy proxy =  Test_NamirialProxyMock.buildMockProxy();
        //Test.setMock(HttpCalloutMock.class, proxy);
        
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        try{
            Test.startTest();
                signNowResponse = NamirialUtility.namirialSignatureProcess_v2(quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL);
            Test.stopTest();
        }catch(Exception e)
        {
            System.assert(e != NULL);
        }
        	
    }
    
    @isTest static void test_getAccess_InsufficientPermissions(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'myStandardUser@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        
        Test.startTest();
        	System.runAs(u)
            {
                NamirialUtility.getAccess(quoteDocument.Id);
			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_QuoteDocumentAlreadySigned(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		Document doc = [SELECT Id, Name, Body FROM Document WHERE Id = :quoteDocument.SBQQ__DocumentId__c];
        
        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = quoteDocument.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        insert cdl;
        
        //quoteDocument.ManualProcess__c = true;
       	quoteDocument.NamirialSignature_ProcessType__c = 'SignNow';
        quoteDocument.IsEditableSignatureDate__c = true;
        //quoteDocument.NamirialSignature_ProcessType__c = ConstantsUtil.MANUAL_NAMIRIALSIGNATUREMETHOD_LABEL;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        update quoteDocument;
        
        SBQQ__Quote__share qshare = new SBQQ__Quote__share();
        qshare.ParentId = quoteDocument.SBQQ__Quote__c;
        qshare.UserOrGroupId = u.Id;
        qshare.AccessLevel = 'Edit';
        qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
        
        insert qshare;
        
        Test.startTest();
        	System.runAs(u)
            {
                NamirialUtility.getAccess(quoteDocument.Id);
			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_Manual_SignatureMethodAlreadyActivated(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        
        quoteDocument.NamirialSignature_ProcessType__c = ConstantsUtil.MANUAL_NAMIRIALSIGNATUREMETHOD_LABEL;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        update quoteDocument;
        
        SBQQ__Quote__share qshare = new SBQQ__Quote__share();
        qshare.ParentId = quoteDocument.SBQQ__Quote__c;
        qshare.UserOrGroupId = u.Id;
        qshare.AccessLevel = 'Edit';
        qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
        
        insert qshare;
        
        Test.startTest();
        	System.runAs(u)
            {
                NamirialUtility.getAccess(quoteDocument.Id);
			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_SendEmail_SignatureMethodAlreadyActivated(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        
        quoteDocument.NamirialSignature_ProcessType__c = ConstantsUtil.SENDMAIL_NAMIRIALSIGNATUREMETHOD_LABEL;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        quoteDocument.EnvelopeId__c = 'EnvelopeId';
        update quoteDocument;
        
        SBQQ__Quote__share qshare = new SBQQ__Quote__share();
        qshare.ParentId = quoteDocument.SBQQ__Quote__c;
        qshare.UserOrGroupId = u.Id;
        qshare.AccessLevel = 'Edit';
        qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
        
        insert qshare;
        
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken', Endpoint__c = Test_NamirialUtility.endpoint);
        Test.startTest();
            Test_NamirialProxyMock.HttpResponseMockProxy proxy =  Test_NamirialProxyMock.buildMockProxy();
            Test.setMock(HttpCalloutMock.class, proxy);
        	System.runAs(u)
            {
                NamirialUtility.getAccess(quoteDocument.Id);
                NamirialUtility.manageCallback('EnvelopeId', ConstantsUtil.NAMIRIAL_ENVELOPESTATUSCALLBACK_WORKSTEPFINISHED, quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_ACTIONCODE, ConstantsUtil.NAMIRIAL_ENVELOPE_STATUS_COMPLETED, 'workStepRedirectUrl');
				NamirialUtility.manageCallback('EnvelopeId', ConstantsUtil.NAMIRIAL_ENVELOPESTATUSCALLBACK_WORKSTEPFINISHED, quoteDocument.Id, ConstantsUtil.SENDMAIL_NAMIRIALSIGNATUREMETHOD_ACTIONCODE, ConstantsUtil.NAMIRIAL_ENVELOPE_STATUS_INPROGRESS, 'workStepRedirectUrl');
				NamirialUtility.manageCallback('EnvelopeId', ConstantsUtil.NAMIRIAL_ENVELOPESTATUSCALLBACK_WORKSTEPFINISHED, quoteDocument.Id, ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_ACTIONCODE, ConstantsUtil.NAMIRIAL_ENVELOPE_STATUS_REJECTED, 'workStepRedirectUrl');

			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_SignNow_SignatureMethodAlreadyActivated(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        
        quoteDocument.NamirialSignature_ProcessType__c = ConstantsUtil.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        quoteDocument.EnvelopeId__c = 'EnvelopeId';
        update quoteDocument;
        
        SBQQ__Quote__share qshare = new SBQQ__Quote__share();
        qshare.ParentId = quoteDocument.SBQQ__Quote__c;
        qshare.UserOrGroupId = u.Id;
        qshare.AccessLevel = 'Edit';
        qshare.RowCause = Schema.SBQQ__Quote__share.RowCause.TerritorySharing__c;
        
        insert qshare;
        
        Test.startTest();
        	System.runAs(u)
            {
                NamirialUtility.getAccess(quoteDocument.Id);
			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_NoQuoteDocumentFound(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        
        Test.startTest();
        	System.runAs(u)
            {
                NamirialUtility.getAccess(null);
			}
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_NoQuoteTemplateFound(){
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        
        quoteDocument.SBQQ__QuoteTemplate__c = NULL;
        update quoteDocument;
        
        Test.startTest();
                NamirialUtility.getAccess(quoteDocument.Id);
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_NoAssociatedPricebook(){
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		SBQQ__QuoteTemplate__c quoteTemplate = Test_DataFactory.createQuoteTemplate('DMC Template 2020 v1.0 - IT');
        
        insert quoteTemplate;
        quoteDocument.SBQQ__QuoteTemplate__c = quoteTemplate.Id;
        
        update quoteDocument;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__PriceBook__c FROM SBQQ__Quote__c WHERE Id = :quoteDocument.SBQQ__Quote__c];
        quote.SBQQ__PriceBook__c = NULL;
        
        update quote;
        
        Test.startTest();
                NamirialUtility.getAccess(quoteDocument.Id);
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_NotConfiguredPricebook(){
        Id standardPricebook = Test.getStandardPricebookId();
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		SBQQ__QuoteTemplate__c quoteTemplate = Test_DataFactory.createQuoteTemplate('DMC Template 2020 v1.0 - IT');
        
        insert quoteTemplate;
        quoteDocument.SBQQ__QuoteTemplate__c = quoteTemplate.Id;
        
        update quoteDocument;
        
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__PriceBook__c FROM SBQQ__Quote__c WHERE Id = :quoteDocument.SBQQ__Quote__c];
        quote.SBQQ__PriceBook__c = standardPricebook;
        
        update quote;
        
        Test.startTest();
                NamirialUtility.getAccess(quoteDocument.Id);
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_Revoked(){
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        quoteDocument.SBQQ__SignatureStatus__c = 'Revoked';
        update quoteDocument;
        
        Test.startTest();
                NamirialUtility.getAccess(quoteDocument.Id);
        Test.stopTest();
    }
    
    @isTest static void test_getAccess_Expired(){
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        quoteDocument.SBQQ__SignatureStatus__c = 'Expired';
        quoteDocument.ExpirationDate__c = Date.today().addDays(-1);
        update quoteDocument;
        
        Test.startTest();
                NamirialUtility.getAccess(quoteDocument.Id);
        Test.stopTest();
    }

    @isTest static void test_DownloadQuoteDocument(){
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Opportunity2__r.Name, SBQQ__SalesRep__c, Filtered_Primary_Contact__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name,EnvelopeId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
        quoteDocument.EnvelopeId__c = 'envelopeId';
        update quoteDocument;
        
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken', Endpoint__c = Test_NamirialUtility.endpoint);
        
        Test.startTest();
        
        Test_NamirialProxyMock.HttpResponseMockProxy proxy =  Test_NamirialProxyMock.buildMockProxy();

        Test.setMock(HttpCalloutMock.class, proxy);
        
        System.runAs(u)
        {
            NamirialUtility.requestDocumentDownload(quoteDocument.Id, quote.Id,quoteDocument.EnvelopeId__c);
        }
        Test.stopTest();

    }
}