/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 
 * Sprint      : 
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : TestClass for Order Trigger (Handler and Helper)
 * Changelog   : 
 */

@isTest
public class Test_OrderTrigger {

    @testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            currentProduct.Tasks__c = ConstantsUtil.PRODUCT_SET_UP_REMINDER_TASK;
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insert(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        Account account = [SELECT Id, New_Customer__c FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE Name = 'Test Opportunity Name' LIMIT 1];
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        ContentVersion contentVersion = new ContentVersion(); 
        contentVersion.Title = 'Test_CV'; 
        contentVersion.PathOnClient= '/' + contentVersion.Title + '.pdf'; 
        Blob body = Blob.valueOf('Unit Test ContentVersion'); 
        contentVersion.VersionData = body; 
        contentVersion.origin = 'H';
        insert contentVersion;
        
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = quote.Id;
        contentDocumentLink.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id].ContentDocumentId;
        contentDocumentLink.ShareType = 'I';
        contentDocumentLink.Visibility = 'AllUsers'; 
        insert contentDocumentLink;
        
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        
        Test.startTest();
        insert order;
        Test.stopTest();
        
        List<ContentDocumentLink> orderAttachment = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :order.Id];
        List<Order> orderAfterTrigger = [SELECT Id, EffectiveDate, New_Customer__c FROM Order WHERE Id = :order.Id LIMIT 1];
        //System.assertEquals(1, orderAttachment.size());
        System.assertEquals(Date.today(), orderAfterTrigger[0].EffectiveDate);
        System.assertEquals(account.New_Customer__c, orderAfterTrigger[0].New_Customer__c);
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_updateUpgradeOrder(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Account account = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        SBQQ__QuoteLine__c fatherQL = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c, Total__c
                                       FROM		SBQQ__QuoteLine__c
                                       WHERE 	SBQQ__RequiredBy__c = NULL
                                       LIMIT 	1];
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        
        List<SBQQ__QuoteLine__c> childrenQLs = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c
                                       			FROM	SBQQ__QuoteLine__c
                                      		 	WHERE 	SBQQ__RequiredBy__c = :fatherQL.Id];
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.StartDate = Date.today();
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        Integer count = 0;
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = childrenSubs[count].Id;
            count++;
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        //Starting upgrade
        Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
        upgradeOpportunity.CloseDate = Date.today().addDays(1);
		upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
		upgradeOpportunity.Upgrade__c = true;     
		upgradeOpportunity.Downgrade__c = false;
		insert upgradeOpportunity;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String upgradeOpportunityId = upgradeOpportunity.Id;
        String queryUpgradeQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :upgradeOpportunityId LIMIT 1';
        SBQQ__Quote__c upgradeQuote = Database.query(queryUpgradeQuote);
        
        SBQQ__QuoteLine__c fatherQLUpgrade = Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('SLS001').Id, Date.today().addDays(10), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLS001').Id));
        insert fatherQLUpgrade;
        
        List<SBQQ__QuoteLine__c> childrenQLsUpgrade = new List<SBQQ__QuoteLine__c>();
		childrenQLsUpgrade.add(Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('ONAP001').Id, Date.today().addDays(10), null, null, fatherQLUpgrade.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLsUpgrade.add(Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('ORER001').Id, Date.today().addDays(10), null, null, fatherQLUpgrade.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLsUpgrade.add(Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('OREV001').Id, Date.today().addDays(10), null, null, fatherQLUpgrade.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLsUpgrade.add(Test_DataFactory.generateQuoteLine_final(upgradeQuote.Id, productCodeToProduct.get('PORER001').Id, Date.today().addDays(10), null, null, fatherQLUpgrade.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('PORER001').Id)));
        insert childrenQLsUpgrade;

        upgradeQuote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update upgradeQuote;
		
        OrderTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_UPGRADE);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        orderMap.put('SBQQ__Quote__c',upgradeQuote.Id);
        Order upgradeOrder = Test_DataFactory.generateOrder(orderMap);
        upgradeOrder.OpportunityId = upgradeOpportunity.Id;
        insert upgradeOrder;
        
        OrderItem upgradeOI = Test_DataFactory.generateFatherOrderItem(upgradeOrder.Id, fatherQLUpgrade);
        upgradeOI.Subscription_To_Terminate__c = fatherSub.Id;
        upgradeOI.Manual_Activation__c = true;
        insert upgradeOI;
        
        List<OrderItem> childrenUpgradeOIs = Test_DataFactory.generateChildrenOderItem(upgradeOrder.Id, childrenQLsUpgrade, upgradeOI.Id);
        insert childrenUpgradeOIs;
        
        OrderUtility.activateContracts(new List<Id>{upgradeOrder.Id});
        
        List<Order> terminationUpgradeOrder = [SELECT Id, Status, Parent_Order__c FROM Order WHERE Type = :ConstantsUtil.ORDER_TYPE_UPGRADE AND Custom_Type__c = :ConstantsUtil.ORDER_TYPE_TERMINATION];
        terminationUpgradeOrder[0].Status = ConstantsUtil.ORDER_STATUS_PUBLICATION;
        terminationUpgradeOrder[0].Parent_Order__c = upgradeOrder.Id;
        
        
        update terminationUpgradeOrder;
        
        upgradeOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        update upgradeOrder;
       	Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_implicitRenewalOrder(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Account account = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        SBQQ__QuoteLine__c fatherQL = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c, Total__c
                                       FROM		SBQQ__QuoteLine__c
                                       WHERE 	SBQQ__RequiredBy__c = NULL
                                       LIMIT 	1];
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        
        List<SBQQ__QuoteLine__c> childrenQLs = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c
                                       			FROM	SBQQ__QuoteLine__c
                                      		 	WHERE 	SBQQ__RequiredBy__c = :fatherQL.Id];
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.StartDate = Date.today();
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        Integer count = 0;
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = childrenSubs[count].Id;
            count++;
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        OrderTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        Order implicitRenewalOrder = Test_DataFactory.generateOrder(orderMap);
        implicitRenewalOrder.Contract__c = contract.Id;
        insert implicitRenewalOrder;
        
        OrderItem renewalOI = Test_DataFactory.generateFatherOrderItem(implicitRenewalOrder.Id, fatherQL);
        insert renewalOI;
        
        List<OrderItem> childrenRenewalOIs = Test_DataFactory.generateChildrenOderItem(implicitRenewalOrder.Id, childrenQLs, renewalOI.Id);
        insert childrenRenewalOIs;
       	Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_updateDeleteAllocation(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Account account = [SELECT Id FROM Account LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        SBQQ__QuoteLine__c fatherQL = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c, Total__c
                                       FROM		SBQQ__QuoteLine__c
                                       WHERE 	SBQQ__RequiredBy__c = NULL
                                       LIMIT 	1];
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        fatherOI.Ad_Context_Allocation__c = 'testAllocationId';
        insert fatherOI;
        
        List<SBQQ__QuoteLine__c> childrenQLs = [SELECT 	Id, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c
                                       			FROM	SBQQ__QuoteLine__c
                                      		 	WHERE 	SBQQ__RequiredBy__c = :fatherQL.Id];
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        order.Status = ConstantsUtil.ORDER_STATUS_CANCELLED;
        update order;
        
        delete order;
        undelete order;
        Test.stopTest();
    }
}