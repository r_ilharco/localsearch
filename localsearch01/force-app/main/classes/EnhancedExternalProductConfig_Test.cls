@isTest
public class EnhancedExternalProductConfig_Test {
    static ByPassFlow__c insertBypassFlowNames(){
            ByPassFlow__c byPassFlow = new ByPassFlow__c();
            byPassFlow.Name = Userinfo.getProfileId();
            byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management,Update Contract Company Sign Info';
            insert byPassFlow;
            return byPassFlow;
    }
    @testSetup static void setup(){
                
        ByPassFlow__c bPF = insertBypassFlowNames();
        
        Id pricebookId = Test.getStandardPricebookId();
        
        List <Product2> lstProd = new List<Product2>();
        
        Product2 p1 = new Product2(AutomaticRenew__c= false,
                                   Family= 'Entry Product',
                                   IsActive= true,
                                   KTRCode__c= '1101020005',
                                   KTRDesignation__c= 'SLT - Swiss List Starter',
                                   L1_Business__c= '510 Verzeichnisse',
                                   L2_ProductRange__c= '510.2 Eintragsgeschäft',
                                   L3_ProductLine__c= '510.24 Swiss List',
                                   MultiplePlaceIDAllowed__c= false,
                                   Name= 'Swiss List Starter',
                                   PlaceIDRequired__c= 'Yes',
                                   ProductCode= 'SRCHBS001',
                                   SBQQ__Component__c= false,
                                   SBQQ__ConfigurationEvent__c= 'Always',
                                   SBQQ__ConfigurationType__c= 'Allowed',
                                   SBQQ__DefaultQuantity__c= 1,
                                   SBQQ__NonDiscountable__c= true,
                                   SBQQ__Optional__c= false,
                                   SBQQ__OptionSelectionMethod__c= 'Click',
                                   SBQQ__PriceEditable__c= false,
                                   SBQQ__PricingMethodEditable__c= false,
                                   SBQQ__PricingMethod__c= 'List',
                                   SBQQ__QuantityEditable__c= true,
                                   SBQQ__SubscriptionBase__c= 'List',
                                   SBQQ__SubscriptionPricing__c= 'Fixed Price',
                                   Subscription_Term__c = '24',
                                   ExternalKey_ProductCode__c = 'SRCHBT001-AR',
                                   Base_term_Renewal_term__c = '12',
                                   SBQQ__BillingFrequency__c='Annual' 
                                   ,   SBQQ__TaxCode__c='Standard'
                                  );
        
        lstProd.add(p1);

        Product2 p2 = new Product2(AutomaticRenew__c= false,
                                   Family= 'Entry Product',
                                   IsActive= true,
                                   MultiplePlaceIDAllowed__c= false,
                                   Name= 'NAP+ Öffnungszeiten',
                                   PlaceIDRequired__c= 'No',
                                   ProductCode= 'TLINK001',
                                   SBQQ__Component__c= true,
                                   SBQQ__ConfigurationType__c= 'Disabled',
                                   SBQQ__DefaultQuantity__c= 1,
                                   SBQQ__NonDiscountable__c= true,
                                   SBQQ__Optional__c= false,
                                   SBQQ__OptionSelectionMethod__c= 'Click',
                                   SBQQ__PriceEditable__c= false,
                                   SBQQ__PricingMethodEditable__c= false,
                                   SBQQ__PricingMethod__c= 'List',
                                   SBQQ__QuantityEditable__c= false,
                                   SBQQ__SubscriptionBase__c= 'List',
                                   SBQQ__SubscriptionPricing__c= 'Fixed Price',
                           		   Subscription_Term__c = '24',
                                   Base_term_Renewal_term__c = '3',
                                   SBQQ__BillingFrequency__c='Annual',
                                   SBQQ__TaxCode__c='Standard');
        
        lstProd.add(p2);
        
        Product2 p3 = new Product2(AutomaticRenew__c= false,
                                   Family= 'Entry Product',
                                   IsActive= true,
                                   MultiplePlaceIDAllowed__c= false,
                                   Name= 'Url',
                                   PlaceIDRequired__c= 'No',
                                   ProductCode= 'ONAP001',
                                   SBQQ__Component__c= true,
                                   SBQQ__ConfigurationType__c= 'Disabled',
                                   SBQQ__DefaultQuantity__c= 1,
                                   SBQQ__NonDiscountable__c= true,
                                   SBQQ__Optional__c= false,
                                   SBQQ__OptionSelectionMethod__c= 'Click',
                                   SBQQ__PriceEditable__c= false,
                                   SBQQ__PricingMethodEditable__c= false,
                                   SBQQ__PricingMethod__c= 'List',
                                   SBQQ__QuantityEditable__c= false,
                                   ExternalKey_ProductCode__c = 'SRCHBS001-FT',
                                   SBQQ__SubscriptionBase__c= 'List',
                                   SBQQ__SubscriptionPricing__c= 'Fixed Price',
                           		   Subscription_Term__c = '24',
                                   Base_term_Renewal_term__c = '3',
                                   SBQQ__BillingFrequency__c='Annual',
                                   SBQQ__TaxCode__c='Standard');
        
        lstProd.add(p3);
        insert lstProd;

        List<PricebookEntry> lstPriceEntry = new List<PricebookEntry>();
         
        
        PricebookEntry pe1 = new PricebookEntry(IsActive= true,
                                                Pricebook2Id= pricebookId,
                                                Product2Id= p1.Id,
                                                UnitPrice= 59.9,
                                                UseStandardPrice= false);      
        lstPriceEntry.add(pe1);

        PricebookEntry pe2 = new PricebookEntry(IsActive= true,
                                                Pricebook2Id= pricebookId,
                                                Product2Id= p2.Id,
                                                UnitPrice= 0,
                                                UseStandardPrice= false);
        lstPriceEntry.add(pe2);
        
        PricebookEntry pe3 = new PricebookEntry(IsActive= true,
                                                Pricebook2Id= pricebookId,
                                                Product2Id= p3.Id,
                                                UnitPrice= 0,
                                                UseStandardPrice= false);
        lstPriceEntry.add(pe3);
        insert lstPriceEntry;
        
        SBQQ__ProductOption__c po1 = new SBQQ__ProductOption__c(SBQQ__Bundled__c= true,
                                                                SBQQ__ConfiguredSKU__c= p2.id,
                                                                SBQQ__MaxQuantity__c= 1,
                                                                SBQQ__MinQuantity__c= 1,
                                                                SBQQ__Number__c= 3,
                                                                SBQQ__OptionalSKU__c= p2.id,
                                                                SBQQ__QuantityEditable__c= false,
                                                                SBQQ__Quantity__c= 1,
                                                                SBQQ__Required__c= false,
                                                                SBQQ__Selected__c= true,
                                                                SBQQ__Type__c= 'Component');
        insert po1;
        
        List<Account> accs = Test_DataFactory.createAccounts('test',1);
        insert accs;   

        List<Opportunity> opps = Test_DataFactory.createOpportunities('TestOpportunity', ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION, accs[0].Id, 1);
        insert opps;  

        Editorial_Plan__c plan = new Editorial_Plan__c(Local_Guide_Number__c = 500, Plan_Closing_Date__c = Date.today().addDays(1));
        insert plan;
        
        List<Place__c> places = Test_DataFactory.createPlacesWithAccountAndLead('PlaceName',accs[0].id, null, 1);   
        insert places;
        
        Id recordTypeId = [select id from RecordType where name = 'External Location'].id;
        Category_Location__c c = new Category_Location__c(
            Name = 'test',
            Location_en__c = 'test',
            Location_fr__c = 'test',
            Location_de__c = 'test',
            Location_it__c = 'test',
            Publication__c = '500',
            RecordTypeId= recordTypeId);
        insert c;
        
        SBQQ__Quote__c quoteQuote = Test_DataFactory.createQuotes('Draft',opps[0], 1)[0];
        quoteQuote.SBQQ__Account__c = accs[0].id;
        quoteQuote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_QUOTE;
        quoteQuote.SBQQ__Primary__c=true;
        insert quoteQuote;
      
        List<SBQQ__QuoteLine__c> quoteLines =Test_DataFactory.createQuoteLines(quoteQuote,lstProd);
        for(SBQQ__QuoteLine__c ql: quoteLines){
            ql.Category__c = 'Test';
            ql.Location__c = 'Test';
        }
        insert quoteLines;
        
        Date tday = Date.today().addDays(10);
        List<Contract> contracts = Test_DataFactory.getContracts(accs[0], ConstantsUtil.CONTRACT_STATUS_DRAFT , 1);
		contracts[0].SBQQ__Opportunity__c = opps[0].Id;
        contracts[0].SBQQ__Quote__c = quoteQuote.Id;
        contracts[0].StartDate = tday;
		contracts[0].CallId__c = 'ewibqleububfuvqbwecuçwbçeuvcqweucçehçhwvcuçehwf';
        insert contracts;
        
        List<SBQQ__Subscription__c>  subs = Test_DataFactory.getSubscriptions(accs[0], places[0], contracts[0],lstProd ,1);
        insert subs;
        
        delete bPF;
     }    

    @isTest
    public static void shouldvalidationCategoryLocation(){
        EnhancedExternalProductConfigurationCtrl ctrl = new EnhancedExternalProductConfigurationCtrl();
        List<SBQQ__QuoteLine__c> qLines = [select id,
                                           Category__c, 
                                           Location__c, 
                                           SBQQ__Quote__r.SBQQ__Account__c, 
                                           SBQQ__Quote__c 
                                           from SBQQ__QuoteLine__c 
                                           where SBQQ__Quote__r.SBQQ__Account__c != ''];    
        if(!qLines.isEmpty()){
            EnhancedExternalProductConfigurationCtrl.validationCategory('Accessories;Accademy',qLines[0].SBQQ__Quote__r.SBQQ__Account__c, qLines[0].id,qLines[0].SBQQ__Quote__c);
            EnhancedExternalProductConfigurationCtrl.validationLocation('Accessories;Accademy', qLines[0].SBQQ__Quote__r.SBQQ__Account__c,qLines[0].id, qLines[0].SBQQ__Quote__c);
        }
    }
    
   @isTest
    public static void shouldgetEdition(){
        EnhancedExternalProductConfigurationCtrl ctrl = new EnhancedExternalProductConfigurationCtrl();
        EnhancedExternalProductConfigurationCtrl.localGuideNumb = '300';
    	ctrl.getEditionAndLocation();
        EnhancedExternalProductConfigurationCtrl.localGuideNumb = '500';
    	ctrl.getEditionAndLocation();
        Account acc = [select id from Account where name like 'test%' limit 1];
        ctrl.result = new EnhancedExternalProductConfigurationCtrl.Result();
        ctrl.result.mapValues = new Map<String,List<SelectOption>>();
        ctrl.accountId = acc.id;
        ctrl.getPlace();
    }
    
    
   @isTest
    public static void shouldsendVariable(){
    	Test.setMock(HttpCalloutMock.class, new EnhancedExternalProductConfig_Mock('category')); 
        EnhancedExternalProductConfigurationCtrl ctrl = new EnhancedExternalProductConfigurationCtrl();
        SBQQ__Quote__c q = [select id, SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Account__c != ''];
        SBQQ__QuoteLine__c ql = [select id, SBQQ__Product__c from SBQQ__QuoteLine__c limit 1];
        SBQQ__ProductOption__c p1 = [select id from SBQQ__ProductOption__c limit 1];
		String parameter= '{"quote":{"attributes":{"type":"SBQQ__Quote__c","url":"/services/data/v46.0/sobjects/SBQQ__Quote__c/a0q1x000006MYP8AAO"},"SBQQ__StartDate__c":"2019-08-21","SBQQ__NetAmount__c":22,"SBQQ__CustomerDiscount__c":25,"SBQQ__CustomerAmount__c":22,"SBQQ__PaymentTerms__c":"Net 30","SBQQ__Opportunity2__r":{"attributes":{"type":"Opportunity","url":"/services/data/v46.0/sobjects/Opportunity/0061x000008TEZ1AAO"},"SBQQ__PrimaryQuote__c":"a0q1x000006MYP8AAO","AccountId":"'+q.SBQQ__Account__c+'","Pricebook2Id":"01s1x000000A1iNAAS","Id":"0061x000008TEZ1AAO"},"SBQQ__Status__c":"In Review","SBQQ__LineItemsGrouped__c":false,"SBQQ__ExpirationDate__c":"2019-09-20","SBQQ__Account__r":{"attributes":{"type":"Account","url":"/services/data/v46.0/sobjects/Account/'+q.SBQQ__Account__c+'"},"SBQQ__RenewalPricingMethod__c":"Same","Id":"'+q.SBQQ__Account__c+'","SBQQ__RenewalModel__c":"Contract Based","Name":"MARIANIIIIIIII"},"SBQQ__ContractingMethod__c":"By Subscription End Date","SBQQ__Primary__c":true,"Name":"Q-28041","SBQQ__Type__c":"Quote","SBQQ__PricebookId__c":"01s1x000000A1iNAAS","SBQQ__Opportunity2__c":"0061x000008TEZ1AAO","SBQQ__LineItemCount__c":2,"Id":"a0q1x000006MYP8AAO","SBQQ__Unopened__c":false,"SBQQ__Account__c":"'+q.SBQQ__Account__c+'","SBQQ__WatermarkShown__c":false},"product":{"configuredProductId":"'+ql.SBQQ__Product__c+'","lineItemId":"'+ql.id+'","lineKey":1,"configurationAttributes":{"attributes":{"type":"SBQQ__ProductOption__c"}},"optionConfigurations":{"test 1":[{"optionId":"'+p1.id+'","selected":true,"ProductCode":"TLINK001","ProductName":"Tel Link","Quantity":1,"configurationData":{},"index":0,"readOnly":{}}]}}}'; 
        Apexpages.currentPage().getParameters().put('parameter',parameter);
        ctrl.sendVariable();
        ctrl.getEditionAndLocation();
    }
        
    
   @isTest
    public static void shouldredirectToGotU(){
        EnhancedExternalProductConfigurationCtrl ctrl = new EnhancedExternalProductConfigurationCtrl();
        Account acc = [select id from Account where name like 'test%' limit 1];
        ctrl.accountId = acc.id;
        ctrl.redirectToGotU();
    }  
}