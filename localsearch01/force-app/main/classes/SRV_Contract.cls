/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-24-2019
 * Sprint      : 4
 * Work item   : SF2-190 - Immediate downgrade through approval process 
 * Testclass   :
 * Package     : 
 * Description : Service Class for Contract
 * Changelog   : 
 * 				#1 - SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 07-02-2019 Fixing
 */

public class SRV_Contract {
    public static Map<Id, SBQQ__Subscription__c> getEvergreenSubTermByContractId(Set<Id> ids)
    {
        Map<Id, SBQQ__Subscription__c> result = new Map<Id, SBQQ__Subscription__c>();
        
        for(SBQQ__Subscription__c sub: SEL_Subscription.getMasterSubscriptionsByContracts(ids).values()){
            if(!result.containsKey(sub.SBQQ__Contract__c)){
                result.put(sub.SBQQ__Contract__c, null);
            }
            
            if(!String.isEmpty(sub.Subscription_Term__c)){
            	result.put(sub.SBQQ__Contract__c,sub);
            }
        }
        
        return result;
    }
    
    //START - SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 07-02-2019 Fixing
    public static List<PriceBookEntry> getSellableProductsByContract(Contract cont, String opType){
		
        List<String> includedGroupsInSearch = new List<String>();
        Set<String> relatedConfigModel = new Set<String>();
        Map<String, Integer> existingProductsToPriority = new Map<String, Integer>();
        List<PriceBookEntry> allowedPbes = new List<PriceBookEntry>();
        
        //last parameters true to only return subscription with PBE active
        List<SBQQ__Subscription__c> relatedSubs = SEL_Subscription.getRelatedPrimarySubscriptionsByContractIdAndStatus(cont.Id, new List<String>{ConstantsUtIl.SUBSCRIPTION_STATUS_ACTIVE}, true);
        
        if(!relatedSubs.isEmpty()){
            
            List<String> relatedGroups = getProductGroupOfContract(relatedSubs);
            
            System.debug('Related Groups: ' + JSON.serializePretty(relatedGroups));
            
            if(!relatedGroups.isEmpty()){
                
                includedGroupsInSearch.addAll(relatedGroups);
                
                List<Upgrade_Downgrade_Contract__mdt> upgradeSettings = [SELECT	MasterLabel, CrossGroup__c
                                                                         FROM 	Upgrade_Downgrade_Contract__mdt 
                                                                         WHERE 	MasterLabel IN :relatedGroups];
                
                Map<String,String> allowedCrossGroupsMap = new Map<String,String>();
                
                if(!upgradeSettings.isEmpty()){
                    for(Upgrade_Downgrade_Contract__mdt upgradeSetting : upgradeSettings){
                        if(String.isNotBlank(upgradeSetting.CrossGroup__c)){
                            List<String> allowedCrossGroups = upgradeSetting.CrossGroup__c.split(';');
                            includedGroupsInSearch.addAll(allowedCrossGroups);
                            for(String allowedCrossGroup : allowedCrossGroups) allowedCrossGroupsMap.put(allowedCrossGroup, upgradeSetting.MasterLabel);   
						}
                    }
                }
        
        		Set<Id> pricebookstoSearchFor = SRV_PricebookAssignment.getPricebookForUser(UserInfo.getUserId());
                
                Boolean disused = false;
                
                for(SBQQ__Subscription__c currentSubscription : relatedSubs){
                    
                    Boolean isEvergreen = ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN.equalsIgnoreCase(currentSubscription.SBQQ__Product__r.Configuration_Model__c);
                    if(String.isNotBlank(currentSubscription.SBQQ__Product__r.Priority__c)){

                        Integer priority = Integer.valueOf(currentSubscription.SBQQ__Product__r.Priority__c);
                        String productGroup = currentSubscription.SBQQ__Product__r.Product_Group__c;
                        Boolean isDisused = ConstantsUtil.PRODUCT2_FAMILY_DISUSED.equalsIgnoreCase(currentSubscription.SBQQ__Product__r.Family);
                        Boolean isMigrated = currentSubscription.SBQQ__OrderProduct__r.SambaMigration__c;
                        

                        if(isMigrated &&  isDisused) disused = true;
                                                    
                        if(!existingProductsToPriority.containsKey(productGroup)) existingProductsToPriority.put(productGroup, priority);
                        else if(existingProductsToPriority.get(productGroup) > priority) existingProductsToPriority.put(productGroup, priority);
                    }               
                    if(isEvergreen) relatedConfigModel.add(currentSubscription.SBQQ__Product__r.Configuration_Model__c);
                }
                
                String selectClause = 'SELECT ';
        		selectClause += 'Id, UnitPrice, PriceBook2Id, Product2Id, Product2.Id, Product2.Priority__c,Product2.Upgrade_Disable__c,Product2.Configuration_Model__c,Product2.Downgrade_Disable__c, Product2.Product_Group__c';
        
        		String whereClause = 'IsActive = TRUE ';
                if(!pricebookstoSearchFor.isEmpty()) whereClause += 'AND PriceBook2Id IN :pricebookstoSearchFor ';
                if(!includedGroupsInSearch.isEmpty()) whereClause += 'AND Product2.Product_Group__c IN :includedGroupsInSearch AND Product2.Family != \'' + ConstantsUtil.PRODUCT2_FAMILY_DISUSED + '\' ';
                if(!relatedConfigModel.isEmpty() && !includedGroupsInSearch.contains(ConstantsUtil.PRODUCT2_PRODUCTGROUP_SWISSLIST)) whereClause += ' AND Product2.Configuration_Model__c IN :relatedConfigModel';
            	
                String query = selectClause + ' FROM PriceBookEntry WHERE ' + whereClause;
                
                List<PriceBookEntry> pricebookEntries = new List<PriceBookEntry>();
        		
                System.debug('Pricebooks available for current user: ' + pricebookstoSearchFor);
                System.debug('Product Groups included in search: ' + includedGroupsInSearch);
                System.debug('Built query: ' + query);
                pricebookEntries = Database.query(query);
                
                for(PriceBookEntry currentPBE : pricebookEntries){
                    if(String.isNotBlank(currentPBE.Product2.Priority__c)){
                        
                        Boolean upgradeDisable = currentPBE.Product2.Upgrade_Disable__c;
                        String productGroup = currentPBE.Product2.Product_Group__c;
                        Integer priority = Integer.valueOf(currentPBE.Product2.Priority__c);
                        Integer existingPriority;
                        
                        if(relatedGroups.contains(productGroup)) existingPriority = existingProductsToPriority.get(productGroup);          
                        else existingPriority = existingProductsToPriority.get(allowedCrossGroupsMap.get(productGroup));
                        
                        if(ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(opType)){
                            if(priority >= existingPriority && !upgradeDisable && disused) allowedPbes.add(currentPBE);
                            else if(!relatedConfigModel.isEmpty() && !disused && priority >= existingPriority && !upgradeDisable) allowedPbes.add(currentPBE); 
							else if(relatedConfigModel.isEmpty() && !disused && priority >= existingPriority && !upgradeDisable) allowedPbes.add(currentPBE); 
                        }
                        /*else if(ConstantsUtil.QUOTE_TYPE_DOWNGRADE.equalsIgnoreCase(opType) ){  
                            if(priority <  existingPriority && currentPBE.Product2.Downgrade_Disable__c == false){   
                                allowedPbes.add(currentPBE); 
                            }
                        }*/
                    }
                }
            }
        }
        System.debug('Allowed entries for current user: ' + allowedPbes);
		return allowedPbes;
    }
    
    private static List<String> getProductGroupOfContract(List<SBQQ__Subscription__c> relatedSub){
        
        List<String> relatedGroups = new List<String>();

        if(!relatedSub.isEmpty()){
            
            for(SBQQ__Subscription__c aSub : relatedSub){   
                if(aSub.SBQQ__Product__r.Product_Group__c != null){
               		relatedGroups.add(aSub.SBQQ__Product__r.Product_Group__c); 
                }
            }
            
        }
        
        return relatedGroups;  
    }
    
    //END - SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 07-02-2019 Fixing
	
	/*public static Map<String, List<SBQQ__Subscription__c>> generateSubscriptionListGrouped(List<SBQQ__Subscription__c> subscriptions){

        Map<String, List<SBQQ__Subscription__c>> groupingToSubList = new Map<String, List<SBQQ__Subscription__c>>();

        try{
            List<Termination_Cancellation_Order_Grouping__mdt> orderGroupingMetadata = getOrderGroupingMetadata();
            
            for(Termination_Cancellation_Order_Grouping__mdt currentMdt : orderGroupingMetadata){

                List<String> productCodes = currentMdt.Product_Codes__c.split(';');

                for(SBQQ__Subscription__c currentSub : subscriptions){
                    if(currentSub.SBQQ__RequiredById__c == null){
                        if(productCodes.contains(currentSub.SBQQ__Product__r.ProductCode)){
                            if(!groupingToSubList.containsKey(currentMdt.Grouping_Mode__c)){
                                groupingToSubList.put(currentMdt.Grouping_Mode__c, new List<SBQQ__Subscription__c>{currentSub});
                            }
                            else{
                                groupingToSubList.get(currentMdt.Grouping_Mode__c).add(currentSub);
                            }
                        }
                    }
                    else{
                        //currentSub.RequiredBy__c = currentSub.SBQQ__RequiredById__c;
                        if(productCodes.contains(currentSub.RequiredBy__r.SBQQ__Product__r.ProductCode)){
                            if(!groupingToSubList.containsKey(currentMdt.Grouping_Mode__c)){
                                groupingToSubList.put(currentMdt.Grouping_Mode__c, new List<SBQQ__Subscription__c>{currentSub});
                            }
                            else{
                                groupingToSubList.get(currentMdt.Grouping_Mode__c).add(currentSub);
                            }
                        }
                    }
                }   
            }
        }
        catch(Exception e){
            System.debug('SRV_Contract.generateOrders Exception--> '+e.getMessage());
        }
		System.debug('VLAUDATO Grouping Map --> '+JSON.serializePretty(groupingToSubList));
        return groupingToSubList;
    }

    public static List<Termination_Cancellation_Order_Grouping__mdt> getOrderGroupingMetadata(){
        return [SELECT Product_Codes__c, Grouping_Mode__c FROM Termination_Cancellation_Order_Grouping__mdt];
    }*/
	
    public static Order generateOrder(SRV_OrderCreation.OrderWrapper orderWrapper, Contract c){
        Order order = new Order();
        order.AccountId = orderWrapper.accountId;
        order.SBQQ__Quote__c = orderWrapper.quoteId;
        order.Pricebook2Id = orderWrapper.pricebookId;
        order.Status = orderWrapper.Status;
        order.Type = orderWrapper.orderType;
        order.Custom_Type__c = orderWrapper.customType;
        if(orderWrapper.noClawback != null) order.No_Clawback__c = orderWrapper.noClawback; 
        order.Contract__c = orderWrapper.contractId;
        
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderWrapper.orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null) c.Cancel_Date__c = Date.today();
        }
        else{
            order.EffectiveDate = c.TerminateDate__c;
            if(c.TerminateDate__c == null) c.TerminateDate__c = Date.today();
            order.ContractId = c.id;
        }
        return order;
    }

    public static List<OrderItem> generateOrderItems(List<SBQQ__Subscription__c> subscriptions, Id orderId, Map<Id, OrderItem> subIdToOrderItem){

        List<OrderItem> orderItems = new List<orderItem>();
                
        for(SBQQ__Subscription__c currentSub : subscriptions){
            OrderItem oi = new OrderItem();
            oi.Partial_Termination__c = currentSub.Partial_Termination__c;
            oi.ServiceDate = currentSub.Termination_Date__c;
            oi.TLPaket_Region__c = currentSub.TLPaket_Region__c;
            oi.CategoryID__c = currentSub.CategoryID__c;
            oi.LocationID__c = currentSub.LocationID__c;
            oi.Edition__c = currentSub.SBQQ__QuoteLine__r.Edition__c;
            oi.Quantity = currentSub.SBQQ__Quantity__c;
            oi.Product2Id = currentSub.SBQQ__Product__c;
            if(currentSub.SBQQ__ListPrice__c == NULL) oi.UnitPrice = currentSub.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            else oi.UnitPrice = currentSub.SBQQ__ListPrice__c;
            oi.Total__c = currentSub.Total__c;
            oi.PricebookEntryId = currentSub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c;
            oi.SBQQ__Contract__c = currentSub.SBQQ__Contract__c;
            oi.SBQQ__QuoteLine__c =currentSub.SBQQ__QuoteLine__c; 
            oi.SBQQ__Subscription__c = currentSub.Id;
            oi.OrderId = orderId;
            if(String.isNotBlank(currentSub.Campaign_Id__c)) oi.Campaign_Id__c = currentSub.Campaign_Id__c;
            if(currentSub.SBQQ__Product__r.Manual_Activation__c == true) oi.Manual_Activation__c = true;
            if(currentSub.SBQQ__Product__r.Production__c == true) oi.Production__c = true;
            
            if(String.isBlank(currentSub.RequiredBy__c)) subIdToOrderItem.put(currentSub.Id, oi);
            else oi.SBQQ__RequiredBy__c = subIdToOrderItem.get(currentSub.RequiredBy__c).Id;
            orderItems.add(oi);
        }

        return orderItems; 

    }
    
    public static void terminateContractsForInvoices(Set<Id> invoiceIds){
        
        Map<Id, Contract> contracts = new Map<Id, Contract>([SELECT 
                                                             	Id
                                                             	,AccountId
                                                             	,SBQQ__Quote__c
                                                             	,SBQQ__Quote__r.SBQQ__PriceBook__c
                                                             	,Cancel_Date__c
                                                             	,TerminateDate__c
                                                             FROM 
                                                             	Contract 
                                                             WHERE 
                                                             	Id IN (SELECT 	Contract__c 
                                                                       FROM		Invoice_Order__c 
                                                                       WHERE	Invoice__c IN :invoiceIds)
                                                            ]);
        
        Map<Id, SBQQ__Subscription__c> subscriptions = new Map<Id, SBQQ__Subscription__c>([SELECT 
                                                                                           		Id
                                                                                           		,SBQQ__Contract__c
                                                                                           		,SBQQ__RequiredById__c
                                                                                           		,TLPaket_Region__c
                                                                                           		,CategoryID__c
                                                                                           		,LocationID__c
                                                                                           		,SBQQ__Quantity__c
                                                                                           		,SBQQ__Product__c
                                                                                           		,SBQQ__ListPrice__c
                                                                                           		,SBQQ__QuoteLine__r.SBQQ__ListPrice__c
                                                                                           		,Total__c
                                                                                           		,SBQQ__Product__r.Production__c
                                                                                           		,SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c
                                                                                           		,SBQQ__QuoteLine__c, Campaign_Id__c
                                                                                           		,SBQQ__Product__r.Manual_Activation__c
                                                                                           		,Termination_Date__c
                                                                                           		,Partial_Termination__c
                                                                                           		,SBQQ__QuoteLine__r.Edition__c
                                                                                           		,SBQQ__QuoteLine__r.OrderGroup__c
                                                                                           		,RequiredBy__c
                                                                                           FROM 
                                                                                           		SBQQ__Subscription__c
                                                                                           WHERE
                                                                                           		SBQQ__Contract__c IN :contracts.keySet()
                                                                                           ORDER BY 
                                                                                           		SBQQ__RequiredById__c
                                                                                          ]);
        
        Map<Id, SBQQ__Subscription__c> masterSubscriptionsMap = new Map<Id, SBQQ__Subscription__c>();
        Map<Id, SBQQ__Subscription__c> childrenSubscriptionsMap = new Map<Id, SBQQ__Subscription__c>();
        
        for(SBQQ__Subscription__c subscription : subscriptions.values()){
            if(String.isNotBlank(subscription.RequiredBy__c)) childrenSubscriptionsMap.put(subscription.Id, subscription);
            else masterSubscriptionsMap.put(subscription.Id, subscription);
        }
        
		Map<Id, Map<String, Map<Id, List<SBQQ__Subscription__c>>>> subscriptionsHierarchyGrouped = SRV_OrderCreation.groupSubscriptions(masterSubscriptionsMap, childrenSubscriptionsMap);
		
        if(!subscriptionsHierarchyGrouped.isEmpty()){

            Map<Integer, SRV_OrderCreation.CompositeOrderValue> indexToComposite = new Map<Integer, SRV_OrderCreation.CompositeOrderValue>();
            Map<Integer, Order> indexToOrder = new Map<Integer, Order>();
            Map<Id, OrderItem> subIdToOrderItem = new Map<Id, OrderItem>();
            List<OrderItem> fatherOrderItems = new List<OrderItem>();
            List<OrderItem> childrenOrderItems = new List<OrderItem>();
            
            Integer orderIndex = 0;
            for(Id currentContractId : subscriptionsHierarchyGrouped.keySet()){
                for(String currentGroup : subscriptionsHierarchyGrouped.get(currentContractId).keySet()){
                    
                    SRV_OrderCreation.CompositeOrderValue compositeOrderValue = new SRV_OrderCreation.CompositeOrderValue();
                    compositeOrderValue.contractId = currentContractId;
                    compositeOrderValue.orderGroup = currentGroup;
                    
                    Order terminationCancellationOrder = SRV_Contract.generateOrder(ContractUtility.generateOrderWrapperInstance(contracts.get(currentContractId), ConstantsUtil.ORDER_TYPE_TERMINATION, false, false), contracts.get(currentContractId));
                    indexToComposite.put(orderIndex, compositeOrderValue);
                    indexToOrder.put(orderIndex, terminationCancellationOrder);
                    orderIndex++;
                }
            }
            
            insert indexToOrder.values();
            
            for(Integer index : indexToOrder.keySet()){
                List<SBQQ__Subscription__c> masterSubscriptions = new List<SBQQ__Subscription__c>();
                for(Id masterSubscriptionId : subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).keySet()) masterSubscriptions.add(masterSubscriptionsMap.get(masterSubscriptionId));
                fatherOrderItems.addAll(SRV_Contract.generateOrderItems(masterSubscriptions, indexToOrder.get(index).Id, subIdToOrderItem));
            }
            
            if(!fatherOrderItems.isEmpty()){
                insert fatherOrderItems;
                
                for(Integer index : indexToOrder.keySet()){    
                    List<SBQQ__Subscription__c> masterSubscriptions = new List<SBQQ__Subscription__c>();
                    for(Id masterSubscriptionId : subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).keySet()){
                        List<SBQQ__Subscription__c> childrenSubscriptions = subscriptionsHierarchyGrouped.get(indexToComposite.get(index).contractId).get(indexToComposite.get(index).orderGroup).get(masterSubscriptionId);
                        if(!childrenSubscriptions.isEmpty()) childrenOrderItems.addAll(SRV_OrderCreation.generateOrderItems(childrenSubscriptions, indexToOrder.get(index).Id, subIdToOrderItem));
                    }
                }
                
                if(!childrenOrderItems.isEmpty()) insert childrenOrderItems;
            }
        }
    }
    
    public static void creditNotesUpdate(Map<Id,Id> subscriptionToContractMap){
        Set<Id> subscriptionIds = subscriptionToContractMap.keySet();
        String statusBilled = ConstantsUtil.CREDIT_NOTE_STATUS_BILLED;
        List<Credit_Note__c> creditNotesToUpdate = new List<Credit_Note__c>();
        Map<Id,Credit_Note__c> creditNotes = new Map<Id,Credit_Note__c>([SELECT Id, UpgradeContract__c, Subscription__c FROM Credit_Note__c WHERE Subscription__c IN :subscriptionIds AND Status__c != :statusBilled]);
        for(Credit_Note__c cn : creditNotes.values()){
            cn.UpgradeContract__c = subscriptionToContractMap.get(cn.Subscription__c);
            creditNotesToUpdate.add(cn);
        }
        if(creditNotesToUpdate.size() > 0){
            update creditNotesToUpdate;
        }
    }
}
