public without sharing class EditLinesCustomController {
    
    @AuraEnabled
    public static String getQuoteStatus(String recordId){
        
        List<SBQQ__Quote__c> foundQuotes = new List<SBQQ__Quote__c>();
        String quoteStatus;
        
        foundQuotes = [SELECT Id, SBQQ__Status__c, SBQQ__ExpirationDate__c FROM SBQQ__Quote__c WHERE Id=: recordId];
        
        if(Date.today() > foundQuotes[0].SBQQ__ExpirationDate__c ){
            quoteStatus = ConstantsUtil.Quote_Status_Expired;
        } else {
            quoteStatus = foundQuotes[0].SBQQ__Status__c;
        }
        return quoteStatus;
        
    }
    
}