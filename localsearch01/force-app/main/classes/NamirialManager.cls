/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author	   : Gennaro Casola <gcasola@deloitte.it>
* Date		   : 10-11-2019
* Sprint      : Sprint 11
* Work item   : SF2-425 - Namirial - Digital Signature integration Enhancement
* Testclass   :
* Package     : 
* Description : 
* Changelog   :
*/

@RestResource(urlMapping='/namirial-digitalsignature/feedbacks')
global without sharing class NamirialManager {
    @HttpGet
    global static String getCallback()
    {
        String errorMessages = '';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String envelopeId = req.params.get('Envelope');
        String action = req.params.get('Action');
        String quoteDocumentId = req.params.get('QuoteDocumentId');
        String actionCode = req.params.get('ActionCode');
        
        System.debug('>envelopeId: ' + envelopeId);
        System.debug('>action: ' + action);
        System.debug('>quoteDocumentId: ' + quoteDocumentId);
        System.debug('>actionCode: ' + actionCode);
        
        Namirial_Callback_Event__e callbackPlatformEvent = new Namirial_Callback_Event__e(envelopeId__c = envelopeId, 
                                                                                          action__c = action, 
                                                                                          quoteDocumentId__c = quoteDocumentId, 
                                                                                          actionCode__c = actionCode);
        
        if(ConstantsUtil.NAMIRIAL_ENVELOPESTATUSCALLBACK_WORKSTEPFINISHED.equalsIgnoreCase(action) 
           || ConstantsUtil.NAMIRIAL_ENVELOPESTATUSCALLBACK_SENDSIGNNOTIFICATION.equalsIgnoreCase(action)){
            Database.SaveResult dbSaveResult = EventBus.publish(callbackPlatformEvent);
            
            if (dbSaveResult.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : dbSaveResult.getErrors()) {
                    errorMessages += 'Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }
        }
        
        LogUtility.saveLogNamirialCallback(req,res,quoteDocumentId,errorMessages);
        
        return '';
    }
    
}