@isTest
public class Test_ContractsOverviewController {
    
    @testSetup
    public static void test_setupData(){
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        

        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation, Subscriptions Handler, Send Owner Notification, Set Contract Start/End date for Quote';
        insert byPassFlow;
        //SBQQ.TriggerControl.disable(); 
        List<Account> accs = Test_DataFactory.generateAccounts('test', 1, false);
        insert accs;
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        for(Contact c : contacts){
            c.AccountId = accounts[0].Id;
        }
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts,contacts,1);
        insert profiles;
        List<Opportunity> opps = Test_DataFactory.createOpportunities('TestNameOpp','Accepted',accounts[0].Id,1);
        insert opps;
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c q = [SELECT Id, Billing_Profile__c FROM SBQQ__Quote__c LIMIT 1];
        q.Billing_Profile__c = profiles[0].Id;
        QuoteTriggerHandler.disableTrigger = true;
        update q;
        QuoteTriggerHandler.disableTrigger = false;
        system.debug('quote: ' + q);
        Test_Billing.generateProduct(false);
        Product2 prod = [SELECT Id FROM Product2];
        Test_Billing.generateLocalization(prod.Id);
        QuoteLineTriggerHandler.disableTrigger = true;
        Test_Billing.generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        Test_Billing.generateQuoteLine(q.Id, prod.Id, Date.today(), null, 'annual');
        QuoteLineTriggerHandler.disableTrigger = false;
        List<SBQQ__QuoteLine__c> qls = [SELECT Id, SBQQ__ListPrice__c, SBQQ__Product__c, SBQQ__Quantity__c, SBQQ__Number__c FROM SBQQ__QuoteLine__c];
        generateContract(q.Id, accounts[0].id, opp.Id);
        Contract c = [SELECT Id, Status, AccountId FROM Contract LIMIT 1];
        c.Status = 'Active';
        update c;
        generateSubs(c, qls);
        generateOrder(q.Id,accounts[0],c,qls[0]);
        SBQQ.TriggerControl.enable();
     
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false; 
    }
    
    @isTest
    public static void test_getSubscriptionByAccount() {
        Test.startTest();
        Account a = [SELECT Id FROM Account LIMIT 1];
        System.debug('111111Number of Queries used in this apex code so far: ' + Limits.getQueries());
        ContractsOverviewForAccountController.getSubscriptionByAccount(a.id, false, '', null, null, new List<String>{ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE}, 'Renewable', 'LAST_N_MONTHS:6');
        ContractsOverviewForAccountController.getSubscriptionByAccount(a.id, true, '', null, null, null, null, null);
        ContractsOverviewForAccountController.retrievePicklistValues('SBQQ__Subscription__c', 'SBQQ__SubscriptionType__c');
        Test.stopTest();
    }
    
    @isTest
    public static void test_getOrderItemsByAccount() {
        Test.startTest();
        Account a = [SELECT Id FROM Account LIMIT 1];
        ContractsOverviewForAccountController.getOrderItemsByAccount(a.id, false, '', null, null,'LAST_N_MONTHS:6',new List<String>{ConstantsUtil.ORDER_STATUS_PRODUCTION});
        ContractsOverviewForAccountController.getOrderItemsByAccount(a.id, True, '', null, null,'LAST_N_MONTHS:6',new List<String>{ConstantsUtil.ORDER_STATUS_PRODUCTION});
        Test.stopTest();
    }
    
    public static void generateOrder(Id quoteId, Account acc, Contract c,SBQQ__QuoteLine__c ql){ 
        SBQQ.TriggerControl.disable();
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        
        
        List<Product2> prdList = Test_DataFactory.createProducts('testPrd',1);
        prdList[0].Base_term_Renewal_term__c = '12';
        insert prdList;
        
        List<Order> ordList = Test_DataFactory.createOrders(acc, c, ConstantsUtil.ORDER_TYPE_NEW, 1);
        ordList[0].Status = ConstantsUtil.ORDER_STATUS_PRODUCTION;
        insert ordList;
        List<SBQQ__Subscription__c> subscrList = [Select Id From SBQQ__Subscription__c Where SBQQ__Contract__c = :c.Id];    
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = pricebookId;
        standardPrice.Product2Id = prdList[0].Id;
        standardPrice.UnitPrice = 1;
        standardPrice.IsActive = true;
        standardPrice.UseStandardPrice = false;
        insert standardPrice ;
        ordlist[0].Pricebook2Id = pricebookId;
        update ordlist;
        List<OrderItem> ordItmList = Test_DataFactory.createOrderItems(ordList[0], prdList[0], standardPrice, subscrList);
        OrdItmList[0].SBQQ__Contract__c = c.Id;
        insert ordItmList;
        
        SBQQ.TriggerControl.enable();
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        
    }
    
    public static void generateContract(Id quoteId, Id AccountId, Id oppId){
        Contract c = new Contract();
        c.StartDate = Date.today();
        c.ContractTerm = 12;
        c.Status = 'Draft';
        c.SBQQ__Opportunity__c = oppId;
        c.SBQQ__Quote__c = quoteId;
        c.AccountId = AccountId;
        insert c;
    }
    
    public static void generateSubs(Contract contr, List<SBQQ__QuoteLine__c> quoteLines){
        System.debug('generateSubs - START Number of Queries used in this apex code so far: ' + Limits.getQueries());
        List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            subscriptions.add(new SBQQ__Subscription__c(SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, SBQQ__BillingType__c='Advance',
                                                        SBQQ__ChargeType__c = 'Recurring', SBQQ__Discount__c=0.0, SBQQ__ListPrice__c=quoteLine.SBQQ__ListPrice__c, 
                                                        SBQQ__Number__c = quoteLine.SBQQ__Number__c, SBQQ__OptionLevel__c = 1, SBQQ__Bundle__c = FALSE, 
                                                        SBQQ__Product__c = quoteLine.SBQQ__Product__c, SBQQ__Quantity__c = quoteLine.SBQQ__Quantity__c, SBQQ__QuoteLine__c = quoteLine.Id, 
                                                        Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE, SBQQ__SubscriptionStartDate__c = Date.today(), SBQQ__SubscriptionEndDate__c=null,
                                                        SBQQ__Contract__c = contr.Id, SBQQ__Account__c = contr.AccountId, SBQQ__Bundled__c = FALSE, Status__c = 'Active', Total__c = 100.0));
        }
        System.debug('generateSubs - END Number of Queries used in this apex code so far: ' + Limits.getQueries());
        SubscriptionTriggerHandler.disableTrigger = true;
        insert subscriptions;
        SubscriptionTriggerHandler.disableTrigger = false;
    }

}