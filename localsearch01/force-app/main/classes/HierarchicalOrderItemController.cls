//Sprint 7 - SF2 274 - Product Option Name translation 

public without sharing class HierarchicalOrderItemController {
    
	@AuraEnabled
    public static OrderItemResponse getItemById(Id id){
		OrderItemResponse res = new OrderItemResponse();
       	//Map<Id, OrderItem> ordItemList = SEL_OrderItem.getMapRecordsByOrderItemId(id);      
        //Map<OrderItem, List<OrderItem>> orderItemHierarchyMap = SEL_OrderItem.getHierarchyMapRecordsByOrderItemMap(ordItemList);
        
        //List <OrderItem> ordItemList = SEL_OrderItem.getOrderItemByOrderId(id);
        
        Map<String,Boolean> featuresToogle = SEL_OrderItem.ReadFeatureToggle();
        List <OrderItem> ordItemList = new List <OrderItem>();
        
        if(featuresToogle.containsKey('Hide_Non_Price_Relevant_Items') && featuresToogle.get('Hide_Non_Price_Relevant_Items')){
            List <OrderItem> ordItemList_All = SEL_OrderItem.getOrderItemByOrderId(id);
            for(OrderItem oi : ordItemList_All){
            	if(!oi.NonPriceRelevant__c) ordItemList.add(oi);
        	}
        }else{
        	 ordItemList = SEL_OrderItem.getOrderItemByOrderId(id);
        }
        
        
        
        List<OrderItem> OrderItemres = new List<OrderItem>();
        
        
        system.debug('ordItemList DEBUG ' + ordItemList);
        
        Map<Id, Map<String, String>> translatedValues = new Map<Id, Map<String, String>>();                
        Set<Id> productIds = new Set<Id>();
        String language = UserInfo.getLanguage();
        System.debug('Lanuage ' + language);
		 
 
        for (OrderItem q: ordItemList) {
            system.debug('q.Product2Id ' + q.Product2Id);
            productIds.add( q.Product2Id);
            
            
        }	
        
		List<SBQQ__Localization__c> localizations = [SELECT ID, SBQQ__Product__c, SBQQ__Language__c, SBQQ__Text__c FROM SBQQ__Localization__c WHERE SBQQ__Product__c IN :productIds AND SBQQ__Label__c = :ConstantsUtil.TRANSLATION_LABEL_PRODUCT_NAME];      
       	System.debug('Locatizations ' + localizations);
        Map<Id, Map<String, String>> productLanguageTranslation = new Map<ID, Map<String, String>>();
        for(SBQQ__Localization__c loc : localizations){
            if(!String.isBlank(loc.SBQQ__Text__c)){
                Map<String, String> languageTranslation = new Map<String, String>();
                languageTranslation.put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                if(!productLanguageTranslation.containsKey(loc.SBQQ__Product__c)){
                    productLanguageTranslation.put(loc.SBQQ__Product__c, new Map<String, String>(languageTranslation));
                }else{
                    productLanguageTranslation.get(loc.SBQQ__Product__c).put(loc.SBQQ__Language__c, loc.SBQQ__Text__c);
                }
            }
        }
                
        for(OrderItem ql : ordItemList){
            String productName = ql.PricebookEntry.Product2.Name;
            System.debug(' productName1 '+  productName);
            if(productLanguageTranslation.containsKey(ql.Product2Id) && productLanguageTranslation.get(ql.Product2Id).containsKey(language))
            {
                productName = productLanguageTranslation.get(ql.Product2Id).get(language);
            }
            
            System.debug(' productName2 '+  productName);
			OrderItemwrapper wrap = new OrderItemwrapper();
            wrap.id= ql.id;
            wrap.status=ql.SBQQ__Status__c;
            wrap.quantity = ql.Quantity;
            wrap.productName = productName;
            wrap.productCode = ql.PricebookEntry.Product2.ProductCode;
            wrap.unitPrice = ql.UnitPrice;
            wrap.product2Id = ql.Product2Id;
            wrap.total = ql.Total__c;
            if(ql.SBQQ__QuoteLine__r.Base_term_Renewal_term__c != null) wrap.baseTerm = Integer.valueOf(ql.SBQQ__QuoteLine__r.Base_term_Renewal_term__c);
            else if(ql.SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c != null) wrap.baseTerm = Integer.valueOf(ql.SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c);
            if(ql.SBQQ__QuoteLine__r.Subscription_Term__c != null) wrap.subTerm = Integer.valueOf(ql.SBQQ__QuoteLine__r.Subscription_Term__c);
            else if(ql.SBQQ__Subscription__r.Subscription_Term__c != null) wrap.subTerm = Integer.valueOf(ql.SBQQ__Subscription__r.Subscription_Term__c);
            if(ql.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null) wrap.baseTermPrice = ql.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            else if(ql.SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null) wrap.baseTermPrice = ql.SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            //else if(ql.SBQQ__Subscription__r.SBQQ__ListPrice__c != null) wrap.baseTermPrice = ql.SBQQ__Subscription__r.SBQQ__ListPrice__c;
            if(ql.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null){
                Decimal roundedDiscount = (Decimal)Math.round(ql.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                wrap.discount = roundedDiscount;
            }
            else{
                Decimal roundedDiscount;
                if(ConstantsUtil.ORDER_TYPE_IMPLICIT_RENEWAL.equalsIgnoreCase(ql.Order.Type) && ConstantsUtil.FIRST_TERM_DISCOUNT.equalsIgnoreCase(ql.SBQQ__Subscription__r.Promotion_Type__c) ){
                    roundedDiscount = 0;
                }else{
                    roundedDiscount = (Decimal)Math.round(ql.SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c)/100;
                }
               	
                wrap.discount = roundedDiscount; 
            }
            
            //vlaudato
            wrap.orderItemNumber = ql.OrderItemNumber;
			if(ql.SBQQ__RequiredBy__c == null || (ql.SBQQ__RequiredBy__c != null && ql.Order.SBQQ__Quote__r.SBQQ__Type__c == ConstantsUtil.QUOTE_TYPE_AMENDMENT)){
                res.orderitem.add(wrap);
            } else {
                for(OrderItemwrapper w : res.orderitem){
                    if(w.id == ql.SBQQ__RequiredBy__c){
                        w.childrens.add(wrap);
                        break;
                    }
                } 
            }
            
            
         system.debug('WRAP ITEM  ' + wrap);
        }
        
        system.debug('RES ITEM ' + JSON.serialize(res));
        return res;
		
	}
    
    @AuraEnabled
    public static String getProfileName(){
        Id profileId = userinfo.getProfileId();
        System.debug('Profile id: '+profileId);
		String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        System.debug('Profile name from Apex: '+profileName);
        return profileName;
    }
        
		
	public class OrderItemResponse{
        @auraEnabled public boolean valid {get; set;}
        @auraEnabled public List<OrderItemwrapper> orderitem {get; set;}
        public OrderItemResponse(){
            orderitem = new List<OrderItemwrapper>();
        }
    }
    
    public class OrderItemwrapper{
        @auraEnabled public Id id {get; set;}
        @auraEnabled public Id product2Id {get; set;}
		@auraEnabled public Decimal quantity {get; set;}
        @auraEnabled public String productCode {get; set;}
        @auraEnabled public string productName {get; set;}
		//vlaudato
        @AuraEnabled public String orderItemNumber {get;set;}        
        @auraEnabled public string status {get; set;}											
        @auraEnabled public List<OrderItemwrapper> childrens {get; set;}
        @auraEnabled public Decimal unitPrice {get; set;}
        @auraEnabled public Decimal total {get;set;}
        
        //Laudato
        @auraEnabled public Integer baseTerm {get; set;}	
        @auraEnabled public Integer subTerm {get; set;}	
        @auraEnabled public Decimal baseTermPrice {get; set;}	
        @auraEnabled public Decimal discount {get; set;}	
        
		public OrderItemwrapper(){
			childrens = new List<OrderItemwrapper>();
		}
	}
}