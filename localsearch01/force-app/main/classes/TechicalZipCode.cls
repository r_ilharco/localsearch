public class TechicalZipCode{
    
    public class InputParam{
        @InvocableVariable
        public String objType;
        
        @InvocableVariable
        public Id recId;
    }
        
    @InvocableMethod(label='Run Assignment Rules' description='Run Assignment Rules')
    public static void invokeAssignmentRule(List<InputParam> myData)
    {
        System.debug('********** '+myData);
        List<Id> recordIds = new List<Id>();
        for(InputParam aIP : myData){
            
            recordIds.add(aIP.recID);
            
        }
        
        if((myData[0].objType).equalsIgnoreCase(ConstantsUtil.LEAD_TECH_ZIP_CODE)){
            system.debug('LEAD_TECH_ZIP_CODE'+ myData[0].objType);
            runLeadAssignment(recordIds);
        }else if((myData[0].objType).equalsIgnoreCase(ConstantsUtil.ACCOUNT_TECH_ZIP_CODE)){
            runAccountAssignment(recordIds);

        }
        // Case Management - run Case Assignment Rule - FA 27-09-2019
        else if((myData[0].objType).equalsIgnoreCase(ConstantsUtil.CASE_TECH_ZIP_CODE)){
            system.debug('CASE_TECH_ZIP_CODE'+ myData[0].objType);            
            runCaseAssignment(recordIds); 
        }
    }


    public static void runLeadAssignment(List<Id> recId){
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;          
        List<Lead> Leads=[SELECT Id FROM Lead WHERE Id in :recId];
        System.debug('*** BF Leads: '+Leads);
        for(Lead aLead : Leads){
            aLead.setOptions(dmo);    
        }
        System.debug('*** AF Leads: '+Leads);
        update Leads;
    
    }
    
       public static void runAccountAssignment(List<Id> recId){
        
        //Database.DMLOptions dmo = new Database.DMLOptions();
        //dmo.assignmentRuleHeader.useDefaultRule= true;          
        List<Account> accounts=[SELECT Id FROM Account WHERE Id in :recId];
        //Leads.setOptions(dmo);
        update accounts;
    
    }

    // Case Management - run Case Assignment Rule - FA 27-09-2019
    public static void runCaseAssignment(List<Id> recId){
        system.debug('*****Inizio runCaseAssignment');    
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;          
        List<Case> Cases=[SELECT Id,OwnerId,Run_Assignment__c FROM Case WHERE Id IN: recId];
        system.debug('*****Cases=[SELECT Id,OwnerId FROM Case WHERE Case.Id IN: recId]= '+Cases);
        
        for(Case aCase : Cases){
            aCase.setOptions(dmo); 
            aCase.Run_Assignment__c=false; //aggiunta - FA 02-10-2019  
        }
        
        system.debug('*****dopo assegnazione '+Cases);
        update Cases;
    
    }
 

}