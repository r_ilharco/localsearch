/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-21-2019
 * Sprint      : 1
 * Work item   : US_61 - blocking down selling on a quote for the same place
 * Testclass   :
 * Package     : 
 * Description : Selector Class for Product2
 * Changelog   : 
 */

public with sharing class SEL_Product2 {
    public SEL_Product2() {

    }

    public static Map<Id, Product2> getProducts(Set<Id> ids)
    {
        /*A. L. Marotta 12-06-19 Wave 2, Sprint 3
        Added Subscription_Term__c in query*/
        return new Map<Id, Product2>([SELECT Id, Name, Priority__c, SBQQ__Component__c, One_time_Fee__c, Subscription_Term__c, SBQQ__NonDiscountable__c, PlaceIDRequired__c, InvoiceCycleSet__c, Product_Group__c, DocumentsFolder__c 
                FROM Product2
                WHERE Id IN :ids]);
    }
    
    public static Map<Id, Product2> getProductsMap(Set<Id> ids)
    {
        /*A. L. Marotta 12-06-19 Wave 2, Sprint 3
        Added Subscription_Term__c in query*/
        return new Map<Id, Product2>([SELECT Id, Name, Priority__c, SBQQ__Component__c, One_time_Fee__c, Subscription_Term__c, SBQQ__NonDiscountable__c, PlaceIDRequired__c, InvoiceCycleSet__c, toLabel(Product_Group__c), DocumentsFolder__c 
                FROM Product2
                WHERE Id IN :ids]);
    } 
}