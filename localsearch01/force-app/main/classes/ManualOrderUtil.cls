public without sharing class ManualOrderUtil {
    
    @InvocableMethod(label='Termination Cancellation Manual Orders' description='Termination Cancellation Manual Orders')
    public static void activateContracts(List<Id> recordIds){
        
        Set<Id> ordersIds = new Set<Id>();
        Set<Id> manualTerminationOrderIds = new Set<Id>();
        
        List<Order> orderList = SEL_Order.getOrdersByIds(recordIds);
        
        for(Order o : orderList){
            ordersIds.add(o.Id);
        }
        
        if(!ordersIds.isEmpty()){
            List<OrderItem> orderItems = [SELECT Id, OrderId, Product2.Manual_Termination__c FROM OrderItem WHERE OrderId IN :ordersIds AND SBQQ__RequiredBy__c = NULL];
            for(OrderItem oi : orderItems){
                if(oi.Product2.Manual_Termination__c){
					manualTerminationOrderIds.add(oi.OrderId);
                }
            }
            if(!manualTerminationOrderIds.isEmpty()){
                OrderUtility.activateContracts(new List<Id>(manualTerminationOrderIds));
            }
        }
    }

    public static void cancellationTerminationContract(Set<Id> ordersIds){
        Set<Id> contractIds = new Set<Id>();
        Set<Id> contractsToTerminate = new Set<Id>();
        Set<Id> contractsToCancel = new Set<Id>();
        Set<Id> parentOrdersIds = new Set<Id>();
        Set<SBQQ__Subscription__c> subscriptionsToUpdate = new Set<SBQQ__Subscription__c>();
        Set<Contract> contractsToUpdate = new Set<Contract>();
        Map<Id, Set<OrderItem>> orderToOrderItems = new Map<Id, Set<OrderItem>>();
        Map<Id, Order> orderMap = new Map<Id, Order>();
        Map<Id,Contract> contracts = new Map<Id,Contract>();
        Map<String,Date> contractToEndDate = new Map<String,Date>();

        //Query for orders and their order items
        List<Order> orders = SEL_Order.getOrdersByIds(new List<Id>(ordersIds));
        if(!orders.isEmpty()) SRV_ManualOrderUtil.generateOrderMap(orders, orderMap);
        List<OrderItem> orderItems = SEL_OrderItem.getOrderItemByOrderIds(ordersIds);
        if(!orderItems.isEmpty()) SRV_ManualOrderUtil.generateOrderToOrderItemsMap(orderItems, orderToOrderItems);

        if(!orderMap.isEmpty() && !orderToOrderItems.isEmpty()){
            for(Order currentOrder : orderMap.values()){
                contractIds.add(currentOrder.Contract__c);
                if(ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(currentOrder.Custom_Type__c)){
                	contractsToTerminate.add(currentOrder.Contract__c); 
                }
                else if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(currentOrder.Type)){
                    contractsToCancel.add(currentOrder.Contract__c);
                }
            }
            if(!contractIds.isEmpty()){
                contracts = SEL_Contract.getAllContractBycontractIds(contractIds);
                List<AggregateResult> maxEndDates = SEL_Subscription.getMaxTerminationEndDatebyContract(contractIds);
                Map<String,Date> endDateMap = SRV_ManualOrderUtil.generateEndDateMap(maxEndDates);
                List<SBQQ__Subscription__c> subsList = SEL_Subscription.getSubscriptionsByContractIds(contractIds);

                for(Order currentOrder : orderMap.values()){
                    Contract contract = contracts.get(currentOrder.Contract__c);
                    if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(currentOrder.Type) &&  ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(currentOrder.Status)){
                        SRV_ManualOrderUtil.cancelContract(contract, endDateMap, contract.Id);
                        contractsToUpdate.add(contract);
                        contractIds.add(contract.Id);
                        SRV_ManualOrderUtil.cancelSubscription(subsList, endDateMap, subscriptionsToUpdate, contract.Id);
                    }
                    else if(ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(currentOrder.Custom_Type__c) && ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(currentOrder.Status)){
                        SRV_ManualOrderUtil.terminateContract(contract, endDateMap, contract.Id);
                        contractsToUpdate.add(contract);
                        contractIds.add(contract.Id);
                        if(ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(currentOrder.Type)){
                            List<SBQQ__Subscription__c> subscriptionsToTerminate = SRV_ManualOrderUtil.getSubscriptionsToTerminate(subsList, orderToOrderItems.get(currentOrder.Id), contract.Id);
                            SRV_ManualOrderUtil.terminateSubscription(subscriptionsToTerminate, endDateMap, subscriptionsToUpdate, contract.Id, false);
                        }
                        else if(ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(currentOrder.Type)){
                            parentOrdersIds.add(currentOrder.Parent_Order__c);
                            List<SBQQ__Subscription__c> subscriptionsToTerminate = SRV_ManualOrderUtil.getUpgradeSubscriptionsToTerminate(subsList, orderToOrderItems.get(currentOrder.Id), contract.Id);
                            SRV_ManualOrderUtil.terminateSubscription(subscriptionsToTerminate, endDateMap, subscriptionsToUpdate, contract.Id, true);
                        }
                    }
                }
                if(!subscriptionsToUpdate.isEmpty()){
                    update new List<SBQQ__Subscription__c>(subscriptionsToUpdate);
                }
            }
        }
        if(!contractsToUpdate.isEmpty()){
            Set<Id> contractsToUpdateIds = new Set<Id>();
            for(Contract currentContract : contractsToUpdate) contractsToUpdateIds.add(currentContract.Id);
            if(!contractsToUpdateIds.isEmpty()){
                Set<Id> contractsToRemove = new Set<Id>();
                List<SBQQ__Subscription__c> subs = SEL_Subscription.getSubscriptionsByContractIds(contractsToUpdateIds);
                Map<Id, List<SBQQ__Subscription__c>> subscriptionMap = new Map<Id, List<SBQQ__Subscription__c>>();

                for(SBQQ__Subscription__c s: subs){
                    if(!subscriptionMap.containsKey(s.SBQQ__Contract__c)) subscriptionMap.put(s.SBQQ__Contract__c, new List<SBQQ__Subscription__c>{s});
                    else subscriptionMap.get(s.SBQQ__Contract__c).add(s);
                }
                
                for(Id currentKey: subscriptionMap.keySet()){
                    Boolean toRemove = false;
                    for(SBQQ__Subscription__c currentSubscription: subscriptionMap.get(currentKey)){
                        if(!currentSubscription.Subsctiption_Status__c.equalsIgnoreCase(ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED) && !currentSubscription.Subsctiption_Status__c.equalsIgnoreCase(ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED)){
                            toRemove = true;
                            break;
                        } 
                    }
                    if(toRemove) contractsToRemove.add(currentKey);
                }
                
                for(Contract currentContract : contractsToUpdate){
                    if(contractsToRemove.contains(currentContract.Id)) contractsToUpdate.remove(currentContract);
                }
                if(!contractsToUpdate.isEmpty()) update new List<Contract>(contractsToUpdate);
            }      
        }

        if(!contractsToCancel.isEmpty()) SRV_ManualOrderUtil.cancelContract(contractsToCancel); 
        if(!contractsToTerminate.isEmpty()) SRV_ManualOrderUtil.terminateContract(contractsToTerminate); 
        if(!parentOrdersIds.isEmpty()) SRV_ManualOrderUtil.evaluateUpgradeActivationOrder(parentOrdersIds);
        SRV_ManualOrderUtil.updateCase(ordersIds);
    }
}