/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-20-2019
 * Sprint      : 1
 * Work item   : US_19
 * Testclass   : Test_CategoryLocationTrigger
 * Package     : 
 * Description : Trigger Handler on Category_Location__c
 * Changelog   : 
 */

public class CategoryLocationTriggerHandler implements ITriggerHandler{
	public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems) {}
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<Category_Location__c> catlocInUpdate = (List<Category_Location__c>) newItems.values();
        Set<Id> places = new Set<Id>();

        for(Category_Location__c cl: catlocInUpdate)
        {
            if(cl.mainLocation__c==true && cl.RecordTypeId == Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId())
            {
                places.add(cl.Place__c);
            }
        }

        List<Category_Location__c> setMainLocationToFalse = [SELECT Id
                                                            FROM Category_Location__c 
                                                            WHERE RecordType.DeveloperName=:ConstantsUtil.RELATION_RT_DEVELOPERNAME
                                                            AND mainLocation__c = true
                                                            AND Category_Location__r.RecordType.DeveloperName=:ConstantsUtil.LOCATION_RT_DEVELOPERNAME
                                                            AND Place__c IN :places AND Id NOT IN :newItems.keySet()];
        
        for(Category_Location__c cl: setMainLocationToFalse)
        {
            cl.mainLocation__c = false;
        }

        update setMainLocationToFalse;
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {}
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}