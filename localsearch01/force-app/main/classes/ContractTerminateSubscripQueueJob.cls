global without sharing class ContractTerminateSubscripQueueJob implements Queueable {

	private List<Contract> vlistContract;
   
    public ContractTerminateSubscripQueueJob(List<Contract> listContract) {
        vlistContract= listContract;       
    }    
  global void execute(QueueableContext SC) {
	 ContractUtility.TerminateSubscripJob(vlistContract);
  }
}