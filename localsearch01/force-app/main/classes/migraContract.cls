global class migraContract implements Database.Batchable<SObject>, Database.Stateful  {
    
    public string batch;
    public boolean sl;
    
    public migraContract(string batchName, boolean sl_migration){
        batch = batchName;
        sl = sl_migration;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) { //Id='a0q1X000000jBzzQAE' AND  
		return Database.getQueryLocator([SELECT Id,AccountId,SBQQ__Quote__c  FROM Contract WHERE Status='Draft' and SBQQ__Quote__r.Quote_Migration_Batch__c =: batch  and SBQQ__Quote__r.SL_Migration__c =: sl]);
	}   
    
    
	global void execute(Database.BatchableContext BC, List<Contract> scope) {
        List<Contract> contractsToUpdate = new List<Contract>();
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        for(Contract c : scope){
            ContractUtility.ContractSubsWrapper tempMap = ContractUtility.ChangeContractStatus(c, ContractUtility.CallName.Activation, ConstantsUtil.CONTRACT_STATUS_PRODUCTION, ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION, null);                       
            if(!tempMap.subscriptions.isEmpty()){
                contractsToUpdate.add(tempMap.contr);
            }
            if(!tempMap.subscriptions.isEmpty()){
                subscriptionsToUpdate.addAll(tempMap.subscriptions);
            }
        }
        update contractsToUpdate;
        update subscriptionsToUpdate;
    }
    
    
	global void finish(Database.BatchableContext BC) {
        
    }
}