@isTest
public class Test_SetLBxContactController {
	
    @testSetup
    public static void test_setupData(){
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;
        
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        String opportunityId = opportunity.Id;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__Quote__c quote2 = quote.clone(false,false,false,false);
        quote2.SBQQ__Status__c = 'In Review';
        insert quote2;
        
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id = :productCodeToProduct.get('LBB001').Id];
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        insert fatherQuoteLine;
        SBQQ__QuoteLine__c exceptionQuoteLine = Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        insert exceptionQuoteLine;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
		QuoteLineTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_getContacts(){
        List<String> fieldNamesQuoteLine = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__QuoteLine__c').getDescribe().fields.getMap().keySet());
        String queryQuoteLine = 'SELECT ' +String.join( fieldNamesQuoteLine, ',' ) +' FROM SBQQ__QuoteLine__c LIMIT 1';
        SBQQ__QuoteLine__c quoteLine = Database.query(queryQuoteLine);
        Test.startTest();
        List<SelectOptionLightning> availableContact = SetLBxContactController.contactToChoose(quoteLine.Id);
        Test.stopTest();
        System.assertEquals(2, availableContact.size());
    }
    @isTest
    public static void test_setContact(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        String accountId = account.Id;
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Quote__r.SBQQ__Status__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.SBQQ__Status__c != 'In Review' LIMIT 1];
		Contact contact = [SELECT Id FROM Contact WHERE AccountId = :accountId LIMIT 1];
        SetLBxContactController.resultFromQL sv = new SetLBxContactController.resultFromQL();
        
        Test.startTest();
        sv = SetLBxContactController.updateQLWithContact(quoteLine.Id, contact.Id);
        Test.stopTest();
        
        SBQQ__QuoteLine__c quoteLineAfter = [SELECT Id, LBx_Contact__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLine.Id LIMIT 1];
        System.assertEquals(true, sv.isSuccess);
        System.assertEquals(contact.Id, quoteLineAfter.LBx_Contact__c);
    }
    
    
    @isTest
    public static void test_setContactException(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        String accountId = account.Id;
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Quote__r.SBQQ__Status__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.SBQQ__Status__c = 'In Review' LIMIT 1];
		Contact contact = [SELECT Id FROM Contact WHERE AccountId = :accountId LIMIT 1];
        SetLBxContactController.resultFromQL sv = new SetLBxContactController.resultFromQL();
        
        Test.startTest();
        sv = SetLBxContactController.updateQLWithContact(quoteLine.Id, contact.Id);
        Test.stopTest();
        
        SBQQ__QuoteLine__c quoteLineAfter = [SELECT Id, LBx_Contact__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLine.Id LIMIT 1];
        
        
    }
}