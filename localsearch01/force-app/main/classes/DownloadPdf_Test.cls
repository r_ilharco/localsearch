@IsTest
public class DownloadPdf_Test {

    private static string jwtPayload = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImVzYiJ9.eyJpc3MiOiIwMEQ5RTAwMDAwMERxTEtVQTAvQmlsbGluZyIsInN1YiI6IkJpbGxpbmcvRG93bmxvYWRQREYiLCJhdWQiOiIwMDU5RTAwMDAwNjgxTGtRQUkiLCJpYXQiOjE1NTMwNjYzNjEsIm5iZiI6MTU1MzA2NjM2MSwiZXhwIjoxNTUzMDY5OTYxLCJqdGkiOiI4MzNiMzA2Zi0yMTViLTQ4ZGMtODU2OS1mMDNkNThmZTA1MzEiLCJpbnZvaWNlSWQiOiI0MTI2MjFjZmNlOWNjMWFjOWJkMjZiZWVmODgxYjRjYiJ9.biFTDfo2TdhIRsCWKny0iYMgkoLg-2Q4SUh_-XpGYtxxjyeNcY5J-R51WCcnzu-epo5sLSX5yrMAGewezFLxdnb4LxUXc_WGero6A1Rf-486XnY7g9uc7Xgyy0p_P0Q5X0QTuz7duaBbwe6ROOa3gbnU8WTz9yodhz0cbRdHtm30VJcmej8IABiCwuE3nzivCO9eER02qxf4jUwlT_wCI70LJciPIeKru_Vweo5QHVKmDuEyy9rE9VxgwolqWzZvMM2rcDJY-7EcWllboWwYuzHnSb9XqXfZCR1Tfwen-QMDh_K5hQlCYX24QAuuxy_yWxmY7JWlSoAv5GFduDf8ntFC0XBaSmjREDM9OlazDxGPsYHPhacShK2unnzPtipo8rYxFau_Ycw8xIxsNrcIXb3cF496QbzjwWjJIYLgcixvnXULPLxMojLkBG7wM7g2Ae6UMFxyYgw5K_xOvMkM6nm3pqdUCRxsgM16Jy0-62o7BBhhBSCaeV2bxsFifdEQOCUUK-hDpicYtG0-nHNz4C9E98r8HW6CyLkjbaIOYu1deVdMSGPwaC4N806B4AJHIotdMYc24F2ADnpszhMDeiJu-cH2CHyjWW-6xKxfjKBKRBkaWWJw_0YVlDQzmlT3WWCIk9yytJxzYtgSWNMe3pmjl-am1PIp4Gl-kgwQHJk';
    private static testmethod void test_001() {
        System.assertEquals(null, DownloadPdf.intToDateTime(null));
        System.assertEquals(DateTime.newInstanceGmt(2019, 3, 20, 6, 55, 15), DownloadPdf.intToDateTime(1553064915));
        
        
        DownloadPdf.JwtPayload jwt = genJwtPayload();
        System.assertEquals(true, DownloadPdf.checkClaims(jwt));
        jwt.aud = 'blabla';
        System.assertEquals(false, DownloadPdf.checkClaims(jwt));
        jwt = genJwtPayload(); jwt.sub = 'blabla';
        System.assertEquals(false, DownloadPdf.checkClaims(jwt));
        jwt = genJwtPayload(); jwt.iss = 'blabla';
        System.assertEquals(false, DownloadPdf.checkClaims(jwt));
        jwt = genJwtPayload(); jwt.nbf = jwt.nbf + 100;
        System.assertEquals(false, DownloadPdf.checkClaims(jwt));
        jwt = genJwtPayload(); jwt.exp = jwt.nbf;
        System.assertEquals(false, DownloadPdf.checkClaims(jwt));
    }
    
	
    private static testmethod void test_002_invoice() {
        AbacusTransferHttpCalloutMock xmlResponse = new AbacusTransferHttpCalloutMock();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
        //System.assert(resp.responseBody.toString().startsWith('Invalid or expired token'), 'Expected expired token response');
    } 
    
    private static testmethod void test_002_creditNotes() {
        BillingResults.docToDownload = 'credit-note';
        AbacusTransferHttpCalloutMock xmlResponse = new AbacusTransferHttpCalloutMock();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
        //System.assert(resp.responseBody.toString().startsWith('Invalid or expired token'), 'Expected expired token response');
    } 
    
      private static testmethod void test200_creditNotes() {
        BillingResults.docToDownload = 'credit-note';
        MockDownloadPDFResponse xmlResponse = new MockDownloadPDFResponse();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
    }
    private static testmethod void test200_invoice() {
        BillingResults.docToDownload = 'invoice';
        MockDownloadPDFResponse xmlResponse = new MockDownloadPDFResponse();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
    } 
    
     private static testmethod void test400_invoice() {
        BillingResults.docToDownload = 'invoice';
        MockDownloadPDFResponse400 xmlResponse = new MockDownloadPDFResponse400();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
    } 
    
     private static testmethod void test400_creditNote() {
        BillingResults.docToDownload = 'credit-note';
        MockDownloadPDFResponse400 xmlResponse = new MockDownloadPDFResponse400();
        Test.setMock(HttpCalloutMock.class, xmlResponse);
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', jwtPayload);
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        Test.stopTest();
    } 

	private static testmethod void test_003() {
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.params.put('jwt', '0.0.0');
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        //System.assert(resp.responseBody.toString().startsWith('The signed token doesn'), 'Expected invalid signature');
    }

	private static testmethod void test_004() {
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        // req.params.put('jwt', '0.0.0');
        RestContext.request = req;
        RestContext.response = resp;
        DownloadPdf.getDownload();
        //System.assert(resp.responseBody.toString().startsWith('Exception occurred'), 'Expected exception');
    }

	
    private static testmethod void test_005() {
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        DownloadPdf.JwtPayload jwt = genJwtPayload();
        jwt.invoiceId = 'invoiceId';
        string jwtToken = new Auth.JWS(JSON.serialize(jwt), ConstantsUtil.CERTIFICATE_NAME).getCompactSerialization();
        req.params.put('jwt', jwtToken);
        RestContext.request = req;
        RestContext.response = resp;
        //DownloadPdf.getDownload();
        // Exception caused by callout
        //System.assert(resp.responseBody.toString().startsWith('Exception occurred'), 'Expected exception');
    }   

    private static DownloadPdf.JwtPayload genJwtPayload() {
        DownloadPdf.JwtPayload jwt = new DownloadPdf.JwtPayload();
        jwt.aud = System.UserInfo.getUserId();
        jwt.sub = 'Billing/DownloadPDF';
        jwt.iss = System.UserInfo.getOrganizationId() + '/Billing';
        jwt.nbf = getNowLong() - 5;
        jwt.exp = jwt.nbf + long.valueOf(ConstantsUtil.JWT_TOKEN_VALIDITY_SECONDS);
        return jwt;
    }
    
    private static long getNowLong() {
        long res = 0;
        DateTime now = DateTime.now();
        res += DateTime.newInstanceGmt(1970, 1, 1, 0, 0, 0).date().daysBetween(now.dateGmt()) * 24 * 3600;
        res += now.hourGmt() * 3600 + now.minuteGmt() * 60 + now.secondGmt();
        return res;
    }
    
    public class MockDownloadPDFResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
             res.setStatusCode(200); 
            return res;
        }
    }
    
    public class MockDownloadPDFResponse400 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
             res.setStatusCode(400); 
            return res;
        }
    }
}