/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/


@IsTest
public class Test_AutomaticCreationTasksBatch {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management, Set Contract Start/End date for Quote, Contract Handler	Insert Only';
        insert byPassTrigger;
    }
    
    @Testsetup  
    static void setup(){
        
        insertBypassFlowNames();
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        ProductTaskConfiguration__mdt conf = [SELECT Id, TaskName__c, daysUntil__c FROM ProductTaskConfiguration__mdt WHERE TaskName__c = :Label.RenewalContractReminderTask_lbl];
        Integer daysUntil = 0;
        
        if(conf.daysUntil__c != NULL && Integer.valueOf(conf.daysUntil__c) > 0)
            daysUntil = Integer.valueOf(conf.daysUntil__c);
        
        List<Territory2> objTerr = [select id from Territory2];
        Profile p = [SELECT id, Name FROM Profile where name =: ConstantsUtil.SysAdmin].get(0);  
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1); 
        insert u;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Task t = new Task();
        t.Subject = 'Call';
        t.Status = 'Open';
        t.Type = 'Anruf';
        //t.WhatId = contact.Id;
        t.OwnerId = u[0].Id;
        insert t;
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generateFatherProduct());
        productsToInsert.add(generateChildProduct());	
        productsToInsert[0].Tasks__c = 'Renewal Contract Reminder Task';
        productsToInsert[1].Tasks__c = 'Renewal Contract Reminder Task';
        insert productsToInsert;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opportunity.Id, account.Id, contact.Id, null);
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote.SBQQ__CustomerDiscount__c = 5 ;
        quote.SBQQ__Primary__c = true;
        quote.AccountOwner__c = account.OwnerId;
        insert quote;
        opportunity.SBQQ__PrimaryQuote__c = quote.Id;
        update opportunity;
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Total__c FROM SBQQ__QuoteLine__c];
        
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        order.SambaIsStartConfirmed__c = true;
        insert order;
        
        SBQQ__QuoteLine__c fatherQL1;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL1 = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL1);
        fatherOI.SambaIsBilled__c = true;
        insert fatherOI;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.EndDate = Date.today().addDays(daysUntil);
        insert contract;
        
        contract.Status = 'Active';
        update contract;
        
        SBQQ__Subscription__c subs = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        subs.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subs.SBQQ__Product__c = productsToInsert[0].Id;
        insert subs;
        
        SBQQ.TriggerControl.enable();
        String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;       
    }
    
    @istest 
    static void test_automaticCreationTasksBatch(){
        ProductTaskConfiguration__mdt conf = [SELECT Id, TaskName__c, daysUntil__c FROM ProductTaskConfiguration__mdt WHERE TaskName__c = :Label.RenewalContractReminderTask_lbl];
        Integer daysUntil = 0;
        
        if(conf.daysUntil__c != NULL && Integer.valueOf(conf.daysUntil__c) > 0)
            daysUntil = Integer.valueOf(conf.daysUntil__c);
        
        String query = 'SELECT Id, Account.OwnerId, '
            + '(SELECT Id, SBQQ__EndDate__c, Subsctiption_Status__c, SBQQ__Product__r.Product_Group__c, SBQQ__Product__r.Name, SBQQ__Product__r.Tasks__c'
            + ' FROM SBQQ__Subscriptions__r'
            + ' WHERE('
            + ' (Place__c != \'\' AND SBQQ__Product__r.PlaceIDRequired__c = \'Yes\')'
            + ' OR'
            + ' (Place__c = \'\' AND SBQQ__Product__r.PlaceIDRequired__c = \'No\')'
            + ' OR SBQQ__Product__r.PlaceIDRequired__c = \'Optional\' '
            + ')'
            + ' AND SBQQ__ProductOption__c = NULL'
            + ')'
            + ' FROM Contract'
            + ' WHERE EndDate = NEXT_N_DAYS:' + daysUntil
            + ' AND EndDate > TODAY'
            + ' AND Status = \'' + ConstantsUtil.CONTRACT_STATUS_ACTIVE + '\'';
        test.startTest();
        List<Contract> cont = Database.query(query);
        
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        AutomaticCreationTasksBatch dab = new AutomaticCreationTasksBatch();
        dab.execute(null);
        dab.start(null);
        dab.execute(null,cont);
        dab.finish(null);
        test.stopTest();   
    }
    
    public static Product2 generateFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generateChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}