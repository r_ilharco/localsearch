@isTest
public class Test_ContractBeforeActive_BatchSch {
    
    public static String CRON_EXP = '0 0 0 20 6 ? 2100';
    
    @testSetup
    static void setup() {
        
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        insert acc;
        
        Opportunity opp = Test_DataFactory.createOpportunities('testOpp', ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON, acc.Id, 1)[0];
        insert opp;
            
        SBQQ__Quote__c qt = Test_DataFactory.createQuotes(ConstantsUtil.Quote_Status_Draft, opp, 1)[0];
        insert qt;
        
        System.debug('***CHECK HERE: '+Date.valueOf(System.Today()));
        System.debug('***CHECK HERE 2: '+Integer.valueOf(Label.Contract_job_Prior_days));
        
        List<Contract> contrct = Test_DataFactory.createContracts(acc.Id, 1);
        for(Contract c: contrct){
            c.Status = ConstantsUtil.CONTRACT_STATUS_DRAFT;
        	c.StartDate = Date.valueOf(System.Today());
            c.SBQQ__Quote__c = qt.Id;
        }
        insert contrct;
    }
    
    @isTest    
        public static void test_runScheduled_ContractBeforeActive_BatchSch(){
            Test.startTest();
            String jobId = System.schedule('ContractBeforeActive_BatchSch',
                CRON_EXP, 
                new ContractBeforeActive_BatchSch());
            Test.stopTest();
            
            List<AsyncApexJob> jobs = [SELECT Id 
                                       FROM AsyncApexJob 
                                       WHERE ApexClassID IN 
                                        (SELECT Id 
                                         FROM ApexClass 
                                         WHERE Name = 'ContractBeforeActive_BatchSch')];
                                       
            System.assertEquals(2, jobs.size());
        }
        
        @isTest
        public static void test_runContractBeforeActive_BatchSch(){
            
            Test.startTest();
            ContractBeforeActive_BatchSch cbab = new ContractBeforeActive_BatchSch();
            Id batchId = Database.executeBatch(cbab);
            Test.stopTest();
            
        }

}