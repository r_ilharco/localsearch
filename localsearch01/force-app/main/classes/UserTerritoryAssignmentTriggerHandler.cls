/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mazzarella Mara <mamazzarella@deloitte.it>
* @created		  2019-10-06
* @systemLayer    Trigger
* @TestClass 	  UserTerritoryAssignmentTriggerTest
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Mazzarella Mara
* 2019-10-06      Sprint 3 / W2-Territory Management - Check if user are already assigned to other territory
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*				  
* @changes
* @modifiedby      Gennaro Casola <gcasola@deloitte.it>
* 2020-06-12      SFRO-389
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*				  
* @changes
* @modifiedby      Gennaro Casola <gcasola@deloitte.it>
* 2020-06-24      [SPIII-1147] TERRITORY MODEL STRUCTURE WITH TWO LEVELS (REGION AND TERRITORIES)
*	
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*				  
* @changes
* @modifiedby      Gennaro Casola <gcasola@deloitte.it>
* 2020-06-30      [SPIII-1149] TERRITORY DMC ASSOCIATION
*				  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class UserTerritoryAssignmentTriggerHandler implements ITriggerHandler{
    
    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
    
    public void BeforeInsert(List<sObject> newItems){
        UserTerritoryAssignmentTriggerHelper.checkUniqueTerritoryforUser(newItems);
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
		UserTerritoryAssignmentTriggerHelper.checkUniqueTerritoryforUser(newItems.values());
    }
    public void BeforeDelete(Map<Id, SObject> oldItems){
    }
    public void AfterInsert(Map<Id, SObject> newItems){
        UserTerritoryAssignmentTriggerHelper.manageAssignedTerritoryOnUsers(newItems, null);
        UserTerritoryAssignmentTriggerHelper.delegatedApproverManagement((Map<Id, UserTerritory2Association>) newItems);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        UserTerritoryAssignmentTriggerHelper.manageDeletedUserAssociations(oldItems);
        UserTerritoryAssignmentTriggerHelper.manageAssignedTerritoryOnUsers(newItems, oldItems);
        UserTerritoryAssignmentTriggerHelper.delegatedApproverManagement((Map<Id, UserTerritory2Association>) newItems);
    }
    public void AfterDelete(Map<Id, SObject> oldItems){
        //gcasola - 20200612 - SFRO-389
        UserTerritoryAssignmentTriggerHelper.manageDeletedUserAssociations(oldItems);
        UserTerritoryAssignmentTriggerHelper.manageAssignedTerritoryOnUsers(null, oldItems);
        UserTerritoryAssignmentTriggerHelper.delegatedApproverManagementForDelete((Map<Id, UserTerritory2Association>) oldItems);
    }
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}