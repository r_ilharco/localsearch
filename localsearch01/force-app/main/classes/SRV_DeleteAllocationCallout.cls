public class SRV_DeleteAllocationCallout {
	public static DeleteAllocationBatchResponse deleteAllocationCallout(Map<String,String> parameters, String correlationId, String recordId){
        
        DeleteAllocationBatchResponse deleteAllocationBatchResponse = new DeleteAllocationBatchResponse();
        
		deleteAllocationBatchResponse.isSuccess = false;        
        String endpoint = '';
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.AD_CONTEXT_ALLOCATION);
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        
        Profile p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2 = Mashery_Setting__c.getInstance(p.Id);
        
        Decimal cached_millisecs;
        
        if(c2.LastModifiedDate !=null) cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        Boolean isNewToken = false;
        
        if(c2.Token__c == null || c2.Token__c == '' ||((cached_millisecs/1000).round()>=(Integer)c2.Expires__c)){
            isNewToken=True;
            token = UtilityToken.refreshToken();
        } 
        else{
            token.access_token= c2.Token__c;
            token.token_type= c2.Token_Type__c;
            token.expires_in= (Integer)c2.Expires__c;           
        }
        
        if(!parameters.isEmpty()){
            endpoint = c2.Endpoint__c + methodName;
            endpoint+='/'+parameters.get('ad_context_allocation');
        }
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setHeader('X-LS-Tracing-CorrelationId',correlationId);
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Authorization', 'Bearer '+token.access_token);
        request.setMethod('DELETE');
        request.setTimeout(60000);
        
        HttpResponse response;
        
        try{
            response = http.send(request);
            if(response.getStatusCode() == 200){
                deleteAllocationBatchResponse.isSuccess = true;
            }
            deleteAllocationBatchResponse.integrationTask = LogUtility.saveCasaReservationLog(methodName+'/'+parameters.get('ad_context_allocation')+' DELETE', response, null, correlationId, recordId, null);
        }
        catch(Exception e){
			deleteAllocationBatchResponse.integrationTask = LogUtility.saveCasaReservationLog(methodName+'/'+parameters.get('ad_context_allocation')+' DELETE', response, null, correlationId, recordId, e);          
        }
        return deleteAllocationBatchResponse;
    }
    public static String generateCorrelationId(String recordId){
        String hashString = recordId + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        return EncodingUtil.convertToHex(hash);
    }
    
    public class DeleteAllocationBatchResponse{
        public Boolean isSuccess {get;set;}
        public Log__c integrationTask {get;set;}
    }
}