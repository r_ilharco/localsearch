/**
 * Author	   : Mohammed Soliman <mosoliman@deloitte.it>
 * Date		   : 04-10-2019
 * Sprint      : 
 * Work item   : Lead creation Management - SF2-772
 * Testclass   : Test_LeadTrigger
 * Package     : 
 * Description : Trigger Helper On Lead  
 * Changelog   : 
 */

public class ATL_LeadTriggerHelper{

    public static void setManualLeadStatusFromRating(Lead[] newLeads){
        for(Lead currentLead: newLeads){
            //change Lead Status to new if it's created manually with cold or warm rating 
            if (currentLead.LeadSource== ConstantsUtil.LEAD_SOURCE_MANUAL_Sales && (currentLead.Rating== ConstantsUtil.LEAD_RATING_COLD || currentLead.Rating== ConstantsUtil.LEAD_RATING_WARM)){
                currentLead.Status= ConstantsUtil.LEAD_STATUS_NEW;
            }
            //change Lead Status to new if it's created manually with Hot
            if (currentLead.LeadSource== ConstantsUtil.LEAD_SOURCE_MANUAL_Sales && currentLead.Rating== ConstantsUtil.LEAD_RATING_HOT){
                currentLead.Status= ConstantsUtil.LEAD_STATUS_QUALIFIED;
            }
        } 
    }
    
    public static void checkNewLeadsLegalAddress(List<Lead> newItems){

        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
        
        if(Trigger.operationType == System.TriggerOperation.BEFORE_INSERT){
            for(Lead ld: newItems){
                if(addressValidationMDT.Active__c){
                ld.AddressValidationStatus__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                ld.AddressValidationDate__c = NULL;
                ld.Address_Integration__c = true;
                }
            }
        }
        else if(Trigger.operationType == System.TriggerOperation.AFTER_INSERT){
            Set<Id> leadIdsToUpdate = new Set<Id>();
            for(Lead ld: newItems){
                if(addressValidationMDT.Active__c){
                    try{
                    	SRV_IntegrationOutbound.asyncValidateAddress(ld.Id);
                    }
                    catch(Exception e){
                        leadIdsToUpdate.add(ld.Id);
                    }
                }
            }
            
            if(leadIdsToUpdate.size() > 0){
                List<Lead> leadsToUpdate = SEL_Lead.getLeadByIds(leadIdsToUpdate);
                for(Lead ld : leadsToUpdate){
                    ld.AddressValidationStatus__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                    ld.AddressValidationDate__c = NULL;
                	ld.Address_Integration__c = false;
                }
                update leadsToUpdate;
            }
        }
    }
    
    public static void checkLeadLegalAddress(Map<Id, Lead> NewLeads, Map<Id, Lead> OldLeads){
        
        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
		
        for(Lead ld: NewLeads.values()){
            Lead oldLd = OldLeads.get(ld.Id);
            if(addressValidationMDT.Active__c){
                if(((ld.City != null && oldLd.City != null && !oldLd.City.equalsIgnoreCase(ld.City))||
                (ld.Street != null && oldLd.Street != null && !oldLd.Street.equalsIgnoreCase(ld.Street))||
                (ld.PostalCode != null && oldLd.PostalCode != null && !oldLd.PostalCode.equalsIgnoreCase(ld.PostalCode))||
                (ld.UID__c != null && oldLd.UID__c != null && !oldLd.UID__c.equalsIgnoreCase(ld.UID__c))||
                (ld.Phone != null && oldLd.Phone != null && !oldLd.Phone.equalsIgnoreCase(ld.Phone))||
                (ld.Email != null && oldLd.Email != null && !oldLd.Email.equalsIgnoreCase(ld.Email)))
                ){         
                    if(!ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED.equals(ld.AddressValidationStatus__c)){
                    	ld.AddressValidationStatus__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                        ld.AddressValidationDate__c = NULL;
                    }
                    
                    SRV_IntegrationOutbound.asyncValidateAddress(ld.Id);
                    ld.Address_Integration__c = true;
                }
            }
        }
    }
    
    //A.L. Marotta SF2 13-01-2020 - extend Lead access through apex sharing to the SMA of that territory
    public static void extendLeadAccessThroughTerritoryHierarchy(Map<Id, Lead> NewLeads){
        
        Map<Id, List<Lead>> ownersToLeads = new Map<Id, List<Lead>>();
        
        for(Lead l : NewLeads.values()){
            if(ConstantsUtil.LEAD_SOURCE_MANUAL_Sales.equalsIgnoreCase(l.LeadSource)){
                if(!ownersToLeads.containsKey(l.OwnerId)){
            		ownersToLeads.put(l.OwnerId, new List<Lead>());
                    ownersToLeads.get(l.OwnerId).add(l);
                }
                else{
                    ownersToLeads.get(l.OwnerId).add(l);
                }
            }
        }
                
        if(!ownersToLeads.isEmpty()){
            Map<Id, UserTerritory2Association> associations = new Map<Id, UserTerritory2Association>([select id, UserId, Territory2Id, RoleInTerritory2, Territory2.ParentTerritory2Id 
                                                         from UserTerritory2Association 
                                                         where UserId in :ownersToLeads.keySet()
                                                         and RoleInTerritory2 =: ConstantsUtil.ROLE_IN_TERRITORY_DMC
                                                         and Territory2.ParentTerritory2Id <> null ]);
                        
            if(!associations.isEmpty()){
                Set<Id> secondLevelIds = new Set<Id>();
                for(UserTerritory2Association a: associations.values()){
                    secondLevelIds.add(a.Territory2Id);
                }
                
            	Map<Id, UserTerritory2Association> smaAssociations = new Map<Id, UserTerritory2Association>([select id, UserId, RoleInTerritory2, Territory2Id
                                                                                                         from UserTerritory2Association
                                                                                                         where Territory2Id in :secondLevelIds
                                                                                                         and RoleInTerritory2 =: ConstantsUtil.ROLE_IN_TERRITORY_SMA]);
                                
                if(!smaAssociations.isEmpty()){
                    
                    Map<Id, UserTerritory2Association> territoryTosmaAssociations = new Map<Id, UserTerritory2Association>();
                    
                    for(UserTerritory2Association ass : smaAssociations.values()){
                        territoryTosmaAssociations.put(ass.Territory2Id, ass);
                    }
                                    
                    Boolean sharingResult = false;
                    
                    for(UserTerritory2Association a : associations.values()){
                        if(territoryTosmaAssociations.containsKey(a.Territory2.ParentTerritory2Id)){
                            UserTerritory2Association sma = territoryTosmaAssociations.get(a.Territory2.ParentTerritory2Id);
                            List<Lead> leads = ownersToLeads.get(a.UserId);
                            if(!leads.isEmpty()){
                                for(Lead l : leads){
                                    sharingResult = SharingUtility.shareLeadManually(l.Id, sma.UserId, 'Edit');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}