@isTest
public class TerminateFutureSubscriptionsTest {
static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Place Name';
        insert byPassFlow;
    }
    @isTest
    public static void testTerminationSubscription(){
        insertbypassflownames();
        test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
        system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
        insert psas;
        }
        permissionSetAssignment psa = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
         user u = [select id from user where id=: psa.AssigneeId][0];
     
        
        
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.GoldenRecordID__c = 'GRID';       
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh';    
        
        insert acc;
       
        Place__c place = new Place__c();
        place.Name = 'test place';
        place.LastName__c = null;
        place.Company__c = null;
        place.City__c = 'test city';
        place.Country__c = 'test coountry';
        place.PostalCode__c = '1234';
        
        insert place;
        System.debug('Place: '+ place);
        
        Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';
                
            insert opp;
            System.Debug('opp '+ opp); 

  			Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '1234';
            myContact.MailingCountry = 'test country';
            
            insert myContact;
            System.Debug('Contact: '+ myContact);
        
            Billing_Profile__c billProfile = new Billing_Profile__c();
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '1234';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'test';   
            billProfile.Name = 'Test Bill Prof Name';
        	insert billProfile;
        
        
         	SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            quot.Billing_Profile__c= billProfile.id;
            
        	insert quot;
            System.Debug('Quote '+ quot);
        
        	Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            myContract.SBQQ__Opportunity__c= opp.Id;
        	myContract.SBQQ__Quote__c= quot.Id;
            myContract.OwnerId= u.Id;
                
            
            insert myContract;
            System.debug('Contract: '+ myContract); 
          

        	SBQQ__Subscription__c subs = new SBQQ__Subscription__c();
            subs.SBQQ__Account__c = acc.Id;
            subs.Place__c =place.Id;
            subs.SBQQ__Contract__c = myContract.Id;
            subs.SBQQ__Quantity__c = 2;
        	subs.Termination_Generate_Credit_Note__c = true;
        	subs.SBQQ__Bundled__c = true;
            subs.Subsctiption_Status__c=ConstantsUtil.SUBSCRIPTION_STATUS_IN_TERMINATION;
        	subs.Termination_Date__c=Date.today();
			
        system.runas(u){
        	insert subs;
        }
        	System.debug('Sub' + subs);
        	SBQQ__Subscription__c sub = [SELECT Id, Termination_Date__c, Subsctiption_Status__c, LastModifiedDate FROM SBQQ__Subscription__c 
                                         WHERE Id =: subs.Id ];
        	System.debug('Sub' + sub);
        	
            
            Test.startTest(); 
        	String CRON_EXP = '0 0 0 15 3 ? *';
            TerminateFutureSubscriptions obj = new TerminateFutureSubscriptions();
            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, obj );
            Test.stopTest(); 
    }
}