/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 
 * Sprint      : 
 * Work item   : 
 * Testclass   : Test_DeleteAllocationBatch
 * Package     : 
 * Description :
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @changes         2021-01-21
 * @modifiedby      Vincenzo Laudato
 * 2020-12-29       SPIII-5080 - Added condition to skip callout for blank allocation id got from 
 *                  String.split function 
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

global class DeleteAllocationBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts {
    
    global void execute(SchedulableContext sc) {
        DeleteAllocationBatch b = new DeleteAllocationBatch(); 
        database.executebatch(b,1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator('SELECT Id, Ad_Allocation_Ids__c FROM SBQQ__Quote__c WHERE Allocation_not_empty__c = true');
    }
    
    global void execute(Database.BatchableContext info, List<SBQQ__Quote__c> scope){
        List<Log__c> logsToInsert = new List<Log__c>();
        List<SBQQ__Quote__c> validQuotes = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c currentQuote : scope){
            if(String.isNotBlank(currentQuote.Ad_Allocation_Ids__c)){
                validQuotes.add(currentQuote);                
            }
        }
        if(!validQuotes.isEmpty()){
            Set<Id> quoteIds = new Set<Id>();
            for(SBQQ__Quote__c currentQuote : validQuotes){
                quoteIds.add(currentQuote.Id);              
            }
            if(!quoteIds.isEmpty()){
                Map<String, List<SBQQ__QuoteLine__c>> quoteToQLines = new Map<String, List<SBQQ__QuoteLine__c>>();
                //Retrieve quotelines of quotes in scope
                Map<Id,SBQQ__QuoteLine__c> allQuoteLine = ATL_QuoteTriggerHelper.retrieveQuoteLines(quoteIds);
				                
                for(SBQQ__QuoteLine__c currentQL : allQuoteLine.values()){
                    if(!quoteToQLines.containsKey(currentQL.SBQQ__Quote__c)){
                        quoteToQLines.put(currentQL.SBQQ__Quote__c, new List<SBQQ__QuoteLine__c>{currentQl});
                    }
                    else{
                        quoteToQLines.get(currentQL.SBQQ__Quote__c).add(currentQl);
                    }
                }
                for(SBQQ__Quote__c currentQuote : validQuotes){
                    Set<String> allocationIdToMantain = new Set<String>();
                    List<SBQQ__QuoteLine__c> quoteLinesToCheck = new List<SBQQ__QuoteLine__c>();
                    
                    List<String> adAllocationIdsSplitted = currentQuote.Ad_Allocation_Ids__c.split(';');
                    Set<String> adAllocationIdsSplittedSet = new Set<String>(adAllocationIdsSplitted);
                    
                    if(!quoteToQLines.isEmpty()){
                        quoteLinesToCheck = quoteToQLines.get(currentQuote.Id);
                        
                        for(SBQQ__QuoteLine__c currentQL : quoteLinesToCheck){
                            if(currentQL.SBQQ__Quote__r.SBQQ__ExpirationDate__c > Date.today()){
                                allocationIdToMantain.add(currentQL.Ad_Context_Allocation__c);
                            }
                        }
                    }
                    adAllocationIdsSplittedSet.removeAll(allocationIdToMantain);
                    if(!adAllocationIdsSplittedSet.isEmpty()){
                        Integer count = 0;
                        for(String currentIdToDeallocate : adAllocationIdsSplittedSet){
                            if(count < 10){
                                if(String.isNotBlank(currentIdToDeallocate)){
                                    Map<String,String> parameters = new Map<String,String>();
                                    parameters.put('ad_context_allocation', currentIdToDeallocate);
                                    String correlationId = SRV_DeleteAllocationCallout.generateCorrelationId(currentIdToDeallocate);
                                    SRV_DeleteAllocationCallout.DeleteAllocationBatchResponse deleteAllocationBatchResponse = SRV_DeleteAllocationCallout.deleteAllocationCallout(parameters, correlationId, currentQuote.Id);
                                    if(deleteAllocationBatchResponse != null){
                                        if(deleteAllocationBatchResponse.isSuccess) adAllocationIdsSplittedSet.remove(currentIdToDeallocate);
                                        logsToInsert.add(deleteAllocationBatchResponse.integrationTask);
                                    }
                                    count++;
                                }
                            }
                            else break;
                        }
                        if(!adAllocationIdsSplittedSet.isEmpty()){
                            String newAdAllocationIds = String.join( new List<String>(adAllocationIdsSplittedSet), ';' );
                            if(!allocationIdToMantain.isEmpty()){
                                String allocationIdtoMantainString = String.join( new List<String>(allocationIdToMantain), ';' );
                                newAdAllocationIds = newAdAllocationIds+';'+allocationIdtoMantainString;
                            }
                            currentQuote.Ad_Allocation_Ids__c = newAdAllocationIds;
                            currentQuote.Allocation_not_empty__c = true;
                        }
                        else{
                            String newAdAllocationIds = '';
                            if(!allocationIdToMantain.isEmpty()){
                                String allocationIdtoMantainString = String.join( new List<String>(allocationIdToMantain), ';' );
                                newAdAllocationIds = allocationIdtoMantainString; 
                            }
                            currentQuote.Ad_Allocation_Ids__c = newAdAllocationIds;
                            if(String.isBlank(newAdAllocationIds)) currentQuote.Allocation_not_empty__c = false;
                            else currentQuote.Allocation_not_empty__c = true;
                        }
                        quotesToUpdate.add(currentQuote);
                    }
                }
            }
            if(!logsToInsert.isEmpty()) insert logsToInsert;
            if(!quotesToUpdate.isEmpty()){
                QuoteTriggerHandler.disableTrigger = true;
                update quotesToUpdate;
                QuoteTriggerHandler.disableTrigger = false;
            }
            
        }
        
    }     
    global void finish(Database.BatchableContext info){     
    }
}