/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* When Adv. Online subscriptions are terminated the flag Tech_Delete_Allocation__c is set to true.
* This batch get all the subscriptions having Tech_Delete_Allocation__c to true and make a callout
* to delete the allocations which are over before the expiration date.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-10-09
* @systemLayer    Service
* @testClass	  Test_DeleteAllocationOnTerminatedSubs
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @modifiedby    Mara Mazzarella
 * 2020-11-09     SPIII-3794 Context products-Subscription & order relationship with de-allocation
 *				  in CASA - deleteAllocation when Termination order is fulfilled.
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class DeleteAllocationOnTerminatedSubscription implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts {
    
    global void execute(SchedulableContext sc) {
        DeleteAllocationOnTerminatedSubscription b = new DeleteAllocationOnTerminatedSubscription(); 
        database.executebatch(b,1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator('SELECT Id, Ad_Context_Allocation__c,SBQQ__Contract__r.Status, SBQQ__Contract__r.In_Cancellation__c,SBQQ__Product__r.ExternalKey_ProductCode__c,'+
                                        ' CategoryID__c, Tech_Delete_Allocation__c, SBQQ__Contract__c,SBQQ__StartDate__c,'+
                                        ' SBQQ__Product__r.ProductCode, Language__c,SBQQ__TerminatedDate__c, LocationID__c'+
                                        ' FROM SBQQ__Subscription__c WHERE Tech_Delete_Allocation__c = true');
    }
    
    global void execute(Database.BatchableContext info, List<SBQQ__Subscription__c> scope){ 
        Set<String> prodCodes = new Set<String>();
        List<SBQQ__Subscription__c> scopeToTerminate = new List<SBQQ__Subscription__c>();
        List<Log__c> logsToInsert = new List<Log__c>();
        Map<Id, SBQQ__Subscription__c> subscriptionsToUpdate = new Map<Id, SBQQ__Subscription__c>();  
        String baseCatgory = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_CATEGORY);
        String baseLocation = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_LOCATION);
        String allocationId = '';
        String recordId;
        Map<String,CASA_Product_Config__mdt> prodCodeToCasaConfig = new Map<String,CASA_Product_Config__mdt>();
        if(!scope.isEmpty()){
            Map<String,String> languageMap = new Map<String,String>{'German' => 'de',
                                                                    'French' => 'fr',
                                                                    'Italian' => 'it', 
                                                                    'English' => 'en'
                                                                    };
            for(SBQQ__Subscription__c sub : scope){
                prodCodes.add(sub.SBQQ__Product__r.ExternalKey_ProductCode__c);
            }
            List<CASA_Product_Config__mdt> productConfiguration = [ SELECT  Product_Code__c, 
                                                                           Slot__c, 
                                                                           Language__c, 
                                                                           Low_Price__c,
                                                                           Medium_Price__c,
                                                                           High_Price__c
                                                                           FROM    CASA_Product_Config__mdt
                                                                           WHERE   Product_Code__c IN :prodCodes];
            for(CASA_Product_Config__mdt currentMdt : productConfiguration){
                prodCodeToCasaConfig.put(currentMdt.Product_Code__c,currentMdt);
            }
            if(!prodCodeToCasaConfig.isEmpty()){
                baseCatgory = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_CATEGORY);
        		baseLocation = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_LOCATION);
            }

            for(SBQQ__Subscription__c currentSubscription : scope){
                if(currentSubscription.SBQQ__Contract__r.In_Cancellation__c == true || currentSubscription.SBQQ__Contract__r.Status == ConstantsUtil.CONTRACT_STATUS_CANCELLED ){                      
                    Map<String,String> parameters = new Map<String,String>();
                    parameters.put('ad_context_allocation', currentSubscription.Ad_Context_Allocation__c);
                    String correlationId = SRV_DeleteAllocationCallout.generateCorrelationId(currentSubscription.Id);
                    SRV_DeleteAllocationCallout.DeleteAllocationBatchResponse deleteAllocationBatchResponse = SRV_DeleteAllocationCallout.deleteAllocationCallout(parameters, correlationId, currentSubscription.SBQQ__Contract__c);
                    if(deleteAllocationBatchResponse != null && deleteAllocationBatchResponse.isSuccess){
                        currentSubscription.Tech_Delete_Allocation__c = false;
                        subscriptionsToUpdate.put(currentSubscription.Id, currentSubscription);
                        logsToInsert.add(deleteAllocationBatchResponse.integrationTask);
                    }
                }
                else{
                    Map<String, String> parameters = new Map<String,String>();
                    allocationId = currentSubscription.Ad_Context_Allocation__c;
                    recordId = currentSubscription.SBQQ__Contract__c;
                    if(String.isNotBlank(currentSubscription.CategoryID__c)) parameters.put('category', baseCatgory+currentSubscription.CategoryID__c);
                    if(String.isNotBlank(currentSubscription.LocationID__c) && !currentSubscription.LocationID__c.equals('all')) parameters.put('region', baseLocation+currentSubscription.LocationID__c);
                    parameters.put('start_date', SRV_LEnhancedProdConfigController.formatDate(currentSubscription.SBQQ__StartDate__c));
                    parameters.put('slot', prodCodeToCasaConfig.get(currentSubscription.SBQQ__Product__r.ExternalKey_ProductCode__c).Slot__c);
                    parameters.put('product_code', currentSubscription.SBQQ__Product__r.ProductCode);                    
                    if(String.isNotBlank(currentSubscription.Language__c)) parameters.put('language', languageMap.get(currentSubscription.Language__c)); 
                    parameters.put('end_date', SRV_LEnhancedProdConfigController.formatDate(currentSubscription.SBQQ__TerminatedDate__c));
                    parameters.put('expiration_date', SRV_LEnhancedProdConfigController.formatDate(currentSubscription.SBQQ__TerminatedDate__c)); 
                    SRV_EnhancedExtProdConfiguration.CasaAdContextResponse casaResponse = LEnhancedProdConfigController.adContextUpdate(recordId, allocationId, parameters, false);
                    if(casaResponse!= null){
                        currentSubscription.Tech_Delete_Allocation__c = false;
                        subscriptionsToUpdate.put(currentSubscription.Id, currentSubscription);
                    }
                }
            }
            if(!logsToInsert.isEmpty()) insert logsToInsert;
            if(!subscriptionsToUpdate.isEmpty()){
                SubscriptionTriggerHandler.disableTrigger = true;
                update subscriptionsToUpdate.values();
                SubscriptionTriggerHandler.disableTrigger = false;
            }
        }
    }    
    global void finish(Database.BatchableContext info){     
    }
}