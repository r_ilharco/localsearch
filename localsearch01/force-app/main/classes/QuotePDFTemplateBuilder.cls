/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 07-16-2019
 * Sprint      : Sprint 5
 * Work item   : SF2-239 Quote PDF creation
 * Testclass   : Test_QuotePDFTemplateBuilder
 * Package     : 
 * Description : Script for the generation of a Quote PDF Document
 * Changelog   : 
 */

public class QuotePDFTemplateBuilder {
    public static String nameTest = 'TemplateTest';
    
	public static void start()
    {
        start(QuotePDFTemplateBuilder.nameTest);
    }
	    
    public static void start(String templateName)
    {
        String refIdLogo = '0155E000000WMUd';
        CompanyInfo info = new CompanyInfo();
        info.name = 'Company Name';
        info.slogan = 'Company Slogan';
        info.phone = 'Company Phone';
        info.fax = 'Company Fax';
        info.email = 'company@email.com';
        info.street = 'Company Street';
        info.city = 'Company City';
        info.state = 'Company State';
        info.postalCode = 'Company Postal Code';
        info.country = 'Company Country';
                
        List<SBQQ__LineColumn__c> lineColumns = new List<SBQQ__LineColumn__c>();
        
        SBQQ__LineColumn__c lineCol = new SBQQ__LineColumn__c();
        lineCol.Name = 'QTY';
        //lineCol.SBQQ__Template__c = 'a0s5E000001vO8DQAU';
        lineCol.SBQQ__DisplayOrder__c = 10;
        lineCol.SBQQ__FieldName__c = 'SBQQ__Quantity__c';
        lineCol.SBQQ__Alignment__c = 'Left';
        lineCol.SBQQ__Width__c = 5;
        
        lineColumns.add(lineCol);
        
        SBQQ__lineColumn__c lineCol2 = new SBQQ__lineColumn__c();
        lineCol2.Name = 'PART #';
        lineCol2.SBQQ__DisplayOrder__c = 20;
        lineCol2.SBQQ__FieldName__c = 'SBQQ__ProductCode__c';
        lineCol2.SBQQ__Alignment__c = 'Left';
        lineCol2.SBQQ__Width__c = 10;
        
        lineColumns.add(lineCol2);
        
        SBQQ__lineColumn__c lineCol3 = new SBQQ__lineColumn__c();
        lineCol3.Name = 'DESCRIPTION';
        lineCol3.SBQQ__DisplayOrder__c = 30;
        lineCol3.SBQQ__FieldName__c = 'SBQQ__Description__c';
        lineCol3.SBQQ__Alignment__c = 'Left';
        lineCol3.SBQQ__Width__c = 38;
        
        lineColumns.add(lineCol3);
        
        SBQQ__lineColumn__c lineCol4 = new SBQQ__lineColumn__c();
        lineCol4.Name = 'UNIT PRICE';
        lineCol4.SBQQ__DisplayOrder__c = 40;
        lineCol4.SBQQ__FieldName__c = 'SBQQ__ListPrice__c';
        lineCol4.SBQQ__Alignment__c = 'Right';
        lineCol4.SBQQ__Width__c = 10;
        
        lineColumns.add(lineCol4);
        SBQQ__lineColumn__c lineCol5 = new SBQQ__lineColumn__c();
        lineCol5.Name = 'DISC (%)';
        lineCol5.SBQQ__DisplayOrder__c = 50;
        lineCol5.SBQQ__FieldName__c = 'SBQQ__Discount__c';
        lineCol5.SBQQ__Alignment__c = 'Center';
        lineCol5.SBQQ__Width__c = 10;
        
        lineColumns.add(lineCol5);
        
        SBQQ__lineColumn__c lineCol6 = new SBQQ__lineColumn__c();
        lineCol6.Name = 'SUBSCRIPTION TERM';
        lineCol6.SBQQ__DisplayOrder__c = 60;
        lineCol6.SBQQ__FieldName__c = 'Subscription_Term__c';
        lineCol6.SBQQ__Alignment__c = 'Center';
        lineCol6.SBQQ__Width__c = 12;
        
        lineColumns.add(lineCol6);
        
        SBQQ__lineColumn__c lineCol7 = new SBQQ__lineColumn__c();
        lineCol7.Name = 'TOTAL';
        lineCol7.SBQQ__DisplayOrder__c = 70;
        lineCol7.SBQQ__FieldName__c = 'Total__c';
        lineCol7.SBQQ__Alignment__c = 'Right';
        lineCol7.SBQQ__Width__c = 15;
        
        lineColumns.add(lineCol7);
        
        
        SBQQ__QuoteTemplate__c quoteTemplate = createQuotePDFTemplate(templateName, lineColumns, info, refIdLogo);
        quoteTemplate.SBQQ__TotalField__c = 'Total__c';
        
        update quoteTemplate;
    }
    
	public static SBQQ__QuoteTemplate__c createQuotePDFTemplate(String name, List<SBQQ__LineColumn__c> lineColumns, CompanyInfo info, String refIdLogo)
    {
        SBQQ__QuoteTemplate__c quoteTemplate = new SBQQ__QuoteTemplate__c();
        
        if(name.equals(QuotePDFTemplateBuilder.nameTest))
        {
            Integer counter = Database.countQuery('SELECT COUNT() FROM SBQQ__QuoteTemplate__c WHERE Name LIKE \'' + QuotePDFTemplateBuilder.nameTest + '%\'');
            name = QuotePDFTemplateBuilder.nameTest + counter;
        }
            
        quoteTemplate.Name = name;
        quoteTemplate.SBQQ__CompanyCity__c = info.city;
        quoteTemplate.SBQQ__CompanyCountry__c = info.country;
        quoteTemplate.SBQQ__CompanyEmail__c = info.email;
        quoteTemplate.SBQQ__CompanyFax__c = info.fax;
        quoteTemplate.SBQQ__CompanyName__c = info.name;
        quoteTemplate.SBQQ__CompanyPhone__c = info.phone;
        quoteTemplate.SBQQ__CompanyPostalCode__c = info.postalCode;
        quoteTemplate.SBQQ__CompanySlogan__c = info.slogan;
        quoteTemplate.SBQQ__CompanyState__c = info.state;
        quoteTemplate.SBQQ__CompanyStreet__c = info.street;
        
        if(!String.isEmpty(refIdLogo) && Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(refIdLogo).matches())
        {
            if(Database.countQuery('SELECT COUNT() FROM Document WHERE Id = \'' + refIdLogo + '\'') == 1)
            {
                quoteTemplate.SBQQ__LogoDocumentId__c = refIdLogo;
            }
        }
        
        quoteTemplate.SBQQ__DeploymentStatus__c = 'Deployed';
        
        insert quoteTemplate;
        
        delete [SELECT Id FROM SBQQ__LineColumn__c WHERE SBQQ__Template__c = :quoteTemplate.Id];
        
        for(SBQQ__LineColumn__c lineCol : lineColumns)
        {
            lineCol.SBQQ__Template__c = quoteTemplate.Id;
        }
        
        insert lineColumns;
        
        return quoteTemplate;
    }
    
    public class CompanyInfo{
        public String name;
        public String slogan;
        public String phone;
        public String fax;
        public String email;
        public String street;
        public String city;
        public String state;
        public String postalCode;
        public String country;
    }
}