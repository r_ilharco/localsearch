/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date		   : 05-29-2019
 * Sprint      : Sprint 2
 * Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
 * 				 US_75 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (downgrade) to replace a product with another one
 * Testclass   :
 * Package     : 
 * Description : general JSONWriter 
 * Changelog   : 
 */

public with sharing class JSONWriterObjects {
    public static string getJSONbyHierarchyMap(Map<SObject, List<SObject>> hierarchyMap, Map<String,String> keyPairs)
    {
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartArray();
        
        for(SObject ql : hierarchyMap.keySet())
        {
            printObjectToJSON(gen, ql, keyPairs, hierarchyMap.get(ql));
        }

        gen.writeEndArray();

        return gen.getAsString();
    }

    private static void printObjectToJSON(JSONGenerator gen, SObject ql, Map<String,String> keyPairs, List<SObject> children)
    {
        gen.writeStartObject();
        gen.writeStringField('name', String.valueOf(ql.get('Name')));
        printObject(gen, ql, KeyPairs);

        if(children!=null)
        {
            String keyvalue = '_children';
            gen.writeFieldName(keyvalue);
            gen.writeStartArray();
            
            for(SObject qlchild : children)
            {
                gen.writeStartObject();
                gen.writeStringField('name', String.valueOf(qlchild.get('Name')));
                printObject(gen, qlchild, KeyPairs);
                gen.writeEndObject();
            }

            gen.writeEndArray();

        }

        gen.writeEndObject();
    }
    
    

    private static void printObject(JSONGenerator gen, SObject ql, Map<String, String> KeyPairs)
    {
        for(String key : keyPairs.keySet())
        {
            gen.writeStringField(String.valueOf(keyPairs.get(key)), (ql.get(key)==null)?'':String.valueOf(ql.get(key)));
        }
    }
}