//CHANGELOG: 
//    #1: SF2-264 - Legal Address Validation - gcasola - 07-19-2019 - Address_Validated changed Type from Checkbox to Picklist
//    #2: SF2-253 - Validate Account and Billing Address on Quote Creation
//                  SF2-264 - Legal Address Validation
//                  gcasola 07-22-2019
@IsTest
public class BillingProfileTriggerHelper_Test {
    
    @TestSetup
    public static void makeData () {
         List<Account> accounts = Test_DataFactory.createAccounts('testAccount', 2);
        insert accounts;
        List<Account> accountsInsert = Test_DataFactory.createAccounts('testAccountInsert', 2);
        
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0].id, 1);
        insert contacts;
        
        List<Contact> contactsInsert = Test_DataFactory.createContacts(accounts[1].id, 1);
        insert contactsInsert;
        }
    
    
    
        @isTest
   		 public static void testAddressValidationOk () {
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        
        List<Account> accounts = [SELECT Id, UID__c FROM Account WHERE Name = 'testAccount 0' LIMIT 1];
        System.debug('Accounts ' + accounts);
        List<Contact> contacts = [SELECT Id FROM Contact WHERE LastName = 'LastName' LIMIT 1];
        System.debug('COntacts ' + contacts);
        List<Billing_Profile__c> billingProfiles = Test_DataFactory.createBillingProfiles(accounts, contacts, 1);
       /*      for(Billing_Profile__c p : billingProfiles) {
                 p.Name='NewName';
             } */
        System.debug('Billing Profiles ' + billingProfiles);
        
        test.startTest();
        insert billingProfiles;
             
        test.stopTest();
        
        
        billingProfiles = [SELECT Id, AddressValidated__c FROM Billing_Profile__c WHERE Billing_City__c = 'test billing city' LIMIT 1];
             if (billingProfiles.size()>0)
             {System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED, billingProfiles[0].AddressValidated__c);
             }
  
    }
    
    
       
        @isTest
   		 public static void testAddressValidationUpdateOk () {
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK));
        List<Account> accounts = [SELECT Id, UID__c FROM Account WHERE Name = 'testAccount 0' LIMIT 1];
        System.debug('Accounts ' + accounts);
        List<Contact> contacts = [SELECT Id FROM Contact WHERE LastName = 'LastName' LIMIT 1];
        System.debug('COntacts ' + contacts);   
        List<Billing_Profile__c> profiles = Test_DataFactory.createBillingProfiles(accounts, contacts, 1);
        insert profiles;
      
        List<Billing_Profile__c> billingProfiles = [SELECT Id, AddressValidated__c, Billing_City__c FROM Billing_Profile__c WHERE Billing_City__c = 'test billing city'  LIMIT 1];
          for(Billing_Profile__c p : billingProfiles) {
                 p.Billing_City__c='NewCity';
             } 
        System.debug('Billing Profiles ' + billingProfiles);
        
        test.startTest();
        update billingProfiles;
        test.stopTest();
        
        
       List<Billing_Profile__c> updatedBillingProfiles = [SELECT Id, AddressValidated__c ,  Billing_City__c FROM Billing_Profile__c WHERE Billing_City__c = 'NewCity'];
             if(updatedBillingProfiles.size()>0){
          System.assertEquals(ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED, updatedbillingProfiles[0].AddressValidated__c);
             }
    } 
    
    


}