/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller for Aura Component "RequestAccountOwnership". 
*
* Contains methods to send Approval Request to requester's SMA.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         	Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        	2020-06-30
* @systemLayer    	Invocation
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        	New field Tech_Requested_Approver__c (SPIII 2048)
* @modifiedby     	Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-23  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        	SPIII-2030 - DMC can require ownership of an accont or the possibility to sell 
*					to an Account he does not own
* @modifiedby     	Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-07-27
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-3656 - SMD should be able (as for DMCs) to request the ownership of an Account
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2021-02-03
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class RequestAccountOwnershipController {
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * The button "Make a Request" is only available for DMC users and if there is any approval process
    * pending on the same record.
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @return	errorsToReturn		Errors class properties are set to true if a specific error occurs
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @AuraEnabled
    public static Errors validateUserAndRecord(String accountId){
        
        Errors errorsToReturn = new Errors();
        
        List<User> currentUser = [SELECT Id, ManagerId, Code__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(!ConstantsUtil.DMC_CODE_USERCONFIGURATION_MDT.equalsIgnoreCase(currentUser[0].Code__c) && !ConstantsUtil.SMD_CODE_USERCONFIGURATION_MDT.equalsIgnoreCase(currentUser[0].Code__c)) errorsToReturn.noDMC = true;
        else if(Approval.isLocked(accountId)) errorsToReturn.inApproval = true;
        
        return errorsToReturn;
    }
    
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Set field Submitter on account with the requester user's Id. The field update triggers "Account
    * Change Owner Management" Process Builder to send Approval Request to requester user's SMA, SMD 
    * or RDI (based on territory hierarchy on Account).
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	accountId   Account Id for which ownership is requested
    * @return   	    Errors		Errors class properties are set to true if a specific error occurs
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @AuraEnabled
    public static Errors requestAccountOwnership(String accountId){
        
        Errors errorsToReturn = new Errors();
        
        List<Account> account = [SELECT Id,
                                 		OwnerId,
                                 		Tech_Requested_Approver__c,
                                 		RegionalLeader__c,
                                 		Sales_manager_substitute__c,
                                 		RegionalDirector__c
                                 FROM 	Account
                                 WHERE 	Id = :accountId LIMIT 1];
        
        if(!account.isEmpty()){
            if(account[0].OwnerId.equals(UserInfo.getUserId())) errorsToReturn.isSameOwner = true;
            else{
                
                Id requestedApprover = getRequestedApproverId(account[0].RegionalLeader__c, account[0].Sales_manager_substitute__c, account[0].RegionalDirector__c);
                
                if(String.isNotBlank(requestedApprover)){
                    
                    account[0].Tech_Submitter__c = UserInfo.getUserId();
                	account[0].Tech_Ownership_Requested__c = true;
                    account[0].Tech_Requested_Approver__c = requestedApprover;
                    account[0].OldApproverId__c = requestedApprover;
                    
                    update account;
                    
                }
                
                else errorsToReturn.noTerritory = true;   
                
            }
        }
        
        else errorsToReturn.genericError = true;
        return errorsToReturn;
        
    }
    
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Set field Submitter on account with the requester user's Id. The field update triggers "Account
    * Change Owner Management" Process Builder to send Approval Request to requester user's SMA, SMD 
    * or RDI (based on territory hierarchy on Account).
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @input param    	accountId   Account Id for which ownership is requested
    * @return   	    Errors		Errors class properties are set to true if a specific error occurs
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @AuraEnabled
    public static Errors requestAccountTeamMembership(String accountId){
        
        Errors errorsToReturn = new Errors();
        
        List<Account> account = [SELECT Id,
                                 		Tech_Requested_Approver__c,
                                 		RegionalLeader__c,
                                 		Sales_manager_substitute__c,
                                 		RegionalDirector__c
                                 FROM 	Account
                                 WHERE 	Id = :accountId LIMIT 1];
        
        if(!account.isEmpty()){
            
            Id requestedApprover = getRequestedApproverId(account[0].RegionalLeader__c, account[0].Sales_manager_substitute__c, account[0].RegionalDirector__c);
            
            if(String.isNotBlank(requestedApprover)){

                account[0].Tech_Submitter__c = UserInfo.getUserId();
                account[0].Tech_Sell_Requested__c = true;
                account[0].Tech_Requested_Approver__c = requestedApprover;
                
                update account;
                
            }
            
            else errorsToReturn.noTerritory = true;   
            
        }
        
        else errorsToReturn.genericError = true;
        return errorsToReturn;
        
    }
    
    public static Id getRequestedApproverId(Id sma, Id smd, Id sdi){
        
        Map<Id, Boolean> userActive = new Map<Id, Boolean>();
        Set<Id> userOutOfOfficeIds = new Set<Id>();
        
        Set<Id> userIds = new Set<Id>();
        if(String.isNotBlank(sma)) userIds.add(sma); 
        if(String.isNotBlank(smd)) userIds.add(smd); 
        if(String.isNotBlank(sdi)) userIds.add(sdi);
        
        if(!userIds.isEmpty()){
            List<User> users = [SELECT Id, IsActive FROM User WHERE Id IN :userIds];
            for(User currentUser : users) userActive.put(currentUser.Id, currentUser.IsActive);
            
            List<OutOfOffice> usersOutOfOffice = [SELECT UserId FROM OutOfOffice WHERE UserId IN :userIds];
            if(!usersOutOfOffice.isEmpty()){
                for(OutOfOffice userOutOfOffice : usersOutOfOffice) userOutOfOfficeIds.add(userOutOfOffice.UserId);
            }
            
            if(String.isNotBlank(sma) 
               && userActive.get(sma) 
               && !userOutOfOfficeIds.contains(sma)){
                   return sma;
               } 
            else if(String.isNotBlank(smd) && userActive.get(smd) && !userOutOfOfficeIds.contains(smd)) return smd;
        	else if(String.isNotBlank(sdi) && userActive.get(sdi) && !userOutOfOfficeIds.contains(sdi)) return sdi;
            else return null;
        }
        else return null;
    }
    
    public class Errors {
        @AuraEnabled
        public Boolean isSameOwner {get;set;}
        @AuraEnabled
        public Boolean noTerritory {get;set;}
        @AuraEnabled
        public Boolean noDMC {get;set;}
        @AuraEnabled
        public Boolean inApproval {get;set;}
        @AuraEnabled
        public Boolean genericError {get;set;}
        
        public Errors(){
            isSameOwner = false;
            noTerritory = false;
            noDMC = false;
            inApproval = false;
            genericError = false;
        }
    }
}