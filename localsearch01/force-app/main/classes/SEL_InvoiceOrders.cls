/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date		   : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : This class contains some useful methods to retrieve InvoiceOrders
 * Changelog   : 
 */

public class SEL_InvoiceOrders {
    public static List<Invoice_Order__c> getInvoiceOrders(List<Invoice__c> invoices){
		return [SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
                    (SELECT Accrual_From__c, Accrual_To__c, Amount__c, Tax__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
                        Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c, Credit_Account__c,
                        KSTCode__c, KTRCode__c, Product__r.ProductCode, Product__r.Name, Product__c, Subscription__c, Credit_Note__r.Reimbursed_Invoice__r.Name,
                        Credit_Note__r.Reimbursed_Invoice__c, Subscription__r.SBQQ__StartDate__c, Credit_Note__c 
                    FROM Invoice_Items__r 
                    ORDER BY Line_Number__c
                    ),Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
                FROM Invoice_Order__c
                WHERE Invoice__c in :invoices
                ORDER BY Item_Row_Number__c
                ];
    }
    
    public static List<AggregateResult> countInvoices (Set<Id> invoiceIds) {
		return new List<AggregateResult> ([
            SELECT count(Contract__c)number, Invoice__c FROM Invoice_Order__c GROUP BY Invoice__c HAVING Invoice__c IN : invoiceIds
        ]);     
    }
    
    public static List<Invoice_Order__c> getInvoiceOrderbyInvoicebyStatus(List<String> status, Set<Id> contractIds){
        return [SELECT Invoice__r.Id, Invoice__r.Correlation_Id__c, Invoice__r.Customer__r.Customer_Number__c,
            Invoice__r.Customer__r.Name, Invoice__r.Name, Invoice__r.Status__c
            FROM Invoice_Order__c
        	WHERE Contract__c in: contractIds 
            and Invoice__r.Status__c not in : status
            and Invoice__r.Invoice_Code__c =: ConstantsUtil.BILLING_DOC_CODE_FAK
            and Invoice__r.Process_Mode__c =: ConstantsUtil.INVOICE_PROCESS_MODE_SWISSLISTMIG
            ORDER BY Period_From__c DESC, Invoice__r.Invoice_Date__c DESC LIMIT 1];
    }

}