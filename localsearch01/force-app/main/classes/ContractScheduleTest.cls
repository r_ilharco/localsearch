@isTest
public class ContractScheduleTest {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    
    @isTest
    public static void testExecute(){
        try{      
            insertBypassFlowNames();
            
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';        
            insert place;
            System.debug('Place: '+ place);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh'; 
            acc.BillingCity = 'test city';
            acc.BillingCountry = 'test country';
            acc.BillingPostalCode = '123';
            acc.BillingState = 'test state';
            acc.PreferredLanguage__c = 'German';       
            insert acc;
            System.Debug('Account: '+ acc); 
            
            place.Account__c = acc.Id;
            update place;
            System.debug('Place update: '+ place);
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';        
            insert myContact;
            System.Debug('Contact: '+ myContact);  
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';        
            insert opp;
            System.Debug('Opportunity '+ opp); 
            
            List<SBQQ__Quote__c> quoteList = [select Id, Name from SBQQ__Quote__c where SBQQ__Opportunity2__c =: opp.id];
            if(quoteList.size() > 0){
                for(SBQQ__Quote__c quoteHere : quoteList){
                    quoteHere.SBQQ__Primary__c = false; 
                }
                update quoteList;
                try{
                    delete quoteList;
                }catch(Exception e){         
                   System.Assert(true, e.getMessage());
                } 
            }
            
            Billing_Profile__c billProfile = new Billing_Profile__c();          
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'print';
            billProfile.Name = 'Test Bill Prof Name';
            insert billProfile;
            System.Debug('Bill Profile: '+ billProfile);
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__PrimaryContact__c = myContact.Id;
            quot.Billing_Profile__c = billProfile.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            quot.SBQQ__Primary__c = true;   
            insert quot;
            System.Debug('Quote nn : '+ quot);
            
            Contract myContract = new Contract();
            myContract.AccountId = acc.Id;
            myContract.CustomerSignedId = myContact.Id;
            myContract.SBQQ__Opportunity__c= opp.Id;
            myContract.Status = 'Draft';
            myContract.StartDate = Date.today();
            myContract.TerminateDate__c = Date.today();
            
            insert myContract;
            System.debug('Contract: '+ myContract);  
            
            Product2 prod = new Product2();
            prod.Name = 'test product';
            prod.ProductCode = 'test';
            prod.IsActive=True;
            prod.SBQQ__ConfigurationType__c='Allowed';
            prod.SBQQ__ConfigurationEvent__c='Always';
            prod.SBQQ__QuantityEditable__c=True;       
            prod.SBQQ__NonDiscountable__c=False;
            prod.SBQQ__SubscriptionType__c='Renewable'; 
            prod.SBQQ__SubscriptionPricing__c='Fixed Price';
            prod.SBQQ__SubscriptionTerm__c=12; 
            insert prod;
            System.Debug('Product '+ prod); 
            
            SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
            quoteLine.SBQQ__Quote__c = quot.Id;
            quoteLine.SBQQ__Product__c = prod.Id;
            quoteLine.Place__c = place.Id;
            quoteLine.SBQQ__Quantity__c = 1;
            insert quoteLine;
            System.Debug('Quote Line '+ quoteLine);
            
            quot.SBQQ__Status__c = 'Accepted';
            update quot;
            System.Debug('Quote '+ quot);
            
            opp.SBQQ__AmendedContract__c = myContract.Id;
            opp.StageName = 'Closed Won';
            update opp;
            System.Debug('opp '+ opp); 
            
            Test.startTest(); 
            ContractSchedule schedule = new ContractSchedule();
            String scheduleTime = '0 0 23 * * ?'; 
            system.schedule('Test ContractSchedule', scheduleTime, schedule); 
            Test.stopTest();    
            
        }catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
    }


}