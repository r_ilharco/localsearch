public without sharing class OppTakeInChargeHandler {
    
    @AuraEnabled
    public static responseWrapper changeOwner(String recordId){
        
        responseWrapper changeOwnerResponse = new responseWrapper();

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        try{
            User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
            List<Opportunity> opp = [Select Id,StageName,Type,OwnerId,CloseDate,AccountId from Opportunity where Id = :recordId LIMIT 1];
            if(!opp.isEmpty()){
                if(opp[0].OwnerId == currentUser.Id){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorMessage = Label.RequestOpportunityOwnership_AlreadyOwner;
                    return changeOwnerResponse;
                }else if(currentUser.Profile.Name!= ConstantsUtil.SysAdmin && currentUser.Profile.Name != ConstantsUtil.TELESALESPROFILE_LABEL){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorMessage = Label.Automated_Opportunity_Profile_authorization;
                    return changeOwnerResponse;
                }else if(opp[0].Type != ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS || (opp[0].StageName != ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION && opp[0].StageName != ConstantsUtil.OPPORTUNITY_STAGE_VALIDATION)){
                    changeOwnerResponse.success = false;
                    changeOwnerResponse.errorMessage = Label.Automated_Opportunity_Type_Authorization;
                    return changeOwnerResponse;
                }else{
                        opp[0].OwnerId = currentUser.Id;
                    	update  opp[0];
                        quote.SBQQ__ExpirationDate__c  = opp[0].CloseDate;
                        quote.SBQQ__Opportunity2__c = opp[0].id;
                        quote.SBQQ__Account__c = opp[0].AccountId;
                        insert quote;
                    	changeOwnerResponse.success = true;  
                }
            }
        }
        catch(Exception e){
            system.debug('ex :: '+e.getMessage());
            changeOwnerResponse.success = false;
            changeOwnerResponse.errorMessage = Label.Generic_Error;
        }
        return changeOwnerResponse;
    }
    
    
    public class responseWrapper {
        @AuraEnabled
        public Boolean success {get;set;}
        @AuraEnabled
        public String errorMessage {get;set;}
        
    }
}