public with sharing class SwissbillingBulkifiedTransferJob implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    private Set<Id> skippedInvoices;
    private Set<Id> specificInvoices;
    private Boolean modeSpecificInvoices; 
    
    public SwissbillingBulkifiedTransferJob () {
    	this.skippedInvoices = new Set<id>();
        this.modeSpecificInvoices = false;
    }

    public SwissbillingBulkifiedTransferJob (Set<id> skippedInvoices) {
    	this.skippedInvoices = skippedInvoices;
        this.modeSpecificInvoices = false;
    }

    public SwissbillingBulkifiedTransferJob (Set<id> skippedInvoices, Set<id> specificInvoices) {
    	this.skippedInvoices = skippedInvoices;
        this.specificInvoices = specificInvoices;
        this.modeSpecificInvoices = true;
    }
    
    public List<sObject> start(Database.BatchableContext context) {
        if(!this.modeSpecificInvoices){
            // Select invoices and related data with invoice date < today & status 'In Collection'
            return [SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c, Customer__r.Email__c, 
                Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
                Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
                Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
                Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
                Billing_Profile__r.Customer_Reference_1__c, Billing_Profile__r.Customer_Reference_2__c, Billing_Profile__r.Customer_Reference_3__c, Billing_Profile__r.Recipient_Line_2__c,
                (SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
                FROM Invoice__c
                WHERE Status__c = :ConstantsUtil.INVOICE_STATUS_DRAFT AND Invoice_Date__c <= TODAY AND Total__c != 0 AND Id not in :skippedInvoices LIMIT 50000];    
        } else {
             return [SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c, Customer__r.Email__c, 
                Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
                Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
                Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
                Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
                Billing_Profile__r.Customer_Reference_1__c, Billing_Profile__r.Customer_Reference_2__c, Billing_Profile__r.Customer_Reference_3__c, Billing_Profile__r.Recipient_Line_2__c,
                (SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
                FROM Invoice__c
                WHERE Status__c = :ConstantsUtil.INVOICE_STATUS_DRAFT AND Invoice_Date__c <= TODAY AND Total__c != 0 /*AND CreatedDate = TODAY*/ AND Id not in :skippedInvoices AND Id in :specificInvoices LIMIT 50000];    
        }

    }
    
    public void execute(Database.BatchableContext context, List<sObject> data) {
        //vlaudato added in query Credit_Note__r.Manual__c,
        List<Invoice__c> invoicesInScope = (List<Invoice__c>) data;
        List<Invoice_Order__c> invoiceOrders = [
            SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
			(SELECT Accrual_From__c, Accrual_To__c, Amount__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
                Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c, Credit_Note__r.Contract__r.StartDate,
                Product__r.ProductCode, Product__c, Subscription__c, Subscription__r.Place__c, Subscription__r.Place__r.Name, Subscription__r.Place__r.PostalCode__c, Subscription__r.Place__r.City__c,
             	Credit_Note__c, Credit_Note__r.Reimbursed_Invoice__r.Name, Credit_Note__r.Reimbursed_Invoice__r.Invoice_Date__c, Credit_Note__r.Period_From__c, Credit_Note__r.Period_To__c,
             	Credit_Note__r.Execution_Date__c, Credit_Note__r.Nx_Refunded_Place__c, Credit_Note__r.Manual__c, Credit_Note__r.Automated__c, Subscription__r.RequiredBy__r.Subsctiption_Status__c,
             	Subscription__r.SBQQ__RequiredById__c, Subscription__r.SBQQ__Product__r.One_time_Fee__c, Subscription__r.RequiredBy__r.Total__c, Subscription__r.RequiredBy__r.Next_Invoice_Date__c,
             	Subscription__r.SBQQ__RequiredByProduct__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductName__c, Subscription__r.Total__c,
             	Subscription__r.SBQQ__QuoteLine__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c,
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__Description__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.Name, 
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.PostalCode__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.City__c,
             	Subscription__r.RequiredBy__r.SBQQ__Product__r.name, Subscription__r.RequiredBy__r.SBQQ__Product__r.ProductCode, Subscription__r.RequiredBy__r.Place__r.name,
             	Subscription__r.RequiredBy__r.Place__r.PostalCode__c, Subscription__r.RequiredBy__r.Place__r.City__c, Subscription__r.Category__c, Subscription__r.Location__c  
                FROM Invoice_Items__r
			),
            Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
            FROM Invoice_Order__c where Invoice__c in :invoicesInScope
            ORDER BY Item_Row_Number__c
        ];
        // Build a map for lower levels, instead of calling SOQL for each invoice (2 level subqueries aren't allowed in SOQL)
        Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = new Map<Id, List<Invoice_Order__c>>();
        
        for(Invoice_Order__c invoiceOrder : invoiceOrders) {
            List<Invoice_Order__c> invoiceOrderList = invoiceOrdersMap.get(invoiceOrder.Invoice__c);
            if(invoiceOrderList == null) {
                invoiceOrderList = new List<Invoice_Order__c>();
                invoiceOrdersMap.put(invoiceOrder.Invoice__c, invoiceOrderList);                
            }
            invoiceOrderList.Add(invoiceOrder);

        }
        
        Http httpClient = new Http();
        
        List<Invoice__c> invoiceUpdates = new List<Invoice__c>();
        //Map<Id, Account> updatedCustomers = new Map<Id, Account>();
        Map<Invoice__c,Boolean> generatedNumberInvoiceMap = new Map<Invoice__c,Boolean>();
        Invoice__c updatedInvoice;
        Boolean customerNumberChanged = false;
        List<Error_Log__c> errors = new List<Error_Log__c>();
        List<Map<String, Object>> invoices = new List<Map<String, Object>>();
        SwissbillingWrappers.invoicesToSend wrap = new SwissbillingWrappers.invoicesToSend();
        Boolean transactionOK = false;
        //SPIII-4293 START
        List<Credit_Note__c> creditNotesUpdates = new List<Credit_Note__c>();
        List<SBQQ__Subscription__c> subscriptionsUpdates = new List<SBQQ__Subscription__c>();
        List<Contract> contractsUpdates = new List<Contract>();
        Set<Id> subscriptionsSet = new Set<Id>();
        Set<Id> creditNotesSet = new Set<Id>();
        //SPIII-4293 END
        for(Invoice__c invoice : invoicesInScope) {
            if(!BillingHelper.checkMinTotalAllowed(invoice)) {
	            errors.add(ErrorHandler.createLogWarning('Billing', 'Billing.SwissbillingBulkifiedTransferJob', ErrorHandler.WarningCode.W_BILL_TOTAL_BELOW_LIMIT, 'Bill total below limit', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));
                skippedInvoices.add(invoice.Id);
                continue;
            }
            long documentNumber = 0;
            long customerNumber;
            long customerNumberTemp;
            boolean generatedNumber = false;
            try {
                customerNumberTemp = long.valueOf(invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''));
                customerNumber = long.valueOf(invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''));
            } catch(Exception ex) {
                customerNumber = null;
            }
            if(invoice.Name != ConstantsUtil.INVOICE_PENDING_NAME || Test.isRunningTest()) {
                try {
                	documentNumber = long.valueOf(invoice.Name.replaceAll('[^0-9]', ''));
                } catch(Exception ex) {
                    errors.add(ErrorHandler.createLogWarning('Billing', 'Billing.SwissbillingBulkifiedTransferJob', ErrorHandler.WarningCode.W_NOT_A_NUMBER, 'Invoice name can\'t be converted to a number', invoice.Name));
                }
            }
            if(documentNumber == 0) {
                documentNumber = BillingHelper.generateDocumentNumber();
                system.debug('documentNumber: ' + documentNumber);
                updatedInvoice = new Invoice__c(
                    Id = invoice.Id, 
                    Name = BillingHelper.formatDocNumber(documentNumber), 
                    Invoice_Number__c = documentNumber,
                    Payment_Reference__c = AbacusHelper.getESRValue(invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''), documentNumber.format().replaceAll('[^0-9]', '')),
                	Status__c = ConstantsUtil.INVOICE_STATUS_IN_COLLECTION
                );
                invoice.Payment_Reference__c = updatedInvoice.Payment_Reference__c;
                generatedNumber = true;
                generatedNumberInvoiceMap.put(updatedInvoice, generatedNumber);
                invoiceUpdates.add(updatedInvoice);
            }else{
                updatedInvoice = new Invoice__c(
                    Id = invoice.Id, Status__c = ConstantsUtil.INVOICE_STATUS_IN_COLLECTION
                );
                invoiceUpdates.add(updatedInvoice);
            }
            system.debug('updatedInvoice: '+ updatedInvoice);
            //Map<string, object> documentData = SwissbillingHelper.prepareInvoiceMap(invoice, invoiceOrdersMap, documentNumber);
            //wrap.invoices.add(documentData);
            //SPIII-4293 START
            for(List<Invoice_Order__c> invoiceOrdersList : invoiceOrdersMap.values()) {
                for(Invoice_Order__c invoiceOrder : invoiceOrdersList) {
                    for(Invoice_Item__c invoiceItem : invoiceOrder.Invoice_Items__r) {
                        if(invoiceItem.Credit_Note__c != null) {
                            if(!creditNotesSet.contains(invoiceItem.Credit_Note__c)) {
                                creditNotesUpdates.add(new Credit_Note__c(Id = invoiceItem.Credit_Note__c, Status__c = ConstantsUtil.CREDIT_NOTE_STATUS_BILLED));
                                creditNotesSet.add(invoiceItem.Credit_Note__c);
                            }
                        }else if(invoiceItem.Subscription__c  != null) {
                            String tempDate = invoiceItem.Period__c.substringAfter('-');
                            tempDate = tempDate.trim();
                            system.debug('tempDate: ' + tempDate);
                            Integer year = Integer.valueOf(tempDate.mid(6,4));
                            Integer month = Integer.valueOf(tempDate.mid(3,2));
                            Integer day = Integer.valueOf(tempDate.mid(0,2));
                            Date nextInvoiceDate = Date.newInstance(year, month, day);
                            system.debug('nextInvoiceDate: ' + nextInvoiceDate);
                            if(!subscriptionsSet.contains(invoiceItem.Subscription__c)) {
                                if(invoiceItem.Subscription__r.SBQQ__Product__r.One_time_Fee__c /*|| !invoiceItem.Subscription__r.SBQQ__Product__r.Deferred_Revenue__c*/){
                                    subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, One_time_Fee_Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
                                }else{
                                    subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
                                }
                                system.debug('subscriptionsUpdates: ' + subscriptionsUpdates);
                                subscriptionsSet.add(invoiceItem.Subscription__c);
                            }
                        }
                    }
                }
            }
            //SPIII-4293 END
        }
        //string documentJson = JSON.serialize(wrap, true);
        //System.debug('DOCUMENT JSON: '+ documentJson);
        //String correlationId = invoicesInScope[0].Correlation_Id__c;
        //system.debug('DML: ' + limits.getDmlStatements());
        //try {
            //HttpResponse res = SRV_Swissbilling.sendInvoices(documentJson, correlationId, ConstantsUtil.SWISSBILLING_TRANSFER_INVOICE);
            //integer statusCode = res.getStatusCode();
            //system.debug('statusCode: ' + statusCode);
            //errors.add(ErrorHandler.createLogInfo('Billing', 'Billing.SwissbillingBulkifiedTransferJob', 'JSON sent to Tibco', '', correlationId, null));
            //if(statusCode == 200) {
                //transactionOK = true;
                //errors.add(ErrorHandler.createLogInfo('Billing', 'Billing.SwissbillingBulkifiedTransferJob', 'List successfully sent to Tibco', null , correlationId, null));                  
            //} else {
                //for(Invoice__c currentInvoice : generatedNumberInvoiceMap.keySet()){
                    //if(generatedNumberInvoiceMap.get(currentInvoice)){
                        //BillingHelper.undoDocumentNumber();
                    //}
                //}
                //transactionOK= false;
                //throw new SwissbillingHelper.SwissbillingHelperException('Swissbilling Bulkified TransferJob ' + statusCode + '\n' + res.getBody() + '\n'+ '' , ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
            //}                                  
        //} catch (Exception ex) {
            //errors.add(ErrorHandler.createLogError('Billing', 'Billing.SwissbillingBulkifiedTransferJob', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, null, correlationId, null));
        //}
        try {
            //if(transactionOK || Test.isRunningTest()){
                update BillingHelper.numbering;
                if(invoiceUpdates.size() > 0) {
                    Database.update(invoiceUpdates, false);
                }
                if(subscriptionsUpdates.size() > 0) {
                    update subscriptionsUpdates;
                }
                if(creditNotesUpdates.size() > 0) {
                    Database.update(creditNotesUpdates, false);
                }
            //}
        } catch (Exception ex) {
            errors.add(ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', '                                                                                                                                                                                                                                                        ', ex, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false));
        } 
        try {
            if(errors.size() > 0) {
                insert errors;
            }
        } catch (Exception exEr) {
            system.debug(exEr);
            //errors.add(ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', 'Billing.SwissbillingBulkifiedTransferJob', exEr, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false));
        }
    }
    
    public void finish(Database.BatchableContext context) {
        
    }
}
