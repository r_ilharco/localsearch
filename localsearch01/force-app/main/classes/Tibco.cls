global class Tibco extends Auth.AuthProviderPluginClass {
    
    static final integer HTTP_TIMEOUT_MILLIS = Integer.valueOf(ConstantsUtil.HTTP_TIMEOUT_MILLIS);
    
    global String getCustomMetadataType() {
        return ConstantsUtil.TIBCO_METADATA_API_NAME;
    }
    
    global PageReference initiate(Map<string,string> authProviderConfiguration, String stateToPropagate) {
        string authUrl = authProviderConfiguration.get('Redirect_Url__c');
        if(authUrl == null || authUrl == '') {
            TibcoPluginException tpException = new TibcoPluginException('Redirect Url not defined in Auth. Provider', ErrorHandler.ErrorCode.E_NULL_VALUE);
            ErrorHandler.logError('Tibco', 'Tibco.AuthProviderPlugin', tpException);
            throw tpException;
        }
        return new PageReference(authUrl + '?state=' + stateToPropagate); // Urlencode not necessary
    }
    
    global Auth.AuthProviderTokenResponse handleCallback(Map<string,string> authProviderConfiguration, Auth.AuthProviderCallbackState state ) {
        String sfState = state.queryParameters.get('state');
      	TibcoToken token = getToken(authProviderConfiguration, sfState);
        return new Auth.AuthProviderTokenResponse('Tibco', token.access_token, 'refreshToken', sfState);
    }
    
    global override Auth.OAuthRefreshResult refresh(Map<String,String> authProviderConfiguration, String refreshToken) {
        TibcoToken token = getToken(authProviderConfiguration, refreshToken);
        return new Auth.OAuthRefreshResult(token.access_token, token.refresh_token);
    }
    
    global Auth.UserData  getUserInfo(Map<string,string> authProviderConfiguration, Auth.AuthProviderTokenResponse response) { 
        return new Auth.UserData('Tibco', null, 'Tibco', 'Tibco', null, null, 'Tibco', null, 'Tibco', null, null);
    }
    
    private TibcoToken getToken (Map<String,String> authProviderConfiguration, String refreshToken) {
        string clientId = authProviderConfiguration.get('Client_Id__c');
        string clientSecret = authProviderConfiguration.get('Client_Secret__c');
        string tokenEndpoint = authProviderConfiguration.get('Access_Token_Url__c');        
        Http httpClient = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(tokenEndpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', ConstantsUtil.OAUTH_REQUEST_CONTENT_TYPE);
        req.setTimeout(HTTP_TIMEOUT_MILLIS);
        string payload = string.format(ConstantsUtil.OAUTH_REQUEST_FORM, new string[] { 
                                           EncodingUtil.urlEncode(clientSecret, 'UTF-8'), 
                                           EncodingUtil.urlEncode(clientId, 'UTF-8')
                                       });
        req.setBody(payload);
        HttpResponse res = httpClient.send(req);
        if(res.getStatusCode()!=200) {
            TibcoPluginException tpException = new TibcoPluginException('Token request returned ' + res.getStatus(), ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
            ErrorHandler.logError('Tibco', 'Tibco.AuthProviderPlugin', tpException);
            throw tpException;
        }        
        return (TibcoToken) JSON.deserialize(res.getBody(), TibcoToken.class);
    }
    
    private class TibcoToken {
    	public string access_token;
    	public string token_type;
    	public long expires_in;
    	public string refresh_token;
    }
    
    public class TibcoPluginException extends ErrorHandler.GenericException {
        public TibcoPluginException(string message, ErrorHandler.ErrorCode errorCode) {
            super(message, errorCode);
        }       
    }      
}