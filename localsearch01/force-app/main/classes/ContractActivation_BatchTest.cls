@isTest
public class ContractActivation_BatchTest implements HttpCalloutMock {
     
    public HttpResponse respond(HTTPRequest req){
        String body = '{"status":"SUCCESS","source_element_ids":["a0m1X000000QuY5QAK"],"technical_fields":{"correlation_id":"2d3b380d4d523eb838210e7f9ed5605e"}}';
        HttpResponse resp = new HttpResponse();
        resp.setStatus('OK');
        resp.setStatusCode(Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS));
        resp.setBody(body);
        return resp;
    }

    @isTest 
    static void testContractActivationBatch(){
        
        List<Contract> listContract = new List<Contract>();
        
        ContractActivation_Batch obj = new ContractActivation_Batch();
        Test.setMock(HttpCalloutMock.class, new ContractActivation_BatchTest());
        test.startTest();
        //DataBase.executeBatch(obj);
        
        
        Database.BatchableContext bc;
        obj.execute(bc, listContract);
        obj.finish(bc);
        
        SchedulableContext sc;
        obj.execute(sc);

        test.stopTest();
    }
    
}