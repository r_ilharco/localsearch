@isTest
public class Test_CancelOrderCtrl {
    
	@testSetup
    public static void test_setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Community User Creation,Quote Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        //Products management
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        //Insert Account and Contact data
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;        

        //Insert Opportunity + Quote
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        
        String opportunityId = opportunity.Id;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id));
        insert fatherQuoteLine;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLine.Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        insert childrenQLs;
        
       	quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, childrenQLs, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = order.Id;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        //Generating termination order
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        insert terminationOrder;
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQuoteLine.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        List<OrderItem> childrenOrderItems = new List<OrderItem>();
        List<String> fieldNamesSubs = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet());
        fieldNamesSubs.add('SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c');
        String querySubs = 'SELECT ' +String.join( fieldNamesSubs, ',' ) +' FROM SBQQ__Subscription__c WHERE SBQQ__RequiredById__c != NULL';
        List<SBQQ__Subscription__c> childrenSubsQueried = Database.query(querySubs);
        for(SBQQ__Subscription__c sub : childrenSubsQueried){
            childrenOrderItems.add(generateChildrenOrderItem(sub,terminationOrder,fatherOrderItem.Id,sub.SBQQ__QuoteLine__r.SBQQ__PricebookEntryId__c));
        }
        insert childrenOrderItems;
        		        
        SBQQ.TriggerControl.enable();
        String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        
    }
    
    @isTest
    public static void test_checkOrderStatus(){
        Test.startTest();
        CancelOrderController.checkOrderStatus(ConstantsUtil.ORDER_STATUS_ACTIVATED);
        Test.stopTest();
    }
    
    @isTest
    public static void test_cancelOrder(){
        Order terminationOrder = [SELECT Id FROM Order WHERE Type = :ConstantsUtil.ORDER_TYPE_TERMINATION LIMIT 1];
        Test.startTest();
        CancelOrderController.cancelOrder(terminationOrder.Id);
        Test.stopTest();
    }
    
    public static Order generateOrder(Contract c, String orderType){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.Type = orderType;
        order.Custom_Type__c = orderType;
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null){
                c.Cancel_Date__c = Date.today();
                order.EffectiveDate = Date.today();
            }
        }
        else{
            
            if(c.TerminateDate__c == null){
                c.TerminateDate__c = Date.today();
                order.EffectiveDate = Date.today();
            }
            else{
                order.EffectiveDate = c.TerminateDate__c;
            }
            order.ContractId = c.id;
        }
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today();
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('FATHER OI ---> '+JSON.serializePretty(fatherOrderItem));
        return fatherOrderItem; 
    }
    public static OrderItem generateChildrenOrderItem(SBQQ__Subscription__c subscription, Order order, String requiredBy, String pbeId){
        OrderItem oi = new OrderItem();
        oi.ServiceDate = Date.today();
        oi.Quantity = subscription.SBQQ__Quantity__c;
        oi.UnitPrice = subscription.SBQQ__ListPrice__c;   
        oi.Product2Id = subscription.SBQQ__Product__c;
        oi.PricebookEntryId = pbeId;
        oi.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        oi.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c;
        oi.SBQQ__Subscription__c = subscription.Id;
        oi.OrderId = order.Id;
        oi.SBQQ__RequiredBy__c = requiredBy;  
        oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        System.debug('CHILD OI ---> '+JSON.serializePretty(oi));
        return oi;   
    }
}