/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 14-05-2019
 * Sprint      : 1
 * User Story  : 25
 * Testclass   : ContractInBoundApiTest
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class ContractApiWrapper {

    public class MyContract{
        String activated_date;
        String start_date;
        String end_date;
        String contract_number;
        String status;
        String terminate_date;
        List<MySubscription> subscriptions;

        public MyContract(){}

        public MyContract(Contract contr, List<MySubscription> subscriptions){
            this.activated_date = String.valueOf(contr.ActivatedDate);
            this.start_date = String.valueOf(contr.StartDate);
            this.end_date = String.valueOf(contr.EndDate);
            this.contract_number = contr.ContractNumber;
            this.status = contr.Status;
            this.terminate_date = String.valueOf(contr.TerminateDate__c);
            this.subscriptions = new List<MySubscription>();
            this.subscriptions = subscriptions;
        }

    }

    public class MySubscription{
        String billed;
        String bundled;
        String termination_date;
        String termination_reason;
        String end_date;
        String next_invoice_date;
        String place_name;
        String product_name;
        String quantity;
        String status;
        String billing_frequency;
        String net_price;

        public MySubscription(){}

        public MySubscription(SBQQ__Subscription__c subscription){
        this.billed = String.valueOf(subscription.Billed__c);
        this.bundled = String.valueOf(subscription.SBQQ__Bundled__c);
        this.termination_date = String.valueOf(subscription.Termination_Date__c);
        this.termination_reason = subscription.Termination_Reason__c;
        this.end_date = String.valueOf(subscription.SBQQ__EndDate__c);
        this.next_invoice_date = String.valueOf(subscription.Next_Invoice_Date__c);
        this.place_name = subscription.Place_Name__c;
        this.product_name = subscription.SBQQ__ProductName__c;
        this.quantity = String.valueOf(subscription.SBQQ__Quantity__c);
        this.status = subscription.Status__c;
        this.billing_frequency = subscription.SBQQ__BillingFrequency__c;
        this.net_price = String.valueOf(subscription.SBQQ__NetPrice__c);
        }
    }
}