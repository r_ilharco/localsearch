global class setCustomerNumberOnAccount_Schedulable implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(system.now().minute()+1);
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'setCustomerNumberOnMigratedAccount%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('setCustomerNumberOnMigratedAccount0')){
                setCustomerNumberOnMigratedAccount b0 = new setCustomerNumberOnMigratedAccount(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('setCustomerNumberOnMigratedAccount0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('setCustomerNumberOnMigratedAccount1')){ 
                setCustomerNumberOnMigratedAccount b1 = new setCustomerNumberOnMigratedAccount(new List<String>{'2','3','e','E','f','F','g','G','h','H','y','Y'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('setCustomerNumberOnMigratedAccount1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('setCustomerNumberOnMigratedAccount2')){
                setCustomerNumberOnMigratedAccount b2 = new setCustomerNumberOnMigratedAccount(new List<String>{'4','5','I','i','l','L','m','M','n','N','j','J'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('setCustomerNumberOnMigratedAccount2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('setCustomerNumberOnMigratedAccount3')){
                setCustomerNumberOnMigratedAccount b3 = new setCustomerNumberOnMigratedAccount(new List<String>{'6','7','o','O','p','P','q','Q','r','R','k','K'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('setCustomerNumberOnMigratedAccount3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('setCustomerNumberOnMigratedAccount4')){
                setCustomerNumberOnMigratedAccount b4 = new setCustomerNumberOnMigratedAccount(new List<String>{'8','9','s','S','t','T','u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('setCustomerNumberOnMigratedAccount4', strSchedule,b4);
            }
        }
    }
}