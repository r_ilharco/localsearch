/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test Class for the Service Class to query the Users Territory Information and the relative Territory 
* Model Hierarchy.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-08-06
* @systemLayer    Service

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Gennaro Casola
* 2020-08-06      SPIII-2042 - Territorial hierarchy shift from Account to Order object
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@IsTest
public class Test_SRV_UserTerritory2Association {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management';
        insert byPassTrigger;
    }
    
	@TestSetup
    public static void test_setupData(){
        insertBypassFlowNames();
        Test_DataFactory.insertFutureUsers();
    }
    
    @IsTest
    public static void test_getUserTerritoryHierarchyByUserIds(){
        Map<Id, User> usersMap = new Map<Id, User>([SELECT Id FROM User]);
        Map<Id, Territory2> territoriesMap = new Map<Id, Territory2>([SELECT Id FROM Territory2]);
        
       SRV_UserTerritory2Association.getUserTerritoryHierarchyByUserIds(usersMap.keySet());
    }
}