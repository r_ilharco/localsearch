public class SblDeliveriesChangesJob implements Queueable, Database.AllowsCallouts {
    private DateTime startDate;
    private DateTime endDate;
    
    public SblDeliveriesChangesJob() {
        // Without parameters, will check for last successful run, and fetch from that date.
        String className = String.valueOf(this).substring(0, String.valueOf(this).indexOf(':'));
        List<AsyncApexJob> lastJobs = [SELECT CreatedDate from AsyncApexJob where Status='Completed' 
                              and ApexClassId in (SELECT Id FROM ApexClass WHERE name=:className) 
                              Order By CreatedDate desc LIMIT 1];
        if(lastJobs.size() > 0) { this.startDate = lastJobs[0].CreatedDate; }
    }
    
    public SblDeliveriesChangesJob(DateTime startDate, DateTime endDate) {
    	this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public void execute(QueueableContext context) {
		BillingResults.getDeliveriesChanges(this.startDate, this.endDate);
    }
}