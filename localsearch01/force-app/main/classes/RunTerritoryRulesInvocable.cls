/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @testClass      Test_RunTerritoryRulesInvocable
* @modifiedby     Mara Mazzarella
* @systemLayer    Invocation
* 2020-09-10      SPIII-3158 Account's Territory not updating when SMA accepts a Change Owner request
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class RunTerritoryRulesInvocable implements Database.AllowsCallouts  {

@invocableMethod (label='RunTerritoryRules' description='RunTerritoryRules')
    public static void RunTerritoryRules(List<String> accntIds){
        if(!System.isFuture() && !System.isBatch()){
            List<AccountShare> accShareList = new List<AccountShare>();  
            Map<Id,Account> NewAccount = new Map<Id,Account>([select id, Assigned_Territory__c, TerritoryException__c, OldApproverId__c, RegionalLeader__c,Tech_Requested_Approver__c,OwnerId from Account where id in:accntIds ]);
            System.debug('--> NewAccount: ' + JSON.serialize(NewAccount));
            Set<String> accsId = new Set<String>();
            List<String> accShareIds = new List<String>();
            for(Account acc: NewAccount.values()){
                if(String.isNotBlank( NewAccount.get(acc.Id).OldApproverId__c)){
                    AccountShare accShare= new AccountShare();
                    accShare.AccountId = acc.Id;
                    accShare.AccountAccessLevel = 'Edit';
                    accShare.OpportunityAccessLevel = 'Read';
                    accShare.UserOrGroupId = (Id) NewAccount.get(acc.Id).OldApproverId__c;
                    accShare.RowCause = Schema.AccountShare.RowCause.Manual;
                    accShareList.add(accShare);
                    accsId.add(acc.Id);
                }
            }
            System.debug('accShareList'+accShareList);
            if(!accShareList.isEmpty()){
                System.debug('insert accShareList');
                insert accShareList;
            }
            for(AccountShare accShare :accShareList){
                accShareIds.add(accShare.Id);
            }
            runterritoryCallout(accntIds,accShareIds);
        }
    }
    
    @Future(callout=true)
    public static void runterritoryCallout(List<String> accntIds, List<String> shareIds){
        Set<Id> accountIds = new Set<Id>();
        String sessionId = null;    
        String accountTag = '<urn:sObjects> '+
            '<urn1:type>Account</urn1:type>  '+
            '<urn1:Id>{ACCID}</urn1:Id>   '+
            '</urn:sObjects> ' ;    
        String requestTemplate = '<soapenv:Envelope '+
            'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'+
            ' xmlns:urn="urn:partner.soap.sforce.com"'+
            ' xmlns:urn1="urn:sobject.partner.soap.sforce.com">'+
            '<soapenv:Header> '+
            '<urn:AssignmentRuleHeader> '+ 
            '<urn:useDefaultRule>true</urn:useDefaultRule> '+
            '<urn:assignmentRuleId></urn:assignmentRuleId> '+
            '</urn:AssignmentRuleHeader>  '+
            '<urn:SessionHeader> '+
            '<urn:sessionId>{SESSID}</urn:sessionId> '+
            '</urn:SessionHeader> '+
            '</soapenv:Header> '+
            '<soapenv:Body> '+
            '<urn:update> '+
            ' {ACCLISTS}'+ 
            '</urn:update> '+
            '</soapenv:Body> '+
            '</soapenv:Envelope>';
        if(!Test.isRunningTest()){
            sessionId = Page.SessionIDHack.getContent().toString(); 
        }
        else{
            sessionId = 'test';
        }
        List<String> lstAccString = new List<String>();
        if(accntIds != null){
            for(String accId:accntIds){
                lstAccString.add(accountTag.replace('{ACCID}', accId));
                accountIds.add(accId);
            }
        } 
        requestTemplate = requestTemplate.replace('{ACCLISTS}', String.join(lstAccString, ' ')) ;
        requestTemplate = requestTemplate.replace('{SESSID}', sessionId) ;        
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(System.URL.getSalesforceBaseUrl().toExternalForm()+
                            '/services/Soap/u/41.0/'+UserInfo.getOrganizationId());
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody(requestTemplate);
        request.setTimeout(120000);
        if(!Test.isRunningTest()){
            String s = String.valueOf(new Http().send(request).getBodyDocument());
        }
        
        if(accntIds != NULL && accntIds.size() > 0){
            List<Account> accountsToUpdateList = new List<Account>();
            for(Id accntId : accntIds){
                Account acc = new Account();
                acc.Id = accntId;
                acc.OldApproverId__c = NULL;
                accountsToUpdateList.add(acc);
            }
            
            AccountTriggerHandler.disableTrigger = true;
            Database.update(accountsToUpdateList, false);
            AccountTriggerHandler.disableTrigger = false;
        }
        
        List<AccountShare> accShares = [select id from AccountShare where id in: shareIds];
        if(!accShares.isEmpty()) delete accShares; 
    }    
}