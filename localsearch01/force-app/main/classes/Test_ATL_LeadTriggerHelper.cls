@isTest
private class Test_ATL_LeadTriggerHelper {
  static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
     
    
    @TestSetup
    static void setup(){
        
        insertBypassFlowNames();

        test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
        system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
        insert psas;
        }
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        insert acc;
        
        Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
        list<lead> Tlead = Test_DataFactory.createleads('Test','Lead',10);
        for(lead l : tlead) {
        l.LeadSource = ConstantsUtil.LEAD_SOURCE_MANUAL_Sales;
        }
        insert Tlead;
                      
       
        Bypass_Triggers__c profilewideBypass = new Bypass_Triggers__c();
        profilewideBypass.SetupOwnerId = UserInfo.getProfileId();
        profilewideBypass.Trigger_Name__c = 'NomeTest';
        insert profilewideBypass;
        Bypass_Triggers__c setting = new Bypass_Triggers__c();
        setting.Trigger_Name__c = 'Trying';
        insert setting;
    }

    @isTest static void test_setManualLeadStatusFromRating(){
     list<lead> leads = [select id,LeadSource,Rating from lead];
     test.startTest();
     ATL_LeadTriggerHelper.setManualLeadStatusFromRating(leads);
     ATL_LeadTriggerHelper.checkNewLeadsLegalAddress(leads);
     test.stopTest();
}

@isTest static void test_checkLeadLegalAddressCity(){
     list<lead> oldleads = [select id,LeadSource,Rating,City,street,PostalCode,UID__c,Phone,Email,ConvertedAccountId,AddressValidationStatus__c from lead order by id asc limit 4];
    for (lead l : oldleads){
     l.City = 'Napoli';
     update l;
     }
     list<lead> newleads = [select id,LeadSource,Rating,City,street,PostalCode,UID__c,Phone,Email,ConvertedAccountId,AddressValidationStatus__c from lead order by id asc limit 4];
     for (lead l : newleads){
     l.City = 'Milano';
     update l;
     }
     map<id,lead> oldmap = new map<id,lead>();
     map<id,lead> newmap = new map<id,lead>();
   
for (lead l : oldleads){
       oldmap.put(l.id,l);
      }
for (lead l2 : newleads){
       newmap.put(l2.id,l2);
      }

     test.startTest();
     ATL_LeadTriggerHelper.checkLeadLegalAddress(newmap, oldmap);
     test.stopTest();
}

@isTest static void test_checkLeadLegalAddressOtherConditions(){
     list<lead> oldleads = [select id,LeadSource,Rating,City,street,PostalCode,UID__c,Phone,Email,ConvertedAccountId,AddressValidationStatus__c from lead order by id asc limit 4];
     list<lead> newleads = [select id,LeadSource,Rating,City,street,PostalCode,UID__c,Phone,Email,ConvertedAccountId,AddressValidationStatus__c from lead order by id asc limit 4];
     map<id,lead> oldmap = new map<id,lead>();
     map<id,lead> newmap = new map<id,lead>();
   
for (lead l : oldleads){
       oldmap.put(l.id,l);
      }
for (lead l2 : newleads){
       newmap.put(l2.id,l2);
      }

     test.startTest();
     ATL_LeadTriggerHelper.checkLeadLegalAddress(newmap, oldmap);
     test.stopTest();
}

@isTest static void test_extendLeadAccessThroughTerritoryHierarchy(){
     
     list<lead> newleads = [select id,OwnerId,LeadSource,Rating,City,street,PostalCode,UID__c,Phone,Email,ConvertedAccountId,AddressValidationStatus__c from lead order by id asc limit 4];
      map<id,lead> newmap = new map<id,lead>();
        user u = [Select id,name from user where Code__c = 'SYSADM' limit 1];
        list<Territory2> terrs = Test_DataFactory.createTerritories('Localsearch_Territory_Type',u);
        territory2 terr = [select id from territory2 limit 1][0];
      UserTerritory2Association Ut2A = new UserTerritory2Association();
      ut2a.RoleInTerritory2 = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
      ut2a.Territory2Id = terr.id;
      ut2a.UserId = u.id;
      //insert ut2a;
for (lead l2 : newleads){
       newmap.put(l2.id,l2);
      }

     test.startTest();
     ATL_LeadTriggerHelper.extendLeadAccessThroughTerritoryHierarchy(newmap);
     test.stopTest();
}
}