@IsTest
public class Test_ProductSubstitutionCmpController {
	static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
    }
    
    @Testsetup  static void setup(){
        insertBypassFlowNames();
        //test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        list<user> usersTel = [select id,name,code__c from user where Code__c = 'DMC'];
        user testuser = users[0];
        system.runas(testuser){
            List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
            PermissionSetAssignment psain = new PermissionSetAssignment();
            psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
            psain.AssigneeId = testuser.Id;
            psas.add(psain);
           // insert psas;
        }
        //permissionSetAssignment psa = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        //user u = [select id from user where id=: psa.AssigneeId][0];
        
        Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        system.runas(testuser){
            acc.OwnerId = userinfo.getUserId();
            insert acc;
        }
        opportunity  opp = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
        opp.OwnerId = Userinfo.getuserid();
        //insert opp;
        opportunity  opp1 = Test_DataFactory.createOpportunities('TestOpp','Quote Definition',acc.id,1)[0];
        opp1.OwnerId = Userinfo.getuserid();
        List<opportunity> insertopp = new list<opportunity>{opp,opp1};
        insert insertopp;
        list<account> accs = new list<account>();
        accs.add(acc);
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc.Id, 'Last Name Test', 1);
        insert cont;
        List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accs, cont, 1);
        insert bp;
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(opp.Id, acc.Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        quote1.Replacement__c = true;
        quote1.Allocation_not_empty__c = true;
        quote1.Ad_Allocation_Ids__c = '5de7a446b919f40001888947';
        quote1.SBQQ__ExpirationDate__c = date.today();
        quote1.Billing_Profile__c = bp[0].id;
        //insert quote1;
        SBQQ__Quote__c quote2 = Test_DataFactory.generateQuote(opp1.Id, acc.Id, cont[0].Id, null);
        quote2.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote2.Allocation_not_empty__c = true;
        quote2.Ad_Allocation_Ids__c = '5de7a446b919f40001888947';
        quote2.SBQQ__ExpirationDate__c = date.today();
        quote2.Billing_Profile__c = bp[0].id;
        //insert quote2;
        List<SBQQ__Quote__c> insertQt = new list<SBQQ__Quote__c>{quote1,quote2};
        insert insertQt;
        Product2 product1 = generateProduct();
        insert product1;
        Place__c place = Test_DataFactory.generatePlaces(acc.Id, 'Place Name Test');
        place.PlaceID__c = 'externalPlaceId';
        insert place;
        SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c 
                                FROM SBQQ__Quote__c LIMIT 1];
        Product2 product3 = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                             ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                             Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                             Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                             Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                             FROM Product2 LIMIT 1 ];
        
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product3.Id, Date.today().addDays(-1), null, null, null, 'Annual');
        quoteLine.Samba_Migration__c = true;
        quoteLine.Place__c = place.id;
        SBQQ__QuoteLine__c quoteLine1 = Test_DataFactory.generateQuoteLine_final(quote2.Id, product3.Id, Date.today().addDays(-1), null, null, null, 'Annual',quoteLine.SBQQ__PricebookEntryId__c);
        quoteLine1.Place__c = place.id;
        test.starttest();
        List<SBQQ__QuoteLine__c> insertQl = new list<SBQQ__QuoteLine__c>{quoteLine,quoteLine1};
        insert insertQl;
        test.stopTest();
        
     
        
        Contract cntrct = Test_DataFactory.createContracts (acc.Id, 1)[0];
        insert cntrct;
        List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',place.id,cntrct.id,1);
        subs[0].SBQQ__QuoteLine__c = quoteLine.Id;
        subs[0].SBQQ__Product__c = product1.Id;
        subs[0].SBQQ__SubscriptionStartDate__c	 = Date.today().addMonths(-13);
        subs[0].SBQQ__Account__c = acc.Id;
        
        system.runas(testuser){
            SubscriptionTriggerHandler.disableTrigger = true;
            insert subs;
             SubscriptionTriggerHandler.disableTrigger = false;
        }
    }
    
    @IsTest
    static void Test_Method1(){
    	Account acc = [SELECT Id FROM Account LIMIT 1];
        ProductSubstitutionCmpController.getActiveMasterSubsFromAccount(acc.Id);
        ProductSubstitutionCmpController.getActiveSubscriptionFromAccount(acc.Id);
    }
    
    
      @IsTest
    static void Test_blockProductSellingOnSamePlace(){
    	SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c where place__r.PlaceID__c =: 'externalPlaceId' LIMIT 1];
        ProductSubstitutionCmpController.blockProductSellingOnSamePlace(quoteLine.SBQQ__Quote__c);  
    }
    @IsTest
    static void Test_getActiveDraftMasterSubsFromAccount(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Date d = Date.today();
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        
        ProductSubstitutionCmpController.getActiveDraftMasterSubsFromAccount(acc.Id,d, quote.Id);
        ProductSubstitutionCmpController.getQuoteLineMapByQuoteId01(quote.Id);
    }
    
     @IsTest
    static void Test_Method2(){
    	SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        ProductSubstitutionCmpController.getQuoteLineMapByQuoteId(quote.Id);
        ProductSubstitutionCmpController.getQuote(quote.Id);
        ProductSubstitutionCmpController.getAccess(quote.Id);
    }
    
    @IsTest
    static void Test_Method3(){
        Map<Id, Id> qlSubMap = new Map<Id, Id>();
    	SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c LIMIT 1];
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c LIMIT 1];
        
        qlSubMap.put(sub.Id, quoteLine.Id);
        ProductSubstitutionCmpController.updateReplacedSubscriptions(qlSubMap, quoteLine.SBQQ__Quote__c);
    }
    
   	@IsTest
    static void Test_Method4(){
        Set<string> quoteTypes = new Set<string>{ConstantsUtil.QUOTE_TYPE_REPLACEMENT};
        Map<Id, Id> qlSubMap = new Map<Id, Id>();
    	SBQQ__QuoteLine__c quoteLine = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Type__c in :quoteTypes LIMIT 1];
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c LIMIT 1];
        
        qlSubMap.put(sub.Id, quoteLine.Id);
        ProductSubstitutionCmpController.updateReplacedSubscriptions(qlSubMap, quoteLine.SBQQ__Quote__c);
    }
    
    @IsTest
    static void test_SRV_QuoteLine(){
        
        SBQQ.TriggerControl.disable();
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
            
        }
        
        List<String> fieldNamesAccount = new List<String>(Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount =' SELECT ' + String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        Billing_Profile__c billingProfile = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :account.Id LIMIT 1];
        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id LIMIT 1];
        Place__c place = [SELECT Id FROM Place__c LIMIT 1];
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote =' SELECT ' + String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes){
            product2Pbe.put(pbe.Product2Id, pbe.Id);
        }
        
        SBQQ__QuoteLine__c fatherQuoteLine = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBB001').Id));
        insert fatherQuoteLine;
        
        quote.SBQQ__Status__c = ConstantsUtil.QUOTE_STATUS_PRESENTED;
        update quote;
        
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        fatherOI.Place__c = place.Id;
        fatherOI.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
        insert fatherOI;
        OrderItem fatherOI1 = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQuoteLine);
        fatherOI1.Place__c = place.Id;
        fatherOI1.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED;
        insert fatherOI1;
        
        
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        update order;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        fatherSub.Place__c = place.Id;
        insert fatherSub;
        
		SBQQ__Subscription__c fatherSub1 = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI1);
        fatherSub1.Subscription_Term__c = fatherQuoteLine.Subscription_Term__c;
        fatherSub1.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        fatherSub1.Place__c = place.Id;
        fatherSub1.In_termination__c = true;
        fatherSub1.SBQQ__TerminatedDate__c = DATE.today().ADDDAYS(2);
        insert fatherSub1;
        
        Opportunity opportunityPlaceValidation = Test_DataFactory.generateOpportunity('Opp Place Validation', account.Id);
        
        insert opportunityPlaceValidation;
        String oppName = 'Opp Place Validation';
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(opportunityPlaceValidation.Id, account.Id, contacT.Id, null);
        quote1.SBQQ__ExpirationDate__c = date.today();
        quote1.Billing_Profile__c = billingProfile.id;
        insert quote1;
        String queryQuotePlaceValidation =' SELECT ' + String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.Name =: oppName LIMIT 1';
        SBQQ__Quote__c quotePlaceValidation = Database.query(queryQuotePlaceValidation);
        SBQQ__QuoteLine__c fatherQuoteLinePV = Test_DataFactory.generateQuoteLine_final(quotePlaceValidation.Id, productCodeToProduct.get('LBB001').Id, Date.today(), contact.Id, place.Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBB001').Id));
        insert fatherQuoteLinePV;
        Test.startTest();
		SRV_QuoteLine.blockProductSubstituionByPlace(quotePlaceValidation.id); 
        Test.stopTest();
        quotePlaceValidation.Replacement__c = true;
        UPDATE quotePlaceValidation;
        SRV_QuoteLine.blockProductSubstituionByPlace(quotePlaceValidation.id); 
        SBQQ.TriggerControl.enable();
        QuoteLineTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
    }
    
    
    public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Free');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit Free');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;
    }
}