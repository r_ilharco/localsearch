/* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed (SPIII 1212)
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
* @changes        Reference to fields listed in SPIII 1057 removed
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-03              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class Test_SubscriptionTerritoryChangedBatch {
    static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management,Subscriptions Handler';
            insert byPassTrigger;
        }
    
    @TestSetup
    public static void setup(){
        insertBypassFlowNames();
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        Product2 product = generateProduct_myCOCKPITBasic();
        insert product;
        List<Account> acc = Test_DataFactory.generateAccounts('Account test name',1,false);
        insert acc;     
        List<Contact> contacts = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last name test', 1);
        insert contacts;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Opportunity Test Name', acc[0].Id);
        insert opportunity;
        SBQQ__Quote__c quote = Test_DataFactory.generateQuote(opportunity.Id, acc[0].Id, contacts[0].Id, null);
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        insert quote;
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today() , null, null, null, 'Annual');
        insert quoteLine;
        Order activationOrder = generateActivationOrder(acc[0].Id, quote.Id, ConstantsUtil.ORDER_TYPE_NEW, ConstantsUtil.ORDER_TYPE_ACTIVATION);
        insert activationOrder;
        OrderItem activationOrderItem = Test_DataFactory.generateFatherOrderItem(activationOrder.Id, quoteLine);
        activationOrderItem.Quantity = 1;
        activationOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        insert activationOrderItem;
        Contract contract = Test_DataFactory.generateContractFromOrder(activationOrder, false);
        contract.Status = 'Draft';
        contract.AccountId = acc[0].Id;
        insert contract;
        Place__c place = Test_DataFactory.generatePlaces(acc[0].Id, 'Place test name');
        insert place;
        //SBQQ__Subscription__c subscription = Test_DataFactory.generateFatherSubFromOI(contract, activationOrderItem);
            SBQQ__Subscription__c subs = new SBQQ__Subscription__c();
            subs.SBQQ__Account__c = acc[0].Id;
            subs.Place__c = place.Id;
            subs.SBQQ__Contract__c = contract.Id;
            subs.SBQQ__Quantity__c = 2;
            subs.Status__c = 'Draft';
            subs.SBQQ__Product__c = product.Id;
            subs.Termination_Generate_Credit_Note__c = true;
        insert subs;
        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void Test_SubscriptionTerritoryChangedBatchExecute(){  
        
        List<Account> listAccount = [SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, BillingCity, BillingState,
                                     BillingPostalCode, BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress, 
                                     ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude,
                                     ShippingGeocodeAccuracy, ShippingAddress, Phone, Fax, AccountNumber, Website, PhotoUrl, Sic, Industry, AnnualRevenue,
                                     NumberOfEmployees, Ownership, TickerSymbol, Description, Rating, Site, OwnerId, CreatedDate, CreatedById,
                                     LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate,
                                     IsExcludedFromRealign, IsCustomerPortal, Jigsaw, JigsawCompanyId, AccountSource, SicDesc,
                                     SBQQ__AssetQuantitiesCombined__c, SBQQ__CoTermedContractsCombined__c, SBQQ__CoTerminationEvent__c, 
                                     SBQQ__ContractCoTermination__c, SBQQ__DefaultOpportunity__c, SBQQ__IgnoreParentContractedPrices__c, SBQQ__PreserveBundle__c, SBQQ__PriceHoldEnd__c, 
                                     SBQQ__RenewalModel__c, SBQQ__RenewalPricingMethod__c, SBQQ__TaxExempt__c, Account_Fax__c, Account_Phone__c, Account_Rating__c, Active_Contracts__c, 
                                     Amount_At_Risk__c, BillingAddressValidationDate__c, BillingAddressValidationStatus__c,                                     ChurnReason__c,
                                     ClientRating__c, Company_Since__c, Created_Date__c, CreditStatus__c, Customer_Number__c, DataSource__c, Email__c, 
                                     GoldenRecordID__c, LCM_NX_MergeIDs__c, LCM_SAMBA_MergeIDs__c, LegalEntity__c, Mobile_Phone__c, 
                                     POBox__c, P_O_Box_City__c, P_O_Box_Zip_Postal_Code__c, PreferredLanguage__c, RegionalDirector__c,
                                     RegionalLeader__c, SalesChannel__c, Salutation__c, ShippingAddressValidationDate__c, ShippingAddressValidationStatus__c,
                                     ShippingPO_BoxCity__c, ShippingPO_BoxZip_PostalCode__c, ShippingPO_Box__c, Special__c, Status__c, Swiss_List_Candidate__c, 
                                     UID__c, Language_Short_Form__c,                                      Reason__c, Contact_Method__c, Contact_Method_i__c, Activation_Date__c, AddressValidated__c, AddressValidationDate__c, Address_Integration__c,
                                     Allowed_communication_channel__c, ApprovalStep__c, Approval_Requester_Email__c, Assigned_Territory__c, Cluster_Id__c,
                                     Communication_Channel__c, Communication_stop__c, Converted__c, Customer_Type__c, Digital_Score__c, Employees__c, Foundation_Date__c, Full_Address__c, 
                                     HRID__c, Last_Inactive_Date__c, Legal_Address_equals_to_Shipping_Address__c, Legal_Form__c, Lock_Account__c, 
                                     MarkedAsNotDuplicated__c, New_Customer__c, Onwer_Formula__c, Owner_Changed__c, Process_Type__c, 
                                     Product_of_interest__c, Reassigned__c, Resubmission_date__c, Resubmission_reason__c, Sales_Status__c,
                                     Sales_manager_substitute__c, Tech_ZipCode__c, Territory_Changed__c, VAT_ID__c,
                                     isDuplicated__c FROM Account LIMIT 1];
        List<Id> ListAccountId = new List<Id>();
        ListAccountId.add(listAccount[0].Id);
        
        SBQQ__Subscription__c listSub = [SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
                                         SystemModstamp, LastViewedDate, LastReferencedDate, SBQQ__Account__c, SBQQ__AdditionalDiscountAmount__c, SBQQ__BillingFrequency__c,
                                         SBQQ__BillingType__c, SBQQ__Bundle__c, SBQQ__BundledQuantity__c, SBQQ__Bundled__c, SBQQ__ChargeType__c, SBQQ__ComponentDiscountedByPackage__c,
                                         SBQQ__ComponentSubscriptionScope__c, SBQQ__ContractNumber__c, SBQQ__Contract__c, SBQQ__CreditProductId__c, SBQQ__CustomerPrice__c, SBQQ__DimensionType__c, SBQQ__Dimension__c, SBQQ__DiscountScheduleType__c, SBQQ__DiscountSchedule__c, SBQQ__Discount__c, SBQQ__DistributorDiscount__c, SBQQ__DynamicOptionId__c, SBQQ__EndDate__c, SBQQ__ListPrice__c, SBQQ__MarkupAmount__c, SBQQ__MarkupRate__c, SBQQ__NetPrice__c, SBQQ__Number__c, SBQQ__OptionDiscountAmount__c, SBQQ__OptionDiscount__c, SBQQ__OptionLevel__c, SBQQ__OptionType__c, SBQQ__OrderProduct__c, SBQQ__OriginalQuoteLine__c, SBQQ__OriginalUnitCost__c, SBQQ__PartnerDiscount__c, SBQQ__PricingMethod__c, SBQQ__ProductId__c, SBQQ__ProductName__c, SBQQ__ProductOption__c, SBQQ__Product__c, SBQQ__ProrateMultiplier__c, SBQQ__Quantity__c, SBQQ__QuoteLine__c, SBQQ__RegularPrice__c, SBQQ__RenewalPrice__c, SBQQ__RenewalProductId__c, SBQQ__RenewalProductOptionId__c, SBQQ__RenewalProductOptionProductId__c, SBQQ__RenewalProductOptionSubscriptionPricing__c, SBQQ__RenewalQuantity__c, SBQQ__RenewalUpliftRate__c, SBQQ__RenewedDate__c, SBQQ__RequiredById__c, SBQQ__RequiredByProduct__c, SBQQ__RevisedSubscription__c, SBQQ__RootId__c, SBQQ__SegmentEndDate__c, SBQQ__SegmentIndex__c, SBQQ__SegmentKey__c, SBQQ__SegmentLabel__c, SBQQ__SegmentQuantity__c, SBQQ__SegmentStartDate__c, SBQQ__SegmentUpliftAmount__c, SBQQ__SegmentUplift__c, SBQQ__SpecialPrice__c, SBQQ__StartDate__c, SBQQ__SubscriptionEndDate__c, SBQQ__SubscriptionStartDate__c, SBQQ__TermDiscountSchedule__c, SBQQ__TerminatedDate__c, SBQQ__UnitCost__c, SBQQ__ProductSubscriptionType__c, SBQQ__SubscriptionType__c, Billed__c, Billing_Status__c, Contract_Ref_Id__c, Parent_Element_Id__c, Place_Name__c, Place__c, Product_Code__c, Status__c, Subsctiption_Status__c, Termination_Date__c, Termination_Generate_Credit_Note__c, Termination_Quantity__c, Termination_Reason__c, SBQQ__HasConsumptionSchedule__c, Next_Invoice_Date__c, Subscription_Activated_Date__c, SBQQ__PackageProductCode__c, SBQQ__PackageProductDescription__c, IS_Billable__c, Activate_Later__c, Ad_Context_Allocation__c, CDescription__c, Campaign_Id__c, CategoryID__c, Category__c, Cluster_Id__c, Edition__c, Editorial_Content__c, End_Date__c, External_Id__c, Grouping_Check__c, In_Termination__c, Is_Master_Product__c, Keywords__c, LBx_Contact__c, Language__c, LocationID__c, Location__c, Manual_Activation__c, Manual_Amendment_Product__c, Manual_Termination_Product__c, Manuscript__c, NetTotal__c, Next_Renewal_Date__c, One_time_Fee_Billed__c, Package_Total__c, Placement_Phone__c, PricebookEntryId__c, Product_Family__c, Product_Group__c, Production__c, RequiredBy__c, SambaIsStartConfirmed__c, Samba_ExternalID__c, Simulation_Number__c, Subscription_Term__c, TLPaket_Region__c, Teaser_Title__c, Teaser__c, Template__c, Total__c, Url2__c, Url3__c, Url4__c, Url__c, Website_Language__c, SubscriptionMigration__c FROM SBQQ__Subscription__c LIMIT 1];
        
        //RETRIEVE DELLA SUBSCRIPTION
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        Database.BatchableContext db = null;
        SubscriptionTerritoryChangedBatch tsc = new SubscriptionTerritoryChangedBatch(ListAccountId);
        tsc.execute(db, new list<SBQQ__Subscription__c>{listSub});
        SubscriptionOwnerRecalculationBatch tsc2 = new SubscriptionOwnerRecalculationBatch(ListAccountId);
        tsc2.execute(db, new list<SBQQ__Subscription__c>{listSub});
        tsc2.finish(db);
        Test.stopTest(); 
    }
    
    public static Product2 generateProduct_myCOCKPITBasic(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Order generateActivationOrder(String accountId, String quoteId, String orderType, String customType){
        Map<String,String> fieldApiNameToValueOrder = new Map<String,String>();
        fieldApiNameToValueOrder.put('Custom_Type__c', customType);
        fieldApiNameToValueOrder.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValueOrder.put('EndDate', String.valueOf(Date.today().addDays(10)));
        fieldApiNameToValueOrder.put('AccountId', accountId);
        fieldApiNameToValueOrder.put('SBQQ__Quote__c', quoteId);
        fieldApiNameToValueOrder.put('Type', orderType);
        Order ord = Test_DataFactory.generateOrder(fieldApiNameToValueOrder);
        return ord;
    }
}