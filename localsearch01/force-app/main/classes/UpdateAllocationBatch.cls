/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* When Adv. Online orderitem is creared the flag Tech_Update_Allocation__c is set to true.
* This batch get all the records having Tech_Update_Allocation__c to true and make a callout
* to update the allocations enddate.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mara Mazzarella   <mamazzarella@deloitte.it>
* @created        2021-01-12
* @systemLayer    Service
* @testClass	  Test_OrderProductTrigger
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class UpdateAllocationBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts {
    
    global void execute(SchedulableContext sc) {
        UpdateAllocationBatch b = new UpdateAllocationBatch(); 
        database.executebatch(b,1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator('SELECT Id, Ad_Context_Allocation__c,Product2.ExternalKey_ProductCode__c,'+
                                        ' CategoryID__c, Tech_Update_Allocation__c, ServiceDate,SBQQ__SubscriptionType__c,'+
                                        ' Product2.ProductCode, Language__c,EndDate,End_Date__c, LocationID__c, OrderId'+
                                        ' FROM OrderItem WHERE Tech_Update_Allocation__c = true');
    }
    
    global void execute(Database.BatchableContext info, List<OrderItem> scope){ 
        Set<String> prodCodes = new Set<String>();
        List<OrderItem> scopeToTerminate = new List<OrderItem>();
        List<Log__c> logsToInsert = new List<Log__c>();
        Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem>();  
        String baseCatgory = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_CATEGORY);
        String baseLocation = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_LOCATION);
        String allocationId = '';
        String recordId;
        Map<String,CASA_Product_Config__mdt> prodCodeToCasaConfig = new Map<String,CASA_Product_Config__mdt>();
        if(!scope.isEmpty()){
            Map<String,String> languageMap = new Map<String,String>{'German' => 'de',
                                                                    'French' => 'fr',
                                                                    'Italian' => 'it', 
                                                                    'English' => 'en'
                                                                    };
            for(OrderItem oi : scope){
                prodCodes.add(oi.Product2.ExternalKey_ProductCode__c);
            }
            List<CASA_Product_Config__mdt> productConfiguration = [ SELECT  Product_Code__c, 
                                                                           Slot__c, 
                                                                           Language__c, 
                                                                           Low_Price__c,
                                                                           Medium_Price__c,
                                                                           High_Price__c
                                                                           FROM    CASA_Product_Config__mdt
                                                                           WHERE   Product_Code__c IN :prodCodes];
            for(CASA_Product_Config__mdt currentMdt : productConfiguration){
                prodCodeToCasaConfig.put(currentMdt.Product_Code__c,currentMdt);
            }
            if(!prodCodeToCasaConfig.isEmpty()){
                baseCatgory = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_CATEGORY);
        		baseLocation = CustomMetadataUtil.getEndpoint(ConstantsUtil.HREF_LOCATION);
            }

            for(OrderItem currentOI : scope){
                Map<String, String> parameters = new Map<String,String>();
                if(String.isNotBlank(currentOI.CategoryID__c)) parameters.put('category', baseCatgory+currentOI.CategoryID__c);
                if(String.isNotBlank(currentOI.LocationID__c) && !currentOI.LocationID__c.equals('all')) parameters.put('region', baseLocation+currentOI.LocationID__c);
                parameters.put('start_date', SRV_LEnhancedProdConfigController.formatDate(currentOI.ServiceDate));
                parameters.put('slot', prodCodeToCasaConfig.get(currentOI.Product2.ExternalKey_ProductCode__c).Slot__c);
                parameters.put('product_code', currentOI.Product2.ProductCode);                    
                if(String.isNotBlank(currentOI.Language__c)) parameters.put('language', languageMap.get(currentOI.Language__c)); 
                parameters.put('end_date', SRV_LEnhancedProdConfigController.formatDate(currentOI.End_Date__c));
                parameters.put('expiration_date', SRV_LEnhancedProdConfigController.formatDate(currentOI.End_Date__c)); 
                Boolean isEvergreen = true;
                if(!ConstantsUtil.PRODUCT2_SUBSCRIPTIONTYPE_EVERGREEN.equalsIgnoreCase(currentOI.SBQQ__SubscriptionType__c)){
                    isEvergreen = false;		                       
                }
                if(isEvergreen) parameters.put('expiration_date', null); 
                SRV_EnhancedExtProdConfiguration.CasaAdContextResponse casaResponse = LEnhancedProdConfigController.adContextUpdate( currentOI.OrderId,  currentOI.Ad_Context_Allocation__c, parameters, isEvergreen);
                if(casaResponse!= null){
                    currentOI.Tech_Update_Allocation__c = false;
                    orderItemsToUpdate.put(currentOI.Id, currentOI);
                }
            }
            if(!logsToInsert.isEmpty()) insert logsToInsert;
            if(!orderItemsToUpdate.isEmpty()){
                OrderProductTriggerHandler.disableTrigger = true;
                update orderItemsToUpdate.values();
                OrderProductTriggerHandler.disableTrigger = false;
            }
        }
    }    
    global void finish(Database.BatchableContext info){     
    }
}