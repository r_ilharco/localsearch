global class migraQuoteDates implements Database.Batchable<sObject>, Database.Stateful{
    
    public Date dt;
    public string batch;
    
    public migraQuoteDates( string batchName, date startDt){
        batch = batchName;
        dt = startDt;
    }
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([select id,SBQQ__StartDate__c,SBQQ__ExpirationDate__c from SBQQ__Quote__c where SBQQ__Opportunity2__r.Quote_Migration_Batch__c =: batch ]);
	}
	
	global void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope) {
 
        List<SBQQ__Quote__c> lstQ = new List<SBQQ__Quote__c>(); 
    	for(SBQQ__Quote__c q : scope) {
            q.SBQQ__StartDate__c = dt;
            q.SBQQ__ExpirationDate__c = dt;
            q.Quote_Migration_Batch__c = batch;
            q.SBQQ__PriceBook__c = '01s1r000003jhYBAAY';
            lstQ.add(q);
		}
        System.debug('lstQ'+ lstQ);
		update lstQ;
		//
	}
	
	global void finish(Database.BatchableContext BC) {
        
    }

}