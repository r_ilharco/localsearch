/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-17-2019
 * Sprint      : 3
 * Work item   : SF2-88 Contract Renewal - US_65 Contract automatically renewed at subscription end date (implicit)
 * Testclass   : Test_UpdateNextRenewalDate
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class UpdateNextRenewalDate {
    @InvocableMethod(label='Next Renewal Date Generation' description='Next Renewal Date Generation')
    public static void nextRenewalDateGeneration (List<Id> recordIds) {
        List<Contract> contracts = [SELECT Id, nextRenewalDate__c, StartDate FROM Contract WHERE Id IN :recordIds];
        Map<Id, Integer> subTerms = new Map<Id, Integer>();
        Map<Id, SBQQ__Subscription__c> subscriptions = SEL_Subscription.getMasterSubscriptionsByContracts(new Set<Id>(recordIds));
        
        for(SBQQ__Subscription__c sub : subscriptions.values()){
            if(sub.SBQQ__Product__r.Subscription_Term__c != null && sub.Subscription_Term__c != null){
                subTerms.put(sub.SBQQ__Contract__c,Integer.valueOf(sub.Subscription_Term__c));
            }
        }
        
        for(Contract cont : contracts){
            if(subTerms.containsKey(cont.Id)){
                cont.nextRenewalDate__c = cont.StartDate.addMonths(subTerms.get(cont.Id));
            }
        }
        
        update contracts;
    }
}