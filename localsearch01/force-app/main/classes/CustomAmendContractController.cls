/**
 * Author	   : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date		   : 09-10-2019
 * Sprint      : Sprint 11
 * Work item   : SF2-367 Contract framework - products alignment
 * Testclass   : Test_CustomAmendContractController
 * Package     : 
 * Description : Controller to manage the upselling/ downselling of contract
 * Changelog   : 
──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-2064 - Add a validation Check on Renewal, Amend and Upgrade button on Contract
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-29
──────────────────────────────────────────────────────────────────────────────────────────────────
*/		

public without sharing class CustomAmendContractController {
    
    @AuraEnabled
    public static String checkForDownsellingUpselling(String recordId){
        
        Set<Id> productIds = new Set<Id>();
        User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
        Set<Id> accountTeamMembers = new Set<Id>();
        List<SBQQ__Subscription__c> masterSubs = SEL_Subscription.getMasterSubscriptionsByContractsId(new Set<Id>{recordId});
        
        Id pricebookId;
        for(SBQQ__Subscription__c s : masterSubs){
            pricebookId = s.SBQQ__Contract__r.SBQQ__Opportunity__r.Pricebook2Id;
            productIds.add(s.SBQQ__Product__c);
        }
        
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        Set<Id> inactiveEntriesForCurrentPB = new Set<Id>();
        if(pricebookId != NULL && !productIds.isEmpty()) pbes = [SELECT Id, Product2Id FROM PricebookEntry WHERE Pricebook2Id = :pricebookId AND Product2Id IN :productIds AND IsActive = FALSE];
        for(PricebookEntry pbe : pbes) inactiveEntriesForCurrentPB.add(pbe.Product2Id);
        
        Integer countUp = 0;
        Integer countDown = 0;
        Integer countInactiveEntries = 0;
        for(SBQQ__Subscription__c s : masterSubs){
            if(s.SBQQ__Product__r.Upselling_Disable__c) countUp++;
            if(s.SBQQ__Product__r.Downselling_Disable__c) countDown++;
            if(inactiveEntriesForCurrentPB?.contains(s.SBQQ__Product__c)) countInactiveEntries++;
        }
        
        if( (countUp == masterSubs.size() && countDown == masterSubs.size()) || countInactiveEntries == masterSubs.size()){
        	throw new AuraHandledException(Label.Amend_not_allowed);
        }
        List<Contract> cont =  [select id, Status, Account.OwnerId, AccountId from Contract where id =: recordId];
        if(!cont.isEmpty()){
            if(cont[0].status != ConstantsUtil.CONTRACT_STATUS_ACTIVE){
                throw new AuraHandledException(Label.Contract_Not_Active);
            }
            for(AccountTeamMember member : [select id, UserId from AccountTeamMember where Accountid =: cont[0].AccountId ]){
                accountTeamMembers.add(member.UserId);
            }
            if(cont[0].Account.OwnerId != currentUser.id 
               && currentUser.Profile.Name!= ConstantsUtil.SysAdmin
               && currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_DMC){
                   throw new AuraHandledException(Label.Upgrade_Downgrade_Amend_Validation);
               }
            
        }        
        return '';
    }
}