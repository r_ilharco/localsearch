/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-22-2019
 * Sprint      : 1
 * Work item   : US_61 - blocking down selling on a quote for the same place
 * Testclass   :
 * Package     : 
 * Description : Trigger Handler on SBQQ__QuoteLine__c
 * Changelog   : 
 */

public class SBQQQuoteLineTriggerHandler implements ITriggerHandler{
	public Boolean IsDisabled(){
        // to be implemented
        return false;
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        ATL_QuoteLineTriggerHelper.qtyValidation((List<SBQQ__QuoteLine__c >) newItems);
        ATL_QuoteLineTriggerHelper.placeValidation((List<SBQQ__QuoteLine__c >) newItems, new Map<Id,SBQQ__QuoteLine__c >());
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        ATL_QuoteLineTriggerHelper.SetToUpdateQuoteLine((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) oldItems);
        ATL_QuoteLineTriggerHelper.qtyValidation((List<SBQQ__QuoteLine__c >) newItems.values());
        ATL_QuoteLineTriggerHelper.placeValidation((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) newItems);

    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {
        ATL_QuoteLineTriggerHelper.qtyValidation((List<SBQQ__QuoteLine__c >) newItems.values());
        ATL_QuoteLineTriggerHelper.placeValidation((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) newItems);
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        //ATL_QuoteLineTriggerHelper.qtyValidation((List<SBQQ__QuoteLine__c >) newItems.values());
        //ATL_QuoteLineTriggerHelper.placeValidation((List<SBQQ__QuoteLine__c >) newItems.values(), (Map<Id,SBQQ__QuoteLine__c >) newItems);
    }
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}