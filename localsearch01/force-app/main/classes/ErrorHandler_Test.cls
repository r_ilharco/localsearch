@isTest()
public class ErrorHandler_Test {
    @isTest
    private static void test_001() {
        // use any object as reference
        Lead obj = new Lead(LastName='Test', Company='Test', GoldenRecordID__c='GR-L-001', City='Zurich', State='Zurich', PostalCode='4008', Country='CH');
        insert obj;
        
        Test.startTest();

        ErrorHandler.logInfo('ClassName', 'Subdomain', 'Info 1');
        ErrorHandler.logWarning('ClassName', 'Subdomain', ErrorHandler.WarningCode.W_UNKNOWN, 'Warning 1', 'ContextData');
        
        try {
            integer a = 0;
            integer b = 1;
            integer c = b/a;
        } catch (Exception ex) {
	        ErrorHandler.log(System.LoggingLevel.FINE, 'ClassName', 'Subdomain', ex, ErrorHandler.ErrorCode.E_UNKNOWN, 'Trace 1', 'ContextData', 'CorrelationId-1', 'Producer', 'Consumer', true);        
	        ErrorHandler.log(System.LoggingLevel.DEBUG, 'ClassName', 'Subdomain', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'ContextData', 'CorrelationId-2', 'Producer', 'Consumer', true);        
	        ErrorHandler.log(System.LoggingLevel.NONE, 'ClassName', 'Subdomain', ex, ErrorHandler.ErrorCode.E_UNKNOWN, 'Not retained', 'ContextData', 'CorrelationId-3', 'Producer', 'Consumer', true);
            TestException nestedException = new TestException(ex, 'Something went wrong', ErrorHandler.ErrorCode.E_UNKNOWN);
            ErrorHandler.logError('ClassName', 'Subdomain', nestedException);
        }

        TestException testException = new TestException('Test Exception', 'Free text');
        ErrorHandler.logError('ClassName', 'Subdomain', testException);
        ErrorHandler.logError('ClassName', 'Subdomain', new System.UnexpectedException('Unexpected'), ErrorHandler.ErrorCode.E_UNKNOWN, 'Error 2', 'ContextData', 'CorrelationId-4');
        ErrorHandler.logError('ClassName', 'Subdomain', new System.UnexpectedException('Unexpected'), ErrorHandler.ErrorCode.E_UNKNOWN, 'Error 2', 'ContextData', 'CorrelationId-5', obj);

        Test.stopTest();

        List<Error_Log__c> lastErrors = [SELECT Code__c, CorrelationId__c, Message__c, BusinessObjectIdValue__c, BusinessObjectIdName__c FROM Error_Log__c order by TimeStamp__c asc];
        System.assertEquals(8, lastErrors.size());
        System.assertEquals('Info 1', lastErrors[0].Message__c);
        System.assertEquals('Warning 1', lastErrors[1].Message__c);
        System.assertEquals('Trace 1 (Divide by 0)', lastErrors[2].Message__c);
        System.assertEquals('CorrelationId-1', lastErrors[2].CorrelationId__c);
        System.assertEquals('CorrelationId-2', lastErrors[3].CorrelationId__c);
        System.assertEquals('Something went wrong', lastErrors[4].Message__c);
        System.assertEquals('Free text', lastErrors[5].Code__c);
        System.assertEquals('CorrelationId-4', lastErrors[6].CorrelationId__c);
        System.assertEquals('CorrelationId-5', lastErrors[7].CorrelationId__c);
        System.assertEquals(obj.Id, lastErrors[7].BusinessObjectIdValue__c);
        System.assertEquals('Lead', lastErrors[7].BusinessObjectIdName__c);
       
        try {
            throw new TestException('Test Exception', ErrorHandler.ErrorCode.E_UNKNOWN);
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals(ErrorHandler.ErrorCode.E_UNKNOWN.name(), ((ErrorHandler.GenericException)ex).ErrorCode);
        }
        
        try {
            throw new TestException('Test Exception', ErrorHandler.WarningCode.W_UNKNOWN);
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals(ErrorHandler.WarningCode.W_UNKNOWN.name(), ((ErrorHandler.GenericException)ex).ErrorCode);
        }

        try {
            throw new TestException('Test Exception', 'Free text');
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals('Free text', ((ErrorHandler.GenericException)ex).ErrorCode);
        }
        
    }

    private static testmethod void test_002() {
        // use any object as reference
        Lead obj = new Lead(LastName='Test', Company='Test', GoldenRecordID__c='GR-L-001', City='Zurich', State='Zurich', PostalCode='4008', Country='CH');
        insert obj;
        List<Error_Log__c> errors = new List<Error_Log__c>();
        Test.startTest();

        errors.add(ErrorHandler.createLogInfo('ClassName', 'Subdomain', 'Info 1'));
        errors.add(ErrorHandler.createLogWarning('ClassName', 'Subdomain', ErrorHandler.WarningCode.W_UNKNOWN, 'Warning 1', 'ContextData'));
        
        try {
            integer a = 0;
            integer b = 1;
            integer c = b/a;
        } catch (Exception ex) {
	        errors.add(ErrorHandler.createLog(System.LoggingLevel.FINE, 'ClassName', 'Subdomain', ex, ErrorHandler.ErrorCode.E_UNKNOWN, 'Trace 1', 'ContextData', 'CorrelationId-1', 'Producer', 'Consumer', true));
	        errors.add(ErrorHandler.createLog(System.LoggingLevel.DEBUG, 'ClassName', 'Subdomain', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'ContextData', 'CorrelationId-2', 'Producer', 'Consumer', true));
            TestException nestedException = new TestException(ex, 'Something went wrong', ErrorHandler.ErrorCode.E_UNKNOWN);
            errors.add(ErrorHandler.createLogError('ClassName', 'Subdomain', nestedException));
        }

        TestException testException = new TestException('Test Exception', 'Free text');
        errors.add(ErrorHandler.createLogError('ClassName', 'Subdomain', testException));
        errors.add(ErrorHandler.createLogError('ClassName', 'Subdomain', new System.UnexpectedException('Unexpected'), ErrorHandler.ErrorCode.E_UNKNOWN, 'Error 2', 'ContextData', 'CorrelationId-4'));
        errors.add(ErrorHandler.createLogError('ClassName', 'Subdomain', new System.UnexpectedException('Unexpected'), ErrorHandler.ErrorCode.E_UNKNOWN, 'Error 2', 'ContextData', 'CorrelationId-5', obj));
		insert errors;
                   
        Test.stopTest();

        List<Error_Log__c> lastErrors = [SELECT Code__c, CorrelationId__c, Message__c, BusinessObjectIdValue__c, BusinessObjectIdName__c FROM Error_Log__c order by TimeStamp__c asc];
        System.assertEquals(8, lastErrors.size());
        System.assertEquals('Info 1', lastErrors[0].Message__c);
        System.assertEquals('Warning 1', lastErrors[1].Message__c);
        System.assertEquals('Trace 1 (Divide by 0)', lastErrors[2].Message__c);
        System.assertEquals('CorrelationId-1', lastErrors[2].CorrelationId__c);
        System.assertEquals('CorrelationId-2', lastErrors[3].CorrelationId__c);
        System.assertEquals('Something went wrong', lastErrors[4].Message__c);
        System.assertEquals('Free text', lastErrors[5].Code__c);
        System.assertEquals('CorrelationId-4', lastErrors[6].CorrelationId__c);
        System.assertEquals('CorrelationId-5', lastErrors[7].CorrelationId__c);
        System.assertEquals(obj.Id, lastErrors[7].BusinessObjectIdValue__c);
        System.assertEquals('Lead', lastErrors[7].BusinessObjectIdName__c);
       
        try {
            throw new TestException('Test Exception', ErrorHandler.ErrorCode.E_UNKNOWN);
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals(ErrorHandler.ErrorCode.E_UNKNOWN.name(), ((ErrorHandler.GenericException)ex).ErrorCode);
        }
        
        try {
            throw new TestException('Test Exception', ErrorHandler.WarningCode.W_UNKNOWN);
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals(ErrorHandler.WarningCode.W_UNKNOWN.name(), ((ErrorHandler.GenericException)ex).ErrorCode);
        }

        try {
            throw new TestException('Test Exception', 'Free text');
        } catch (Exception ex) {
            System.assert(ex instanceOf ErrorHandler.GenericException);
            System.assertEquals('Free text', ((ErrorHandler.GenericException)ex).ErrorCode);
        }
        
    }

    private static testmethod void test_003() {
    	Error_Log__c error = ErrorHandler.createLogInfo('ClassName', 'Subdomain', 'Info 1');
        ErrorEnqueueJob job = new ErrorEnqueueJob(error);
        job.execute(null);
        List<Error_Log__c> lastErrors = [SELECT Code__c, CorrelationId__c, Message__c, BusinessObjectIdValue__c, BusinessObjectIdName__c FROM Error_Log__c order by TimeStamp__c asc];
		System.assertEquals(1, lastErrors.size());
        System.assertEquals('Info 1', lastErrors[0].Message__c);        
    }
    
    public class TestException extends ErrorHandler.GenericException {
        public TestException(string message, string errorCode) {
            super(message, errorCode);
        }
        public TestException(string message, ErrorHandler.ErrorCode errorCode) {
            super(message, errorCode);
        }
        public TestException(string message, ErrorHandler.WarningCode warningCode) {
            super(message, warningCode);
        }
        public TestException(Exception initException, string message, ErrorHandler.ErrorCode errorCode) {
            super(initException, message, errorCode);
        }
    }     
}