@isTest
public class SwissbillingTransferJob_Test {
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Update Contract Company Sign Info';
        insert byPassTrigger;
    }
    
    public static testmethod void classTestMethod(){
        insertBypassFlowNames();
        Billing_Test.createAll(1);
        SwissBillingHttpResponseMock jsonResponse = new SwissBillingHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        Test.startTest();
        Billing.collectAndSend(10);
        Test.stopTest();
    }
    public static testmethod void classTestMethod2(){
        insertBypassFlowNames();
        billing_Test.createAll(1);     
        SwissbillingHelper.CheckPrintingProfilePublicLinks();
        List<SBQQ__Subscription__c> billableSubscriptions = Billing.getBillableSubscriptions();
        Billing.generateInvoices(billableSubscriptions);
        SwissBillingHttpResponseMock jsonResponse = new SwissBillingHttpResponseMock();
        Test.setMock(HttpCalloutMock.class, jsonResponse);
        Test.startTest();
        Billing.sendToSwissbilling();
        List<Invoice__c> invoices = [SELECT Id FROM Invoice__c];
        Set<Id> ids = new Set<Id>();
        if(invoices.size()>0){
            for(Invoice__c inv : invoices){
                ids.add(inv.id);
            }
            Database.executeBatch(new SwissbillingTransferJob(ids), 1);
            Database.executeBatch(new SwissbillingTransferJob(ids,ids), 1);
        }
        
        Test.stopTest();
    }
    
     public class SwissBillingHttpResponseMock implements HttpCalloutMock {
        public integer answerType = 200;
        public string invoiceDocId = 'invoice-id';
       
        public HTTPResponse respond(HTTPRequest req) {
            // Assert body and headers are as expected
            HttpResponse res = new HttpResponse();
            switch on answerType {
                when 200 {
                    Map<string, object> responseMap = new Map<string, object> {
                        'invoiceDocId' => invoiceDocId
                    };
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody(JSON.serialize(responseMap));
                    res.setStatusCode(200);                
                }
                when 400 {
                    res.setBody('<h1>400 - Bad Request</h1>');
                    res.setStatusCode(400);                     
                }
                when 401 {
                    res.setBody('<h1>401 - Unauthorized</h1>');
                    res.setStatusCode(401);                     
                }
            }
            return res;
        }
    }
 public class AbacusHttpResponseMock implements HttpCalloutMock {
        public integer answerType = 200;
        string soapNS = ConstantsUtil.XMLNS_SOAP;
        string xsi = ConstantsUtil.XMLNS_SCHEMA_INSTANCE;
        string abaTypesNS = ConstantsUtil.XMLNS_ABA_CONNECT_TYPES;
        
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            if(req.getEndpoint() == ConstantsUtil.BILLING_ESB_ABACUS_ENDPOINT + '/login') {
                switch on answerType {
                    when 200 {
                        res.setHeader('Content-Type', abaResponseContentType);
                        res.setBody(
                        '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82\n' +
                        'Content-Type: application/xop+xml; charset=UTF-8; type="text/xml"\n' +
                        'Content-Transfer-Encoding: binary\n' + 
                        '\n' +
                        xmlAbaResponse + '\n' +
                        '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82--\n'
                        );
                        res.setStatusCode(200);                
                    }
                    when 400 {
                        res.setBody('<h1>400 - Bad Request</h1>');
                        res.setStatusCode(400);                     
                    }
                    when 401 {
                        res.setBody('<h1>401 - Unauthorized</h1>');
                        res.setStatusCode(401);                     
                    }
                }                
            } else {
                switch on answerType {
                    when 200 {
                        Dom.Document responseDoc = new Dom.Document();
                        // TODO, return expected answers
                        responseDoc
                            .createRootElement('Envelope', soapNS, 'soapenv')
                            .addChildElement('Body', soapNS, 'soapenv');
                        
                        res.setHeader('Content-Type', abaResponseContentType);
                        res.setBody(
                        '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82\n' +
                        'Content-Type: application/xop+xml; charset=UTF-8; type="text/xml"\n' +
                        'Content-Transfer-Encoding: binary\n' + 
                        '\n' +
                        responseDoc.toXmlString() + '\n' +
                        '--MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82--\n'
                        );                        
                        res.setStatusCode(200);                
                    }
                    when 400 {
                        res.setBody('<h1>400 - Bad Request</h1>');
                        res.setStatusCode(400);                     
                    }
                    when 401 {
                        res.setBody('<h1>401 - Unauthorized</h1>');
                        res.setStatusCode(401);                     
                    }
                }
            }
            return res;
        }
    }
    
    private static final string abaResponseContentType = 'multipart/related; boundary="MIMEBoundary_52c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82"; type="application/xop+xml"; start="<0.42c07a7d73af8468895da7ec7fa69ca0a0b7cc2bbe773a82@apache.org>"; start-info="text/xml"';
	private static final string xmlAbaResponse = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + 
            '<soapenv:Body>' + 
                '<act:LoginResponse xmlns:act="http://www.abacus.ch/abaconnect/2007.10/core/AbaConnectTypes">' + 
                    '<act:LoginToken>0d3d834349c80afb64ef6e6dd30b5d52a6873c6d</act:LoginToken>' + 
                    '<act:Code>0</act:Code>' + 
                    '<act:Message />' +
                '</act:LoginResponse>' + 
            '</soapenv:Body>' +
        '</soapenv:Envelope>';
}