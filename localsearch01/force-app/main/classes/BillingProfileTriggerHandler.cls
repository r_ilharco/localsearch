/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-08-2019
 * Sprint      : 1
 * Work item   : 25
 * Testclass   : Test_BillingProfileTrigger
 * Package     : 
 * Description : Trigger Handler on Billing Profile
 * Changelog   : 
                #1: SF2-253 - Validate Account and Billing Address on Quote Creation
                    SF2-264 - Legal Address Validation
                    gcasola 07-22-2019
 */

public class BillingProfileTriggerHandler implements ITriggerHandler{
	public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        if(!System.isFuture())
		    BillingProfileTriggerHelper.addressValidation((List<Billing_Profile__c>) newItems);
    }
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        System.debug('trigger update');
        if(!System.isFuture())
            BillingProfileTriggerHelper.addressValidation((Map<Id, Billing_Profile__c>)newItems, (Map<Id, Billing_Profile__c>)oldItems);
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {
        
        System.debug('trigger insert');
        if(!System.isFuture())
            BillingProfileTriggerHelper.addressValidation((List<Billing_Profile__c>)newItems.values());
        
        List<Billing_Profile__c> bpToUpdate = new List<Billing_Profile__c>();
        Map<Id, Billing_Profile__c> bpOnDefault = new Map<Id, Billing_Profile__c>();

        for(Billing_Profile__c bp: (List<Billing_Profile__c>) newItems.values())
        {
            if(bp.Is_Default__c == true) 
            {
                bpOnDefault.put(bp.Id, bp);
            }
        }

        Map<Id, Billing_Profile__c> defaultBPs = BillingProfileSelector.getBPDefaultsNotIn(bpOnDefault);

        for(Billing_Profile__c bp : defaultBPs.values())
        {
            bp.Is_Default__c = false;
            bpToUpdate.add(bp);
        }

        update bpToUpdate;
        
        
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        
        List<Billing_Profile__c> bpToUpdate = new List<Billing_Profile__c>();
        Map<Id, Billing_Profile__c> bpOnDefault = new Map<Id, Billing_Profile__c>();

        for(Billing_Profile__c bp: (List<Billing_Profile__c>) newItems.values())
        {
            if(bp.Is_Default__c == true && (bp.Is_Default__c != ((Billing_Profile__c)oldItems.get(bp.Id)).Is_Default__c))
            {
                bpOnDefault.put(bp.Id, bp);
            }
        }

        Map<Id, Billing_Profile__c> defaultBPs = BillingProfileSelector.getBPDefaultsNotIn(bpOnDefault);

        for(Billing_Profile__c bp : defaultBPs.values())
        {
            bp.Is_Default__c = false;
            bpToUpdate.add(bp);
        }

        update bpToUpdate;
        
        
    }
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}