/**
* Author      : Mara Mazzarella <mamazzarella@deloitte.it>
* Date	      : 2020-27-11
* Sprint      : Sprint 8 - phase 5 
* Work item   : New Customer Flag
* Testclass   : SetAccountStatusBatch_Test
* Description : 
* Changelog   : SPIII-4307
*/

global class NewCustomerMergeAccount_Batch implements Database.batchable<sObject>, Schedulable{
    
    global void execute(SchedulableContext sc) {
        NewCustomerMergeAccount_Batch b = new NewCustomerMergeAccount_Batch(); 
        Database.executebatch(b, 50);
    }
    String quote = ConstantsUtil.QUOTE_TYPE_QUOTE;
    String typeNew = ConstantsUtil.ORDER_TYPE_NEW;
    String upgrade = ConstantsUtil.QUOTE_TYPE_UPGRADE;
    String replacement = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
    global List<Account> accountsToUpdate = new List<Account>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String wherecondition = Label.Query_NewCustomerMergeAccount_Batch;
        String query = 'SELECT Id, Status__c,  Active_DMC_Contracts__c, Activation_Date__c, Last_Inactive_Date__c,  Active_Contracts__c'+
            ' FROM Account WHERE id in (select accountid from AccountHistory '+wherecondition+')';
        
        /* return Database.getQueryLocator('SELECT Id, Status__c,  Active_DMC_Contracts__c, Activation_Date__c, Last_Inactive_Date__c,  Active_Contracts__c'+ 
        ' FROM Account WHERE Account.Activation_Date__c = null AND Id IN (SELECT AccountId FROM Opportunity '+
        ' WHERE StageName = \''+ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON+'\' AND ( Pricebook2.ExternalId__c in (\'PB3\',\'PB6\') or '+
        ' (SBQQ__PrimaryQuote__r.SBQQ__SalesRep__r.Profile.name = \'Sales\' and SBQQ__PrimaryQuote__r.Migrated__c = true) ) '+
        ' and SBQQ__PrimaryQuote__r.SBQQ__Type__c in (:quote,:upgrade,:replacement))');*/
        
        /*return Database.getQueryLocator('SELECT Id, Status__c,  Active_DMC_Contracts__c, Activation_Date__c, Last_Inactive_Date__c,  Active_Contracts__c'+ 
		' FROM Account WHERE id in (select accountid from AccountHistory where field = \'accountMerged\' and CreatedDate = today)');*/                                
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Account> scope){
        Set<Id> accToClean = new Set<Id>();
        for(Account a :scope){
            accToClean.add(a.Id);
        }
        AccountTriggerHandler.disableTrigger = true;
        AccountStatusUpdateScript.updateAccountStatus(accToClean);
        /* List<Order> orders =[select id, EffectiveDate, CreatedDate, Migrated__c, Type, Custom_Type__c, AccountId from order
		where AccountId in: accToClean
		and Type in (:typeNew,:upgrade,:replacement) and Custom_Type__c =:ConstantsUtil.ORDER_TYPE_ACTIVATION and 
        ( 
        Opportunity.Pricebook2.ExternalId__c in ('PB3','PB6') or 
        (SBQQ__Quote__r.SBQQ__SalesRep__r.Profile.name = 'Sales' and SBQQ__Quote__r.Migrated__c = true)
        ) 
        and SBQQ__Quote__r.SBQQ__Type__c in (:quote,:upgrade,:replacement)];*/
        
        List<String> validPricebookExternalIds = new List<String>{ConstantsUtil.DMC_PRICEBOOK, ConstantsUtil.ONEPRESENCE_PRICEBOOK,ConstantsUtil.IPROMOTE_PRICEBOOK };

        List<SBQQ__Quote__c> quotes =[select id,
                                      SignatureDate__c, Contract_Signed_Date__c,
                                      Migrated__c, SBQQ__Account__c, SBQQ__Account__r.Last_Inactive_Date__c 
                                      from SBQQ__Quote__c
                                      where SBQQ__Account__c in: accToClean
                                      and SBQQ__Type__C in (:quote,:upgrade,:replacement) and 
                                      ( 
                                          SBQQ__Opportunity2__r.Pricebook2.ExternalId__c IN :validPricebookExternalIds or 
                                          (SBQQ__SalesRep__r.Profile.name = 'Sales' and Migrated__c = true)
                                      ) 
                                      and (Contract_Signed_Date__c != null or SignatureDate__c != null)
                                     ];
        
        List<SBQQ__QuoteDocument__c> docs =[select id, 
                                            SignatureDate__c, 
                                            SBQQ__Quote__r.SBQQ__Account__c, 
                                            SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c
                                            from SBQQ__QuoteDocument__c
                                            where SBQQ__Quote__r.SBQQ__Account__c in: accToClean
                                            and SBQQ__Quote__r.SBQQ__Type__c in (:quote,:upgrade,:replacement) 
                                            and SBQQ__Quote__r.SBQQ__Opportunity2__r.Pricebook2.ExternalId__c IN :validPricebookExternalIds
                                            and SignatureDate__c != null
                                           ];
        Map<Id,Date> accToActivationDate = new Map<Id,Date>();
        /*  for(Order o : orders){
        if(accToActivationDate.containsKey(o.AccountId)){
        if(o.Migrated__c == true){
        if(accToActivationDate.get(o.AccountId) > o.EffectiveDate ){
        accToActivationDate.put(o.AccountId,o.EffectiveDate);
        }
        }
        else{
        if(accToActivationDate.get(o.AccountId) > Date.valueof(o.CreatedDate) ){
        accToActivationDate.put(o.AccountId,Date.valueof(o.CreatedDate));
        }
        }
        }
        else{
        if(o.Migrated__c == true){
        accToActivationDate.put(o.AccountId,o.EffectiveDate);
        }
        else{
        accToActivationDate.put(o.AccountId,Date.valueof(o.CreatedDate));
        }
        }
        }*/
        for(SBQQ__Quote__c quote : quotes){
            if(quote.SBQQ__Account__r.Last_Inactive_Date__c == null){
                if(accToActivationDate.containsKey(quote.SBQQ__Account__c)){
                    if(quote.Migrated__c == true){
                        if(accToActivationDate.get(quote.SBQQ__Account__c) > quote.Contract_Signed_Date__c ){
                            accToActivationDate.put(quote.SBQQ__Account__c,quote.Contract_Signed_Date__c);
                        }
                    }
                    else{
                        if(accToActivationDate.get(quote.SBQQ__Account__c) > Date.valueof(quote.SignatureDate__c) ){
                            accToActivationDate.put(quote.SBQQ__Account__c,Date.valueof(quote.SignatureDate__c));
                        }
                    }
                }
                else{
                    if(quote.Migrated__c == true){
                        accToActivationDate.put(quote.SBQQ__Account__c,quote.Contract_Signed_Date__c);
                    }
                    else{
                        accToActivationDate.put(quote.SBQQ__Account__c,Date.valueof(quote.SignatureDate__c));
                    }
                }
            }
            else{
                if(accToActivationDate.containsKey(quote.SBQQ__Account__c)){
                    if(quote.Migrated__c == true){
                        if((accToActivationDate.get(quote.SBQQ__Account__c) >  Date.valueof(quote.Contract_Signed_Date__c)
                            && accToActivationDate.get(quote.SBQQ__Account__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c
                            && Date.valueof(quote.Contract_Signed_Date__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c)
                           || ( Date.valueof(quote.Contract_Signed_Date__c) > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c
                               && accToActivationDate.get(quote.SBQQ__Account__c) <  Date.valueof(quote.Contract_Signed_Date__c))
                           || ( Date.valueof(quote.Contract_Signed_Date__c) > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c)> quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c) >  Date.valueof(quote.Contract_Signed_Date__c))
                          )
                            accToActivationDate.put(quote.SBQQ__Account__c,quote.Contract_Signed_Date__c);
                        
                    }
                    else{
                        if((accToActivationDate.get(quote.SBQQ__Account__c) >  Date.valueof(quote.SignatureDate__c)
                            && accToActivationDate.get(quote.SBQQ__Account__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c
                            && Date.valueof(quote.SignatureDate__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c)
                           || ( Date.valueof(quote.SignatureDate__c) > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c) < quote.SBQQ__Account__r.Last_Inactive_Date__c
                               && accToActivationDate.get(quote.SBQQ__Account__c) < Date.valueof(quote.SignatureDate__c))
                           || ( Date.valueof(quote.SignatureDate__c) > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c)> quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                               && accToActivationDate.get(quote.SBQQ__Account__c) >  Date.valueof(quote.SignatureDate__c))
                          )
                            accToActivationDate.put(quote.SBQQ__Account__c,Date.valueof(quote.SignatureDate__c));
                    }
                    
                }
                else{
                    if(quote.Migrated__c == true){
                        if(quote.Contract_Signed_Date__c <= quote.SBQQ__Account__r.Last_Inactive_Date__c || quote.Contract_Signed_Date__c > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12))
                            accToActivationDate.put(quote.SBQQ__Account__c,quote.Contract_Signed_Date__c);
                    }
                    else if(quote.SignatureDate__c <= quote.SBQQ__Account__r.Last_Inactive_Date__c || quote.SignatureDate__c > quote.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12))
                        accToActivationDate.put(quote.SBQQ__Account__c,Date.valueof(quote.SignatureDate__c));
                }
            }
        }
        for(SBQQ__QuoteDocument__c doc : docs){
            if(doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c == null){
                if(accToActivationDate.containsKey(doc.SBQQ__Quote__r.SBQQ__Account__c)){
                    if(accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) > Date.valueof(doc.SignatureDate__c) ){
                        accToActivationDate.put(doc.SBQQ__Quote__r.SBQQ__Account__c,Date.valueof(doc.SignatureDate__c));
                    }
                }
                else{
                    accToActivationDate.put(doc.SBQQ__Quote__r.SBQQ__Account__c,Date.valueof(doc.SignatureDate__c));
                }
            }
            else{
                if(accToActivationDate.containsKey(doc.SBQQ__Quote__r.SBQQ__Account__c)){
                    if((accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) >  Date.valueof(doc.SignatureDate__c)
                        && accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) < doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c
                        && Date.valueof(doc.SignatureDate__c) < doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c)
                       || ( Date.valueof(doc.SignatureDate__c) > doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                           && accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) < doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c
                           && accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) <  Date.valueof(doc.SignatureDate__c))
                       || ( Date.valueof(doc.SignatureDate__c) > doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                           && accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c)> doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12)
                           && accToActivationDate.get(doc.SBQQ__Quote__r.SBQQ__Account__c) >  Date.valueof(doc.SignatureDate__c))
                      ){
                          accToActivationDate.put(doc.SBQQ__Quote__r.SBQQ__Account__c,Date.valueof(doc.SignatureDate__c));
                      }
                }
                else if(doc.SignatureDate__c <doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c || doc.SignatureDate__c >= doc.SBQQ__Quote__r.SBQQ__Account__r.Last_Inactive_Date__c.addMonths(12))
                    accToActivationDate.put(doc.SBQQ__Quote__r.SBQQ__Account__c,Date.valueof(doc.SignatureDate__c));
            }
        }
        
        List<Account> acc = [select id, Activation_Date__c from Account where id in : accToActivationDate.keyset()];
        for(Account a : acc){
            a.Activation_Date__c = accToActivationDate.get(a.Id);
        }        
        
        String log = '';
        List<Database.SaveResult> resultsAccount = Database.update(acc, false);
        AccountTriggerHandler.disableTrigger = false;
        for (Database.SaveResult sr : resultsAccount) {
            if (!sr.isSuccess()) {              
                for(Database.Error err : sr.getErrors()) {          
                    log +=err.getStatusCode() + ': ' + err.getMessage()+' - '+ err.getFields() + 'id: '+sr.getId();
                }
            }
        }
        if(log != ''){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'NewCustomerMergeAccount_Batch', 'NewCustomerMergeAccount_Batch.updateAccount', null, ErrorHandler.ErrorCode.E_DML_FAILED, log, null, null, null, null, false);
        }
    }
    
    
    global void finish(Database.BatchableContext info){
        Map<Id,Date> dateToOrders = new Map<Id,Date>();
        List<Order> orders = [select id, Migrated__c, New_Customer__c, Account.Activation_Date__c, Opportunity.Pricebook2.name, CreatedDate, EffectiveDate,
                              Type, Custom_Type__c from order where CreatedDate >= LAST_N_MONTHS:3 and Migrated__c = false 
                              and ((Account.New_Customer__c = true and New_Customer__c = false) or (Account.New_Customer__c = false and New_Customer__c = true)) ];
        for (Order o : orders){
            if(Date.valueof(o.CreatedDate) >= o.Account.Activation_Date__c && Date.valueof(o.CreatedDate)< o.Account.Activation_Date__c.addMonths(3))
                o.New_Customer__c = true;
            else
                o.New_Customer__c = false;                    
        }
        
        String log = '';
        OrderTriggerHandler.disableTrigger = true;
        List<Database.SaveResult> resultsOrder = Database.update(orders, false);
        OrderTriggerHandler.disableTrigger = false;
        for (Database.SaveResult sr : resultsOrder) {
            if (!sr.isSuccess()) {              
                for(Database.Error err : sr.getErrors()) {          
                    log +=err.getStatusCode() + ': ' + err.getMessage()+' - '+ err.getFields() + 'id: '+sr.getId();
                }
            }
        }
        if(log != ''){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'NewCustomerMergeAccount_Batch', 'NewCustomerMergeAccount_Batch.updateOrder', null, ErrorHandler.ErrorCode.E_DML_FAILED, log, null, null, null, null, false);
        }
    }
}