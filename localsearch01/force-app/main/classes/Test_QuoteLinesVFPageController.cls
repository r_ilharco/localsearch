@istest public class Test_QuoteLinesVFPageController {
    static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Update Contract Company Sign Info';
            insert byPassTrigger;
    }
    

@testSetup
    public static void test_setupData(){
        insertBypassFlowNames();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;
        SBQQ.TriggerControl.disable();
        createVatSchedules();
        
        Editorial_Plan__c ep = new Editorial_Plan__c();
        ep.Local_Guide_Number__c = 120;
        ep.Local_Guide_Main_Title__c = 'Local_Guide_Main_Title__c';
        ep.Local_Guide_Title_2__c = 'Local_Guide_Title_2__c';
        ep.Local_Guide_Title_3__c = 'Local_Guide_Title_3__c';
        insert ep;
        
        Swisslist_CPQ.config();
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        
        productCodeToProduct.get('LBB001').SBQQ__NonDiscountable__c = false;
        productCodeToProduct.get('OPMBASICLOW001').SBQQ__NonDiscountable__c = false;
        productCodeToProduct.get('OPMBASICLOW001').PlaceIDRequired__c = 'No';
        productCodeToProduct.get('LBB001').ExternalKey_ProductCode__c = 'LGC-SPA';
        productCodeToProduct.get('LBB001').ProductCode = 'LGC-SPA';
        update productCodeToProduct.get('OPMBASICLOW001');
        update productCodeToProduct.get('LBB001');
        
        SBQQ__ProductOption__c po = new SBQQ__ProductOption__c();
        po.SBQQ__Number__c = 1;
        insert po;
        
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity Name', account.Id);
        insert opportunity;
        String opportunityId = opportunity.Id;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        quote.SBQQ__SalesRep__c = UserInfo.getUserId();
        quote.SBQQ__Opportunity2__c = opportunity.Id;
        update quote;
        
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id = :productCodeToProduct.get('LBB001').Id];
        List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',1);
        places[0].Account__c = Account.id;  
        places[0].PlaceID__c = 'ZpP8fTVhKmMOLps8FiDAxQ';
        insert places;
        
        SBQQ__QuoteLine__c fatherQuoteLineWithPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        fatherQuoteLineWithPlace.TrialStartDate__c = date.today();
        fatherQuoteLineWithPlace.SBQQ__Product__c = products[0].id;
        fatherQuoteLineWithPlace.TrialEndDate__c = date.today().adddays(30);
        fatherQuoteLineWithPlace.place__c = places[0].id;
        fatherQuoteLineWithPlace.Package_Total__c = 2.50;
        fatherQuoteLineWithPlace.Edition__c = ep.Id;
        fatherQuoteLineWithPlace.Location__c = 'Location__c';
        fatherQuoteLineWithPlace.Category__c = 'Category__c';
        fatherQuoteLineWithPlace.Total__c = 2000;
        fatherQuoteLineWithPlace.DocRowNumber__c = 1;
        insert fatherQuoteLineWithPlace;
        fatherQuoteLineWithPlace.SBQQ__AdditionalDiscountAmount__c = 10;
        update fatherQuoteLineWithPlace;
        SBQQ__QuoteLine__c childQuoteLineWithPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('LBB001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        childQuoteLineWithPlace.SBQQ__RequiredBy__c = fatherQuoteLineWithPlace.id;
        childQuoteLineWithPlace.Package_Total__c = 2.50;
        childQuoteLineWithPlace.Total__c = 2000;
        childQuoteLineWithPlace.DocRowNumber__c = 2;
        childQuoteLineWithPlace.Subscription_Term__c = '12';
        //childQuoteLineWithPlace.SBQQ__ProductOption__c = po.Id;
        insert childQuoteLineWithPlace;
        childQuoteLineWithPlace.SBQQ__Discount__c = 10;
        update childQuoteLineWithPlace;
        
        SBQQ__QuoteLine__c fatherQuoteLineWithoutPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OPMBASICLOW001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        fatherQuoteLineWithoutPlace.TrialStartDate__c = date.today();
        fatherQuoteLineWithoutPlace.SBQQ__Product__c = products[0].id;
        fatherQuoteLineWithoutPlace.TrialEndDate__c = date.today().adddays(30);
        fatherQuoteLineWithoutPlace.place__c = places[0].id;
        fatherQuoteLineWithoutPlace.Package_Total__c = 2.50;
        //fatherQuoteLineWithoutPlace.Subscription_To_Terminate__c = subs[0].Id;
        fatherQuoteLineWithoutPlace.SBQQ__ProductOption__c = NULL;
        fatherQuoteLineWithoutPlace.Total__c = 2000;
        fatherQuoteLineWithoutPlace.Subscription_Term__c = '36';
        fatherQuoteLineWithoutPlace.Place__c = NULL;
        fatherQuoteLineWithoutPlace.DocRowNumber__c = 3;
        insert fatherQuoteLineWithoutPlace;
        fatherQuoteLineWithoutPlace.SBQQ__AdditionalDiscountAmount__c = 10;
        update fatherQuoteLineWithoutPlace;
        SBQQ__QuoteLine__c childQuoteLineWithoutPlace = Test_DataFactory.generateQuoteLine_final(quote.Id, productCodeToProduct.get('OPMBASICLOW001').Id, Date.today(), null, null, null, 'Annual', pbe.Id);
        childQuoteLineWithoutPlace.SBQQ__RequiredBy__c = fatherQuoteLineWithoutPlace.id;
        childQuoteLineWithoutPlace.Package_Total__c = 2.50;
        childQuoteLineWithoutPlace.Total__c = 2000;
        childQuoteLineWithoutPlace.Place__c = NULL;
        childQuoteLineWithoutPlace.Subscription_Term__c = NULL;
        childQuoteLineWithoutPlace.DocRowNumber__c = 4;
        //childQuoteLineWithoutPlace.SBQQ__ProductOption__c = po.Id;
        insert childQuoteLineWithoutPlace;
        childQuoteLineWithoutPlace.SBQQ__Discount__c = 10;
        update childQuoteLineWithoutPlace;
        
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        Contract cntrct = Test_DataFactory.createContracts (Account.Id, 1)[0];
        cntrct.SBQQ__Order__c = order.Id;
        insert cntrct;
        List<SBQQ__Subscription__c> subs = test_datafactory.createSubscriptionsWithPlace('Active',places[0].id,cntrct.id,1);
        subs[0].SBQQ__QuoteLine__c = fatherQuoteLineWithPlace.Id;
        subs[0].SBQQ__Product__c = productCodeToProduct.get('LBB001').Id;
        subs[0].SBQQ__SubscriptionStartDate__c	 = Date.today().addMonths(-13);
        subs[0].SBQQ__Account__c = Account.Id;
        subs[0].Total__c = 500;
        subs[0].Subscription_Term__c = '12';
        insert subs;
        
        fatherQuoteLineWithPlace.Subscription_To_Terminate__c = subs[0].Id;
        update fatherQuoteLineWithPlace;
        
        List<OpportunityLineItem> opplineitems = new List<OpportunityLineItem>();
       	OpportunityLineItem opplineitem = new OpportunityLineItem();
        opplineitem.OpportunityId = opportunity.Id;
        opplineitem.SBQQ__QuoteLine__c = childQuoteLineWithPlace.Id;
        opplineitem.Quantity = 1;
        opplineitem.TotalPrice = 10;
        opplineitem.PricebookEntryId = pbe.Id;
        opplineitems.add(opplineitem);
        /*OpportunityLineItem opplineitem2 = new OpportunityLineItem();
        opplineitem2.OpportunityId = opportunity.Id;
        opplineitem2.SBQQ__QuoteLine__c = fatherQuoteLineWithoutPlace.Id;
        opplineitem2.Quantity = 1;
        opplineitem2.TotalPrice = 10;
        opplineitem2.PricebookEntryId = pbe.Id;
        opplineitems.add(opplineitem2);*/
        
        insert opplineitems;
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
		QuoteLineTriggerHandler.disableTrigger = false;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'DMC Template 2020 v1.0 - IT';
        template.SBQQ__TermsConditions__c = 'Die Preise werden nach Aufschaltung bzw. Publikation des jeweiligen Produkts monatlich bzw. jährlich in Rechnung gestellt. Einmalige Gebühren werden nach Vertragsabschluss in Rechnung gestellt.Es gelten die jeweils aktuellen Produktbeschreibungen auf www.localsearch.ch';
        insert template;
    }

    @istest static void test_QuoteLinesVFPageController_IT(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_ADDITIONALDISCOUNT(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        List<SBQQ__QuoteLine__c> quotelines = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        SBQQ.TriggerControl.disable();
        for(SBQQ__QuoteLine__c quoteLine : quotelines){
            quoteLine.SBQQ__AdditionalDiscountAmount__c = 10;
            quoteLine.SBQQ__Discount__c = NULL;
        }
        update quotelines;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_QLDISCOUNT(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        List<SBQQ__QuoteLine__c> quotelines = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        SBQQ.TriggerControl.disable();
        for(SBQQ__QuoteLine__c quoteLine : quotelines){
            quoteLine.SBQQ__AdditionalDiscountAmount__c = NULL;
            quoteLine.SBQQ__Discount__c = 10;
        }
        update quotelines;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_AMENDMENT(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ.TriggerControl.disable();
            quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_AMENDMENT;
            update quote;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_UPGRADE_REPLACEMENT_MIGRATED(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        Contract contract = [SELECT Id, SBQQ__Order__c FROM Contract LIMIT 1];
		Order o = [SELECT Migrated__c FROM Order WHERE Id = :contract.SBQQ__Order__c];        
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c = :contract.Id];
        SBQQ.TriggerControl.disable();
            quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
            update quote;
        	
        	o.ContractSAMBA_Key__c = 'ContractSAMBA_Key__c';
        	update o;
        	//contract.ContractMigration__c = true;
        	//update contract;
        	sub.Contract_Ref_Id__c = '111.1111.1111';
        	update sub;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_UPGRADE_REPLACEMENT_NOMIGRATED(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        Contract contract = [SELECT Id, SBQQ__Order__c FROM Contract LIMIT 1];
		Order o = [SELECT Migrated__c FROM Order WHERE Id = :contract.SBQQ__Order__c]; 
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c = :contract.Id];
        SBQQ.TriggerControl.disable();
            quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
            update quote;

			o.ContractSAMBA_Key__c = NULL;
        	update o;        
        	//contract.ContractMigration__c = false;
        	//update contract;
        	sub.Contract_Ref_Id__c = '111.1111.1111';
        	update sub;
        SBQQ.TriggerControl.enable();
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_IT_QuoteDiscounted(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ.TriggerControl.disable();
            quote.SBQQ__CustomerDiscount__c = 10;
            update quote;
        SBQQ.TriggerControl.enable();
        
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - IT';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'it');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'it');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_DE(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - DE';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'de');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'de');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_FR(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - FR';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'fr');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'fr');
        Test.stopTest();
	}
    
    @istest static void test_QuoteLinesVFPageController_EN(){
		SBQQ__Quote__c quote = [select id from SBQQ__Quote__c limit 1];
        SBQQ__QuoteTemplate__c template = [select id from SBQQ__QuoteTemplate__c limit 1];
        template.Name = 'DMC Template 2020 v1.0 - EN';
        update template;
        Test.startTest();
            apexpages.currentpage().getparameters().put('qid' , quote.id);
            apexpages.currentpage().getparameters().put('tid' , template.id);
            apexpages.currentpage().getparameters().put('language' , 'en_US');
            QuoteLinesVFPageController tFVFPC = new QuoteLinesVFPageController();
        	QuoteLinesVFPageController tFVFPC2 = new QuoteLinesVFPageController(quote.id,template.id,'en_US');
        Test.stopTest();
	}
    
    private static void createVatSchedules() {
        insert new List<Vat_Schedule__c> {
            new Vat_Schedule__c(Tax_Code__c = 'Standard', Start_Date__c=null, End_Date__c=Date.newInstance(2020, 2, 15), Value__c=7.7, Name='Standard 7.7%'),
            new Vat_Schedule__c(Tax_Code__c = 'Standard', Start_Date__c=Date.newInstance(2020, 2, 16), End_Date__c=null, Value__c=7.9, Name='Standard 7.9%'),
            new Vat_Schedule__c(Tax_Code__c = 'Reduced', Start_Date__c=null, End_Date__c=null, Value__c=2.5, Name='Reduced 2.5%')
        };
    }

}
