@isTest
public class TestDataBank {
    
    public static void createProducts(Integer num) {
        List<Product2> listProds = new List<Product2>();
        for(Integer i = 0; i < num; i++) {
         Product2 pr = new Product2();
           pr.Name = 'XXX ' + i;
           pr.ProductCode = 'SLT00'+i;
           pr.IsActive=True;
           pr.SBQQ__ConfigurationType__c='Allowed';
           pr.SBQQ__ConfigurationEvent__c='Always';
           pr.SBQQ__QuantityEditable__c=True;
            
           pr.SBQQ__NonDiscountable__c=False;
           pr.SBQQ__SubscriptionType__c='Renewable'; 
           pr.SBQQ__SubscriptionPricing__c='Fixed Price';
           pr.SBQQ__SubscriptionTerm__c=12; 
           
            listProds.add(pr);
        }
		insert listProds;
    }
    //@isTest
    public static Product2 createMainProduct() {        
        createProducts(10);
        Product2 p = [SELECT Id, Name FROM Product2 WHERE ProductCode = 'SLT001' Limit 1];
        return p;
    }
	
	public static List<Product2> createOptionProducts(Id idMainProduct) {
        //List<Product2> subProds = [SELECT Id, Name FROM Product2 WHERE Id IN (SELECT SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :idMainProduct)];
        List<Product2> subProds = [SELECT Id, Name FROM Product2 WHERE ProductCode != 'SLT001' Limit 5];
        return subProds;
    }
    
    public static SBQQ__Quote__c createQuote() {
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Type__c = 'Quote', SBQQ__Status__c = 'Draft');
        insert quote;
        return quote;
    }
    
    public static List<SBQQ__QuoteLine__c> createQuoteLine(Decimal qty, Boolean withPlace, Boolean withSubscription) {
        SBQQ__Quote__c quote = createQuote();
        Product2 p = createMainProduct();
        SBQQ__QuoteLine__c mainQuoteLine = new SBQQ__QuoteLine__c(
            SBQQ__Quote__c=quote.Id, SBQQ__Product__c=p.Id, SBQQ__Quantity__c=qty
        );
        if(withPlace) {
            Place__c place = createPlace();
            mainQuoteLine.Place__c = place.Id;
        }
        insert mainQuoteLine;
        List<Product2> subProds = createOptionProducts(p.Id);
        List<SBQQ__QuoteLine__c> listQLines = new List<SBQQ__QuoteLine__c>();
        for(Product2 subProd : subProds) {
            listQLines.add(new SBQQ__QuoteLine__c(
                SBQQ__Quote__c=quote.Id, 
                SBQQ__Product__c=subProd.Id, 
                SBQQ__RequiredBy__c=mainQuoteLine.Id,
                SBQQ__Bundled__c = true,
                SBQQ__OptionType__c = 'Component'
            ));
        }
        if(!listQLines.isEmpty()) insert listQLines;
        listQLines.add(mainQuoteLine);
        return listQLines;
    }
    
    public static Place__c createPlace () {
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'))));
        Place__c  thePlace = new Place__c ();
           thePlace.LastName__c  = 'XXX0123';
           thePlace.GoldenRecordID__c = 'GRIDTest';
           thePlace.City__c = 'dh';
           thePlace.State__c  = 'bd';
           thePlace.Company__c  = 'ITL';
           thePlace.Street__c  = 'utt';
           thePlace.PostalCode__c  = '1230';
           thePlace.Country__c  = 'bd';  
           thePlace.PlaceID__c = EncodingUtil.convertToHex(hash);
        //insert thePlace;
		//Place__c thePlace = new Place__c(GoldenRecordID__c = 'GRIDTest', City__c ='Test City', PlaceID__c =EncodingUtil.convertToHex(hash));
        insert thePlace;
        return thePlace;
    } 
    
}