@isTest
public class PlaceTriggerHandlerTest {
static void insertBypassFlowNames(){
        ByPassFlow__c byPassTrigger = new ByPassFlow__c();
        byPassTrigger.Name = Userinfo.getProfileId();
        byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
        insert byPassTrigger;
}
    
    @isTest
    public static void testPlaceTriggerHandler(){
        insertBypassFlowNames();
        bypass_triggers__c bpt = new bypass_triggers__c();
         bpt.Trigger_Name__c = 'testbp';
        insert bpt;
        try{
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = null;
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';           
            insert place;
            System.debug('Place: '+ place);
            
            List<Place__c> placeList = new List<Place__c>();
            placeList.add(place);
            
            PlaceTriggerHandler placeTrigHandler = new PlaceTriggerHandler(false, 5);

            Set<ID> newPlaceID = new Set<ID>();
            newPlaceID.add(place.Id);
            
            PlaceTriggerHandler.OnAfterUpdateAsync(newPlaceID);
            boolean isTriggerContext = placeTrigHandler.IsTriggerContext;
            boolean isVisualforcePageContext = placeTrigHandler.IsVisualforcePageContext;
            boolean isWebServiceContext = placeTrigHandler.IsWebServiceContext;
            boolean isExecuteAnonymousContext = placeTrigHandler.IsExecuteAnonymousContext;

           boolean isdisabled = placetrighandler.IsDisabled();
           isdisabled = true;
          
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }
        
    }

@isTest
    public static void testPlaceTriggerHandler2(){
        insertBypassFlowNames();
        bypass_triggers__c bpt = new bypass_triggers__c();
        insert bpt;
        try{
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = null;
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';           
            insert place;
            System.debug('Place: '+ place);
            
            List<Place__c> placeList = new List<Place__c>();
            placeList.add(place);
            
            PlaceTriggerHandler placeTrigHandler = new PlaceTriggerHandler(false, 5);

            Set<ID> newPlaceID = new Set<ID>();
            newPlaceID.add(place.Id);
            
            PlaceTriggerHandler.OnAfterUpdateAsync(newPlaceID);
            boolean isTriggerContext = placeTrigHandler.IsTriggerContext;
            boolean isVisualforcePageContext = placeTrigHandler.IsVisualforcePageContext;
            boolean isWebServiceContext = placeTrigHandler.IsWebServiceContext;
            boolean isExecuteAnonymousContext = placeTrigHandler.IsExecuteAnonymousContext;

           boolean isdisabled = placetrighandler.IsDisabled();
           isdisabled = true;
          
        }catch(Exception e){	                            
            System.Assert(false, e.getMessage());
        }
        
    }

}