@isTest
public class OpportunityAutomaticalClose_test {
 	
    @testSetup
    public static void setupData(){
        
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Account Status Management,Send Owner Notification,Community User Creation,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        List<Account> accounts = new List<Account>();
		List<Contact> contacts = new List<Contact>();
		
		accounts.add(account);
		contacts.add(contact);
		
		List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accounts, contacts, 1);
		insert bp;
        
        Place__c place = Test_DataFactory.generatePlaces(account.Id, 'Place Name Test');
        insert place;
        
        Test_DataFactory.insertFutureUsers();
        
        Territory2 terr = [Select Id, developerName FROM Territory2 WHERE Name LIKE '%2.%' Limit 1];
        ObjectTerritory2Association oTa = new ObjectTerritory2Association();
        oTa.ObjectId = account.Id;
        oTa.Territory2Id = terr.Id;
        oTa.AssociationCause = 'Territory2Manual';
        insert oTa;  
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
    }
    
	
   	@isTest
    public static void test_closeOpportunity(){
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<User> u =  Test_DataFactory.createUsers(ConstantsUtil.SysAdmin,1);
        u[0].email = 'joao.soares@localsearch.com';
        insert u;
        User updatedUser = [SELECT lastName,LanguageLocaleKey,email FROM user WHERE Id= :u[0].id];
       
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        List<String> fieldNamesAccount = new List<String>(Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet());
        String queryAccount =' SELECT ' + String.join( fieldNamesAccount, ',' ) +' FROM Account LIMIT 1';
        Account account = Database.query(queryAccount);
        
        Billing_Profile__c billingProfile = [SELECT Id FROM Billing_Profile__c WHERE Customer__c = :account.Id LIMIT 1];
        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id LIMIT 1];
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        
        insert opportunity;
        
        SBQQ__Quote__c quote = [SELECT Id,Name,SBQQ__SalesRep__r.Email,Replacement__c,SBQQ__Opportunity2__c,SBQQ__Account__c FROM SBQQ__Quote__c Where SBQQ__Opportunity2__c = :opportunity.Id LIMIT 1];
       	opportunity.SBQQ__PrimaryQuote__c = quote.Id;
        update opportunity;
        quote.SBQQ__SalesRep__c = updatedUser.id;
        update quote;
        
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        
       
        SBQQ__QuoteDocument__c quoteDocument1 = Test_DataFactory.generateQuoteDocument(quote.Id);
        insert quoteDocument1;

       	
        
        List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>{quote};
      
        test.startTest();
         List<String> fillers = new List<String>{quote.Name};
        OpportunityAutomaticalClose.closeOpportunity(quotes);
        SBQQ__Quote__c qt = [SELECT Id,Name,SBQQ__SalesRep__r.Email,Replacement__c,SBQQ__Opportunity2__c,SBQQ__Account__c FROM SBQQ__Quote__c Where Id =:quote.Id LIMIT 1];
        OpportunityAutomaticalClose.sendEmail(Label.Substitution_Process_Approval,String.format(Label.Approval_Replacement_Missing_Steps, fillers),qt);
        test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
     public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Product2 generteChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}