@isTest
public class Test_NewQuoteController {
    @testSetup
    public static void testSetup(){
        Test_DataFactory.createUsers('Sales',2);
        User dmcUser = [SELECT Id, Code__c, Name FROM User WHERE profile.Name= 'Sales' and IsActive = true LIMIT 1];
        dmcUser.Code__c = ConstantsUtil.ROLE_IN_TERRITORY_DMC;
        update dmcUser;
        User smaUser = [SELECT Id, Code__c, Name FROM User WHERE profile.Name= 'Sales' and IsActive = true and Code__c = '' LIMIT 1];
        smaUser.Code__c = ConstantsUtil.ROLE_IN_TERRITORY_SMA;
        update smaUser;
        User admin = [SELECT Id, Code__c, Name FROM User WHERE Id=:Userinfo.getUserId() LIMIT 1];
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;	
        OpportunityTriggerHandler.disableTrigger = true;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        
        system.runAs(admin){
            ByPassFlow__c byPassFlow = new ByPassFlow__c();
            byPassFlow.Name = Userinfo.getProfileId();
            byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Order Management Insert Only,Order Management, Opportunity Handler, Opportunity Management';
            insert byPassFlow; 
            
            
            insert account;
            String accId = account.Id;
            List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
            String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
            List<Billing_Profile__c> bps = Database.query(queryBP); 
            Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
            contact.Primary__c = true;
            insert contact;
            account.OwnerId = dmcUser.Id;
            update account;
        }
        
        
        
        
        
        System.runAs(dmcUser){
            Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
            insert opportunity;
        }
        system.runAs(admin){
            account.OwnerId = dmcUser.Id;
            System.debug('Name '+dmcUser.Name);
            update account;
        }
        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        
    }
    
    public static testmethod void createQuoteasDMC(){
        test.startTest();
        Opportunity opp = [SELECT Id, AccountId, StageName, Account.Assigned_Territory__c, Account.OwnerId  FROM Opportunity LIMIT 1];
        User dmcUser = [SELECT Id, Name FROM User 
                        WHERE id =: opp.Account.OwnerId 
                        and Profile.Name = 'Sales' 
                        and IsActive = true 
                        and Code__c =: ConstantsUtil.ROLE_IN_TERRITORY_DMC LIMIT 1];
        Account account = [select id,OwnerId from Account limit 1];
        User smaUser = [SELECT Id, Name FROM User WHERE Profile.Name = 'Sales' 
                        and Code__c =: ConstantsUtil.ROLE_IN_TERRITORY_SMA and IsActive = true LIMIT 1];
        account.OwnerId = smaUser.Id;
        update account;
        QuoteTriggerHandler.disableTrigger = true;
        System.runAs(dmcUser){
            NewQuoteController.createQuote(opp.id);
        }
        
        QuoteTriggerHandler.disableTrigger = false;
        test.stopTest();
    }
    
    public static testmethod void createQuoteasSMA(){
        test.startTest();
        Opportunity opp = [SELECT Id, AccountId, StageName, Account.Assigned_Territory__c, Account.OwnerId  FROM Opportunity LIMIT 1];
        Account account = [select id,OwnerId from Account limit 1];
        User smaUser = [SELECT Id, Name FROM User WHERE Profile.Name = 'Sales' 
                        and Code__c =: ConstantsUtil.ROLE_IN_TERRITORY_SMA and IsActive = true LIMIT 1];
        account.OwnerId = smaUser.Id;
        opp.OwnerId =smaUser.Id;
        update opp;
        update account;
        QuoteTriggerHandler.disableTrigger = true;
        System.runAs(smaUser){
            
            NewQuoteController.createQuote(opp.id);
        }
        
        QuoteTriggerHandler.disableTrigger = false;
        test.stopTest();
    }
    public static testmethod void createQuoteasAdmin(){
        test.startTest();
        OpportunityTriggerHandler.disableTrigger = true;
        Opportunity opp = [SELECT Id, AccountId, StageName, Account.Assigned_Territory__c, Account.OwnerId  FROM Opportunity LIMIT 1];
        opp.Type = ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS;
        update opp;
        QuoteTriggerHandler.disableTrigger = true;
        
        NewQuoteController.createQuote(opp.id);
        
        opp.StageName = ConstantsUtil.Quote_Status_Accepted;
        NewQuoteController.createQuote(opp.id);
        
        opp.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        NewQuoteController.createQuote(opp.id);
        
        
        
        QuoteTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        test.stopTest();
    }
}