/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-10-2019
 * Sprint      : 3
 * Work item   : SF2-22 IH: Contract cancellation
 * Testclass   :
 * Package     : 
 * Description : Service Class for Invoice__c
 * Changelog   : 
 */

public class SRV_Invoice {
    @future(callout=true)
    public static void cancelInvoices(Set<Id> ids){
        for(Invoice__c invoice : SEL_Invoices.getInvoicesById(ids).values()){
			SwissbillingHelper.cancelInvoice(invoice);
        }
    }
    
    public static Map<Id, Decimal> remainingAmountCalculation(Set<Id> subscriptionIds){
        Map<Id,SBQQ__Subscription__c> subs = new Map<Id, SBQQ__Subscription__c>([SELECT Id, Next_Invoice_Date__c, End_Date__c, Total__c, SBQQ__StartDate__c, SBQQ__RequiredById__c, SBQQ__Product__r.One_time_Fee__c, One_time_Fee_Billed__c
                                                                                 FROM SBQQ__Subscription__c 
                                                                                 WHERE (Id IN :subscriptionIds OR SBQQ__RequiredById__c IN :subscriptionIds) AND Total__c != 0 
                                                                                 ORDER by SBQQ__RequiredById__c nulls first]);
        system.debug('subscriptionIds: ' + subscriptionIds);
        Map<Id, List<SBQQ__Subscription__c>> subFatherSonsMap = new Map<Id, List<SBQQ__Subscription__c>>();
        Map<Id, Decimal> subAmountMap = new Map<Id, Decimal>();
        if(subs.values().size() > 0){
            for(Id subId : subs.keySet()){
                system.debug('subId: ' + subId);
                Decimal priceByDay = 0 ;
                Decimal totalAmount = 0;
                Integer totalDays = 0;
                Integer daysToBill = 0;
                if(subs.get(subId).Next_Invoice_Date__c == null || (subs.get(subId).SBQQ__Product__r.One_time_Fee__c && !subs.get(subId).One_time_Fee_Billed__c)){
                    totalAmount = subs.get(subId).Total__c;
                }else if(subs.get(subId).End_Date__c != null && subs.get(subId).SBQQ__StartDate__c != null){
                    totalDays = subs.get(subId).SBQQ__StartDate__c.daysBetween(subs.get(subId).End_Date__c);
                    system.debug('totalDays: ' + totalDays);
                    priceByDay = ((Decimal)subs.get(subId).Total__c)/((Decimal)totalDays);
                    system.debug('priceByDay: ' + priceByDay);
                    if(subs.get(subId).Next_Invoice_Date__c != null && subs.get(subId).Next_Invoice_Date__c.daysBetween(subs.get(subId).End_Date__c) > 0){
                        daysToBill = subs.get(subId).Next_Invoice_Date__c.daysBetween(subs.get(subId).End_Date__c);
                        system.debug('daysToBill: ' + daysToBill);
                        totalAmount = ((Decimal)daysToBill) * priceByDay;
                        system.debug('totalAmount: ' + totalAmount);
                    }
                }
                system.debug('totalAmount: ' + totalAmount);
                if(totalAmount > 0){
                    system.debug('pre subAmountMap: ' + subAmountMap);
                    if(!subAmountMap.containsKey(subId)){
                        subAmountMap.put(subId, totalAmount);
                    }else{
                        subAmountMap.put(subId,subAmountMap.get(subId) + totalAmount);
                    }
                }
                system.debug('post subAmountMap: ' + subAmountMap);
            }
        }
        return subAmountMap;
    }
}
