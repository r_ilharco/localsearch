/*
 * Author      : Mara Mazzarella <mamazzarella@deloitte.it>
 * Date	       : 2021-01-20
 * Sprint      : Sprint 11
 * Work item   : SPIII-4046 - Revenue Amount not calculated on all Accounts
 * Testclass   : SetRevenueAmount_batch_Test
 */

global without sharing class SetRevenueAmount_batch implements Database.Batchable<sObject>, Schedulable{
    global List<String> staticValues = new List<String>();

    global SetRevenueAmount_batch(List<String> val){
        this.staticValues = val;
    }
	global void execute(SchedulableContext sc) {
    	SetRevenueAmount_batch a = new SetRevenueAmount_batch(this.staticValues); 
    	Database.executeBatch(a, 200);
   	}
    global Database.QueryLocator start(Database.BatchableContext bc){    
    
       return Database.getQueryLocator('select id, Subsctiption_Status__c, SBQQ__Account__c,Total__c, SBQQ__Account__r.Revenue_Amount__c' +
                                       ' from SBQQ__Subscription__c where Total__c != null and Total__c >0'+
                                       ' and (Subsctiption_Status__c = \'' + ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE + '\' or Subsctiption_Status__c = \'' + ConstantsUtil.SUBSCRIPTION_STATUS_EXPIRED + '\')'+
                                       ' and SBQQ__Account__r.Cluster_Id__c in:  staticValues'+
                                       ' order by SBQQ__Account__c, Subsctiption_Status__c');
   	}
    
    global void execute(Database.BatchableContext info, List<SBQQ__Subscription__c> scope){
        try{
			Map<Id,Double> mapAccToTotal = new Map<Id,Double>();
			Double accRevenue =0;		
            for(SBQQ__Subscription__c sub: scope){ 
                if(mapAccToTotal.containsKey(sub.SBQQ__Account__c)){
                    accRevenue = mapAccToTotal.get(sub.SBQQ__Account__c);
                    accRevenue+=sub.Total__c;
                }
                else{
                    accRevenue=sub.Total__c;
                }
                mapAccToTotal.put(sub.SBQQ__Account__c,accRevenue);
            }
            List<Account> accToUpdate = new List<Account>();
            accToUpdate = [select id, Revenue_Amount__c from Account where id in:mapAccToTotal.keyset()];
            for(Account acc: accToUpdate){
                acc.Revenue_Amount__c = mapAccToTotal.get(acc.Id);
            }
            String log = '';
            AccountTriggerHandler.disableTrigger = true;
            List<Database.SaveResult> resultsAccount = Database.update(accToUpdate, false);
            AccountTriggerHandler.disableTrigger = false;
			for (Database.SaveResult sr : resultsAccount) {
                if (!sr.isSuccess()) {              
                    for(Database.Error err : sr.getErrors()) {
                        log += err.getMessage()+ ' ';
                    }
                }
            }
            if(log != '')
                ErrorHandler.log(System.LoggingLevel.ERROR, 'SetRevenueAmount_batch', 'SetRevenueAmount_batch.execute', null, ErrorHandler.ErrorCode.E_DML_FAILED, log, null, null, null, null, false);
        }
        catch(Exception e){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'SetRevenueAmount_batch', 'SetRevenueAmount_batch.execute', e, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);
        }
    }
    global void finish(Database.BatchableContext context) {}
}