@isTest
public class SendPDFController_test {
     @TestSetup
    static void initData(){
        List<Account> acc = Test_DataFactory.generateAccounts('Test Name', 1, false);
       /*   Account acc = new Account(
            Name='TestAccount',
            Billing_City__c = 'aTest1',
            Billing_Country__c = 'aTest1',
            Billing_Zip_PostalCode__c = '1001',
            P_O_Box_City__c = 'aTest1',
            P_O_Box_Zip_Postal_Code__c = '1001',
            POBox__c = 'aTest1',
            PreferredLanguage__c = 'German',
            Status__c = 'New',
            GoldenRecordID__c ='goldenTest'
        ); */
        
        insert acc; 
        
    }

    static testmethod void SendPDFController(){
       account acc = [select id, name,GoldenRecordID__c from account limit 1];
      
        
        Test.startTest();
        PageReference pageRef = Page.AccountOverviewPDF;
        system.debug('Test Class: SendPDFController_Test');
        pageRef.getParameters().put('ID', acc.Id);
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SendPDFController spc = new SendPDFController(sc);
		sc = new ApexPages.StandardController(new Account());
        spc = new SendPDFController(sc);
        Test.stoptest();
    }
}