/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2020-11-16
 * Sprint      : 2
 * Work item   : SPIII-4293
 * Testclass   : Test_InvoiceTrigger
 * Package     : 
 * Description :
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing class InvoiceTriggerHelper {
    public static void statusUpdates(Map<Id, Invoice__c> newItems, Map<Id, Invoice__c> oldItems){
        Set<Id> invoiceToTerminateIds = new Set<Id>();
        for(Invoice__c invoice : newItems.values()) { 
            Invoice__c oldInvoice = oldItems.get(invoice.Id);
            //system.debug('Ldimartino - newInvoice.timestamp: ' + newInvoice.timestamp + ' newInvoice.payment_status: ' + newInvoice.Payment_Status__c);
            if(invoice.Payment_Status__c == oldInvoice.Payment_Status__c && invoice.Open_Amount__c == oldInvoice.Open_Amount__c) {
                continue;
            }
            if(invoice.Payment_Status__c == '0') {
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_PAYED;
                invoice.Payment_Date__c = invoice.Swissbilling_Update_Timestamp__c;
            }
            //Vincenzo Laudato 2019-06-04 AS-6 *** START
            else if(invoice.Payment_Status__c.equals('1')){
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
            }
            //ldimartino, SF2-181 *** START
            else if(invoice.Payment_Status__c.equals('S3') && oldInvoice.Process_Mode__c == ConstantsUtil.INVOICE_PROCESS_MODE_MODESWISSLIST){
                invoiceToTerminateIds.add(invoice.Id);
            }
            //ldimartino, SF2-181 *** END
            //Vincenzo Laudato 2019-06-04 *** END
            if(BillingHelper.OverduePaymentStatusList.contains(invoice.Payment_Status__c)) {
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_OVERDUE;
            }
            if(invoice.Payment_Status__c.contains('C')){
                invoice.Status__c = ConstantsUtil.INVOICE_STATUS_CANCELLED;
            }
            if(invoice.Open_Amount__c < oldInvoice.Total__c && oldInvoice.Status__c != ConstantsUtil.INVOICE_STATUS_CANCELLED){
                invoice.Payment_Date__c = invoice.Swissbilling_Update_Timestamp__c;
            }
        }
        if(invoiceToTerminateIds.size() > 0){
            SRV_Contract.terminateContractsForInvoices(invoiceToTerminateIds);
        }
    }
}