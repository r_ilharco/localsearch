public class ErrorHandler {
    /*
     * Main application error handler class.
     * The purpose is to feed a specific channel (Object) to allow subscription for business active monitoring
     */
        
    public enum ErrorCode {
        E_UNKNOWN,
        E_DML_FAILED,
        E_NO_TAX_SCHEDULE,
        E_NO_PRINTING_PROFILE,
        E_TAX_SCHEDULE_OVERLAP,
        E_MAX_RETRIES_REACHED,
        E_HTTP_BAD_RESPONSE,
        E_NULL_VALUE,
        E_NOT_IMPLEMENTED,
        E_JSON_DESERIALIZATION_FAILED,
        E_SOAP_FAULT,
        E_INTEGRATION_PEER_FAULT,
        E_MISSING_CONTENT
    }
    
    public enum WarningCode {
        W_UNKNOWN,
        W_READ_ONLY_ORG,
        W_BILL_VALUE_ZERO,
        W_NOT_A_NUMBER,
        W_BILL_TOTAL_BELOW_LIMIT,
        W_NO_INVOICE_PERIOD
    }
    
    public static void logInfo(string bpName, string subdomain, string message) {
    	ErrorHandler.logInfo(bpName, subdomain, message, null);
    }
    
    public static void logInfo(string bpName, string subdomain, string message, string contextData) {
        ErrorHandler.logInfo(bpName, subdomain, message, contextData, null);
    }

    public static void logInfo(string bpName, string subdomain, string message, string contextData, string correlationId) {
        ErrorHandler.logInfo(bpName, subdomain, message, contextData, correlationId, null);
    }

    public static void logInfo(string bpName, string subdomain, string message, string contextData, string correlationId, SObject businessObject) {
        ErrorHandler.log(System.LoggingLevel.INFO, bpName, subdomain, null, (string)null, message, contextData, correlationId, null, null, true, businessObject);
    }    
    
    public static void logWarning(string bpName, string subdomain, WarningCode warningCode, string message, string contextData) {
    	ErrorHandler.logWarning(bpName, subdomain, warningCode, message, contextData, null, null);
    }

    public static void logWarning(string bpName, string subdomain, WarningCode warningCode, string message, string contextData, string correlationId, SObject businessObject) {
    	ErrorHandler.log(System.LoggingLevel.WARN, bpName, subdomain, null, warningCode.name(), message, contextData, correlationId, null, null, true, businessObject);
    }
    
    public static void logError(string bpName, string subdomain, GenericException exceptionVar) {
        ErrorHandler.logError(bpName, subdomain, exceptionVar, null);
    }
    
    public static void logError(string bpName, string subdomain, GenericException exceptionVar, string contextData) {
    	ErrorHandler.logError(bpName, subdomain, exceptionVar, exceptionVar.ErrorCode, null, contextData, null);
    }

    public static void logError(string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, string message, string contextData, string correlationId) {
    	ErrorHandler.logError(bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId);
    }

    public static void logError(string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, string message, string contextData, string correlationId, SObject businessObject) {
    	ErrorHandler.logError(bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId, businessObject);
    }
    
    public static void logError(string bpName, string subdomain, Exception exceptionVar, string errorCode, string message, string contextData, string correlationId) {
    	ErrorHandler.logError(bpName, subdomain, exceptionVar, errorCode, message, contextData, correlationId, null);
    }
    
    public static void logError(string bpName, string subdomain, Exception exceptionVar, string errorCode, string message, string contextData, string correlationId, SObject businessObject) {
    	ErrorHandler.log(System.LoggingLevel.ERROR, bpName, subdomain, exceptionVar, errorCode, message, contextData, correlationId, null, null, false, businessObject);
    }
    
    public static void log(System.LoggingLevel level, string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, 
                           string message, string contextData, string correlationId, string initiator, string process, boolean isHandled) {
    	ErrorHandler.log(level, bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId, initiator, process, isHandled, null);
    }

    public static Error_Log__c createLogInfo(string bpName, string subdomain, string message) {
    	return ErrorHandler.createLogInfo(bpName, subdomain, message, null);
    }
    
    public static Error_Log__c createLogInfo(string bpName, string subdomain, string message, string contextData) {
        return ErrorHandler.createLogInfo(bpName, subdomain, message, contextData, null);
    }

    public static Error_Log__c createLogInfo(string bpName, string subdomain, string message, string contextData, string correlationId) {
        return ErrorHandler.createLogInfo(bpName, subdomain, message, contextData, correlationId, null);
    }

    public static Error_Log__c createLogInfo(string bpName, string subdomain, string message, string contextData, string correlationId, SObject businessObject) {
        return ErrorHandler.createLog(System.LoggingLevel.INFO, bpName, subdomain, null, (string)null, message, contextData, correlationId, null, null, true, businessObject);
    }    
    
    public static Error_Log__c createLogWarning(string bpName, string subdomain, WarningCode warningCode, string message, string contextData) {
    	return ErrorHandler.createLogWarning(bpName, subdomain, warningCode, message, contextData, null, null);
    }

    public static Error_Log__c createLogWarning(string bpName, string subdomain, WarningCode warningCode, string message, string contextData, string correlationId, SObject businessObject) {
    	return ErrorHandler.createLog(System.LoggingLevel.WARN, bpName, subdomain, null, warningCode.name(), message, contextData, correlationId, null, null, true, businessObject);
    }
    
    public static Error_Log__c createLogError(string bpName, string subdomain, GenericException exceptionVar) {
        return ErrorHandler.createLogError(bpName, subdomain, exceptionVar, null);
    }
    
    public static Error_Log__c createLogError(string bpName, string subdomain, GenericException exceptionVar, string contextData) {
    	return ErrorHandler.createLogError(bpName, subdomain, exceptionVar, exceptionVar.ErrorCode, null, contextData, null);
    }

    public static Error_Log__c createLogError(string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, string message, string contextData, string correlationId) {
    	return ErrorHandler.createLogError(bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId);
    }

    public static Error_Log__c createLogError(string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, string message, string contextData, string correlationId, SObject businessObject) {
    	return ErrorHandler.createLogError(bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId, businessObject);
    }
    
    public static Error_Log__c createLogError(string bpName, string subdomain, Exception exceptionVar, string errorCode, string message, string contextData, string correlationId) {
    	return ErrorHandler.createLogError(bpName, subdomain, exceptionVar, errorCode, message, contextData, correlationId, null);
    }
    
    public static Error_Log__c createLogError(string bpName, string subdomain, Exception exceptionVar, string errorCode, string message, string contextData, string correlationId, SObject businessObject) {
    	return ErrorHandler.createLog(System.LoggingLevel.ERROR, bpName, subdomain, exceptionVar, errorCode, message, contextData, correlationId, null, null, false, businessObject);
    }
    
    public static Error_Log__c createLog(System.LoggingLevel level, string bpName, string subdomain, Exception exceptionVar, ErrorCode errorCode, 
                           string message, string contextData, string correlationId, string initiator, string process, boolean isHandled) {
    	return ErrorHandler.createLog(level, bpName, subdomain, exceptionVar, errorCode.name(), message, contextData, correlationId, initiator, process, isHandled, null);
    }

    public static Error_Log__c createLog(System.LoggingLevel level, string bpName, string subdomain, Exception exceptionVar, string errorCode, 
                           string message, string contextData, string correlationId, string initiator, string process, boolean isHandled, SObject businessObject) {
        string exceptionClass = '';
        string exceptionMessage = '';
        string exceptionStackTrace = '';
        if(exceptionVar != null){
			exceptionClass = exceptionVar.getTypeName();
            exceptionMessage = exceptionVar.getMessage();
            exceptionStackTrace = exceptionVar.getStackTraceString();
        }
        // Log to system log
        System.debug(level, errorCode + ':' + message + ':' + bpName + ':' + subdomain + ':' + exceptionClass + ':' + exceptionMessage + ':' + initiator + ':' + process + ':' + correlationId + ':' + isHandled);

        string normalizedLevel = level.name();

        if(level == System.LoggingLevel.FINE || level == System.LoggingLevel.FINER || level == System.LoggingLevel.FINEST || level == System.LoggingLevel.INTERNAL) {
			normalizedLevel = 'TRACE';
        }
        if(normalizedLevel == 'TRACE' || normalizedLevel == 'NONE' || normalizedLevel == 'FATAL'){
        	normalizedLevel = 'INFO';
        }
        // Merge custom message and Exception message on a single field
        if(message == null || message == ''){
        	message = exceptionMessage;
        } else if(exceptionMessage !='') {
            message = message + ' (' + exceptionMessage + ')';
        }
		string objName;
        string objId;
        if(businessObject!=null) {
        	objName = businessObject.getSObjectType().getDescribe().getName();
            objId = businessObject.Id;
        }
                               
        return new Error_Log__c(
            TimeStamp__c = DateTime.now(),
            Level__c = normalizedLevel,
            SubDomain__c = subdomain,
            BusinessProcessName__c = bpName,
            Exception__c = exceptionClass,
            Code__c = errorCode,
            Message__c = message,
            StackTrace__c = exceptionStackTrace,
            Payload__c = contextData,
            CorrelationId__c = correlationId,
            Is_Handled__c = isHandled,
            Initiator__c = initiator,
            Process__c = process,
            BusinessObjectIdValue__c = objId,
            BusinessObjectIdName__c = objName,
            Status__c = 'New'
        );
	}
    
    public static void log(System.LoggingLevel level, string bpName, string subdomain, Exception exceptionVar, string errorCode, 
                           string message, string contextData, string correlationId, string initiator, string process, boolean isHandled, SObject businessObject) {
        string exceptionClass = '';
        string exceptionMessage = '';
        string exceptionStackTrace = '';
        if(exceptionVar != null){
			exceptionClass = exceptionVar.getTypeName();
            exceptionMessage = exceptionVar.getMessage();
            exceptionStackTrace = exceptionVar.getStackTraceString();
        }
        // Log to system log
        System.debug(level, errorCode + ':' + message + ':' + bpName + ':' + subdomain + ':' + exceptionClass + ':' + exceptionMessage + ':' + initiator + ':' + process + ':' + correlationId + ':' + isHandled);
        if(level == System.LoggingLevel.NONE) {
            return;
        }
        string normalizedLevel = level.name();	
        if(level == System.LoggingLevel.FINE || level == System.LoggingLevel.FINER || level == System.LoggingLevel.FINEST || level == System.LoggingLevel.INTERNAL) {
			normalizedLevel = 'TRACE';
        }
        if(normalizedLevel == 'TRACE' || normalizedLevel == 'NONE' || normalizedLevel == 'FATAL'){
        	normalizedLevel = 'INFO';
        }
                               
        // Merge custom message and Exception message on a single field
        if(message == null || message == ''){
        	message = exceptionMessage;
        } else if(exceptionMessage !='') {
            message = message + ' (' + exceptionMessage + ')';
        }
		string objName;
        string objId;
        if(businessObject!=null) {
        	objName = businessObject.getSObjectType().getDescribe().getName();
            objId = businessObject.Id;
        }
        if(System.isQueueable() || System.isFuture() || System.isBatch()) {
            Error_Log__c errorLog = new Error_Log__c(
                TimeStamp__c = DateTime.now(),
                Level__c = normalizedLevel,
                SubDomain__c = subdomain,
                BusinessProcessName__c = bpName,
                Exception__c = exceptionClass,
                Code__c = errorCode,
                Message__c = message,
                StackTrace__c = exceptionStackTrace,
                Payload__c = contextData,
                CorrelationId__c = correlationId,
                Is_Handled__c = isHandled,
                Initiator__c = initiator,
                Process__c = process,
                BusinessObjectIdValue__c = objId,
                BusinessObjectIdName__c = objName,
                Status__c = 'New'
            );
            
            if(System.isFuture()) {
                System.enqueueJob(new ErrorEnqueueJob(errorLog)); 
            } else {
               insert errorLog;
            }
        } else {
			futureInsertLog(DateTime.now(), normalizedLevel, subdomain, bpName, exceptionClass, 
                errorCode, message, exceptionStackTrace, contextData, correlationId, initiator, process, isHandled, objName, objId);
        }
	}

    @future
    public static void futureInsertLog(DateTime now, string normalizedLevel, string subdomain, string bpName, string exceptionClass, 
              string errorCode, string message, string exceptionStackTrace, string contextData, string correlationId, string initiator, string process, boolean isHandled, string objectName, string objectId) {
         // Subscribe to channel for active monitoring
        
        insert new Error_Log__c(
            TimeStamp__c = now,
            Level__c = normalizedLevel,
            SubDomain__c = subdomain,
            BusinessProcessName__c = bpName,
            Exception__c = exceptionClass,
            Code__c = errorCode,
            Message__c = message,
            StackTrace__c = exceptionStackTrace,
            Payload__c = contextData,
            CorrelationId__c = correlationId,
            Is_Handled__c = isHandled,
            Initiator__c = initiator,
            Process__c = process,
            BusinessObjectIdValue__c = objectId,
            BusinessObjectIdName__c = objectName,
            Status__c = 'New'
       );
    }

    public abstract class GenericException extends Exception {
        
        protected string pErrorCode;
        
        public string ErrorCode {
            get { return pErrorCode; }
        }
        
        public GenericException(string message, string errorCode) {
            super.setMessage(message);
			this.pErrorCode = errorCode;
        }
        
        public GenericException(string message, ErrorCode errorCode) {
            this(message, errorCode.name());
        }
        
        public GenericException(string message, WarningCode warningCode) {
            this(message, warningCode.name());
        }
        
        public GenericException(Exception initException, string message, ErrorCode errorCode) {
        	this(initException, message, errorCode.name());
        }
        
		public GenericException(Exception initException, string message, string errorCode) {
            super.setMessage(message);
            super.initCause(initException);
			this.pErrorCode = errorCode;
        }
    } 
}