public class ValidateAddressRequestWrapper {
    //public String correlationId{get;set;}
    public String company{get;set;}
    public String street{get;set;}
    public String zip{get;set;}
    public String uid{get;set;}
    public String email{get;set;}
    public String phone{get;set;}
    public String location{get;set;}
    public Map<String, String> params {get; set;}
    
    public ValidateAddressRequestWrapper()
    {
        //NOP
    }
    
    //public ValidateAddressRequestWrapper(String correlationId, String company, String street, String zip, String uid, String email, String phone, String location){
    public ValidateAddressRequestWrapper(String company, String street, String zip, String uid, String email, String phone, String location){
        //this.correlationId = correlationId;
        this.company = company;
        this.street = street;
        this.zip = zip;
        this.uid = uid;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.params = new Map<String, String>();
    }
        
    public ValidateAddressRequestWrapper(String company, String street, String zip, String uid, String email, String phone, String location, Map<String, String> params){
        this.company = company;
        this.street = street;
        this.zip = zip;
        this.uid = uid;
        this.email = email;
        this.phone = phone;
        this.location = location;
        if(params != NULL)
            this.params = params;
        else
        	this.params = new Map<String, String>();
    }
}