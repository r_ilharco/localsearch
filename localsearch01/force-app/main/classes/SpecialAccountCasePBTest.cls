@isTest
public class SpecialAccountCasePBTest {
    
    @isTest 
    static void testSpecialAccountCase(){
        
        try{
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh';    
            acc.Special__c = true;          
            insert acc;
            
            Case cs =  new Case();
            cs.RecordTypeId = '0121r000000VicNAAS';
            cs.SuppliedEmail = 'iqbal.rocky@arollotech.com';
            cs.Origin = 'Email';
            cs.AccountId = acc.Id;
            
            test.startTest();
            insert cs;
            update cs;
            test.stopTest();
            
            Case cd= [select Special__c from Case];
            system.debug('CaseSpc' +cd);
        }
        catch(Exception e)
        {
            system.assert(false, e.getMessage());
        }
        
    }
}