@isTest
public class CPQModelsTest {

    @isTest
    public static void testCPQModels(){
        try{
            CPQModels.ConfigurationModel cm = new CPQModels.ConfigurationModel();
            CPQModels cpq = new CPQModels();
            SBQQ__Quote__c quote = createQuote();
            Product2 prod = createMainProduct();
            
            Pricebook2 prBook = new Pricebook2();
            prBook.Name = 'test price book';
            insert prBook;
            System.Debug('Pr.Book '+ prBook); 
            
            CPQModels.QuoteModel qm = cpq.readQuote(quote.Id);
            
            CPQModels.ProductModel pm = cpq.readProduct(prod.Id, prBook.Id, 'CHF');
            
            CPQModels.ProductModel[] products = new CPQModels.ProductModel[1];
            products[0] = pm;
            
            cpq.saveQuote(qm);
            cpq.addProduct(qm, pm);
            
            try{
                cpq.addProducts(qm, products, 12345);
                cpq.calculateQuote(qm, 'TestClass');
                }catch(Exception e){                                
           System.Assert(true, e.getMessage());
        }
                       
            }catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
    }
    
    @isTest
    public static SBQQ__Quote__c createQuote() {
        Account acc = createAccount();
        
        Opportunity opp = new Opportunity(StageName = 'Draft', CloseDate = Date.today().AddDays(89), Account = acc, AccountId = acc.Id, Name = 'Test Opportunity xxx');
        	insert opp;
            System.Debug('opp '+ opp);
            
            opp.SBQQ__AmendedContract__c = null;           
            update opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc.Id, SBQQ__Opportunity2__c =opp.Id, SBQQ__Type__c = 'Quote', SBQQ__Status__c = 'Draft', SBQQ__ExpirationDate__c = Date.today().AddDays(89));
        insert quote;
        
        return quote;
        
    }
    
    @isTest
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'XXX ';
        acc.GoldenRecordID__c = 'GRID';
        
        acc.POBox__c = '10';
        acc.P_O_Box_Zip_Postal_Code__c ='101';
        acc.P_O_Box_City__c ='dh';    
                   
        insert acc;
        System.Debug('Account '+ acc); 
              
        return acc;
    }
    
    @isTest
	 public static void createProducts() {
        List<Product2> listProds = new List<Product2>();
        for(Integer i = 0; i < 3; i++) {
         Product2 pr = new Product2();
           pr.Name = 'XXX ' + i;
           pr.ProductCode = 'SLT00'+i;
           pr.IsActive=True;
           pr.SBQQ__ConfigurationType__c='Allowed';
           pr.SBQQ__ConfigurationEvent__c='Always';
           pr.SBQQ__QuantityEditable__c=True;
            
           pr.SBQQ__NonDiscountable__c=False;
           pr.SBQQ__SubscriptionType__c='Renewable'; 
           pr.SBQQ__SubscriptionPricing__c='Fixed Price';
           pr.SBQQ__SubscriptionTerm__c=12; 
           
           listProds.add(pr);
        }
    insert listProds;
         system.debug('Product' + listProds);
    }
    
    @isTest
    public static Product2 createMainProduct() {        
        createProducts();
        Product2 p = [SELECT Id, Name FROM Product2 WHERE ProductCode = 'SLT001' Limit 1];
        return p;
    }
}