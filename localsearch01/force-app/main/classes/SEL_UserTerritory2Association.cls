/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 01-20-2020
 * Sprint      : 
 * Work item   :
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class SEL_UserTerritory2Association {
	public static Map<Id, UserTerritory2Association> getUserTerritory2AssocationsByUserId(Set<Id> userIds)
    {
        return new Map<Id, UserTerritory2Association>([
            SELECT Id, UserId, Territory2Id, Territory2.Name, Territory2.DeveloperName, 
            Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.DeveloperName,
            Territory2.ParentTerritory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2.DeveloperName,
            IsActive, RoleInTerritory2, LastModifiedDate, LastModifiedById, SystemModstamp
            FROM UserTerritory2Association
            WHERE UserId IN :userIds
        ]);
    }
    
    public static Map<Id, UserTerritory2Association> getUserTerritory2AssocationsByTerritoryIds(Set<Id> territoryIds)
    {
        return new Map<Id, UserTerritory2Association>([
            SELECT Id, UserId, Territory2Id, Territory2.Name, Territory2.DeveloperName, 
            Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.DeveloperName,
            Territory2.ParentTerritory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2.DeveloperName,
            IsActive, RoleInTerritory2, LastModifiedDate, LastModifiedById, SystemModstamp
            FROM UserTerritory2Association
            WHERE Territory2Id IN :territoryIds
        ]);
    }
}