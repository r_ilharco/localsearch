/**
 * An apex class that updates details of a portal user.
   Guest users are never able to access this page.
 */
@IsTest public with sharing class MyProfilePageControllerTest {

    @IsTest(SeeAllData=true) static void testSave() {
        // Modify the test to query for a portal user that exists in your org
        List<User> existingPortalUsers = [SELECT id, profileId, userRoleId FROM User WHERE UserRoleId <> null AND UserType='CustomerSuccess'];

        if (existingPortalUsers.isEmpty()) {
            User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                                FROM User WHERE id =: UserInfo.getUserId()];
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
            controller.edit();
            System.assert(controller.getIsEdit() == true);
            controller.cancel();
            System.assert(controller.getIsEdit() == false);
            
            System.assert(Page.ChangePassword.getUrl().equals(controller.changePassword().getUrl()));
            
            String randFax = Math.rint(Math.random() * 1000) + '5551234';
            controller.getUser().Fax = randFax;
            controller.save();
            System.assert(controller.getIsEdit() == false);
            
            currentUser = [Select id, fax from User where id =: currentUser.Id];
            System.assert(currentUser.fax == randFax);
        } else {
            User existingPortalUser = existingPortalUsers[0];
            String randFax = Math.rint(Math.random() * 1000) + '5551234';

            System.runAs(existingPortalUser) {
                MyProfilePageController controller = new MyProfilePageController();
                System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
                System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
                controller.edit();
                System.assert(controller.getIsEdit() == true);

                controller.cancel();
                System.assert(controller.getIsEdit() == false);

                controller.getUser().Fax = randFax;
                controller.save();
                System.assert(controller.getIsEdit() == false);
            }

            // verify that the user was updated
            existingPortalUser = [Select id, fax from User where id =: existingPortalUser.Id];
            System.assert(existingPortalUser.fax == randFax);
        }
        
        User testGuestUser = createTestGuestUser();
        System.runAs(testGuestUser)
        {
            try{
                MyProfilePageController controller = new MyProfilePageController();
                controller = new MyProfilePageController();
            }catch(Exception e){	                            
                System.Assert(true, e.getMessage());
            } 
        } 
    }
    
    public static User createTestGuestUser(){
        Profile guestProfile = [SELECT Id FROM Profile WHERE Name =: 'localsearchtst Profile']; 
        User testUser = new User();
        testUser.ProfileId = guestProfile.Id;
        testUser.Alias = 'test';
        testuser.email='testme@testorg.com';              
        testUser.LastName='Testln'; 
        testuser.username='testme@testorg.com';
        testUser.EmailEncodingKey='UTF-8';
        testUser.LanguageLocaleKey='en_US'; 
        testUser.LocaleSidKey='en_US';             
        testUser.TimeZoneSidKey='America/Los_Angeles';  
        
        insert testUser ;
        System.debug('user: '+ testUser);
        return testUser;
    }
}