/*
----------------------------------------------------------------------
-- - Class Name   : SG_ApprovalACLTriggerHelper
-- - Author        : Fabio Giuliani
-- - Date         : 21/02/2019
-- - Description  : Trigger for the manage ApprovalACL__c
-- - Version      : 1.0
----------------------------------------------------------------------
*/
public class SG_ApprovalACLTriggerHelper {

    public static void updateFieldCheck1(List<ApprovalACL__c> approvalList){
        system.debug('INSERT');
        
/*--------------------------------------------------------------------
-- Download all RecordType  
----------------------------------------------------------------------*/        
        List <RecordType> recordType = [SELECT Id, DeveloperName,Name FROM RecordType];
        Map<Id,RecordType> recordTypeMap = new Map<Id,RecordType>(recordType);
        
/*--------------------------------------------------------------------
-- I manage two cycles to find the exact RecordType  
----------------------------------------------------------------------*/  
        
        for(ApprovalACL__c app :approvalList){
            system.debug('record Type '+app.RecordType.DeveloperName);            
            for(recordType rt :recordTypeMap.values()){
                if (rt.Id==app.RecordTypeId){
                    if (rt.DeveloperName=='TypAcc'){
                        app.check1__c = 'AC';
                    }
                    if (rt.DeveloperName=='TypCon'){
                        app.check1__c = 'CO';
                    }
                    if (rt.DeveloperName=='TypLea'){
                        app.check1__c = 'LE';
                    }
                }
            }
        }
    }
    
    public static void creaACL(List<ApprovalACL__c> approvalList){
        system.debug('UPDATE');

/*--------------------------------------------------------------------
-- I create three lists to prepare for insert  
----------------------------------------------------------------------*/        
        List<Account> newAccount = new List<Account>();
        List<Contact> newContact = new List<Contact>();
        List<Lead> newLead = new List<Lead>();
/*--------------------------------------------------------------------
-- I create a lists to prepare for update the field check2 = NU to avoid mistakes  
----------------------------------------------------------------------*/        
        List<ApprovalACL__c> approvalListToUpdate = new List<ApprovalACL__c>();
        
/*--------------------------------------------------------------------
-- I manage the list  
----------------------------------------------------------------------*/        
        for(ApprovalACL__c app :approvalList){
            if (app.check1__c=='AC' && app.check2__c=='OK'){
                Account newAcc = new Account(
                    Name = app.acc_AccountName__c,
                    Status__c = app.acc_Status__c,
                    PreferredLanguage__c = app.acc_PreferredLanguage__c,
                    BillingCity = app.acc_Billing_City__c,
                    Billingstreet =app.acc_Billing_street__c, 
                    BillingPostalCode = app.acc_Billing_Zip_PostalCode__c,
                    BillingCountry = app.acc_Billing_Country__c,
                    POBox__c = app.acc_POBox__c,
                    P_O_Box_City__c = app.acc_P_O_Box_City__c,
                    P_O_Box_Zip_Postal_Code__c = app.acc_P_O_Box_Zip_Postal_Code__c);                    
                newAccount.add(newAcc);
                app.check2__c = 'NU';
            }else  if (app.check1__c=='CO' && app.check2__c=='OK'){
                Contact newCon = new Contact(
                    FirstName = app.con_FirstName__c,
                    LastName = app.con_LastName__c,
                    Phone = app.con_Business_Phone__c,
                    Email = app.con_Email__c,
                    Accountid = app.con_AccountId__c,
                    Position__c = app.con_Position__c,
                    GoldenRecordID__c = app.con_GoldenRecordID__c,
                    PO_Box__c = app.con_PO_Box__c,
                    PO_BoxCity__c = app.con_PO_BoxCity__c,
                    PO_BoxZip_PostalCode__c = app.con_PO_BoxZip_PostalCode__c,
                    MailingCity = app.acc_Billing_City__c,
                    MailingStreet =app.acc_Billing_street__c, 
                    MailingPostalCode = app.acc_Billing_Zip_PostalCode__c,
                    MailingCountry = app.acc_Billing_Country__c);
                newContact.add(newCon);
                app.check2__c = 'NU';
            } else if (app.check1__c=='LE' && app.check2__c=='OK'){
                Lead newLea = new Lead(
                    FirstName = app.lea_FirstName__c,
                    LastName = app.lea_LastName__c,
                    Company = app.lea_Company__c,
                    LeadSource = app.lea_LeadSource__c,
                    Status = app.lea_Status__c,
                    Rating = app.lea_Rating__c,
                    P_O_Box__c = app.lea_P_O_Box__c,
                    P_O_Box_City__c = app.lea_P_O_Box_City__c,
                    P_O_Box_Zip_Postal_Code__c = app.lea_P_O_Box_Zip_Postal_Code__c,
                    MobilePhone = app.lea_MobilePhone__c,
                    Phone = app.lea_Phone__c,
                    City = app.acc_Billing_City__c,
                    Street =app.acc_Billing_street__c, 
                    PostalCode = app.acc_Billing_Zip_PostalCode__c,
                    Country = app.acc_Billing_Country__c);
                newLead.add(newLea);
                app.check2__c = 'NU';
            }
        }
/*--------------------------------------------------------------------
-- I insert the relative records  
----------------------------------------------------------------------*/        
try{
    Database.DMLOptions dml = new Database.DMLOptions();
    dml.DuplicateRuleHeader.AllowSave = true; 
        if (newAccount.size()>0){
            Database.SaveResult[] sr = Database.insert(newAccount, dml);            
        }
        if (newContact.size()>0){
             Database.SaveResult [] sr2 = Database.insert(newContact, dml);       
        }
        if(newLead.size()>0){
            Database.SaveResult[] sr3 = Database.insert(newLead, dml);           
        }
        if(approvalListToUpdate.size()>0){
            Database.SaveResult[] sr4 = Database.update(approvalListToUpdate, dml);         
        } 
         
    }catch (exception e) {
        system.debug('exception: '+e);
        
        }   
    }
}