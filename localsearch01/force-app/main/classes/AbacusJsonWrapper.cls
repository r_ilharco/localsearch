/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date		   : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : this class is used for the construction of the JsonPayload
 * Changelog   : 
 */

public class AbacusJsonWrapper {
    
    
	List<MyInvoice> invoices;
    
    //public AbacusJsonWrapper(){}
    
    public AbacusJsonWrapper(List<MyInvoice> invoices){
        this.invoices = new List<MyInvoice>();
            this.invoices = invoices;
    }
        
    public class MyInvoice{
        String id;
        String name;
        String correlation_id;
        String payment_reference;
        String invoice_date;
        String code;
        String process_mode;
        public String rounding {get; set;}
        String tax;
        String tax_mode;
        public String total {get; set;}
        String status;
        String billing_channel;
        MyCustomer customer;
        String open_amount;
        MyBillingProfile billing_profile;
        List<MyOrder> orders;
        String payment_date;
        
        public MyInvoice(){}
        
  		public MyInvoice (Invoice__c invoice, MyCustomer customer, MyBillingProfile billing_profile, List<MyOrder> orders){
        	this.id = invoice.Id;
            this.name = invoice.Name;
            this.correlation_id = invoice.Correlation_Id__c;
            this.payment_reference = invoice.Payment_Reference__c;
            System.debug(invoice.Invoice_Date__c);
            this.invoice_date = String.valueOf(invoice.Invoice_Date__c);
            System.debug(this.invoice_date);
            this.code = invoice.Invoice_Code__c;
            this.process_mode = invoice.Process_Mode__c;
            this.rounding = String.valueOf(invoice.Rounding__c);
            this.tax = String.valueOf(invoice.Tax__c);
            this.tax_mode = invoice.Tax_Mode__c;
            this.total = String.valueOf(invoice.Total__c);
            this.status = invoice.Status__c;
            this.billing_channel = invoice.Billing_Channel__c;
            this.customer = new MyCustomer();
                this.customer = customer;
            this.open_amount = String.valueOf(invoice.Open_Amount__c);
            this.billing_profile = new MyBillingProfile();
            	this.billing_profile = billing_profile;
            this.orders = new List<MyOrder>();
            	this.orders = orders;	    
            this.payment_date = invoice.Payment_Date__c != null ? invoice.Payment_Date__c.format('yyyy-MM-dd') : null ;       
        }

    }
    
    public class MyCustomer{
        String customer_number;
		String name;
        
        public MyCustomer(){}
        
        public MyCustomer(Invoice__c invoice){
            this.customer_number = String.valueOf(invoice.Customer__r.Customer_Number__c);
            this.name = invoice.Customer__r.Name;
        }
    }
    
    public class MyBillingProfile{
        String street;
        String city;
        String country;
        String postal_code;
        String state;
        
        public MyBillingProfile(){}
        
        public MyBillingProfile(Invoice__c invoice){
            if(!String.isBlank(invoice.Billing_Profile__r.Billing_Street__c)){
            	this.street = invoice.Billing_Profile__r.Billing_Street__c;                
            }else{
                this.street = invoice.Billing_Profile__r.P_O_Box__c;
            }
            if(!String.isBlank(invoice.Billing_Profile__r.Billing_City__c)){
            	this.city = invoice.Billing_Profile__r.Billing_City__c;                
            }else{
                this.city = invoice.Billing_Profile__r.P_O_Box_City__c;
            }
            if(!String.isBlank(String.valueOf(invoice.Billing_Profile__r.Billing_Postal_Code__c))){
				this.postal_code = String.valueOf(invoice.Billing_Profile__r.Billing_Postal_Code__c);
            }else{
                this.postal_code = String.valueOf(invoice.Billing_Profile__r.P_O_Box_Zip_Postal_Code__c);
            }
            this.country =  invoice.Billing_Profile__r.Billing_Country__c;
            this.state =  invoice.Billing_Profile__r.Billing_State__c;
        }
    }
    
    public class MyOrder{
        String id;
        String amount_subtotal;
        String description;
        String invoice;
        String invoice_order_code;
        String item_row_number;
        String period_from;
        String period_to;
        String tax_subtotal;
        String title;
        MyContract contract;
        public List<MyItem> items {get; set;}
        
        public MyOrder(){}
        
        public MyOrder(Invoice_Order__c invoiceOrder, MyContract contract, List<MyItem> items){
        	this.id = invoiceOrder.Id;
            this.amount_subtotal = String.valueOf(invoiceOrder.Amount_Subtotal__c);
            this.description = invoiceOrder.Description__c;
            this.invoice = invoiceOrder.Invoice__c;
            this.invoice_order_code = String.valueOf(invoiceOrder.Invoice_Order_Code__c);
            this.item_row_number = String.valueOf(invoiceOrder.Item_Row_Number__c);
            this.period_from = String.valueOf(invoiceOrder.Period_From__c);
            this.period_to = String.valueOf(invoiceOrder.Period_To__c);
            this.tax_subtotal = String.valueOf(invoiceOrder.Tax_Subtotal__c);
            this.title = invoiceOrder.Title__c;
            this.contract = new MyContract();
            	this.contract = contract;
            this.items = new List<MyItem>();
                this.items = items;
        }
    }
    
    public class MyContract{
        String id;
        String contract_number;
        String start_date;
        
        public MyContract(){}
        
        public MyContract(Invoice_Order__c invoiceOrder){
            this.id = invoiceOrder.Contract__c;  
            this.contract_number = invoiceOrder.Contract__r.ContractNumber;
            this.start_date =  String.valueOf(invoiceOrder.Contract__r.StartDate);
        }
    }
    
    public class MyItem{
        public List<MyTaxItem> tax_items {get; set;}
        String accrual_from;
        String accrual_to;
        String amount;
        String tax;
        String description;
        String discount_percentage;
        String discount_value;
        String installment;
        String level;
        String line_number;
        String period;
        String quantity;
        String sales_channel;
        String tax_code;
        String title;
        public String total {get; set;}
        String unit;
        String unit_price;
        String credit_account;
        String kst_code;
        String ktr_code;
        Myproduct product;
        MySubscription subscription;
        MyCreditNote credit_note;
        
        Public MyItem(){}
        
        public MyItem(List<MyTaxItem> tax_items, Invoice_Item__c invoiceItem, Myproduct product, MySubscription subscription, MyCreditNote credit_note){
                
            this.tax_items = new List<MyTaxItem>();
            	this.tax_items = tax_items;
            this.accrual_from = String.valueOf(invoiceItem.Accrual_From__c);
            this.accrual_to = String.valueOf(invoiceItem.Accrual_To__c);
            this.amount = String.valueOf(invoiceItem.Amount__c);
            this.tax = String.valueOf(invoiceItem.Tax__c);
            this.description = invoiceItem.Description__c;
            this.discount_percentage = String.valueOf(invoiceItem.Discount_Percentage__c);
            this.discount_value = String.valueOf(invoiceItem.Discount_Value__c);
            this.installment = String.valueOf(invoiceItem.Installment__c);
            this.level = String.valueOf(invoiceItem.Level__c); 
            this.line_number = String.valueOf(invoiceItem.Line_Number__c);
            this.period = String.valueOf(invoiceItem.Period__c);
            this.quantity = String.valueOf(invoiceItem.Quantity__c);
            this.sales_channel = invoiceItem.Sales_Channel__c;
            this.tax_code = invoiceItem.Tax_Code__c;
            this.title = invoiceItem.Title__c;
            this.total = String.valueOf(invoiceItem.Total__c);
            this.unit = invoiceItem.Unit__c;
            this.unit_price = String.valueOf(invoiceItem.Unit_Price__c);
            this.credit_account = invoiceItem.Credit_Account__c;
            this.kst_code = invoiceItem.KSTCode__c;
            this.ktr_code = invoiceItem.KTRCode__c;
            this.product = new MyProduct();
            	this.product = product;
            this.subscription = new MySubscription();
            	this.subscription = subscription;
            this.credit_note = new MyCreditNote();
                this.credit_note = credit_note;
        }
	}
    
    public class MyTaxItem{
        String date_from;
        String date_to;
        String net;
        String percentage;
        String vat;
        public String total {get; set;}
        String vat_code;
        String has_accrual;
        
        public MyTaxItem(){}
        
        Public MyTaxItem(TaxHelper.VatListItem vatList){
            this.date_from = String.valueOf(vatList.dateFrom);
            this.date_to = String.valueOf(vatList.dateTo);
            this.net = String.valueOf(vatList.net);
            this.percentage = String.valueOf(vatList.percentage);
            this.vat = String.valueOf(vatList.vat);
            this.total = String.valueOf(vatList.total);
            this.vat_code = String.valueOf(vatList.vatCode);
            this.has_accrual = String.valueOf(vatList.hasAccrual);
        }
        
        public MyTaxItem (String date_from, String date_to, String net, String percentage, String vat, String total, String vat_code, String has_accrual){
            this.date_from = date_from;
            this.date_to = date_to;
            this.net = net;
            this.percentage = percentage;
            this.vat = vat;
            this.total = total;
           	this.vat_code = vat_code;
            this.has_accrual = has_accrual;
        }
    }
    
    public class MyProduct{
        String id;
        String code;
        String name;
        
        public MyProduct(){}
        
        public MyProduct(Invoice_Item__c invoiceItem){
            this.id = invoiceItem.Product__c;
            this.code = invoiceItem.Product__r.ProductCode;
            this.name = invoiceItem.Product__r.Name;
        }
    }
    
    public class MySubscription{
        String id;
        String sbqq_start_date;
        
        public MySubscription(){}
        
        public MySubscription(Invoice_Item__c invoiceItem){
            this.id = invoiceItem.Subscription__c;
            this.sbqq_start_date = String.valueOf(invoiceItem.Subscription__r.SBQQ__StartDate__c);
        }
    }
    
    public class MyCreditNote{
        String reimbursed_invoice_id;
        String reimbursed_invoice_name;
        
        public MyCreditNote(){}
        
        public MyCreditNote(Invoice_Item__c invoiceItem){
            this.reimbursed_invoice_id = invoiceItem.Credit_Note__r.Reimbursed_Invoice__c;
            this.reimbursed_invoice_name = invoiceItem.Credit_Note__r.Reimbursed_Invoice__r.Name;
        }
    }
    
}