@isTest
global class Test_AddressValidationMock implements HttpCalloutMock{
    
	public String responseType;

    global Test_AddressValidationMock(final String responseType){
    	this.responseType = responseType;
    }
    
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.SERVICE_MASHERY);
        if(request.getEndpoint().contains(methodName)){
            response.setStatusCode(200);
        	response.setBody('{"access_token":"testToken","token_type":"bearer","expires_in":28800}');	    
        }
        else{
            if(responseType.equalsIgnoreCase(ConstantsUtil.ADDRESS_VALIDATION_OK))
                response.setBody('{"attributes":{"company_name":"comapanyNameABCDE","address":{"zip":1005,"street":"streetABCDE","location":"locationABCDE","street_number":"streetNumberABCDE","country":"countryABCDE"},"uid":"uidABCDE","hrid":"hridABCDE","phone":"111111","vatid":"vatIdABCDE","mobile":"222222","fax":"333333","legal_form":"legalForm","start_vat_obligation":"10.10.2001 11.00.00"},"result":"OK","source":"UID Register","request_date":"2020-02-27 20:13:00","response_date":"2020-02-27 20:14:00","error":{"error_code":"","error_description":""}}');
            else if(responseType.equalsIgnoreCase(ConstantsUtil.ADDRESS_VALIDATION_KO))
                response.setBody('{"result":"KO","attributes":{"hrid":null,"vatid":null,"legal_form":null,"uid":null,"address":null},"error":{"error_code":"1","error_description":"Generic Error"}}');
            response.setStatusCode(200);  
        }
        return response;
    }
}