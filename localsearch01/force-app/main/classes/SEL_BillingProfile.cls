/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date        : 28-05-2019
 * Sprint      : 2
 * Work item   : #90 #91 #92 #93
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
                #1: SF2-253 - Validate Account and Billing Address on Quote Creation
                    SF2-264 - Legal Address Validation
                    gcasola 07-22-2019
 */

public with sharing class SEL_BillingProfile {
    
    public static Billing_Profile__c getBillingProfileById (Id billingProfileId){

        return [SELECT Id, Customer__c, Customer__r.Name, Customer__r.UID__c, Billing_City__c, 
        Billing_Country__c, Billing_Street__c, Billing_Postal_Code__c,
        Mail_address__c, Phone__c, AddressValidated__c, Address_Integration__c, Customer__r.SkipAddressValidation__c
        FROM Billing_Profile__c 
        WHERE Id = :billingProfileId]; 

    }
    
    public static Map<Id, Billing_Profile__c> getBillingProfilesById(Set<Id> ids)
    {
        return new Map<Id, Billing_Profile__c>([SELECT Id, Customer__c, Customer__r.Name, Customer__r.UID__c, Billing_City__c, 
        Billing_Country__c, Billing_Street__c, Billing_Postal_Code__c,
        Mail_address__c, Phone__c, AddressValidated__c, Address_Integration__c, Customer__r.SkipAddressValidation__c
        FROM Billing_Profile__c 
        WHERE Id IN :ids]
        );
    }
    
    public static Map<Id, Billing_Profile__c> getBillingProfilesByAccountIdForMergingAccounts(Set<Id> accountIds)
    {
        Boolean first = true;
        String query = 'SELECT Id, Customer__c, Customer__r.Name, Customer__r.UID__c, Billing_City__c, ' +
                'Billing_Country__c, Billing_Street__c, Billing_Postal_Code__c, ' + 
                'Mail_address__c, Phone__c, AddressValidated__c, Address_Integration__c ' +
                'FROM Billing_Profile__c ' +
                'WHERE Customer__c IN (';
       for(Id accountId : accountIds)
       {
           if(!first)
               query += ',';
           query += '\'' + accountId +'\'';
       }
        query += ') ' +
             	'AND Is_Default__c = true ' +
            	'AND LastModifiedDate >= ' + System.now().addSeconds(-5) + 
            	'AND LastModifiedDate <= ' + System.now().addSeconds(5) +
            	'AND LastModifiedById = \''  + UserInfo.getUserId() + '\'';
        
        System.debug('>Query: ' + query);
        
        return new Map<Id, Billing_Profile__c>(
            [SELECT Id, Customer__c, Customer__r.Name, Customer__r.UID__c, Billing_City__c, 
                Billing_Country__c, Billing_Street__c, Billing_Postal_Code__c,
                Mail_address__c, Phone__c, AddressValidated__c, Address_Integration__c
                FROM Billing_Profile__c 
                WHERE Customer__c IN :accountIds
             	AND Is_Default__c = true
            	AND LastModifiedDate >= :System.now().addSeconds(-5)
            	AND LastModifiedDate <= :System.now().addSeconds(5)
            	AND LastModifiedById = :UserInfo.getUserId()]
        );
	}
}