/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Order Management - This class evaluates orders based on type, status and integration type.
* a case is created for manual orders.
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mazzarella Mara <mamazzarella@deloitte.it>
* @created        09-25-2019
* @systemLayer    Invocation
* @testClass	  
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-19      SPIII-1251 -  Modify Termination Case Logic for Manual Products
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class OrderCaseUtility {
    
    /*public class OrderCaseParameter{
        @InvocableVariable(required=true)
        public Id recId;
        @InvocableVariable(required=true)
        public String method;
    }
    
    @InvocableMethod(label='Manual-Activation Case' description='Manual Activation Case')
    public static void generateUpdateCase(List<OrderCaseParameter> orderCaseParameters){
        System.debug('generateUpdateCase '+orderCaseParameters);
        Set<Id> recordIds = new Set<Id>();
        Set<Id> recordIdsToUpdate = new Set<Id>();
        for(OrderCaseParameter param : orderCaseParameters){
            if(param.method == 'UPDATE'){
                recordIdsToUpdate.add(param.recId); 
            }
            else if(param.method == 'CREATE') {
                recordIds.add(param.recId);    
            }
        }
        if(!recordIds.isEmpty()){
        	generateCase(recordIds);
        }
        if(!recordIdsToUpdate.isEmpty()){
        	updateCase(recordIdsToUpdate);
        }
    }*/
    
        public static void generateCase(Set<Id> orderIds){
        System.debug('generateCase '+orderIds);
        Id rtId;

        List<OrderItem> orderItemtoUpdate = new List<OrderItem> ();
        Set<Order> orderToUpdate = new Set<Order>();
        List<RecordType> RTs = [select Id from RecordType where DeveloperName =: ConstantsUtil.CASE_RECORDT_MANUAL];
        if(!RTs.isEmpty()){
        	rtId =RTs[0].id;
        }
        Map<String,Manual_Activation_Case__mdt> settings = new Map<String,Manual_Activation_Case__mdt>(); // (mdt)product code, (mdt)
        Map<String,String> queueIdsMap = new  Map<String,String>();// product code , (QueueSobject)queue id 
        Map<Id,String> orderProductMap = new  Map<Id,String>(); // master order, master product code
        for(Manual_Activation_Case__mdt mdt : [select Id, Product_Code__c, Topic__c, Product__c from Manual_Activation_Case__mdt ]){
            settings.put( mdt.Product_Code__c, mdt);
        }
        for(OrderItem oItem : [SELECT   Id, Product2.ProductCode,Product2.Family, OrderId, Order.Custom_Type__c, SBQQ__Status__c,
                                        SBQQ__RequiredBy__c, RequiredByProductCode__c, Order.Status,
                                        SBQQ__QuoteLine__r.RequiredByProductCode__c, Product2.Product_Group__c
                               FROM     OrderItem 
                               WHERE    OrderId IN :orderIds]){

			if(oItem.SBQQ__RequiredBy__c == null){
				if(String.isNotBlank(oItem.RequiredByProductCode__c)){
					orderProductMap.put(oItem.OrderId,oItem.RequiredByProductCode__c);                         
				}
				else{
					orderProductMap.put(oItem.OrderId,oItem.SBQQ__QuoteLine__r.RequiredByProductCode__c);                        
				}
			}
            //Vincenzo Laudato - Order Management Enhancement
            String productGroupType = '';
            String currentProductGroup = oItem.Product2.Product_Group__c;
            if(oItem.Product2.Family == ConstantsUtil.PRODUCT2_FAMILY_DISUSED && !ConstantsUtil.PRODUCT2_DISUSED_EXCEPT.contains(oItem.Product2.ProductCode)){
            	currentProductGroup = ConstantsUtil.PRODUCT2_FAMILY_DISUSED;              
            } 
			if(String.isNotEmpty(currentProductGroup)){
				if(currentProductGroup.contains('MyCampaign')){
					productGroupType = oItem.Product2.ProductCode.contains('SEARCH') || oItem.Product2.ProductCode.contains('VISUAL') ? 'search' : 'social';
				} 
			}
                                   
            String currentOIStatus = oItem.SBQQ__Status__c;
                                   
            List<Order_Management__mdt> orderManagementMetadata;
			if(String.isEmpty(productGroupType)){
				orderManagementMetadata = [SELECT  Product_Group__c, Current_Status__c, Next_Status__c
                                           FROM    Order_Management__mdt
                                           WHERE   Product_Group__c = :currentProductGroup
                                           AND     Current_Status__c = :currentOIStatus
                                           LIMIT   1];
			}
			else{
				orderManagementMetadata = [SELECT  Product_Group__c, Current_Status__c, Next_Status__c
                                           FROM    Order_Management__mdt
                                           WHERE   Product_Group__c = :currentProductGroup
                                           AND 	   Product_Code__c = :productGroupType
                                           AND     Current_Status__c = :currentOIStatus
                                           LIMIT   1];
			}
            System.debug('orderManagementMetadata '+orderManagementMetadata);
            if(!orderManagementMetadata.isEmpty()){
                oItem.SBQQ__Status__c = orderManagementMetadata[0].Next_Status__c;
                oItem.Order.Status = orderManagementMetadata[0].Next_Status__c;
                
               	orderToUpdate.add(oItem.Order);
            }
            orderItemtoUpdate.add(oItem);
        }
        if(!orderItemtoUpdate.isEmpty()){
            update orderItemtoUpdate;
        }
        if(!orderToUpdate.isEmpty()){
           update new List<Order>(orderToUpdate);
        }
        List<Case> cases = new  List<Case>();
        for(Order o : [select id,Custom_Type__c,AccountId, Type,Filtered_Primary_Contact__c from Order where id in: orderIds]){
           
        	Case ordercase = new Case();
            orderCase.status = ConstantsUtil.CASE_NEW_STATUS;
        	orderCase.RecordTypeId = rtId;
            orderCase.Run_Assignment__c = true;
            if(orderProductMap.containsKey(o.id)){
                System.debug('orderProductMap '+orderProductMap);
                if(settings.containsKey(orderProductMap.get(o.id))){
                    System.debug('settings '+settings);
                	//orderCase.Topic__c = settings.get(orderProductMap.get(o.id)).Topic__c;
                    orderCase.Product__c = settings.get(orderProductMap.get(o.id)).Product__c;
                    System.debug('Topic__c '+orderCase.Topic__c);
                }
            }
            orderCase.AccountId = o.AccountId;
            orderCase.ContactId = o.Filtered_Primary_Contact__c;
            orderCase.Order__c = o.id;
            orderCase.Origin = ConstantsUtil.CASE_ORIGIN_AUTOMATIC;
            
            if(ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(o.Type) // SPIII-1251
              && ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(o.Custom_Type__c) ){
                orderCase.Subject = ConstantsUtil.CASE_ACTIVATION_UPG_SBJ;  
                orderCase.Topic__c = ConstantsUtil.CASE_ACTIVATION_TYPE;
                orderCase.Order_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
            }
            else if(ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(o.Type) // SPIII-1251 
              && ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(o.Custom_Type__c) ){
                orderCase.Subject = ConstantsUtil.CASE_TERMINATION_UPG_SBJ;
                orderCase.Topic__c = ConstantsUtil.CASE_TERMINATION_TYPE;
                orderCase.Order_Type__c = ConstantsUtil.ORDER_TYPE_TERMINATION;
            }
            else if(ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(o.Custom_Type__c)){
                orderCase.Subject = ConstantsUtil.CASE_TERMINATION_SBJ;
                orderCase.Topic__c = ConstantsUtil.CASE_TERMINATION_TYPE;
                orderCase.Order_Type__c = ConstantsUtil.ORDER_TYPE_TERMINATION;
            }
            else if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(o.Custom_Type__c)){
                orderCase.Subject = ConstantsUtil.CASE_TERMINATION_SBJ;
                orderCase.Topic__c = ConstantsUtil.CASE_TERMINATION_TYPE;
                orderCase.Order_Type__c = ConstantsUtil.ORDER_TYPE_CANCELLATION;
            }
            else{
                orderCase.Subject = ConstantsUtil.CASE_ACTIVATION_SBJ;
                orderCase.Topic__c = ConstantsUtil.CASE_ACTIVATION_TYPE;
                orderCase.Order_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
                System.debug('Topic__c '+orderCase.Topic__c);
            }    
            cases.add(ordercase);  

            System.debug('cases');
        } 
        if(!cases.isEmpty()){
            insert cases;
        }
    }
    
    public static void updateCase(Set<Id> orderIds){
        System.debug('updateCase '+orderIds);
        List<Case> cases = new  List<Case>();
        for(Case c : [select id, status from Case where Order__c in: orderIds AND RecordType.DeveloperName = :ConstantsUtil.CASE_RECORDT_MANUAL]){
            if( c.status != ConstantsUtil.CASE_CLOSED_STATUS){
                c.status = ConstantsUtil.CASE_CLOSED_STATUS;
                cases.add(c);
            }
        } 
        if(!cases.isEmpty()){
            update cases;
        }
    } 
}