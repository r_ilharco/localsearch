public without sharing class CustomChangeAccountController {    
    @AuraEnabled
    public static List <sObject> findByName(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '%' + searchKeyWord + '%';
       
        List<sObject> returnList = new List<sObject>();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name, Customer_Number__c from ' +ObjectName + ' where Name LIKE: searchKey OR Customer_Number__c LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static void updateAccount(String recordId, String acc) {
        Id userId = UserInfo.getUserId();
        Boolean isDM = false;
        try{
            system.debug('updateAccount');
            system.debug('recordId: ' + recordId);
            system.debug('acc: ' + acc);
            
            Map<String, Object> deserialized = (Map<String, Object>) JSON.deserializeUntyped(acc);
            Id accId;
            for(String currentKey : deserialized.keySet()){
                if(currentKey == 'Id'){
                    accId = String.valueOf(deserialized.get(currentKey));
                    break;
                }
            }
            List<PermissionSetAssignment> permissionSets = [SELECT Id, PermissionSetId,PermissionSet.Name FROM PermissionSetAssignment WHERE Assigneeid = :userId];
			Account a = [SELECT Id FROM Account WHERE Id = :accId LIMIT 1];
            Place__c p = [SELECT Id, New_Account__c, Account__c FROM Place__c WHERE Id = :recordId LIMIT 1];
            if(permissionSets.size() > 0) {
                for(PermissionSetAssignment ps : permissionSets){
                    if(ps.PermissionSet.Name == 'Data_Management'){
                        isDM = true;
                    }
                }
            }
            if(isDM){
                p.Account__c = a.id;
            }else{
                p.New_Account__c = a.Id;
            } 
            update p;
        }catch(Exception ex){
            system.debug('Exception: ' + ex);
        }
    }
}