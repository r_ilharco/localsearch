/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola <gcasola@deloitte.it>
* @created		  11-04-2019
* @systemLayer    Invocation 
* @TestClass 	  Test_AutomaticCreationTasksBatch
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser field removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29           
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class AutomaticCreationTasksBatch implements Database.batchable<sObject>, Schedulable {
    
    global void execute(SchedulableContext sc) {
        AutomaticCreationTasksBatch b = new AutomaticCreationTasksBatch(); 
        database.executebatch(b);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        ProductTaskConfiguration__mdt conf = [SELECT Id, TaskName__c, daysUntil__c FROM ProductTaskConfiguration__mdt WHERE TaskName__c = :Label.RenewalContractReminderTask_lbl];        
        Integer daysUntil = 0;
        
        if(conf.daysUntil__c != NULL && Integer.valueOf(conf.daysUntil__c) > 0)
        	daysUntil = Integer.valueOf(conf.daysUntil__c);
        
        String query = 'SELECT Id, Account.OwnerId, '
                                        + '(SELECT Id, SBQQ__EndDate__c, Subsctiption_Status__c, SBQQ__Product__r.Product_Group__c, SBQQ__Product__r.Name, SBQQ__Product__r.Tasks__c'
                                        + ' FROM SBQQ__Subscriptions__r'
                                        + ' WHERE('
                                        + ' (Place__c != \'\' AND SBQQ__Product__r.PlaceIDRequired__c = \'Yes\')'
                                        + ' OR'
                                        + ' (Place__c = \'\' AND SBQQ__Product__r.PlaceIDRequired__c = \'No\')'
                                        + ' OR'
                                        + ' (SBQQ__Product__r.PlaceIDRequired__c = \'Optional\')'
                                        + ')'
                                        + ' AND SBQQ__ProductOption__c = NULL'
                                        + ')'
                                        + ' FROM Contract'
                                        + ' WHERE EndDate = NEXT_N_DAYS:' + daysUntil
                                 		+ ' AND EndDate > TODAY'
                                        + ' AND Status = \'' + ConstantsUtil.CONTRACT_STATUS_ACTIVE + '\'';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        ProductTaskConfiguration__mdt conf = [SELECT Id, TaskName__c, daysUntil__c FROM ProductTaskConfiguration__mdt WHERE TaskName__c = :Label.RenewalContractReminderTask_lbl];
        List<Task> taskToInsert = new List<Task>();
        Integer daysUntil = 0;
        
        if(conf.daysUntil__c != NULL && Integer.valueOf(conf.daysUntil__c) > 0)
        	daysUntil = Integer.valueOf(conf.daysUntil__c);
        
        for(Contract c : scope)
        {
            if(c.SBQQ__Subscriptions__r.size() > 0)
            {
                for(SBQQ__Subscription__c sub : c.SBQQ__Subscriptions__r)
                {
                	if(String.isNotBlank(sub.SBQQ__Product__r.Tasks__c) && sub.SBQQ__Product__r.Tasks__c.toLowerCase().split(';').contains(Label.RenewalContractReminderTask_lbl.toLowerCase())
                      && ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE.equalsIgnoreCase(sub.Subsctiption_Status__c)
                      && sub.SBQQ__EndDate__c != NULL && (Date.today().addDays(daysUntil) == sub.SBQQ__EndDate__c))
                    {
                        Task t = new Task();
                    	t.Subject = Label.automaticCreationTask_subject_renewal_txt + ' ' + sub.SBQQ__Product__r.Name;
                        t.WhatId = c.Id;
                        t.OwnerId = c.Account.OwnerId;
                        t.ActivityDate = sub.SBQQ__EndDate__c;
                        taskToInsert.add(t);
                    }
                }
            }
        }
        
        if(taskToInsert.size() > 0)
        	insert taskToInsert;
    }
    
    global void finish(Database.BatchableContext info){     
    }
}