public class QuoteDocumentSendNotification {
	@InvocableMethod
    public static void processQuoteDocument(List<String> Ids){
        system.debug('processQuoteDocument');
        List<SBQQ__QuoteDocument__c> lst = [select id, Name,SBQQ__SignatureStatus__c, ManualProcess__c,
                                            SBQQ__Quote__r.SBQQ__SalesRep__c,
                                            SBQQ__Quote__r.SBQQ__SalesRep__r.LanguageLocaleKey
                                            from SBQQ__QuoteDocument__c 
                                            where id in :Ids];
        if(lst.isEmpty()) return;
        system.debug('processQuoteDocument'+lst);
        List<SBQQ__QuoteDocument__c> sendNotification = new List<SBQQ__QuoteDocument__c>();
        for(SBQQ__QuoteDocument__c qd : lst){
        	Boolean isCustomerSigned = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_IN_PROGRESS.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c);
            if(isCustomerSigned && !qd.ManualProcess__c){ sendNotification.add(qd); }
        }
        List<FeedItem> feedItemsToInsert = new List<FeedItem>();
        if(!sendNotification.isEmpty()) feedItemsToInsert.addAll(QuoteDocumentTriggerHelper.sendNotification(sendNotification, QuoteDocumentTriggerHelper.QUOTEDOCUMENT_NOTIFICATIONTYPE_DOCSIGNEDCUSTOMER));
        if(!feedItemsToInsert.isEmpty() && !Test.isRunningTest()) insert feedItemsToInsert;
        system.debug('processQuoteDocument:feedItemsToInsert'+feedItemsToInsert.size());
    }
}