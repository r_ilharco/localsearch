@isTest
public class RoleUtility_test {
     @testSetup
    static void setupData()
    {
        Test_DataFactory.insertFutureUsers();
    }
    
    @isTest 
     public static void test_getRoleSubordinateUsers() {
        Map<Id,UserRole> userroles = new Map<Id, UserRole>([select id from UserRole limit 2]);
     	RoleUtility.getRoleSubordinateUsers(userroles.keySet());
    }
    @isTest 
     public static void test_getRoleUsers() {
        Map<Id,UserRole> userroles = new Map<Id, UserRole>([select id from UserRole limit 2]);
        RoleUtility.getRoleUsers(userroles.keySet());
    }
}