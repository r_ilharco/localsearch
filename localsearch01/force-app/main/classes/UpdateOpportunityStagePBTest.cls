@isTest
public class UpdateOpportunityStagePBTest {
    
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    
    @isTest
    public static void testUpdateOpportunityStage(){
        try{
            insertBypassFlowNames();
            
            Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'test city';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '1234';        
            insert place;
            System.debug('Place: '+ place);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh'; 
            acc.BillingCity = 'test city';
            acc.BillingCountry = 'test country';
            acc.BillingPostalCode = '123';
            acc.BillingState = 'test state';
            acc.PreferredLanguage__c = 'German';       
            insert acc;
            System.Debug('Account: '+ acc); 
            
            place.Account__c = acc.Id;
            update place;
            System.debug('Place update: '+ place);
            
            Contact myContact = new Contact();
            myContact.AccountId = acc.Id;
            myContact.Phone = '07412345';
            myContact.Email = 'test@test.com';
            myContact.LastName = 'tlame';
            myContact.MailingCity = 'test city';
            myContact.MailingPostalCode = '123';
            myContact.MailingCountry = 'test country';        
            insert myContact;
            System.Debug('Contact: '+ myContact);  
            
            Pricebook2 prBook = new Pricebook2();
            prBook.Name = 'test p book';       
            insert prBook;
            System.Debug('Pr.Book '+ prBook);
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.Pricebook2Id = prBook.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';        
            insert opp;
            System.Debug('Opportunity '+ opp); 
            
            List<SBQQ__Quote__c> quoteList = [select Id, Name from SBQQ__Quote__c where SBQQ__Opportunity2__c =: opp.id];
            if(quoteList.size() > 0){
                for(SBQQ__Quote__c quoteHere : quoteList){
                    quoteHere.SBQQ__Primary__c = false; 
                }
                update quoteList;
                try{
                    delete quoteList;
                }catch(Exception e){         
                   System.Assert(true, e.getMessage());
                }  
            }
            
            Billing_Profile__c billProfile = new Billing_Profile__c();          
            billProfile.Customer__c = acc.Id;
            billProfile.Billing_Contact__c = myContact.Id;
            billProfile.Billing_Language__c = 'French';
            billProfile.Billing_City__c = 'test';
            billProfile.Billing_Country__c = 'test';
            billProfile.Billing_Name__c = 'test bill name';
            billProfile.Billing_Postal_Code__c = '12345';
            billProfile.Billing_Street__c = 'test 123 Secret Street';   
            billProfile.Channels__c = 'print';
            billProfile.Name = 'Test Bill Prof Name';
            insert billProfile;
            System.Debug('Bill Profile: '+ billProfile);
            
            SBQQ__Quote__c quot = new SBQQ__Quote__c();
            quot.SBQQ__Account__c = acc.Id;
            quot.SBQQ__Opportunity2__c =opp.Id;
            quot.SBQQ__PrimaryContact__c = myContact.Id;
            quot.Billing_Profile__c = billProfile.Id;
            quot.SBQQ__Type__c = 'Quote';
            quot.SBQQ__Status__c = 'Draft';
            quot.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
            quot.SBQQ__Primary__c = true;   
            insert quot;
            System.Debug('Quote nn : '+ quot);
            
            List<Product2> listProds = new List<Product2>();
            for(Integer i = 0; i < 3; i++) {
                Product2 pr = new Product2();
                pr.Name = 'XXX ' + i;
                pr.ProductCode = 'SLT00'+i;
                pr.IsActive=True;
                pr.SBQQ__ConfigurationType__c='Allowed';
                pr.SBQQ__ConfigurationEvent__c='Always';
                pr.SBQQ__QuantityEditable__c=True;
                
                pr.SBQQ__NonDiscountable__c=False;
                pr.SBQQ__SubscriptionType__c='Renewable'; 
                pr.SBQQ__SubscriptionPricing__c='Fixed Price';
                pr.SBQQ__SubscriptionTerm__c=12; 
                
                listProds.add(pr);
            }
        
        	insert listProds;
            System.Debug('Product list: '+ listProds);
            
            Product2 mainProduct = [SELECT Id, Name FROM Product2 WHERE ProductCode = 'SLT001' Limit 1];
            
            SBQQ__ProductFeature__c productFeature = new SBQQ__ProductFeature__c();
            productFeature.Name= 'test product feature';
            productFeature.SBQQ__Number__c= 1;
            insert productFeature;
            System.Debug('ProductFeature: '+ productFeature);
            
            SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c();
            productOption.SBQQ__Number__c= 2;
            insert productOption;
            System.Debug('ProductOption: '+ productOption);
        	
            SBQQ__QuoteLine__c mainQuoteLine = new SBQQ__QuoteLine__c();
            mainQuoteLine.SBQQ__Quote__c = quot.Id; 
            mainQuoteLine.SBQQ__Product__c = mainProduct.Id; 
            mainQuoteLine.SBQQ__Quantity__c = 1;
            mainQuoteLine.SBQQ__Bundled__c = true;
            mainQuoteLine.SBQQ__Bundle__c = false;
            mainQuoteLine.Place__c = place.Id;
            //mainQuoteLine.Place_Name__c = place.Name;
            mainQuoteLine.SBQQ__Renewal__c = false;
            mainQuoteLine.SBQQ__NetPrice__c = 10.00;
           
            insert mainQuoteLine;
            system.debug('main ql: ' + mainQuoteLine);
                       
            List<Product2> optionProducts = [SELECT Id, Name FROM Product2 WHERE ProductCode != 'SLT001' Limit 2];
            
            List<SBQQ__QuoteLine__c> quoteLineList = new List<SBQQ__QuoteLine__c>();
            for(Product2 optProduct : optionProducts) {
                SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
                quoteLine.SBQQ__Quote__c = quot.Id;
                quoteLine.SBQQ__Product__c = optProduct.Id;
                quoteLine.SBQQ__RequiredBy__c = mainQuoteLine.Id;
                quoteLine.SBQQ__Bundled__c = true;
                quoteLine.SBQQ__OptionType__c = 'Component';
                quoteLine.SBQQ__Bundle__c = false;
                quoteLine.SBQQ__Quantity__c = 1;
                quoteLine.SBQQ__Renewal__c = false;
            	quoteLine.SBQQ__NetPrice__c = 10.00;
                quoteLine.SBQQ__ProductOption__c = productOption.Id;
                quoteLineList.add(quoteLine);
            }
            
            if(!quoteLineList.isEmpty()){
                insert quoteLineList;
            }
            
            List<Opportunity> oppList = [select Id, Name, StageName from Opportunity];
            System.Debug('Opportunity list1: '+ oppList);
                       
            //criteria 2            
            quot.SBQQ__Status__c = 'Presented';           
            update quot;
            
            List<Opportunity> oppList2 = [select Id, Name, StageName from Opportunity];
            System.Debug('Opportunity list2: '+ oppList2);
            
        }catch(Exception e){         
            System.Debug('error: '+ e.getMessage());
           System.Assert(false, e.getMessage());
        }
    }
    
   

}