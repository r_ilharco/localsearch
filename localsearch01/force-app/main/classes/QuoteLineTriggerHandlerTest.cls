/*    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SPIII-1678 API Name changed 
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-07              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class QuoteLineTriggerHandlerTest {

static ByPassFlow__c insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management';
        insert byPassFlow;
        
        return byPassFlow;
    }
    
    @TestSetup
    public static void testSetup(){
         ByPassFlow__c bp = insertBypassFlowNames();
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        Opportunity oppo =  Test_DataFactory.generateOpportunity('Test Opportunity', acc[0].Id);
        insert oppo;
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last Name Test', 1);
        insert cont;
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(oppo.Id, acc[0].Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        insert quote1;
        Product2 product = generateProduct();
        insert product;
        
    }
    
   /* @isTest
    public static void testQuoteLineTriggerHandler(){
        try{            
            QuoteLineTriggerHandler.triggerName = 'QuoteLineTrigger';
            QuoteLineTriggerHandler.triggerHandler(null,null,null,true,true,false,false);
            QuoteLineTriggerHandler.triggerHandler(null,null,null,true,false,true,false);
            QuoteLineTriggerHandler.triggerHandler(null,null,null,true,false,false,true);
            QuoteLineTriggerHandler.triggerHandler(null,null,null,false,true,false,false);
            QuoteLineTriggerHandler.triggerHandler(null,null,null,false,false,true,false);
            QuoteLineTriggerHandler.triggerHandler(null,null,null,false,false,false,true);
            
            boolean disableTrigger1 = QuoteLineTriggerHandler.disableTrigger;
                
            Bypass_Triggers__c byPassTrigger = new Bypass_Triggers__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.Trigger_Name__c = 'test1,test2';
            insert byPassTrigger;
            
            boolean disableTrigger2 = QuoteLineTriggerHandler.disableTrigger;

            QuoteLineTriggerHandler.disableTrigger = true;
            QuoteLineTriggerHandler.disableTrigger = null;
            boolean disableTriggerLast = QuoteLineTriggerHandler.disableTrigger;
            
        }catch(Exception e){                                
            //System.Assert(false, e.getMessage());
        }
    }*/
    
    @isTest
    public static void test_BeforeInsert(){
        QuoteTriggerHandler.disableTrigger = true;
        SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Type__c 
                                            FROM SBQQ__Quote__c LIMIT 1];
            Product2 product = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                                 ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                                 Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                                 Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                                 Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                                    FROM Product2 LIMIT 1 ];
            SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(1), null, null, null, 'Annual');
            quoteLine.Samba_Migration__c = false;
            //quoteLine.SBQQ__AdditionalDiscountAmount__c = null;
            //quoteLine.SBQQ__PricebookEntryId__c = null;
            system.debug('LIMITS -->' + Limits.getDMLStatements());
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        Test.startTest();
            system.debug('LIMITS1 -->' + Limits.getDMLStatements());
            insert quoteLine;
            system.debug('LIMITS2 -->' + Limits.getDMLStatements());
        Test.stopTest(); 
        QuoteTriggerHandler.disableTrigger = false;
    }
    
   /* @isTest
    public static void test_BeforeInsertRequiredByTrue(){
        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Type__c 
                                            FROM SBQQ__Quote__c LIMIT 1];
            Product2 product = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                                 ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                                 Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                                 Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                                 Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                                    FROM Product2 LIMIT 1 ];
            SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(1), null, null, null, 'Annual');
            quoteLine.Samba_Migration__c = false;
            insert quoteLine;
            SBQQ__QuoteLine__c quoteLine1 = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(1), null, null, quoteLine.Id, 'Annual');
            quoteLine.Samba_Migration__c = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        Test.startTest();   
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            insert quoteLine1;
        Test.stopTest(); 
        QuoteTriggerHandler.disableTrigger = false; 
    } */
    
    
    /* @isTest
    public static void test_BeforeUpdate(){
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c 
                                            FROM SBQQ__Quote__c LIMIT 1];
            Product2 product = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                                 ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                                 Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                                 Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                                 Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                                    FROM Product2 LIMIT 1 ];
            SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(1), null, null, null, 'Annual');
            quoteLine.Samba_Migration__c = true;
            quoteLine.SBQQ__AdditionalDiscountAmount__c = null;
            insert quoteLine;
            //quoteLine.SBQQ__PricebookEntryId__c = null;
            QuoteLineTriggerHandler.disableTrigger = true;
        Test.startTest();   
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            quoteLine.SBQQ__PricebookEntryId__c = null;
            quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_AMENDMENT;
            update quoteLine;
        Test.stopTest(); 
        QuoteTriggerHandler.disableTrigger = false;
    } */
    
    
    @isTest
    public static void test_BeforeDelete(){
      QuoteLineTriggerHandler.disableTrigger = true;
           SBQQ__Quote__c quote = [SELECT SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
                                            Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c 
                                            FROM SBQQ__Quote__c LIMIT 1];
            Product2 product = [SELECT Id, Name, Family, SBQQ__Component__c, SBQQ__NonDiscountable__c, SBQQ__SubscriptionTerm__c, SBQQ__SubscriptionType__c,
                                 ExternalKey_ProductCode__c, KSTCode__c, KTRCode__c, KTRDesignation__c, Base_term_Renewal_term__c, Billing_Group__c,
                                 Configuration_Model__c, Downgrade_Disable__c, Downselling_Disable__c, Eligible_for_Trial__c, One_time_Fee__c,
                                 Priority__c, Product_Group__c, Production__c, Subscription_Term__c, Upgrade_Disable__c, Upselling_Disable__c,
                                 Credit_Account__c, ProductCode, SBQQ__AssetAmendmentBehavior__c, SBQQ__AssetConversion__c
                                    FROM Product2 LIMIT 1 ];
            SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today().addDays(1), null, null, null, 'Annual');
            quoteLine.Samba_Migration__c = true;
            insert quoteLine;
        QuoteLineTriggerHandler.disableTrigger = false;
         Test.startTest();  
            delete quoteLine;
        Test.stopTest(); 
            
        QuoteLineTriggerHandler.disableTrigger = false;
    }
        
    public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MCOBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }

}