global class DeleteAllocationOnExpireBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts{
    
    global void execute(SchedulableContext sc) {
        DeleteAllocationOnExpireBatch b = new DeleteAllocationOnExpireBatch(); 
        database.executebatch(b, 10);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String acceptedStageName = ConstantsUtil.Quote_StageName_Accepted;
        return Database.getQueryLocator('SELECT Id, Ad_Context_Allocation__c,SBQQ__Quote__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__r.SBQQ__ExpirationDate__c = YESTERDAY AND SBQQ__Quote__r.SBQQ__Status__c != :acceptedStageName');
    }
    
    global void execute(Database.BatchableContext info, List<SBQQ__QuoteLine__c> scope){
        List<Log__c> logsToInsert = new List<Log__c>();
        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
        Map<Id, Set<String>> quoteToAllocationIds = new Map<Id,Set<String>>();
        for(SBQQ__QuoteLine__c quoteLine : scope){
            if(String.isNotBlank(quoteLine.Ad_Context_Allocation__c)){
                quoteLines.add(quoteLine);
            }
        }
        for(SBQQ__QuoteLine__c currentQL : quoteLines){
            if(String.isNotBlank(currentQL.Ad_Context_Allocation__c)){
                Map<String,String> parameters = new Map<String,String>();
                parameters.put('ad_context_allocation', currentQL.Ad_Context_Allocation__c);
                String correlationId = SRV_DeleteAllocationCallout.generateCorrelationId(currentQL.Id);
                SRV_DeleteAllocationCallout.DeleteAllocationBatchResponse deleteAllocationBatchResponse = SRV_DeleteAllocationCallout.deleteAllocationCallout(parameters, correlationId, currentQL.SBQQ__Quote__c);
                if(deleteAllocationBatchResponse != null && deleteAllocationBatchResponse.isSuccess){
                    if(!quoteToAllocationIds.containsKey(currentQL.SBQQ__Quote__c)){
                        quoteToAllocationIds.put(currentQL.SBQQ__Quote__c, new Set<String>{currentQL.Ad_Context_Allocation__c});
                    }
                    else{
                        quoteToAllocationIds.get(currentQL.SBQQ__Quote__c).add(currentQL.Ad_Context_Allocation__c);
                    }
                }
                logsToInsert.add(deleteAllocationBatchResponse.integrationTask);
            }
        }
        if(!logsToInsert.isEmpty()) insert logsToInsert;
        if(!quoteToAllocationIds.isEmpty()){
            List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
            List<SBQQ__Quote__c> quotes = [SELECT Id, Ad_Allocation_Ids__c FROM SBQQ__Quote__c WHERE Id IN :quoteToAllocationIds.keySet()];
            for(SBQQ__Quote__c currentQuote : quotes){
                if(String.isNotBlank(currentQuote.Ad_Allocation_Ids__c)){
                    List<String> adAllocationIdsSplitted = currentQuote.Ad_Allocation_Ids__c.split(';');
					Set<String> adAllocationIdsSplittedSet = new Set<String>(adAllocationIdsSplitted);
                    if(!quoteToAllocationIds.get(currentQuote.Id).isEmpty()){
                        adAllocationIdsSplittedSet.removeAll(quoteToAllocationIds.get(currentQuote.Id));
                    }
                    
					String newAdAllocationIds = '';
                    if(!adAllocationIdsSplittedSet.isEmpty()) newAdAllocationIds = String.join( new List<String>(adAllocationIdsSplittedSet), ';' );
                    currentQuote.Ad_Allocation_Ids__c = newAdAllocationIds;
                    if(String.isBlank(newAdAllocationIds)) currentQuote.Allocation_not_empty__c = false;
					else currentQuote.Allocation_not_empty__c = true;
                    quotesToUpdate.add(currentQuote);
                }
            }
            QuoteTriggerHandler.disableTrigger = true;
			if(!quotesToUpdate.isEmpty()) update quotesToUpdate;
			QuoteTriggerHandler.disableTrigger = false;			            
        }
    }
    global void finish(Database.BatchableContext info){     
    }
}