// TEMP CLASS TO PRODUCE XML DOCUMENTS FOR ABACUS UPLOAD INTERFACE
public class AbacusFileJob implements Database.Batchable<sObject>, Database.AllowsCallouts  {

    public List<sObject> start(Database.BatchableContext context) {
        // prepare data
        // Select invoices and related data with invoice status 'Collected' && 	Registered = false        
    	return [SELECT Id, Name, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c,
			Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
            Customer__r.Customer_Number__c, Customer__r.Name, Open_Amount__c, Payment_Reference__c,
            Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_City__c,
			Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c
			FROM Invoice__c
            WHERE Booking_Status__c = :ConstantsUtil.ABACUS_STATUS_DRAFT 
            AND (
                (Process_Mode__c = :ConstantsUtil.INVOICE_PROCESS_MODE_SWISSLISTMIG AND Is_Partially_Payed__c = TRUE)
                OR 
                (Process_Mode__c != :ConstantsUtil.INVOICE_PROCESS_MODE_SWISSLISTMIG AND Status__c = :ConstantsUtil.INVOICE_STATUS_COLLECTED)
            )
            LIMIT 50000];
    }
    
    public void execute(Database.BatchableContext context, List<sObject> dataset) {
        
		List<Invoice__c> invoices = (List<Invoice__c>)dataset;
		
        List<Invoice_Order__c> invoiceOrders = [
            SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
			(SELECT Accrual_From__c, Accrual_To__c, Amount__c, Tax__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
                Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c, Credit_Account__c,
                KSTCode__c, KTRCode__c, Product__r.ProductCode, Product__r.Name, Product__c, Subscription__c,  Credit_Note__r.Reimbursed_Invoice__r.Name,
             	Credit_Note__r.Reimbursed_Invoice__c, Subscription__r.SBQQ__StartDate__c
             FROM Invoice_Items__r ORDER BY Line_Number__c
			),
            Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
            FROM Invoice_Order__c where Invoice__c in :invoices
            ORDER BY Item_Row_Number__c
        ];
        
        // Build a map for lower levels, instead of calling SOQL for each invoice (2 level subqueries aren't allowed)
        Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = new Map<Id, List<Invoice_Order__c>>();
        for(Invoice_Order__c invoiceOrder : invoiceOrders) {
            List<Invoice_Order__c> invoiceOrderList;
            if(invoiceOrdersMap.containsKey(invoiceOrder.Invoice__c)) {
                invoiceOrderList = invoiceOrdersMap.get(invoiceOrder.Invoice__c);
            } else {
                invoiceOrderList = new List<Invoice_Order__c>();
                invoiceOrdersMap.put(invoiceOrder.Invoice__c, invoiceOrderList);                
            }
            invoiceOrderList.Add(invoiceOrder);
        }
        
		List<Invoice__c> invoiceUpdates = new List<Invoice__c>();
        
        for(Invoice__c invoice : invoices) {
            try {
                if(!BillingHelper.checkMinTotalAllowed(invoice)) {
	                ErrorHandler.logWarning('Billing', 'Billing.sendToAbacus', ErrorHandler.WarningCode.W_BILL_TOTAL_BELOW_LIMIT, 'Bill total below limit', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice);
                    invoiceUpdates.add(new Invoice__c(Id=invoice.Id, Status__c = ConstantsUtil.INVOICE_STATUS_IN_COLLECTION));
                    continue;
                }
                List<Invoice_Order__c> invoiceGroups = invoiceOrdersMap.get(invoice.Id);                
                Dom.Document doc = new Dom.Document();
                Dom.XmlNode abaConnectContainer = doc.createRootElement('AbaConnectContainer', '', '');
				abaConnectContainer.addChildElement('TaskCount', '', '').addTextNode('1');
                Dom.XmlNode task = abaConnectContainer.addChildElement('Task', '', '');
               	Dom.XmlNode parameter = task.addChildElement('Parameter', '', '');
                parameter.addChildElement('Application', '', '').addTextNode('DEBI');
                parameter.addChildElement('Id', '', '').addTextNode('Belege');
                parameter.addChildElement('MapId', '', '').addTextNode('AbaDefault');
                parameter.addChildElement('Mandant', '', '').addTextNode(BillingHelper.setting.Abacus_Mandant__c); // 8284
                parameter.addChildElement('Version', '', '').addTextNode('2015.00');
 				Dom.XmlNode transact = task.addChildElement('Transaction', '', '');
                transact.setAttribute('id', '1');

                Dom.XmlNode document = transact.addChildElement('Document', '', '');
                document.setAttribute('mode', 'SAVE');
                putTextElement(document, 'DocumentCode', invoice.Invoice_Code__c);
                putTextElement(document, 'CustomerNumber', AbacusHelper.getAbaCustomer(invoice));
                putTextElement(document, 'Number', invoice.Name.replaceAll('[^0-9]', ''));
                putTextElement(document, 'Reference', invoice.Correlation_Id__c);
                putTextElement(document, 'AccountReceivableDate', AbacusHelper.toISO8601String(AbacusHelper.validTaxMonth(invoice.Invoice_Date__c)));
                putTextElement(document, 'Currency', 'CHF');
                putTextElement(document, 'Amount', invoice.Total__c);
                putTextElement(document, 'KeyAmount', invoice.Total__c);
                putTextElement(document, 'ReminderProcedure', ConstantsUtil.ABACUS_REMINDER_PROCEDURE);
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                    putTextElement(document, 'GroupNumber1', invoice.Name.replaceAll('[^0-9]', ''));
                } else {
                    // Should not group Credit Notes, so the only item is related to the generating Credit Note, which points to the reimbursed invoice
                    Credit_Note__c cn = invoiceGroups[0].Invoice_Items__r[0].Credit_Note__r;
                    if(cn != null && cn.Reimbursed_Invoice__r !=null) {
                    	putTextElement(document, 'GroupNumber1', cn.Reimbursed_Invoice__r.Name.replaceAll('[^0-9]', ''));
                    } else {
                        // TODO: Appropriate error. Check also invoice total >= cn total
                    	throw new AbacusHelper.AbacusHelperException('Credit note without reference to Invoice', ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                    }
                }
                putTextElement(document, 'GroupNumber2', '0');
                putTextElement(document, 'GroupNumber3', '0');
                putTextElement(document, 'PaymentCentre', '0');
                putTextElement(document, 'PaymentOrderProcedure', ConstantsUtil.ABACUS_PAYMENT_ORDER_PROCEDURE);
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                	putTextElement(document, 'PaymentReferenceLineType', ConstantsUtil.ABACUS_PAYMENT_REFERENCE_LINE_TYPE);
                	putTextElement(document, 'PaymentReferenceLine', invoice.Payment_Reference__c);
                	putTextElement(document, 'CollectiveAccount', ConstantsUtil.ABACUS_COLLECTIVE_ACCOUNT);
                }
                
                integer lineCount = 1;

                for(Invoice_Order__c invoiceGroup : invoiceGroups) {
                    for(Invoice_Item__c invoiceItem : invoiceGroup.Invoice_Items__r) {
                        // split per tax schedule periods. Need: amount (gross), tax, tax from/to (TaxHelper: split period)
                        List<TaxHelper.VatListItem> taxItems = TaxHelper.getVatItems(invoiceItem.Tax_Code__c, invoiceItem.Accrual_From__c, invoiceItem.Accrual_To__c, invoiceItem.Amount__c, Date.today());
                        decimal runTax = 0;
                        decimal runTotal = 0;
                        for(integer taxLine = 0; taxLine < taxItems.size(); taxLine++) {
                            TaxHelper.VatListItem taxItem = taxItems[taxLine];
                            decimal total = (taxItem.total * 100.0).round(RoundingMode.HALF_EVEN) / 100.0;
                            decimal tax = (taxItem.vat * 100.0).round(RoundingMode.HALF_EVEN) / 100.0;
                            // Last row absorbs rounding errors
                            if(taxLine == taxItems.size() -1 ) {
                                total = invoiceItem.Total__c - runTotal;
                                tax = invoiceItem.Tax__c - runTax;
                            } else {
                                runTotal += total;
                             	runTax += tax;   
                            }
                        
                            Dom.XmlNode line = document.addChildElement('LineItem', '', '');
                            line.setAttribute('mode', 'SAVE');
                            putTextElement(line, 'Number', lineCount /* invoiceItem.Line_Number__c */);
                            putTextElement(line, 'Amount', total);
                            putTextElement(line, 'KeyAmount', total);
                            putTextElement(line, 'CreditAccount', invoiceItem.Credit_Account__c);
                            // Phase 2 + Credit costs from finance
                            /*  
                            line.addChildElement('Project', abaDocTypeNS, '').addTextNode('Phone Book Entry');
                            */
                            putTextElement(line, 'CreditCostCentre1', invoiceItem.KSTCode__c);
                            putTextElement(line, 'CreditCostCentre2', invoiceItem.KTRCode__c);
    
                            // Tax code in product: 31 or 32 (High vat, Low vat). 30 for vat excluded, 29 for rounding
                            if(invoice.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_STANDARD) {
                                putTextElement(line, 'TaxCode', AbacusHelper.convertTaxCode(taxItem.vatCode));
                            } else if(invoice.Tax_Mode__c == ConstantsUtil.BILLING_TAX_MODE_EXEMPT){
                                putTextElement(line, 'TaxCode', '30');
                            }
                            putTextElement(line, 'TaxIncluded', ConstantsUtil.ABACUS_TAX_INCLUDED);
                            putTextElement(line, 'TaxDateValidFrom', AbacusHelper.toISO8601String(taxItem.dateFrom));
                            putTextElement(line, 'TaxAmount', -tax);
                            putTextElement(line, 'KeyTaxAmount', -tax);
                            putTextElement(line, 'Text', invoiceItem.Description__c.mid(0,80));
                            if(taxItem.hasAccrual) {
                                putTextElement(line, 'AccrualNumber', ConstantsUtil.ABACUS_ACCRUAL_NUMBER);
                                putTextElement(line, 'AccrualFrom', AbacusHelper.toISO8601String(taxItem.dateFrom));
                                putTextElement(line, 'AccrualTo', AbacusHelper.toISO8601String(taxItem.dateTo));
                            }
     
                            if(invoiceItem.Product__r != null) {
                                putTextElement(line, '_USERFIELD6', invoiceItem.Product__r.ProductCode);
                                putTextElement(line, '_USERFIELD7', invoiceItem.Product__r.Name);
                            }
                            // putTextElement(line, '_USERFIELD9', 'Book Entry Edition');
                            putTextElement(line, '_USERFIELD10', invoiceItem.Sales_Channel__c);
                            if(invoiceGroup.Contract__r != null) {
                                putTextElement(line, '_USERFIELD14', invoiceGroup.Contract__r.ContractNumber);
                            }
                            putTextElement(line, '_USERFIELD15', invoiceItem.Line_Number__c);
                            if(invoiceItem.Subscription__r != null) {
                                putTextElement(line, '_USERFIELD16', AbacusHelper.toISO8601String(invoiceItem.Subscription__r.SBQQ__StartDate__c));
                            }
                            lineCount++;
                        }
                    }
                }
                
                if(invoice.Rounding__c != null && invoice.Rounding__c != 0) {
                    Dom.XmlNode line = document.addChildElement('LineItem', '', '');
                    line.setAttribute('mode', 'SAVE');
                    putTextElement(line, 'Number', lineCount);
                    putTextElement(line, 'Amount', invoice.Rounding__c);
                    putTextElement(line, 'KeyAmount', invoice.Rounding__c);
                    putTextElement(line, 'CreditAccount', ConstantsUtil.ABACUS_ROUNDING_CREDIT_ACCOUNT);
                    putTextElement(line, 'CreditCostCentre1', ConstantsUtil.ABACUS_ROUNDING_CREDIT_COST_CENTER);
                    putTextElement(line, 'TaxCode', '29');
                    putTextElement(line, 'TaxIncluded', ConstantsUtil.ABACUS_TAX_INCLUDED);
                    putTextElement(line, 'TaxDateValidFrom', AbacusHelper.toISO8601String(invoice.Invoice_Date__c));
                    putTextElement(line, 'TaxAmount', '0');
                    putTextElement(line, 'KeyTaxAmount', '0');
                    putTextElement(line, 'Text', ConstantsUtil.ABACUS_ROUNDING_DESCRIPTION);
                    putTextElement(line, '_USERFIELD7', ConstantsUtil.ABACUS_ROUNDING_DESCRIPTION);
                }
                Dom.XmlNode paymentTerm = document.addChildElement('PaymentTerm', '', '');
                paymentTerm.setAttribute('mode', 'SAVE');
                if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_FAK) {
                    putTextElement(paymentTerm, 'Number', '60');
                } else if(invoice.Invoice_Code__c == ConstantsUtil.BILLING_DOC_CODE_GUT) {
                    putTextElement(paymentTerm, 'Number', '1');
                }
                putTextElement(paymentTerm, 'CopyFromTable', 'true');
                putTextElement(paymentTerm, 'Type', '0');
                
                putTextElement(document, '_USERFIELD14', invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''));
                putTextElement(document, '_USERFIELD15', invoice.Customer__r.Name);
                putTextElement(document, '_USERFIELD16', invoice.Billing_Profile__r.Billing_Street__c);
                // putTextElement(document, '_USERFIELD17', invoice.Billing_Profile__r.Billing_State__c);
                putTextElement(document, '_USERFIELD18', invoice.Billing_Profile__r.Billing_Country__c);
                putTextElement(document, '_USERFIELD19', invoice.Billing_Profile__r.Billing_Postal_Code__c);
                putTextElement(document, '_USERFIELD20', invoice.Billing_Profile__r.Billing_City__c);

System.debug(doc.toXmlString());                
                invoiceUpdates.add(new Invoice__c(Id=invoice.Id, Booking_Status__c = ConstantsUtil.ABACUS_STATUS_SENT));
                ErrorHandler.logInfo('Billing', 'Billing.abacusFileJob', 'Bill produced for Abacus', doc.toXmlString(), invoice.Correlation_Id__c, invoice);
            } catch (Exception ex) {
                ErrorHandler.logError('Billing', 'Billing.abacusFileJob', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice);
            }
		}
        Savepoint savePt;
        if(!Test.isRunningTest()) { savePt = Database.setSavepoint(); }
        try {
            if(invoiceUpdates!=null && invoiceUpdates.size() > 0) {
            	update invoiceUpdates;
            }
        } catch (Exception ex) {
            if(!Test.isRunningTest()) { Database.rollback(savePt); }
            ErrorHandler.log(System.LoggingLevel.ERROR, 'Billing', 'Billing.sendToAbacus', ex, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);                
        }
    }
	
    public void finish(Database.BatchableContext context) {
    }
    
    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content) {
        if(parent == null || string.isBlank(elementName)) {
            return null;
        }        
        return putTextElement(parent, elementName, content, parent.getNamespace());
    }

    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content, string namespace) {
    	return putTextElement(parent, elementName, content, namespace, null);
    }
    
    @testVisible
    private static Dom.XmlNode putTextElement(Dom.XmlNode parent, string elementName, object content, string namespace, Map<string, string> attributes) {
        if(parent == null || string.isBlank(elementName)) {
            return null;
        }
        string text;
        if(content == null) {
            text = '';
        } else {
          	text = string.valueOf(content);
        }
        Dom.XmlNode child = parent.addChildElement(elementName, namespace, parent.getPrefixFor(namespace));
		child.addTextNode(text);
        if(attributes != null && attributes.size() > 0) {
            for(string key : attributes.keySet()) {
                child.setAttribute(key, attributes.get(key));
            }
        }
        return child;
    }
}