@isTest
public class Test_CreditNoteTriggerHandler {

    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management';
        insert byPassFlow;
    }
    
    @TestSetup
    static void setup(){
        /*insertBypassFlowNames();
        test_datafactory.insertFutureUsers();*/
        Test_ContractUtility.test_setupData();
        Account account = [SELECT Id FROM Account LIMIT 1];
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        Invoice__c invoice = new Invoice__c(Name = 'InvoiceTest',
                                           Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED,
                                           Customer__c = account.Id, Amount__c = 500);
        insert invoice;
        Invoice_Order__c invoiceOrder = new Invoice_Order__c(Invoice__c = invoice.Id,
                                                             Contract__c = contract.Id);
        insert invoiceOrder;
        SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id AND SBQQ__RequiredById__c = null LIMIT 1];
        Invoice_Item__c invoiceItem = new Invoice_Item__c(Invoice_Order__c = invoiceOrder.Id,
                                                          Subscription__c = sub.Id, 
                                                          Amount__c = 150,
                                                          Accrual_From__c = Date.today());
        insert invoiceItem;        
    }
    
	@isTest 
    static void testSetDisableTrigger_1(){
        
        Test.startTest();
        CreditNoteTriggerHandler.disableTrigger = True;
        Test.stopTest();
        
    }
    
    @isTest 
    static void testGetDisableTrigger_2(){
        
        Test.startTest();
        Bypass_Triggers__c bt = new Bypass_Triggers__c(Trigger_Name__c='test');
        insert bt;
        Boolean value = CreditNoteTriggerHandler.disableTrigger;
        Test.stopTest();
        
    }
    
    @isTest 
    static void testUpdate(){
        /*user u = [select id from user where Code__c = 'CFO'][0];
        system.runas(u){*/
            Account a = [SELECT Id FROM Account LIMIT 1][0];
            Contract c = [SELECT Id FROM Contract LIMIT 1][0];
        	Invoice__c inv = [SELECT Id FROM Invoice__c LIMIT 1];
        	SBQQ__Subscription__c sub = [SELECT Id FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: c.Id AND SBQQ__RequiredById__c = null LIMIT 1];
            Credit_Note__c cn = new Credit_Note__c(Account__c=a.Id, Contract__c=c.Id, Execution_Date__c=System.today(), Value__c=105, manual__c=true, Subscription__c = sub.Id,
                                                  Reimbursed_Invoice__c = inv.Id, Period_From__c = Date.today(), Period_To__c = Date.today().addDays(1));
            insert cn;
        	Invoice_Item__c invoiceItem = [SELECT Id, Credit_Note__c FROM Invoice_Item__c LIMIT 1];
        	invoiceItem.Credit_Note__c = cn.Id;
        	update invoiceItem;
            cn.Description__c = 'test';
        	cn.Period_From__c = Date.today().addDays(10);
            Test.startTest();
        	try{
            	update cn;
            }catch(Exception e){
                
            }
            Test.stopTest();
        //}        
    }
    
    @isTest 
    static void testDeleteUndelete(){
        Account a = [SELECT Id FROM Account LIMIT 1][0];
        Contract c = [SELECT Id FROM Contract LIMIT 1][0];
        Credit_Note__c cn = new Credit_Note__c(Account__c=a.Id, Contract__c=c.Id, Execution_Date__c=System.today(), Value__c=105);
        insert cn;
        Test.startTest();
        delete cn;
        undelete cn;
        Test.stopTest();
        
    }

   /* @istest static void test_creditnotetriggerhelper(){

 Account a = [SELECT Id FROM Account LIMIT 1][0];
        profile p = [select id,name from profile where name = 'Finance' limit 1];
        string profid = p.id;
        user u = [select profileid,id,name from user where profileid =: profid limit 1];
        
        Contract c = [SELECT Id FROM Contract LIMIT 1][0];
        Credit_Note__c cn = new Credit_Note__c(Account__c=a.Id, Contract__c=c.Id, Execution_Date__c=System.today(), Value__c=105, manual__c=true);
        insert cn;
        cn.Description__c = 'test';
        Test.startTest();
        system.runas(u){
        update cn;
        }
        Test.stopTest();

}    */


}