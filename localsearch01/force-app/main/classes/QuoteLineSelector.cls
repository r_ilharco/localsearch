/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-02-2019
 * Sprint      : 1
 * Work item   : 19
 * Testclass   :
 * Package     : 
 * Description : Selector Object for QuoteLine
 * Changelog   : 
 */

public with sharing class QuoteLineSelector implements IObjectSelector{
	//FIXME: It has to be designed and extended. Commented out to gain the code coverage for this sprint.
	//
	/*public static SObject getRecordById(Id rid){
		return [SELECT Id, Name, Place_Name__c, SBQQ__ProductName__c, 	SBQQ__Quantity__c, SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id = :rid];
	}
	
	public static List<SObject> getRecordsByIds(Set<Id> rids){
		return [SELECT Id, Name, Place_Name__c, SBQQ__ProductName__c, 	SBQQ__Quantity__c, SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id IN :rids];
	}
	
	public static Map<Id, SObject> getMapRecordsByIds(Set<Id> rids){
        return new Map<Id,SObject>([SELECT Id, Name, Place_Name__c, SBQQ__ProductName__c, 	SBQQ__Quantity__c, SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id IN :rids]);
	}*/

    public static Map<Id, SBQQ__QuoteLine__c> getMapRecordsByQuoteId(Id quoteId)
    {
        return new Map<Id, SBQQ__QuoteLine__c>([SELECT Id, Name, Place__c, Place_Name__c, SBQQ__Product__c, Place__r.PlaceId__c,
                                                SBQQ__ProductName__c, SBQQ__Quantity__c,SBQQ__BundledQuantity__c,
                                                SBQQ__RequiredBy__c,
                                                SBQQ__AdditionalDiscount__c,Total__c,Base_term_Renewal_term__c,Is_LBx__c,
                                                Subscription_Term__c,SBQQ__ListPrice__c,SBQQ__TotalDiscountRate__c,
                                                SBQQ__AdditionalDiscountAmount__c, SBQQ__Discount__c, CustomerDisc_update__c,NonPriceRelevant__c,
                                                ASAP_Activation__c,SBQQ__StartDate__c,Quote_Type__c,SBQQ__UpgradedSubscription__c,SBQQ__UpgradedSubscription__r.SBQQ__StartDate__c
                                                FROM SBQQ__QuoteLine__c
                                                WHERE SBQQ__Quote__c = :quoteId
                                                order by SBQQ__RequiredBy__c, SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c nulls last,SBQQ__ProductOption__r.SBQQ__Number__c]);
    }
    
    public static Map<Id, SBQQ__QuoteLine__c> getMapRecordsByQuoteIds(Set<Id> quoteIds)
    {
        return new Map<Id, SBQQ__QuoteLine__c>([SELECT Id, Name, Place__c, Place__r.PlaceID__c,Place_Name__c, SBQQ__Product__c, SBQQ__Product__r.ProductCode,
                                                SBQQ__Quote__r.SBQQ__Account__r.Phone, SBQQ__Quote__r.SBQQ__Account__r.Email__c,
                                                SBQQ__ProductName__c, SBQQ__Quantity__c,SBQQ__BundledQuantity__c,
                                                SBQQ__RequiredBy__c,
                                                SBQQ__AdditionalDiscount__c,Total__c,Base_term_Renewal_term__c,Subscription_Term__c,
                                                SBQQ__ListPrice__c,SBQQ__TotalDiscountRate__c
                                                FROM SBQQ__QuoteLine__c
                                                WHERE SBQQ__Quote__c IN:quoteIds]);
    }

    public static Map<SBQQ__QuoteLine__c, List<SBQQ__QuoteLine__c>> getHierarchyMapRecordsByQuoteLinesMap(Map<Id, SBQQ__QuoteLine__c> quoteLinesMap)
    {
        Map<Id, List<SBQQ__QuoteLine__c>> quoteLinesChildrenMap = new Map<Id, List<SBQQ__QuoteLine__c>>();
        Map<SBQQ__QuoteLine__c, List<SBQQ__QuoteLine__c>> quoteLinesHierarchyMap = new Map<SBQQ__QuoteLine__c, List<SBQQ__QuoteLine__c>>();

        for(Id quoteLineId : quoteLinesMap.keySet())
        {
            SBQQ__QuoteLine__c quoteLine = (SBQQ__QuoteLine__c)quoteLinesMap.get(quoteLineId);
            if(quoteLine.SBQQ__RequiredBy__c != null)
            {
                if(!quoteLinesChildrenMap.containsKey(quoteLine.SBQQ__RequiredBy__c))
                {
                    quoteLinesChildrenMap.put(quoteLine.SBQQ__RequiredBy__c,new List<SBQQ__QuoteLine__c>());
                }
                
                quoteLinesChildrenMap.get(quoteLine.SBQQ__RequiredBy__c).add(quoteLine);
            }
            else {
                quoteLinesHierarchyMap.put((SBQQ__QuoteLine__c)quoteLinesMap.get(quoteLineId), null);
            }

        }

        System.debug('[QuoteLineSelector.getHierarchyMapRecordsByQuoteLinesMap] quoteLinesMap.keySet() :' + quoteLinesMap.keySet());
        System.debug('[QuoteLineSelector.getHierarchyMapRecordsByQuoteLinesMap] quoteLinesChildrenMap.keySet() :' + quoteLinesChildrenMap.keySet());

        for(Id quoteLineParentId: quoteLinesChildrenMap.keySet())
        {
            quoteLinesHierarchyMap.put((SBQQ__QuoteLine__c)quoteLinesMap.get(quoteLineParentId),quoteLinesChildrenMap.get(quoteLineParentId));
            System.debug('[QuoteLineSelector.getHierarchyMapRecordsByQuoteLinesMap] [ParentId, [ChidrenListSize]: [' + quoteLineParentId + '[' + quoteLinesChildrenMap.get(quoteLineParentId).size() + ']]');
        }

        return quoteLinesHierarchyMap;
    }
}