@istest public class test_leadTriggerHandler {

    @testsetup static void testsetup(){
     list<lead> leads = test_datafactory.createleads('TestFirst','TestLast',1);
     insert leads;
    }

    @istest static void test_lead(){
    list<lead> l = [select id,FirstName,LeadSource from lead];
    test.startTest();
    LeadTriggerHandler lth = new leadtriggerHandler(true,1);
    lth.BeforeInsert(l);
    test.stopTest();
  }
    @istest static void beforeupdate(){
    list<lead> l = [select id,FirstName,LeadSource,City,Street,PostalCode,UID__c,Phone,Email from lead];
    map<id,lead> olditem = new map<id,lead>();
    map<id,lead> newitem = new map<id,lead>();
    olditem.put(l[0].id,l[0]);
    newitem.put(l[0].id,l[0]);
    test.startTest();
    LeadTriggerHandler lth = new leadtriggerHandler(true,1);
    lth.Beforeupdate(olditem,newitem);
    lth.BeforeDelete(olditem);
    lth.AfterInsert(newitem);
    lth.AfterUpdate(newItem, oldItem);
    lth.AfterDelete(oldItem);
    lth.AfterUndelete(oldItem);
    lth.IsDisabled();
    test.stopTest();
    
     
    

      }

}