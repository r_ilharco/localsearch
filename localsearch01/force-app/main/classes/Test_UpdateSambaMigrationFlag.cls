@isTest
public class Test_UpdateSambaMigrationFlag {
    @testSetup
    public static void setupData(){
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;

        Account account = Test_DataFactory.createAccounts('Test Name Account', 1)[0];
        insert account;

        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Test Name Contact', 1)[0];
        contact.Primary__c = true;
        insert contact;
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        insert productsToInsert;
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Name Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        SBQQ__QuoteLine__c ql = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        ql.SambaMigration__c = true;
        ql.Samba_Migration__c= false;
        insert ql;
    }
    
    
     @isTest
    static void test_updateOfFlag(){
        List<SBQQ__Quote__c> quote = [select id, Cluster_Id__c from SBQQ__Quote__c ];
        List<String> clusterids = new List<String>();
        for(SBQQ__Quote__c q : quote){
            clusterids.add(q.Cluster_Id__c);
        }
        UpdateSambaMigrationFlag_Schedulable a = new UpdateSambaMigrationFlag_Schedulable();
        a.execute(null);
        UpdateSambaMigrationFlag b = new UpdateSambaMigrationFlag(clusterids); 
        Database.executeBatch(b, 200); 
    }
    
    
       public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
}