/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.      
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date		   : 04-11-2019
 * Sprint      : 1
 * Work item   : 25
 * Package     : 
 * Description : Test class for SBQQQuoteTriggerHandler
 * Changelog   : 
 */


@isTest
public with sharing class Test_SBQQQuoteTriggerHandler {

    @TestSetup
    static void setup(){
        List<Account> accs = Test_DataFactory.createAccounts('test account', 1);
        insert accs;
        List<Opportunity> opps = Test_DataFactory.createOpportunities('test opp', ConstantsUtil.OPPORTUNITY_STAGE_QUALIFICATION, accs.get(0).Id, 1 );
        insert opps;
        List<SBQQ__Quote__c> quotes = Test_DataFactory.createQuotes(ConstantsUtil.Quote_Status_Accepted, opps.get(0), 1);
        insert quotes;
        
    }

    @isTest
    static void testBeforeInsert(){
        
        List<SBQQ__Quote__c> quotes = [select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c];

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.BeforeInsert(quotes);
        Test.stopTest();
    }

    @isTest
    static void testBeforeUpdate(){

        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.BeforeUpdate(quotes,quotes);
        Test.stopTest();

    }

    @isTest
    static void testBeforeDelete(){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.BeforeDelete(quotes);
        Test.stopTest();
    }

    @isTest
    static void testAfterInsert(){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.AfterInsert(quotes);
        Test.stopTest();
    }

    @isTest
    static void testAfterUpdate(){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.AfterUpdate(quotes,quotes);
        Test.stopTest();
    }

    @isTest
    static void testAfterDelete(){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.AfterDelete(quotes);
        Test.stopTest();
    }

    @isTest
    static void testAfterUndelete(){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>([select id, SBQQ__Account__c, Billing_Profile__c from SBQQ__Quote__c]);

        Test.startTest();
            SBQQQuoteTriggerHandler instance = new SBQQQuoteTriggerHandler();
            instance.AfterUndelete(quotes);
        Test.stopTest();
    }

    




}