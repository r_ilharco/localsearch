@isTest
public class QuoteManagerTest {
    
    @testSetup
    public static void test_setupData() {
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
                
        List<Product2> banners = new List<Product2>();
        banners.add(generteFatherProduct_localBANNER());
        banners.add(generteFatherProduct_searchBANNER());
        insert banners;
        List<PricebookEntry> pbesBanner = new List<PricebookEntry>();
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[0].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[1].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        insert pbesBanner;
        
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 1', account.Id));
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 2', account.Id));
        insert opportunities;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c';
        List<SBQQ__Quote__c> quotes = Database.query(queryQuotes);
        
        List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes) product2Pbe.put(pbe.Product2Id, pbe.Id);
        
        SBQQ__Quote__c quote1 = quotes[0];
        SBQQ__Quote__c quote2 = quotes[1];
        
        opportunities[0].SBQQ__PrimaryQuote__c = quote1.Id;
        opportunities[1].SBQQ__PrimaryQuote__c = quote2.Id;
        update opportunities;
        
        List<SBQQ__QuoteLine__c> fatherQuoteLines = new List<SBQQ__QuoteLine__c>();
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, banners[1].Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SBANNER001').Id)));
        insert fatherQuoteLines;
        
       	quote1.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        quote2.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        quote2.Replacement__c = TRUE;
        quote2.SBQQ__Status__c = ConstantsUtil.Quote_Status_Draft;
        update quotes;
        
        Order activationOrder = createActivationOrder(account.Id, quote1.Id);
        insert activationOrder;
        
        OrderItem fatherOrderItem = Test_DataFactory.generateFatherOrderItem(activationOrder.Id, fatherQuoteLines[0]);
        insert fatherOrderItem;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(activationOrder, false);
        insert contract;
		
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOrderItem);
        fatherSub.Subscription_Term__c = fatherQuoteLines[0].Subscription_Term__c;
        insert fatherSub;
        
        fatherOrderItem.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOrderItem;
        
        activationOrder.Contract__c = contract.Id;
        activationOrder.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        activationOrder.SBQQ__Contracted__c = true;
        update activationOrder;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contract.SBQQ__Order__c = activationOrder.Id;
        update contract;
        
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
        fatherSub.Replaced_by_Product__c = fatherQuoteLines[1].Id;
        update fatherSub;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_updateQuote_TrialRequest(){
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Status__c, SBQQ__Ordered__c FROM SBQQ__Quote__c LIMIT 1];
        quote.SBQQ__Status__c = Label.Quote_Status_for_Trial_Request;
        QuoteTriggerHandler.disableTrigger = true;
        update quote;
        QuoteTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        QuoteManager.updateQuote(new List<SBQQ__Quote__c>{quote});
        Test.stopTest();
    }
    
    @isTest
    public static void test_updateQuote_Draft(){
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Status__c, SBQQ__Ordered__c FROM SBQQ__Quote__c WHERE SBQQ__Status__c = :ConstantsUtil.Quote_Status_Draft LIMIT 1];
        
        Test.startTest();
        QuoteManager.updateQuote(new List<SBQQ__Quote__c>{quote});
        Test.stopTest();
    }
    
    @isTest
    public static void test_updateQuote_Rejected(){
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Status__c, SBQQ__Ordered__c FROM SBQQ__Quote__c LIMIT 1];
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Rejected;
        QuoteTriggerHandler.disableTrigger = true;
        update quote;
        QuoteTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        QuoteManager.updateQuote(new List<SBQQ__Quote__c>{quote});
        Test.stopTest();
    }
    
    @isTest
    public static void test_convertTrial(){
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__Status__c, SBQQ__Ordered__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c ql = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        QuoteLineTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        ql.TrialStatus__c = ConstantsUtil.Quote_Status_Draft;
        update ql;
        QuoteLineTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        QuoteManager.convertTrial(quote.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void test_CalculateQuoteDiff(){
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c WHERE SBQQ__Type__c = :ConstantsUtil.QUOTE_TYPE_REPLACEMENT LIMIT 1];
        
        Test.startTest();
        QuoteManager.CalculateQuoteDiff(quote.Id, false);
        Test.stopTest();
    }
    
    public static Product2 generteFatherProduct_localBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','LBANNER');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;
    }
    
    public static Product2 generteFatherProduct_searchBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','generteFatherProduct_searchBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','SBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','SBANNER');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','searchBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','SBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Order createActivationOrder(String accountId, String quoteId){
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c', ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type', ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate', String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId', accountId);
        fieldApiNameToValue.put('SBQQ__Quote__c', quoteId);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        return order;
    }
}