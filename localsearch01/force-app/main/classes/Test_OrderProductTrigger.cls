@isTest
public class Test_OrderProductTrigger{
    
	@testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Community User Creation,Quote Management,Order Management Insert Only,Order Management';
		insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
		QuoteLineTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        
        Product2 product = generteFatherProduct_localBANNER();
        insert product;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
		Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, product.Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;

        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
      	        
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insert(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, Total__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        fatherOI.Ad_Context_Allocation__c = 'TestContextAllocationId';
        fatherOI.Related_Product_Group__c = 'TestPG';
        Test.startTest();
		insert fatherOI;
        
        delete fatherOI;
        Test.stopTest();
    }
    
    @isTest
    public static void test_insertTerminationOrder(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c fatherQL = [SELECT Id, SBQQ__RequiredBy__c, SBQQ__Product__c,Total__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c FROM SBQQ__QuoteLine__c LIMIT 1];

        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;

        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.Total__c = 490.0;
        insert fatherSub;

        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;

        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        update fatherSub;
        
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        insert terminationOrder;
        
        OrderProductTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_insertTerminationOrder_Promotion(){
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c fatherQL = [SELECT Id,SBQQ__Discount__c,Promotion_Type__c,Promotion_Applied__c , SBQQ__RequiredBy__c, SBQQ__Product__c,Total__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c, Subscription_Term__c FROM SBQQ__QuoteLine__c LIMIT 1];
        fatherQL.SBQQ__Discount__c = 10;
        fatherQL.Promotion_Type__c =  ConstantsUtil.FIRST_TERM_DISCOUNT;
       	fatherQL.Promotion_Applied__c = true;
        update fatherQL;
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;

        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;
        
        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.Promotion_Type__c = ConstantsUtil.FIRST_TERM_DISCOUNT;
        fatherSub.Promotion_Applied__c = true;
        fatherSub.Total__c = 490.0;
        fatherSub.Termination_Date__c = system.today();
        insert fatherSub;

        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;

        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        update fatherSub;
        
        Order terminationOrder = generateOrder(contract, ConstantsUtil.ORDER_TYPE_TERMINATION);
        insert terminationOrder;
        
        OrderProductTriggerHandler.disableTrigger = false;
        
        Test.startTest();
        OrderItem fatherOrderItem = generateFatherOrderItem(fatherSub, terminationOrder, fatherQL.SBQQ__PricebookEntryId__c);
        insert fatherOrderItem;
        Test.stopTest();
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_update(){
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        SBQQ__Quote__c quote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id,Total__c, SBQQ__RequiredBy__c, SBQQ__Product__c, SBQQ__PricebookEntryId__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__StartDate__c FROM SBQQ__QuoteLine__c];
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        SBQQ__QuoteLine__c fatherQL;
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c ql : quoteLines){
            if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                fatherQL = ql;
            }
            else childrenQLs.add(ql);
        }
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        fatherOI.Ad_Context_Allocation__c = 'TestContextAllocationId';
        fatherOI.Related_Product_Group__c = 'TestPG';
        
        insert fatherOI;
        Test.startTest();
        fatherOI.ServiceDate = Date.today().addDays(2);
		update fatherOI;
        LEnhancedProdConfigController.futureUpdateAllocationIDS(new Set<Id> {fatherOI.id});
        //UpdateAllocationBatch b = new UpdateAllocationBatch(); 
        //b.execute(null);
        Test.stopTest();
    }
    
    public static Product2 generteFatherProduct_localBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Order generateOrder(Contract c, String orderType){
        Order order = new Order();
        order.AccountId = c.AccountId;
        order.SBQQ__Quote__c = c.SBQQ__Quote__c;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        order.Type = orderType;
        order.Custom_Type__c = orderType;
        if(ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(orderType)){
            order.EffectiveDate = c.Cancel_Date__c;
            if(c.Cancel_Date__c == null){
                c.Cancel_Date__c = Date.today();
                order.EffectiveDate = Date.today();
            }
        }
        else{
            
            if(c.TerminateDate__c == null){
                c.TerminateDate__c = Date.today();
                order.EffectiveDate = Date.today();
            }
            else{
                order.EffectiveDate = c.TerminateDate__c;
            }
            order.ContractId = c.id;
        }
        order.Contract__c = c.id;
        return order;
    }
    public static OrderItem generateFatherOrderItem(SBQQ__Subscription__c subscription, Order order, String pbeId){
        OrderItem fatherOrderItem = new OrderItem();
        fatherOrderItem.ServiceDate = Date.today();
        fatherOrderItem.Quantity = subscription.SBQQ__Quantity__c;
        fatherOrderItem.Product2Id = subscription.SBQQ__Product__c;
        fatherOrderItem.UnitPrice = subscription.SBQQ__ListPrice__c;   
        fatherOrderItem.PricebookEntryId = pbeId;
        fatherOrderItem.SBQQ__Contract__c = subscription.SBQQ__Contract__c;
        fatherOrderItem.SBQQ__QuoteLine__c =subscription.SBQQ__QuoteLine__c; 
        fatherOrderItem.SBQQ__Subscription__c = subscription.Id;
        fatherOrderItem.OrderId = order.Id;
        fatherOrderItem.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_DRAFT;
        return fatherOrderItem; 
    }
}