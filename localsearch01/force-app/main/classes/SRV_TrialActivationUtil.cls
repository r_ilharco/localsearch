public without sharing class SRV_TrialActivationUtil {

    public static List<SBQQ__QuoteLine__c> setTrialOnQuoteLines(List<SBQQ__QuoteLine__c> qls){
        
        List<SBQQ__QuoteLine__c> listQLinesToUpdate = new List<SBQQ__QuoteLine__c>();

        for(SBQQ__QuoteLine__c ql: qls){
            if(ql.TrialStartDate__c == null) {
                ql.TrialStartDate__c = Date.today();
                ql.TrialEndDate__c = Date.today()+Integer.valueOf(Label.Trial_end_prior_days);
            }  
            ql.TrialStatus__c = ConstantsUtil.QUOTELINE_STATUS_TRIAL_REQUESTED;
            listQLinesToUpdate.add(ql);
        }
        return listQLinesToUpdate; 
    }

    public static QuoteTrialActivationPayload getTrialJSON(SBQQ__Quote__c quote, Set<SBQQ__QuoteLine__c> quoteLines, Map<String, List<Product_Activation_JSON__mdt>> productToJsonConfigMdt){

        QuoteTrialActivationPayload quoteTrialActivationPayload = new QuoteTrialActivationPayload();
        quoteTrialActivationPayload.customer_internal_id = quote.SBQQ__Account__c;
        quoteTrialActivationPayload.customer_id = quote.SBQQ__Account__r.Customer_Number__c;
        quoteTrialActivationPayload.internal_id = quote.Id;
        quoteTrialActivationPayload.id = quote.External_Id__c;
        quoteTrialActivationPayload.type = ConstantsUtil.QUOTE_TYPE_QUOTE;
        quoteTrialActivationPayload.primary_quote = quote.SBQQ__Primary__c;

        for(SBQQ__QuoteLine__c currentQL : quoteLines){

            if(currentQL.SBQQ__RequiredBy__c == null){
                //IT IS THE ELEMENT NODE
                Map<String, Object> masterOptionMap = new  Map<String, Object>();
                
                masterOptionMap.put('element_id', currentQL.External_Id__c); //TODO CREARE CAMPO AUTONUMBER EXTERNAL ID E CHECK CON FABIUS
                masterOptionMap.put('start_date', formatDate(currentQL.TrialStartDate__c));
                masterOptionMap.put('end_date', formatDate(currentQL.TrialEndDate__c)); //
                masterOptionMap.put('element_internal_id', currentQL.Id);
                masterOptionMap.put('product_code', currentQL.SBQQ__Product__r.ProductCode);
                if(currentQL.Place__c != null){
                    if(currentQL.Place__r.PlaceID__c != null){
                        masterOptionMap.put('place_id', currentQL.Place__r.PlaceID__c);
                    }
                }
                else if(currentQL.SBQQ__RequiredBy__c != null){
                    if(currentQL.SBQQ__RequiredBy__r.Place__c != null){
                        if(currentQL.SBQQ__RequiredBy__r.Place__r.PlaceID__c != null){
                            masterOptionMap.put('place_id', currentQL.SBQQ__RequiredBy__r.Place__r.PlaceID__c);
                        }
                    }
                }

                String productCode = currentQL.SBQQ__Product__r.ProductCode;
                
                if(!productToJsonConfigMdt.isEmpty()){

                    List<Product_Activation_JSON__mdt> productAdditionalAttributes = productToJsonConfigMdt.get(productCode);
                    
                    if(productAdditionalAttributes != null && !productAdditionalAttributes.isEmpty()){
                        for(Product_Activation_JSON__mdt currentMdt : productAdditionalAttributes){
                            FieldTypeValue ftv = getFieldValueForKey(currentMdt, currentQL);
                            if(ftv != null){
                                if(ftv.objValue != null){
                                    if(ftv.type == Schema.DisplayType.STRING){
                                        masterOptionMap.put(currentMdt.Key__c, String.valueOf(ftv.objValue));
                                    } 
                                    else if(ftv.type == Schema.DisplayType.BOOLEAN){
                                        masterOptionMap.put(currentMdt.Key__c, Boolean.valueOf(ftv.objValue));
                                    }
                                    else if(ftv.type == Schema.DisplayType.INTEGER){
                                        masterOptionMap.put(currentMdt.Key__c, Integer.valueOf(ftv.objValue));
                                    }
                                    else if(ftv.type == Schema.DisplayType.DOUBLE){
                                        masterOptionMap.put(currentMdt.Key__c, Double.valueOf(ftv.objValue));
                                    }
                                    else if(ftv.type == Schema.DisplayType.PICKLIST){
                                        masterOptionMap.put(currentMdt.Key__c, String.valueOf(ftv.objValue));
                                    }
                                }
                            }
                        }
                    }
                }
                List<Map<String, Object>> innerOptionMapList = new List<Map<String, Object>>();
                for(SBQQ__QuoteLine__c innerQuoteLine : quoteLines){
                    if(innerQuoteLine.SBQQ__RequiredBy__c != null && innerQuoteLine.SBQQ__RequiredBy__c.equals(currentQL.Id)){
                        Map<String, Object> innerOptionMap = new  Map<String, Object>();
                        innerOptionMap.put('element_id', innerQuoteLine.External_Id__c);
                        innerOptionMap.put('element_internal_id', innerQuoteLine.Id);
                        innerOptionMap.put('option_code', innerQuoteLine.SBQQ__Product__r.ProductCode);
                        innerOptionMap.put('quantity', Integer.valueOf(innerQuoteLine.SBQQ__Quantity__c));

                        /*String innerProductCode = innerQuoteLine.SBQQ__Product__r.ProductCode;
                        if(!productToJsonConfigMdt.isEmpty()){
                            List<Product_Activation_JSON__mdt> productAdditionalAttributes = productToJsonConfigMdt.get(innerProductCode);
                            if(productAdditionalAttributes != null && !productAdditionalAttributes.isEmpty()){
                                for(Product_Activation_JSON__mdt currentMdt : productAdditionalAttributes){
                                    FieldTypeValue ftv = getFieldValueForKey(currentMdt, innerQuoteLine);
                                    if(ftv != null){
                                        if(ftv.objValue != null){
                                            if(ftv.type == Schema.DisplayType.STRING){
                                                innerOptionMap.put(currentMdt.Key__c, String.valueOf(ftv.objValue));
                                            } 
                                            else if(ftv.type == Schema.DisplayType.BOOLEAN){
                                                innerOptionMap.put(currentMdt.Key__c, Boolean.valueOf(ftv.objValue));
                                            }
                                            else if(ftv.type == Schema.DisplayType.INTEGER){
                                                innerOptionMap.put(currentMdt.Key__c, Integer.valueOf(ftv.objValue));
                                            }
                                            else if(ftv.type == Schema.DisplayType.DOUBLE){
                                                innerOptionMap.put(currentMdt.Key__c, Double.valueOf(ftv.objValue));
                                            }
                                            else if(ftv.type == Schema.DisplayType.PICKLIST){
                                                innerOptionMap.put(currentMdt.Key__c, String.valueOf(ftv.objValue));
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                        innerOptionMapList.add(innerOptionMap);
                    }
                }
                if(!innerOptionMapList.isEmpty()){
                   masterOptionMap.put('options', innerOptionMapList); 
                }
                quoteTrialActivationPayload.elements.add(masterOptionMap);
            }
        }
        return quoteTrialActivationPayload;
    }

    public class QuoteTrialActivationPayload {

        public QuoteTrialActivationPayload(){
            elements = new List<Map<String,Object>>();
        }
        
        public String customer_internal_id {get;set;}
        public String customer_id {get; set;}
        public String internal_id {get;set;}
        public String quote_internal_id {get;set;}
        public String id {get; set;}
        public String quote_id {get; set;}
        public String type {get; set;}
        public List<Map<String,Object>> elements {get;set;}
        public Boolean primary_quote {get;set;}
        
        public string getJson(){
            return JSON.serialize(this, true);
        }
    }

    public static String formatDate(Datetime d) {
        return d.year() + '-' + String.valueof(d.month()+100).right(2) + '-' + String.valueof(d.day()+100).right(2);
    }

    public static FieldTypeValue getFieldValueForKey(Product_Activation_JSON__mdt metadataConfiguration, SBQQ__QuoteLine__c quoteLine){
        
        FieldTypeValue ftv = new FieldTypeValue();
        Object obj;
        
        if(metadataConfiguration.Object_API_Name__c != null && metadataConfiguration.Field_API_Name__c != null){
            String quoteLineObject = 'SBQQ__QuoteLine__c';
            String orderItemObject = 'OrderItem';
            String product2Object = 'Product2';
            Schema.SobjectField field = Schema.getGlobalDescribe().get(metadataConfiguration.Object_API_Name__c).getDescribe().fields.getMap().get(metadataConfiguration.Field_API_Name__c);
            Schema.DisplayType fieldType = field.getDescribe().getType();
            ftv.type = fieldType;
            if(quoteLineObject.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c) || orderItemObject.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c)){
                obj = quoteLine.get(field);
            }
            else if(product2Object.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c)){
                String objApiName = 'SBQQ__Product__r';
                obj = quoteLine.getSObject(objApiName).get(metadataConfiguration.Field_API_Name__c);
            }
        }
        ftv.objValue = obj;
        return ftv;
    }

    public class FieldTypeValue{
        public Object objValue {get;set;}
        public Schema.DisplayType type {get;set;}
    }

    public static void addQueryFields(Product_Activation_JSON__mdt metadataConfiguration, Set<String> queryFields){
        String quoteLineObject = 'SBQQ__QuoteLine__c';
        String orderItemObject = 'OrderItem';
        String product2Object = 'Product2';
        if(metadataConfiguration.Object_API_Name__c != null && metadataConfiguration.Field_API_Name__c != null){
            if(quoteLineObject.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c) || orderItemObject.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c)){
                queryFields.add(metadataConfiguration.Field_API_Name__c);
            }
            else if(product2Object.equalsIgnoreCase(metadataConfiguration.Object_API_Name__c)){
                String objApiName = metadataConfiguration.Object_API_Name__c.replace('Product2','SBQQ__Product__r');
                String fieldRelationshipName = objApiName+'.'+metadataConfiguration.Field_API_Name__c;
                queryFields.add(fieldRelationshipName);
            }
        }       
    }

    public static void generateListMdtValueMap(Map<String, List<Product_Activation_JSON__mdt>> myMap, String myKey, Product_Activation_JSON__mdt myValue){
		if(!myMap.containsKey(myKey)) myMap.put(myKey, new List<Product_Activation_JSON__mdt>{myValue});
        else myMap.get(myKey).add(myValue);
	}

    @future(callout=true)
    public static void sendTrialInformation(String body, String quoteId, String correlationId){

        Mashery_Setting__c c2;
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else c2 = Mashery_Setting__c.getOrgDefaults();
        Decimal cachedMillisecs;

       	if(c2.LastModifiedDate !=null){
            cachedMillisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        
		SRV_OrderUtil.TokenInfo tokenInfoObject = SRV_OrderUtil.getValidToken(c2, cachedMillisecs);

        HttpRequest httpRequest = SRV_OrderUtil.buildHttpRequest(c2, tokenInfoObject, body, correlationId);

        if(httpRequest != null){

            Http http = new Http();

            try{
                HttpResponse response = http.send(httpRequest);
                LogUtility.saveTrialActivationRequest(httpRequest,response,body, correlationId, quoteId,null); //String body, String quoteId, String correlationId
            } 
            catch(exception e) {
                LogUtility.saveTrialActivationRequest(httpRequest,null,body, correlationId, quoteId,e);
            }            
        }
        if(tokenInfoObject.isNewToken == true){
            c2.Token__c = tokenInfoObject.token.access_token;
            c2.Expires__c = tokenInfoObject.token.expires_in;           
            upsert c2;
        }
    }
}