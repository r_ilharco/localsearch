public class ErrorEnqueueJob implements Queueable {
    private Error_Log__c errorLog;
    public ErrorEnqueueJob(Error_Log__c errorLog) {
        this.errorLog = errorLog;
    }
    public void execute(QueueableContext context) {
        insert errorLog;
    }
}