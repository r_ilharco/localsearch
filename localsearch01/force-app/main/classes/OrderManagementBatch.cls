/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date	       : 2019-08-06
 * Sprint      : Sprint 7
 * Work item   : SF2-300, Sprint7, Order Management.
 * Testclass   : Test_OrderManagementBatch
 * Package     : 
 * Description : 
 * Changelog   :
 */

global without sharing class OrderManagementBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable{
  
    global List<String> staticValues = new List<String>();

    global OrderManagementBatch(List<String> val){
        this.staticValues = val;
    }
    global OrderManagementBatch(){
    }
    global void execute(SchedulableContext sc) {
        Integer sizeBatch = 10;
        Integration_Config__mdt config;
        config= [SELECT Id, Active__c,Method__c  FROM Integration_Config__mdt WHERE MasterLabel = 'TriggerMigration'][0];
        if(config != null){
			sizeBatch = Integer.valueOf(config.Method__c);
        }        
    	OrderManagementBatch orderManagementB = new OrderManagementBatch(this.staticValues); 
    	Database.executeBatch(orderManagementB, sizeBatch);
   	}

    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = ''; 
        	query = 'SELECT Id, ' +
                                        'Name, ' +
                                        'Type, ' +
                                        'OrderNumber, ' + 
                                        'External_Id__c, ' + 
                                        'Status, ' +
                                        'OwnerId, ' +
                                        'EffectiveDate, ' +
                                        'EndDate, ' +
                                        'AccountId, ' +
                                        'SBQQ__Quote__c, ' +
                                        'Custom_Type__c ' +
                                        'FROM Order ' + 
                                        'WHERE ' + 
                                        'EffectiveDate <= TODAY AND ContractSamba_Key__c != null ' +
                            			'and SambaIsStartConfirmed__c = false AND Type = \'New\' and Custom_Type__c=\'Activation\'' +
                                        'and Status = \'' + ConstantsUtil.ORDER_STATUS_DRAFT + '\' ';
        if(!staticValues.isEmpty())  {
            query +=' and cluster_Id__c in: staticValues';
        }       
     	System.debug('***QUERY : '+query);
          return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext info, List<Order> scope){
        List<Id> ordersId = new List<Id>();
        for(Order currentOrder : scope) {
            ordersId.add(currentOrder.Id);
        }
        OrderUtility.activateContracts(ordersId);
    }
    
    global void finish(Database.BatchableContext context) {}
}