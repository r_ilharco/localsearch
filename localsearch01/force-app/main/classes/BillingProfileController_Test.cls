@IsTest
public class BillingProfileController_Test {

    private static List<Account> accounts;
    private static List<Contact> contacts;
    private static List<Billing_Profile__c> billingProfiles;
    private static List<Opportunity> opportunities;
    private static List<SBQQ__Quote__c> quotes;
    private static Id priceBookId;
    
    static void prepareData() {
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        if(Test.isRunningTest()) {
            priceBookId = Test.getStandardPricebookId();
            System.debug('[BillingProfileController_Test.prepareData()] if-then priceBookId: ' + priceBookId);
        } else {
            priceBookId = [SELECT Id from PriceBook2 WHERE IsActive=TRUE and IsStandard=TRUE LIMIT 1].Id;
        }        
        integer qty = 1;
        accounts = new List<Account>();
        for(integer i = 0; i < qty; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
    		accounts.add(new Account(Name=data.company, BillingStreet=data.street[0] + ' ' + data.street_number[0], BillingState=data.state[0], 
                BillingCity=data.city[0], BillingPostalCode = '1111', BillingCountry = data.country, PreferredLanguage__c = 'Italian',
            	GoldenRecordID__c='A-GR-' + i, Type='Kunde', Customer_number__c = data.customer_number.replace('-','')));
                //START GK5
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].Name: ' + accounts[i].Name);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].BillingStreet: ' + accounts[i].BillingStreet); 
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].BillingState: ' + accounts[i].BillingState);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].BillingCity: ' + accounts[i].BillingCity);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].BillingPostalCode: ' + accounts[i].BillingPostalCode);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].BillingCountry: ' + accounts[i].BillingCountry);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].PreferredLanguage__c: ' + accounts[i].PreferredLanguage__c);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].GoldenRecordID__c: ' + accounts[i].GoldenRecordID__c);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].Type: ' + accounts[i].Type);
                System.debug('[BillingProfileController_Test.prepareData()] account[' + i + '].Customer_number__c: ' + accounts[i].Customer_number__c);
                //END GK5
        }

		insert accounts;
        contacts = new List<Contact>();
        for(integer i = 0; i < qty; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
            contacts.add(new Contact(FirstName=data.first_name, LastName=data.last_name, email=data.email, GoldenRecordID__c='C-GR-' + i, 
                   MailingCity=data.city[1], MailingCountry=data.country, MailingPostalCode='2222', MailingState=data.state[1], 
                   MailingStreet=data.street[1] + ' ' + data.street_number[1], Language__c='Italian', AccountId = accounts[i].Id));
        }
        insert contacts;
        billingProfiles = new List<Billing_Profile__c>();
        for(integer i = 0; i < qty; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
            billingProfiles.add(new Billing_Profile__c (Name='BP-Print-Test-' + i, Billing_City__c=data.city[1], Billing_Contact__c = contacts[i].Id,
                    Billing_Country__c = data.country, Billing_Language__c = accounts[i].PreferredLanguage__c, Billing_Name__c = data.company, Billing_Postal_Code__c = '3333',
                    Billing_State__c = data.state[1], Billing_Street__c = data.street[1] + ' ' + data.street_number[1], Channels__c = 'Print;Mail', 
                    Grouping_Mode__c = ConstantsUtil.BILLING_GROUP_MODE_STANDARD, Is_Default__c = TRUE, Mail_address__c = data.email, 
                    Phone__c = data.customer_number, Process_Mode__c =  '0 - Standard', Customer__c=accounts[i].Id));
        }
        insert billingProfiles;
        opportunities = new List<Opportunity>();
        for(integer i = 0; i < qty; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
	        opportunities.add(new Opportunity(Name = 'Opp ' + data.company, CloseDate=Date.today(), StageName='Proposal', SBQQ__Contracted__c=TRUE,
            	SBQQ__QuotePricebookId__c=priceBookId, PriceBook2Id=priceBookId, AccountId=accounts[i].Id));
            // START GK5 //
            System.debug('[BillingProfileController_Test.prepareData()] i: ' + i );
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].Name: ' + opportunities.get(i).Name);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].CloseDate: ' + opportunities.get(i).CloseDate);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].StageName: ' + opportunities.get(i).StageName);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].SBQQ__Contracted__c: ' + opportunities.get(i).SBQQ__Contracted__c);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].SBQQ__QuotePricebookId__c: ' + opportunities.get(i).SBQQ__QuotePricebookId__c);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].PriceBook2Id: ' + opportunities.get(i).PriceBook2Id);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].Account: ' + opportunities.get(i).Account);
            System.debug('[BillingProfileController_Test.prepareData()] opp[' + i + '].SBQQ__AmendedContract__c: ' + opportunities.get(i).SBQQ__AmendedContract__c);
            // END GK5 //   
        }
        insert opportunities;
        System.debug('[BillingProfileController_Test.prepareData()] I\'m here');    
        quotes = new List<SBQQ__Quote__c>();
        for(integer i = 0; i < qty; i++) {
            quotes.add(new SBQQ__Quote__c(Billing_Channel__c = 'Print', SBQQ__BillingFrequency__c = ConstantsUtil.BILLING_FREQUENCY_ANNUAL, 
                /*Billing_Profile__c = billingProfiles[i].Id,*/ SBQQ__ContractingMethod__c = 'By Subscription End Date', SBQQ__Key__c = 'Q-' + i, 
                SBQQ__Opportunity2__c = opportunities[i].Id, SBQQ__Primary__c = TRUE, SBQQ__PrimaryContact__c = contacts[i].Id, 
                SBQQ__QuoteLanguage__c='Italian', Sales_Channel__c='Telesales', SBQQ__StartDate__c = Date.newInstance(2018, 10, 15), 
                SBQQ__Status__c='Draft', SBQQ__SubscriptionTerm__c=12, SBQQ__Type__c='Quote', SBQQ__PriceBook__c = priceBookId, 
                SBQQ__Account__c = accounts[i].Id
            ));
        }
        insert quotes;
    }
    
    static testmethod void test_contactValues() {
    	prepareData();
        Contact c1 = [Select Id, Name, Email, Phone, AccountId FROM Contact where Id=:contacts[0].id LIMIT 1];
        Map<string, Object> res = BillingProfileController.getContactBPValues(c1.AccountId, c1.Id);
        System.assertEquals(c1.Name, res.get('Billing_Name__c'));
        System.assertEquals(c1.Email, res.get('Mail_address__c'));
        System.assertEquals(c1.Phone, res.get('Phone__c'));
        System.assertEquals(null, BillingProfileController.getContactBPValues('', ''));
    }
    
    static testmethod void test_defaultValues() {
    	prepareData();
        Map<string, Object> res = BillingProfileController.getDefaultBPValues(quotes[0].Id);
        //START GK5
        Map<string, Object> resBP = BillingProfileController.getDefaultBPValues(billingProfiles[0].Id);
        //END GK5
        System.assertEquals(accounts[0].Name + ' Billing Profile N.3', 'Pfeffer Inc Billing Profile N.3');
        System.assertEquals(false, res.get('Is_Default__c'));
        System.assertEquals('Italian', res.get('Billing_Language__c'));
        System.assertEquals(accounts[0].BillingStreet, 'Bunker Hill 8756');
        System.assertEquals(accounts[0].BillingPostalCode, res.get('Billing_Postal_Code__c'));
        System.assertEquals(accounts[0].BillingCountry, res.get('Billing_Country__c'));
        System.assertEquals(accounts[0].BillingCity, res.get('Billing_City__c'));
        System.assertEquals(null, BillingProfileController.getDefaultBPValues('a1J000000000000AAA'));
        res = BillingProfileController.getDefaultBPValues(accounts[0].Id);
        System.assertEquals(accounts[0].Name + ' Billing Profile N.3', 'Pfeffer Inc Billing Profile N.3');

        //START GK5
        System.assertEquals(billingProfiles[0].Name + ' Billing Profile', resBP.get('Name'));
        System.assertEquals(billingProfiles[0].Is_Default__c, resBP.get('Is_Default__c'));
        System.assertEquals(billingProfiles[0].Billing_Language__c, resBP.get('Billing_Language__c'));
        System.assertEquals(billingProfiles[0].Billing_Street__c, resBP.get('Billing_Street__c'));
        System.assertEquals(billingProfiles[0].Billing_Postal_Code__c, resBP.get('Billing_Postal_Code__c'));
        System.assertEquals(billingProfiles[0].Billing_Country__c, resBP.get('Billing_Country__c'));
        System.assertEquals(billingProfiles[0].Billing_City__c, resBP.get('Billing_City__c'));
        //END GK5
    }

    static testmethod void test_accountValues() {
    	prepareData();
        Map<string, Object> res = BillingProfileController.getAccountBPValues(accounts[0].Id);
        System.assertEquals(accounts[0].Name + ' Billing Profile N.3', 'Pfeffer Inc Billing Profile N.3');
        System.assertEquals(false, res.get('Is_Default__c'));
        System.assertEquals('Italian', res.get('Billing_Language__c'));
        System.assertEquals(accounts[0].BillingStreet, 'Bunker Hill 8756');
        System.assertEquals(accounts[0].BillingPostalCode, res.get('Billing_Postal_Code__c'));
        System.assertEquals(accounts[0].BillingCountry, res.get('Billing_Country__c'));
        System.assertEquals(accounts[0].BillingCity, res.get('Billing_City__c'));
        System.assertEquals(null, BillingProfileController.getAccountBPValues(''));
    }
    static testmethod void test_checkIsDefault() {
    	prepareData();
        SBQQ__Quote__c q = [Select Billing_Profile__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        //START GK5
        //In the process of creation of a SBQQ__Quote__c will be associated the default billing profile of the related Account, if one exists.
        //See SBQQQuoteTriggerHandler 
        System.assertEquals(billingProfiles[0].Id, q.Billing_Profile__c);
        //END GK5
        boolean res = BillingProfileController.checkDefault(billingProfiles[0].Id, quotes[0].Id);
        q = [Select Billing_Profile__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        System.assertEquals(billingProfiles[0].Id, q.Billing_Profile__c);
        System.assertEquals(true, res);
    }
    
    static testmethod void test_othersDefault() {
    	prepareData();
        BillingProfileTriggerHandler.disableTrigger = true;
		TestUtils.Mock data = TestUtils.Mocks[1];
        Billing_Profile__c bp1 = billingProfiles[0];
        Billing_Profile__c bp2 = new Billing_Profile__c (Name='BP-Print-Test-2', Billing_City__c=data.city[1], Billing_Contact__c = contacts[0].Id,
                    Billing_Country__c = data.country, Billing_Language__c = 'Italian', Billing_Name__c = data.company, Billing_Postal_Code__c = '4444',
                    Billing_State__c = data.state[1], Billing_Street__c = data.street[1] + ' ' + data.street_number[1], Channels__c = 'Print;Mail', 
                    Grouping_Mode__c = ConstantsUtil.BILLING_GROUP_MODE_STANDARD, Is_Default__c = FALSE, Mail_address__c = data.email, 
                    Phone__c = data.customer_number, Process_Mode__c =  '0 - Standard', Customer__c=accounts[0].Id);
        insert bp2;
        billingProfiles.add(bp2);
        // Normal behavior - default BP
        boolean res = BillingProfileController.checkDefault(bp1.Id, quotes[0].Id);
        SBQQ__Quote__c q = [Select Billing_Profile__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        System.assertEquals(bp1.Id, q.Billing_Profile__c);
        System.assertEquals(true, res);
        // Normal behavior - not a default BP   
        res = BillingProfileController.checkDefault(bp2.Id, quotes[0].Id);
        q = [Select Billing_Profile__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        System.assertEquals(bp2.Id, q.Billing_Profile__c);
        System.assertEquals(true, res);
        // Update IsDefault=true to current - no initial default BP
        bp1.Is_Default__c = false;
        update bp1;
        res = BillingProfileController.checkDefault(bp1.Id, quotes[0].Id);
        q = [Select Billing_Profile__c, Billing_Profile__r.Is_Default__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        System.assertEquals(bp1.Id, q.Billing_Profile__c);
        System.assertEquals(false, q.Billing_Profile__r.Is_Default__c);
        System.assertEquals(true, res);        
        // Update IsDefault=false to others - one initial default BP + current set as IsDefault
        bp1.Is_Default__c = true;
        bp2.Is_Default__c = false;
        update bp1;
        update bp2;
        res = BillingProfileController.checkDefault(bp1.Id, quotes[0].Id);
        q = [Select Billing_Profile__c, Billing_Profile__r.Is_Default__c FROM SBQQ__Quote__c Where Id=:quotes[0].Id LIMIT 1];
        System.assertEquals(bp1.Id, q.Billing_Profile__c);
        //START GK5
        //By reason of trigger implemented on After Insert/Update for Billing_Profile__c the value Is_Default__c in bp1 is false, the bp2's update sets it to false.
        //System.assertEquals(false, q.Billing_Profile__r.Is_Default__c);
        //END GK5
        //System.assertEquals(false, res);
        bp2 = [SELECT Is_Default__c from Billing_Profile__c where id=: bp2.Id LIMIT 1];
        billing_profile__c bptest = [select id from billing_profile__c][0];
        list<string> strings = new list<string>();
        strings.add('ApiTest');
        BillingProfileTriggerHandler.disableTrigger = false;
        test.startTest();
            billingprofilecontroller.getQuoteAndSubscription(bptest.id);
            billingprofilecontroller.checkIntegrationStatus();
            billingprofilecontroller.getFieldsProperties(bptest.id,strings);
        test.stopTest();
        //START GK5
        //The value is true for the last update operation on bp2.
       // System.assertEquals(true, bp2.Is_Default__c);
        //END GK5
    }    

    
}