@IsTest
public class Test_SetQuoteDocumentsExpiredBatch {
	@TestSetup 
    static void setup(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Opportunity Management,Quote Management';
		insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Map<String, Id> usersByUsernameMap = new Map<String,Id>();
        Test_DataFactory.namirial_insertFutureUsers();
        Map<Id, User> users = new Map<Id,User>([SELECT Id, Username FROM User]);
        if(users != NULL && users.size() > 0)
        {
            for(User u : users.values())
            {
            	usersByUsernameMap.put(u.Username, u.Id);
            }
        }
        
        Pricebook2 pbDMC = new Pricebook2();
        pbDMC.ExternalId__c = 'PB3';
        pbDMC.Name = 'DMC';
        pbDMC.IsActive = true;
        insert pbDMC;
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        
        List<Account> accounts = Test_DataFactory.createAccounts('AccountTest01',1);
 		insert accounts;
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0],1);
        contacts[0].FirstName = 'FirstName';
        insert contacts;
        
        List<Opportunity> opps = Test_DataFactory.createOpportunities('OppName01','Draft',accounts[0].Id,1);
        opps[0].Name = 'OppName01';
        opps[0].Pricebook2Id = pbDMC.Id;
        insert opps;
        List<SBQQ__Quote__c> quotes = Test_DataFactory.createQuotes('New', opps[0], 1);
        quotes[0].SBQQ__SalesRep__c = usersByUsernameMap.get('dmctester@test.com');
        quotes[0].Filtered_Primary_Contact__c = contacts[0].Id;
        quotes[0].SBQQ__PrimaryContact__c = contacts[0].Id;
        quotes[0].SBQQ__PriceBook__c = pbDMC.Id;
        insert quotes;
        List<Document> documents = Test_DataFactory.createDocuments('Q-45445-20191126-0945.pdf',
                                                                    'provaprovaprova',
                                                                    'application/pdf',
                                                                    'CABJBBCGAd4f957419a4f4936898439621d9cf51e',
                                                                   	true,
                                                                   	UserInfo.getUserId(),
                                                                    1);
        insert documents;
        
        SBQQ__QuoteTemplate__c template = new Sbqq__quotetemplate__c();
        template.Name = 'DMC Template 2020 v1.0 - IT';
        template.SBQQ__TermsConditions__c = 'Die Preise werden nach Aufschaltung bzw. Publikation des jeweiligen Produkts monatlich bzw. jährlich in Rechnung gestellt. Einmalige Gebühren werden nach Vertragsabschluss in Rechnung gestellt.Es gelten die jeweils aktuellen Produktbeschreibungen auf www.localsearch.ch';
        insert template;
        
        SBQQ__QuoteDocument__c quoteDocument =  Test_DataFactory.generateQuoteDocument(quotes[0].Id);
        quoteDocument.SBQQ__DocumentId__c = documents[0].Id;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        quoteDocument.SBQQ__QuoteTemplate__c = template.Id;
        quoteDocument.SBQQ__Quote__c = quotes[0].Id;
        insert quoteDocument;
        quoteDocument.ExpirationDate__c = Date.today().addDays(-1);
        update quoteDocument;
    }
    
    @IsTest
    public static void test_SetQuoteDocumentsExpiredBatch_defaultConstructor(){
        List<SBQQ__QuoteDocument__c> qds = [SELECT Id, ExpirationDate__c FROM SBQQ__QuoteDocument__c];
        
        System.debug('qds -> ' + JSON.serialize(qds));
        
        Test.startTest();
        	Database.executeBatch(new SetQuoteDocumentsExpiredBatch(),2000);
        Test.stopTest();
    }
    
    @IsTest
    public static void test_SetQuoteDocumentsExpiredBatch_executeSchedulable(){
        Test.startTest();
        	SetQuoteDocumentsExpiredBatch setQuoteDocumentsExpiredBatch = new SetQuoteDocumentsExpiredBatch();
            String sch = '20 30 8 10 2 ?';
            String jobID = System.schedule('SetQuoteDocumentsExpiredBatch', sch, setQuoteDocumentsExpiredBatch);
        Test.stopTest();
    }
    
    
}