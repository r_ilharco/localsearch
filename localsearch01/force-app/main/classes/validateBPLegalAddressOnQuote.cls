/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 07-22-2019
 * Sprint      : 6
 * Work item   : 
                SF2-253 - Validate Account and Billing Address on Quote Creation
                SF2-264 - Legal Address Validation
                SF2-772 - Address Validation Review
 * Testclass   :
 * Package     : 
 * Description : Invocable method called by 'Quote_Management' PB for the BillingProfile Legal Address Validation
 * Changelog   : 
 */

public class validateBPLegalAddressOnQuote {
	//START - SF2-253, Wave C, Sprint 6 - Validate Account and Billing Address on Quote Creation - gcasola - 07-27-2019
    @InvocableMethod(label='Validate BP Legal Address on Quote' description='Validate Billing Profile Legal Address on Quote')
    public static void validateAccountAndBPonQuote(List<ID> ids) {
        if(ids.size() == 1)
        {
            Map<Id,SBQQ__Quote__c> quote = SEL_Quote.getQuoteById(new Set<Id>{ids[0]});
        	if(quote.size() == 1)
            {
                if(quote.values()[0].Billing_Profile__c != NULL && !SRV_IntegrationOutbound.getSkipAddressValidationFlag(quote.values()[0].Billing_Profile__c))
                {
                    SRV_IntegrationOutbound.asyncValidateAddress(quote.values()[0].Billing_Profile__c);
                }
            }
        }
    }
    //END - SF2-253, Wave C, Sprint 6 - Validate Account and Billing Address on Quote Creation - gcasola - 07-27-2019
}