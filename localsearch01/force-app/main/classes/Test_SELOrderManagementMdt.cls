/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* TestClass for FutureActivationContractBatch
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-06-26
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
@isTest
public class Test_SELOrderManagementMdt {
	
    @isTest
    public static void test_selector(){
        SEL_OrderManagementMdt.requestWithoutProductCode(ConstantsUtil.PRODUCT2_PRODUCTGROUP_LBX, ConstantsUtil.ORDER_STATUS_DRAFT);
        SEL_OrderManagementMdt.requestWithProductCode(ConstantsUtil.MYCAMPAIGN_PROD_GROUP, ConstantsUtil.O_MYCSEARCHBASIC_SETUP001, ConstantsUtil.ORDER_STATUS_DRAFT);
        SEL_OrderManagementMdt.responseWithoutProductCode(ConstantsUtil.PRODUCT2_PRODUCTGROUP_LBX, ConstantsUtil.ORDER_STATUS_PUBLICATION);
    }
}