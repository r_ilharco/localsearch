/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @testClass      Test_changeOwnerController
* @modifiedby     
* @systemLayer    LC Controller
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public without sharing class changeOwnerController {
	@AuraEnabled
    public static ResponseHasUserGrant hasUserGrant(String accountId){
        system.debug('accountId: ' + accountId);
        ResponseHasUserGrant result = new ResponseHasUserGrant();
        List<UserTerritory2Association> territoryAssociations = new List<UserTerritory2Association>();
        String territoryNumbers;
        String territoryName;
        Boolean userAllowed = false;
        Boolean territoryAllowed = true;
        Boolean errorDMC = false;
        Set<String> territoriesToCheck = new Set<String>();
        List<Territory2> territories = new List<Territory2>();
        Boolean okApproved = true;
        Boolean alreadyInApproval = false;
        User currentUser = [SELECT Id, Name, Profile.Name, Code__c,AssignedTerritory__c, (SELECT PermissionSet.Name, PermissionSet.Label, PermissionSet.PermissionsModifyAllData FROM PermissionSetAssignments WHERE PermissionSet.Name IN (:ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME)) FROM User WHERE Id = :UserInfo.getUserId()];
        Account ac = [SELECT Id, Assigned_Territory__c, ApprovalStep__c FROM Account WHERE Id = :accountId LIMIT 1];
        if(ConstantsUtil.ROLE_IN_TERRITORY_DMC.equalsIgnoreCase(currentUser.Code__c)) errorDMC = true;
        
        if(Approval.isLocked(accountId)) alreadyInApproval = true;

        if(String.isNotBlank(ac.Assigned_Territory__c)){
            if(currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA || currentUser.Code__c == 'SMAL' || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD){
                if(ac.Assigned_Territory__c != currentUser.AssignedTerritory__c){
                    territoryAllowed = false;
                }  
            }else if(CurrentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SDI){
                territoriesToCheck.add(ac.Assigned_Territory__c);
            }
        }
        if(territoriesToCheck.size() > 0){
            territories = [SELECT Id, ParentTerritory2.Name FROM Territory2 WHERE Name IN :territoriesToCheck AND ParentTerritory2Id != null];
            for(Territory2 terr : territories){
                if(terr.ParentTerritory2.Name != CurrentUser.AssignedTerritory__c){
                     territoryAllowed = false;
                }
            }
        }
        System.debug('>>> currentUser.Profile.Name: ' + currentUser.Profile.Name);
        System.debug('>>> currentUser.PermissionSetAssignments.size(): ' + currentUser.PermissionSetAssignments.size());
        
        result.territoryAllowed = territoryAllowed;
        userAllowed = isAllowed(currentUser);
        result.alreadyInApproval = alreadyInApproval;
        result.allowed = userAllowed && !alreadyInApproval;
        result.approved = okApproved;
        result.errorDMC = errorDMC;
        system.debug('result: '+ result);
        return result; 
    }
    
    @AuraEnabled
    public static ResponseHasUserGrant hasUserGrantMassive(String accountIds){
        Boolean userAllowed = false;
        Boolean territoryAllowed = true;
        Boolean errorDMC = false;
        ResponseHasUserGrant result = new ResponseHasUserGrant();
        List<Territory2> territories = new List<Territory2>();
        Set<String> territoriesToCheck = new Set<String>();
        Map<String, List<String>> territoryAccountsMap = new Map<String, List<String>>();
        List<UserTerritory2Association> territoryAssociations = new List<UserTerritory2Association>();
        List<Account> accounts = new List<Account>();
        System.debug('accountIds -> ' + JSON.serialize(accountIds));
        String ids = accountIds.remove(' ').remove('[').remove(']');
        Set<String> recordIds = new Set<String>(ids.split(','));
		User currentUser = [SELECT Id, Name, Profile.Name, Code__c, AssignedTerritory__c, (SELECT PermissionSet.Name, PermissionSet.Label FROM PermissionSetAssignments WHERE PermissionSet.Name IN (:ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME)) FROM User WHERE Id = :UserInfo.getUserId()];
        accounts = [SELECT Id, Assigned_Territory__c FROM Account WHERE Id IN :recordIds];
        if(ConstantsUtil.ROLE_IN_TERRITORY_DMC.equalsIgnoreCase(currentUser.Code__c)) errorDMC = true;
        for(Account a : accounts){
            if(String.isNotBlank(a.Assigned_Territory__c)){
            	if(currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA || currentUser.Code__c == 'SMAL' || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD){
                    if(a.Assigned_Territory__c != currentUser.AssignedTerritory__c){
                        territoryAllowed = false;
                    }  
                }else if(CurrentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SDI){
                    territoriesToCheck.add(a.Assigned_Territory__c);
                }
            }
        }
        if(territoriesToCheck.size() > 0){
            territories = [SELECT Id, ParentTerritory2.Name FROM Territory2 WHERE Name IN :territoriesToCheck AND ParentTerritory2Id != null];
            for(Territory2 terr : territories){
                if(terr.ParentTerritory2.Name != CurrentUser.AssignedTerritory__c){
                     territoryAllowed = false;
                }
            }
        }
        userAllowed = isAllowed(currentUser);
        result.territoryAllowed = territoryAllowed;
        result.allowed = userAllowed && territoryAllowed;
        result.errorDMC = errorDMC;
        return result;
    }
    
    
    public static Boolean isAllowed(User currentUser){
        Boolean okAllowed = false;
        Boolean okProfile = false;
        Boolean okPermissionSetConfiguration = false;
        if(ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) ||
           ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
               okProfile = true;
           }
        if(currentUser.PermissionSetAssignments.size() > 0){
            for(PermissionSetAssignment psa: currentUser.PermissionSetAssignments){
                System.debug('>>>currentUser.PermissionSetAssignments.Name: ' + psa.PermissionSet.Name + ' Label: ' + psa.PermissionSet.Label);
                if(ConstantsUtil.SALESPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) && 
                   ConstantsUtil.SALESMANAGERPERMISSIONSET_NAME.equalsIgnoreCase(psa.PermissionSet.Name)){
                       okPermissionSetConfiguration = true;
                   } 
            }
        }else if(ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name) ||
                 ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) || 
                 ConstantsUtil.ROLE_IN_TERRITORY_SDI.equalsIgnoreCase(currentUser.Code__c) ||
                 ConstantsUtil.SYSADMINPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)){
                     okPermissionSetConfiguration = true;
                 }
        okAllowed = okPermissionSetConfiguration && okProfile;
        return okAllowed;
    }
    
    @AuraEnabled
    public static String updateOwner(String recordId, String newOwner, Boolean approvalNeeded) {
        system.debug('updateOwner');
        system.debug('recordId: ' + recordId);
        system.debug('acc: ' + newOwner);
		system.debug('approvalNeeded: ' + approvalNeeded);
        String errorMessage;
        
        Map<String, Object> deserialized = (Map<String, Object>) JSON.deserializeUntyped(newOwner);
        Id userId;
        for(String currentKey : deserialized.keySet()){
            if(currentKey == 'Id'){
                userId = String.valueOf(deserialized.get(currentKey));
                break;
            }
        }
        Account currentAcc = [SELECT Id, ApprovalStep__c, Tech_Requested_Approver__c, Sales_manager_substitute__c, RegionalLeader__c, TerritoryException__c,RegionalDirector__c FROM Account WHERE Id = :recordId LIMIT 1];
        
        if(!approvalNeeded){
            currentAcc.ApprovalStep__c = 'Approved';
            currentAcc.Tech_Submitter__c = userId;
        }
        else{
            currentAcc.Tech_Submitter__c = userId;
            currentAcc.Tech_Change_Owner_Requested__c = true;
            if(currentAcc.RegionalLeader__c != null){
                currentAcc.Tech_Requested_Approver__c = currentAcc.RegionalLeader__c;
                currentAcc.OldApproverId__c = currentAcc.RegionalLeader__c;
            }else if(currentAcc.Sales_manager_substitute__c != null){
                currentAcc.Tech_Requested_Approver__c = currentAcc.Sales_manager_substitute__c;
            }else if(currentAcc.RegionalDirector__c != null){
                currentAcc.Tech_Requested_Approver__c = currentAcc.RegionalDirector__c;
            }else{
                return errorMessage = Label.ChangeOwner_quickaction_NoSMAorSMDorSDI_msg;
            }
        }
        update currentAcc;
        return errorMessage;
    }
    
    @AuraEnabled
    public static void updateOwnerMassive(List<String> recordIds, String newOwner) {
        system.debug('updateOwnerMassive');
        System.debug('recordIds -> ' + JSON.serialize(recordIds));
        system.debug('recordIds: ' + recordIds[0]);
        system.debug('acc: ' + newOwner);
        Map<Id, UserTerritory2Association> ut2aMap;
        /*String ids = recordIds[0].remove(' ').remove('[').remove(']');
		Set<String> accountIds = new Set<String>(ids.split(','));*/
        
        if(recordIds != NULL && recordIds.size() > 0){
            for(Integer i = 0; i < recordIds.size(); i++)
                recordIds[i] = recordIds[i].trim();
        }
        
        Set<String> accountIds = new Set<String>(recordIds);
        
        system.debug('accountIds: ' + accountIds);
        List<Account> accountsToUpdate = new List<Account>();
        Map<String, Object> deserialized = (Map<String, Object>) JSON.deserializeUntyped(newOwner);
        Id userId;
        for(String currentKey : deserialized.keySet()){
            if(currentKey == 'Id'){
                userId = String.valueOf(deserialized.get(currentKey));
                break;
            }
        }
        
        if(userId != NULL)
            ut2aMap = SEL_UserTerritory2Association.getUserTerritory2AssocationsByUserId(new Set<Id>{userId});
        
        if(accountIds.size() > 0 && (ut2aMap != NULL && ut2aMap.size() == 1)){
            Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id, Name, ApprovalStep__c, Assigned_Territory__c, TerritoryException__c FROM Account WHERE Id IN :accountIds]);
            for(String accId : accountIds){
				accounts.get(accId).OwnerId = userId;      
                accountsToUpdate.add(accounts.get(accId));
            }
        }
        
        System.debug('MAP accountsToUpdate -> ' + JSON.serialize(accountsToUpdate));
        if(accountsToUpdate.size() > 0){
            update accountsToUpdate;
        }
    }
    
    @AuraEnabled
    public static List <sObject> getDMCs(String searchKeyWord, String ObjectName, String recordId) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '' + searchKeyWord + '%';
        String searchKey2 = '% ' + searchKeyWord + '%';
        Set<String> userCodes = new Set<String>{ConstantsUtil.ROLE_IN_TERRITORY_DMC, 'SMAL', ConstantsUtil.ROLE_IN_TERRITORY_SMD, ConstantsUtil.ROLE_IN_TERRITORY_SMA, ConstantsUtil.ROLE_IN_TERRITORY_SDI};
        String terrDevName;
        String tempName;
        String ut2aName;
        List<Group> groups = new List<Group>();
        List<String> members = new List<String>();
        List<sObject> returnList = new List<sObject>();
        List <sObject> lstOfRecords = new List<sObject>();
        User currentUser = [SELECT Id, Name, Profile.Name, Code__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SDI 
           || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA 
           || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD
           || currentUser.Code__c == 'SMAL'
           || ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name)
           || ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name)){
               
               if(currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SDI 
                  || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA 
                  || currentUser.Code__c == 'SMAL'
                  || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD){
                      UserTerritory2Association uT2A = [SELECT Id, Territory2.Name, Territory2.ParentTerritory2Id FROM UserTerritory2Association WHERE UserId =:currentUser.id LIMIT 1];
                      List<UserTerritory2Association> ut2a2 = [SELECT Id, Territory2.Name, Territory2.ParentTerritory2Id FROM UserTerritory2Association WHERE UserId =:currentUser.id AND (User.Name LIKE :searchKey OR User.Name LIKE :searchKey2) LIMIT 1];
                      system.debug('');
                      if(uT2A.Territory2.ParentTerritory2Id == null){
                          if(ut2a2 != NULL && ut2a2.size() == 1)
                          	returnList.add(currentUser);
                          ut2aName = 'Territory_' + uT2A.Territory2.Name.remove(' ').remove('Region')+ '%';
                          groups = [SELECT Id, DeveloperName, (Select Id,UserOrGroupId from Groupmembers) FROM group WHERE DeveloperName LIKE :ut2aName and (DeveloperName LIKE 'Territory_%_DMC' OR DeveloperName LIKE 'Territory_%_SMA')];
                      }else{
                          ut2aName = uT2A.Territory2.Name.replace(' ', '_').replace('.', '_') + '%';
                          groups = [SELECT id, name, developername, (Select Id,UserOrGroupId from Groupmembers) Type  FROM group  WHERE developerName LIKE :ut2aName and (DeveloperName LIKE 'Territory_%_DMC' OR DeveloperName LIKE 'Territory_%_SMA')];
                      }
                  }else if(ConstantsUtil.SALESEXCELLENCEPROFILE_LABEL.equalsIgnoreCase(currentUser.Profile.Name) || ConstantsUtil.CUSTOMER_CARE_PROFILE.equalsIgnoreCase(currentUser.Profile.Name)){
                      List<User> srdUsers = [SELECT Id, Name, Profile.Name, Code__c FROM User WHERE Id IN (SELECT UserId FROM UserTerritory2Association WHERE Territory2.Territory2Model.State = 'Active' AND Territory2.ParentTerritory2Id = NULL) AND (Name LIKE :searchKey OR Name LIKE :searchKey2)];
                      if(srdUsers != NULL && srdUsers.size() > 0){	
                          for(User srdUser : srdUsers)
                              returnList.add(srdUser);
                      }
                      groups = [SELECT Id, DeveloperName, (Select Id,UserOrGroupId from Groupmembers) FROM group WHERE DeveloperName LIKE 'Territory_%_DMC' OR DeveloperName LIKE 'Territory_%_SMA'];
                  }
               
               if(groups != NULL && groups.size() > 0){
                   for(Group g : groups){
                       if(g.Groupmembers.size() > 0){
                           Map<Id, Groupmember> groupMembers = new Map<Id, Groupmember>(g.Groupmembers);
                           for(Id gMId : groupMembers.keySet()){
                               members.add(groupMembers.get(gMId).UserOrGroupId);
                           }
                       }
                   }
               
                   if(members.size() > 0){
                       lstOfRecords = [SELECT Id, Name FROM User WHERE Code__c IN :userCodes AND (Name LIKE :searchKey OR Name LIKE :searchKey2) AND Id IN :members];
                   }
               }
               
           }else{
               String sQuery =  'select id, Name from ' +ObjectName + ' where (Name LIKE :searchKey OR Name LIKE :searchKey2) AND Code__c = :userCodes limit 10';
               system.debug('query: ' + sQuery);
               lstOfRecords = Database.query(sQuery);
        }
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    public class ResponseHasUserGrant{
        @AuraEnabled
        public Boolean allowed;
        @AuraEnabled
        public Boolean alreadyInApproval;
        @AuraEnabled
        public Boolean territoryAllowed;
        @AuraEnabled
        public String errorMessage;
        @AuraEnabled
        public Boolean approved;
        @AuraEnabled
        public Boolean errorDMC;
    }
}