/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 14-05-2019
 * Sprint      : 1
 * User Story  : 25
 * Testclass   : Test_ContractInBoundApi
 * Package     : 
 * Description : 
 * Changelog   : 
 */

@RestResource(urlMapping='/Contracts/*')
global with sharing class ContractInBoundApi{
	@HttpGet
    global static void getContractAndSubs(){
        RestResponse res = RestContext.response;
        String contractNumber = RestContext.request.params.get('ContractNumber');
        try {
            Contract contr = SEL_Contract.getContract(contractNumber);
            
            List<SBQQ__Subscription__c> subs = SEL_Subscription.getSubs(contr);

            List<ContractApiWrapper.MySubscription> subscriptions = new List<ContractApiWrapper.MySubscription>();
            for(SBQQ__Subscription__c sub : subs){
                subscriptions.add(new ContractApiWrapper.MySubscription(sub));
            }
            ContractApiWrapper.MyContract c = new ContractApiWrapper.MyContract(contr, subscriptions);
            
            res.responseBody = Blob.valueOf(JSON.serialize(c));
            res.statusCode = 200;
            
            LogUtility.saveContractResponse(RestContext.request,JSON.serialize(c), contractNumber ,contr ,true,null );
        } catch (Exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
             LogUtility.saveContractResponse(RestContext.request,'', contractNumber ,null ,true,null );
            //LogUtility.saveContractResponse(RestContext.request, c,null ,false,ex );
        }
    }
}