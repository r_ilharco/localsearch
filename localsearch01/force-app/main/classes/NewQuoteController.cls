/** ──────────────────────────────────────────────────────────────────────────────────────────────────
* @TestClass       
* @changes		  SPIII-2063 - Add a validation on New Quote Button (newQuoteValidation)
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-29
*/

public without sharing class NewQuoteController {
    
    @AuraEnabled
    public static ResponseWrapper createQuote(Id oppId){
        ResponseWrapper response = new ResponseWrapper();
        User currentUser = [SELECT Id, Code__c, AssignedTerritory__c, Profile.Name  FROM User WHERE Id = :UserInfo.getUserId()];
        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        if(!String.isBlank(oppId)){
             System.debug('currentUser.Code__c  '+currentUser.Code__c );
            List<Opportunity> oppList = [SELECT Id, AccountId, StageName, Account.Assigned_Territory__c, Account.OwnerId, Automated_Opportunity_Flow__c,Type,CloseDate,(Select Id from SBQQ__Quotes2__r) from Opportunity WHERE Id = :oppId LIMIT 1];
             System.debug('oppList  '+oppList );
            if(!oppList.isEmpty()){
                System.debug('oppList[0].Account.OwnerId  '+oppList[0].Account.OwnerId );
                System.debug('currentUser.Id   '+currentUser.Id );
                System.debug('currentUser.Code__c  '+currentUser.Code__c );
                System.debug('currentUser.AssignedTerritory__c '+currentUser.AssignedTerritory__c);
                System.debug('oppList[0].Account.Assigned_Territory__c '+oppList[0].Account.Assigned_Territory__c);
                if(currentUser.Profile.Name!= ConstantsUtil.SysAdmin
                   && oppList[0].Account.OwnerId != currentUser.Id 
                   && currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_DMC ){
                       response.message = Label.New_Quote_Validation;     
                       return response;
                   }
                if(currentUser.Profile.Name!= ConstantsUtil.SysAdmin
                   && oppList[0].Account.OwnerId != currentUser.Id 
                   && currentUser.AssignedTerritory__c != oppList[0].Account.Assigned_Territory__c
                   && (currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMA || currentUser.Code__c == 'SMAL' || currentUser.Code__c == ConstantsUtil.ROLE_IN_TERRITORY_SMD )){
                       response.message = Label.New_Quote_Validation_for_SMA;  
                       return response;
                       }
                Map<Id, SBQQ__Quote__c> quotesMap = SEL_Quote.getQuotesByOpportunityIdsAndStatuses(new Set<Id>{oppId}, new Set<String>{ConstantsUtil.Quote_Status_Accepted});
                if(oppList[0].StageName == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON || oppList[0].StageName == ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST){ 
                    response.message = ConstantsUtil.CANT_CREATE_QUOTE_ON_CLOSED_OPP;
                }else if(quotesMap != NULL && quotesMap.size() > 0){
                    response.message = Label.NewQuoteCtrl_QuoteCreationValidation;                
                }else{
                    if(oppList[0].Type == ConstantsUtil.OPPORTUNITY_TYPE_EXPIRING_CONTRACTS && !string.isBlank(oppList[0].Automated_Opportunity_Flow__c)){
                            quote.SBQQ__ExpirationDate__c  = oppList[0].CloseDate;
                        	if(oppList[0].SBQQ__Quotes2__r.isEmpty()) quote.SBQQ__Primary__c = true;
                    }
                    quote.SBQQ__Opportunity2__c = oppList[0].id;
                    quote.SBQQ__Account__c = oppList[0].AccountId;
                    insert quote;
                }
            }
        }
        if(quote.id != null){
            response.quoteId = quote.id;
        }
        
        return response;
    }
    
    public Class ResponseWrapper{
        @AuraEnabled
        public String quoteId {get; set;}
        @AuraEnabled
        public String message {get; set;}
        
    }
    
}