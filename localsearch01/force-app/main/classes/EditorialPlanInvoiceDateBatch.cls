/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 25-11-2019
 * Sprint      : 
 * User Story  : 
 * Testclass   : Test_PublishEditorialPlanController
 * Package     : 
 * Description : 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-2000 Publish Edition Batch Issue ( Subscription )
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-13
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-3236 / 'SwissbillingBulkifiedTransferJob' for job id '7071r0000AWoh8s' : Attempt to de-reference a null object
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-09-16
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-3236 / 'SwissbillingBulkifiedTransferJob' for job id '7071r0000AWoh8s' : Attempt to de-reference a null object
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-10-12              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘*/
global class EditorialPlanInvoiceDateBatch implements Database.batchable<sObject>{ 
    
    private Date publicationDate;
    private String editionName;
    private Decimal editionNumber;
    
    public EditorialPlanInvoiceDateBatch(Date pubDate, String edName, Decimal edNumber){
        this.publicationDate = pubDate;
        this.editionName = edName;
    	this.editionNumber = edNumber;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){   
        System.debug('edName '+editionName);
        System.debug('editionNumber '+editionNumber);
        return Database.getQueryLocator('SELECT Id, RequiredBy__c, RequiredBy__r.Next_Invoice_Date__c, Next_Invoice_Date__c, SBQQ__QuoteLine__r.Edition__r.Publication_Date__c, Number_of_Publications__c,' +
                                        'SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c, SBQQ__QuoteLine__r.Edition__r.Name, RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c,'+
                                        'RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Name ' +
                                      	'FROM SBQQ__Subscription__c ' + 
                                      	'WHERE Total__c >0 AND ((SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c = :editionNumber AND SBQQ__QuoteLine__r.Edition__r.Name = :editionName AND Next_Invoice_Date__c != NULL) OR '+
                                        '(RequiredBy__c != NULL AND RequiredBy__r.Next_Invoice_Date__c != NULL '+
                                        'AND RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c = :editionNumber '+
                                        'AND RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Name = :editionName)) ' +
                                        'AND ((SBQQ__QuoteLine__r.Product_Group__c = \'' + ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT + '\' '+
                                        'AND Number_of_Publications__c !=0 ) '+
                                        'OR (RequiredBy__r.SBQQ__QuoteLine__r.Product_Group__c = \'' + ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT + '\' '+
                                        ' AND RequiredBy__r.Number_of_Publications__c !=0)) '
                                     	);
   	}
    
   	global void execute(Database.BatchableContext info, List<SBQQ__Subscription__c> scope){
        try{
            Map<Id, SBQQ__Subscription__c> subscriptionsMap = new Map<Id, SBQQ__Subscription__c>(scope);
            Set<Id> missingNextInvoiceDateSubs = new Set<Id>();
            List<SBQQ__Subscription__c> missingNextInvoiceDateSubsToUpdateList = new List<SBQQ__Subscription__c>();
            List<SBQQ__Subscription__c> valuedNextInvoiceDateSubs = new List<SBQQ__Subscription__c>();
            
            for(SBQQ__Subscription__c sub : scope){
                if(sub.Next_Invoice_Date__c == NULL)
                    missingNextInvoiceDateSubs.add(sub.Id);
                else
                    valuedNextInvoiceDateSubs.add(sub);
            }
            List<Editorial_Plan__c> editorialPlans = [Select Id, Name, Publication_Date__c, Local_Guide_Number__c
                                                      FROM Editorial_Plan__c 
                                                      WHERE Name = :editionName AND Local_Guide_Number__c = :editionNumber
                                                      AND Publication_Date__c =:publicationDate
                                                      ORDER BY Publication_Date__c asc];
            Map<String,Date> editionPublicationDatesMap = new Map<String,Date>();
            String edName = '';
            String edNumber = '';
            String key ='';
            for(Editorial_Plan__c edition : editorialPlans){
                edName = edition.Name;
                edNumber = String.valueOf(edition.Local_Guide_Number__c);
                key = edName + edNumber;
                editionPublicationDatesMap.put(key, edition.Publication_Date__c);    
            }
            Map<Id, SBQQ__Subscription__c> subscriptionsToUpdateMap = new Map<Id, SBQQ__Subscription__c>();
            String editorialName = '';
            Integer editorialNumber;
            String editorialNumberString = '';
            string uniqueKey = '';
            Date dateToCheck ;
            for(SBQQ__Subscription__c currentSub : valuedNextInvoiceDateSubs){
                if(currentSub.RequiredBy__c== null){
                    editorialName = currentSub.SBQQ__QuoteLine__r.Edition__r.Name;
                    editorialNumber =(Integer) currentSub.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c;
                }
                else{
                    editorialName = currentSub.RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Name;
                    editorialNumber =(Integer) currentSub.RequiredBy__r.SBQQ__QuoteLine__r.Edition__r.Local_Guide_Number__c;
                }
                editorialNumberString = String.valueOf(editorialNumber);
                uniqueKey = editorialName + editorialNumberString;
               
                if(editionPublicationDatesMap.containsKey(uniqueKey)){
                    dateToCheck = editionPublicationDatesMap.get(uniqueKey);
                    currentSub.Next_Invoice_Date__c = dateToCheck;
                    if(currentSub.Number_of_Publications__c > 0){
                        currentSub.Number_of_Publications__c = (Integer)currentSub.Number_of_Publications__c - 1;
                    }
                    subscriptionsToUpdateMap.put(currentSub.Id, currentSub);
                }
            }
            if(missingNextInvoiceDateSubs != NULL && missingNextInvoiceDateSubs.size() > 0){
                for(Id missingNextInvoiceDateSubId : missingNextInvoiceDateSubs){
                    SBQQ__Subscription__c sub = subscriptionsMap.get(missingNextInvoiceDateSubId);
                    if(sub.RequiredBy__c != NULL && sub.RequiredBy__r.Next_Invoice_Date__c != NULL){
                        sub.Next_Invoice_Date__c = sub.RequiredBy__r.Next_Invoice_Date__c;
                        if(subscriptionsToUpdateMap.containsKey(sub.Id))
                            subscriptionsToUpdateMap.get(sub.Id).Next_Invoice_Date__c = sub.RequiredBy__r.Next_Invoice_Date__c;
                        else
                            subscriptionsToUpdateMap.put(sub.Id,sub);
                    }
                }
            }
            
            if(subscriptionsToUpdateMap.size() > 0){
                
                update subscriptionsToUpdateMap.values();
            }
            
        }catch(Exception ex){
            system.debug('Error ex: ' + ex.getMessage() + '/n at line: ' + ex.getStackTraceString());
        }
   	}
    
   	global void finish(Database.BatchableContext info){
   		//...     
	}
}