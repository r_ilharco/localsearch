/*
* Author      : Joao Soares
* Date	      : 2021-01-21
* Sprint      : Sprint 11
* Work item   : SPIII-4072 - Primary contact empty on accepted Quotes
* Testclass   : Cls_SetQuotePrimaryContact_Test
*/



global without sharing class Cls_SetQuotePrimaryContact implements Database.Batchable<sObject>, Schedulable{
    
    global integer limitSize = 0;
    
    global Cls_SetQuotePrimaryContact(integer limitValue){
        this.limitSize = limitValue;
    }
    
    
    global void execute(SchedulableContext sc) {
        Cls_SetQuotePrimaryContact a = new Cls_SetQuotePrimaryContact(this.limitSize); 
        Database.executeBatch(a, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){    
        string query = 'select id,SBQQ__PrimaryContact__c,Filtered_Primary_Contact__c, SBQQ__Account__c'+
            +' From SBQQ__Quote__c' + 
            +' where Filtered_Primary_Contact__c = null and SBQQ__Status__c = \'' + ConstantsUtil.Quote_Status_Accepted + '\'';
        
        if(limitSize>0) query += ' Limit :limitSize';
        system.debug('Query == '+query);
        return Database.getQueryLocator(query);   
        
    }
    
    global void execute(Database.BatchableContext info, List<SBQQ__Quote__c> scope){
        
        
        try{
            List<SBQQ__Quote__c> quoteUp = new List<SBQQ__Quote__c>();
            Set<Id> accids = new Set<Id>();
            Map<Id,Id> contacts = new Map<Id,Id>();
            
            for(SBQQ__Quote__c quote : scope){
                accids.add(quote.SBQQ__Account__c);
            }
            for(Contact c : [select id, AccountId from Contact where Primary__c = true and AccountId  in: accids]){
               if(!contacts.containsKey(c.AccountId)){
                   contacts.put(c.AccountId,c.Id);
               }
            }
            
            for(SBQQ__Quote__c quote : scope){
                if(contacts.containsKey(quote.SBQQ__Account__c)){
                    quote.SBQQ__PrimaryContact__c = contacts.get(quote.SBQQ__Account__c);
                    quote.Filtered_Primary_Contact__c = contacts.get(quote.SBQQ__Account__c);
                    quoteUp.add(quote);
                }
            }
            
            String log = '';  
            system.debug('quoteUp '+quoteUp.size());
            
            if(!quoteUp.isempty()){
                QuoteTriggerHandler.disableTrigger = true;
                SBQQ.TriggerControl.disable();
                List<Database.SaveResult> resultsQuotes = Database.update(quoteUp, false);
                SBQQ.TriggerControl.enable();
                QuoteTriggerHandler.disableTrigger = false;
                
                for (Database.SaveResult sr : resultsQuotes) {
                    if (!sr.isSuccess()) {              
                        for(Database.Error err : sr.getErrors()) {
                            log += err.getMessage()+ ' ';
                            System.debug(err.getMessage()+ ' field: '+ err.getFields() );
                        }
                        ErrorHandler.log(System.LoggingLevel.ERROR, 'Cls_SetQuotePrimaryContact', 'Cls_SetQuotePrimaryContact.execute', null, ErrorHandler.ErrorCode.E_DML_FAILED, log, null, null, null, null, false);
                    }else{
                        System.debug('Updated :: '+sr.getId() );
                        
                    }
                }  
            }    
            
            
            
        }catch(Exception e){
            system.debug('Exception :: '+e);
            ErrorHandler.log(System.LoggingLevel.ERROR, 'SetRevenueAmount_batch', 'SetRevenueAmount_batch.execute', e, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);
        }
        
        
    }
    
    global void finish(Database.BatchableContext context) {}
    
    public static String sched = '0 0 0-6 ? * * *';
    global static string scheduleMe() {
        string name = 'Cls_SetQuotePrimaryContact';
        integer size = 40000;
        if (Test.isRunningTest() ) {
            name = name + system.now();
        }
        return scheduleMe(name, sched,size) ;      
    }
    
    global static string scheduleMe(string name,String schedule, integer size) {
        return System.schedule(name, schedule, new Cls_SetQuotePrimaryContact(size));       
    }
    
}