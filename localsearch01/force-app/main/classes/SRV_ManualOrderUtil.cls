public without sharing class SRV_ManualOrderUtil {

    public static void cancelContract(Set<Id> contractsToCancel){
        List<Contract_Setting__mdt> contractCancellation = [SELECT  InvoiceStatus__c 
                                                            FROM    Contract_Setting__mdt 
                                                            WHERE   MasterLabel = 'ContractCancellation_Process']; 

        List<String> invoiceStatus = contractCancellation[0].InvoiceStatus__c.split(';');
        Map<Id, Invoice__c> invoiceToCancel = SEL_Invoices.getInvoicesByContractIDsAndStus(contractsToCancel, invoiceStatus);
        SRV_Invoice.cancelInvoices(new Set<Id> (invoiceToCancel.keySet())); 
    }

    public static void terminateContract(Set<Id> contractsToTerminate){
        List<Invoice_Order__c> iOrders = SEL_InvoiceOrders.getInvoiceOrderbyInvoicebyStatus(ConstantsUtil.INVOICE_CANCEL_STATUS, contractsToTerminate);
        if(!iOrders.isEmpty()){
            for(Invoice_Order__c io : iOrders){
                System.enqueueJob(new CancellationEnqueueJob(io.Invoice__r));
            }
        }
    }

    public static void generateOrderMap(List<Order> orders, Map<Id, Order> orderMap){
        for(Order currentOrder : orders){
            orderMap.put(currentOrder.Id, currentOrder);
        }
    }

    public static void generateOrderToOrderItemsMap(List<OrderItem> orderItems, Map<Id, Set<OrderItem>> orderToOrderItems){
        for(OrderItem currentOI : orderItems){
            if(!orderToOrderItems.containsKey(currentOI.OrderId)){
                orderToOrderItems.put(currentOI.OrderId, new Set<OrderItem>{currentOI});
            }
            else{
                orderToOrderItems.get(currentOI.OrderId).add(currentOI);
            }
        }
    }

    public static Map<String, Date> generateEndDateMap(List<AggregateResult> maxEndDates){
        Map<String,Date> endDateMap = new Map<String,Date>();
        for(AggregateResult l : maxEndDates){
            endDateMap.put((String)l.get('SBQQ__Contract__c'),(Date)l.get('expr0'));
        }
        return endDateMap;
    } 

    public static void cancelContract(Contract contract, Map<String, Date> endDateMap, Id contractId){
        Date endDate = Date.today().addDays(-1);
        contract.Status = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
        contract.TerminateDate__c = endDateMap.get(contractId) == null ? endDate : endDateMap.get(contractId);
        contract.Cancel_Date__c = endDateMap.get(contractId) == null ? endDate : endDateMap.get(contractId);
        contract.In_Cancellation__c = false;
        contract.ByPass_Terminate_cancelation__c = true;
    }

    public static void cancelSubscription(List<SBQQ__Subscription__c> subscriptions, Map<String, Date> endDateMap, Set<SBQQ__Subscription__c> subscriptionsToUpdate, Id contractId){
        Date endDate = Date.today().addDays(-1);
        for(SBQQ__Subscription__c currentSubscription: subscriptions){
            currentSubscription.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED;
            currentSubscription.In_Termination__c = false;
            currentSubscription.Termination_Date__c =  endDateMap.get(contractId) == null ? endDate : endDateMap.get(contractId);
            subscriptionsToUpdate.add(currentSubscription);
        }
    }

    public static void terminateContract(Contract contract, Map<String, Date> endDateMap, Id contractId){
        Date endDate = Date.today().addDays(-1);
        contract.TerminateDate__c = endDateMap.get(contractId) == null ? endDate : endDateMap.get(contractId);
        contract.In_Termination__c = false;
        contract.ByPass_Terminate_cancelation__c = true;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
    }

    public static List<SBQQ__Subscription__c> getSubscriptionsToTerminate(List<SBQQ__Subscription__c> subscriptions, Set<OrderItem> orderItems, Id contractId){
        List<SBQQ__Subscription__c> subscriptionsToTerminate = new List<SBQQ__Subscription__c>();
        Set<Id> subscriptionsToTerminateIds = new Set<Id>();
        for(OrderItem currentOI : orderItems){
            subscriptionsToTerminateIds.add(currentOI.SBQQ__Subscription__c);
        }
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            if(subscriptionsToTerminateIds.contains(currentSubscription.Id) && currentSubscription.In_Termination__c && currentSubscription.SBQQ__Contract__c == contractId){
                subscriptionsToTerminate.add(currentSubscription);
            }
        }
        return subscriptionsToTerminate;
    }

    public static void terminateSubscription(List<SBQQ__Subscription__c> subscriptions, Map<String, Date> endDateMap, Set<SBQQ__Subscription__c> subscriptionsToUpdate, Id contractId, Boolean isUpgrade){
        Date endDate = Date.today().addDays(-1);
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            if(currentSubscription.Termination_Date__c <= Date.today()){
                currentSubscription.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
                currentSubscription.In_Termination__c = false;
                //currentSubscription.Termination_Date__c =  Date.today();
                //currentSubscription.SBQQ__TerminatedDate__c =  Date.today();
                if(isUpgrade) currentSubscription.Termination_Generate_Credit_Note__c = true;
                subscriptionsToUpdate.add(currentSubscription);
            }
        }
    }

    public static List<SBQQ__Subscription__c> getUpgradeSubscriptionsToTerminate(List<SBQQ__Subscription__c> subscriptions, Set<OrderItem> orderItems, Id contractId){
        List<SBQQ__Subscription__c> subscriptionsToTerminate = new List<SBQQ__Subscription__c>();
        Set<Id> subscriptionsToTerminateIds = new Set<Id>();
        for(OrderItem currentOI : orderItems){
            //if(String.isNotBlank(currentOI.Subscription_To_Terminate__c)) subscriptionsToTerminateIds.add(currentOI.Subscription_To_Terminate__c);
            if(String.isNotBlank(currentOI.SBQQ__Subscription__c)) subscriptionsToTerminateIds.add(currentOI.SBQQ__Subscription__c);
        }
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            if( (subscriptionsToTerminateIds.contains(currentSubscription.Id) || subscriptionsToTerminateIds.contains(currentSubscription.SBQQ__RequiredById__c) ) && currentSubscription.In_Termination__c && currentSubscription.SBQQ__Contract__c == contractId){
                subscriptionsToTerminate.add(currentSubscription);
            }
        }
        return subscriptionsToTerminate;
    }

    public static void evaluateUpgradeActivationOrder(Set<Id> activationUpgradeOrderId){
        Set<Id> orderToCase = new Set<Id>();
        System.debug('VLAUDATO ACT UPGRADE/DOWNGRADE ORDER ID: '+activationUpgradeOrderId);
        List<OrderItem> orderItems = new List<OrderItem>();
        Map<Id, Set<OrderItem>> orderToOrderItems = new Map<Id, Set<OrderItem>>();
               
        Map<String, List<Product_Activation_JSON__mdt>> productToJsonConfigMdt = new Map<String, List<Product_Activation_JSON__mdt>>();
        Set<String> productCodes = new Set<String>();
        Set<String> queryFields = ConstantsUtil.QUERY_FIELDS;
		queryFields.add('Order.Contract__r.Phase1Source__c');
        List<Order> upgradeDowngradeOrders = SEL_Order.getOrderByIds(activationUpgradeOrderId); 
        orderItems = SEL_OrderItem.getOrderItemsByOrderIds(activationUpgradeOrderId, queryFields);
        for(OrderItem oi : orderItems){
            productCodes.add(oi.Product2.ProductCode);
        }
        
        if(!productCodes.isEmpty()){
            List<Product_Activation_JSON__mdt> productAdditionalAttributes = [SELECT  Product_Code__c, 
                                                                              		  Key__c,
                                                                                      Object_API_Name__c,
                                                                                      Field_API_Name__c,
                                                                                      Child_Object_API_Name__c,
                                                                                      Object_Is_Custom__c
                                                                              FROM    Product_Activation_JSON__mdt
                                                                              WHERE   Product_Code__c IN :productCodes];
            
            for(Product_Activation_JSON__mdt currentMdt : productAdditionalAttributes){
                if(!productToJsonConfigMdt.containsKey(currentMdt.Product_Code__c)){
                    productToJsonConfigMdt.put(currentMdt.Product_Code__c, new List<Product_Activation_JSON__mdt>{currentMdt});
                }
                else{
                    productToJsonConfigMdt.get(currentMdt.Product_Code__c).add(currentMdt);
                }    
                SRV_OrderUtil.addQueryFields(currentMdt,queryFields);
            }
            orderItems = SEL_OrderItem.getOrderItemsByOrderIds(activationUpgradeOrderId, queryFields);
            for(OrderItem oi : orderItems){
                SRV_OrderUtil.generateSetOIValueMap(orderToOrderItems, oi.OrderId, oi);
            }
        }
        
        for(Order o : upgradeDowngradeOrders){
            List<Order_Management__mdt> orderManagementMetadata = getOrderManagementMdt(orderToOrderItems.get(o.Id), o);
            if(orderManagementMetadata != null && !orderManagementMetadata.isEmpty()){
                if(!orderManagementMetadata[0].Manual_Activation__c){
                    o.Status =  orderManagementMetadata[0].Next_Status__c;
                    o.CallId__c = SRV_OrderUtil.getUniqueId(o.Id);
                	SRV_OrderUtil.callExternalSystem(o, new Set<OrderItem>(orderToOrderItems.get(o.Id)), productToJsonConfigMdt, null, null, null);
                }
                else if(orderManagementMetadata[0].Manual_Activation__c){
                    orderToCase.add(o.Id);
                }
                
            }
        }
        update upgradeDowngradeOrders;
        if(!orderToCase.isEmpty()){
            OrderCaseUtility.generateCase(orderToCase);
        }
    }
    
    public static List<Order_Management__mdt> getOrderManagementMdt(Set<OrderItem> orderItemsOrder, Order currentOrder){
        List<Order_Management__mdt> orderManagementMetadata;
        
        for(OrderItem currentOrderItem : orderItemsOrder){
            if(currentOrderItem.SBQQ__RequiredBy__c == null){
                String currentProductGroup = currentOrderItem.Product2.Product_Group__c;
                orderManagementMetadata = [SELECT   Product_Group__c,
                                           Product_Code__c,
                                           Current_Status__c,
                                           Next_Status__c,
                                           Send_to_Backend__c,
                                           Manual_Activation__c,
                                           Manual_Termination__c,
                                           Manual_Cancellation__c,
                                           Manual_Amend__c
                                           FROM    Order_Management__mdt
                                           WHERE   Product_Group__c = :currentProductGroup
                                           AND     Current_Status__c = :currentOrder.Status
                                           LIMIT   1];
                break;
            } 
        }
        return orderManagementMetadata;
    }

    public static void updateCase(Set<Id> orderIds){
        System.debug('updateCase '+orderIds);
        List<Case> cases = new  List<Case>();
        for(Case c : [SELECT Id, Status FROM Case WHERE Order__c IN :orderIds AND RecordType.DeveloperName = :ConstantsUtil.CASE_RECORDT_MANUAL]){
            c.status = ConstantsUtil.CASE_CLOSED_STATUS;
            cases.add(c);
        } 
        if(!cases.isEmpty()){
            update cases;
        }
    }
}