/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
*	The Territory Assignment Rules Service
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-10-05
* @systemLayer    Service
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Territory Hierarchy Wrong Populated - Territory Model Code Review (SPIII-3856)
* @modifiedby     Gennaro Casola <gcasola@deloitte.it>
* 2020-10-15
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class SRV_TerritoryRules {
    
    public static void setFieldsOnAccount(Account acc, Id Territory2Id, UserTerritoryHierarchyInfo utHierarchyInfo, BillingPostalCodesTerritoryMappingInfo bpctmInfo){
        //TerritoryException
        //AssignedTerritory
        //SRD, SMA, SMD
        //OwnerId
        //
        System.debug('Territory2Id --> ' + Territory2Id);
        System.debug('utHierarchyInfo != NULL --> ' + utHierarchyInfo != NULL);
        if(utHierarchyInfo != NULL){
        	System.debug('utHierarchyInfo.territoriesMap != NULL ' + utHierarchyInfo.territoriesMap != NULL);
            System.debug('utHierarchyInfo.territoriesMap.containsKey(Territory2Id) --> ' + utHierarchyInfo.territoriesMap.containsKey(Territory2Id));
        }
        

        if(Territory2Id == NULL){
            acc.TerritoryException__c = false;
            acc.Assigned_Territory__c = NULL;
            acc.RegionalDirector__c = NULL;
            acc.RegionalLeader__c = NULL;
            acc.Sales_manager_substitute__c = NULL;
        }else if(utHierarchyInfo != NULL && utHierarchyInfo.territoriesMap != NULL && utHierarchyInfo.territoriesMap.containsKey(Territory2Id)){
            Territory2 territory = utHierarchyInfo.territoriesMap.get(Territory2Id);
            Boolean isRegion = (territory.ParentTerritory2Id == NULL);
            Boolean existsTerritoryRule = bpctmInfo != NULL && bpctmInfo.availableTerritoryRulesDNamesSet != NULL && bpctmInfo.availableTerritoryRulesDNamesSet.contains(territory.DeveloperName);
            UserTerritory2Association ut2aOwner;
            if(utHierarchyInfo != NULL && utHierarchyInfo.ut2aByUserId != NULL && utHierarchyInfo.ut2aByUserId.containsKey(acc.OwnerId) && utHierarchyInfo.ut2aMap != NULL && utHierarchyInfo.ut2aMap.containsKey(utHierarchyInfo.ut2aByUserId.get(acc.OwnerId))){
                ut2aOwner = utHierarchyInfo.ut2aMap.get(utHierarchyInfo.ut2aByUserId.get(acc.OwnerId));
            }
            
            if(existsTerritoryRule)
            	acc.Assigned_Territory__c = territory.Name;
            else
                acc.Assigned_Territory__c = NULL;
            
            if(existsTerritoryRule && String.isNotBlank(acc.BillingPostalCode) && bpctmInfo != NULL && bpctmInfo.territoryDeveloperNamesByPostalCodeMap != NULL && bpctmInfo.territoryDeveloperNamesByPostalCodeMap.containsKey(acc.BillingPostalCode)){
                String territoryDeveloperNamesByPostalCode = bpctmInfo.territoryDeveloperNamesByPostalCodeMap.get(acc.BillingPostalCode);
                String territoryDeveloperName = territory.DeveloperName;
                acc.TerritoryException__c = !territoryDeveloperName.equalsIgnoreCase(territoryDeveloperNamesByPostalCode);
            }else if(existsTerritoryRule){
                acc.TerritoryException__c = true;
            }else{
                acc.TerritoryException__c = false;
            }
            
            if(existsTerritoryRule){
                if(isRegion){
                    if(utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                       && utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI))
                        acc.RegionalDirector__c = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                    else
                        acc.RegionalDirector__c = NULL;
                    
                    acc.RegionalLeader__c = NULL;
            		acc.Sales_manager_substitute__c = NULL; 
                    
                }else{
                    
                    if(utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(territory.ParentTerritory2Id)
                       && utHierarchyInfo.userTerritoryHierarchyMap.get(territory.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI))
                        acc.RegionalDirector__c = utHierarchyInfo.userTerritoryHierarchyMap.get(territory.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                    else
                        acc.RegionalDirector__c = NULL;
                    
                    if(utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                       && utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA))
                        acc.RegionalLeader__c = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                    else
                        acc.RegionalLeader__c = NULL;
                    
                    if(utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                       && utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD))
                        acc.Sales_manager_substitute__c = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD);
                    else
                        acc.Sales_manager_substitute__c = NULL;
                    
                }
            }else{
              	acc.RegionalDirector__c = NULL;
            	acc.RegionalLeader__c = NULL;
            	acc.Sales_manager_substitute__c = NULL;  
            }
            
            if(existsTerritoryRule && (ut2aOwner == NULL || (ut2aOwner != NULL && !ut2aOwner.Territory2.DeveloperName.equalsIgnoreCase(territory.DeveloperName)))){
                if(isRegion){
                    if(utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                       && utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI))
                        acc.OwnerId = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                }else{
                    Boolean hasOwnerChanged = false;
                    if(!hasOwnerChanged && utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                   	&& utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)){
                        acc.OwnerId = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                        hasOwnerChanged = true;
                   }

					if(!hasOwnerChanged && utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(Territory2Id)
                   	&& utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMD)){
                        acc.OwnerId = utHierarchyInfo.userTerritoryHierarchyMap.get(Territory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SMD);
                        hasOwnerChanged = true;
                   }

					if(!hasOwnerChanged && utHierarchyInfo.userTerritoryHierarchyMap != NULL 
                       && utHierarchyInfo.userTerritoryHierarchyMap.containsKey(territory.ParentTerritory2Id)
                   	&& utHierarchyInfo.userTerritoryHierarchyMap.get(territory.ParentTerritory2Id).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)){
                        acc.OwnerId = utHierarchyInfo.userTerritoryHierarchyMap.get(territory.ParentTerritory2Id).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);
                        hasOwnerChanged = true;
                   }                    
                }
            }
        }
        
    }
    
    public static BillingPostalCodesTerritoryMappingInfo getTerritoryDeveloperNamesByPostalCodeMap(){
        BillingPostalCodesTerritoryMappingInfo bpctmInfo = new BillingPostalCodesTerritoryMappingInfo();
        Map<String, String> territoryDeveloperNamesByPostalCodeMap = bpctmInfo.territoryDeveloperNamesByPostalCodeMap;
        Set<String> availableTerritoryRulesDNamesSet = bpctmInfo.availableTerritoryRulesDNamesSet;
        
        Map<Id, ObjectTerritory2AssignmentRuleItem> ot2aRuleItemsMap = new Map<Id, ObjectTerritory2AssignmentRuleItem>([SELECT Id, RuleId, Rule.DeveloperName, Value FROM ObjectTerritory2AssignmentRuleItem WHERE Field = 'Account.Tech_ZipCode__c' AND Rule.IsActive = true]);
		List<String> billingPostalCodesList = new List<String>();
        
        if(ot2aRuleItemsMap != NULL && ot2aRuleItemsMap.size() > 0){
            for(Id ot2aRuleItemId : ot2aRuleItemsMap.keySet()){
                ObjectTerritory2AssignmentRuleItem ot2aRuleItem = ot2aRuleItemsMap.get(ot2aRuleItemId);
                billingPostalCodesList.clear();
                billingPostalCodesList.addAll(ot2aRuleItem.value.split(','));
                
                if(billingPostalCodesList != NULL && billingPostalCodesList.size() > 0){
                    for(String billingPostalCode : billingPostalCodesList)
                        territoryDeveloperNamesByPostalCodeMap.put(billingPostalCode, ot2aRuleItem.Rule.DeveloperName);
                }
            }
        }
        
        if(territoryDeveloperNamesByPostalCodeMap != NULL && territoryDeveloperNamesByPostalCodeMap.size() > 0)
        	availableTerritoryRulesDNamesSet.addAll(territoryDeveloperNamesByPostalCodeMap.values());
		        
        return bpctmInfo;
    }
    
    public static UserTerritoryHierarchyInfo getUserTerritoryHierarchyMap(){
        return getUserTerritoryHierarchyMap(new Set<Id>());
    }
    
    public static UserTerritoryHierarchyInfo getUserTerritoryHierarchyMap(Set<Id> userIds){
        UserTerritoryHierarchyInfo utHierarchyInfo = new UserTerritoryHierarchyInfo();
        Map<Id, Map<String, Id>> userTerritoryHierarchyMap;
        Map<Id, UserTerritory2Association> ut2aMap;
        Map<Id, Id> ut2aByUserId;
        Map<Id, Territory2> territoriesMap;
        Map<String, Id> territory2IdByDeveloperNameMap;
        
        List<String> rolesInTerritoryList = new List<String>{ConstantsUtil.ROLE_IN_TERRITORY_SDI, ConstantsUtil.ROLE_IN_TERRITORY_SMA, ConstantsUtil.ROLE_IN_TERRITORY_SMD};
        utHierarchyInfo.ut2aMap = new Map<Id, UserTerritory2Association>([SELECT Id, UserId, Territory2Id, Territory2.Name, Territory2.DeveloperName, 
                                                                          Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.Name, Territory2.ParentTerritory2.DeveloperName, 
                                                                          RoleInTerritory2 
                                                                          FROM UserTerritory2Association
                                                                          WHERE Isactive = true 
                                                                          AND Territory2.Territory2Model.State = 'Active' 
                                                                          AND (RoleInTerritory2 IN :rolesInTerritoryList OR UserId IN :userIds)]);
		
        userTerritoryHierarchyMap = utHierarchyInfo.userTerritoryHierarchyMap;
        ut2aMap = utHierarchyInfo.ut2aMap;
        ut2aByUserId = utHierarchyInfo.ut2aByUserId;
        territory2IdByDeveloperNameMap = utHierarchyInfo.territory2IdByDeveloperNameMap;
        
        utHierarchyInfo.territoriesMap = new Map<Id, Territory2>([SELECT Id, Name, Territory2TypeId, Territory2ModelId, ParentTerritory2Id, DeveloperName FROM Territory2 WHERE Territory2Model.State = 'Active']);
        territoriesMap = utHierarchyInfo.territoriesMap;
        
        for(Id territory2Id : territoriesMap.keySet()){
            Territory2 territory = territoriesMap.get(territory2Id);
            territory2IdByDeveloperNameMap.put(territory.DeveloperName, territory2Id);
        }
        
        if(ut2aMap != NULL && ut2aMap.size() > 0){
            for(Id ut2aId : ut2aMap.keySet()){
                UserTerritory2Association ut2a = ut2aMap.get(ut2aId);
                ut2aByUserId.put(ut2a.UserId, ut2aId);
                if(rolesInTerritoryList.contains(ut2a.RoleInTerritory2)){
                    if(!userTerritoryHierarchyMap.containsKey(ut2a.Territory2Id))
						userTerritoryHierarchyMap.put(ut2a.Territory2Id, new Map<String, Id>());
                    
                    userTerritoryHierarchyMap.get(ut2a.Territory2Id).put(ut2a.RoleInTerritory2, ut2a.UserId);
                }
                
            }
        }
        
        return utHierarchyInfo;
    }
    
    public class UserTerritoryHierarchyInfo{
        public Map<Id, Map<String, Id>> userTerritoryHierarchyMap;
        public Map<Id, UserTerritory2Association> ut2aMap;
        public Map<Id, Id> ut2aByUserId;
        public Map<Id, Territory2> territoriesMap;
        public Map<String, Id> territory2IdByDeveloperNameMap;
        
        public UserTerritoryHierarchyInfo(){
            userTerritoryHierarchyMap = new Map<Id, Map<String, Id>>();
            ut2aByUserId = new Map<Id, Id>();
            territory2IdByDeveloperNameMap = new Map<String, Id>();
            territoriesMap = new Map<Id, Territory2>();
        }
    }
    
    public class BillingPostalCodesTerritoryMappingInfo{
        public Map<String, String> territoryDeveloperNamesByPostalCodeMap;
        public Set<String> availableTerritoryRulesDNamesSet;
        
        BillingPostalCodesTerritoryMappingInfo(){
            territoryDeveloperNamesByPostalCodeMap = new Map<String, String>();
            availableTerritoryRulesDNamesSet = new Set<String>();
        }
    }
    
}