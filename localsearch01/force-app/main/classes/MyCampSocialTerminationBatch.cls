/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Send contracts termination message to MyCampaigns Social backend.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato <vlaudato@deloitte.it>
* @created        2020-07-13
* @systemLayer    Invocation
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class MyCampSocialTerminationBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts{
    
    global void execute(SchedulableContext sc) {
        MyCampSocialTerminationBatch b = new MyCampSocialTerminationBatch(); 
        database.executebatch(b, 1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Integer nextNDays = 0;
        Integer nextNDaysMinus1 = 0;
        
        GlobalConfiguration_cs__c setting = GlobalConfiguration_cs__c.getOrgDefaults();
        if(setting.MyCamp_Social_Termination_Days__c != null){
            nextNDays = Integer.valueOf(setting.MyCamp_Social_Termination_Days__c);
        	nextNDaysMinus1 = Integer.valueOf(setting.MyCamp_Social_Termination_Days__c)-1;
        }
		        
        Set<String> myCampSocialProductCodes = new Set<String>{ConstantsUtil.MYCSOCIAL_BOOSTER, ConstantsUtil.MYCSOCIAL_SOCIAL ,ConstantsUtil.MYCSOCIAL_AWARE ,ConstantsUtil.MYCSOCIAL_LEAD};
        
        return Database.getQueryLocator('SELECT Id, MigratedTermination__c, Contract__c, Contract__r.Phase1Source__c FROM Order WHERE Id IN (' +
            							'SELECT OrderId ' +
                                        'FROM OrderItem ' +
                                        'WHERE ServiceDate = NEXT_N_DAYS:' + nextNDays + ' ' +
                                        'AND   ServiceDate > NEXT_N_DAYS:' + nextNDaysMinus1 + ' ' +
                                        'AND ( Order.Type = \'' + ConstantsUtil.ORDER_TYPE_CANCELLATION + '\' OR Order.Custom_Type__c = \'' + ConstantsUtil.ORDER_TYPE_TERMINATION + '\') ' +
                                        'AND Order.Status = \'' + ConstantsUtil.ORDER_STATUS_DRAFT + '\'' +
                                        'AND Product2.Product_Group__c = \'' + ConstantsUtil.MYCAMPAIGN_PROD_GROUP + '\'' +
                                        'AND Product2.ProductCode IN :myCampSocialProductCodes ' +
                                        'AND Order.MigratedTermination__c = FALSE' +
                                        ')'
                                       );
    }

    global void execute(Database.BatchableContext info, List<Order> scope){
        
        Set<Id> orderIds = new Set<Id>();
        Set<Id> orderFullfilledIds = new Set<Id>();
		Set<Id> contractIds = new Set<Id>();
        
        for(Order currentOrder : scope){
            orderIds.add(currentOrder.Id);
        }
        if(!orderIds.isEmpty()){
            if(!Test.isRunningTest()) OrderUtility.activateContracts(new List<Id>(orderIds));
        }
    } 
    
    global void finish(Database.BatchableContext info){}
    
}