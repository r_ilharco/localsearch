global class SLMigrationCopyActivation_BatchSchedule implements Database.Batchable<sObject> , Database.stateful, Schedulable{
    private  String query='SELECT SBQQ_StartDatecopy__c,Quote_Line__r.SBQQ__ProductOption__c ,quoteline_list__c,   Billing_Profilecopy__c, SBQQ_Accountcopy__c,SBQQ_Opportunity2copy__c,'+
       ' SBQQ_EndDatecopy__c,SBQQ_PrimaryContactcopy__c, Quote__c,Quote__r.SBQQ__Status__c ,quote__r.SBQQ__Primary__c, SBQQ_Opportunity_Statuscopy__c,  quote__r.SBQQ__Opportunity2__r.StageName from quote_copy__c';
    private List<quote_copy__c> trials;  
    private List<quote_copy__c> contractualized;
    private integer days = 30;//days when the trial will start
    private integer batchSize = 10; // bacth size of the contract activation
    private integer offset = 10; // delay for ther contract activation
    private map <string, list<quote_copy__c>> opportunityQuoteMap;
    private map <string, list<SBQQ__Quote__c>> acceptedQuoteMap;
    public integer Contract_job_Prior_days = Integer.valueOf(Label.Contract_job_Prior_days);
    list <id> wonOpties = new list<id>();
    //errors
    public static final string GENERIC_CONTRACT_ACTIVATION_ERROR_LOG = ConstantsUtil.GENERIC_CONTRACT_ACTIVATION_ERROR_LOG;
    public static final string ACTIVATING_PROCESS_ERROR = ConstantsUtil.ACTIVATING_PROCESS_ERROR;
    //OptyStatus
    private final string OPTY_STAGENAME_CLOSED_WON=ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
    private final string  OPTY_STAGENAME_CLOSED_LOST =ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_LOST;
    // quote status
    private final string QUOTE_STATUS_ACCEPTED = ConstantsUtil.QUOTE_STATUS_ACCEPTED;   
    private final string  QUOTE_STAGENAME_REJECTED =ConstantsUtil.QUOTE_STAGENAME_REJECTED;
    private final string  QUOTE_STAGENAME_ACCEPTED = ConstantsUtil.QUOTE_STAGENAME_ACCEPTED;  
    private final string  QUOTE_STAGENAME_DENIED =ConstantsUtil.QUOTE_STAGENAME_DENIED;
    global SLMigrationCopyActivation_BatchSchedule  ( integer inputBatchSize, integer inputOffset){
      this.batchSize = inputBatchSize;
      this.offset = inputOffset;
   }
    global SLMigrationCopyActivation_BatchSchedule  (integer day, integer inputBatchSize, integer inputOffset){
     this(inputBatchSize, inputOffset);
      days=day;       
   }
    global SLMigrationCopyActivation_BatchSchedule  (String q){
      this.query=q; 
   } 
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<quote_copy__c> scope){
        trials= new List<quote_copy__c>();
        system.debug('scope : ' + scope);
        contractualized=new List<quote_copy__c>();
        list<quote_copy__c> deleteList = new list<quote_copy__c>();
        for (quote_copy__c curCopy :scope ){
             system.debug('working on : ' + curCopy);
            // collect all the copies allready activate or rejected to delete them in the end
             if (curCopy.quote__r.SBQQ__Primary__c==false || curCopy.quote__r.SBQQ__Opportunity2__r.StageName == OPTY_STAGENAME_CLOSED_LOST ||
              curCopy.quote__r.SBQQ__Opportunity2__r.StageName == OPTY_STAGENAME_CLOSED_WON || curCopy.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_DENIED  ||
              curCopy.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_REJECTED || curCopy.Quote__r.SBQQ__Status__c==QUOTE_STATUS_ACCEPTED ){
                deleteList.add(curCopy);
                continue;
            }// contract will be created the day before the activation to be elegible to the future activation batch
            if (curCopy.SBQQ_StartDatecopy__c==system.today().adddays(Contract_job_Prior_days)){
                contractualized.add(curCopy);                 
            }//collect all the start date that will start in the next days less 1, then the start date and the status "trial request" will be setted in the quoteline
            // in order to manaje the record eleigible for the TrialActivation_BatchSchedule
            /*old 
            if(curCopy.SBQQ_StartDatecopy__c==system.today().adddays(days-1)){
                trials.add(curCopy);                
            }  */          
        }
        if (!contractualized.isEmpty())
            deleteList.addAll(activateContracts(contractualized));
         /*old
        if (!trials.isEmpty())
             activateTrial(trials);*/ 
        // deleting copies allready activated or rejectes
        if (!deleteList.isEmpty())
            database.delete (deleteList, false);   
    }
     global void finish(Database.BatchableContext BC){
         if (!wonOpties.isEmpty()){
              system.debug('SLMigrationCopyActivation_BatchSchedule - creating contracts: '+ wonOpties);
            // the contract cration will be performed using the cpq api, is necessary wait the cpq finish elaborates all the 
            // closed won opportunities
            // input: batch, job name, minute fom now when the job will start, batch size
             SLMigrationMassiveContractCreation_Batch createContract=  new  SLMigrationMassiveContractCreation_Batch(wonOpties);
             createContract.offset = offset;
             createContract.batchSize = batchSize;
            system.scheduleBatch(createContract, 'MassiveContractCreation', offset, batchSize);
            //only for test execution the contract activation is sinconus 
            if (Test.isRunningTest()){
                database.executeBatch( new  SLMigrationMassiveContractCreation_Batch(wonOpties),batchSize);
                }
            }
         system.debug('SLMigrationCopyActivation_BatchSchedule  Batch Ended');
    }

     //if scheduled from apex all parameters can be set othewise it takes the default
     global void execute(SchedulableContext sc)   {       
        database.executeBatch(this,batchSize);
    }
     /*---------------------inner mhetods----------------------------------------------- */
   //old >> now this process will be handled by the normal trial activation process
   /* private void activateTrial(List<quote_copy__c> trialsQuote){
        system.debug('@@@@@@@@@@@@@@@@@@ trials ' +trialsQuote);
        list<SBQQ__QuoteLine__c>  quoteLineList = new   list<SBQQ__QuoteLine__c> ();       
        for (quote_copy__c currentCopy :trialsQuote){
            //trial is activated only if the quote is already accepted or declined and it aha some quoteline
            if ( string.isBlank(currentCopy.quoteline_list__c)||currentCopy.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_DENIED 
            ||currentCopy.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_REJECTED||currentCopy.Quote__r.SBQQ__Status__c==QUOTE_STATUS_ACCEPTED)
            continue;
            list<string> quoteLineIdList = currentCopy.quoteline_list__c.split(':');
            system.debug('@@@@@@@@@@@@@@@@@@ Quote Line Id List:  '+ quoteLineIdList );
            for (string id :quoteLineIdList){
            system.debug('@@@@@@@@@@@@@@@@@@ id ' +id);
            SBQQ__QuoteLine__c tempQuoteLine=new SBQQ__QuoteLine__c();
            tempQuoteLine.id=id;            
           // tempQuoteLine.Status__c = ConstantsUtil.QUOTELINE_STATUS_TRIAL_REQUESTED;
            //tempQuoteLine.TrialStartDate__c = System.Today() - Integer.valueOf(Label.TRIAL_PROCESS_PRIOR_DAYS); old>>> new version with process builder
            tempQuoteLine.TrialStartDate__c = System.Today().adddays(1);
            quoteLineList.add(tempQuoteLine);          
            }             
        }
         system.debug('@@@@@@@@@@@@@@@@@@ quoteLineList: '+ quoteLineList);
         if (!quoteLineList.isEmpty())
        system.debug('@@@@@@@@@@@@@@@@@@ res: '+ database.update (quoteLineList, false));  
        
    }*/

   
    private list<quote_copy__c> activateContracts(List<quote_copy__c> contractsQuote){
        system.debug('@@@@@@@@@@@@@@@@@@ Contracts ' +contractsQuote);
        list<quote_copy__c> deleteList = new list<quote_copy__c>();
        list<quote_copy__c> copyErrorList = new list<quote_copy__c>();
        opportunityQuoteMap = new map<string, list<quote_copy__c>>();
        set<opportunity> optyList = new set<opportunity>();
        set<SBQQ__Quote__c> quoteList = new  set<SBQQ__Quote__c>();
        list<id> successId = new list<id>();
        list<Sobject> upsertError = new list<Sobject>();
        for (quote_copy__c contractQuote : contractsQuote){
            //skip all the declined or  already  accepted quote
            if (contractQuote.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_DENIED ||contractQuote.Quote__r.SBQQ__Status__c==QUOTE_STAGENAME_REJECTED
                ||contractQuote.Quote__r.SBQQ__Status__c==QUOTE_STATUS_ACCEPTED)
            continue;
            if (opportunityQuoteMap.containsKey(contractQuote.SBQQ_Opportunity2copy__c))
                opportunityQuoteMap.get(contractQuote.SBQQ_Opportunity2copy__c).add(contractQuote);
            else{
               list<quote_copy__c> tempDeleteList = new list<quote_copy__c>();
               tempDeleteList.add(contractQuote);
               opportunityQuoteMap.put(contractQuote.SBQQ_Opportunity2copy__c,tempDeleteList ); 
            }           
            opportunity tempOpty= new opportunity();           
            tempOpty.Id= contractQuote.SBQQ_Opportunity2copy__c;
            tempOpty.StageName=OPTY_STAGENAME_CLOSED_WON;               
            tempOpty.SBQQ__Contracted__c=false;
            tempOpty.bulkCreation__C=true;
            optyList.add(tempOpty);
            contractQuote.Quote__r.SBQQ__Status__c= QUOTE_STATUS_ACCEPTED;
            quoteList.add(contractQuote.Quote__r);
        }
        list<Log__c > errors = new  list<Log__c > ();
        database.saveResult[] srs = database.update (new List<SBQQ__Quote__c>(quoteList), false);
       for(database.saveResult sr :srs)
         system.debug('QuoteList Errors: ' + sr.getErrors());
        database.saveResult[] saveUpd= database.update (new List<opportunity>(optyList), false);
        system.debug('opportunityQuoteMap: '+ opportunityQuoteMap);
        for (database.saveResult singleRes: saveUpd){
            if (singleRes.isSuccess()){
               deleteList.addAll(opportunityQuoteMap.get(singleRes.getId()));
               wonOpties.add(singleRes.getId());
               opportunityQuoteMap.remove(singleRes.getId());
                 system.debug('Opty saved successfully: '+ singleRes.getId());
            }else {
                 system.debug('Single save result: '+ singleRes);
                  Log__c  log = new Log__c(
                    Name = GENERIC_CONTRACT_ACTIVATION_ERROR_LOG,
                    Communication_Ok__c = false,
                    Message__c = json.serialize(singleRes.getErrors()).abbreviate(9900)
                );
                errors.add(log);
              }
            }
        if (!errors.isEmpty())
          database.insert (errors, false);
        if (!opportunityQuoteMap.isEmpty()){
                for (string key : opportunityQuoteMap.keyset()){
                    for (quote_copy__c qc : opportunityQuoteMap.get(key)) {
                        qc.Process_Error__c=ACTIVATING_PROCESS_ERROR;
                        copyErrorList.add( qc);
                    }         
                }
             upsertError.addall(copyErrorList);
         }  
    if (!UpsertError.isEmpty())
    database.update (upsertError, false);
    return deleteList;
    // old >> logic mooved out in the execute 
    //database.delete (deleteList, false);
    }   
}