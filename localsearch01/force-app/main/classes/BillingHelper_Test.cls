@isTest
public class BillingHelper_Test {
    
    @testSetup
    private static void createSettings() {
        Numbering_cs__c nc = Numbering_cs__c.getOrgDefaults();
        nc.SetupOwnerId = Userinfo.getOrganizationId();
        nc.Next_Invoice_Number__c = 400000000L;
        upsert nc;

        Customer_Number_cs__c cn = Customer_Number_cs__c.getOrgDefaults();
        cn.SetupOwnerId = Userinfo.getOrganizationId();
        cn.Next_Customer_Number__c = 200000000L;
        upsert cn;
        
        Bypass_Triggers__c th = Bypass_Triggers__c.getOrgDefaults();
        th.SetupOwnerId = Userinfo.getOrganizationId();
        th.Trigger_Name__c = 'ProduceInvoiceTemporaryCreditNoteTrigger, ProduceInvoiceTemporarySBQQSubscriptionTrigger, QuoteTrigger, QuoteLineTrigger';
        upsert th;        
    }
    
    private static testmethod void test_properties() {
        Numbering_cs__c numbering = BillingHelper.numbering;
        System.assertEquals(400000000L, numbering.Next_Invoice_Number__c);
        Customer_Number_cs__c custNumber = BillingHelper.customerNumber;
        System.assertEquals(200000000L, custNumber.Next_Customer_Number__c);
        System.assertEquals(ConstantsUtil.BILLING_PAYMENT_STATUS, string.join(BillingHelper.PaymentStatusList, ', '));
        System.assertEquals(ConstantsUtil.BILLING_CUSTOMER_RATING, string.join(BillingHelper.CustomerRatingList, ', '));
    }
    
    /*private static testmethod void test_methods() {
        System.assertEquals(ConstantsUtil.BILLING_TAX_MODE_EXEMPT, BillingHelper.getTaxModeValue('Yes'));
        System.assertEquals(ConstantsUtil.BILLING_TAX_MODE_STANDARD, BillingHelper.getTaxModeValue('No'));
        System.assertEquals(400000000L, BillingHelper.generateDocumentNumber());
        System.assertEquals(400000001L, BillingHelper.generateDocumentNumber());
        BillingHelper.undoDocumentNumber();
        System.assertEquals(400000001L, BillingHelper.numbering.Next_Invoice_Number__c);
        System.assertEquals(200000000L, BillingHelper.generateCustomerNumber());
        System.assertEquals(200000001L, BillingHelper.generateCustomerNumber());
    	System.assert(BillingHelper.getGUID().length() == 32);
        System.assertEquals(0, BillingHelper.calcRounding(0));
        System.assertEquals(-.01, BillingHelper.calcRounding(.01));
        System.assertEquals(-.02, BillingHelper.calcRounding(.02));
        System.assertEquals(.02, BillingHelper.calcRounding(.03));
        System.assertEquals(.01, BillingHelper.calcRounding(.04));
        System.assertEquals(0, BillingHelper.calcRounding(.05));
        System.assertEquals(.01, BillingHelper.calcRounding(-.01));
        System.assertEquals(.02, BillingHelper.calcRounding(-.02));
        System.assertEquals(-.02, BillingHelper.calcRounding(-.03));
        System.assertEquals(-.01, BillingHelper.calcRounding(-.04));
        System.assertEquals(0, BillingHelper.calcRounding(-.05));
        
        Invoice__c i1 = new Invoice__c(Total__c = 100, Billing_Channel__c='Print');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i1));
        Invoice__c i2 = new Invoice__c(Total__c = 10, Billing_Channel__c='Print');
        System.assertEquals(false, BillingHelper.checkMinTotalAllowed(i2));
        Invoice__c i3 = new Invoice__c(Total__c = -10, Billing_Channel__c='Print');
        System.assertEquals(false, BillingHelper.checkMinTotalAllowed(i3));
        Invoice__c i4 = new Invoice__c(Total__c = -50, Billing_Channel__c='Print');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i4));
        Invoice__c i5 = new Invoice__c(Total__c = 100, Billing_Channel__c='Mail');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i5));
        Invoice__c i6 = new Invoice__c(Total__c = 10, Billing_Channel__c='Mail');
        System.assertEquals(false, BillingHelper.checkMinTotalAllowed(i6));
        Invoice__c i7 = new Invoice__c(Total__c = -10, Billing_Channel__c='Mail');
        System.assertEquals(false, BillingHelper.checkMinTotalAllowed(i7));
        Invoice__c i8 = new Invoice__c(Total__c = -50, Billing_Channel__c='Mail');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i8));
        
        BillingHelper.setting.Min_Printable_Invoice__c = null;
        Invoice__c i9 = new Invoice__c(Total__c = 10, Billing_Channel__c='Print');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i9));
        BillingHelper.setting.Min_Billable_Invoice__c = null;
        Invoice__c i10 = new Invoice__c(Total__c = 10, Billing_Channel__c='Mail');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i10));

        BillingHelper.setting.Min_Printable_Invoice__c = 0;
        Invoice__c i11 = new Invoice__c(Total__c = 10, Billing_Channel__c='Print');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i11));
        BillingHelper.setting.Min_Billable_Invoice__c = 0;
        Invoice__c i12 = new Invoice__c(Total__c = 10, Billing_Channel__c='Mail');
        System.assertEquals(true, BillingHelper.checkMinTotalAllowed(i12));
        
        System.assertEquals('Print', BillingHelper.getFallbackChannel('Print', null));
        System.assertEquals(ConstantsUtil.BILLING_CHANNEL_PRINT, BillingHelper.getFallbackChannel(null, null));
        System.assertEquals('Print', BillingHelper.getFallbackChannel(null, 'Print'));
        System.assertEquals(ConstantsUtil.BILLING_CHANNEL_PRINT, BillingHelper.getFallbackChannel('', 'Channel1;' + ConstantsUtil.BILLING_CHANNEL_MAIL + ';' + ConstantsUtil.BILLING_CHANNEL_PRINT));
        System.assertEquals(ConstantsUtil.BILLING_CHANNEL_MAIL, BillingHelper.getFallbackChannel('', 'Channel1;' + ConstantsUtil.BILLING_CHANNEL_MAIL));
        System.assertEquals('Channel1', BillingHelper.getFallbackChannel('', 'Channel1;Channel2'));
        
        System.assertEquals('123\'456\'789', BillingHelper.formatDocNumber(123456789));
        System.assertEquals('', BillingHelper.formatDocNumber(null));
        System.assertEquals('0', BillingHelper.formatDocNumber(0));
        System.assertEquals('400\'000\'001', BillingHelper.formatDocNumber(400000001));
    }
    */
}