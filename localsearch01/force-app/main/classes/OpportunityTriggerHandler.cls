/**
 * Author	   : Esposito Antonio <antesposito@deloitte.it>
 * Date		   : 22-11-2019
 * Sprint      : 
 * Work item   : SF2 -242
 * Testclass   : Test_OpportunityTrigger
 * Package     : 
 * Description : Trigger Handler on Opportunity
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-4307 - New Customer Flag - change calculation logic:
				  setActivationDateOnAccount commented 
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-11-24
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public class OpportunityTriggerHandler implements ITriggerHandler {

    public static Boolean disableTrigger {
        get {
            if(disableTrigger != null) return disableTrigger;
            else return false;
        } 
        set {
            if(value == null) disableTrigger = false;
            else disableTrigger = value;
        }
    }
	public Boolean IsDisabled(){
        return disableTrigger;
    }

    public void BeforeInsert(List<SObject> newItems){
        OpportunityTriggerHelper.setPricebook((List<Opportunity>) newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        OpportunityTriggerHelper.checkAccountDuplicate((List<Opportunity>) newItems.Values());
       // OpportunityTriggerHelper.setActivationDateOnAccount((List<Opportunity>) newItems.values(),(Map<Id, Opportunity>)oldItems);
        OpportunityTriggerHelper.startDateValidation((List<Opportunity>) newItems.values(),(Map<Id, Opportunity>)oldItems);
    }
    public void BeforeDelete(Map<Id, SObject> oldItems){}

    public void AfterInsert(Map<Id, SObject> newItems){
        OpportunityTriggerHelper.checkAccountDuplicate((List<Opportunity>) newItems.Values());
        OpportunityTriggerHelper.checkAccountTerritory((Map<Id, Opportunity>)newItems);
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}
    public void AfterDelete(Map<Id, SObject> oldItems){}
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}