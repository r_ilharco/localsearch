/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author	   : Gennaro Casola <gcasola@deloitte.it>
* Date		   : 04-08-2019
* Sprint      : 1
* Work item   : 25
* Testclass   :
* Package     : 
* Description : Trigger Dispatcher
* Changelog   : 
*/

public class TriggerDispatcher{
    /*Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
    This method will fire the appropriate methods on the handler depending on the trigger context.*/
    
    public static void Run(ITriggerHandler handler, String triggerName){
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.IsDisabled()){
            return;
        }
        
        Run(handler);

        /*List<Profile> currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1];
        if(!currentUserProfile.isEmpty()){
            List<TriggerSetting__mdt> triggerSettingMDT = [SELECT 	Id,
                                                     				DeveloperName,
                                                     				IsActive__c,
                                                     				DisabledOnlyForSysAdmin__c
                                                 			FROM 	TriggerSetting__mdt 
                                                 			WHERE 	DeveloperName = :triggerName LIMIT 1];
            
            if(!triggerSettingMDT.isEmpty()){
                if(!triggerSettingMDT[0].IsActive__c) return;
                else if(triggerSettingMDT[0].DisabledOnlyForSysAdmin__c && currentUserProfile[0].Name.equalsIgnoreCase(ConstantsUtil.SysAdmin)) return;
                else Run(handler);
            }
            else Run(handler);
        }
        else Run(handler);*/
    }
    
    public static void Run(ITriggerHandler handler){
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.IsDisabled()){
            return;
        }
        //Before trigger logic
        if (Trigger.IsBefore){
            if(Trigger.IsInsert){
                handler.BeforeInsert(Trigger.new);
            }
            if(Trigger.IsUpdate){
                handler.BeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }
            if(Trigger.IsDelete){
                handler.BeforeDelete(Trigger.oldMap);
            }
        }
        //After trigger logic
        if(Trigger.IsAfter){
            if(Trigger.IsInsert){
                handler.AfterInsert(Trigger.newMap);
            }
            if(Trigger.IsUpdate){
                handler.AfterUpdate(Trigger.newMap, Trigger.oldMap);
            }
            if(Trigger.IsDelete){
                handler.AfterDelete(Trigger.oldMap);
            }
            if(Trigger.isUndelete){
                handler.AfterUndelete(Trigger.oldMap);
            }
        }
    }
}