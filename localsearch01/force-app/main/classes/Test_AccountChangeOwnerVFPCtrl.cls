@isTest
public class Test_AccountChangeOwnerVFPCtrl {
    
    @testSetup
    static void setup() {
        UserTriggerHandler.disableTrigger=true;
        Test_DataFactory.insertFutureUsers();
        UserTriggerHandler.disableTrigger=false;
        List<Account> accs = Test_DataFactory.generateAccounts('Test', 1, false);
        accs[0].Assigned_Territory__c = 'Territory 2.01';
        insert accs;
    }
    
    @isTest 
    static void testAccountChangeOwnerVFPController(){
        List<Account> accs =  [SELECT Id FROM Account];
        List<String> accountIds = new List<String>();
        for(Account a : accs){
            accountIds.add(a.Id);
        }
        User uSMA = [select id from User where code__c =: ConstantsUtil.ROLE_IN_TERRITORY_SMA and isActive = true limit 1];
        User uSDI = [select id from User where code__c =: ConstantsUtil.ROLE_IN_TERRITORY_SDI and isActive = true limit 1];
        
        Test.StartTest(); 
        System.runAs(uSMA){
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(accs);
            AccountChangeOwnerVFPController  obj = new AccountChangeOwnerVFPController(sc);		
            AccountChangeOwnerVFPController.hasUserGrantMassive(accountIds);
        }
         System.runAs(uSDI){
            AccountChangeOwnerVFPController.hasUserGrantMassive(accountIds);
        }
        AccountChangeOwnerVFPController.hasUserGrantMassive(accountIds);
		Test.StopTest();
    }
	
}