/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Gennaro Casola <gcasola@deloitte.it>
 * Date        : 07-26-2019
 * Sprint      : 6
 * User Story  : SF2-270 - Quote and Contract Template 
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class SEL_Contact{
    public static Map<Id, Contact> getContactsById(Set<Id> ids)
    {
        return new Map<Id, Contact>([
            	SELECT Id, AccountId, LastName, FirstName, Salutation, MiddleName, 
            	Suffix, Name, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, 
            	OtherLatitude, OtherLongitude, OtherGeocodeAccuracy, OtherAddress, MailingStreet, MailingCity, 
            	MailingState, MailingPostalCode, MailingCountry, MailingLatitude, MailingLongitude, MailingGeocodeAccuracy, MailingAddress, 
            	Phone, Fax, MobilePhone, HomePhone, OtherPhone, AssistantPhone, ReportsToId, Email, Title, Department, AssistantName, LeadSource, 
            	Birthdate, Description, GoldenRecordID__c, Language__c, MailingAddressValidationDate__c, 
            	MailingAddressValidationStatus__c, PO_BoxCity__c, PO_BoxZip_PostalCode__c, PO_Box__c, Personalnummer__c, Position__c, Primary__c, 
            	RD__c, RL__c, Regionaldirektor__c, Regionalleiter__c, Title__c, ZIPExtension__c, c_o__c, Contact_Personal_Function__c 
            	FROM Contact
            	WHERE Id IN :ids
               ]);
    }
}