/**
 * Author	   : Mazzarella Mara <mamazzarella@deloitte.it>
 * Date		   : 05-29-2019
 * Sprint      : Sprint 2
 * Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
 * 				 US_75 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (downgrade) to replace a product with another one
 * Package     : 
 * Description : Test Class for the Controller to manage the termination of related to upgraded/downgraded contract subscriptions 
 * Changelog   :
 * 				#1 SF2-190 - Immediate downgrade through approval process - gcasola 06-28-2019
 */

@isTest
public class CheckSubscriptionControllerTest {
    static void insertBypassFlowNames(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Update Contract Company Sign Info,Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management,Quote Line Management';
        insert byPassFlow;
    }
    
    @testSetup 
    private static void setup(){
        insertBypassFlowNames();
        test_datafactory.insertFutureUsers();
        Map<String, Id> permissionSetsMap = Test_DataFactory.getPermissionSetsMap();
        list<user> users = [select id,name,code__c from user where Code__c = 'SYSADM'];
        user testuser = users[0];
       // system.runas(testuser){
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        PermissionSetAssignment psain = new PermissionSetAssignment();
        psain.PermissionSetId = permissionSetsMap.get('Samba_Migration');
        psain.AssigneeId = testuser.Id;
        psas.add(psain);
     //   insert psas;
       // }
		//START SF2-190 - Immediate downgrade through approval process - gcasola 06-28-2019
        Account acct = Test_DataFactory.createAccounts('AccountTest01', 1)[0];
        insert acct;
        
        Opportunity oppt = Test_DataFactory.createOpportunities('OppTest','Qualification',acct.Id,1)[0];
        oppt.Pricebook2Id = Test.getStandardPricebookId();
        insert oppt;
        
        List<Place__c> places = Test_DataFactory.createPlaces('PName',2);
        places[0].Account__c = acct.id;
        places[1].Account__c = acct.id;
        
        insert places;
        
        Swisslist_CPQ.config();
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        List<Contract> contracts = Test_DataFactory.getContracts(acct, ConstantsUtil.CONTRACT_STATUS_DRAFT, 2);
        contracts[0].SBQQ__Evergreen__c = false;
        contracts[0].In_Termination__c = false;
        contracts[0].EndDate = Date.today().addMonths(12);
        contracts[0].SBQQ__Opportunity__c = oppt.Id;
        
        contracts[1].SBQQ__Evergreen__c = true;
        contracts[1].In_Termination__c = false;
        contracts[1].SBQQ__Opportunity__c = oppt.Id;

        insert contracts;
        
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        
        SBQQ__Subscription__c subStandard = Test_DataFactory.getSubscriptions(acct, places[1], contracts[0], new List<Product2>{products.get(productIdByCode.get('SLS001'))}, 1)[0];
        subStandard.Subscription_Term__c = products.get(productIdByCode.get('SLS001')).Subscription_Term__c;
        subStandard.SBQQ__Quantity__c = 1;
        subStandard.In_Termination__c = false;
        subStandard.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subStandard.Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        
        SBQQ__Subscription__c subStandard2 = Test_DataFactory.getSubscriptions(acct, places[1], contracts[1], new List<Product2>{products.get(productIdByCode.get('SLS001'))}, 1)[0];
        subStandard2.Subscription_Term__c = products.get(productIdByCode.get('SLS001')).Subscription_Term__c;
        subStandard2.SBQQ__Quantity__c = 1;
        subStandard2.In_Termination__c = false;
        subStandard2.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subStandard2.Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        
        subs.add(subStandard);
        subs.add(subStandard2);
        
        SubscriptionTriggerHandler.disableTrigger = true;
        insert subs;
        SubscriptionTriggerHandler.disableTrigger = false;

        subs.clear();
        
        SBQQ__Subscription__c subA1 = Test_DataFactory.getSubscriptions(acct, null, contracts[0], new List<Product2>{products.get(productIdByCode.get('ONAP001'))}, 1)[0];
        subA1.Subscription_Term__c = products.get(productIdByCode.get('ONAP001')).Subscription_Term__c;
        subA1.SBQQ__Quantity__c = 1;
        subA1.In_Termination__c = false;
        subA1.SBQQ__RequiredById__c = subStandard.Id;
        subA1.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subA1.Status__c = 'Active';
        
        subs.add(subA1);
        
        // SBQQ__Subscription__c subA2 = Test_DataFactory.getSubscriptions(acct, null, cont, new List<Product2>{products.get(productIdByCode.get('ORER001'))}, 1)[0];
        // subA2.Subscription_Term__c = products.get(productIdByCode.get('ORER001')).Subscription_Term__c;
        // subA2.SBQQ__Quantity__c = 1;
        // subA2.In_Termination__c = false;
        // subA2.SBQQ__RequiredById__c = subStandard.Id;
        // subA2.Status__c = 'Active';
        
        // subs.add(subA2);
        
        // SBQQ__Subscription__c subA3 = Test_DataFactory.getSubscriptions(acct, null, cont, new List<Product2>{products.get(productIdByCode.get('OURL001'))}, 1)[0];
        // subA3.Subscription_Term__c = products.get(productIdByCode.get('OURL001')).Subscription_Term__c;
        // subA3.SBQQ__Quantity__c = 1;
        // subA3.In_Termination__c = false;
        // subA3.SBQQ__RequiredById__c = subStandard.Id;
        // subA3.Status__c = 'Active';
        
        // subs.add(subA3);
        
        // SBQQ__Subscription__c subA4 = Test_DataFactory.getSubscriptions(acct, null, cont, new List<Product2>{products.get(productIdByCode.get('OREV001'))}, 1)[0];
        // subA4.Subscription_Term__c = products.get(productIdByCode.get('OREV001')).Subscription_Term__c;
        // subA4.SBQQ__Quantity__c = 1;
        // subA4.In_Termination__c = false;
        // subA4.SBQQ__RequiredById__c = subStandard.Id;
        // subA4.Status__c = 'Active';
        
        // subs.add(subA4);
        
        // SBQQ__Subscription__c subA5 = Test_DataFactory.getSubscriptions(acct, null, cont, new List<Product2>{products.get(productIdByCode.get('OPMBASIC001'))}, 1)[0];
        // subA5.Subscription_Term__c = products.get(productIdByCode.get('OPMBASIC001')).Subscription_Term__c;
        // subA5.SBQQ__Quantity__c = 1;
        // subA5.In_Termination__c = false;
        // subA5.SBQQ__RequiredById__c = subStandard.Id;
        // subA5.Status__c = 'Active';
        
        // subs.add(subA5);
        SubscriptionTriggerHandler.disableTrigger = true;
        insert subs;
        SubscriptionTriggerHandler.disableTrigger = false;
        
        contracts[0].Status = 'Active';
        contracts[1].Status = 'Active';
        PermissionSetAssignment PSA = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Samba_migration'][0];
        string UserId = PSA.AssigneeId;
        user u = [select id from user where id=: UserId][0];
        system.runas(u){
        update contracts;
        }
        Opportunity oppt2 = Test_DataFactory.createOpportunities('OppTestDowngrade','Qualification',acct.Id,1)[0];
        oppt2.Pricebook2Id = Test.getStandardPricebookId();
        oppt2.UpgradeDowngrade_Contract__c = contracts[0].Id;
        insert oppt2;
        
        SBQQ__Quote__c quoteDowngrade = Test_DataFactory.createQuotes('Draft', oppt2, 1)[0];
        quoteDowngrade.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_DOWNGRADE;
        quoteDowngrade.Upgrade_Downgrade_Contract__c = contracts[0].Id;
        insert quoteDowngrade;
        
        quoteDowngrade.SBQQ__StartDate__c = Date.today().addDays(1);
        update quoteDowngrade;
        
        SBQQ.TriggerControl.disable();
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.createQuoteLines(quoteDowngrade, new List<Product2>{products.get(productIdByCode.get('SLT001'))})[0];
        quoteLine.Place__c = places[1].Id;
        insert quoteLine;
        SBQQ.TriggerControl.enable();
        
        //START TestData Set - mamazzarella - 05-29-2019
        // Account acct = Test_DataFactory.createAccounts('AccountTest', 1)[0];
        // insert acct;
        
        // List<Place__c> places = Test_DataFactory.createPlaces('PlaceName',1);
        // places[0].Account__c = acct.id;
        
        // insert places;
        
        // List<Product2> products = Test_DataFactory.createProducts('ProductName', 2);
        // products[0].Priority__c = '20';
        // products[1].Priority__c = '20';
        
        // insert products;
        
       	// List<Opportunity> oppts = Test_DataFactory.createOpportunities('OppTest','Qualification',acct.Id,2);
        // insert oppts;
        
        // List<Contract>cont= Test_DataFactory.getContracts(acct,'Draft',2);
        // insert cont;
        // cont[0].Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        // cont[0].SBQQ__Evergreen__c = false;
        // cont[0].EndDate = Date.today().addDays(29);
        // update cont[0];
        
        // SBQQ__Quote__c quoteDraft = Test_DataFactory.createQuotes('Draft', oppts[0], 1)[0];
        // quoteDraft.Upgrade_Downgrade_Contract__c = cont[1].id;
        // insert quoteDraft;
        
        // SBQQ__QuoteLine__c quoteLineHIGH = Test_DataFactory.createQuoteLines(quoteDraft, new List<Product2>{products[1]})[0];
        // quoteLineHIGH.Place__c = places[0].Id;
        // insert quoteLineHIGH;
        
        // SBQQ__Quote__c quoteDraft2 = Test_DataFactory.createQuotes('Draft', oppts[1], 1)[0];
        // quoteDraft2.Upgrade_Downgrade_Contract__c = cont[1].id;
        // quoteDraft.SBQQ__Type__c = Constantsutil.QUOTE_TYPE_DOWNGRADE;
        // insert quoteDraft2;
            
        // List<SBQQ__Subscription__c> subs = Test_DataFactory.getSubscriptions(acct,places[0], cont[1], products,2);
        // insert subs;
        // subs[0].Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
		// subs[0].place__c = places[0].Id;		
        // update subs[0];
        
        // subs[1].SBQQ__RequiredById__c = subs[0].id;
        // update subs[1];
        //END TestData Set - mamazzarella - 05-29-2019
        
        //END SF2-190 - Immediate downgrade through approval process - gcasola 06-28-2019
        
        
    }
    
    @isTest
    public static void ShouldgetSubscriptionByContract(){    
        id quoteId = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft' ][0].id;
													  
         Test.startTest();
        CheckSubscriptionController.getSubscriptionByContract(quoteId);
          Test.stopTest();
													   
    }
    
    @isTest
    public static void ShouldHaveQuoteLines(){    
        id quoteId = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'][0].id;
													  
        Test.startTest();
        CheckSubscriptionController.checkQuoteLines(quoteId);
        checkSubscriptionController.getReplaceableSubscriptions(quoteid);
        Test.stopTest();
													   
    }
   
    @isTest
    public static void ShouldgetSubscriptionByContractWhitoutQuoteLine(){    
        id quoteId = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'][0].id;
													  
        Test.startTest();
        CheckSubscriptionController.getSubscriptionByContract(quoteId);
        Test.stopTest();
													   
    }
    
    @isTest
    public static void ShouldgetSubscriptionByContractWhitoutSubscriptionChild(){    
													  
        Test.startTest();
        id quoteId = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'][0].id;
		SBQQ__Subscription__c sub = [select id, SBQQ__RequiredById__c from SBQQ__Subscription__c where SBQQ__RequiredById__c != null ][0];   
        sub.SBQQ__RequiredById__c = null;
        update sub;
        CheckSubscriptionController.getSubscriptionByContract(quoteId);
        Test.stopTest();
													   
    }
    
    @isTest
    public static void ShouldselectToterminate(){     
        Test.startTest();
        id quoteId = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'][0].id;
													  
        List<Id>subIds = new List<Id>();
        
        for(SBQQ__Subscription__c s : [select id from SBQQ__Subscription__c ]){
            subIds.add(s.Id);
        } 
        CheckSubscriptionController.selectToterminate(subIds,quoteId);
        Test.stopTest();
													   
    }
    
    @isTest
    public static void ShouldselectToterminate_upgrade(){     
														  
        Test.startTest();
        SBQQ__Quote__c quote = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'];
        Id quoteId = quote.id;
        
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        List<Product2> products = [SELECT Family FROM Product2];
        for(Product2 prod : products)
            prod.Family = ConstantsUtil.PRODUCT2_FAMILY_DISUSED;
        
        update quote;
        update products;
        
        List<Id>subIds = new List<Id>();
        
        for(SBQQ__Subscription__c s : [select id from SBQQ__Subscription__c ]){
            subIds.add(s.Id);
        } 
        CheckSubscriptionController.selectToterminate(subIds,quoteId);
        Test.stopTest();
													   
    }
    
    @isTest
    public static void ShouldselectToterminate_upgrade_disused(){     
														  
        Test.startTest();
        SBQQ__Quote__c quote = [select id from SBQQ__Quote__c where SBQQ__Status__c = 'Draft'];
        Id quoteId = quote.id;
        
        quote.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_UPGRADE;
        List<Product2> products = [SELECT Family FROM Product2];
        for(Product2 prod : products)
            prod.Family = ConstantsUtil.PRODUCT2_FAMILY_DISUSED;
        
        List<SBQQ__Subscription__c> subscriptions = [SELECT Place__c FROM SBQQ__Subscription__c];
        for(SBQQ__Subscription__c sub : subscriptions)
            sub.Place__c = NULL;
        
        update quote;
        update products;
        update subscriptions;
        
        List<Id>subIds = new List<Id>();
        
        for(SBQQ__Subscription__c s : [select id from SBQQ__Subscription__c ]){
            subIds.add(s.Id);
        } 
        CheckSubscriptionController.selectToterminate(subIds,quoteId);
        Test.stopTest();
													   
    }
    
    //START SF2-190 - Immediate downgrade through approval process - gcasola 06-28-2019
    
    //END SF2-190 - Immediate downgrade through approval process - gcasola 06-28-2019
}
