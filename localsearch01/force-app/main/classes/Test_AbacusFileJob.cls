@isTest
public class Test_AbacusFileJob {
    
    private static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
    @TestSetup
    static void setup(){
        
        List<Account> accts = Test_DataFactory.createAccounts('AcctTest',1);
        for(Account acct: accts)
            acct.Customer_Number__c = '200002783';
        insert accts;
        
        List<Invoice__c> invcs = Test_DataFactory.createInvoices(10, ConstantsUtil.INVOICE_STATUS_COLLECTED, ConstantsUtil.INVOICE_PROCESS_MODE_STANDARD0, accts);
        List<Invoice_Order__c> invOrds = new List<Invoice_Order__c>();
        List<Invoice_Item__c> invItms = new List<Invoice_Item__c>();
        for(Invoice__c inv : invcs){
            inv.Booking_Status__c = ConstantsUtil.ABACUS_STATUS_DRAFT;
            inv.Status__c=ConstantsUtil.INVOICE_STATUS_COLLECTED;
            inv.Invoice_Code__c = ConstantsUtil.BILLING_DOC_CODE_FAK;
            inv.Rounding__c = 10;
        }
        insert invcs;
        
        for(Invoice__c inv : invcs)
            invOrds.addAll(Test_DataFactory.createInvoiceOrders(5, inv.Id));
        insert invOrds;
        
        for(Invoice_Order__c invOrd: invOrds)
            invItms.addAll(Test_DataFactory.createInvoiceItems(3, invOrd.Id));
        
        for(Invoice_Item__c invItm: invItms){
            invItm.Tax__c=10;
            invItm.Total__c = 10;
            invItm.Tax_Code__c= 'Standard';
            invItm.Accrual_From__c =date.today().adddays(-181);
            invItm.Accrual_To__c = date.today().adddays(10);
            invItm.Amount__c = 10;
            invItm.Description__c = generateRandomString(100);
        }
            
        insert invItms;
        
        Vat_Schedule__c vs = new Vat_Schedule__c(Tax_Code__c='Standard', Start_Date__c=date.today().adddays(-182) , End_Date__c= date.today().adddays(11), Value__c = 0.12);
        insert vs;
		
        
    }
    
    @isTest 
    static void testAbacusFileJob(){
        Test.startTest();
        AbacusFileJob afj = new AbacusFileJob();
        Id batchId = Database.executeBatch(afj);
        Test.stopTest();
    }

}