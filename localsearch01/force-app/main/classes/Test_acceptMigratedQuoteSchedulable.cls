@isTest
public class Test_acceptMigratedQuoteSchedulable {
     public static String endpoint = 'www.endpointtest.it/'; 
    static void insertBypassFlowNames(){
            ByPassFlow__c byPassTrigger = new ByPassFlow__c();
            byPassTrigger.Name = Userinfo.getProfileId();
            byPassTrigger.FlowNames__c = 'Opportunity Management,Send Owner Notification,Opportunity Handler,Quote Management, Quote Line Management';
            insert byPassTrigger;
		}
    
	@TestSetup
    public static void setup(){
        QuoteLineTriggerHandler.disableTrigger = true;
        insertBypassFlowNames();
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        Opportunity oppo =  Test_DataFactory.generateOpportunity('Test Opportunity', acc[0].Id);
        oppo.Pricebook2Id = Test.getStandardPricebookId();
        insert oppo;
        List<Contact> cont = Test_DataFactory.generateContactsForAccount(acc[0].Id, 'Last Name Test', 1);
        insert cont;
        SBQQ__Quote__c quote1 = Test_DataFactory.generateQuote(oppo.Id, acc[0].Id, cont[0].Id, null);
        quote1.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_RENEWAL;
        quote1.SBQQ__CustomerDiscount__c = 5 ;
        insert quote1;
        Product2 product = generateProduct();
        Product2 product1 = generateProduct1();
        insert product;
        insert product1;
        SBQQ__ProductOption__c productOption = generateProductOption(product, product1);
        insert productOption;
        SBQQ__QuoteLine__c quoteLine = Test_DataFactory.generateQuoteLine(quote1.Id, product.Id, Date.today().addDays(1), null, null, null, 'Annual');
        quoteLine.Integration_List_Price__c = 15;
        quoteLine.Migration_Net_Price__c = 0;
        quoteLine.SBQQ__Discount__c = 5;
        insert quoteLine;
        //SBQQ__QuoteLine__c quoteLine1 = Test_DataFactory.generateQuoteLine(quote1.Id, product1.Id, Date.today().addDays(1), null, null, quoteLine.Id, 'Annual');
        //insert quoteLine1;
        QuoteLineTriggerHandler.disableTrigger = false;

    }
    
    
    @isTest
    public static void test_acceptMigratedQuote_Schedulable(){
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken', Endpoint__c = endpoint);    
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        SchedulableContext sc = null;
        acceptMigratedQuote_Schedulable tsc = new acceptMigratedQuote_Schedulable();
        tsc.execute(sc);
        Test.stopTest(); 
    }
    
     @isTest
    public static void test_acceptMigratedQuote_SchedulableExecute(){
         List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
											Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                			Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            Database.BatchableContext db = null;
            List<String> x = new List<String>();
            acceptMigratedQuote daoeb = new acceptMigratedQuote(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X'}); 
            daoeb.execute(db, new list<SBQQ__Quote__c>{quote});
           
        Test.stopTest();
    }
    
    @isTest
    public static void test_acceptMigratedQuote_SchedulableExecute_1(){
         List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
											Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                			Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Contract_Ref_Id__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        List<Product2> products = [SELECT Id, Product_Group__c FROM Product2];
        
        for(Product2 product : products)
        {
            product.Family = 'Advertising';
            product.Product_Group__c = ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT;
        }
        
        update products;
        
        for(SBQQ__QuoteLine__c ql : quoteLines)
        {
            if(quoteLines[0].Id != ql.Id)
            	ql.SBQQ__RequiredBy__c = quoteLines[0].Id;
            ql.Subscription_Term__c = NULL;
            ql.Contract_Ref_Id__c = '2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222';
        }

        QuoteLineTriggerHandler.disableTrigger = true;
        update quoteLines;
        QuoteLineTriggerHandler.disableTrigger = false;
        
        quote.Contract_Signed_Date__c = Date.today().addMonths(36);
        update quote;
        System.debug('quote.Contract_Signed_Date__c: ' + quote.Contract_Signed_Date__c);
        Account a = [SELECT Activation_Date__c FROM Account WHERE Id = :quote.SBQQ__Account__c];
        a.Activation_Date__c = Date.today();
        update a;
        System.debug('a.Activation_Date__c: ' + a.Activation_Date__c);
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            Database.BatchableContext db = null;
            List<String> x = new List<String>();
            acceptMigratedQuote daoeb = new acceptMigratedQuote(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X'}); 
            daoeb.execute(db, new list<SBQQ__Quote__c>{quote});
        	daoeb.start(null);
        	daoeb.execute(null);
           
        Test.stopTest();
    }
    
    @isTest
    public static void test_acceptMigratedQuote_SchedulableExecute_2(){
         List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
											Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                			Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Contract_Ref_Id__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        List<Product2> products = [SELECT Id, Product_Group__c FROM Product2];
        
        for(Product2 product : products)
        {
            product.Family = 'Advertising';
            product.Product_Group__c = ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT;
        }
        
        update products;
        
        for(SBQQ__QuoteLine__c ql : quoteLines)
        {
            if(quoteLines[0].Id != ql.Id)
            	ql.SBQQ__RequiredBy__c = quoteLines[0].Id;
            ql.Contract_Ref_Id__c = '2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222';
        }
        
        update quoteLines;
        
        quote.Contract_Signed_Date__c = Date.today().addMonths(36);
        update quote;
        System.debug('quote.Contract_Signed_Date__c: ' + quote.Contract_Signed_Date__c);
        Account a = [SELECT Activation_Date__c FROM Account WHERE Id = :quote.SBQQ__Account__c];
        a.Activation_Date__c = Date.today();
        update a;
        System.debug('a.Activation_Date__c: ' + a.Activation_Date__c);
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            Database.BatchableContext db = null;
            List<String> x = new List<String>();
            acceptMigratedQuote daoeb = new acceptMigratedQuote(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X'}); 
            daoeb.execute(db, new list<SBQQ__Quote__c>{quote});
           
        Test.stopTest();
    }
    
    @isTest
    public static void test_acceptMigratedQuote_SchedulableExecute_3(){
         List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        SBQQ__Quote__c quote = [SELECT Id,SBQQ__Account__c, SBQQ__Opportunity2__c, Billing_Profile__c, Allocation_not_empty__c, Billing_Channel__c, 
											Filtered_Primary_Contact__c, SBQQ__Status__c, SBQQ__PriceBook__c, SBQQ__Opportunity2__r.Pricebook2Id, SBQQ__Account__r.Activation_Date__c,
                                			Contract_Signed_Date__c, SBQQ__PrimaryContact__c FROM SBQQ__Quote__c LIMIT 1];
        
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Contract_Ref_Id__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id];
        List<Product2> products = [SELECT Id, Product_Group__c FROM Product2];
        
        for(Product2 product : products)
        {
            product.One_time_Fee__c = true;
            product.Family = 'Advertising';
            product.Product_Group__c = ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT;
        }
        
        update products;
        
        for(SBQQ__QuoteLine__c ql : quoteLines)
        {
            ql.Integration_List_Price__c = 20;
            ql.SBQQ__Quantity__c = 10;
            ql.SBQQ__NetPrice__c = 3;
            ql.Migration_Net_Price__c = NULL;
            if(quoteLines[0].Id != ql.Id)
            	ql.SBQQ__RequiredBy__c = quoteLines[0].Id;
            ql.Contract_Ref_Id__c = '2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222';
        }
        
        QuoteLineTriggerHandler.disableTrigger = true;
        update quoteLines;
        QuoteLineTriggerHandler.disableTrigger = false;

        quote.Contract_Signed_Date__c = Date.today().addMonths(36);
        update quote;
        System.debug('quote.Contract_Signed_Date__c: ' + quote.Contract_Signed_Date__c);
        Account a = [SELECT Activation_Date__c FROM Account WHERE Id = :quote.SBQQ__Account__c];
        a.Activation_Date__c = Date.today();
        update a;
        System.debug('a.Activation_Date__c: ' + a.Activation_Date__c);
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
            Database.BatchableContext db = null;
            List<String> x = new List<String>();
            acceptMigratedQuote daoeb = new acceptMigratedQuote(new List<String>{'0','1','a','A','b','B','c','C','d','D','x','X'}); 
            daoeb.execute(db, new list<SBQQ__Quote__c>{quote});
           
        Test.stopTest();
    }
    
    public static Product2 generateProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Tool & Services');
        fieldApiNameToValueProduct.put('Name','MyCOCKPIT Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','BCB');
        fieldApiNameToValueProduct.put('KSTCode__c','8932');
        fieldApiNameToValueProduct.put('KTRCode__c','1202010005');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyCockpit Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyCockpit');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','SBSTANDARD001');
        //MCOBASIC001
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Product2 generateProduct1(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family','Presence');
        fieldApiNameToValueProduct.put('Name','MyWEBSITE Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','localBANNER');
        //MYWEBSITEBASIC001
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static SBQQ__ProductOption__c generateProductOption(Product2 productMaster, Product2 productChild){
            SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c();
        	productOption.SBQQ__Feature__c = null;
            productOption.SBQQ__OptionalSKU__c = productChild.Id;
            productOption.SBQQ__Number__c = 1;
            productOption.SBQQ__Type__c = null;
            productOption.SBQQ__UnitPrice__c = null;
            productOption.SBQQ__Quantity__c = null;
            productOption.SBQQ__MinQuantity__c = 1;
        	productOption.SBQQ__Bundled__c = true;
            productOption.SBQQ__ConfiguredSKU__c = productMaster.Id;
            return productOption;
    }
    
}