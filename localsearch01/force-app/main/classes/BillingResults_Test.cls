@IsTest
public class BillingResults_Test {

     @testSetup
    static void setupData() {
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Update Contract Company Sign Info';
		insert byPassFlow;
        Mashery_Setting__c setting = new Mashery_Setting__c();
        setting.Client_Id__c = 'qmwyz3avgvkwq45qjq3recaqamdythnk';
        setting.Client_Secret__c = 'CFHCQrApkEjDy5zmyGX7gKDQ2HruNnfQ';
        setting.Expires__c = 1;
        setting.Token_Type__c = 'bearer';
        setting.Grant_Type__c = 'client_credentials';
        setting.Endpoint__c = 'https://apicloud-preprod.localsearch.tech/';
        setting.Token__c = 'e5brpmtgup9wx3cqsq7gwrd2';
        insert setting;
        List<Account> accounts = new List<Account>();
        Account ac = new Account();
        ac.Name = 'Test 1';
        ac.AddressValidated__c = 'validated_address';
        ac.BillingCity = 'Zürich';
        ac.BillingCountry = 'Switzerland';
        ac.BillingPostalCode = '8005';
        ac.BillingStreet = 'Förrlibuckstrasse 62';
        ac.Customer_Number__c = 'B1150452-B152-4660-897F-E4D150B9803D';
        ac.LegalEntity__c = 'AG';
        ac.P_O_Box_City__c = 'Buchberg';
        ac.P_O_Box_Zip_Postal_Code__c = '8454';
        ac.PreferredLanguage__c = 'German';
        ac.GoldenRecordID__c='GR-001';
        accounts.add(ac);
        Account ac1 = new Account();
        ac1.Name = 'Test 2';
        ac1.AddressValidated__c = 'validated_address';
        ac1.BillingCity = 'Zürich';
        ac1.BillingCountry = 'Switzerland';
        ac1.BillingPostalCode = '8005';
        ac1.BillingStreet = 'Förrlibuckstrasse 62';
        ac1.Customer_Number__c = '4C806A6B-2862-465E-8781-872C9D271AB3';
        ac1.LegalEntity__c = 'AG';
        ac1.P_O_Box_City__c = 'Buchberg';
        ac1.P_O_Box_Zip_Postal_Code__c = '8454';
        ac1.PreferredLanguage__c = 'German';
        ac1.GoldenRecordID__c='GR-002';
        ac1.ClientRating__c='A-Payments settled';
        ac1.Amount_At_Risk__c=0;
        accounts.add(ac1);
        Account ac2 = new Account();
        ac2.Name = 'Test 3';
        ac2.AddressValidated__c = 'validated_address';
        ac2.BillingCity = 'Zürich';
        ac2.BillingCountry = 'Switzerland';
        ac2.BillingPostalCode = '8005';
        ac2.BillingStreet = 'Förrlibuckstrasse 62';
        ac2.Customer_Number__c = '9B80B84D-2EAB-4003-BA91-723DA772D758';
        ac2.LegalEntity__c = 'AG';
        ac2.P_O_Box_City__c = 'Buchberg';
        ac2.P_O_Box_Zip_Postal_Code__c = '8454';
        ac2.PreferredLanguage__c = 'German';
        ac2.GoldenRecordID__c='GR-003';
        ac2.ClientRating__c='C-Pre-legal process';
        accounts.add(ac2);
        insert accounts;  
        /*
        List<Account> accounts = new List<Account>();
    	accounts.add(new Account(Name='Test 1', BillingStreet='Cape Cannaveral Blvd, 10', BillingState='TX', BillingCity='Texas', BillingPostalCode = '10001',
             BillingCountry='US', GoldenRecordID__c='GR-001', Type='Kunde', Customer_number__c = 'B1150452-B152-4660-897F-E4D150B9803D'));
    	accounts.add(new Account(Name='Test 2', BillingStreet='Cape Cannaveral Blvd, 10', BillingState='TX', BillingCity='Texas', BillingPostalCode = '10201',
             BillingCountry='US', GoldenRecordID__c='GR-002', Type='Kunde', Customer_number__c = '4C806A6B-2862-465E-8781-872C9D271AB3', ClientRating__c='A', Amount_At_Risk__c=0));
    	accounts.add(new Account(Name='Test 3', BillingStreet='Cape Cannaveral Blvd, 10', BillingState='TX', BillingCity='Texas', BillingPostalCode = '10501',
             BillingCountry='US', GoldenRecordID__c='GR-003', Type='Kunde', Customer_number__c = '9B80B84D-2EAB-4003-BA91-723DA772D758', ClientRating__c='C'));
		insert accounts;        

        // Contact, Billing Profile, Invoice_Order, Contract
        // inv.Invoice_Orders__r[0].Contract__c
        // acc.Billing_Profiles__r[0].Billing_Contact__c
        
        List<Contact> contacts = new List<Contact>();
        for(integer i = 0; i < 3; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
            contacts.add(new Contact(FirstName=data.first_name, LastName=data.last_name, email=data.email, GoldenRecordID__c='C-GR-' + i, 
                   MailingCity=data.city[1], MailingCountry=data.country, MailingPostalCode=data.postal_code[1], MailingState=data.state[1], 
                   MailingStreet=data.street[1] + ' ' + data.street_number[1], Language__c='Italian', AccountId = accounts[i].Id));
        }
        insert contacts;
        
        List<Billing_Profile__c> billingProfiles = new List<Billing_Profile__c>();
        for(integer i = 0; i < 3; i++) {
            TestUtils.Mock data = TestUtils.Mocks[i];
            billingProfiles.add(new Billing_Profile__c (Name='BP-Print-Test-' + i, Billing_City__c=data.city[1], Billing_Contact__c = contacts[i].Id,
                    Billing_Country__c = data.country, Billing_Language__c = 'Italian', Billing_Name__c = data.company, Billing_Postal_Code__c = data.postal_code[1],
                    Billing_State__c = data.state[1], Billing_Street__c = data.street[1] + ' ' + data.street_number[1], Channels__c = 'Print;Mail', 
                    Grouping_Mode__c = ConstantsUtil.BILLING_GROUP_MODE_STANDARD, Is_Default__c = TRUE, Mail_address__c = data.email, 
                    Phone__c = data.customer_number, Process_Mode__c =  '0 - Standard', Customer__c=accounts[i].Id));
        }
        insert billingProfiles;*/        
		
        List<Contract> contracts = new List<Contract>();
        Contract c1 = new Contract(AccountId=accounts[0].id, StartDate=Date.Today(), Status='Draft');
        insert c1;
        Contract c2 = new Contract(AccountId=accounts[1].id, StartDate=Date.Today(), Status='Draft');
        insert c2;
        contracts.add(c1);
        contracts.add(c2);
        
        List<Invoice__c> invoices = new List<Invoice__c>();
        invoices.add(new Invoice__c(
        	Customer__c = accounts[0].Id, Correlation_Id__c = '2A8A8647-8CDC-4B51-82A2-17DA5C62B505', Name='400\'000\'001', Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED
        ));
        invoices.add(new Invoice__c(
        	Customer__c = accounts[1].Id, Correlation_Id__c = 'dc8d037f-b9e5-4652-8e42-0e8d8a3e232d', Name='400\'000\'002', Payment_Status__c = 'S3'
        ));
        insert invoices;
        
        List<Invoice_Order__c> invoiceOrders = new List<Invoice_Order__c>();
        invoiceOrders.add(new Invoice_Order__c(Invoice__c = invoices[0].Id, Contract__c = contracts[0].Id));
        invoiceOrders.add(new Invoice_Order__c(Invoice__c = invoices[1].Id, Contract__c = contracts[1].Id));
        insert invoiceOrders;
		
        //AccountStatusConfiguration__mdt accountStatusConfig = new AccountStatusConfiguration__mdt(DeveloperName='C', MasterLabel='C', Language='en_US', Label='C', QualifiedApiName='C', NextAccountStatus__c='Blocked', Reason__c='C');
        //accountStatusConfig.add(new AccountStatusConfiguration__mdt(DeveloperName='D', MasterLabel='D', Language='en_US', Label='D', QualifiedApiName='D', NextAccountStatus__c='Blocked', Reason__c='D'));
        //accountStatusConfig.add(new AccountStatusConfiguration__mdt(DeveloperName='E', MasterLabel='E', Language='en_US', Label='E', QualifiedApiName='E', NextAccountStatus__c='Blocked', Reason__c='E'));
        //insert accountStatusConfig;
        
        //Invoice__c invoice = invoices[0];
        //string pdfName = string.format(ConstantsUtil.INVOICE_PDF_NAME, new List<String> {invoice.Name} );
        //ContentVersion cv = new ContentVersion(Title = pdfName, PathOnClient = pdfName, VersionData = Blob.toPdf('Mock Data'), IsMajorVersion = true);
        //insert cv;
        //string contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion where Id = :cv.Id].ContentDocumentId;
        //insert new ContentDocumentLink(LinkedEntityId = invoice.Id, ContentDocumentId = contentDocumentId, shareType = 'V');    
		
        //Test.startTest();
    }
    
    static testmethod void test_InvoiceChanges() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(null, null);
        Test.stopTest();
		/*System.assertEquals(4, summaries.salesInvoiceSummaries.size());
		System.assertEquals('Gomes', summaries.salesInvoiceSummaries[0].clientName);*/
        
    }
    
	
    static testmethod void test_InvoiceChangesUnauthorized() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('billing/invoices').mockedResponse = 401;
        Test.startTest();
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(DateTime.newInstance(2018, 10, 15, 12, 15, 55), null);
		Test.stopTest();
        System.assert(summaries == null);
    }

    static testmethod void test_InvoiceChangesInvalidJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('billing/invoices').responses.put(200, '{ "field":"this will not cast" }');
        Test.startTest();
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(null, DateTime.newInstance(2019, 10, 14, 12, 15, 55));
		Test.stopTest();
        System.assert(summaries == null);
    }

    static testmethod void test_InvoiceChangesNotJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('billing/invoices').responses.put(200, 'will never parse');
        Test.startTest();
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(DateTime.newInstance(2018, 10, 15, 12, 15, 55), DateTime.newInstance(2018, 10, 16, 12, 15, 55));
		Test.stopTest();
        System.assert(summaries == null);
    }

    static testmethod void test_InvoiceChangesEmptyJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('billing/invoices').responses.put(200, '');
        Test.startTest();
        BillingResults.InvoiceSummaries summaries = BillingResults.getInvoicesChanges(null, null);
        Test.stopTest();
		System.assert(summaries == null);
    }

    static testmethod void test_InvoiceStatus() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        BillingResults.InvoiceSummary summary = BillingResults.getInvoiceSummary('2A8A8647-8CDC-4B51-82A2-17DA5C62B505');
		Test.stopTest();
        //System.assertEquals('Gomes', summary.clientName);        
    }
	
    static testmethod void test_retrievePDF() {
        AttachmentMock mock =  new AttachmentMock();
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        //Database.executeBatch(new SwissbillingPDFRetrieverBatch(), 1);
        Test.stopTest();
    }
    
     static testmethod void test_callRestGetDocument() {
        AttachmentMock mock =  new AttachmentMock();
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        BillingResults.getPdfDocument('AttachPdfDocument', '123');
        BillingResults.getPdfCreditNoteDocument('AttachPdfDocument', '123');
        BillingResults.getPdfDocument_v2('AttachPdfDocument', '123');
        Test.stopTest();
    }
    static testmethod void test_attachPdfDocument() {
        AttachmentMock mock =  new AttachmentMock();
        Test.setMock(HttpCalloutMock.class, mock);
        List<Invoice__c> invoices = [select id, Name, Correlation_Id__c,Invoice_Code__c from invoice__c];
        Test.startTest();
         BillingResults.attachPdfDocument(invoices);
        Test.stopTest();
    }
    
    static testmethod void test_DeliveryChanges() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        BillingResults.DeliveryReport report = BillingResults.getDeliveriesChanges(null, null);
        Test.stopTest();
		/*System.assertEquals(1, report.failedDeliveries.size());
		System.assertEquals(1, report.updatedAddresses.size());
		System.assertEquals('vice', report.failedDeliveries[0].clientName);
		System.assertEquals('belly', report.updatedAddresses[0].clientName);*/
    }
	/*
    static testmethod void test_DeliveryChangesInvalidJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('v1/deliveries').responses.put(200, '{ "field":"this will not cast" }');
        Test.startTest();
        BillingResults.DeliveryReport report = BillingResults.getDeliveriesChanges(null, null);
        Test.stopTest();
		System.assert(report == null);
    }

    static testmethod void test_DeliveryChangesEmptyJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('v1/deliveries').responses.put(200, '');
        Test.startTest();
        BillingResults.DeliveryReport report = BillingResults.getDeliveriesChanges(null, null);
        Test.stopTest();
		System.assert(report == null);
    }*/
    
    
    static testmethod void test_RatingChanges() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        BillingResults.CustomerRatingReport report = BillingResults.getCustomerRatingChanges(null, null);
        Test.stopTest();
		/*System.assertEquals(4, report.customerRatings.size());
		System.assertEquals('Pearson', report.customerRatings[0].clientName);*/
    }
	/*
    static testmethod void test_RatingChangesInvalidJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('changes/customer-ratings').responses.put(200, '{ "field":"this will not cast" }');
        Test.startTest();
        BillingResults.CustomerRatingReport report = BillingResults.getCustomerRatingChanges(null, null);
        Test.stopTest();
		System.assert(report == null);
    }

    static testmethod void test_RatingChangesEmptyJson() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        proxy.mocks.get('changes/customer-ratings').responses.put(200, '');
        Test.startTest();
        BillingResults.CustomerRatingReport report = BillingResults.getCustomerRatingChanges(null, null);
        Test.stopTest();
		System.assert(report == null);
    }*/
    
    static testmethod void test_CustomerRating() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        BillingResults.CustomerRatingUpdate rating = BillingResults.getCustomerRating('9B80B84D-2EAB-4003-BA91-723DA772D758');
		Test.stopTest();
        //System.assertEquals('Pearson', rating.clientName);
    }
    static testmethod void test_CrmView() {
        HttpResponseMockProxy proxy =  buildMockProxy();
        Test.setMock(HttpCalloutMock.class, proxy);
        Test.startTest();
        string crmView = BillingResults.getCrmViewHtml(new Account(Customer_Number__c=''));
        string crmView1 = BillingResults.getCrmViewHtml(new Account(Customer_Number__c='200\'000\'001'));
        string crmView2 = BillingResults.getCrmViewHtml(new Account(Customer_Number__c='200\'000\'002'));
        Test.stopTest();
		/*System.assertEquals('<h2>No data available</h2>', crmView);
		System.assert(crmView1.startsWith('<head><style>'));
		System.assert(crmView1.endsWith('</table></body>'));
		System.assertEquals('', crmView2);        */
    }
    
    public static HttpResponseMockProxy buildMockProxy() {
        HttpResponseMockProxy proxy = new HttpResponseMockProxy();
        proxy.mocks = new Map<string, GenericMock> {
            'billing/invoices' => new GenericMock(new map<integer, string> { 
            	200 => '{ "timestamp": "2018-11-24T09:11:00.6057728Z", "sales_invoice_summaries": [{"sales_invoice_id": "2A8A8647-8CDC-4B51-82A2-17DA5C62B505", "client_id": "F3D31ED5-EE90-4451-886C-1E938974245C",' +
            		'"client_name": "Gomes", "total_amount_incl_tax": 51.35, "payment_status": "S3", "open_amount": 13.6 }, {"sales_invoice_id": "2A8A8647-8CDC-4B51-82A2-17DA5C62B505", "client_id": "F3D31ED5-EE90-4451-886C-1E938974245C",' +
            		'"client_name": "Gomes", "total_amount_incl_tax": 0, "payment_status": "2", "open_amount": 13.6 }, {"sales_invoice_id": "2A8A8647-8CDC-4B51-82A2-17DA5C62B505", "client_id": "F3D31ED5-EE90-4451-886C-1E938974245C",' +
            		'"client_name": "Gomes", "total_amount_incl_tax": 13.6, "payment_status": "0", "open_amount": 0 }, {"sales_invoice_id": "dc8d037f-b9e5-4652-8e42-0e8d8a3e232d", "client_id": "F3D31ED5-EE90-4451-886C-1E938974245C",' +
            		'"client_name": "Gomes", "total_amount_incl_tax": 19.8, "payment_status": "S3", "open_amount": 50.1 }]}',
                401 => 'Unauthorized'
            }),
            'ei/status' => new GenericMock(new map<integer, string> { 
            	200 => '{"salesInvoiceId": "2A8A8647-8CDC-4B51-82A2-17DA5C62B505", "clientId": "F3D31ED5-EE90-4451-886C-1E938974245C",' +
            		'"clientName": "Gomes", "totalAmountInclTax": 51.35, "paymentStatus": "S3", "openAmount": 13.6 }'
            }),                
            'billing/deliveries' => new GenericMock(new map<integer, string> { 
            	200 => '{ "timestamp": "2018-11-25T14:00:51.6756718Z", "failed_deliveries": [ { "delivery_type": "Postal", "sent_date": "1960-11-11", "invoice_id": "dc8d037f-b9e5-4652-8e42-0e8d8a3e232d", "client_id": "B1150452-B152-4660-897F-E4D150B9803D", ' +
                    '"client_name": "vice", "recipient": { "full_name": "craft", "address": { "line1": "2604 Moore Street ", "line2": "", "postal_code": "A0P 5K4", "city": "Sainte-Anne-de-Bellevue", "country_code": "CH", "attention_of": "gentrify" } }, ' +
                    '"delivery_email": "Ozella.Clark@hotmail.com" } ], "updated_addresses": [ { "client_id": "4C806A6B-2862-465E-8781-872C9D271AB3", "client_name": "belly", "delivery_address": { "fullName": "craft", "address": { "line1": "9462 Brighton 3rd Lane ", ' +
                    '"line2": "", "postalCode": "C0R 0Y3", "city": "Las Palmas-Juarez", "countryCode": "CH", "attentionOf": "cosby" } }, "delivery_email": "Jeremy.Williams@hotmail.com" } ]}'
            }),
            'billing/customer-ratings' => new GenericMock(new map<integer, string> { 
            	200 => '{ "time_stamp": "2018-11-25T14:15:19.9421963Z", "customer_ratings": [ { "client_id": "9B80B84D-2EAB-4003-BA91-723DA772D758", "time_stamp": "2018-11-19T10:03:37.0928786", ' +
                    '"client_name": "Pearson", "old_client_rating": "C", "new_client_rating": "E", "amount_at_risk": 89.63 }, { "client_id": "9B80B84D-2EAB-4003-BA91-723DA772D758", "time_stamp": "2018-11-19T10:03:37.0928786", ' +
                    '"client_name": "Pearson", "old_client_rating": "E", "new_client_rating": "B", "amount_at_risk": 20.10 }, { "client_id": "9B80B84D-2EAB-4003-BA91-723DA772D758", "time_stamp": "2018-11-19T10:03:37.0928786", ' +
                    '"client_name": "Pearson", "old_client_rating": "B", "new_client_rating": "C", "amount_at_risk": 109.63 }, { "client_id": "4C806A6B-2862-465E-8781-872C9D271AB3", "time_stamp": "2018-11-19T10:03:37.0928786", ' +
                    '"client_name": "Pearson", "old_client_rating": "N", "new_client_rating": "A", "amount_at_risk": 0 }]}'
            }),                
            'ei/rating' => new GenericMock(new map<integer, string> { 
            	200 => '{ "clientId": "9B80B84D-2EAB-4003-BA91-723DA772D758", "timestamp": "2018-11-19T10:03:37.0928786", ' +
                    '"clientName": "Pearson", "oldClientRating": "C", "newClientRating": "E", "amountAtRisk": 89.63 }'
            }),
            'ei/200\'000\'001' => new GenericMock(new map<integer, string> { 
            	200 => '<head><style>h1 { font-size: 25px; } a:link, a:visited {color: black;text-align: center;text-decoration: none;display: inline-block;}' +
                    'table {font-family: arial, sans-serif;	border-collapse: collapse; width: 80%;}	td, th {text-align: left;padding: 8px;}' +
                    'tr.events > th {font-size: 15px;} .container {	padding-top: 20px; }.border-top {border-top: 1px solid;	}</style> </head>' +
                    '<body class="container"> <h1>Customer Details Page</h1> <table id="invoices"> <tr class="invoice"> <th> Invoice ID </th> <th> Event Type </th>' +
                    '<th>Event Date	</th><th>Status</th><th>Amount</th><th>Open Amount</th><th>Download PDF</th></tr>' +
                    '<tr class="border-top"> <td>412621cfce9cc1ac9bd26beef881b4cb </td> <td> Invoice Creation </td> <td>14.12.2018</td> <td>Created </td> <td> 390.00 </td>' + 
                    '<td> </td> <td> <a target="_blank" href="/api/invoices/412621cfce9cc1ac9bd26beef881b4cb/documents">Download</a> </td> </tr> <tr class="">' + 
                    '<td> </td> <td> Invoice Paid </td> <td>14.12.2018</td> <td> </td> <td> 390.00 </td> <td>0.00 </td> <td> </td> </tr>' +
                    '</table></body>'
 
            })
        };
        return proxy;
    }
    
	public class HttpResponseMockProxy implements HttpCalloutMock {
        public Map<string, GenericMock> mocks;
	   	
        public HTTPResponse respond(HTTPRequest req) {
            system.debug('LDIMARTINO - REQ: '+ req);
            List<string> parts = req.getEndpoint().substring(ConstantsUtil.BILLING_ESB_SWISSBILLING_ENDPOINT.length()).split('/');
			system.debug('LDIMARTINO - parts: '+ parts);
            string key = parts[1] + '/' + parts[parts.size() - 1]; // first and last key, pattern is ei/sf/method1/id?/method2
            system.debug('LDIMARTINO - key: ' + key);
            GenericMock mock;
            if(mocks != null) {
                mock = mocks.get(key);
            } 
            if(mock == null) {
                HttpResponse res = new HttpResponse();
                res.setStatusCode(404);
                return res;
            }
            return mock.respond(req);
        }
    }

    public class GenericMock implements HttpCalloutMock {
        public integer mockedResponse = 200;
        
        public map<integer, string> responses;
        
        public GenericMock(map<integer, string> responses) {
            this.responses = responses;
        }
        
        public virtual HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HTTPResponse();
            string responseBody = responses.get(mockedResponse);
            if(responseBody == null) {
                res.setStatusCode(404);
                return res;
            }
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(mockedResponse);
            res.setBody(responseBody);
            return res;
        }
    }

    public class AttachmentMock implements HttpCalloutMock {
                
        public virtual HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HTTPResponse();

            res.setHeader('Content-Type', 'application/pdf');
            res.setStatusCode(200);
            res.setBodyAsBlob(Blob.toPdf('Sample pfd'));
            return res;
        }
    }   
    
    /*static testmethod void test(){
        TestSetupUtilClass.createAccounts(1);
        Account a = [SELECT id,Customer_Number__c FROM Account limit 1];
        BillingResults.getCrmViewHtml(a);
    }*/
}