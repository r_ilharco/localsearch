global without sharing class ContractActivationQueueJob implements Queueable {

	private List<Contract> vlistContract;
   
    public ContractActivationQueueJob(List<Contract> listContract) {
        vlistContract= listContract;       
    }    
  global void execute(QueueableContext SC) {
	 ContractUtility.ActivateContractsJob(vlistContract);
  }
}