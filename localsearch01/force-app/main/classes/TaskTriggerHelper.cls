/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Giovanni Prota <gprota@deloitte.it>
 * Date        : 05-12-2019
 * Sprint      : 2
 * Work item   : #655
 * Testclass   : Test_TaskTrigger
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public without sharing class TaskTriggerHelper {
    
    public static void checkContactisFromAccount(List<Task> NewTask){
        
        
        Map<Id,List<Task>> mapTaskToCheck = new Map<Id,List<Task>>();
        
        for(Task t: NewTask){
            if (t.WhoId != null && t.WhatId != null && t.WhoId.getSObjectType().getDescribe().getName().equals('Contact') && t.WhatId.getSObjectType().getDescribe().getName().equals('Account')){
                if(!mapTaskToCheck.containsKey(t.WhoId))
                    mapTaskToCheck.put(t.WhoId, new List<Task>());
                mapTaskToCheck.get(t.WhoId).add(t);                
            } 
        }
        
        Map<Id, Contact> mapContact = SEL_Contact.getContactsById(mapTaskToCheck.keySet()); 
        
        for(Id cId: mapContact.keySet()){
            for(Task t: mapTaskToCheck.get(cId)){
                if(t.WhatId != mapContact.get(cId).AccountId){
                    t.addError(Label.Task_Account_Contact_Check);
                }
            }
        }
    }
}