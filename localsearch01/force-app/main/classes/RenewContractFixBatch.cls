/**
* Author	   : Mara Mazzarella <mamazzarella@deloitte.it>
* Date		   : 21-06-2020
* Sprint      : phase 3- sprint 1
* Work item   : 
* Testclass   : Test_RenewContractController
* Package     : 
* Description : Batch class for renew contracts of CPU time limit exceeded issue
* Changelog   :
*/

global class RenewContractFixBatch implements Database.batchable<sObject>, Schedulable{
   
    global void execute(SchedulableContext sc) {
        RenewContractFixBatch b = new RenewContractFixBatch(); 
        database.executebatch(b, 5);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Status, SBQQ__RenewalQuoted__c,ToFix__c ' +
            'FROM Contract ' +
            'WHERE SBQQ__RenewalQuoted__c = false and ToFix__c = true and SBQQ__Evergreen__c  = false';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext info, List<Contract> scope){
        for(Contract c : scope){
            c.SBQQ__RenewalQuoted__c = true;
            c.ToFix__c= false;
        }
        List<Database.SaveResult> res = Database.update(scope, false);
        for (Database.SaveResult sr : res) {
            if (!sr.isSuccess()) {              
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Order fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext info){     
    }
    

}