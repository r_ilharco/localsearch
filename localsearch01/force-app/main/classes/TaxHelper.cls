public without sharing class TaxHelper {
    
    private static List<Vat_Schedule__c> schedulesSglt; 
    
    private static List<Vat_Schedule__c> schedules {
        get {
            if(schedulesSglt == null) {
                schedulesSglt =  [SELECT Tax_Code__c, Start_Date__c, End_Date__c, Value__c from Vat_Schedule__c LIMIT 200];
            }
            return schedulesSglt;
        }
    }
    
    // Returns a grouping key for common vat schedules
    public static string getHashKey(string taxCode, date dateFrom, date dateTo) {
        return taxCode + ':' + dateFrom.year() + string.valueOf(dateFrom.month()).leftPad(2, '0') + string.valueOf(dateFrom.day()).leftPad(2, '0')
            + '-' + dateTo.year() + string.valueOf(dateTo.month()).leftPad(2, '0') + string.valueOf(dateTo.day()).leftPad(2, '0');
    }
    
    public static date getMaxDate(date one, date two) {
        if(one == null) return two;
        if(two == null) return one;
        if(one.daysBetween(two)>0) return two;
        return one;
    }

    public static date getMinDate(date one, date two) {
        if(one == null) return two;
        if(two == null) return one;
        if(one.daysBetween(two)<0) return two;
        return one;
    }
    
    public static List<VatListItem> getVatItems(string taxCode, date dateFrom, date dateTo, decimal amount, date refDate) {
        // List is expected to be short (2 vat codes, few changes a year) so no need to indexize for seeking
        system.debug('ldimartinoqa, taxCode: ' + taxCode);
        system.debug('ldimartinoqa, dateFrom: ' + dateFrom);
        system.debug('ldimartinoqa, dateTo: ' + dateTo);
        system.debug('ldimartinoqa, amount: ' + amount);
        system.debug('ldimartinoqa, refDate: ' + refDate);
        List<VatListItem> vatList = new List<VatListItem>();
        if(taxCode == ConstantsUtil.INVOICE_TAX_CODE_EXEMPTION){
            vatList.add(new VatListItem(
                            taxCode,
                            dateFrom,
                            null,
                            ((amount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                            0,
                            0,
                            0,
                            false
                        ));
        }else{
            for(Vat_Schedule__c schedule : schedules) {
                if(schedule.Tax_Code__c == taxCode 
                   && ((schedule.End_Date__c == null || schedule.End_Date__c.daysBetween(dateFrom) <=0) && 
                       (schedule.Start_Date__c == null || schedule.Start_Date__c.daysBetween(dateTo) >=0)
                   )
                ) {
                    system.debug('ldimartinoqa, here1');
                    date periodFrom = getMaxDate(schedule.Start_Date__c, dateFrom);
                    date periodTo = getMinDate(schedule.End_Date__c, dateTo);
                    decimal periodAmount = amount;
                    system.debug('ldimartinoqa, periodFrom: ' + periodFrom);
                    system.debug('ldimartinoqa		, refDate: ' + refDate);
                    if(dateFrom == dateTo){
                        vatList.add(new VatListItem(
                            taxCode,
                            dateFrom,
                            dateTo,
                            ((amount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                            schedule.Value__c,
                            ((amount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) /(decimal)100,
                            ((amount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) /(decimal)100,
                            false
                        ));
                    }else{
                        if(refDate !=null && periodFrom.monthsBetween(refDate) > 0) {
                            Date nextAccrualFrom = periodFrom.addMonths(periodFrom.monthsBetween(refDate));
                            Date pastPartTo = nextAccrualFrom.addDays(-1);
                            if(periodFrom.daysBetween(pastPartTo)> 0 && dateFrom.daysBetween(dateTo) > 0) {
                                periodAmount = amount * (periodFrom.monthsBetween(pastPartTo.addDays(1))) / (dateFrom.monthsBetween(dateTo.addDays(1)));
                            }
                            vatList.add(new VatListItem(
                                taxCode,
                                periodFrom,
                                pastPartTo,
                                ((periodAmount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                                schedule.Value__c,
                                ((periodAmount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                ((periodAmount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                false
                            ));
                            periodFrom = nextAccrualFrom;
                        }
                        if(refDate == null || refDate.daysBetween(periodTo) > 0) {
                            if(periodFrom.daysBetween(periodTo)> 0 && dateFrom.daysBetween(dateTo) > 0) {
                                periodAmount = amount * (periodFrom.daysBetween(periodTo.addDays(1))) / (dateFrom.daysBetween(dateTo.addDays(1)));
                            }
                            vatList.add(new VatListItem(
                                taxCode,
                                periodFrom,
                                periodTo,
                                ((periodAmount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                                schedule.Value__c,
                                ((periodAmount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                ((periodAmount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                true
                            ));
                        }
                    }
                }
            }
        }
        if(vatList.size() == 0) {
        	throw new TaxHelperException('Tax schedule missing', ErrorHandler.ErrorCode.E_NO_TAX_SCHEDULE);
        }
        // Check if have all days. Gaps "use" one day
        integer days = dateFrom.daysBetween(dateTo) - vatList.size() + 1;
        if(taxCode != ConstantsUtil.INVOICE_TAX_CODE_EXEMPTION){
            for(VatListItem vatItem : vatList) {
                days -= vatItem.dateFrom.daysBetween(vatItem.dateTo);
            }
            system.debug('days: ' + days);
            /*
            if(days < 0) {
                throw new TaxHelperException('Tax schedules overlap', ErrorHandler.ErrorCode.E_TAX_SCHEDULE_OVERLAP);
            }
            if(days > 0) {
                throw new TaxHelperException('Tax schedule missing', ErrorHandler.ErrorCode.E_NO_TAX_SCHEDULE);
            }*/
        }
        return vatList;
    }

    public static List<VatListItem> getVatItemsForCreditNoteToAbacus(string taxCode, date dateFrom, date dateTo, decimal amount, date refDate){
        List<VatListItem> vatList = new List<VatListItem>();
        if(taxCode == ConstantsUtil.INVOICE_TAX_CODE_EXEMPTION){
                      system.debug('here for tax exemption - credit notes');
                      vatList.add(new VatListItem(
                          taxCode,
                          dateFrom,
                          null,
                          amount,
                          0,
                          0,
                          0,
                          false
                      ));
        }else{
            for(Vat_Schedule__c schedule : schedules) {
                if(schedule.Tax_Code__c == taxCode 
                    && ((schedule.End_Date__c == null || schedule.End_Date__c.daysBetween(dateFrom) <=0) && 
                        (schedule.Start_Date__c == null || schedule.Start_Date__c.daysBetween(dateTo) >=0)
                       )
                  ) {
                      date periodFrom = getMaxDate(schedule.Start_Date__c, dateFrom);
                      date periodTo = getMinDate(schedule.End_Date__c, dateTo);
                      decimal periodAmount = amount;
                      if(dateFrom == dateTo){
                          vatList.add(new VatListItem(
                              taxCode,
                              dateFrom,
                              dateTo,
                              ((amount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                              schedule.Value__c,
                              ((amount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                              ((amount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                              false
                          ));
                      }else{
                          if(periodTo.daysBetween(refDate) >= 0){
                              vatList.add(new VatListItem(
                                  taxCode,
                                  refDate,
                                  refDate,
                                  ((periodAmount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                                  schedule.Value__c,
                                  ((periodAmount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  ((periodAmount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  true
                              ));
                          }else if(refDate !=null && periodFrom.monthsBetween(refDate) > 0) {
                              Date nextAccrualFrom = periodFrom.addMonths(periodFrom.monthsBetween(refDate));
                              Date pastPartTo = nextAccrualFrom.addDays(-1);
                              if(periodFrom.daysBetween(pastPartTo)> 0 && dateFrom.daysBetween(dateTo) > 0) {
                                  periodAmount = amount * (periodFrom.monthsBetween(pastPartTo.addDays(1))) / (dateFrom.monthsBetween(dateTo.addDays(1)));
                              }
                              vatList.add(new VatListItem(
                                  taxCode,
                                  periodFrom,
                                  pastPartTo,
                                  ((periodAmount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                                  schedule.Value__c,
                                  ((periodAmount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  ((periodAmount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  false
                              ));
                              periodFrom = nextAccrualFrom;
                          }
                          if(refDate == null || refDate.daysBetween(periodTo) > 0) {
                              if(periodFrom.daysBetween(periodTo)> 0 && dateFrom.daysBetween(dateTo) > 0) {
                                  periodAmount = amount * (periodFrom.monthsBetween(periodTo.addDays(1))) / (dateFrom.monthsBetween(dateTo.addDays(1)));
                              }
                              vatList.add(new VatListItem(
                                  taxCode,
                                  refDate,
                                  periodTo,
                                  ((periodAmount * 100.0).round(RoundingMode.HALF_EVEN)/(decimal)100),
                                  schedule.Value__c,
                                  ((periodAmount * schedule.Value__c / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  ((periodAmount * (schedule.Value__c + 100.0) / 100.0) * 100.0).round(RoundingMode.HALF_EVEN) / (decimal)100,
                                  true
                              ));
                          }
                      }
                      system.debug('ldimartinoqa, here3');
                  }
            } 
        }
        if(vatList.size() == 0) {
            throw new TaxHelperException('Tax schedule missing', ErrorHandler.ErrorCode.E_NO_TAX_SCHEDULE);
        }
        return vatList;
    }
    
    public class VatListItem {
        public date dateFrom;
        public date dateTo;
        public decimal net;
        public decimal percentage;
        public decimal vat;
        public decimal total;
        public string vatCode;
        public boolean hasAccrual;
        
        public VatListItem(string taxCode, date dateFrom, date dateTo, decimal amount, decimal percentage, decimal vat, decimal total, boolean hasAccrual) {
            this.vatCode = taxCode;
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
            this.net = amount;
            this.percentage = percentage;
            this.vat = vat;
            this.total = total;
            this.hasAccrual = hasAccrual;
        }
    }
    
    public class TaxHelperException extends ErrorHandler.GenericException {
        
        public TaxHelperException(string message, ErrorCode errorCode) {
            super(message, errorCode);
        }      
    }
}
