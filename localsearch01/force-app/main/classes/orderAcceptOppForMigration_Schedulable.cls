global class orderAcceptOppForMigration_Schedulable implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(system.now().minute()+1);
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];

        if(async.isEmpty()){ 
            for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'orderAcceptedOpportunityForMigration%']){
                if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                    System.abortjob(ct.id);
                else 
                    batchs.add(ct.CronJobDetail.name);
            }
            if(batchs.isEmpty()|| !batchs.contains('orderAcceptedOpportunityForMigration0')){
                orderAcceptedOpportunityForMigration b0 = new orderAcceptedOpportunityForMigration(new List<String>{'0','1','a','A','b','B'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
                System.schedule('orderAcceptedOpportunityForMigration0', strSchedule, b0);
            } 
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration1')){ 
                orderAcceptedOpportunityForMigration b1 = new orderAcceptedOpportunityForMigration(new List<String>{'2','3','e','E','f','F'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration1', strSchedule,b1);
            } 
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration2')){
                orderAcceptedOpportunityForMigration b2 = new orderAcceptedOpportunityForMigration(new List<String>{'4','5','I','i','l','L'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration2', strSchedule,b2);
            } 
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration3')){
                orderAcceptedOpportunityForMigration b3 = new orderAcceptedOpportunityForMigration(new List<String>{'6','7','o','O','p','P'});
                String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration3', strSchedule, b3); 
            } 
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration4')){
                orderAcceptedOpportunityForMigration b4 = new orderAcceptedOpportunityForMigration(new List<String>{'8','9','s','S','t','T'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration4', strSchedule,b4);
            }
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration5')){
                orderAcceptedOpportunityForMigration b5 = new orderAcceptedOpportunityForMigration(new List<String>{'u','U','v','V','z','Z'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration5', strSchedule,b5);
            }
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration6')){
                orderAcceptedOpportunityForMigration b6 = new orderAcceptedOpportunityForMigration(new List<String>{'c','C','d','D','x','X','w','W'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration6', strSchedule,b6);
            }
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration7')){
                orderAcceptedOpportunityForMigration b7 = new orderAcceptedOpportunityForMigration(new List<String>{'g','G','h','H','y','Y'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration7', strSchedule,b7);
            }
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration8')){
                orderAcceptedOpportunityForMigration b8 = new orderAcceptedOpportunityForMigration(new List<String>{'m','M','n','N','j','J'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration8', strSchedule,b8);
            }
            if(batchs.isEmpty() || !batchs.contains('orderAcceptedOpportunityForMigration9')){
                orderAcceptedOpportunityForMigration b9 = new orderAcceptedOpportunityForMigration(new List<String>{'q','Q','r','R','k','K'});
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                System.schedule('orderAcceptedOpportunityForMigration9', strSchedule,b9);
            }
        }
    }
}