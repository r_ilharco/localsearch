/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*	This batch executes every day and sets the SBQQ__SignatureStatus__c to "Expired" on all Quote Documents
*	whose ExpirationDate is YESTERDAY AND are not 'Signed'/'Expired'/'Revoked'. 
* 	Furthermore the related Quotes are set to "Draft" if they haven't reached their expiration date to
*	permit to generate a new quote document. 
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2020-07-20
* @systemLayer    Batch

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedby      Name LastName
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class SetQuoteDocumentsExpiredBatch implements Database.batchable<sObject>, Schedulable{
    
    global void execute(SchedulableContext sc) {
       SetQuoteDocumentsExpiredBatch b = new SetQuoteDocumentsExpiredBatch(); 
       Database.executebatch(b, 2000);
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, SBQQ__Status__c, SBQQ__ExpirationDate__c, (SELECT Id, SBQQ__Quote__c, Name, SBQQ__SignatureStatus__c, ExpirationDate__c FROM SBQQ__R00N70000001lX7YEAU__r) '
        +'FROM SBQQ__Quote__c ' +
        'WHERE Id IN (SELECT SBQQ__Quote__c FROM SBQQ__QuoteDocument__c WHERE ExpirationDate__c = YESTERDAY)');
    }
    
    global void execute(Database.BatchableContext info, List<SBQQ__Quote__c> scope){
        Map<Id, SBQQ__Quote__c> quotesToUpdateMap = new Map<Id, SBQQ__Quote__c>();
        Map<Id, SBQQ__QuoteDocument__c> quoteDocumentsMap = new Map<Id, SBQQ__QuoteDocument__c>();
        Map<Id, SBQQ__Quote__c> quotesMap = new Map<Id, SBQQ__Quote__c>(scope);
        List<SBQQ__QuoteDocument__c> quoteDocumentsToUpdateList = new List<SBQQ__QuoteDocument__c>();
        Set<Id> expiredQuoteDocumentsSet = new Set<Id>();
        Map<Id, Map<String, Set<Id>>> qdsByQuoteIdAndSignatureStatusMap = new Map<Id, Map<String, Set<Id>>>();
        
        for(SBQQ__Quote__c quote : scope){
            List<SBQQ__QuoteDocument__c> quoteDocumentsList = quote.SBQQ__R00N70000001lX7YEAU__r;
            if(quoteDocumentsList != NULL && quoteDocumentsList.size() > 0){
                for(SBQQ__QuoteDocument__c qd : quoteDocumentsList){
                    Boolean isExpired = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_EXPIRED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c);
                    Boolean isRevoked = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_REVOKED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c);
                    Boolean isSigned = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_SIGNED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c);
                    if(qd != NULL){
                        quoteDocumentsMap.put(qd.Id, qd);
                        if(qd.ExpirationDate__c != NULL && qd.ExpirationDate__c == Date.today().addDays(-1) && !isExpired && !isRevoked && !isSigned ){
                            expiredQuoteDocumentsSet.add(qd.Id);
                        }
                        else{
                            if(!qdsByQuoteIdAndSignatureStatusMap.containsKey(quote.Id))
                                qdsByQuoteIdAndSignatureStatusMap.put(quote.Id, new Map<String, Set<Id>>());
                            
                            if(!qdsByQuoteIdAndSignatureStatusMap.get(quote.Id).containsKey(qd.SBQQ__SignatureStatus__c))
                                qdsByQuoteIdAndSignatureStatusMap.get(quote.Id).put(qd.SBQQ__SignatureStatus__c, new Set<Id>());
                            
                            qdsByQuoteIdAndSignatureStatusMap.get(quote.Id).get(qd.SBQQ__SignatureStatus__c).add(qd.Id);
                        }
                    }
                }
            }
        }
        
        for(Id expiredQuoteDocumentId : expiredQuoteDocumentsSet){
            SBQQ__QuoteDocument__c qd = quoteDocumentsMap.get(expiredQuoteDocumentId);
            if(!ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_EXPIRED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c)
              && !ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_REVOKED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c)
              && !ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_SIGNED.equalsIgnoreCase(qd.SBQQ__SignatureStatus__c)){
                qd.SBQQ__SignatureStatus__c = ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_EXPIRED;
                quoteDocumentsToUpdateList.add(qd);
            }
            Boolean quoteToUpdateInDraft = true;
            
            if((quotesToUpdateMap != NULL && !quotesToUpdateMap.containsKey(qd.SBQQ__Quote__c)) 
               && (qdsByQuoteIdAndSignatureStatusMap != NULL && qdsByQuoteIdAndSignatureStatusMap.containsKey(qd.SBQQ__Quote__c))){
                Map<String, Set<Id>> qdsBySignatureStatusMap = qdsByQuoteIdAndSignatureStatusMap.get(qd.SBQQ__Quote__c);
                
                for(String signatureStatus : qdsBySignatureStatusMap.keySet()){
                    if(!ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_EXPIRED.equalsIgnoreCase(signatureStatus) 
                      && !ConstantsUtil.QUOTEDOCUMENT_SIGNATURESTATUS_REVOKED.equalsIgnoreCase(signatureStatus)
                      && qdsBySignatureStatusMap.get(signatureStatus).size() > 0)
                        quoteToUpdateInDraft = false;
                }
            }
            
            Boolean isAcceptedQuote = ConstantsUtil.Quote_Status_Accepted.equalsIgnoreCase(quotesMap.get(qd.SBQQ__Quote__c).SBQQ__Status__c);
            Boolean isRejectedQuote = ConstantsUtil.QUOTE_STATUS_REJECTED.equalsIgnoreCase(quotesMap.get(qd.SBQQ__Quote__c).SBQQ__Status__c);
            if(quoteToUpdateInDraft && !quotesToUpdateMap.containsKey(qd.SBQQ__Quote__c) && !isAcceptedQuote && !isRejectedQuote){
                SBQQ__Quote__c quote = new SBQQ__Quote__c();
                quote.Id = qd.SBQQ__Quote__c;
                quote.SBQQ__Status__c = ConstantsUtil.Quote_Status_Draft;
                quote.IsQuoteDocumentRevoked__c = true;
                quotesToUpdateMap.put(quote.Id,quote);
            }
        }
        
        if(quoteDocumentsToUpdateList != NULL && quoteDocumentsToUpdateList.size() > 0)
            Database.update(quoteDocumentsToUpdateList,false);
        
        if(quotesToUpdateMap != NULL && quotesToUpdateMap.size() > 0)
            Database.update(quotesToUpdateMap.values(),false);
        
        for(SBQQ__Quote__c quote : quotesToUpdateMap.values())
            quote.IsQuoteDocumentRevoked__c = false;
        
        if(quotesToUpdateMap != NULL && quotesToUpdateMap.size() > 0)
            Database.update(quotesToUpdateMap.values(),false);
		        
    }

    global void finish(Database.BatchableContext info){}

}