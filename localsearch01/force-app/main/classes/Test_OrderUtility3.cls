@isTest
public class Test_OrderUtility3 {

    @isTest
    public static void test_OrderUtility_AutomaticAmendment(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{childQL}, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        insert childrenSubs;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        String quoteJSON = SBQQ.ServiceRouter.load('SBQQ.ContractManipulationAPI.ContractAmender', contract.Id, null);
        String quoteTypeAmend = ConstantsUtil.QUOTE_TYPE_AMENDMENT;
        String queryAmendQuote =' SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Type__c = :quoteTypeAmend LIMIT 1';
        SBQQ__Quote__c amendQuote = Database.query(queryAmendQuote); 
        
        String contractId = contract.Id;
        List<String> fieldNamesOpportunity = new List<String>(Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().keySet());
        String queryOpp =' SELECT ' +String.join( fieldNamesOpportunity, ',' ) +' FROM Opportunity WHERE SBQQ__AmendedContract__c = :contractId LIMIT 1';
        Opportunity amendOpp = Database.query(queryOpp);
        
        Product2 prodForAmend = generateProductForAmend();
        insert prodForAmend;
        
        SBQQ__QuoteLine__c fatherQLAmend = fatherQL.clone(false, true, false, false);
        fatherQLAmend.SBQQ__Quote__c = amendQuote.Id;
        fatherQLAmend.SBQQ__PriorQuantity__c  = fatherQLAmend.SBQQ__Quantity__c;
        fatherQLAmend.SBQQ__Quantity__c  = 0;
        insert fatherQLAmend;
        SBQQ__QuoteLine__c childQLAmend = childQL.clone(false, true, false, false);
        childQLAmend.SBQQ__Quote__c = amendQuote.Id;
        childQLAmend.SBQQ__RequiredBy__c = fatherQLAmend.Id;
        childQLAmend.SBQQ__PriorQuantity__c  = childQLAmend.SBQQ__Quantity__c;
        childQLAmend.SBQQ__Quantity__c  = 0;
        insert childQLAmend;
        SBQQ__QuoteLine__c quoteLineAmend = Test_DataFactory.generateQuoteLine(amendQuote.Id, prodForAmend.Id, Date.today(), null, null, fatherQLAmend.Id, ConstantsUtil.BILLING_FREQUENCY_ANNUAL);
        insert quoteLineAmend;
        
        amendQuote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update amendQuote;
		
        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_AMENDMENT);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        orderMap.put('SBQQ__Quote__c',amendQuote.Id);
        Order amendOrder = Test_DataFactory.generateOrder(orderMap);
        amendOrder.OpportunityId = amendOpp.Id;
        insert amendOrder;
        
        OrderItem amendOI = Test_DataFactory.generateFatherOrderItem(amendOrder.Id, quoteLineAmend);
        insert amendOI;
        OrderUtility.activateContracts(new List<Id>{amendOrder.Id});
		Test.stopTest();        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
	
    @isTest
    public static void test_OrderUtility_AutomaticUpgrade(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{childQL}, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.StartDate = Date.today().addMonths(-3);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        childrenSubs[0].SBQQ__QuoteLine__c = childQL.Id;
        insert childrenSubs;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
        upgradeOpportunity.CloseDate = Date.today().addDays(1);
		upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
		upgradeOpportunity.Upgrade__c = true;     
		upgradeOpportunity.Downgrade__c = false;
		insert upgradeOpportunity;
        
        String upgradeOpportunityId = upgradeOpportunity.Id;
        String queryUpgradeQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :upgradeOpportunityId LIMIT 1';
        SBQQ__Quote__c upgradeQuote = Database.query(queryUpgradeQuote);
        
        List<Product2> productsToInsertUpgrade = new List<Product2>();
        productsToInsertUpgrade.add(generteFatherProductUpgrade());
        productsToInsertUpgrade.add(generteChildProductUpgrade());
        insert productsToInsertUpgrade;
		
        SBQQ__QuoteLine__c fatherQLUpgrade = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsertUpgrade[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQLUpgrade;
        
        SBQQ__QuoteLine__c childQLUpgrade = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsertUpgrade[1].Id, Date.today(), contact.Id, null, fatherQLUpgrade.Id, 'Annual');
        insert childQLUpgrade;
        
        upgradeQuote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update upgradeQuote;

        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_UPGRADE);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        orderMap.put('SBQQ__Quote__c',upgradeQuote.Id);
        Order upgradeOrder = Test_DataFactory.generateOrder(orderMap);
        upgradeOrder.OpportunityId = upgradeOpportunity.Id;
        insert upgradeOrder;
        
        OrderItem upgradeOI = Test_DataFactory.generateFatherOrderItem(upgradeOrder.Id, fatherQLUpgrade);
        upgradeOI.Subscription_To_Terminate__c = fatherSub.Id;
        insert upgradeOI;
        List<OrderItem> childrenUpgradeOIs = Test_DataFactory.generateChildrenOderItem(upgradeOrder.Id, new List<SBQQ__QuoteLine__c>{childQLUpgrade}, upgradeOI.Id);
        childrenUpgradeOIs[0].Subscription_To_Terminate__c = childrenSubs[0].Id;
        insert childrenUpgradeOIs;
        OrderUtility.activateContracts(new List<Id>{upgradeOrder.Id});
		Test.stopTest();        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_OrderUtility_AutomaticUpgrade_catchBlock(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Send Owner Notification,Community User Creation,Quote Management,Order Management Insert Only,Order Management';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        OrderTriggerHandler.disableTrigger = true;
        OrderProductTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        
        Account account = Test_DataFactory.generateAccounts('Test Account',1,false)[0];
        insert account;
        
        String accId = account.Id;
        List<String> fieldNamesBP = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String queryBP =' SELECT ' +String.join( fieldNamesBP, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(queryBP); 
        
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Test Name', 1)[0];
        contact.Primary__c = true;
        insert contact;
        
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opportunity', account.Id);
        insert opportunity;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String opportunityId = opportunity.Id;
        String queryQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :opportunityId LIMIT 1';
        SBQQ__Quote__c quote = Database.query(queryQuote);
        
        SBQQ__QuoteLine__c fatherQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQL;
        
        SBQQ__QuoteLine__c childQL = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsert[1].Id, Date.today(), contact.Id, null, fatherQL.Id, 'Annual');
        insert childQL;
        
        quote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quote;
        //Creating activation order in fulfilled status
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type',ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate',String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId',account.Id);
        fieldApiNameToValue.put('SBQQ__Quote__c',quote.Id);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        order.OpportunityId = opportunity.Id;
        insert order;
        
        OrderItem fatherOI = Test_DataFactory.generateFatherOrderItem(order.Id, fatherQL);
        insert fatherOI;
        
        List<OrderItem> childrenOIs = Test_DataFactory.generateChildrenOderItem(order.Id, new List<SBQQ__QuoteLine__c>{childQL}, fatherOI.Id);
        insert childrenOIs;
        
        Contract contract = Test_DataFactory.generateContractFromOrder(order, false);
        contract.StartDate = Date.today().addMonths(-3);
        insert contract;

        SBQQ__Subscription__c fatherSub = Test_DataFactory.generateFatherSubFromOI(contract, fatherOI);
        fatherSub.Subscription_Term__c = fatherQL.Subscription_Term__c;
        fatherSub.SBQQ__QuoteLine__c = fatherQL.Id;
        insert fatherSub;
        
        List<SBQQ__Subscription__c> childrenSubs = Test_DataFactory.generateChildrenSubsFromOis(contract, childrenOIs, fatherSub.Id);
        childrenSubs[0].SBQQ__QuoteLine__c = childQL.Id;
        insert childrenSubs;
        
        fatherOI.SBQQ__Subscription__c = fatherSub.Id;
        update fatherOI;
        
        childrenOIs[0].SBQQ__Subscription__c = childrenSubs[0].Id;
        update childrenOIs;
        
        order.Contract__c = contract.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        order.SBQQ__Contracted__c = true;
        update order;
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        update contract;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        fatherSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        subsToUpdate.add(fatherSub);
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_OrderUtilityMock());
        
        Opportunity upgradeOpportunity = Test_DataFactory.generateOpportunity('Upgrade Opportunity', account.Id);
        upgradeOpportunity.CloseDate = Date.today().addDays(1);
		upgradeOpportunity.UpgradeDowngrade_Contract__c = contract.Id;
		upgradeOpportunity.Upgrade__c = true;     
		upgradeOpportunity.Downgrade__c = false;
		insert upgradeOpportunity;
        
        String upgradeOpportunityId = upgradeOpportunity.Id;
        String queryUpgradeQuote = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c = :upgradeOpportunityId LIMIT 1';
        SBQQ__Quote__c upgradeQuote = Database.query(queryUpgradeQuote);
        
        List<Product2> productsToInsertUpgrade = new List<Product2>();
        productsToInsertUpgrade.add(generteFatherProductUpgrade());
        productsToInsertUpgrade.add(generteChildProductUpgrade());
        insert productsToInsertUpgrade;
		
        SBQQ__QuoteLine__c fatherQLUpgrade = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsertUpgrade[0].Id, Date.today(), contact.Id, null, null, 'Annual');
        insert fatherQLUpgrade;
        
        SBQQ__QuoteLine__c childQLUpgrade = Test_DataFactory.generateQuoteLine(quote.Id, productsToInsertUpgrade[1].Id, Date.today(), contact.Id, null, fatherQLUpgrade.Id, 'Annual');
        insert childQLUpgrade;
        
        upgradeQuote.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update upgradeQuote;

        Map<String,String> orderMap = new Map<String,String>();
        orderMap.put('Custom_Type__c',ConstantsUtil.ORDER_TYPE_ACTIVATION);
        orderMap.put('Type',ConstantsUtil.ORDER_TYPE_UPGRADE);
        orderMap.put('EffectiveDate',String.valueOf(Date.today()));
        orderMap.put('EndDate',String.valueOf(Date.today().addMonths(12)));
        orderMap.put('AccountId',account.Id);
        orderMap.put('SBQQ__Quote__c',upgradeQuote.Id);
        Order upgradeOrder = Test_DataFactory.generateOrder(orderMap);
        upgradeOrder.OpportunityId = upgradeOpportunity.Id;
        insert upgradeOrder;
        
        OrderItem upgradeOI = Test_DataFactory.generateFatherOrderItem(upgradeOrder.Id, fatherQLUpgrade);
        insert upgradeOI;
        List<OrderItem> childrenUpgradeOIs = Test_DataFactory.generateChildrenOderItem(upgradeOrder.Id, new List<SBQQ__QuoteLine__c>{childQLUpgrade}, upgradeOI.Id);
        insert childrenUpgradeOIs;
        OrderUtility.activateContracts(new List<Id>{upgradeOrder.Id});
		Test.stopTest();        
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        OrderTriggerHandler.disableTrigger = false;
        OrderProductTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    public static Product2 generteFatherProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProduct(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Product2 generateProductForAmend(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Zusätzliche Sprache');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL ZSB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_SPRACHE001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Product2 generteFatherProductUpgrade(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Standard');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWS');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Standard');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITESTANDARD001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProductUpgrade(){
        Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWS');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Standard');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWSTANDARD_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public class QuoteModel {
        public SBQQ__Quote__c record;
        public QuoteLineModel[] lineItems;
        public QuoteLineGroupModel[] lineItemGroups;
        public Integer nextKey;
        public Boolean applyAdditionalDiscountLast;
        public Boolean applyPartnerDiscountFirst;
        public Boolean channelDiscountsOffList;
        public Decimal customerTotal;
        public Decimal netTotal;
        public Decimal netNonSegmentTotal;
    } 
    public class QuoteLineModel {
        public SBQQ__QuoteLine__c record;
        public Boolean amountDiscountProrated;
        public Integer parentGroupKey;
        public Integer parentItemKey;
        public Integer key;
        public Boolean upliftable;
        public String configurationType;
        public String configurationEvent;
        public Boolean reconfigurationDisabled;
        public Boolean descriptionLocked;
        public Boolean productQuantityEditable;
        public Decimal productQuantityScale;
        public String dimensionType;
        public Boolean productHasDimensions;
        public Decimal targetCustomerAmount;
        public Decimal targetCustomerTotal;
    }
    public class QuoteLineGroupModel {
        public SBQQ__QuoteLineGroup__c record;
        public Decimal netNonSegmentTotal;
        public Integer key;
    }
}