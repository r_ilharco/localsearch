@isTest
public class Test_SblInvoicesChangesBatch {
	 public static Integer nInvoices = 6;
    
    @testSetup
    static void setup() {
        
        //Account acc = Test_DataFactory.createAccounts('testAcc', 1)[0];
        List<Account> acc = Test_DataFactory.generateAccounts('Test Account', 1, false);
        insert acc;
        
       List<Invoice__c> invs = Test_DataFactory.createInvoices(acc[0], 'testInv', nInvoices);
        insert invs;

        for(Integer idx=0; idx<nInvoices; idx++){
            if(nInvoices > idx+1 && math.mod(idx,2) == 0)
                invs[idx].Correlation_Id__c=invs[idx+1].Id;
            else if(math.mod(idx,2) != 0)
                invs[idx].Correlation_Id__c=invs[idx-1].Id;
            if(idx < nInvoices-2){
                 invs[idx].Total__c = 100;
            	 invs[idx].Payment_Status__c='S9';
            }
        }
        update invs; 
        
    }
    
    @isTest
    public static void test_runSblInvoicesChangesBatch(){
        
        List<String> ids = new List<String>();
        Map<String, BillingResults.InvoiceSummary> resultMap = new Map<String, BillingResults.InvoiceSummary>();
        Integer idx = 0;
        
        for(Invoice__c inv: [SELECT Id, Name, Open_Amount__c FROM Invoice__c ORDER BY Name]){
            BillingResults.InvoiceSummary binv = new BillingResults.InvoiceSummary();
            if(idx < nInvoices-2){
                binv.payment_status = String.valueOf(idx);
                binv.open_amount = 10;
            }
            resultMap.put(''+inv.Id, binv);
            idx++;
        } 
                                
        Test.startTest();
        SblInvoicesChangesBatch sblICB = new SblInvoicesChangesBatch(resultMap);
        Id batchId = Database.executeBatch(sblICB);
        Test.stopTest();
        
    }

}