/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Digital signature process - Callout wrappers
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Gennaro Casola   <gcasola@deloitte.it>
* @created        2019-11-10
* @systemLayer    Domain
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  SPIII-3499, SPIII-3582, SPIII-3431, SPIII-3432 - Removed document download logic
*				  and improved performance with a new Wrapper structure
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2020-09-15
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class NamirialJSONWrappers{
    
    public class UploadPrepareAndSendRequest{
		public SendEnvelopeDescription send_envelope_description{get;set;}
        public SspFileUploadTemporary sspfile_uploadtemporary{get;set;}
        
        public UploadPrepareAndSendRequest(){
            send_envelope_description = new SendEnvelopeDescription();
            sspfile_uploadtemporary = new SspFileUploadTemporary();
        }
	}
	public class SendEnvelopeDescription{
		public Integer before_expiration_day_amount{get;set;}
		public Integer first_reminder_day_amount{get;set;}
		public Integer recurrent_reminder_day_amount{get;set;}
		public Boolean enable_reminders{get;set;}
		public Integer days_until_expire{get;set;}
		public String email_body{get;set;}
		public String status_update_callback_url{get;set;}
		public String email_subject{get;set;}
		public List<Steps> steps{get;set;}
		public String name{get;set;}
        public String displayed_email_sender{get;set;}
        public String callback_url{get;set;}
        //public AddFormFields add_form_fields{get;set;}
        
        public SendEnvelopeDescription(){
            steps = new List<Steps>();
            //add_form_fields = new AddFormFields();
        }
	}
	public class Steps{
		public String recipient_type{get;set;}
		//public List<DocumentOptions> document_options{get;set;}
		//public WorkstepConfiguration workstep_configuration{get;set;}
		public Boolean use_default_agreements{get;set;}
		public List<Recipients> recipients{get;set;}
		//public AuditingToolsConfiguration auditing_tools_configuration{get;set;}
		public Integer order_index{get;set;}
        
        public Steps(){
            //document_options = new List<DocumentOptions>();
            //workstep_configuration = new WorkstepConfiguration();
            recipients = new List<Recipients>();
            //auditing_tools_configuration = new AuditingToolsConfiguration();
        }
	}
	public class Recipients{
		//public Pkcs7SignerData pkcs7_signer_data{get;set;}
		//public SwissComCertificateData swiss_com_certificate_data{get;set;}
		//public RemoteCertificateData remote_certificate_data{get;set;}
		//public DisposableCertificateData disposable_certificate_data{get;set;}
		//public OtpData otp_data{get;set;}
		//public List<AuthenticationMethods> authentication_methods{get;set;}
		public Boolean skip_external_data_validation{get;set;}
		public Boolean disable_email{get;set;}
		public String email_body_extra{get;set;}
		public Boolean add_android_app_link{get;set;}
		public String language_code{get;set;}
		public Boolean add_ios_app_link{get;set;}
		public String last_name{get;set;}
		public Boolean add_windows_app_link{get;set;}
		public String first_name{get;set;}
		public Boolean allow_delegation{get;set;}
		public String email{get;set;}
        
        /*public Recipients(){
            pkcs7_signer_data = new Pkcs7SignerData();
            swiss_com_certificate_data = new SwissComCertificateData();
            remote_certificate_data = new RemoteCertificateData();
            disposable_certificate_data = new DisposableCertificateData();
            otp_data = new OtpData();
            authentication_methods = new List<AuthenticationMethods>();
        }*/
	}
	public class SspFileUploadTemporary{
        public File file{get;set;}
        
        public SspFileUploadTemporary(){
            file = new File();
        }
    }
	public class File{
        public String data{get;set;}
        public String name{get;set;}
    }

    public class UploadPrepareAndSendResponse{
		public String envelope_id{get;set;}
        public String workstep_redirection_url{get;set;}
	}
    
    public class DownloadCompletedDocumentRequest {
		public EnvelopeSendResult envelope_download_completed_document {get;set;} 

		public DownloadCompletedDocumentRequest(){
            envelope_download_completed_document = new EnvelopeSendResult();
        }

	} 
    public class DownloadCompletedDocumentResponse {
		public EnvelopeGetResult envelope_get_result {get;set;} 
		public SspFileUploadTemporary envelope_download_completed_document_result {get;set;} 

		public DownloadCompletedDocumentResponse(){
			envelope_get_result = new EnvelopeGetResult();
			envelope_download_completed_document_result = new SspFileUploadTemporary();
        }

	}

    public class GetEnvelopeByIdRequest{
		public EnvelopGet envelope_get{get;set;}
        
        public GetEnvelopeByIdRequest(){
            envelope_get = new EnvelopGet();
        }
	}
	public class EnvelopGet{
		public String envelope_id{get;set;}        
	}

    public class GetEnvelopeByIdResponse{
        public EnvelopeGetResult envelope_get_result{get;set;}
        public EnvelopeSendResult envelope_send_result{get;set;}
        
        public GetEnvelopeByIdResponse(){
            envelope_get_result = new EnvelopeGetResult();
            envelope_send_result = new EnvelopeSendResult();
        }
	}
	public class EnvelopeSendResult{
		public String envelope_id{get;set;}
	}
	public class EnvelopeGetResult{
		public List<Bulks> bulks{get;set;}
		public List<Documents> documents{get;set;}
		public String id{get;set;}
		public String expiration_date{get;set;}
		//public String bulk{get;set;}
		public String send_date{get;set;}
		public BasicOptions basic_options{get;set;}
		public String status{get;set;}
        
        public EnvelopeGetResult(){
            bulks = new List<Bulks>();
            documents = new List<Documents>();
            basic_options = new BasicOptions();
        }
	}
	public class BasicOptions{
		public String name{get;set;}
		public String email_body{get;set;}
		public Boolean enable_reminders{get;set;}
		public String email_subject{get;set;}
	}
	public class Documents{
		public List<PageSizesInPoints> page_sizes_in_points{get;set;}
		public Integer doc_ref_number{get;set;}
		public String file_name{get;set;}
		public List<String> form_fields{get;set;}
        
        public Documents(){
            page_sizes_in_points = new List<PageSizesInPoints>();
            form_fields = new List<String>();
        }
	}
	public class PageSizesInPoints{
		public Decimal height{get;set;}
		public Decimal width{get;set;}
	}
	public class Bulks{
		public List<StepsZ> steps{get;set;}
		public List<FinishedDocuments> finished_documents{get;set;}
		public String log_document_id{get;set;}
		public String email{get;set;}
		public String log_xml_document_id{get;set;}
		public String status{get;set;}
        
        public Bulks(){
            steps = new List<StepsZ>();
            finished_documents = new List<FinishedDocuments>();
        }
	}
    public class FinishedDocuments{
		public Integer doc_ref_number{get;set;}
		public List<SignedSignatureFields> signed_signature_fields{get;set;}
		public List<String> attachments{get;set;}
		public String file_name{get;set;}
		public String log_document_id{get;set;}
		public List<String> form_fields{get;set;}
		public String flow_document_id{get;set;}
        
        public FinishedDocuments(){
            signed_signature_fields = new List<SignedSignatureFields>();
            attachments = new List<String>();
            form_fields = new List<String>();
        }
	}
    public class SignedSignatureFields{
		public String name{get;set;}
	}
	public class StepsZ{
		public String id {get;set;} 
		public String first_name {get;set;} 
		public String last_name {get;set;} 
		public Integer order_index {get;set;} 
		public String email {get;set;} 
		public String language_code {get;set;} 
		public String status {get;set;} 
		public String status_reason {get;set;} 
		public String recipient_type {get;set;} 
		public String signed_date {get;set;} 
		public String opened_date {get;set;} 
		public String workstep_redirection_url {get;set;} 
		public Boolean is_parallel {get;set;} 
		public WorkstepConfigurationZ workstep_configuration {get;set;}
        
        public StepsZ(){
            workstep_configuration = new WorkstepConfigurationZ();
        }
	}
	public class WorkstepConfigurationZ{
		public String workstep_label {get;set;} 
		public Integer small_text_zoom_factor_percent {get;set;} 
		public FinishActionZ finish_action {get;set;} 
		public ReceiverInformationZ receiver_information {get;set;} 
		public SenderInformation sender_information {get;set;} 
		public List<TransactionCodeConfigurations> transaction_code_configurations {get;set;} 
		public List<SignatureConfigurationsZ> signature_configurations {get;set;} 
		public ResourceUrisZ viewer_preferences {get;set;} 
		public String additional_client_workstep_information {get;set;} 
		public PolicyZ policy {get;set;} 
        
        public WorkstepConfigurationZ(){
            finish_action = new FinishActionZ();
            receiver_information = new ReceiverInformationZ();
            sender_information = new SenderInformation();
            transaction_code_configurations = new List<TransactionCodeConfigurations>();
            signature_configurations = new List<SignatureConfigurationsZ>();
            viewer_preferences = new ResourceUrisZ();
            policy = new PolicyZ();
        }
	}
	public class FinishActionZ{
		public List<ServerActions> server_actions {get;set;} 
		public List<ResourceUrisZ> client_actions {get;set;}
        
        public FinishActionZ(){
            server_actions = new List<ServerActions>();
            client_actions = new List<ResourceUrisZ>();
        }
	}
	public class ServerActions{
		public String action{get;set;}
		public Boolean call_synchronous{get;set;}
	}
	public class ResourceUrisZ{}
    public class ReceiverInformationZ{
		public UserInformation user_information {get;set;} 
		//public List<ResourceUrisZ> transaction_code_push_plugin_data {get;set;}
        public ReceiverInformationZ(){
            user_information = new UserInformation();
        }
	}
	public class SenderInformation{
		public UserInformation user_information{get;set;}
        
        public SenderInformation(){
            user_information = new UserInformation();
        }
	}
	public class UserInformation{
		public String e_mail{get;set;}
		public String first_name{get;set;}
		public String last_name{get;set;}
	}
	public class TransactionCodeConfigurations{
		public String id{get;set;}
		public List<Texts> texts{get;set;}
		public String hash_algorithm_identifier{get;set;}
        
        public TransactionCodeConfigurations(){
            texts= new List<Texts>();
        }
	}
	public class Texts{
		public String value{get;set;}
		public String language{get;set;}
	}
    public class SignatureConfigurationsZ{
		public String spc_id {get;set;} 
		public PdfSignaturePropertiesZ pdf_signature_properties {get;set;} 
		public PdfSignatureCryptographicData pdf_signature_cryptographic_data {get;set;}
        
        public SignatureConfigurationsZ(){
            pdf_signature_properties = new PdfSignaturePropertiesZ();
            pdf_signature_cryptographic_data = new PdfSignatureCryptographicData();
        }
	}
	public class PdfSignaturePropertiesZ{
		public Boolean pdf_a_conformant {get;set;} 
		public Boolean p_ad_es_part4_compliant {get;set;} 
		public Boolean include_signing_certificate_chain {get;set;} 
		public String signing_certificate_revocation_information_include_mode {get;set;} 
		public SignatureTimestampDataZ signature_timestamp_data {get;set;}
        
        public PdfSignaturePropertiesZ(){
            signature_timestamp_data = new SignatureTimestampDataZ();
        }
	}
	public class SignatureTimestampDataZ {
		public String uri {get;set;} 
		public String username {get;set;} 
		public String password {get;set;} 
		public String signature_hash_algorithm {get;set;}
	}
	public class PdfSignatureCryptographicData{
		public SigningCertificateDescriptor signing_certificate_descriptor{get;set;}
		public String signature_hash_algorithm{get;set;}
        
        public PdfSignatureCryptographicData(){
            signing_certificate_descriptor = new SigningCertificateDescriptor();
        }
	}
	public class SigningCertificateDescriptor{
		//public String type{get;set;}
		public String csp{get;set;}
		public String identifier{get;set;}
	}
    public class PolicyZ{
		public GeneralPolicies general_policies {get;set;} 
		public WorkstepTasksZ workstep_tasks {get;set;}
        
        public PolicyZ(){
            general_policies = new GeneralPolicies();
            workstep_tasks = new WorkstepTasksZ();
        }
    }
    public class WorkstepTasksZ{
		public Integer picture_annotation_min_resolution {get;set;} 
		public Integer picture_annotation_max_resolution {get;set;} 
		public String picture_annotation_color_depth {get;set;} 
		public String sequence_mode {get;set;} 
		public String position_units {get;set;} 
		public String reference_corner {get;set;} 
		public List<TasksZ> tasks {get;set;}
        
        public WorkstepTasksZ(){
            tasks = new List<TasksZ>();
        }
	}
	public class TasksZ{
		public Integer position_page {get;set;} 
		public Position position {get;set;} 
		public Size size {get;set;} 
		public List<Parameters> additional_parameters {get;set;} 
		public List<AllowedSignatureTypesY> allowed_signature_types {get;set;} 
		public Boolean use_timestamp {get;set;} 
		public Boolean is_required {get;set;} 
		public String id {get;set;} 
		public String display_name {get;set;} 
		public Integer doc_ref_number {get;set;} 
		public String discriminator_type {get;set;}
        public List<Forms> forms {get;set;}
        
        public TasksZ(){
            position = new Position();
            size = new Size();
            additional_parameters = new List<Parameters>();
            allowed_signature_types = new List<AllowedSignatureTypesY>();
            forms = new List<Forms>();
        }
	}
	public class Position{
		public Decimal position_y{get;set;}
		public Decimal position_x{get;set;}
	}
	public class Size{
		public Decimal height{get;set;}
		public Decimal width{get;set;}
	}
	public class Parameters{
		public String value{get;set;}
		public String key{get;set;}
	}
	public class AllowedSignatureTypesY {
		public String allowed_capturing_method {get;set;} 
		public StampImprintConfigurationZ stamp_imprint_configuration {get;set;} 
		public String id {get;set;} 
		public String discriminator_type {get;set;} 
		public Boolean preferred {get;set;}
        
        public AllowedSignatureTypesY(){
            stamp_imprint_configuration = new StampImprintConfigurationZ();
        }
    }
    public class StampImprintConfigurationZ {
		public Boolean display_extra_information {get;set;} 
		public Boolean display_email {get;set;} 
		public Boolean display_ip {get;set;} 
		public Boolean display_name {get;set;} 
		public Boolean display_signature_date {get;set;} 
		public String font_family {get;set;} 
		public Decimal font_size {get;set;}
	}
	public class Forms{
		public String form_type{get;set;}
		public String position_page{get;set;}
		public Integer doc_ref_number{get;set;}
		public String id{get;set;}
		public Boolean is_required{get;set;}
		public String radio_button_group_id{get;set;}
		public String required_eval_policy{get;set;}
		public FormFieldValidation form_field_validation{get;set;}
		public Position position{get;set;}
		public String discriminator_type{get;set;}
		public Size size{get;set;}
		public List<A1> a1{get;set;}
        
        public Forms(){
            form_field_validation = new FormFieldValidation();
            position = new Position();
            size = new Size();
            a1 = new List<A1>();
        }
	}
	public class FormFieldValidation{
		public String format{get;set;}
		public Range range{get;set;}
		public String discriminator_type{get;set;}
		//public String type{get;set;}
		public String phone_type{get;set;}
        
        public FormFieldValidation(){
            range = new Range();
        }
	}
	public class Range{
		//public String from{get;set;}
		public String to{get;set;}
	}
	public class A1{
		public String text_align{get;set;}
		public String font_name{get;set;}
		public Decimal font_size{get;set;}
		public Boolean bold{get;set;}
		public Boolean is_comb{get;set;}
		public FormFieldValidation form_field_validation{get;set;}
		public String export_value{get;set;}
		public String form_type{get;set;}
		public Boolean is_checked{get;set;}
		//public String type{get;set;}
		public Boolean is_select_unison{get;set;}
		public String discriminator_type{get;set;}
		public Boolean is_file_select{get;set;}
		public Integer page{get;set;}
		public Boolean is_multiline{get;set;}
		public Decimal height{get;set;}
		public Boolean is_password{get;set;}
		public Decimal width{get;set;}
		public Boolean is_scroll_allowed{get;set;}
		public Decimal y{get;set;}
		public Boolean italic{get;set;}
		public Decimal x{get;set;}
		public Integer max_length{get;set;}
		public Boolean required{get;set;}
		public String text_color{get;set;}
		public Boolean read_only{get;set;}
		public String value{get;set;}
		public String name{get;set;}
	}
	public class GeneralPolicies{
		public Boolean allow_append_task_to_workstep{get;set;}
		public Boolean allow_finish_workstep{get;set;}
		public Boolean allow_print_document{get;set;}
		public Boolean allow_reject_workstep{get;set;}
		public Boolean allow_email_document{get;set;}
		public Boolean allow_reject_workstep_delegation{get;set;}
		public Boolean allow_append_file_to_workstep{get;set;}
		public Boolean allow_undo_last_action{get;set;}
		public Boolean allow_rotating_pages{get;set;}
		public Boolean allow_colorize_pdf_forms{get;set;}
		public Boolean allow_save_audit_trail{get;set;}
		public Boolean allow_adhoc_pdf_attachments{get;set;}
		public Boolean allow_save_document{get;set;}
		public Boolean allow_adhoc_signatures{get;set;}
		public Boolean allow_adhoc_picture_annotations{get;set;}
		public Boolean allow_adhoc_typewriter_annotations{get;set;}
		public Boolean allow_adhoc_pdf_page_appending{get;set;}
		public Boolean allow_adhoc_free_hand_annotations{get;set;}
		public Boolean allow_reload_of_finished_workstep{get;set;}
		public Boolean allow_adhoc_stampings{get;set;}
	}
}