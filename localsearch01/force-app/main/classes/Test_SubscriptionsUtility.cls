@isTest
public class Test_SubscriptionsUtility {
    @isTest
    	public static void Test_activateSubscriptions()
        {            
			List<Id> ids=new List<Id>();
            try {
            	TestSetupUtilClass.createContracts(1);                
            	Contract cn = [SELECT Id FROM Contract limit 1];
	            ids.add(cn.Id);
                
                SubscriptionsUtility.activateSubscriptions(ids);
            }catch(Exception e){	                            
                System.Assert(False,e.getMessage());
            }
        }
}