//SI-156 / SI-162
public with sharing class SEL_PermissionSetLicenseAssign {
	public static Map<Id, PermissionSetLicenseAssign> getPSLAssignByUserId(Set<Id> userIds)
    {
        return new Map<Id, PermissionSetLicenseAssign>(
            [
                SELECT Id, PermissionSetLicenseId, AssigneeId 
                FROM PermissionSetLicenseAssign
                WHERE AssigneeId IN :userIds 
            ]
        );
    }
}
