/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Laudato Vincenzo <vlaudato@deloitte.it>
 * Date        : 15-04-2020
 * Sprint      : 
 * Work item   : 
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   :
 */

public class ReplaceQuotelineFields {
    @InvocableMethod(label='ReplaceQLFields' description='ReplaceQLFields')
    public static void replaceQLFields(List<Id> recordIds){
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Contract_Ref_Id__c, Ad_Context_Allocation__c, Old_Ad_Context_Allocation__c,Campaign_Id__c FROM SBQQ__QuoteLine__c WHERE Id IN :recordIds];
        if(!quoteLines.isEmpty()){
            for(SBQQ__QuoteLine__c currentQuoteLine : quoteLines){
                if(currentQuoteLine.Contract_Ref_Id__c != null) currentQuoteLine.Contract_Ref_Id__c = null;
                if(currentQuoteLine.Ad_Context_Allocation__c == currentQuoteLine.Old_Ad_Context_Allocation__c) currentQuoteLine.Ad_Context_Allocation__c = null;
                if(currentQuoteLine.Campaign_Id__c != null) currentQuoteLine.Campaign_Id__c = null; 
            }
            QuoteLineTriggerHandler.disableTrigger = true;
            update quoteLines;
            QuoteLineTriggerHandler.disableTrigger = false;
        }
        
    }
}