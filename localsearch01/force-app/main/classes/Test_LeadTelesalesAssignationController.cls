@isTest
public class Test_LeadTelesalesAssignationController {

    @TestSetup
    static void setup(){        
        User dmc = Test_DataFactory.createUsers(ConstantsUtil.SALESPROFILE_LABEL, 1)[0];
        dmc.Email = 'testDMC1=@email.org';
        insert dmc;
        
        PermissionSet digital_marketing_consultant = [select id from PermissionSet where Name =: ConstantsUtil.DMCPERMISSIONSET_DEVNAME][0];
        PermissionSetAssignment dmcAssignment = new PermissionSetAssignment();
        dmcAssignment.PermissionSetId = digital_marketing_consultant.Id;
        dmcAssignment.AssigneeId = dmc.Id;
        insert dmcAssignment;
    }
    
    @isTest
    static void testassignLeadToTelesalesQueue(){
        User dmc = [select id from User where Email = 'testDMC1=@email.org'];
        Test.startTest();
        System.runAs(dmc){
            Lead testLead = Test_DataFactory.createLeads('testLeadName', 'testLastName', 1).get(0);
        	insert testLead;
        	LeadTelesalesAssignationController.ResponseWrapper result = LeadTelesalesAssignationController.assignLeadToTelesalesQueue(testLead.Id);
        	System.assertEquals(true,result.oldOwnerIsDMC);
        }
        Test.stopTest();
    }
    
}