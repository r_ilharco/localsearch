@isTest
public class ResetApiTokenScheduleTest implements HttpCalloutMock{
    
    public HttpResponse respond(HTTPRequest req){
        String body = '{"test": "test"}';
        HttpResponse resp = new HttpResponse();
        resp.setStatus('OK');
        resp.setStatusCode(Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS));
        resp.setBody(body);
        return resp;
    }

    @isTest
    public static void testResetApiTokenSchedule(){
        try{   
            Test.setMock(HttpCalloutMock.class, new ResetApiTokenScheduleTest());
            Test.startTest();
            
            ResetApiToken_Schedule ras = new ResetApiToken_Schedule();
            ras.execute(null);
            
            Test.stopTest();
            
            }catch(Exception e){                                
           //System.Assert(false, e.getMessage());
        }
    }
}