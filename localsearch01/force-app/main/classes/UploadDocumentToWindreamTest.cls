@isTest
public class UploadDocumentToWindreamTest implements HttpCalloutMock{
    private String statusCode;
    
    public UploadDocumentToWindreamTest(String status){
        this.statusCode = status;
    }
    
    public HttpResponse respond(HTTPRequest req){
        String body = '{"access_token": "test","token_type": "bearer","expires_in": 3600}';
        HttpResponse resp = new HttpResponse();
        resp.setStatus('OK');        
        resp.setStatusCode(Integer.Valueof(statusCode));
        resp.setBody(body);
        return resp;
    }
    
    @testSetup static void setupData()
    {
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Opportunity Management,Quote Management';
		insert byPassFlow;
        
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        
        Map<String, Id> usersByUsernameMap = new Map<String,Id>();
        UserTriggerHandler.disableTrigger = true;
        Test_DataFactory.namirial_insertFutureUsers();
        UserTriggerHandler.disableTrigger = false;
        Map<Id, User> users = new Map<Id,User>([SELECT Id, Username FROM User]);
        if(users != NULL && users.size() > 0)
        {
            for(User u : users.values())
            {
            	usersByUsernameMap.put(u.Username, u.Id);
            }
        }
        
        Test.setMock(HttpCalloutMock.class, new Test_AddressValidationMock(ConstantsUtil.ADDRESS_VALIDATION_OK) );
        
        List<Account> accounts = Test_DataFactory.createAccounts('AccountTest01',1);
        accounts[0].Customer_Number__c = '12345';
 		insert accounts;
        List<Contact> contacts = Test_DataFactory.createContacts(accounts[0],1);
        contacts[0].FirstName = 'FirstName';
        insert contacts;
        
        List<Opportunity> opps = Test_DataFactory.createOpportunities('OppName01','Draft',accounts[0].Id,1);
        opps[0].Name = 'OppName01';
        insert opps;
        List<SBQQ__Quote__c> quotes = Test_DataFactory.createQuotes('New', opps[0], 1);
        quotes[0].SBQQ__SalesRep__c = usersByUsernameMap.get('dmctester@test.com');
        quotes[0].Filtered_Primary_Contact__c = contacts[0].Id;
        quotes[0].SBQQ__PrimaryContact__c = contacts[0].Id;
        insert quotes;
        List<Document> documents = Test_DataFactory.createDocuments('Q-45445-20191126-0945.pdf',
                                                                    'provaprovaprova',
                                                                    'application/pdf',
                                                                    'CABJBBCGAd4f957419a4f4936898439621d9cf51e',
                                                                   	true,
                                                                   	UserInfo.getUserId(),
                                                                    1);
        insert documents;
        
        SBQQ__QuoteDocument__c quoteDocument =  Test_DataFactory.generateQuoteDocument(quotes[0].Id);
        quoteDocument.SBQQ__DocumentId__c = documents[0].Id;
        quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED;
        insert quoteDocument;
    }
     @isTest
	public static void testUploadDocumentSent()
    {
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		quoteDocument.SBQQ__SignatureStatus__c = ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SENT;
        update quoteDocument;
        Document doc = [SELECT Id, Name, Body FROM Document WHERE Id = :quoteDocument.SBQQ__DocumentId__c];
        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
		ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = quoteDocument.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        insert cdl;
        
 		UploadDocumentToWindream.getRequestObject(quoteDocument.Id); 
        
    }
       @isTest
	public static void testUploadDocumentNoDocument()
    {
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
 		UploadDocumentToWindream.getRequestObject(quoteDocument.Id); 
        
    }
       @isTest
	public static void testUploadDocumentNoDocumentv2()
    {
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c,WindreamUploadStatus__c,Url__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
 		quoteDocument.URL__c ='https://windream-test';
        quoteDocument.WindreamUploadStatus__c = ConstantsUtil.WINDREAM_UPLOAD_SUCCESS_STATUS;
        update quoteDocument;
        UploadDocumentToWindream.getRequestObject(quoteDocument.Id); 
         
    }
    @isTest
	public static void testUploadDocument()
    {
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		Document doc = [SELECT Id, Name, Body FROM Document WHERE Id = :quoteDocument.SBQQ__DocumentId__c];
        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = quoteDocument.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        insert cdl;
        
        Try{

            Account acc = [SELECT Id FROM Account LIMIT 1];
            
            Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
            
            opp.SBQQ__AmendedContract__c = null;           
            update opp;
            
            SBQQ__Quote__c quot = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
            
            Contract contrct = new Contract (AccountId = acc.Id, SBQQ__Quote__c = quot.Id, StartDate = Date.today(), Status ='Draft', CustomerSignedDate=Date.today());           
            insert contrct;
            System.Debug('Contract '+ contrct); 
            
            SBQQ__QuoteDocument__c quoteDoc = [SELECT Id FROM SBQQ__QuoteDocument__c LIMIT 1];
        	
        	//ListFolder flder = [Select Id from Folder Where Name = 'Quotes'];
            //Document doc = [SELECT Id FROM Document LIMIT 1];
            
            List<String> quoteDocumntRecordIds = new List<String>();
            quoteDocumntRecordIds.add(quoteDoc.Id);                
                            	
            Test.startTest();
            UploadDocumentToWindream.uploadDocumnetCalledByProcessBuilder(quoteDocumntRecordIds);                        
                     
            Test.setMock(HttpCalloutMock.class, new UploadDocumentToWindreamTest(ConstantsUtil.STATUSCODE_SUCCESS));
            //UtilityToken.accessTokenInformation token = UtilityToken.getAccessToken(ConstantsUtil.SERVICE_WINDREAM_TOKEN);
            UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();           
        	
            UploadDocumentToWindream.QuoteDocInfo quoteDocInfo = new UploadDocumentToWindream.QuoteDocInfo(11, 1234, 'SalesForce', Date.today(), 'pdf', doc.Body);       
            UploadDocumentToWindream.callHttpRequest(quoteDocInfo.getJson(), quoteDoc.Id, quoteDocInfo, contrct, quoteDoc, null);
            //UploadDocumentToWindream.getUploadedDocUrl(quoteDoc.Id, null, token, quoteDocInfo, quoteDoc, null);
            //getUploadedDocUrl(Id quoteDocumntRecordId, String endPoint, UtilityToken.masheryTokenInfo token, QuoteDocInfo quoteDocInfo, SBQQ__QuoteDocument__c quoteDoc, List<Log__c> integrationLogs)
            Test.stopTest();
                                  
            contrct.SBQQ__Quote__c = null;
            update contrct;             
            UploadDocumentToWindream.getRequestObject(quoteDoc.Id); 
            
            doc.Name = 'test2.pdf';
            update doc;
            UploadDocumentToWindream.getRequestObject(quoteDoc.Id); 
            
            UploadDocumentToWindream.getRequestObject(null); 
                  
            
            
        }catch(Exception e){	                            
           System.Assert(true,e.getMessage());
        }
    }
    
    @isTest
	public static void testUploadDocument2()
    {
        User u = [SELECT Id, Username FROM User WHERE Username = 'dmctester@test.com'];
        SBQQ__QuoteDocument__c quoteDocument = [SELECT Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name, SBQQ__DocumentId__c FROM SBQQ__QuoteDocument__c WHERE SBQQ__Quote__r.SBQQ__Opportunity2__r.Name = 'OppName01' LIMIT 1];
		Document doc = [SELECT Id, Name, Body FROM Document WHERE Id = :quoteDocument.SBQQ__DocumentId__c];

        ContentVersion cv = new ContentVersion();
        cv.Title = doc.Name; 
        cv.PathOnClient = doc.Name;
        cv.VersionData = doc.Body;
        cv.IsMajorVersion = true;

		insert cv;
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = quoteDocument.Id;
        cdl.ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1].ContentDocumentId;
        cdl.shareType = 'V';
        
        insert cdl;
        
        Try{

            Account acc = [SELECT Id FROM Account LIMIT 1];
            
            Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
            
            opp.SBQQ__AmendedContract__c = null;           
            update opp;
            
            SBQQ__Quote__c quot = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
            
            Contract contrct = new Contract (AccountId = acc.Id, SBQQ__Quote__c = quot.Id, StartDate = Date.today(), Status ='Draft', CustomerSignedDate=Date.today());           
            insert contrct;
            System.Debug('Contract '+ contrct); 
            
            SBQQ__QuoteDocument__c quoteDoc = [SELECT Id FROM SBQQ__QuoteDocument__c LIMIT 1];
        	
        	//ListFolder flder = [Select Id from Folder Where Name = 'Quotes'];
            //Document doc = [SELECT Id FROM Document LIMIT 1];
            
            List<String> quoteDocumntRecordIds = new List<String>();
            quoteDocumntRecordIds.add(quoteDoc.Id);                
                            	
            Test.startTest();
            UploadDocumentToWindream.uploadDocumnetCalledByProcessBuilder(quoteDocumntRecordIds);                        
                     
            Test.setMock(HttpCalloutMock.class, new UploadDocumentToWindreamTest(ConstantsUtil.STATUSCODE_SUCCESS));
            //UtilityToken.accessTokenInformation token = UtilityToken.getAccessToken(ConstantsUtil.SERVICE_WINDREAM_TOKEN);
            UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();           
        	
            UploadDocumentToWindream.QuoteDocInfo quoteDocInfo = new UploadDocumentToWindream.QuoteDocInfo(11, 1234, 'SalesForce', Date.today(), 'pdf', doc.Body);       
            //UploadDocumentToWindream.callHttpRequest(quoteDocInfo.getJson(), quoteDoc.Id, quoteDocInfo, contrct, quoteDoc, null);
            UploadDocumentToWindream.getUploadedDocUrl(quoteDoc.Id, null, token, quoteDocInfo, quoteDoc, null);
            //getUploadedDocUrl(Id quoteDocumntRecordId, String endPoint, UtilityToken.masheryTokenInfo token, QuoteDocInfo quoteDocInfo, SBQQ__QuoteDocument__c quoteDoc, List<Log__c> integrationLogs)
            Test.stopTest();
                                  
            contrct.SBQQ__Quote__c = null;
            update contrct;             
            UploadDocumentToWindream.getRequestObject(quoteDoc.Id); 
            
            doc.Name = 'test2.pdf';
            update doc;
            UploadDocumentToWindream.getRequestObject(quoteDoc.Id); 
            
            UploadDocumentToWindream.getRequestObject(null); 
                  
            
            
        }catch(Exception e){	                            
           System.Assert(true,e.getMessage());
        }
    }
}