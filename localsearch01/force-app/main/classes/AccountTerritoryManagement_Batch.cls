/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*  Check when new account is assigned to territory 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Mazzarella Mara <mamazzarella@deloitte.it>
* @created        06-12-2019
* @systemLayer    Invocation 
* @TestClass      AccountTerritoryManagement_BatchTest

* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*               
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
* @changes        Reference to fields listed in SPIII 1057 removed
* @modifiedby     Clarissa De Feo <cdefeo@deloitte.it>
* 2020-07-03              
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class AccountTerritoryManagement_Batch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    global List<String> staticValues = new List<String>();
    
    global AccountTerritoryManagement_Batch(List<String> val){
        this.staticValues = val;
    }
    
    global void execute(SchedulableContext sc) {
        AccountTerritoryManagement_Batch accToTerritory = new AccountTerritoryManagement_Batch(this.staticValues); 
        Database.executeBatch(accToTerritory, 200);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        system.debug('AccountTerritoryManagement_Batch');
        return Database.getQueryLocator(
            'SELECT ObjectId, SobjectType, Territory2Id, Territory2.Name, Territory2.ParentTerritory2.Name,'+
            ' Object.Name, SystemModstamp FROM ObjectTerritory2Association where LastModifiedDate >= LAST_N_WEEKS:2 AND Object.Type =\'' + ConstantsUtil.TERRITORY_OBJECT_TYPE + '\' and ObjectId in (select id from Account where cluster_id__c in: staticValues and toProcess__c=false)' );
    }
    global void execute(Database.BatchableContext bc, List<ObjectTerritory2Association> scope){
        System.debug('scope: '+ scope);
        Map <Id, List<String>> territoryAccountMap = new Map <Id, List<String>> ();
        List<Account> accs = new List<Account>();
        Map <Id, Territory2> accTerritoryMap = new  Map <Id, Territory2>();
        Map <Id, Territory2> accRegionMap = new  Map <Id, Territory2>();
        List<Error_Log__c> errors = new List<Error_Log__c>();
        
        List<Territory2> terrs = new  List<Territory2>();
        //Map accID , TerrId
        Map <Id, Id> accDMCTerr = new  Map <Id, Id> ();
        Map <Id, Id> accSMATerr = new  Map <Id, Id> ();
        Map <Id, Id> accSDITerr = new  Map <Id, Id> ();
        List<Id> terrIds = new List<Id>();
        try {
            for (ObjectTerritory2Association u : scope) {
                // check 3rd level territory
                /*     if(u.Territory2.ParentTerritory2.ParentTerritory2 != null){
System.debug('3rd level territory');
accTerritoryMap.put(u.ObjectId, u.Territory2);
terrIds.add(u.Territory2.id);
terrIds.add(u.Territory2.ParentTerritory2.id);
terrIds.add(u.Territory2.ParentTerritory2.ParentTerritory2.id);
accDMCTerr.put(u.ObjectId, u.Territory2.id);
accSMATerr.put(u.ObjectId, u.Territory2.ParentTerritory2.id);
accSDITerr.put(u.ObjectId, u.Territory2.ParentTerritory2.ParentTerritory2.id);
accRegionMap.put(u.ObjectId, u.Territory2.ParentTerritory2.ParentTerritory2);

}*/
                // check 2rd level territory
                //else 
                if(!accTerritoryMap.containsKey(u.ObjectId) && u.Territory2.ParentTerritory2 != null){
                    System.debug('2rd level territory');
                    accTerritoryMap.put(u.ObjectId, u.Territory2);
                    terrIds.add(u.Territory2.id);
                    terrIds.add(u.Territory2.ParentTerritory2.id);
                    accDMCTerr.put(u.ObjectId, u.Territory2.id);
                    accSMATerr.put(u.ObjectId, u.Territory2.id);
                    accSDITerr.put(u.ObjectId, u.Territory2.ParentTerritory2.id);
                    accRegionMap.put(u.ObjectId, u.Territory2.ParentTerritory2);
                }    
                else if(!accTerritoryMap.containsKey(u.ObjectId)){
                    System.debug('1rd level territory');
                    accRegionMap.put(u.ObjectId, u.Territory2);
                    terrIds.add(u.Territory2.id);
                    accTerritoryMap.put(u.ObjectId, u.Territory2);
                    accSDITerr.put(u.ObjectId, u.Territory2.id);
                }    
            }      
            if(!accTerritoryMap.isEmpty()){
                Set<Id> sambaUserId = new Set<Id>();
                for(PermissionSetAssignment u: [select Id, Assignee.name, Assignee.id from PermissionSetAssignment where PermissionSet.name = 'Samba_Migration']){
                    sambaUserId.add(u.Assignee.Id);
                }
                //OAVERSANO 20191007 Enhancement #33 -- START
                //Map<Id,List<Id>> userinTerMap = new  Map<Id,List<Id>>();
                Map<Id, Map<String, String>> userinTerMap = new  Map<Id, Map<String, String>>();
                /*for(UserTerritory2Association usinT : [select id, UserId, Territory2Id  from UserTerritory2Association where Territory2Id  in : terrIds*/
                for(UserTerritory2Association usinT : [select id, UserId, Territory2Id, RoleInTerritory2 from UserTerritory2Association where Territory2Id  in : terrIds
                                                       order by SystemModstamp DESC]){
                                                           if(!userinTerMap.containsKey(usinT.Territory2Id )){
                                                               userinTerMap.put(usinT.Territory2Id, new Map<String, String>{usinT.RoleInTerritory2 => usinT.UserId});                 
                                                           }
                                                           else{
                                                               if(!userinTerMap.get(usinT.Territory2Id).containsKey(usinT.RoleInTerritory2))
                                                               {
                                                                   Map<String, String> idUsTerMap = new Map<String, String>{usinT.RoleInTerritory2 => usinT.UserId};
                                                                       userinTerMap.get(usinT.Territory2Id).putAll(idUsTerMap);  
                                                               }
                                                           }
                                                           /*
if(!userinTerMap.containsKey(usinT.Territory2Id )){
userinTerMap.put(usinT.Territory2Id, new List<Id>{usinT.UserId});                 
}
else{
userinTerMap.get(usinT.Territory2Id).add(usinT.UserId);  
}*/
                                                           //OAVERSANO 20191007 Enhancement #33 -- END
                                                       }
                for(Account a :[select id, Assigned_Territory__c,       DataSource__c,
                                AccountSource, OwnerId,
                                RegionalLeader__c,Sales_manager_substitute__c,RegionalDirector__c
                                from Account where id =:accTerritoryMap.keySet()]){                  
                                    
                                    if(accTerritoryMap.containsKey(a.id)){
                                        a.Assigned_Territory__c = accTerritoryMap.get(a.id).Name;
                                    }
                                    /*system.debug('accRegionMap' + accRegionMap);
                                    if(accRegionMap.containsKey(a.id)){
                                        a.Seller_Region__c = accRegionMap.get(a.id).Name;
                                    }*/
                                    a.Sales_manager_substitute__c= null;
                                    a.RegionalDirector__c= null;
                                    a.RegionalLeader__c= null;
                                    if(userinTerMap.containsKey(accDMCTerr.get(a.id))){
                                        if(userinTerMap.get(accDMCTerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_DMC)){
                                            if(sambaUserId.contains(a.OwnerId) ){
                                                a.OwnerId = userinTerMap.get(accDMCTerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_DMC);
                                            }
                                        }
                                        //OAVERSANO 20191007 Enhancement #33 -- END
                                    }
                                    if(userinTerMap.containsKey(accSMATerr.get(a.id))){
                                        //OAVERSANO 20191007 Enhancement #33 -- START
                                        //a.RegionalLeader__c = userinTerMap.get(accSMATerr.get(a.id))[0];
                                        if(userinTerMap.get(accSMATerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA)){
                                            a.RegionalLeader__c = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                                            if(!userinTerMap.get(accDMCTerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_DMC)
                                               && sambaUserId.contains(a.OwnerId)){
                                                   a.OwnerId = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA);
                                               }
                                        }
                                        if(userinTerMap.get(accSMATerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP)){
                                            a.Sales_manager_substitute__c = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP);   
                                            if( !userinTerMap.get(accDMCTerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_DMC)
                                               && a.RegionalLeader__c == NULL && sambaUserId.contains(a.OwnerId)){
                                                   a.OwnerId = userinTerMap.get(accSMATerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SMA_DEP);
                                               }
                                        }
                                        //OAVERSANO 20191007 Enhancement #33 -- END
                                    }
                                    if(userinTerMap.containsKey(accSDITerr.get(a.id))){
                                        //OAVERSANO 20191007 Enhancement #33 -- START
                                        //a.RegionalDirector__c = userinTerMap.get(accSDITerr.get(a.id))[0];
                                        if(userinTerMap.get(accSDITerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_SDI)){
                                            a.RegionalDirector__c = userinTerMap.get(accSDITerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);    
                                            if(!userinTerMap.get(accDMCTerr.get(a.id)).containsKey(ConstantsUtil.ROLE_IN_TERRITORY_DMC)
                                               && a.RegionalLeader__c == NULL && a.Sales_manager_substitute__c == NULL && sambaUserId.contains(a.OwnerId)){
                                                   a.OwnerId =  userinTerMap.get(accSDITerr.get(a.id)).get(ConstantsUtil.ROLE_IN_TERRITORY_SDI);   
                                               }    
                                        }
                                        //OAVERSANO 20191007 Enhancement #33 -- END   
                                    }
                                    a.toProcess__c = true;
                                    accs.add(a);
                                }
                if(accs.size()>0){
                    Database.SaveResult[] srList = Database.update(accs, false);
                    for (Database.SaveResult sr : srList) {
                        if (sr.isSuccess()) {
                            System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                        }
                        else {           
                            for(Database.Error err : sr.getErrors()) {
                                errors.add(ErrorHandler.createLogInfo('AccountTerritoryManagement_Batch', '', err.getStatusCode() + ': ' + err.getMessage(), 'Account Id: ' + sr.getId(), sr.getId()));         
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Account fields that affected this error: ' + err.getFields());
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e){
            system.debug('Error: ' + e.getMessage());
        }
    }           
    global void finish(Database.BatchableContext bc){
    }    
}