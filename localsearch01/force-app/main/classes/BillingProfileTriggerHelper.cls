/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-08-2019
 * Sprint      : 1
 * Work item   : 25
 * Testclass   : Test_BillingProfileTrigger
 * Package     : 
 * Description : Trigger Handler on Billing Profile
 */

public class BillingProfileTriggerHelper {
    
    public static void addressValidation(List<Billing_Profile__c> newItems){
 
        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);

        if(Trigger.operationType == System.TriggerOperation.BEFORE_INSERT)
        {
            if(addressValidationMDT.Active__c){
                for(Billing_Profile__c bp: newItems){
               
                    bp.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                    bp.AddressValidationDate__c = NULL;
                    bp.Address_Integration__c = true;
                }
            }
        }else if(Trigger.operationType == System.TriggerOperation.AFTER_INSERT)
        {
            Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(newItems);
            Set<Id> billingProfileIdsToUpdate = new Set<Id>();
            if(addressValidationMDT.Active__c){

                for (Billing_Profile__c bp : newItems) {
                    try{
                        if(!skipAddressValidationFlagsMap.get(bp.Id))
                    		SRV_IntegrationOutbound.asyncValidateAddress(bp.Id);
                    }catch(Exception e)
                    {
                        billingProfileIdsToUpdate.add(bp.Id);
                    }
                }
            }
            
            if(billingProfileIdsToUpdate.size() > 0)
            {
                List<Billing_Profile__c> billingProfilesToUpdate = SEL_BillingProfile.getBillingProfilesById(billingProfileIdsToUpdate).values();
                for(Billing_Profile__c bp : billingProfilesToUpdate)
                {
                    bp.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                    bp.AddressValidationDate__c = NULL;
                	bp.Address_Integration__c = false;
                }
                
                update billingProfilesToUpdate;
            }
        }
    }
    
    public static void addressValidation (Map<Id, Billing_Profile__c> newItems, Map<Id, Billing_Profile__c> oldItems){
        Integration_Config__mdt addressValidationMDT =  SEL_Integration_Config_mdt.getIntegrationConfigByMasterLabel(ConstantsUtil.ADDRESS_VALIDATION);
		Map<Id, Boolean> skipAddressValidationFlagsMap = SRV_IntegrationOutbound.getSkipAddressValidationFlag(newItems.values());
        
        if(addressValidationMDT.Active__c){
            for(Billing_Profile__c bill: newItems.values()){
                Billing_Profile__c oldBill = oldItems.get(bill.Id);
                
                    if((bill.Billing_City__c != null && !bill.Billing_City__c.equalsIgnoreCase(oldBill.Billing_City__c))||
                    (bill.Billing_Street__c != null && !bill.Billing_Street__c.equalsIgnoreCase(oldBill.Billing_Street__c))||
                    (bill.Billing_Country__c != null && !bill.Billing_Country__c.equalsIgnoreCase(oldBill.Billing_Country__c))||
                    (bill.Billing_Postal_Code__c != null && !bill.Billing_Postal_Code__c.equalsIgnoreCase(oldBill.Billing_Postal_Code__c))||
                    (bill.Customer__r.UID__C != null && !bill.Customer__r.UID__C.equalsIgnoreCase(oldBill.Customer__r.UID__C))||
                    (bill.Phone__c != null && !bill.Phone__c.equalsIgnoreCase(oldBill.Phone__c))||
                    (bill.Mail_address__c != null && !bill.Mail_address__c.equalsIgnoreCase(oldBill.Mail_address__c))
                    ){
                        if(!ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED.equals(bill.AddressValidated__c)){
                            bill.AddressValidated__c = ConstantsUtil.ADDRESS_VALIDATED_TOBEVALIDATED;
                            bill.AddressValidationDate__c = NULL;
                        }          
                        System.debug('Address validation update');
                        
                        if(!skipAddressValidationFlagsMap.get(bill.Id)){
                        	SRV_IntegrationOutbound.asyncValidateAddress(bill.Id);
                        	bill.Address_Integration__c = true;
                        }
                }
            }
        }
    }
}