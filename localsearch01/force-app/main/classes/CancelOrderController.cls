public without sharing class CancelOrderController {
    
    @AuraEnabled
    public static ResponseWrapper checkOrderStatus(String orderStatus){
        ResponseWrapper response = new ResponseWrapper();
        if(orderStatus == ConstantsUtil.ORDER_STATUS_ACTIVATED){
            response.isOk = false;
            response.mex = ConstantsUtil.CANT_CANCEL_ACTIVE_ORDER;
            return response;
        }
        else response.isOk = true;
        return response;
    }
    
	@AuraEnabled
    public static ResponseWrapper cancelOrder(String orderId){
        ResponseWrapper response = new ResponseWrapper();
        try{
            Order currentOrder = [  SELECT 
                                            Id
                                            ,Type
                                            ,(SELECT Id, SBQQ__Subscription__c FROM OrderItems) 
                                    FROM    Order 
                                    WHERE   Id = :orderId 
                                    LIMIT   1];

            Set<Id> subIds = new Set<Id>();
            List<SBQQ__Subscription__c> subscriptions = new List<SBQQ__Subscription__c>();
            if(currentOrder.Type == ConstantsUtil.ORDER_TYPE_TERMINATION || currentOrder.Type == ConstantsUtil.ORDER_TYPE_CANCELLATION){
                for(OrderItem oi : currentOrder.OrderItems) subIds.add(oi.SBQQ__Subscription__c);
            }
            
            if(subIds.size() > 0){
                subscriptions = [SELECT Id, In_Termination__c, Termination_Date__c, SBQQ__Contract__c 
                                 FROM   SBQQ__Subscription__c 
                                 WHERE  Id IN :subIds];
            }
            Set<Id> contractIds = new Set<Id>();
            List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
            for(SBQQ__Subscription__c s : subscriptions){
                s.In_Termination__c = false;
                s.Termination_Date__c = null;
                s.Termination_Reason__c = null;
                subscriptionsToUpdate.add(s);
                contractIds.add(s.SBQQ__Contract__c);
            }      
            List<Contract> contracts = [SELECT  Id, In_Termination__c, TerminateDate__c, In_Cancellation__c, Termination_Reason__c, Cancel_Date__c
                                        FROM    Contract
                                        WHERE   Id IN :contractIds];
            List<Contract> contractsToUpdate = new List<Contract>();
            for(Contract c : contracts){
                c.In_Termination__c = false;
                c.TerminateDate__c = null;
                c.In_Cancellation__c = false;
                c.Termination_Reason__c = null;
                c.Cancel_Date__c = null;
                contractsToUpdate.add(c);
            }
            if(subscriptionsToUpdate.size() > 0) update subscriptionsToUpdate;
            if(contractsToUpdate.size() > 0) update contractsToUpdate;
            
            response.isOk = true;
            response.mex = ConstantsUtil.ORDER_CANCELLATION_SUCCESS;
            system.debug('okay: ' + response);
            return response;
        }
        catch(Exception ex){
            system.debug('ex: ' + ex.getMessage() + '/n at line: '+ ex.getStackTraceString());
            response.isOk = false;
            response.mex = ConstantsUtil.ORDER_CANCELLATION_FAILED;
            system.debug('not okay: ' + response);
            return response;
        }        
    }
    
    public class ResponseWrapper{
        @AuraEnabled
        public Boolean isOk {get; set;}
        @AuraEnabled
        public String mex {get; set;}
        
        public responseWrapper(){
            this.isOk = false;
            this.mex = '';
        }
    }
}