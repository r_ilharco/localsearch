global class MigraQuotePrimary implements Database.Batchable<SObject>, Database.Stateful  {
    
    public string batch;
    public Integer num;
    
    public MigraQuotePrimary(string batchName, Integer num){
        this.batch = batchName;
        this.num = num;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) { 
		return Database.getQueryLocator([select id,SBQQ__Primary__c from SBQQ__Quote__c where Quote_Migration_Batch__c =: batch and SBQQ__Primary__c = false limit :num]);
	}   
    
    
	global void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope) {
        List<SBQQ__Quote__c> lstQuote = new List<SBQQ__Quote__c>();
        for(SBQQ__Quote__c q : scope){
            q.SBQQ__Primary__c = true;
            lstQuote.add(q);
        }
        update lstQuote;
  	}
    
    
	global void finish(Database.BatchableContext BC) {
        
    }
}