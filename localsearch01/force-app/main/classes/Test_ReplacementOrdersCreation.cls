@isTest
public class Test_ReplacementOrdersCreation {
	
    @testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
        byPassFlow.Name = Userinfo.getProfileId();
        byPassFlow.FlowNames__c = 'Quote Management,Send Owner Notification';
        insert byPassFlow;
        
        SBQQ.TriggerControl.disable();
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        PlaceTriggerHandler.disableTrigger = true;
        OpportunityTriggerHandler.disableTrigger = true;
        QuoteTriggerHandler.disableTrigger = true;
        QuoteLineTriggerHandler.disableTrigger = true;
        ContractTriggerHandler.disableTrigger = true;
        SubscriptionTriggerHandler.disableTrigger = true;
        
		Swisslist_CPQ.config();
        
        List<Product2> banners = new List<Product2>();
        banners.add(generteFatherProduct_localBANNER());
        banners.add(generteFatherProduct_searchBANNER());
        insert banners;
        List<PricebookEntry> pbesBanner = new List<PricebookEntry>();
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[0].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        pbesBanner.add(new PricebookEntry(IsActive = true,
                                          Pricebook2Id = Test.getStandardPricebookId(),
                                          Product2Id = banners[1].Id,
                                          UnitPrice = 489,
                                          UseStandardPrice= false));
        insert pbesBanner;
        
		List<String> fieldNamesProduct = new List<String>(Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().keySet());
        String queryProducts =' SELECT ' +String.join( fieldNamesProduct, ',' ) +' FROM Product2';
        List<Product2> products = Database.query(queryProducts);
        
        Map<String, Product2> productCodeToProduct = new Map<String, Product2>();
        for(Product2 currentProduct : products){
            currentProduct.Base_term_Renewal_term__c = '12';
            productCodeToProduct.put(currentProduct.ProductCode, currentProduct);
        }
        update products;
        
        Account account = Test_DataFactory.generateAccounts('Test Account Name', 1, false)[0];
        insert account;
        Contact contact = Test_DataFactory.generateContactsForAccount(account.Id, 'Last Name Test', 1)[0];
        insert contact;
        
        List<Place__c> places = new List<Place__c>();
        places.add(Test_DataFactory.generatePlaces(account.Id, 'Place Name Test 1'));
        places.add(Test_DataFactory.generatePlaces(account.Id, 'Place Name Test 2'));
        insert places;
        
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 1', account.Id));
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 2', account.Id));
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 3', account.Id));
        opportunities.add(Test_DataFactory.generateOpportunity('Test Opportunity Name 4', account.Id));
        insert opportunities;
        
        List<String> fieldNamesQuote = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Quote__c').getDescribe().fields.getMap().keySet());
        String queryQuotes = 'SELECT ' +String.join( fieldNamesQuote, ',' ) +' FROM SBQQ__Quote__c';
        List<SBQQ__Quote__c> quotes = Database.query(queryQuotes);
                
        Map<Id, Id> product2Pbe = new Map<Id, Id>();
        List<PricebookEntry> pbes = [SELECT Id, Product2Id FROM PricebookEntry];
        for(PricebookEntry pbe : pbes) product2Pbe.put(pbe.Product2Id, pbe.Id);
        
        SBQQ__Quote__c quote1 = quotes[0];
        SBQQ__Quote__c quote2 = quotes[1];
        SBQQ__Quote__c quote3 = quotes[2];
        SBQQ__Quote__c quote4 = quotes[3];
        
        opportunities[0].SBQQ__PrimaryQuote__c = quote1.Id;
        opportunities[1].SBQQ__PrimaryQuote__c = quote2.Id;
        opportunities[2].SBQQ__PrimaryQuote__c = quote3.Id;
        opportunities[3].SBQQ__PrimaryQuote__c = quote4.Id;
        update opportunities;
        
        List<SBQQ__QuoteLine__c> fatherQuoteLines = new List<SBQQ__QuoteLine__c>();
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, places[0].Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('SLT001').Id, Date.today(), contact.Id, places[1].Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SLT001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id)));        
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, productCodeToProduct.get('LBB001').Id, Date.today().addDays(1), contact.Id, places[0].Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBB001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, banners[1].Id, Date.today().addDays(1), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SBANNER001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, productCodeToProduct.get('LBB001').Id, Date.today().addDays(1), contact.Id, places[0].Id, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBB001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, banners[1].Id, Date.today().addDays(1), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SBANNER001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, banners[0].Id, Date.today(), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('LBANNER001').Id)));
        fatherQuoteLines.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, banners[1].Id, Date.today().addDays(1), contact.Id, null, null, 'Annual', product2Pbe.get(productCodeToProduct.get('SBANNER001').Id)));
        insert fatherQuoteLines;
        
        List<SBQQ__QuoteLine__c> childrenQLs = new List<SBQQ__QuoteLine__c>();
		childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLines[0].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLines[0].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLines[0].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote1.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLines[0].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLines[1].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLines[1].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLines[1].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote2.Id, productCodeToProduct.get('POREV001').Id, Date.today(), null, null, fatherQuoteLines[1].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('POREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, productCodeToProduct.get('ORER001').Id, Date.today().addDays(1), null, null, fatherQuoteLines[4].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, productCodeToProduct.get('OURL001').Id, Date.today().addDays(1), null, null, fatherQuoteLines[4].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OURL001').Id))); 
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, productCodeToProduct.get('ONAP001').Id, Date.today().addDays(1), null, null, fatherQuoteLines[4].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id))); 
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote3.Id, productCodeToProduct.get('OREV001').Id, Date.today().addDays(1), null, null, fatherQuoteLines[4].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, productCodeToProduct.get('ORER001').Id, Date.today(), null, null, fatherQuoteLines[6].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ORER001').Id)));
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, productCodeToProduct.get('OURL001').Id, Date.today(), null, null, fatherQuoteLines[6].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OURL001').Id))); 
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, productCodeToProduct.get('ONAP001').Id, Date.today(), null, null, fatherQuoteLines[6].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('ONAP001').Id))); 
        childrenQLs.add(Test_DataFactory.generateQuoteLine_final(quote4.Id, productCodeToProduct.get('OREV001').Id, Date.today(), null, null, fatherQuoteLines[6].Id, 'Annual', product2Pbe.get(productCodeToProduct.get('OREV001').Id)));
        insert childrenQLs;
        
       	quote1.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        quote2.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        quote3.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        quote3.Replacement__c = TRUE;
        quote3.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        quote4.SBQQ__Type__c = ConstantsUtil.QUOTE_TYPE_REPLACEMENT;
        quote4.Replacement__c = TRUE;
        quote4.SBQQ__Status__c = ConstantsUtil.Quote_StageName_Accepted;
        update quotes;
        
        List<Order> activationOrders = new List<Order>();
        for(SBQQ__Quote__c quote : quotes){
            activationOrders.add(createActivationOrder(account.Id, quote.Id));
        }
        insert activationOrders;
        
        List<OrderItem> fatherOrderItems = new List<OrderItem>();
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrders[0].Id, fatherQuoteLines[0]));
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrders[0].Id, fatherQuoteLines[2]));
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrders[1].Id, fatherQuoteLines[1]));
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrders[1].Id, fatherQuoteLines[3]));
        fatherOrderItems.add(Test_DataFactory.generateFatherOrderItem(activationOrders[0].Id, fatherQuoteLines[8]));
        insert fatherOrderItems;
        
        List<OrderItem> childrenOIs = new List<OrderItem>();
        childrenOIs.addAll(Test_DataFactory.generateChildrenOderItem(activationOrders[0].Id, new List<SBQQ__QuoteLine__c>{childrenQLs[0],childrenQLs[1],childrenQLs[2],childrenQLs[3]}, fatherOrderItems[0].Id));
        childrenOIs.addAll(Test_DataFactory.generateChildrenOderItem(activationOrders[1].Id, new List<SBQQ__QuoteLine__c>{childrenQLs[4],childrenQLs[5],childrenQLs[6],childrenQLs[7]}, fatherOrderItems[1].Id));
        insert childrenOIs;
        
        List<Contract> contracts = new List<Contract>();
        contracts.add(Test_DataFactory.generateContractFromOrder(activationOrders[0], false));
        contracts.add(Test_DataFactory.generateContractFromOrder(activationOrders[1], false));
        insert contracts;
		
        List<SBQQ__Subscription__c> fatherSubs = new List<SBQQ__Subscription__c>();
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contracts[0], fatherOrderItems[0]));
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contracts[0], fatherOrderItems[1]));
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contracts[1], fatherOrderItems[2]));
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contracts[1], fatherOrderItems[3]));
        fatherSubs.add(Test_DataFactory.generateFatherSubFromOI(contracts[0], fatherOrderItems[4]));
        for(SBQQ__Subscription__c sub : fatherSubs){
            sub.Subscription_Term__c = fatherQuoteLines[0].Subscription_Term__c;
        }
        insert fatherSubs;
        
        List<SBQQ__Subscription__c> childrenSubs = new List<SBQQ__Subscription__c>();
        childrenSubs.addAll(Test_DataFactory.generateChildrenSubsFromOis(contracts[0], new List<OrderItem>{childrenOIs[0],childrenOIs[1],childrenOIs[2],childrenOIs[3]}, fatherSubs[0].Id));
        childrenSubs.addAll(Test_DataFactory.generateChildrenSubsFromOis(contracts[1], new List<OrderItem>{childrenOIs[4],childrenOIs[5],childrenOIs[6],childrenOIs[7]}, fatherSubs[2].Id));
        insert childrenSubs;
        
        Map<Id, Id> oiToSub = new Map<Id, Id>();
        for(SBQQ__Subscription__c sub : childrenSubs){
            oiToSub.put(sub.SBQQ__OrderProduct__c, sub.Id);
        }
        
        fatherOrderItems[0].SBQQ__Subscription__c = fatherSubs[0].Id;
        fatherOrderItems[1].SBQQ__Subscription__c = fatherSubs[1].Id;
        fatherOrderItems[2].SBQQ__Subscription__c = fatherSubs[2].Id;
        fatherOrderItems[3].SBQQ__Subscription__c = fatherSubs[3].Id;
        fatherOrderItems[4].SBQQ__Subscription__c = fatherSubs[4].Id;
        update fatherOrderItems;
        
        for(OrderItem oi : childrenOIs){
            oi.SBQQ__Subscription__c = oiToSub.get(oi.Id);
        }
        update childrenOIs;
        
        activationOrders[0].Contract__c = contracts[0].Id;
        activationOrders[1].Contract__c = contracts[1].Id;
        activationOrders[0].Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        activationOrders[1].Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
        activationOrders[0].SBQQ__Contracted__c = true;
        activationOrders[1].SBQQ__Contracted__c = true;
        update activationOrders;
        
        contracts[0].Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contracts[0].SBQQ__Order__c = activationOrders[0].Id;
        contracts[1].Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        contracts[1].SBQQ__Order__c = activationOrders[1].Id;
        update contracts;
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        for(SBQQ__Subscription__c currentSub : fatherSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        for(SBQQ__Subscription__c currentSub : childrenSubs){
            currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
            subsToUpdate.add(currentSub);
        }
        update subsToUpdate;
        
        //Replacement quotes management
        fatherSubs[0].Replaced_by_Product__c = fatherQuoteLines[4].Id;
        fatherSubs[1].Replaced_by_Product__c = fatherQuoteLines[5].Id;
        fatherSubs[2].Replaced_by_Product__c = fatherQuoteLines[6].Id;
        fatherSubs[3].Replaced_by_Product__c = fatherQuoteLines[7].Id;
        fatherSubs[4].Replaced_by_Product__c = fatherQuoteLines[9].Id;
        update fatherSubs;
        
        SBQQ.TriggerControl.enable();
        AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        PlaceTriggerHandler.disableTrigger = false;
        OpportunityTriggerHandler.disableTrigger = false;
        QuoteTriggerHandler.disableTrigger = false;
        QuoteLineTriggerHandler.disableTrigger = false;
        ContractTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_replaceSubscriptions(){
        
        List<Opportunity> opportunities = [SELECT Id, SBQQ__PrimaryQuote__c FROM Opportunity WHERE Name IN ('Test Opportunity Name 3', 'Test Opportunity Name 4')];
        
        SBQQ.TriggerControl.disable();
        for(Opportunity opty : opportunities){
            opty.StageName = ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON;
        }
        Test.startTest();
		update opportunities[0];
        Test.stopTest();
        SBQQ.TriggerControl.enable();
        
    }
    
    public static Product2 generteFatherProduct_localBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','localBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','LBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','LBANNER');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','localBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','LBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;
    }
    
    public static Product2 generteFatherProduct_searchBANNER(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_ADV);
        fieldApiNameToValueProduct.put('Name','generteFatherProduct_searchBANNER');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','SBANNER001-FT');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','SBANNER');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_FT);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','searchBANNER');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','true');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','SBANNER001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static Order createActivationOrder(String accountId, String quoteId){
        Map<String,String> fieldApiNameToValue = new Map<String,String>();
        fieldApiNameToValue.put('Custom_Type__c', ConstantsUtil.ORDER_TYPE_ACTIVATION);
        fieldApiNameToValue.put('Type', ConstantsUtil.ORDER_TYPE_NEW);
        fieldApiNameToValue.put('EffectiveDate', String.valueOf(Date.today()));
        fieldApiNameToValue.put('EndDate', String.valueOf(Date.today().addMonths(12)));
        fieldApiNameToValue.put('AccountId', accountId);
        fieldApiNameToValue.put('SBQQ__Quote__c', quoteId);
        Order order = Test_DataFactory.generateOrder(fieldApiNameToValue);
        return order;
    }
}