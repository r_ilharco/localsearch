/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 19-07-2019
 * Sprint      : 5
 * User Story  : SF-210
 * Testclass   : 
 * Package     : 
 * Description : Order Management
 * Changelog   : 
 */

public without sharing class SEL_Log {

    public static Log__c getLogByCorrelationId(String correlationId){
        List<Log__c> logs = [SELECT Id, 
                                    Quote__c, 
                                    Correlation_Id__c, 
                                    Order__c  
                            FROM    Log__c 
                            WHERE   Correlation_Id__c =: correlationId  
                            ORDER BY CreatedDate DESC 
                            LIMIT 1];
        
        if(!logs.isEmpty()){
            return logs[0];
        }
        else{
            return null;
        }
    }

    //Vincenzo Laudato - SF2-300 *** 2019_08_06
    public static List<Log__c> getLogById(Id logId){
        return [SELECT  Id, 
                        Error__c, 
                        Communication_Ok__c 
                FROM    Log__c 
                WHERE   Id = :logid 
                LIMIT 1];
    } 
}