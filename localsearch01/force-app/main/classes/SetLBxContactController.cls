public without sharing class SetLBxContactController {
	
    @AuraEnabled
    public static List<SelectOptionLightning> contactToChoose(String quoteLineId){
        List<SelectOptionLightning> selectOptions = new List<SelectOptionLightning>();
        try{
            List<SBQQ__QuoteLine__c> qls = [SELECT Account__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLineId LIMIT 1];
            String accountId = qls[0].Account__c;
            List<Contact> contacts = [SELECT Id, Name FROM Contact WHERE AccountId = :accountId];
            SelectOptionLightning solNone = new SelectOptionLightning('--NONE SELECTED--', '');
            selectOptions.add(solNone);
            for(Contact c : contacts){
                SelectOptionLightning sol = new SelectOptionLightning(c.Name, c.Id);
                selectOptions.add(sol);
            }
        }
        catch(Exception e){
            System.debug('Exception in contactToChoose: '+e.getMessage());
        }
        selectOptions.sort();
        return selectOptions;
    }
    
    @AuraEnabled
    public static resultFromQL updateQLWithContact(String quoteLineId, String contactId){
        Boolean isSuccess = true;
        resultFromQL sv = new resultFromQL();
        sv.isSuccess = true;
        try{
            List<SBQQ__QuoteLine__c> qls = [SELECT Id, LBx_Contact__c FROM SBQQ__QuoteLine__c WHERE Id = :quoteLineId LIMIT 1];
            if(!qls.isEmpty()){
                
                if(qls[0].LBx_Contact__c == null){
                  qls[0].LBx_Contact__c = contactId;
                QuoteLineTriggerHandler.disableTrigger = true;
                update qls;
                QuoteLineTriggerHandler.disableTrigger = false;
                } else {
                qls[0].LBx_Contact__c = contactId;
                update qls;
                }
            }
        }
        catch(Exception e){
            System.debug('Exception in updateQLWithContact: '+e.getMessage());
            sv.isSuccess = false;
           	sv.errorMessage = e.getMessage().substringAfter(':').substringAfter(',').substringBefore(':');

        }
        return sv;
    }
    
   
    public class resultFromQL{
        @AuraEnabled public Boolean isSuccess{get;set;}
        @AuraEnabled public string errorMessage {get;set;}
    }
}