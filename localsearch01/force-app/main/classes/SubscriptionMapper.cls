public class SubscriptionMapper {
    @InvocableMethod
    public static void setFieldsOnSubscription(List<Id> subscriptionsID) {
        Set<String> myWebProductCodes = new Set<String>{ConstantsUtil.MY_WEB_BASIC, 
            											ConstantsUtil.MY_WEB_STD, 
            											ConstantsUtil.MY_WEB_PROF,
            											ConstantsUtil.O_MYWEB_PREMIUMDOMAIN,
            											ConstantsUtil.O_MYWEB_LOGO,
                                                        ConstantsUtil.O_MYWEB_ADDPAGE,
                                                        ConstantsUtil.O_MYWEB_TRANSLADDPAGE,
                                                        ConstantsUtil.O_MYWEB_IFRAME,
                                                        ConstantsUtil.O_MYWEB_BLOG,
                                                        ConstantsUtil.O_MYWEB_REBUILD,
                                                        ConstantsUtil.O_MYWPROFESSIONALIT_SPRACHE001,
														ConstantsUtil.O_MYWPROFESSIONALFR_SPRACHE001,
														ConstantsUtil.O_MYWPROFESSIONALDE_SPRACHE001,
														ConstantsUtil.O_MYWPROFESSIONALEN_SPRACHE001,
														ConstantsUtil.O_MYWSTANDARDIT_SPRACHE001,
														ConstantsUtil.O_MYWSTANDARDDE_SPRACHE001,
														ConstantsUtil.O_MYWSTANDARDFR_SPRACHE001,
														ConstantsUtil.O_MYWSTANDARDEN_SPRACHE001,
														ConstantsUtil.O_MYWBASICIT_SPRACHE001,
														ConstantsUtil.O_MYWBASICFR_SPRACHE001,
														ConstantsUtil.O_MYWBASICDE_SPRACHE001,
														ConstantsUtil.O_MYWBASICEN_SPRACHE001};
                                                            
        Set<String> myWebProduktionCodes = new Set<String>{ConstantsUtil.O_MYWPROFESSIONAL_PRODUKTION001, 
                                                            ConstantsUtil.O_MYWSTANDARD_PRODUKTION001, 
                                                            ConstantsUtil.O_MYWBASIC_PRODUKTION001};
        try{
            System.debug('setFieldsOnSubscription - process Builder');
            List<SBQQ__Subscription__c> subList =  SEL_Subscription.getSubscriptionForUpdateField(subscriptionsID);
            Map<Id,SBQQ__Subscription__c> orderItemToSub = new Map<Id,SBQQ__Subscription__c>(); //ID OrderItem --> Subscriptions
            Map<Id,OrderItem> idToOrderItem = new Map<Id,OrderItem>();							//ID OrderItem --> OrderItem
            
            for(SBQQ__Subscription__c currentSub : subList){
                system.debug('current sub order product --> '+currentSub.SBQQ__OrderProduct__c+' current sub --> '+currentSub);
                orderItemToSub.put(currentSub.SBQQ__OrderProduct__c, currentSub);      
            }
            System.debug('orderItemToSub '+orderItemToSub);
            
            if(!orderItemToSub.isEmpty()){
                List<OrderItem> orderItems = SEL_OrderItem.getOrderItemsByIds(orderItemToSub.keySet());
                
                System.debug('orderItemToSub.keySet() '+orderItemToSub.keySet());
                System.debug('orderItems size'+orderItems.size());
                
                System.debug('order items --> '+orderItems);
                for(OrderItem currentOI : orderItems){
                    idToOrderItem.put(currentOI.Id, currentOI);
                }
                System.debug('id to order items --> '+idToOrderItem);
                System.debug(' orderItems '+ orderItems);
                Map<String, Integer> mapInvoiceCycle = new  Map<String, Integer> ();
                for(Invoice_Cycle__mdt invc : [SELECT MasterLabel, Number_of_months__c FROM Invoice_Cycle__mdt]){
                    mapInvoiceCycle.put(invc.MasterLabel, (Integer)invc.Number_of_months__c);
                }
                Date next_invoice_date;
                
                if(!idToOrderItem.isEmpty()){
                    List<SBQQ__Subscription__c> subToUpdate = new List<SBQQ__Subscription__c>();
                    Map<Id, Contract> contractsToUpdate = new Map<Id,Contract>();
                    //Vincenzo Laudato - Order Management Enhancement
                    Set<Order> orderToUpdate = new Set<Order>();
                    for(Id currentKey : orderItemToSub.keySet()){
                        System.debug('current key order item '+idToOrderItem.get(currentKey));
                        orderItemToSub.get(currentKey).Place__c = idToOrderItem.get(currentKey).Place__c;
                        orderItemToSub.get(currentKey).Location__c = idToOrderItem.get(currentKey).Location__c;
                        orderItemToSub.get(currentKey).Category__c = idToOrderItem.get(currentKey).Category__c;
                        orderItemToSub.get(currentKey).Editorial_Content__c = idToOrderItem.get(currentKey).Editorial_Content__c;
                        orderItemToSub.get(currentKey).Campaign_Id__c = idToOrderItem.get(currentKey).Campaign_Id__c;
                        orderItemToSub.get(currentKey).Url__c = idToOrderItem.get(currentKey).Url__c;
                        orderItemToSub.get(currentKey).Url2__c = idToOrderItem.get(currentKey).Url2__c;
                        orderItemToSub.get(currentKey).Url3__c = idToOrderItem.get(currentKey).Url3__c;
                        orderItemToSub.get(currentKey).Url4__c = idToOrderItem.get(currentKey).Url4__c;
                        orderItemToSub.get(currentKey).Package_Total__c = idToOrderItem.get(currentKey).Package_Total__c;
                        orderItemToSub.get(currentKey).CategoryID__c = idToOrderItem.get(currentKey).CategoryID__c;
                        orderItemToSub.get(currentKey).LocationID__c = idToOrderItem.get(currentKey).LocationID__c;
                        orderItemToSub.get(currentKey).TLPaket_Region__c = idToOrderItem.get(currentKey).TLPaket_Region__c;
                        orderItemToSub.get(currentKey).Contract_Ref_Id__c = idToOrderItem.get(currentKey).Contract_Ref_Id__c;
                        orderItemToSub.get(currentKey).Activate_Later__c = idToOrderItem.get(currentKey).Activate_Later__c;
                        orderItemToSub.get(currentKey).End_Date__c = idToOrderItem.get(currentKey).End_Date__c;
                        orderItemToSub.get(currentKey).Ad_Context_Allocation__c = idToOrderItem.get(currentKey).Ad_Context_Allocation__c;
                        orderItemToSub.get(currentKey).Keywords__c = idToOrderItem.get(currentKey).Keywords__c;
                        orderItemToSub.get(currentKey).Placement_Phone__c = idToOrderItem.get(currentKey).Placement_Phone__c;
                        orderItemToSub.get(currentKey).Manuscript__c = idToOrderItem.get(currentKey).Manuscript__c;
                        orderItemToSub.get(currentKey).Manual_Termination_Product__c = idToOrderItem.get(currentKey).Manual_Termination_Product__c;
                        orderItemToSub.get(currentKey).Manual_Amendment_Product__c = idToOrderItem.get(currentKey).Manual_Amendment_Product__c;
                        orderItemToSub.get(currentKey).SBQQ__RenewalPrice__c = idToOrderItem.get(currentKey).SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
                        orderItemToSub.get(currentKey).LBx_Contact__c = idToOrderItem.get(currentKey).LBx_Contact__c;
                        orderItemToSub.get(currentKey).Simulation_Number__c = idToOrderItem.get(currentKey).Simulation_Number__c;
                        System.debug('Start Date subscription --> '+orderItemToSub.get(currentKey).SBQQ__StartDate__c);
                        
                        if(orderItemToSub.get(currentKey).SBQQ__StartDate__c <= Date.today() && (idToOrderItem.get(currentKey).SBQQ__RequiredBy__c == null || (!idToOrderItem.get(currentKey).Activate_Later__c))){
                            
                            orderItemToSub.get(currentKey).Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
                            
                            if(ConstantsUtil.CONTRACT_STATUS_DRAFT.equalsIgnoreCase(orderItemToSub.get(currentKey).SBQQ__Contract__r.Status) && orderItemToSub.get(currentKey).SBQQ__Contract__r.StartDate > orderItemToSub.get(currentKey).SBQQ__StartDate__c){
                                Contract c = orderItemToSub.get(currentKey).SBQQ__Contract__r;
                                c.StartDate = orderItemToSub.get(currentKey).SBQQ__StartDate__c;
                                c.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
                                contractsToUpdate.put(c.Id, c);
                            }
                        }
                        else{
                            if(ConstantsUtil.CONTRACT_STATUS_DRAFT.equalsIgnoreCase(orderItemToSub.get(currentKey).SBQQ__Contract__r.Status) && orderItemToSub.get(currentKey).SBQQ__Contract__r.StartDate > orderItemToSub.get(currentKey).SBQQ__StartDate__c){
                                Contract c = orderItemToSub.get(currentKey).SBQQ__Contract__r;
                                c.StartDate = orderItemToSub.get(currentKey).SBQQ__StartDate__c;
                                contractsToUpdate.put(c.Id, c);
                            }
                        }
                        
                        //Vincenzo Laudato - MyWEBSITE management
                        if(idToOrderItem.get(currentKey).Order.SambaIsBilled__c == FALSE /*&& orderItemToSub.get(currentKey).SBQQ__StartDate__c <= Date.today()*/ && !idToOrderItem.get(currentKey).Activate_Later__c && myWebProductCodes.contains(orderItemToSub.get(currentKey).Product_Code__c)){
                            System.debug('set to Production sub');
                            orderItemToSub.get(currentKey).Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION;  
                        }
                        if(idToOrderItem.get(currentKey).Order.SambaIsBilled__c == FALSE && !idToOrderItem.get(currentKey).Activate_Later__c && myWebProduktionCodes.contains(orderItemToSub.get(currentKey).Product_Code__c)){
                            System.debug('set to Active sub');
                            orderItemToSub.get(currentKey).Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
                            //ldimartino, SPIII-3483 START
                            orderItemToSub.get(currentKey).SBQQ__SubscriptionStartDate__c = Date.valueOf(orderItemToSub.get(currentKey).SBQQ__Contract__r.CreatedDate);
                            if(String.isNotEmpty(orderItemToSub.get(currentKey).Subscription_Term__c)){
                                orderItemToSub.get(currentKey).End_Date__c = orderItemToSub.get(currentKey).SBQQ__SubscriptionStartDate__c.addMonths(Integer.valueOf(orderItemToSub.get(currentKey).Subscription_Term__c))-1;
                                if(!ConstantsUtil.PRODUCT2_SUBSCRIPTIONTYPE_EVERGREEN.equalsIgnoreCase(orderItemToSub.get(currentKey).SBQQ__SubscriptionType__c)){
                                    orderItemToSub.get(currentKey).SBQQ__SubscriptionEndDate__c = (Date.valueOf(orderItemToSub.get(currentKey).SBQQ__Contract__r.CreatedDate)).addMonths(Integer.valueOf(orderItemToSub.get(currentKey).Subscription_Term__c))-1;                
                                }
                            }
                            Contract c = orderItemToSub.get(currentKey).SBQQ__Contract__r;
                            c.StartDate = Date.valueOf(orderItemToSub.get(currentKey).SBQQ__Contract__r.CreatedDate);
                            c.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
                            contractsToUpdate.put(c.Id, c);
                            //ldimartino, SPIII-3483 END
                        }
                        if(idToOrderItem.get(currentKey).Order.SambaIsBilled__c == FALSE && idToOrderItem.get(currentKey).Order.SambaIsStartConfirmed__c  && myWebProduktionCodes.contains(orderItemToSub.get(currentKey).Product_Code__c)){
                            System.debug('set to Production sub if migrated');
                            orderItemToSub.get(currentKey).Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION;
                        }
                        if( orderItemToSub.get(currentKey).SBQQ__Contract__r.SBQQ__Evergreen__c){
                        	orderItemToSub.get(currentKey).Next_Renewal_Date__c = idToOrderItem.get(currentKey).End_Date__c.addDays(1);
                        }
                        if( orderItemToSub.get(currentKey).SBQQ__ListPrice__c <0 || orderItemToSub.get(currentKey).SBQQ__ListPrice__c == NULL ){
                        	orderItemToSub.get(currentKey).SBQQ__ListPrice__c = idToOrderItem.get(currentKey).ListPrice;
                        }
                        
                        //Vincenzo Laudato
                        if(orderItemToSub.get(currentKey).SBQQ__RequiredById__c != null){
                            orderItemToSub.get(currentKey).RequiredBy__c = orderItemToSub.get(currentKey).SBQQ__RequiredById__c;
                        }
                        
                        if(idToOrderItem.get(currentKey).Order.ContractSAMBA_Key__c != null
                           && idToOrderItem.get(currentKey).Order.SambaIsStartConfirmed__c == TRUE 
                           && idToOrderItem.get(currentKey).Order.SambaIsBilled__c == TRUE
                           && orderItemToSub.get(currentKey).SBQQ__Bundled__c == FALSE 
                           && orderItemToSub.get(currentKey).SBQQ__BillingFrequency__c != NULL){
                               next_invoice_date = orderItemToSub.get(currentKey).SBQQ__SubscriptionStartDate__c;
                               while (next_invoice_date < date.today()){
                                     next_invoice_date = next_invoice_date.addMonths(mapInvoiceCycle.get(orderItemToSub.get(currentKey).SBQQ__BillingFrequency__c));
                               }
                            orderItemToSub.get(currentKey).Next_Invoice_Date__c = next_invoice_date;
                            orderItemToSub.get(currentKey).Billed__c = true;
                            orderItemToSub.get(currentKey).Subscription_Activated_Date__c = orderItemToSub.get(currentKey).SBQQ__StartDate__c;
                        }
                        
                        //Vincenzo Laudato - Order Management Enhancement
                        idToOrderItem.get(currentKey).Order.Contract__c = orderItemToSub.get(currentKey).SBQQ__Contract__c;
                        orderToUpdate.add(idToOrderItem.get(currentKey).Order);
                        //Vincenzo Laudato - Renewal Discount
                        if(idToOrderItem.get(currentKey).SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c != null){
                            Contract c = orderItemToSub.get(currentKey).SBQQ__Contract__r;
                            if(!contractsToUpdate.containsKey(c.Id)){
                                c.Applied_Discount__c = idToOrderItem.get(currentKey).SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c;
                            	contractsToUpdate.put(c.Id, c);
                            }
                            else{
                                contractsToUpdate.get(c.Id).Applied_Discount__c = idToOrderItem.get(currentKey).SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c;
                            }
                        	
                        }
                        //Ldimartino, START
                        if(orderItemToSub.get(currentKey).SBQQ__Product__r.Product_Group__c == ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT
                           && orderItemToSub.get(currentKey).Subscription_Term__c != null
                           && orderItemToSub.get(currentKey).SBQQ__Product__r.Base_term_Renewal_term__c != null){
                               orderItemToSub.get(currentKey).Number_of_Publications__c = (Integer.valueOf(orderItemToSub.get(currentKey).Subscription_Term__c)/Integer.valueOf(orderItemToSub.get(currentKey).SBQQ__Product__r.Base_term_Renewal_term__c))-1;
                        }
                        //Ldimartino, END
                        subToUpdate.add(orderItemToSub.get(currentKey));                        
                    }
                    update subToUpdate;
                    if(!orderToUpdate.isEmpty()){
                        OrderTriggerHandler.disableTrigger = true;
                        update new List<Order>(orderToUpdate);
                        OrderTriggerHandler.disableTrigger = false;
                    }
                    if(!contractsToUpdate.isEmpty()){
                        ContractTriggerHandler.disableTrigger = true;
                        update contractsToUpdate.values();
                        ContractTriggerHandler.disableTrigger = false;
                    }
                }
            }
        }
        catch(Exception e){
             ErrorHandler.log(System.LoggingLevel.ERROR, 'SubscriptionMapper', 'SubscriptionMapper.setFieldsOnSubscription', e, ErrorHandler.ErrorCode.E_UNKNOWN, e.getMessage(), null, null, null, null, false);
        }
    }
}
