@isTest
global class Test_OrderUtilityMock implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.SERVICE_MASHERY);
        if(request.getEndpoint().contains(methodName)){
            response.setStatusCode(200);
        	response.setBody('{"access_token":"testToken","token_type":"bearer","expires_in":28800}');	    
        }
        else{
            response.setStatusCode(200);  
        }
        return response;
    }
}