/**
* Author      : Mara Mazzarella <mamazzarella@deloitte.it>
* Date	       : 2020-25-05
* Sprint      : phase 2
* Work item   : Migration phase
* Testclass   : SetAccountStatusBatch_Test
* Package     : restore account status and number of active contracts.
* Description : 
* Changelog   :
*/

global without sharing class SetAccountStatusBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    global List<String> staticValues = new List<String>();
    
    global SetAccountStatusBatch(List<String> val){
        this.staticValues = val;
    }
    
    global void execute(SchedulableContext sc) {
        SetAccountStatusBatch setStatus = new SetAccountStatusBatch(this.staticValues); 
        Database.executeBatch(setStatus, 100); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String clientRatingD = ConstantsUtil.CLIENT_RATING_DSUBJ_LEGAL_PROCESS;
        String clientRatingC = ConstantsUtil.CLIENT_RATING_C_PRE_LEGAL_PROCESS;
        return Database.getQueryLocator('SELECT Id, Customer_Number__c, Status__c, Last_Inactive_Date__c, Activation_Date__c FROM Account WHERE '+
                                        ' Cluster_Id__c IN :staticValues'+ 
                                        ' and id in (select accountid from Opportunity where StageName =\''+ConstantsUtil.OPPORTUNITY_STAGE_CLOSE_WON +'\')' +
                                        ' ORDER BY LastModifiedDate ASC');
    }
    
    global String log = '';
    global void execute(Database.BatchableContext info, List<Account> scope){
        Set<Id> accIds = new set<Id>();
        for(Account a : scope){
            accIds.add(a.id);
        }
        AccountStatusUpdateScript.updateAccountStatus(accIds);  
    }
    global void finish(Database.BatchableContext context) {
        NewCustomerMergeAccount_Batch a = new NewCustomerMergeAccount_Batch();
        a.execute(null);
    } 
}