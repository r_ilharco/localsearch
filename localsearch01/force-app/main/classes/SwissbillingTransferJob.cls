public class SwissbillingTransferJob implements Database.Batchable<sObject>, Database.AllowsCallouts {
    private Set<Id> skippedInvoices;
    private Set<Id> specificInvoices;
    private Boolean modeSpecificInvoices; 
    
    public SwissbillingTransferJob () {
    	this.skippedInvoices = new Set<id>();
        this.modeSpecificInvoices = false;
    }

    public SwissbillingTransferJob (Set<id> skippedInvoices) {
    	this.skippedInvoices = skippedInvoices;
        this.modeSpecificInvoices = false;
    }

    public SwissbillingTransferJob (Set<id> skippedInvoices, Set<id> specificInvoices) {
    	this.skippedInvoices = skippedInvoices;
        this.specificInvoices = specificInvoices;
        this.modeSpecificInvoices = true;
    }
    
    public List<sObject> start(Database.BatchableContext context) {
        if(!this.modeSpecificInvoices){
            // Select invoices and related data with invoice date < today & status 'In Collection'
            return [SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c,
                Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
                Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
                Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
                Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
                (SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
                FROM Invoice__c
                WHERE Status__c = :ConstantsUtil.INVOICE_STATUS_IN_COLLECTION /*AND Invoice_Date__c <= TODAY*/ AND Id not in :skippedInvoices LIMIT 50000];    
        } else {
             return [SELECT Id, Name, Amount__c, Bill_Type__c, Correlation_Id__c, Invoice_Date__c, Invoice_Code__c, Payment_Terms__c,
                Process_Mode__c, Rounding__c, Tax__c, Tax_Mode__c, Total__c, Status__c, Billing_Channel__c,
                Customer__c, Customer__r.Customer_Number__c, Customer__r.Name, Payment_Reference__c,
                Billing_Profile__r.Billing_Street__c, Billing_Profile__r.Billing_Language__c, Billing_Profile__r.Billing_City__c,
                Billing_Profile__r.Billing_Country__c, Billing_Profile__r.Billing_Postal_Code__c, Billing_Profile__r.Billing_State__c,
                Billing_Profile__r.Billing_Contact__r.FirstName, Billing_Profile__r.Billing_Contact__r.LastName, Billing_Profile__r.Billing_Contact__r.Phone, Billing_Profile__r.Billing_Contact__r.Email,
                Billing_Profile__r.P_O_Box__c, Billing_Profile__r.P_O_Box_City__c, Billing_Profile__r.P_O_Box_Zip_Postal_Code__c, Billing_Profile__r.Billing_Name__c,
                (SELECT From__c, Net__c, Percentage__c, To__c, Total__c, Vat__c, Vat_Code__c from Invoice_Taxes__r)
                FROM Invoice__c
                WHERE Status__c = :ConstantsUtil.INVOICE_STATUS_IN_COLLECTION /*AND Invoice_Date__c <= TODAY */AND Id not in :skippedInvoices AND Id in :specificInvoices LIMIT 50000];    
        }

    }
    
    public void execute(Database.BatchableContext context, List<sObject> data) {
        //vlaudato added in query Credit_Note__r.Manual__c,
        List<Invoice__c> invoices = (List<Invoice__c>) data;
        List<Invoice_Order__c> invoiceOrders = [
            SELECT Id, Amount_Subtotal__c, Description__c, Invoice__c, Invoice_Order_Code__c, Item_Row_Number__c, Period_From__c, Period_To__c, Tax_Subtotal__c, Title__c,
			(SELECT Accrual_From__c, Accrual_To__c, Amount__c, Description__c, Discount_Percentage__c, Discount_Value__c, Installment__c, Level__c,
                Line_Number__c, Period__c, Quantity__c, Sales_Channel__c, Tax_Code__c, Title__c, Total__c, Unit__c, Unit_Price__c,
                Product__r.ProductCode, Product__c, Subscription__c, Subscription__r.Place__c, Subscription__r.Place__r.Name, Subscription__r.Place__r.PostalCode__c, Subscription__r.Place__r.City__c,
             	Credit_Note__c, Credit_Note__r.Reimbursed_Invoice__r.Name, Credit_Note__r.Reimbursed_Invoice__r.Invoice_Date__c, Credit_Note__r.Period_From__c, Credit_Note__r.Period_To__c,
             	Credit_Note__r.Execution_Date__c, Credit_Note__r.Nx_Refunded_Place__c, Credit_Note__r.Manual__c,
             	Subscription__r.SBQQ__RequiredById__c, Subscription__r.SBQQ__Product__r.One_time_Fee__c,
             	Subscription__r.SBQQ__RequiredByProduct__c,
             	Subscription__r.SBQQ__QuoteLine__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c,
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__ProductCode__c,
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__Description__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.Name, 
             	Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.PostalCode__c, Subscription__r.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Place__r.City__c
             FROM Invoice_Items__r
			),
            Contract__c, Contract__r.ContractNumber, Contract__r.StartDate
            FROM Invoice_Order__c where Invoice__c in :invoices
            ORDER BY Item_Row_Number__c
        ];
        // Build a map for lower levels, instead of calling SOQL for each invoice (2 level subqueries aren't allowed in SOQL)
        Map<Id, List<Invoice_Order__c>> invoiceOrdersMap = new Map<Id, List<Invoice_Order__c>>();
        
        for(Invoice_Order__c invoiceOrder : invoiceOrders) {
            List<Invoice_Order__c> invoiceOrderList = invoiceOrdersMap.get(invoiceOrder.Invoice__c);
            if(invoiceOrderList == null) {
                invoiceOrderList = new List<Invoice_Order__c>();
                invoiceOrdersMap.put(invoiceOrder.Invoice__c, invoiceOrderList);                
            }
            invoiceOrderList.Add(invoiceOrder);

        }
        
        Http httpClient = new Http();
        
        List<Invoice__c> invoiceUpdates = new List<Invoice__c>();
        List<Credit_Note__c> creditNotesUpdates = new List<Credit_Note__c>();
        List<SBQQ__Subscription__c> subscriptionsUpdates = new List<SBQQ__Subscription__c>();
        List<Contract> contractsUpdates = new List<Contract>();
        Map<Id, Account> updatedCustomers = new Map<Id, Account>();
        Set<Id> subscriptionsSet = new Set<Id>();
        Set<Id> creditNotesSet = new Set<Id>();
        
        Invoice__c updatedInvoice;
        boolean invoiceNumberChanged = false;
        boolean customerNumberChanged = false;
        List<Error_Log__c> errors = new List<Error_Log__c>();
        for(Invoice__c invoice : invoices) {
            try {
                if(!BillingHelper.checkMinTotalAllowed(invoice)) {
	                errors.add(ErrorHandler.createLogWarning('Billing', 'Billing.SwissbillingTransferJob', ErrorHandler.WarningCode.W_BILL_TOTAL_BELOW_LIMIT, 'Bill total below limit', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));
                    skippedInvoices.add(invoice.Id);
                    continue;
                }
				updatedInvoice = null;
                long documentNumber;
                long customerNumber;
                boolean generatedNumber = false;
                try {
                    customerNumber = long.valueOf(invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''));
                } catch(Exception ex) {
                    customerNumber = null;
                }
                if(customerNumber == null) {
                    if(updatedCustomers.containsKey(invoice.Customer__c)) {
                        invoice.Customer__r.Customer_Number__c = updatedCustomers.get(invoice.Customer__c).Customer_Number__c;
                    } else {
                    	customerNumber = BillingHelper.generateCustomerNumber();
                        customerNumberChanged = true;
                    	invoice.Customer__r.Customer_Number__c = String.valueOf(customerNumber);
                    	updatedCustomers.put(invoice.Customer__c, new Account(Id = invoice.Customer__c, Customer_Number__c = String.valueOf(customerNumber)));
                    }
                }
                
                if(invoice.Name != ConstantsUtil.INVOICE_PENDING_NAME) {
                    try {
                    	documentNumber = long.valueOf(invoice.Name.replaceAll('[^0-9]', ''));
                    } catch(Exception ex) {
                        errors.add(ErrorHandler.createLogWarning('Billing', 'Billing.SwissbillingTransferJob', ErrorHandler.WarningCode.W_NOT_A_NUMBER, 'Invoice name can\'t be converted to a number', invoice.Name));
                    }
                }
                
                if(documentNumber == null) {
                    documentNumber = BillingHelper.generateDocumentNumber();
                    updatedInvoice = new Invoice__c(
                        Id = invoice.Id, 
                        Name = BillingHelper.formatDocNumber(documentNumber), 
                        Invoice_Number__c = documentNumber,
                        Payment_Reference__c = AbacusHelper.getESRValue(invoice.Customer__r.Customer_Number__c.replaceAll('[^0-9]', ''), documentNumber.format().replaceAll('[^0-9]', ''))
                    );
                    invoice.Payment_Reference__c = updatedInvoice.Payment_Reference__c;
                    generatedNumber = true;
                }
                
                Map<string, object> documentData = SwissbillingHelper.prepareInvoiceMap(invoice, invoiceOrdersMap, documentNumber);
                string documentJson = JSON.serialize(documentData, true);
                System.debug('DOCUMENT JSON: '+documentJson);
                HttpRequest req = new HttpRequest();
                req.setEndpoint(BillingHelper.billingEndpoint + '/jobs/transfer');
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, invoice.Correlation_Id__c);
                req.setHeader(ConstantsUtil.TRACE_HEAD_BP_NAME, 'SendToSwissbilling');
                req.setHeader(ConstantsUtil.TRACE_HEAD_INITIATOR, 'Salesforce');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CALLING_APP, 'Billing');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_NAME, 'Invoice__c');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_ID, invoice.Id);             
                req.setBody(documentJson);
                req.setTimeout(120000);
                HttpResponse res = httpClient.send(req);
                System.debug('DOCUMENT JSON: '+documentJson);
                
                errors.add(ErrorHandler.createLogInfo('Billing', 'Billing.SwissbillingTransferJob', 'Bill sent to Swissbilling', documentJson, invoice.Correlation_Id__c, invoice));
                integer statusCode = res.getStatusCode();
                system.debug('ldimartino, statusCode: ' + statusCode);
                system.debug('ldimartino, body: ' + res.getBody());
                if(statusCode == 200) {
                    string returnValue = res.getBody();
                    /* Check if content answer is ok. Assume yes until valid response from Swissbilling */
                    if(updatedInvoice == null) {
                    updatedInvoice = new Invoice__c(Id=invoice.Id);
                    }
                    if(generatedNumber) {
                        invoiceNumberChanged = true;
                    }
                    updatedInvoice.Status__c = ConstantsUtil.INVOICE_STATUS_COLLECTED;
                    Map<id, id> sub_parent = new Map<id,id>();
                    for(List<Invoice_Order__c> invoiceOrdersList : invoiceOrdersMap.values()) {
                        for(Invoice_Order__c invoiceOrder : invoiceOrdersList) {
                            for(Invoice_Item__c invoiceItem : invoiceOrder.Invoice_Items__r) {
                                String tempDate = invoiceItem.Period__c.substringAfter('-');
                                tempDate = tempDate.trim();
                                system.debug('tempDate: ' + tempDate);
                                Integer year = Integer.valueOf(tempDate.mid(6,4));
                                Integer month = Integer.valueOf(tempDate.mid(3,2));
                                Integer day = Integer.valueOf(tempDate.mid(0,2));
                                date nextInvoiceDate = date.newInstance(year, month, day);
                                system.debug('nextInvoiceDate: ' + nextInvoiceDate);
                                if(invoiceItem.Subscription__c  != null) {
                                    if(!subscriptionsSet.contains(invoiceItem.Subscription__c)) {
                                        system.debug('AS-147, invoiceItem.Accrual_To__c.addDays(1): ' + invoiceItem.Accrual_To__c.addDays(1));  
                                        if(invoiceItem.Subscription__r.SBQQ__Product__r.One_time_Fee__c){
                                            subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, One_time_Fee_Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
                                        }else{
                                            subscriptionsUpdates.add(new SBQQ__Subscription__c(Id = invoiceItem.Subscription__c, Billed__c = true, Next_Invoice_Date__c = nextInvoiceDate.addDays(1)));
                                        }               	
                                        subscriptionsSet.add(invoiceItem.Subscription__c);
                                    }
                                }
                                if(invoiceItem.Credit_Note__c != null) {
                                    if(!creditNotesSet.contains(invoiceItem.Credit_Note__c)) {
                                        creditNotesUpdates.add(new Credit_Note__c(Id = invoiceItem.Credit_Note__c, Status__c = ConstantsUtil.CREDIT_NOTE_STATUS_BILLED));
                                        creditNotesSet.add(invoiceItem.Credit_Note__c);
                                    }
                                }
                            }
                        }
                    }                
                    errors.add(ErrorHandler.createLogInfo('Billing', 'Billing.SwissbillingTransferJob', 'Bill sent to Swissbilling', 'Invoice number: ' + documentNumber + ' Invoice Id:' + invoice.id, invoice.Correlation_Id__c, invoice));                  
                } else {
                    /* Handle other specific errors */
                    // throw or retry
                    // undo numbering to avoid holes
                    if(generatedNumber) {
                        updatedInvoice = null;
                        BillingHelper.undoDocumentNumber();
                    }
                    throw new SwissbillingHelper.SwissbillingHelperException('Swissbilling TransferJob ' + statusCode + '\n' + res.getBody() + '\n'+ documentJson , ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                }

                                  
            } catch (Exception ex) {
                errors.add(ErrorHandler.createLogError('Billing', 'Billing.SwissbillingTransferJob', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null, 'Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice));
            } finally {
                if(updatedInvoice!=null) {
                	invoiceUpdates.add(updatedInvoice);
                }
            }
		}
        contractsUpdates = SwissbillingHelper.contractsToBeUpdated(subscriptionsUpdates);
        Savepoint savePt;
        if(!Test.isRunningTest()) savePt = Database.setSavepoint();
        try {
            if(invoiceUpdates!=null && invoiceUpdates.size() > 0) {
            	update invoiceUpdates;
            }
            if(subscriptionsUpdates.size() > 0) {
                update subscriptionsUpdates;
            }
            if(contractsUpdates.size() > 0){
                update contractsUpdates;
            }
            if(creditNotesUpdates.size() > 0) {
                update creditNotesUpdates;
            }
            if(updatedCustomers.size() > 0) {
                update updatedCustomers.values();
            }
            if(invoiceNumberChanged) {
            	update BillingHelper.numbering;
            }
            if(customerNumberChanged) {
            	update BillingHelper.customerNumber;
            }
        } catch (Exception ex) {
            if(!Test.isRunningTest()) Database.rollback(savePt);
            errors.add(ErrorHandler.createLog(System.LoggingLevel.ERROR, 'Billing', 'Billing.SwissbillingTransferJob', ex, ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false));
            return;
        } finally {
            if(errors.size() > 0) {
                insert errors;
            }
        }
    }
    
    public void finish(Database.BatchableContext context) {
        GlobalConfiguration_cs__c setting = GlobalConfiguration_cs__c.getOrgDefaults();
		if(!Test.isRunningTest() && setting.AbacusBatch__c == false) { Database.executeBatch( (Database.Batchable<sObject>) Type.forName(ConstantsUtil.ABACUS_JOB_CLASS).newInstance(), 1); }
    }
}