//Changelog: SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019 - Integration for invoice cancellation by a future method.
//			 SF1- 210 -mmazzarella 07-15-2019 Order Management
//@RestResource(urlMapping='/product-configuration/feedbacks/*')// /services/apexrest/product-configuration/feedbacks/
global with sharing class ContractManager {
  //  @HttpPost
    global static void activate() {
        System.debug('Rest call start');
        RestResponse res = RestContext.response;
        RestRequest request = RestContext.request;
        
        try{
            //Extract the parameters in a map
            ResponseInterface responseInfo = (ResponseInterface)JSON.deserialize(request.requestbody.tostring(), ResponseInterface.class);
            System.debug('responseInfo: ' + responseInfo );    
            
            //Extract the correlationId, meaning id of the call
            String idCall = responseInfo.technical_fields.correlation_id;
            System.debug('idCall: ' + idCall );
            Log__c log = new Log__c(Name = idCall+' '+ConstantsUtil.RESPONSE, Body_Message__c = request.requestbody.tostring(), Communication_Ok__c = true);
            insert log;
            
            //Extract the contract with callId of the call
            List<Contract> contracts = new List<Contract>();
            Id quoteId = null;
            String namingLog = idCall+' '+ConstantsUtil.Request;

           	// ErrorHandler.logInfo('ContractManager', '', namingLog+'1');

            try {
                contracts = [SELECT id, CallId__c, In_Termination__c, In_Cancellation__c, Status, EndDate,SBQQ__Quote__c FROM Contract WHERE CallId__c =: idCall  limit 1];
            } catch(Exception e) {
                System.debug('No contract found for this correlation_id');
            }
            // ErrorHandler.logInfo('ContractManager', '', namingLog+'2' + contracts)oldLead;
            SBQQ__Quote__c Quote;
            if(contracts.isEmpty()) {
                try {

                    Quote = [SELECT ID FROM SBQQ__Quote__c WHERE Trial_CallId__c =: idCall  limit 1];
                    quoteId = Quote.Id;
                    // ErrorHandler.logInfo('ContractManager', '', namingLog+' quoteId ' + quoteId);
                } catch(Exception e) {
                    System.debug('No Quote found for this correlation_id');
                }
            }
            Log__c l;
            if(quoteId==null) {
                try {
                    //AND Name =: namingLog
                    l = [SELECT Id, Quote__c, Correlation_Id__c, Contract__c  FROM Log__c WHERE Correlation_Id__c =: idCall  order by CreatedDate desc limit 1];
                    quoteId = l.Quote__c;
                    // ErrorHandler.logInfo('ContractManager', '', namingLog+'1' + l);

                } catch(Exception e) {
                    System.debug('No log found for this quote by this correlation_id');
                    // ErrorHandler.logInfo('ContractManager', '', namingLog+'1e' + e.getMessage());
                }
            }

            if(contracts.isEmpty()) {
                try {
                    if(l.Contract__c!=null){
                        contracts = [SELECT id, CallId__c, Status,In_Termination__c, In_Cancellation__c, EndDate,SBQQ__Quote__c FROM Contract WHERE id =:  l.Contract__c  limit 1];
                    }
                    // ErrorHandler.logInfo('ContractManager', '', namingLog+'2' + l);

                } catch(Exception e) {
                    System.debug('No log found for this quote by this correlation_id');
                    // ErrorHandler.logInfo('ContractManager', '', namingLog+'2e' + e.getMessage());

                }
            }
        //    ErrorHandler.logInfo('ContractManager', '', namingLog+'3');

            System.debug('contracts:'+contracts.size());
            //If exist a contract with this CallId
            if(!contracts.isEmpty()) {
                System.debug('there is a contract with this call id');
                Contract contract = contracts.get(0);
                System.debug('contract: ' + contracts);
                log.contract__c = contract.id;
                string status = contract.status;
                boolean terminationCheckbox = contract.In_Termination__c;
                boolean cancellationCheckbox = contract.In_Cancellation__c;
                System.debug('the contract is in '+status+' status');
                
                if( status.equals(ConstantsUtil.CONTRACT_STATUS_PRODUCTION) || status.equals(ConstantsUtil.CONTRACT_STATUS_DRAFT) 
                    || status.equals(ConstantsUtil.CONTRACT_STATUS_FAILED) ){
                    ContractManager.activateSubscription( responseInfo, contract, log);
                } else if( status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE ) && cancellationCheckbox==true ){
                    ContractManager.cancellationContract(responseInfo, contract, log); //} else if( status.equals(ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION) || status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE)){
                } else if( status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE) && terminationCheckbox==true ){                    
                    ContractManager.terminationContract(responseInfo, contract, log);
                }
                else if( status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE) ){
                    ContractManager.activateSubscriptionOnly( responseInfo, contract, log);
                }                                
              res.responseBody = Blob.valueOf('Ok');
              res.statusCode = 200;
            } else if(quoteId != null) {
                log.Quote__c = quoteId;                                
                // ErrorHandler.logInfo('ContractManager', '', namingLog+'4 call activateQuoteTrial' + quoteId);
                ContractManager.activateQuoteTrial(responseInfo, quoteId, log);
                // ErrorHandler.logInfo('ContractManager', '', namingLog+'4 call finished back activateQuoteTrial' + quoteId);
            } else {
                // ErrorHandler.logInfo('ContractManager', '', namingLog+'>> invalid correlation_id' );

                res.responseBody = Blob.valueOf('Invalid correlation_id');
                res.statusCode = 200;
            } 
            update log;

        } catch(Exception e) {
            Log__c log = new Log__c(Name = ConstantsUtil.RESPONSE+'_ ' + String.valueOf(Datetime.now()), Body_Message__c = request.requestbody.tostring(),
            Message__c = e.getMessage());
            insert log;
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
        }
        
        System.debug('activate call End');
    } 
    
    
    private static string getPrettyPrint(ResponseInterface responseInfo, SBQQ__Subscription__c sub){
        List<String> fillers = new String[]{sub.Name,sub.SBQQ__ProductName__c,responseInfo.status};
        //return 'Subscription: '+sub.Name+' Product Name: '+sub.SBQQ__ProductName__c+' Response Status: '+responseInfo.status+'\n\r';
        return String.format(Label.Subscription_Pretty_Print , fillers);
    }

    public static void activateQuoteTrial(ResponseInterface responseInfo, Id quoteId, Log__c log ){
        System.debug('activateQuoteTrial');
        //Extract the status
        String status = responseInfo.status;

        System.debug('status: ' + status );     

        if(status == ConstantsUtil.AMA_STATUS_SUCCESS){
            status = ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED;
        } else {
            status = ConstantsUtil.QUOTELINE_STATUS_TRIAL_FAILED;
        }
        
        Set<String> quoteLineIds = new set<string>(responseInfo.source_element_ids);
        log.Message__c = '';
        List<SBQQ__QuoteLine__c> listQuoteLineToUpdate = new List<SBQQ__QuoteLine__c>();

        try{
//        for(SBQQ__QuoteLine__c qline : [SELECT Id, Name, SBQQ__ProductName__c, Status__c, TrialStatus__c FROM SBQQ__QuoteLine__c WHERE Id in :quoteLineIds]){
          for(SBQQ__QuoteLine__c qline : [SELECT Id, Name, SBQQ__ProductName__c, Status__c, TrialStatus__c FROM SBQQ__QuoteLine__c 
                                          WHERE SBQQ__Quote__c = :quoteId AND TrialStatus__c!=:ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED]){
            qline.TrialStatus__c = status;
            /*A. L. Marotta 24-07-19 Start
             * SF2-231 Wave 3, Sprint 6 - Checked flag Trial on Quote Line*/
            if(status==ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED){
                qline.Trial__c = true; 
                }
            //A. L. Marotta 24-07-19 End
            // qline.Status__c = status;

            listQuoteLineToUpdate.add(qline);
            
            log.Message__c += TrialActivationUtil.getQuoteLinePrettyPrint(responseInfo.status, qline)+'; ';
        }
        
        if(!listQuoteLineToUpdate.isEmpty()){
            update listQuoteLineToUpdate;
        }
       }catch (Exception ex) {
           	ErrorHandler.logInfo('ContractManager', '', 'activateQuoteTrial '+ex.getMessage());
        }   
    }
    
    public static void activateSubscription(ResponseInterface responseInfo, Contract contract, Log__c log ){
        system.debug('activateSubscription');
        //Extract the status
        String status = responseInfo.status;
        System.debug('status: ' + status );     
        
        if(status == ConstantsUtil.AMA_STATUS_SUCCESS){
            status = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        }else{
            status = ConstantsUtil.SUBSCRIPTION_STATUS_FAILED;
        }

        //AS-03, Hotfix2, ldimartino: Initialize a boolean variable to check if all subs on the contract are failed
        Boolean allSubscriptionsFailed = true;
        //Initialize a boolean variable to see if i activate all contract subscriptions
        Boolean checkedSubscriptions = true;
        set<string> ids = new set<string>(responseInfo.source_element_ids);
        
        //Extract the subscriptions of the contract  
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        //Cycle on contract subscriptions
        // ErrorHandler.logInfo('ContractManager', '', String.join(new List<String>(ids), ','));
    try{
        for(SBQQ__Subscription__c sub : [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__Contract__c, Subsctiption_Status__c,Subscription_Activated_Date__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id])
        {
            //If sub is the subscription needs to be activated
            // if (String.valueOf(sub.Id).equals(responseInfo.source_element_ids)){
            //     ErrorHandler.logInfo('ContractManager', '', '1  matched Id'+String.valueOf(sub.Id));
            // }

            if( status == ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE){
                sub.Subscription_Activated_Date__c=DateTime.now();
                allSubscriptionsFailed = false;
             }else{
                 sub.Subscription_Activated_Date__c=null;
             }

            if(ids.contains(String.valueOf(sub.Id))){
              //  String.valueOf(sub.Id).equals(responseInfo.source_element_ids)){
                //Change subscription status
                sub.Subsctiption_Status__c = status;
                
                log.message__c = ContractManager.getPrettyPrint(responseInfo, sub);

                subscriptionsToUpdate.add(sub);

            //    ErrorHandler.logInfo('ContractManager', '', '2  matched Id'+String.valueOf(sub.Id)+' status '+status );
            }
            
            //If subscription status difference from active, don't activate the contract
            /*if(!sub.Subsctiption_Status__c.equals(ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE)){
                //block the activation of the contract
                checkedSubscriptions = false;
            }*/
        }
        
        //Update subscription
        if(subscriptionsToUpdate.size() >0){
            update subscriptionsToUpdate;
        }

        if(allSubscriptionsFailed){
            contract.Status = ConstantsUtil.CONTRACT_STATUS_FAILED;
        }

        //If all subscriptions are active, activate the contract 
        //
       Integer countsubs;

       countsubs = [SELECT count() FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =:contract.Id and Subsctiption_Status__c!=:ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE];
        //  ErrorHandler.logInfo('ContractManager', '', 'countsubs '+String.valueOf(countsubs) );
        if(countsubs==0){
            contract.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
            system.debug(contract.SBQQ__Quote__c);
            //QuoteManager.convertTrial(contract.SBQQ__Quote__c);
        }
        update contract;
        }catch (Exception ex) {
           	ErrorHandler.logInfo('ContractManager', '', 'activateSubscription '+ex.getMessage());
        }    
    }


    public static void activateSubscriptionOnly(ResponseInterface responseInfo, Contract contract, Log__c log ){
        system.debug('activateSubscription');
        //Extract the status
        
        String status ='';//= responseInfo.status;
        
        //System.debug('status: ' + status );     
        
        if(responseInfo.status == ConstantsUtil.AMA_STATUS_FAILED){                        
            status = ConstantsUtil.SUBSCRIPTION_STATUS_FAILED;
        }
        
        //Initialize a boolean variable to see if i activate all contract subscriptions
        Boolean checkedSubscriptions = true;
        set<string> ids = new set<string>(responseInfo.source_element_ids);
        
        //Extract the subscriptions of the contract  
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        //Cycle on contract subscriptions
   try {      
        for(SBQQ__Subscription__c sub : [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__Contract__c, Subsctiption_Status__c FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =:contract.Id AND Subsctiption_Status__c!=:ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE]){
            //If sub is the subscription needs to be activated           
    
                if(responseInfo.status==ConstantsUtil.AMA_STATUS_SUCCESS){
                    if(sub.Subsctiption_Status__c==ConstantsUtil.SUBSCRIPTION_STATUS_IN_TERMINATION){
                        status=ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
                    }else if(sub.Subsctiption_Status__c==ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION){
                        status=ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;    
                    }
                
                    if(status!=''){
                        sub.Subsctiption_Status__c = status;
                        log.message__c = ContractManager.getPrettyPrint(responseInfo, sub);
                        subscriptionsToUpdate.add(sub);
                    }
                }
        }        
            //Update subscription
            if(subscriptionsToUpdate.size() >0){
                update subscriptionsToUpdate;
            }  
        }catch (Exception ex) {
           	ErrorHandler.logInfo('ContractManager', '', 'activateSubscriptionOnly '+ex.getMessage());
        }                 
    }
        
    public static void cancellationContract(ResponseInterface responseInfo, Contract contract, Log__c log){
        Contract_Setting__mdt contractCancellation = [SELECT InvoiceStatus__c FROM Contract_Setting__mdt WHERE MasterLabel = 'ContractCancellation_Process'][0];
        List<String> invoiceStatus = contractCancellation.InvoiceStatus__c.split(';');
        //Extract status
        String status = responseInfo.status;
        System.debug('status: ' + status );
        
        if(status == ConstantsUtil.AMA_STATUS_SUCCESS){
            status = ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED;
        }else{
            status = ConstantsUtil.SUBSCRIPTION_STATUS_IN_CANCELLATION;
        }
        
        System.debug('status after the change of state: ' + status);
        
        //Inizialize a boolean variable to see if i activate all contract subscriptions 
        Boolean checkedSubscriptions = true;
        Date endDate = Date.today();
        endDate = endDate.addDays(-1);
        
        //Create a list of log to insert on all subscriptions
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        set<string> ids = new set<string>(responseInfo.source_element_ids);
        try {           
        //Cycle on contract subscriptions
        for(SBQQ__Subscription__c sub : [SELECT Id, Name,SBQQ__ProductName__c, SBQQ__Contract__c, Subsctiption_Status__c,Termination_Date__c, SBQQ__RequiredById__c  FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id]){
            //if sub is the subscription I have to activate
           // if( String.valueOf(sub.Id).equals(responseInfo.source_element_ids) ){
              if(ids.contains(String.valueOf(sub.Id)) || ids.contains(String.valueOf(sub.SBQQ__RequiredById__c))){
                //Change subscription status
                sub.Subsctiption_Status__c = status;
                subscriptionsToUpdate.add(sub);
                log.message__c = ContractManager.getPrettyPrint(responseInfo, sub);
                System.debug('The subscription has been changed in: ' + sub.Subsctiption_Status__c);
            }
            
            // if the subscription status is different from canceled, the contract is not active
            if(!sub.Subsctiption_Status__c.equals(ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED)){
                //block the cancellation of the contract
                checkedSubscriptions = false;
            }
            
            if(sub.Termination_Date__c  != null && sub.Termination_Date__c  > endDate){
                endDate = sub.Termination_Date__c ;
            }
        }
        
        if(subscriptionsToUpdate.size() > 0){
            update subscriptionsToUpdate;
        }
        
        integer count = [SELECT count() FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id and Subsctiption_Status__c != :ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED];
        if(count == 0){
            contract.Status = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
            contract.TerminateDate__c  = endDate;
            contract.Cancel_Date__c = endDate;
            //START SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
            Database.SaveResult res = Database.update(contract);
            
			if(res.isSuccess())
            {    
                Map<Id, Invoice__c> invoiceToCancel = SEL_Invoices.getInvoicesByContractIDAndInvoiceStatus(contract.Id,invoiceStatus);
				System.debug ('invoiceToCancel' + invoiceToCancel);
                if(!invoiceToCancel.isEmpty())
                {
                    List<AggregateResult> results = SEL_InvoiceOrders.countInvoices(invoiceToCancel.keySet());
                    
                    System.debug('AggregateResult' + results );
                    for (AggregateResult r : results) {
                        if( r.get('number') == 1) {
                            System.debug ('Aggregate result number 1');
                           SRV_Invoice.cancelInvoices(new Set<Id> (invoiceToCancel.keySet())); 
                        } 
                        else {
                            System.debug ('Invoice is referenced in more than one contract');
                        }
                    }
                    
                }    
            }

            //END SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
            
            System.debug('Contract cancelled'+contract);
        }
       }catch (Exception ex) {
           	ErrorHandler.logInfo('ContractManager', '', 'activateSubscriptionOnly '+ex.getMessage());
        }  
    }
    
    public static void terminationContract(ResponseInterface responseInfo, Contract contract, Log__c log){
        //Extract the status
        String status = responseInfo.status;

        System.debug('terminationContract> Status after status change: ' + status);        
        if(status == ConstantsUtil.AMA_STATUS_SUCCESS){
            status = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
        }else{
            status = ConstantsUtil.SUBSCRIPTION_STATUS_IN_TERMINATION;
        }
        
        System.debug('terminationContract Status after status change: ' + status);
        //Initialize a boolean to see if I have activated all contract subscriptions 
        Boolean checkedSubscriptions = true;
        Date endDate = Date.today();
        endDate = endDate.addDays(-1);
        
        List<SBQQ__Subscription__c> subscriptionsToUpdate = new List<SBQQ__Subscription__c>();
        set<string> ids = new set<string>(responseInfo.source_element_ids);
        try {
           
        //Cycle on contract subscriptions
        for(SBQQ__Subscription__c sub : [SELECT Id, Name,SBQQ__ProductName__c, SBQQ__Contract__c, SBQQ__RequiredById__c, Subsctiption_Status__c, Termination_Date__c  
                                         FROM SBQQ__Subscription__c 
                                         WHERE SBQQ__Contract__c =: contract.Id]){
            //If sub is the subscription needs to be activated
            //if( String.valueOf(sub.Id).equals(responseInfo.source_element_ids) ){
              if(ids.contains(String.valueOf(sub.Id)) || ids.contains(String.valueOf(sub.SBQQ__RequiredById__c))){
                //Change subscription status if date today else keep it in same status
                if(sub.Termination_Date__c <= Date.today()){
                    sub.Subsctiption_Status__c = status;
                  subscriptionsToUpdate.add(sub);
                }

                log.message__c = ContractManager.getPrettyPrint(responseInfo, sub);
                System.debug('Status changed of the subscription in: ' + sub.Subsctiption_Status__c);
            }
            
            // if the subscription status is different from Terminated the contract is not active
            if(!sub.Subsctiption_Status__c.equals(ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED)){
                //block the termination of the contract of the contract 
                checkedSubscriptions = false;
            }
            
            if(sub.Termination_Date__c  != null && sub.Termination_Date__c  > endDate){
                endDate = sub.Termination_Date__c ;
            }
        }
        
        if(subscriptionsToUpdate.size() > 0){
            update subscriptionsToUpdate;
        }
        
       Integer countsubs;
       countsubs = [SELECT count() FROM SBQQ__Subscription__c WHERE SBQQ__Contract__c =: contract.Id AND Subsctiption_Status__c!=:ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED];
        
        if(countsubs==0){
            contract.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
            contract.TerminateDate__c  = endDate;
            update contract;
            billing.cancelcontract(contract.Id);

        }   
        }catch (Exception ex) {
           	ErrorHandler.logInfo('ContractManager', '', 'terminationContract '+ex.getMessage());
        }     
    }
  
    @TestVisible  private class ResponseInterface{
        public string status{get; set;}
        public List<string> source_element_ids {get; set;}
        public ResponseInterfaceTechnical technical_fields{get; set;}
        public ResponseInterfaceDetails details{get; set;}
        
        public ResponseInterface(){
            technical_fields = new ResponseInterfaceTechnical();
            details = new ResponseInterfaceDetails();
            source_element_ids = new List<string>();
        }
    }
    
    @TestVisible  private class ResponseInterfaceTechnical{
        public string correlation_id {get; set;}
    }
    
    @TestVisible  private class ResponseInterfaceDetails{
        public string error_code {get; set;}
        public string  error_description {get; set;}
    }
/*
    @InvocableMethod(label='Contract Manager Retry CallBack Update' description='Retry CallBack Update if any log did not updated with transaction')
    public static void RetryCallBackUpdate(List<Id> logIds) {
      
         Set<Id> SetlogIds = new Set<Id>();
        try {   
            for(Id lId : logIds){
                SetlogIds.add(lId);
            }
            ContractManager.RetryCallBackTranUpdate(SetlogIds);
        }catch (Exception ex) {
            ErrorHandler.logInfo('ContractUtility', '', 'TriaActivationQueueJob '+ex.getMessage());
        }
    }

    @Future
    public static void RetryCallBackTranUpdate(Set<Id> SetlogIds) {
        try{
            List<Log__c> logToUpdate = new List<Log__c>();

            // //Extract the contract with callId of the call
            List<Contract> contracts = new List<Contract>();
            Id quoteId = null;
            // String namingLog = idCall+' '+ConstantsUtil.Request;

            // try {
            //     contracts = [SELECT id, CallId__c, Status, EndDate,SBQQ__Quote__c FROM Contract WHERE CallId__c =: idCall  limit 1];
            // } catch(Exception e) {
            //     System.debug('No contract found for this correlation_id');
            // }
            ResponseInterface responseInfo;
            Log__c log_req;
            //get response

        List<Log__c> listLog=[SELECT Id, Quote__c, Correlation_Id__c, Contract__c,Body_Message__c  FROM Log__c WHERE Id IN :SetlogIds];

        for(Log__c log : listLog){
        // for(Log__c log : [SELECT Id, Quote__c, Correlation_Id__c, Contract__c,Body_Message__c  FROM Log__c WHERE Id IN :logIds]){
            // for(Log__c log : logs){            
               try {   
                responseInfo = (ResponseInterface)JSON.deserialize(log.Body_Message__c, ResponseInterface.class);
                System.debug('responseInfo '+responseInfo);
                String correlation_id = responseInfo.technical_fields.correlation_id;
                //get request 
                log_req = [SELECT Id, Quote__c, Correlation_Id__c, Contract__c  FROM Log__c WHERE Correlation_Id__c =: correlation_id AND Name like '%Request%' limit 1];

                quoteId = log_req.Quote__c;
            } catch(Exception e) {
                System.debug('No log found for this quote by this correlation_id'+ e.getMessage());
            }
            if(log_req.Contract__c!=''){
            try {
                    contracts = [SELECT id, CallId__c, Status, EndDate,SBQQ__Quote__c FROM Contract WHERE id =:  log_req.Contract__c  limit 1];
                } catch(Exception e) {
                    System.debug('No log found for this contract by this correlation_id');
                }
            }
            System.debug('contracts:'+contracts.size());
            //If exist a contract with this CallId
            if(!contracts.isEmpty()) {
                System.debug('there is a contract with this call id');
                Contract contract = contracts.get(0);
                System.debug('contract: ' + contracts);
                log.contract__c = contract.id;
                string status = contract.status;
                System.debug('the contract is in '+status+' status');
                
                if( status.equals(ConstantsUtil.CONTRACT_STATUS_PRODUCTION) || status.equals(ConstantsUtil.CONTRACT_STATUS_DRAFT) ){
                    ContractManager.activateSubscription( responseInfo, contract, log);
                } else if( status.equals(ConstantsUtil.CONTRACT_STATUS_IN_CANCELLATION) ){
                    ContractManager.cancellationContract(responseInfo, contract, log);
//                } else if( status.equals(ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION) || status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE)){
                } else if( status.equals(ConstantsUtil.CONTRACT_STATUS_IN_TERMINATION)){                    
                    ContractManager.terminationContract(responseInfo, contract, log);
                }
                else if( status.equals(ConstantsUtil.CONTRACT_STATUS_ACTIVE) ){
                    ContractManager.activateSubscriptionOnly( responseInfo, contract, log);
                }
               
            } else if(quoteId != null) {
                log.Quote__c = quoteId;     
           
                ContractManager.activateQuoteTrial(responseInfo, quoteId, log);
            } else {
                    //
            } 
            logToUpdate.add(log);
            //  System.debug('log '+ log);
            update log;
        }

        } catch(Exception e) {
            Log__c log = new Log__c(Name = ConstantsUtil.RESPONSE+'_' + String.valueOf(Datetime.now()), Body_Message__c = e.getMessage(),
            Message__c = e.getMessage());
            insert log;
        }
        
    }    
    */
}