@isTest
public class CaseManagementFakeUpdatePBTest {
	
    @isTest
    static void testContactCreation()
    {
				try{
                     Contact contactt= New Contact();
          							        
          							contactt.Phone='01700000000';
                                  	contactt.Email='iqbal.rocky@arollotech.com';
        							contactt.PO_Box__c= 'Ramna';
         							contactt.PO_BoxCity__c= 'Dhaka'; 
         							contactt.PO_BoxZip_PostalCode__c	='Dhaka-1000';
         							contactt.LastName = 'Test';
         							contactt.FirstName='Chu';
					 	
        
        			insert contactt;  
       					system.debug('Contact'+ contactt);

					}catch(Exception e){	                            
           								System.Assert(false, e.getMessage());
										}        
        
        
    }
    
    @isTest
    static void testAccountCreation()
    {
				try{
                     // Create common accounts    
                    Account testAccount = new Account(
                        Name = 'TestAccount'
                        ,BillingCity  = 'PT'
                        ,BillingPostalCode = '8765'
                        ,BillingCountry  = 'Italy'
                        ,BillingStreet = 'Street'
                        ,Email__c = 'acc@arollotech.com'
                        ,Phone='07412345'
                    ); 
                    
                    insert testAccount;   

					}catch(Exception e){	                            
           								System.Assert(false, e.getMessage());
										}        
        
        
    }
    
    @isTest
    static void testUpdateCaseFromWeb()
    {
        try{
            testContactCreation();
            Case cs =  new Case();
        				cs.SuppliedEmail = 'iqbal.rocky@arollotech.com';
        				cs.Origin = 'Web';

      	 test.startTest();
         insert cs;
         update cs;
         test.stopTest();

			}catch(Exception e){	                            
           							System.Assert(false, e.getMessage());
								}
 
    }

@isTest
    static void testUpdateCaseFromEmail()
    {
        try{
             testContactCreation();
        
        			Case cs =  new Case();
        				cs.SuppliedEmail = 'iqbal.rocky@arollotech.com';
        				cs.Origin = 'Email';

      	 test.startTest();
         insert cs;
         update cs;
         test.stopTest();

			}catch(Exception e){	                            
           							System.Assert(false, e.getMessage());
								}
          
    } 
    
    @isTest
    static void testAccount()
    {
        try{
            
            testAccountCreation();
            testContactCreation();
        	Account a = [Select id from Account where Name = 'TestAccount'];
            Contact c = [Select id from Contact where email = 'iqbal.rocky@arollotech.com'];
        	Case cs =  new Case();
        				cs.SuppliedPhone = '07412345';
        				cs.Origin = 'Web';
                        cs.subject = 'GE04';
            			cs.Forwarded__c = 'Yes';
            			cs.AccountId = a.id;
            			cs.ContactId = c.id;

           	Case cs1 =  new Case();
        				cs1.SuppliedPhone = '07412345';
        				cs1.Origin = 'Web';
                        cs1.subject = 'GE04';
            			cs1.Forwarded__c = 'Yes';
            			cs1.AccountId = a.id;
            			cs1.ContactId = c.id;
            			
            
      	 test.startTest();
         insert cs;
         update cs;
         insert cs1;
         update cs1;
         test.stopTest();

			}catch(Exception e){	                            
           							System.Assert(false, e.getMessage());
								}
          
    }   
    
    @isTest
    static void testIBM()
    {
        try{
            
            testContactCreation();

        	Case cs =  new Case();
        				cs.SuppliedPhone = '07412345';
        				cs.Origin = 'Email';
                        cs.subject = 'GE04';
            			cs.Forwarded__c = 'Yes';

           
            
      	 test.startTest();
         insert cs;
         update cs;
         test.stopTest();

			}catch(Exception e){	                            
                System.Assert(false, e.getMessage());
            }
          
    }   
    @isTest
    static void testManual()
    {
        try{
            
            testContactCreation();

        	Case cs =  new Case();
        				cs.SuppliedPhone = '07412345';
        				cs.Origin = 'Manual';
                        cs.subject = 'test';
            			cs.Forwarded__c = 'Yes';
            			cs.Run_Assignment__c = true;

           
            
      	 test.startTest();
         insert cs;
         update cs;
         test.stopTest();

			}catch(Exception e){	                            
                System.Assert(false, e.getMessage());
            }
          
    }   
    
}