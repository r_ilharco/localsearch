/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.      
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Alessandro Luigi Marotta <amarotta@deloitte.it>
 * Date		   : 04-11-2019
 * Sprint      : 1
 * Work item   : 25
 * Package     : 
 * Description : Test class for BillingProfileTriggerHandler
 * Changelog   : 
 */


@IsTest
public with sharing class Test_BillingProfileTriggerHandler {

    @TestSetup
    static void setup(){
        List<Account> accs = Test_DataFactory.createAccounts('test acc', 1);
        insert accs;
        List<Contact> conts = Test_DataFactory.createContacts(accs.get(0), 1);
        insert conts;
        List<Billing_Profile__c> bp = Test_DataFactory.createBillingProfiles(accs, conts, 1);
        bp.get(0).Is_Default__c = false;
        insert bp;
        Bypass_Triggers__c setting = new Bypass_Triggers__c();
        setting.Trigger_Name__c = 'Trying';
        insert setting;
        /*bp.get(0).Is_Default__c = false;
        update bp;*/
    }


    @isTest
    static void testAfterInsert(){
        
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id, Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.AfterInsert(profiles);
        Test.stopTest();
    }

    @isTest
    static void testAfterUpdate(){
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id, Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            /*BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.AfterUpdate(profiles,profiles);*/
            profiles.values().get(0).Is_Default__c = true;
            update profiles.values();
        Test.stopTest();
    }


    @isTest
    static void testBeforeInsert(){
        
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id,Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.BeforeInsert(profiles.values());
        Test.stopTest();
        
    }

    @isTest
    static void testBeforeUpdate(){

        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id,Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            /*BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.BeforeUpdate(profiles, profiles);*/
            profiles.values().get(0).Is_Default__c = true;
            update profiles.values();
        Test.stopTest();

    }

    @isTest
    static void testBeforeDelete(){
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id,Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            /*BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.BeforeDelete(profiles);*/
            delete profiles.values();
        Test.stopTest();
    }

    

    @isTest
    static void testAfterDelete(){
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id,Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            /*BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.AfterDelete(profiles);*/
            delete profiles.values();
        Test.stopTest();
    }

    @isTest
    static void testAfterUndelete(){
        Map<Id, Billing_Profile__c> profiles = new Map<Id, Billing_Profile__c>([select id,Customer__c, Is_Default__c from Billing_Profile__c]);

        Test.startTest();
            /*BillingProfileTriggerHandler instance = new BillingProfileTriggerHandler();
            instance.AfterUndelete(profiles);*/
            delete profiles.values();
            undelete profiles.values();
        Test.stopTest();
    }

}