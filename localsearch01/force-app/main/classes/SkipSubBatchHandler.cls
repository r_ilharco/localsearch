public class SkipSubBatchHandler {

    public static void processRecords(List<SBQQ__Subscription__c> subsList) {
        Set<Id> masterSubsIdSet = new Set<Id>();
        Map<Id, SBQQ__Subscription__c> subscriptionsToUpdateMap = new Map<Id, SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> subsriptionsList = querySubsToProcess(subsList);
        for(SBQQ__Subscription__c subToCheckItem : subsriptionsList){
            Decimal totalToCheck = 0;
            Decimal discountAmount = 0;
            Integer totalMonths = 0;
            Decimal baseTerm = 0;
            Integer numberOfMonths = 0;
            Invoice_Cycle__mdt billing_frequency = new Invoice_Cycle__mdt();
            if(subToCheckItem.SBQQ__BillingFrequency__c != null){
                billing_frequency = queryInovoiceCycleMdt(subToCheckItem.SBQQ__BillingFrequency__c);
            }else{
                billing_frequency = queryInovoiceCycleMdt(subToCheckItem.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__BillingFrequency__c);
            }  
            numberOfMonths = (Integer)billing_frequency.Number_of_months__c;
            totalToCheck = subToCheckItem.SBQQ__QuoteLine__r.SBQQ__Quantity__c * subToCheckItem.SBQQ__QuoteLine__r.SBQQ__ListPrice__c;
            if(isDiscount(subToCheckItem)){
                discountAmount = (totalToCheck/100) * subToCheckItem.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c;
                totalToCheck = totalToCheck-discountAmount;
            }
            if(subToCheckItem.SBQQ__QuoteLine__r.Base_term_Renewal_term__c != null){
                baseTerm = subToCheckItem.SBQQ__QuoteLine__r.Base_term_Renewal_term__c;
            }else{
                baseTerm = subToCheckItem.SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Base_term_Renewal_term__c;
            }
            if(!subToCheckItem.SBQQ__Product__r.One_time_Fee__c){
                if(baseTerm >= numberOfMonths){
                    totalToCheck = totalToCheck/(baseTerm/numberOfMonths);
                }else{
                    totalToCheck = totalToCheck * (numberOfMonths/baseTerm);
                }
            }
            if(isFlagsAreChecked(subToCheckItem, totalToCheck)){
                subToCheckItem.Technical_Billing_Issue__c = true;
                subToCheckItem.SkipBilling__c = true;
                if(subToCheckItem.RequiredBy__c != null){
                    masterSubsIdSet.add(subToCheckItem.RequiredBy__c);
                }else{
                    masterSubsIdSet.add(subToCheckItem.Id);
                } 
            }else{
                subToCheckItem.SkipBilling__c = false;
                subToCheckItem.Technical_Billing_Issue__c = false;
            }
            subscriptionsToUpdateMap.put(subToCheckItem.id, subToCheckItem);           
        }    
        if(masterSubsIdSet.size() > 0){
            updateSubscriptions(masterSubsIdSet, subscriptionsToUpdateMap);
        }
    }

    private static void updateSubscriptions(Set<Id> masterSubsId, Map<Id, SBQQ__Subscription__c> subscriptionsToUpdate) {
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        
        subsToUpdate = [SELECT  Id, Total_Amount_To_Bill__c, Wrong_Amount_To_Bill__c, SkipBilling__c, Technical_Billing_Issue__c
                                     FROM SBQQ__Subscription__c 
                                     WHERE Id IN :masterSubsId OR RequiredBy__c IN :masterSubsId];
        for(SBQQ__Subscription__c subscriptionItem : subsToUpdate){
            subscriptionItem.SkipBilling__c = true;
            subscriptionItem.Technical_Billing_Issue__c = true;
            if(!subscriptionsToUpdate.containsKey(subscriptionItem.Id)){
                subscriptionsToUpdate.put(subscriptionItem.Id, subscriptionItem);
            }else{
                SBQQ__Subscription__c tempSub = subscriptionsToUpdate.get(subscriptionItem.Id);
                tempSub.SkipBilling__c = true;
                subscriptionItem.Technical_Billing_Issue__c = true;
                subscriptionsToUpdate.put(tempSub.Id, tempSub);
            }                
        }
        if(subscriptionsToUpdate.values().size() > 0){
            update subscriptionsToUpdate.values();
        }
    }

    private static Boolean isDiscount(SBQQ__Subscription__c subToCheck) {
        return subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null && subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != 0 && !subToCheck.SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c;
    }

    private static Boolean isFlagsAreChecked(SBQQ__Subscription__c subToCheck, Decimal totalToCheck) {
        return (subToCheck.Total_Amount_To_Bill__c != totalToCheck || subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c > 100 || 
        (subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c < 0 && subToCheck.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c != null));
    }

    private static Invoice_Cycle__mdt queryInovoiceCycleMdt(String label) {
        return [SELECT Number_of_months__c FROM Invoice_Cycle__mdt WHERE MasterLabel = :label];
    }

    private static List<SBQQ__Subscription__c> querySubsToProcess(List<SBQQ__Subscription__c> subsList) {
        Set<Id> subIdSet = new Set<Id>();
        for(SBQQ__Subscription__c subscriptionItem : subsList){
            subIdSet.add(subscriptionItem.Id);
        }
        List<SBQQ__Subscription__c> subsToReturnList = new List<SBQQ__Subscription__c>([SELECT  Id, SBQQ__QuoteLine__r.Base_term_Renewal_term__c, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Base_term_Renewal_term__c, 
                                                                                        SBQQ__QuoteLine__r.Subscription_Term__c, SBQQ__QuoteLine__r.SBQQ__Quantity__c, SBQQ__QuoteLine__r.SBQQ__ListPrice__c, 
                                                                                        SBQQ__QuoteLine__r.SBQQ__Discount__c, SBQQ__QuoteLine__r.SBQQ__NonDiscountable__c, SBQQ__Product__r.One_time_Fee__c, Total__c,
                                                                                        SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c, SBQQ__QuoteLine__r.SBQQ__AdditionalDiscountAmount__c,
                                                                                        SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Subscription_Term__c, Total_Amount_To_Bill__c, Wrong_Amount_To_Bill__c, SkipBilling__c,
                                                                                        SBQQ__BillingFrequency__c, SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.SBQQ__BillingFrequency__c, RequiredBy__c, SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c
                                                                                        FROM SBQQ__Subscription__c
                                                                                        WHERE Id IN :subIdSet]);
        return subsToReturnList;
    }
}
