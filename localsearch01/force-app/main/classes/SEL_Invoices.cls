/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author      : Luciano di Martino <ldimartino@deloitte.it> / Sara Dubbioso <sdubbioso@deloitte.it>
 * Date        : 18-04-2019
 * Sprint      : 1
 * Work item   : #179
 * Testclass   : Test_AbacusTransferJob
 * Package     : invoice
 * Description : This class contains some useful methods to retrieve Invoices 
 * Changelog   : 
 */

//AS-22, Hotfix2, ldimartino : WHERE condition modified in order to stop sending cancelled invoices
//AS-09, Hotfix2, ldimartino : Fields retrieved from the query have been modified
public class SEL_Invoices {    
    public static List<Invoice__c> getInvoices(String modeSwissList, Boolean isPartiallyPayed, Boolean registeredFalse, String statusCollected, String statusCancelled,
                                               String paymentStatusC1, String paymentStatusC2, String paymentStatusC3, String statusOverdue, String statusPayed){
    	return [SELECT 	Id
                    	,Name
                        ,Correlation_Id__c
                        ,Invoice_Date__c
                        ,Invoice_Code__c
                        ,Process_Mode__c
                        ,Rounding__c
                        ,Tax__c
                        ,Tax_Mode__c
                        ,Total__c
                        ,Status__c
                        ,Billing_Channel__c
                        ,Customer__r.Customer_Number__c
                        ,Customer__r.Name
                        ,Open_Amount__c
                        ,Payment_Reference__c
                        ,Billing_Profile__r.Billing_Street__c
                        ,Billing_Profile__r.Billing_City__c
                        ,Billing_Profile__r.Billing_Country__c
                        ,Billing_Profile__r.Billing_Postal_Code__c
                        ,Billing_Profile__r.Billing_State__c
                        ,Billing_Profile__r.P_O_Box__c
                        ,Billing_Profile__r.P_O_Box_City__c
                        ,Billing_Profile__r.P_O_Box_Zip_Postal_Code__c
                        ,Payment_Date__c
                FROM 	Invoice__c
                WHERE  	Booking_Status__c = :ConstantsUtil.ABACUS_STATUS_DRAFT
               	AND 	Status__c != :statusCancelled
               	AND 	Payment_Status__c != :paymentStatusC1
               	AND 	Payment_Status__c != :paymentStatusC2
               	AND 	Payment_Status__c != :paymentStatusC3
               	AND 	(
                    		(
                                Process_Mode__c = :modeSwissList
                       			AND Is_Partially_Payed__c = :isPartiallyPayed
                          	)
                      		OR ( 
                                	Process_Mode__c != :modeSwissList 
                           			AND ( 
                                        	Status__c = :statusCollected 
                                  			OR Status__c = :statusOverdue 
                                  			OR Status__c = :statusPayed 
                                    	) 
                            	) 
                		) 
        		LIMIT  50000 ];
    }
    
    public static Map<Id, Invoice__c> getInvoicesByContractIDAndInvoiceStatus(Id contractId, List<String> status)
    {
        return new Map<Id, Invoice__c>([
            SELECT Id, Name, Correlation_Id__c, Status__c, 
                    Customer__r.Customer_Number__c, Customer__r.Name 
            FROM Invoice__c 
            WHERE Id IN (SELECT Invoice__c FROM Invoice_Order__c WHERE Contract__c = :contractId)
            AND Status__c IN :status
            ]);
    }
    
    public static Map<Id, Invoice__c> getInvoicesByContractIDsAndStus(Set<Id> contractIds, List<String> status){
        return new Map<Id, Invoice__c>([
            SELECT Id, Name, Correlation_Id__c, Status__c, 
                    Customer__r.Customer_Number__c, Customer__r.Name 
            FROM Invoice__c 
            WHERE Id IN (SELECT Invoice__c FROM Invoice_Order__c WHERE Contract__c in:contractIds)
            AND Status__c IN :status
            ]);
    }
    
    public static Map<Id, Invoice__c> getInvoicesById(Set<Id> ids)
    {
        return new Map<Id, Invoice__c>([
            SELECT Id, Name, Correlation_Id__c, Status__c, 
                    Customer__r.Customer_Number__c, Customer__r.Name 
            FROM Invoice__c 
            WHERE Id IN :ids
            ]);
    }
    
}