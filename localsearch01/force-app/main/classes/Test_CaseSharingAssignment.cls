@isTest
public class Test_CaseSharingAssignment {
	
    @testSetup
    public static void test_setupData(){
        
        Account account = Test_DataFactory.generateAccounts('Test Name Account', 1, false)[0];
        insert account;
        
        Profile assignedProfile = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SALESPROFILE_LABEL LIMIT 1];
        User user = new User(ProfileId = assignedProfile.Id,
                             LastName = 'last',
                             Email = 'testclass@user.com',
                             Username = 'testclass@user.com'+System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'test',
                             Alias = 'test',
                             TimeZoneSidKey='America/Los_Angeles',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_US'
                            );
        UserTriggerHandler.disableTrigger = true;
        insert user;
        UserTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_shareCaseToUser(){
        
        User user = [SELECT Id FROM User WHERE Email = 'testclass@user.com'];
        
        Case internalSupportCase = new Case();
        
        Test.startTest();
        System.runAs(user){
            internalSupportCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.CASE_RECORDT_INTERNALSUPP).getRecordTypeId();
            internalSupportCase.Status = ConstantsUtil.CASE_NEW_STATUS;
            internalSupportCase.SuppliedEmail = 'testclass@mail.com';
            internalSupportCase.Origin = ConstantsUtil.CASE_ORIGIN_WEB;
            internalSupportCase.Topic__c = ConstantsUtil.CASE_TOPIC_OTHER;
            internalSupportCase.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
            internalSupportCase.Employee__c = user.Id;
            insert internalSupportCase;
            System.debug(internalSupportCase.Id);
        }
        internalSupportCase.OwnerId = UserInfo.getUserId();
        update internalSupportCase;
        Test.stopTest();
    }
}