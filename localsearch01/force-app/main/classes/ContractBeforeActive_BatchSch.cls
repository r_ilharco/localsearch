global class ContractBeforeActive_BatchSch implements Database.Batchable<SObject>, Schedulable, Database.AllowsCallouts {
    
    private String sSQL=null;
    public ContractBeforeActive_BatchSch() //String query 
    {
    }
    
    global database.querylocator start(Database.BatchableContext bc)
    {         
        Date dt_from = Date.valueOf(System.Today()) - (Integer.valueOf(Label.Contract_job_Prior_days));
        Date dt_to = Date.valueOf(System.Today());       
        String status=ConstantsUtil.CONTRACT_STATUS_DRAFT;    
        List<Contract> Contracttoupdate = new List<Contract>();

        sSQL = 'SELECT CallId__c,ID FROM Contract WHERE Status = :status AND StartDate >= :dt_from AND StartDate <= :dt_to AND SBQQ__Quote__c !=NULL AND CallId__c = NULL';

        return Database.getQueryLocator(sSQL);   
    }
    
    global void execute(Database.BatchableContext bc, Contract[] listContract)
    {
        List<Contract> Contracttoupdate = new List<Contract>();
        for (Contract Contract:listContract){ 
            contract.CallId__c = ContractUtility.getUniqueId(contract.Id);
            contract.Status =     ConstantsUtil.CONTRACT_STATUS_PRODUCTION;
            Contracttoupdate.add(contract);
            System.debug(contract.Id);
        }
    update Contracttoupdate;
     
    }
    global void finish(Database.BatchableContext bc)
    {        
    }
    
    global void execute(SchedulableContext sc)
    {
        //System.schedule('Trial Activation Request Schedulabled', '0 0 13 * * ?', new ContractBeforeActive_BatchSch());
        
        ContractBeforeActive_BatchSch job = new ContractBeforeActive_BatchSch();
        Database.executeBatch(job,50); 
    }
}