/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes		  Reference to SalesUser and AreaCode fields removed
* @modifiedby     Mazzarella Mara <mamazzarella@deloitte.it>
* 2020-06-29       
*				
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class QuoteTerritoryChangedBatch implements Database.Batchable<SObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
    private List<Id> accountIds;
    private String sSQL = null;
    public QuoteTerritoryChangedBatch(List<Id> accountIds){
        this.accountIds = new List<Id>(accountIds);
        system.debug('accountIds in constructor: ' + accountIds);
    }
    
    global database.querylocator start(Database.BatchableContext bc){      
        system.debug('accountIds: '+ this.accountIds);
        sSQL = 'SELECT Id, SBQQ__Opportunity2__r.Account.Owner_Changed__c, SBQQ__Opportunity2__r.AccountId  FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__r.AccountId IN :accountIds';
        system.debug(sSQL);
        return Database.getQueryLocator(sSQL);
    }
    
    global void execute(Database.BatchableContext bc, List<SBQQ__Quote__c> scope){
        Map<Id, SBQQ__Quote__c> quotes = new Map<Id, SBQQ__Quote__c>(scope);
        system.debug('quotes: ' + quotes);
        try{
            Map<Id, String> quoteToAreaCode = new Map<Id,String>();
            Map<Id, Group> groupsMap = new Map<Id, Group>();
            List<Id> territoryIds = new List<Id>();
            Map<String,Id> thirdToSecondLevel = new Map<String,Id>();
            Set<String> terrDevName = new Set<String>();
            List<UserTerritory2Association> territoryAssociations = new List<UserTerritory2Association>();
            Map<Id, List<Id>> areaCodeTerritoryRolesMap = new Map<Id, List<Id>>();
            Map<String,Id> groupDevName = new Map<String,Id>();
            Set<Id> accIds = new Set<Id>();
            List<SBQQ__Quote__Share> newQuoteShares = new List<SBQQ__Quote__Share>();
            Map<Id, List<Id>> accountTerritoryMap = new Map<Id, List<Id>>();
            Map<Id, Id> quoteToAccountMap = new Map<Id, Id>();
            List<ObjectTerritory2Association> objTerritoryAssociations = new List<ObjectTerritory2Association>();
            
            if(quotes.values().size() > 0){
                List<SBQQ__Quote__Share> oldQuoteShares = [SELECT Id FROM SBQQ__Quote__Share WHERE RowCause = :Schema.SBQQ__Quote__Share.RowCause.Territory_Changed_Sharing__c AND ParentId IN:quotes.keySet()];
                if(oldQuoteShares.size() > 0){
                    delete oldQuoteShares;
                }
                Set<String> areaCodes = new Set<String>();
                for(SBQQ__Quote__c q : quotes.values()){
                    /*if(q.SBQQ__Opportunity2__r.Account.Area_Code__c != null){
                        areaCodes.add(q.SBQQ__Opportunity2__r.Account.Area_Code__c);
                        quoteToAreaCode.put(q.Id,q.SBQQ__Opportunity2__r.Account.Area_Code__c);
                    }else*/ 
                        if(q.SBQQ__Opportunity2__r.AccountId != null){
                        accIds.add(q.SBQQ__Opportunity2__r.AccountId);
                        quoteToAccountMap.put(q.Id, q.SBQQ__Opportunity2__r.AccountId);
                    }
                }
                /*
                if(areaCodes.size() > 0){
                    territoryAssociations = [SELECT Id, UserId, Territory2.Name, 
                                             Territory2.ParentTerritory2Id, 
                                             Territory2.ParentTerritory2.DeveloperName, 
                                             Territory2.ParentTerritory2.Name,
                                             Territory2.DeveloperName
                                             FROM UserTerritory2Association 
                                             WHERE Territory2.Name IN :areaCodes];
                    for(UserTerritory2Association terrAs : territoryAssociations){
                        
                        territoryIds.add(terrAs.Territory2.ParentTerritory2Id);
                        terrDevName.add(terrAs.Territory2.ParentTerritory2.DeveloperName);
                        terrDevName.add(terrAs.Territory2.DeveloperName);
                        thirdToSecondLevel.put(terrAs.Territory2.Name, terrAs.Territory2.ParentTerritory2Id);
                    }
                    
                    if(terrDevName.size() > 0){
                        groupsMap = SEL_Group.getGroupsByDevNameAndType(terrDevName,'Territory');
                        for(Group aGroup : groupsMap.values()){
                            groupDevName.put(aGroup.DeveloperName, aGroup.Id); 
                        }
                        
                        if(groupDevName.size() > 0){
                            for(UserTerritory2Association territory : territoryAssociations){
                                List<Id> groupIds =  new List<Id>();
                                if(groupDevName.containsKey(territory.Territory2.ParentTerritory2.DeveloperName)){
                                    groupIds.add(groupDevName.get(territory.Territory2.ParentTerritory2.DeveloperName));
                                }
                                if(groupDevName.containsKey(territory.Territory2.DeveloperName)){
                                    groupIds.add(groupDevName.get(territory.Territory2.DeveloperName));
                                }
                                if(groupIds.size() > 0){
                                    if(!areaCodeTerritoryRolesMap.containsKey(territory.Territory2.ParentTerritory2Id)){
                                        areaCodeTerritoryRolesMap.put(territory.Territory2.ParentTerritory2Id, groupIds);
                                    }else{
                                        areaCodeTerritoryRolesMap.get(territory.Territory2.ParentTerritory2Id).addAll(groupIds);
                                    }
                                    
                                }
                            }
                            for(SBQQ__Quote__c quote : quotes.values()){
                                Id secondLevelId = thirdToSecondLevel.get(quoteToAreaCode.get(quote.Id));
                                if(areaCodeTerritoryRolesMap.containsKey(secondLevelId)){
                                    for(Id groupId : areaCodeTerritoryRolesMap.get(secondLevelId)){
                                        SBQQ__Quote__Share newQShare = new SBQQ__Quote__Share();
                                        newQShare.ParentId = quote.id;
                                        newQShare.UserOrGroupId = groupId;
                                        newQShare.AccessLevel = 'Read';
                                        newQShare.RowCause = Schema.SBQQ__Quote__Share.RowCause.Territory_Changed_Sharing__c;
                                        newQuoteShares.add(newQShare);
                                    }
                                }
                            }
                            if(newQuoteShares.size() > 0){
                                insert newQuoteShares;
                            }
                        }
                    }       
                } else */if(accIds.size() > 0){
                    objTerritoryAssociations = [SELECT id, ObjectId, Territory2.DeveloperName 
                                             FROM ObjectTerritory2Association
                                             WHERE ObjectId IN :accIds];
                    for(ObjectTerritory2Association terrAs : objTerritoryAssociations){
                        terrDevName.add(terrAs.Territory2.DeveloperName);
                    }
                    if(terrDevName.size() > 0){
                        groupsMap = SEL_Group.getGroupsByDevNameAndType(terrDevName,'Territory');
                        for(Group aGroup : groupsMap.values()){
                            groupDevName.put(aGroup.DeveloperName, aGroup.Id); 
                        }
                        if(groupDevName.size() > 0){
                            for(ObjectTerritory2Association territory : objTerritoryAssociations){
                                List<Id> groupIds =  new List<Id>();
                                if(groupDevName.containsKey(territory.Territory2.DeveloperName)){
                                    groupIds.add(groupDevName.get(territory.Territory2.DeveloperName));
                                }
                                if(groupIds.size() > 0){
                                    if(!accountTerritoryMap.containsKey(territory.ObjectId)){
                                        accountTerritoryMap.put(territory.ObjectId, groupIds);
                                    }else{
                                        accountTerritoryMap.get(territory.ObjectId).addAll(groupIds);
                                    }
                                }
                            }
                            for(SBQQ__Quote__c quote : quotes.values()){
                                Id accountId = quoteToAccountMap.get(quote.id);
                                if(accountTerritoryMap.containsKey(accountId)){
                                    for(Id groupId : accountTerritoryMap.get(accountId)){
                                        SBQQ__Quote__Share newQShare = new SBQQ__Quote__Share();
                                        newQShare.ParentId = quote.id;
                                        newQShare.UserOrGroupId = groupId;
                                        newQShare.AccessLevel = 'Read';
                                        newQShare.RowCause = Schema.SBQQ__Quote__Share.RowCause.Creation_Sharing__c;
                                        newQuoteShares.add(newQShare);
                                    }
                                }
                            }
                            if(newQuoteShares.size() > 0){
                                insert newQuoteShares;
                            }
                        }
                    }
                }
            }
        }catch(Exception ex){
            system.debug('stackTrace: ' + ex.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext bc){     
        
    }
    
    global void execute(SchedulableContext sc){
    }
    
}