public class TriggerHelper {
    
    public static boolean isTriggerDisabled(string triggername) {
		return !String.isBlank(triggername) && (
            	isStringInField(triggername, Bypass_Triggers__c.getOrgDefaults()) ||
            	isStringInField(triggername, Bypass_Triggers__c.getInstance(Userinfo.getProfileId())) ||
            	isStringInField(triggername, Bypass_Triggers__c.getInstance(Userinfo.getUserId()))
            );
    }
    
    @testVisible
    private static boolean isStringInField(string value, Bypass_Triggers__c config) {
        if(config == null || String.isBlank(config.Trigger_Name__c)) return false;
        return config.Trigger_Name__c.trim().split('\\s*,\\s*').contains(value.trim());
    }
}