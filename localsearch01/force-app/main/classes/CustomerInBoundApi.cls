/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 14-05-2019
 * Sprint      : 1
 * User Story  : 24
 * Testclass   : Test_CustomerInBoundApi
 * Package     : 
 * Description : 
 * Changelog   : 
 */

@RestResource(urlMapping='/Accounts/*')
global with sharing class CustomerInBoundApi{
	@HttpGet
    global static void getAccount(){
        RestResponse res = RestContext.response;
        String accGoldenRecordId = RestContext.request.params.get('CustomerNumber');
        try {
            Account acc = SEL_Account.getAccount(accGoldenRecordId);
            CustomerInfo cInfo = new CustomerInfo(acc);
            res.responseBody = Blob.valueOf(JSON.serialize(cInfo));
            res.statusCode = 200;
            LogUtility.saveAccountResponse(RestContext.request,JSON.serialize(cInfo), accGoldenRecordId ,acc ,true,null );
        } catch (Exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
            LogUtility.saveAccountResponse(RestContext.request,'', accGoldenRecordId ,null ,true,null );
        }
    }

    public class CustomerInfo{

        String id;
        String name;
        String billing_street;
        String billing_city;
        String billing_state;
        String billing_postal_code;
        String billing_country;
        String shipping_street;
        String shipping_city;
        String shipping_state;
        String shipping_postal_code;
        String shipping_country;
        String phone;
        String email;
        String lcm_customer_number;
        String po_box;
        String po_box_city;
        String po_box_postal_code;
        String preferred_language;
        String status;
        String mobile_phone;
        String fax;
        String website;
        String uid;
        String sales_channel;
        String client_rating;
    
        public CustomerInfo(Account acc){

            this.id = acc.Id;
            this.name = acc.Name;
            this.billing_street = acc.BillingStreet;
            this.billing_city = acc.BillingCity;
            this.billing_state = acc.BillingState;
            this.billing_postal_code = acc.BillingPostalCode;
            this.billing_country = acc.BillingCountry;
            this.shipping_street = acc.ShippingStreet;
            this.shipping_city = acc.ShippingCity;
            this.shipping_state = acc.ShippingState;
            this.shipping_postal_code = acc.ShippingPostalCode;
            this.shipping_country = acc.ShippingCountry;
            this.phone = acc.Phone;
            this.email = acc.Email__c;
            this.lcm_customer_number = acc.GoldenRecordID__c;
            this.po_box = acc.POBox__c;
            this.po_box_city = acc.P_O_Box_City__c;
            this.po_box_postal_code = acc.P_O_Box_Zip_Postal_Code__c;
            this.preferred_language = acc.PreferredLanguage__c;
            this.status = acc.Status__c;
            this.mobile_phone = acc.Mobile_Phone__c;
            this.fax = acc.Fax;
            this.website = acc.Website;
            this.uid = acc.UID__c;
            this.sales_channel = acc.SalesChannel__c;
            this.client_rating = acc.ClientRating__c;

        }
    
    }

}