public class CheckTotalInvoiceHandler {
   

    public static void processRecords(List<Invoice__c> invoicesList) {
        Map<Id, Invoice__c> invoiceMap = new Map<Id, Invoice__c>();
        Map<Id, List<Invoice_Item__c>> invoiceItemMap = new Map<Id, List<Invoice_Item__c>>();
        Map<Id, Set<Id>> invoiceSubsMap = new Map<Id, Set<Id>>();
        Set<Id> subsIdSet = new Set<Id>();
        Map<Id, SBQQ__Subscription__c> subsRecordMap = new Map<Id, SBQQ__Subscription__c>();
        for(Invoice__c invoiceItem : invoicesList) {
            invoiceMap.put(invoiceItem.Id, invoiceItem);
        }
        List<Invoice_Item__c> invoiceItemList = queryInvoiceItems(invoiceMap.keySet());
        for(Invoice_Item__c invItem : invoiceItemList) {
            if(!invoiceItemMap.containsKey(invItem.Invoice_Order__r.Invoice__c)) {
                invoiceItemMap.put(invItem.Invoice_Order__r.Invoice__c, new List<Invoice_Item__c>());
            }
            invoiceItemMap.get(invItem.Invoice_Order__r.Invoice__c).add(invItem);

            if(!invoiceSubsMap.containsKey(invItem.Invoice_Order__r.Invoice__c)) {
                invoiceSubsMap.put(invItem.Invoice_Order__r.Invoice__c, new Set<Id>());
            }
            invoiceSubsMap.get(invItem.Invoice_Order__r.Invoice__c).add(invItem.Subscription__c);
            subsIdSet.add(invItem.Subscription__c);
        }
        List<SBQQ__Subscription__c> subsList = querySubcriptions(subsIdSet);
        for(SBQQ__Subscription__c subsItem : subsList) {
            subsRecordMap.put(subsItem.Id, subsItem);
        }
        updateInvoices(invoicesList, invoiceSubsMap, invoiceItemMap, subsRecordMap);
    } 

    private static List<Invoice_Item__c> queryInvoiceItems(Set<Id> invoiceIdSet){
        List<Invoice_Item__c> invoiceItemList = new List<Invoice_Item__c>([SELECT Id, Subscription__c, Amount__c, Invoice_Order__r.Invoice__c  FROM Invoice_Item__c WHERE Invoice_Order__r.Invoice__c IN :invoiceIdSet]);
        return invoiceItemList;
    }

    private static List<SBQQ__Subscription__c> querySubcriptions(Set<Id> subsIdSet){
        List<SBQQ__Subscription__c> subsList = new List<SBQQ__Subscription__c>([SELECT Id, Total_Amount_To_Bill__c FROM SBQQ__Subscription__c WHERE Id IN :subsIdSet]);
        return subsList;
    }

    private static void updateInvoices(List<Invoice__c> invoicesList, Map<Id, Set<Id>> invoiceSubsMap, Map<Id, List<Invoice_Item__c>> invoiceItemMap, Map<Id, SBQQ__Subscription__c> subsRecordMap) {
        List<Invoice__c> toUpdateList = new List<Invoice__c>();
        for(Invoice__c invoiceItem : invoicesList) {
            Decimal invoiceTotal = 0;
            Decimal subsTotal = 0;
            for(Invoice_Item__c invItem : invoiceItemMap.get(invoiceItem.Id)) {
                invoiceTotal += invItem.Amount__c != null ? invItem.Amount__c : 0;
            }
            for(Id subsIdItem : invoiceSubsMap.get(invoiceItem.Id)) {
                subsTotal += subsRecordMap.get(subsIdItem).Total_Amount_To_Bill__c != null ? subsRecordMap.get(subsIdItem).Total_Amount_To_Bill__c : 0;
            }
            if(invoiceTotal != subsTotal) {
                invoiceItem.Invoice_wrongly_generated__c = true;
                toUpdateList.add(invoiceItem);
            }
        }
        if(toUpdateList.size() > 0) {
            update toUpdateList;
        }
    }
}