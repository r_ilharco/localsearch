/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller of the ActivateSubscriptionsCmp component
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedby     	Luciano di Martino   <ldimartino@deloitte.it>
* 2020-10-06
* @systemLayer    	Invocation 
* Test Class : 		Test_ActivateProductionSubsCtrl
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class ActivateProductionSubsCtrl {
    
    @AuraEnabled
    public static Boolean hasUserGrant(){
        Boolean hasGrant = false;
        User currentUser = [SELECT Id, Profile.Name,
                            (SELECT PermissionSet.Name, PermissionSet.Label 
                             FROM PermissionSetAssignments 
                             WHERE PermissionSet.Name IN (:ConstantsUtil.PERMISSION_SET_ORDER_MANAGEMENT)) 
                            FROM User 
                            WHERE Id = :UserInfo.getUserId()];
        if(currentUser.PermissionSetAssignments.size() > 0 || ConstantsUtil.SYSADMINPROFILE_LABEL.equals(currentUser.Profile.Name)){
            hasGrant = true;
        }
        return hasGrant;
    }
    
    @AuraEnabled
    public static List<ActivateSubscriptionsWrapper> getSubscriptions(Id contractId){
        
        List<ActivateSubscriptionsWrapper> results = new List<ActivateSubscriptionsWrapper>();
        
        List<SBQQ__Subscription__c> subs = [SELECT 	Id
                                            		,Name
                                            		,SBQQ__ProductName__c
                                            		,SBQQ__SubscriptionStartDate__c
                                            		,SBQQ__Product__r.Product_Group__c
                                            		,RequiredBy__c
                                            		,Subsctiption_Status__c
                                            		,SBQQ__SubscriptionType__c
                                            		,Subscription_Term__c
                                            		,SBQQ__Product__r.ProductCode
                                            FROM 	SBQQ__Subscription__c
                                            WHERE 	SBQQ__Contract__c = :contractId
                                            AND 	(
                                                		Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION 
                                                 		OR 
                                                		Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE
                                               	 		OR 
                                                		Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_DRAFT
                                            		)
                                           ];
        
        List<SBQQ__Subscription__c> masterSubs = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> masterActiveSubs = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> masterSubsToShow = new List<SBQQ__Subscription__c>();
        List<SBQQ__Subscription__c> masterSubsMyWebFree = new List<SBQQ__Subscription__c>();
        
        for(SBQQ__Subscription__c sub : subs){
            if(String.isBlank(sub.RequiredBy__c)){
                
                Boolean isMyWebsite = ConstantsUtil.MYWEBSITE_PROD_GROUP.equalsIgnoreCase(sub.SBQQ__Product__r.Product_Group__c);
                Boolean isActive = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE.equalsIgnoreCase(sub.Subsctiption_Status__c);
                Boolean isFreemium = ConstantsUtil.MYWEBFREE.equalsIgnoreCase(sub.SBQQ__Product__r.ProductCode);
                
                masterSubs.add(sub);
                if(isActive) masterActiveSubs.add(sub);
                if(isMyWebsite && !isFreemium) masterSubsToShow.add(sub);
                if(isFreemium) masterSubsMyWebFree.add(sub);
            }
        }
        
        Integer daysToAdd = 365;
        
        ActivateSubscriptionsWrapper result;
        
        if(masterSubsToShow.size() == 0 && masterSubsMyWebFree.size() > 0){
            result = new ActivateSubscriptionsWrapper();
            result.errorMessage = Label.ActivateSubscriptionsCmp_MyWebFree;
            results.add(result);
        }
        else if(masterSubs.size() == masterActiveSubs.size()){
            result = new ActivateSubscriptionsWrapper();
            result.errorMessage = Label.ActivateSubscriptionsCmp_SubActive;
            results.add(result);
        }
        else if(masterSubsToShow.size() == 0){
            result = new ActivateSubscriptionsWrapper();
            result.errorMessage = Label.ActivateSubscriptionsCmp_MyWeb;
            results.add(result);
        }
        else if(masterSubsToShow.size() > 0){
            
            for(SBQQ__Subscription__c s : masterSubsToShow){
                result = new ActivateSubscriptionsWrapper();
                result.subId = s.Id;
                result.productName = s.SBQQ__ProductName__c;
                result.subStatus = s.Subsctiption_Status__c;
                result.oldStartDate = s.SBQQ__SubscriptionStartDate__c;
                result.minDate = Date.today();
                if(s.Subscription_Term__c == '12') daysToAdd = 363;
                result.maxDate = s.SBQQ__SubscriptionStartDate__c.addDays(daysToAdd);
                results.add(result);
            }
        }
        return results;
    }
    
    @AuraEnabled
    public static ActivateSubscriptionsWrapper updateSubscriptions(String jsonSubs, Id contractId){
        
        List<ActivateSubscriptionsWrapper> newSubs = (List<ActivateSubscriptionsWrapper>) JSON.deserialize(jsonSubs, List<ActivateSubscriptionsWrapper>.class) ;
        List<SBQQ__Subscription__c> newSubscriptions = new List<SBQQ__Subscription__c>();
        
        SBQQ__Subscription__c tempSub;
        
        for(ActivateSubscriptionsWrapper wrapSub : newSubs){
            tempSub = new SBQQ__Subscription__c(Id = wrapSub.subId, SBQQ__SubscriptionStartDate__c = wrapSub.oldStartDate);
            newSubscriptions.add(tempSub);
        }
        
        Map<Id, SBQQ__Subscription__c> newSubsMap = new Map<Id,SBQQ__Subscription__c>(newSubscriptions);
        
        Map<Id, SBQQ__Subscription__c> oldSubsMap = new Map<Id,SBQQ__Subscription__c>([SELECT 	Id
                                                                                       			,SBQQ__SubscriptionStartDate__c
                                                                                       			,Subscription_Term__c
                                                                                       FROM 	SBQQ__Subscription__c 
                                                                                       WHERE 	SBQQ__Contract__c = :contractId
                                                                                      ]);
        
        List<SBQQ__Subscription__c> subsToUpdate = new List<SBQQ__Subscription__c>();
        
        Set<Id> subIds = new Set<Id>();
        ActivateSubscriptionsWrapper result = new ActivateSubscriptionsWrapper();
        Integer daysToAdd = 365;
        
        try{
            Date subEndDate;
            
            for(SBQQ__Subscription__c sub : newSubsMap.values()){
                if(oldSubsMap.get(sub.Id).Subscription_Term__c == '12') daysToAdd = 363;
                
                if(sub.SBQQ__SubscriptionStartDate__c > oldSubsMap.get(sub.Id).SBQQ__SubscriptionStartDate__c.addDays(daysToAdd)){
                    result.errorMessage = Label.ActivateSubscriptionsCmp_Dates;
                    result.isStatusOk = false;
                    return result;
                }
                subIds.add(sub.Id);
            }
            
			List<SBQQ__Subscription__c> subsToActivate = [SELECT 	Id
                                                          			,Subsctiption_Status__c
                                                          			,RequiredBy__c
                                                          			,SBQQ__SubscriptionType__c
                                                          			,Subscription_Term__c
                                                          			,SBQQ__Product__r.ProductCode 
                                                          FROM 		SBQQ__Subscription__c 
                                                          WHERE 	SBQQ__Contract__c = :contractId 
                                                          AND 		Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION 
                                                          AND 		(
                                                              			RequiredBy__c IN :subIds 
                                                              			OR 
                                                              			Id IN :subIds
                                                          			)
                                                         ];
            
            for(SBQQ__Subscription__c currentSub : subsToActivate){
                if(ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION.equalsIgnoreCase(currentSub.Subsctiption_Status__c)){
                    
                    currentSub.Activation_Confirmed__c = true;
                    
                    if(String.isBlank(currentSub.RequiredBy__c)) currentSub.SBQQ__SubscriptionStartDate__c = newSubsMap.get(currentSub.Id).SBQQ__SubscriptionStartDate__c;
                    else currentSub.SBQQ__SubscriptionStartDate__c = newSubsMap.get(currentSub.RequiredBy__c).SBQQ__SubscriptionStartDate__c;
                    
                    if(currentSub.SBQQ__SubscriptionStartDate__c == Date.today()) currentSub.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
                    
                    if(String.isNotEmpty(currentSub.Subscription_Term__c)){
                        currentSub.End_Date__c = currentSub.SBQQ__SubscriptionStartDate__c.addMonths(Integer.valueOf(currentSub.Subscription_Term__c))-1;
                        currentSub.Next_Renewal_Date__c = currentSub.SBQQ__SubscriptionStartDate__c.addMonths(Integer.valueOf(currentSub.Subscription_Term__c));
                        if(!ConstantsUtil.PRODUCT2_SUBSCRIPTIONTYPE_EVERGREEN.equalsIgnoreCase(currentSub.SBQQ__SubscriptionType__c)){
                            currentSub.SBQQ__SubscriptionEndDate__c = currentSub.SBQQ__SubscriptionStartDate__c.addMonths(Integer.valueOf(currentSub.Subscription_Term__c))-1;
                            subEndDate = currentSub.SBQQ__SubscriptionEndDate__c;
                        }
                    }
                    subsToUpdate.add(currentSub);
                }
            }
            if(!subsToUpdate.isEmpty()) update subsToUpdate;
            
            Boolean updateContract = true;
            List<SBQQ__Subscription__c> subsToCheck = [SELECT 	Id
                                                       			,Subsctiption_Status__c
                                                       			,SBQQ__SubscriptionStartDate__c
                                                       			,SBQQ__Contract__r.Status
                                                       			,SBQQ__Contract__r.Account.Active_Contracts__c
                                                       			,SBQQ__Contract__r.StartDate
                                                       			,SBQQ__Contract__r.SBQQ__Evergreen__c
                                                       			,SBQQ__Contract__r.EndDate
                                                       FROM 	SBQQ__Subscription__c
                                                       WHERE 	SBQQ__Contract__c = :contractId];
            
            if(!subsToCheck.isEmpty()){
                Date subStartDate;
                Date contractStartDate;
                for(SBQQ__Subscription__c currentSub : subsToCheck){
                    
                    if(subStartDate == NULL) subStartDate = currentSub.SBQQ__SubscriptionStartDate__c;
                    else if(currentSub.SBQQ__SubscriptionStartDate__c < subStartDate) subStartDate = currentSub.SBQQ__SubscriptionStartDate__c;
                    contractStartDate = currentSub.SBQQ__Contract__r.StartDate;
                    
                    if(!ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE.equalsIgnoreCase(currentSub.Subsctiption_Status__c)){
                        updateContract = false;
                        break;
                    }
                }
                if(updateContract){
                    subsToCheck[0].SBQQ__Contract__r.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
                    if(contractStartDate != NULL) subsToCheck[0].SBQQ__Contract__r.StartDate = contractStartDate;
                    if(!subsToCheck[0].SBQQ__Contract__r.SBQQ__Evergreen__c && subEndDate != null)  subsToCheck[0].SBQQ__Contract__r.EndDate = subEndDate;
                    update subsToCheck[0].SBQQ__Contract__r;
                }
                else if(subStartDate != NULL && contractStartDate != NULL && subStartDate != contractStartDate){
                    subsToCheck[0].SBQQ__Contract__r.StartDate = subStartDate;
                    update subsToCheck[0].SBQQ__Contract__r;
                }
            }
            result.isStatusOk = true;
        }
        catch(Exception ex){
            System.debug('Exception thrown in ActivateProductionSubsCtrl.updateSubscriptions: ' + ex.getMessage() + ex.getLineNumber());
            result.errorMessage = ex.getMessage() + ex.getLineNumber();
            result.isStatusOk = false;
        }
        return result;
    }
    
    public class ActivateSubscriptionsWrapper{
        @AuraEnabled
        public String errorMessage;
        @AuraEnabled
        public String subId;
        @AuraEnabled
        public String productName;
        @AuraEnabled
        public String subStatus;
        @AuraEnabled
        public Boolean isStatusOk;
        @AuraEnabled
        public Date oldStartDate;
        @AuraEnabled
        public Date minDate;
        @AuraEnabled
        public Date maxDate;
        
        public ActivateSubscriptionsWrapper(){
            errorMessage = '';
            productName = '';
            subStatus = '';
            subId = '';
            isStatusOk = false;
            oldStartDate = Date.today();
            minDate = Date.today();
            maxDate = Date.today();
        }
        public ActivateSubscriptionsWrapper(ActivateSubscriptionsWrapper wrap){
            errorMessage = wrap.errorMessage;
            productName = wrap.productName;
            subStatus = '';
            subId = '';
            isStatusOk = false;
            oldStartDate = Date.today();
            minDate = Date.today();
            maxDate = Date.today();
        }
    }
        
}