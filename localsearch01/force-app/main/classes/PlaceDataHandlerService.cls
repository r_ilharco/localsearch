//Changelog: 05-20-2019 - US_19 -
//gcasola: Added MainCity, List<CategoryLocation> Categories, List<CategoryLocation> Location to syncPlaceData method.
//gcasola: SF2-351 Category Location Refactor 09-09-2019
//
/* testClass: Test_PlaceDataHandlerService
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-1566 PlaceDataHandler Service Class SF Errors
* @modifiedby     Mara Mazzarella <mamazzarella@deloitte.it>
* 2020-07-08
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        SPIII-4638 - Removed logic that insert the Category/Location relations when a
*                 place switches to PRIVATE type. When a place switches to private it is deleted,
*                 for this reason the insert of related Cat/Loc failed.
* @modifiedby     Vincenzo Laudato <vlaudato@deloitte.it>
* 2021-02-05
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@RestResource(urlMapping='/places/*')

global with sharing class PlaceDataHandlerService{

    @HttpGet
    global static void getPlaces() {
        RestResponse res = RestContext.response;
        String placeId = RestContext.request.params.get('placeId');
        try {
            List<Place__c> places = new List<Place__c>();
            places = [SELECT Id, Name FROM Place__c WHERE Id = :placeId];
            res.responseBody = Blob.valueOf(JSON.serialize(places));
            res.statusCode = 200;
        }
        catch (Exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
        }
    }

    @HttpPost
    global static void syncPlaceData(
        String City, 
        String Company,
        String Country,
        String Email,
        String FirstName,
        String LastName,
        String PlaceID,
        String MobilePhone,
        String Phone,
        String PostalCode,
        String Street,
        String Website,
        String Title,
        List<String> PlaceIdAliases,
        CategoryLocation MainCity,
        List<CategoryLocation> Categories,
        List<CategoryLocation> Locations,
        String Status,
        String SearchName,
        String Type,
        String GoldenRecordId,
        String CustomerNumber,
        String LeadId,   
        String SubSource,
        DateTime ModifiedDate,
        PoBoxDetails PostOfficeBox
    ){
        
        RestResponse res = RestContext.response;
        
        Place__c thePlace = new Place__c();
        Boolean noOp = false;
        Map<Integer, Category_Location__c> catLocMap = new Map<Integer, Category_Location__c>();
        Integer mainCityHashCode = null;

        if(MainCity != NULL && !MainCity.isEmpty()) mainCityHashCode = (ConstantsUtil.LOCATION_RT_DEVELOPERNAME + MainCity.Name.It.trim().toLowerCase() + MainCity.Name.En.trim().toLowerCase() + MainCity.Name.De.trim().toLowerCase() + MainCity.Name.Fr.trim().toLowerCase()).hashCode();
        
        if(String.isNotBlank(PlaceID)){            
            List<Place__c> p = [SELECT Id, Type__c, Place_Timestamp__c, Place_Type__c FROM Place__c WHERE PlaceID__c=:PlaceID LIMIT 1];
            if(!p.isEmpty()){
                thePlace.Id = p[0].Id;
                thePlace.Type__c = p[0].Type__c;
                thePlace.Place_Type__c = p[0].Place_Type__c;
                thePlace.Place_Timestamp__c = p[0].Place_Timestamp__c;
            }
            else if(Status == ConstantsUtil.PLACE_STATUS_DISABLED || type == ConstantsUtil.PLACE_TYPE_PRIVATE) noOp = true;
        }
        
        System.debug ('Timestamp from Place Record ' + thePlace.Place_Timestamp__c);
        System.debug ('Timestamp recevied from TIBCO' + ModifiedDate);
                
        if(String.isNotBlank(CustomerNumber) && thePlace.Id == NULL){
            List<Account> account = [SELECT Id, Customer_Number__c, GoldenRecordID__c FROM Account WHERE Customer_Number__c =:CustomerNumber LIMIT 1];
            if(!account.isEmpty()){
                thePlace.Account__c = account[0].Id;
                thePlace.CustomerNumber__c = CustomerNumber;
            }
        }
        else if(String.isNotBlank(LeadId)){
            Lead l = [SELECT Id, GoldenRecordID__c FROM Lead WHERE Id =:LeadId LIMIT 1];
            if(l!= NULL) thePlace.Lead__c = l.Id;
        }
        
        if(thePlace.Place_Timestamp__c < ModifiedDate|| thePlace.Place_Timestamp__c == null ) {
            System.debug('Place found and Place_Timestamp__c > ModifiedDate');
            catLocMap = convertCatLocMap(MainCity, Locations, Categories);
            
            String aliases = '';
            try{
                for(String a: PlaceIdAliases) aliases = (aliases=='' ? a : aliases+', '+a);
            }
            catch(Exception ex){
                System.debug('Exception thrown: ' + ex.getMessage());
            }

            thePlace.Type__c = Type;
            thePlace.Place_Type__c = Type;                          
            thePlace.City__c = City;
            thePlace.Country__c = Country;
			
            if(ConstantsUtil.PLACE_UNKNOWN.equalsIgnoreCase(Company)) thePlace.Company__c = null;
            else if(Company?.length() > 254) thePlace.Company__c = Company?.substring(0,254);
            else if(String.isNotBlank(Company)) thePlace.Company__c = Company;
			
            if(ConstantsUtil.PLACE_UNKNOWN.equalsIgnoreCase(FirstName)) thePlace.FirstName__c = null;
            else if (FirstName?.length() > 254) thePlace.FirstName__c = FirstName?.substring(0,254);
            else if(String.isNotBlank(FirstName)) thePlace.FirstName__c = FirstName;
            
            if(ConstantsUtil.PLACE_UNKNOWN.equalsIgnoreCase(LastName)) thePlace.LastName__c = null;
            else if (LastName?.length() > 254) thePlace.LastName__c = LastName?.substring(0,254);
            else if(String.isNotBlank(LastName)) thePlace.LastName__c = LastName;

            if(Website?.length() > 254) thePlace.Website__c = Website?.substring(0, 254);
            else if(String.isNotBlank(Website)) thePlace.Website__c = Website;
            
            thePlace.PlaceID__c = PlaceID;
            thePlace.PostalCode__c = PostalCode;
            thePlace.Street__c = Street;
            thePlace.Title__c = Title;
            thePlace.Status__c = Status;
            thePlace.Email__c = Email;
            thePlace.MobilePhone__c = MobilePhone;
            thePlace.Phone__c = Phone;
            thePlace.PlaceIdAliases__c = aliases;
            thePlace.SubSource__c = SubSource;
            thePlace.Place_Timestamp__c = ModifiedDate;
            thePlace.PO_Box__c = PostOfficeBox.PoNumber;
            thePlace.PO_Box_City__c = PostOfficeBox.City;
            thePlace.PO_Box_Zip_Postal_Code__c = PostOfficeBox.ZipCode;
            thePlace.DataSource__c = 'Place-DB';

            try {
                if(!noOp){
                    Boolean retryUpsert = false;
                    Database.UpsertResult resultsPlace = Database.upsert(thePlace, false);
                    if(!resultsPlace.isSuccess()){
                        for(Database.Error err : resultsPlace.getErrors()) {
                            if(!err.getFields().isEmpty() && err.getFields().contains('Email__c')){
                                thePlace.Email__c = '';
                                retryUpsert = true;
                            }
                        }
                    }
                    if(retryUpsert) upsert thePlace;
                    res.responseBody = Blob.valueOf(JSON.serialize(thePlace));
                }
                else{
                    String mex = ConstantsUtil.PLACE_DISABLED_OR_PRIVATE;
                    res.responseBody = Blob.valueOf(JSON.serialize(mex));
                    res.statusCode = 200;
                    return;
                }

                if(!ConstantsUtil.PLACE_TYPE_PRIVATE.equalsIgnoreCase(type)){

                    List<Category_Location__c> catLocAlreadyOnDB = SEL_CategoryAndLocation.getCategoriesAndLocationsByhashCodes(catLocMap.keyset());
                    List<Category_Location__c> catLocToINS = new List<Category_Location__c>();
                    Set<Integer> hashCodesAlreadyOnDB = new Set<Integer>();
                    Set<Integer> hashCodesINS = new Set<Integer>(); 
                    
                    if(!catLocAlreadyOnDB.isEmpty()){
                        for(Category_Location__c cl : catLocAlreadyOnDB) hashCodesAlreadyOnDB.add(Integer.valueOf(cl.clHashCode__c));
                    }
                    
                    for(Integer clHashCode : catLocMap.keySet()){
                        if(!hashCodesAlreadyOnDB.contains(clHashCode)){
                            hashCodesINS.add(clHashCode);
                            catLocToINS.add(catLocMap.get(clHashCode));
                        }
                    }
                    
                    if(!catLocToINS?.isEmpty()) insert catLocToINS;
                    
                    List<Category_Location__c> clPlaceRelations = SEL_CategoryAndLocation.getCategoriesAndLocationsByhashCodes(thePlace.Id, catLocMap.keySet());
                    List<Category_Location__c> relationsToINS = new List<Category_Location__c>();
                    
                    Set<Integer> refIdInRelations = new Set<Integer>();
                    Set<Integer> refIdNOTInRelations = new Set<Integer>(); 
                    
                    if(!clPlaceRelations.isEmpty()){
                        for(Category_Location__c clPR: clPlaceRelations){
                            refIdInRelations.add(Integer.valueOf(clPR.Category_Location__r.clHashCode__c));
                        }
                    }
                    
                    for(Integer clHashCode : catLocMap.keySet()){
                        if(!refIdInRelations.contains(clHashCode)) refIdNOTInRelations.add(clHashCode);
                    }
                    
                    List<Category_Location__c> catLocToAddInRelation = SEL_CategoryAndLocation.getCategoriesAndLocationsByhashCodes(refIdNOTInRelations);
                    
                    if(!catLocToAddInRelation.isEmpty()){
                        for(Category_Location__c cl : catLocToAddInRelation){
                            Category_Location__c newRel = new Category_Location__c();
                            newRel.Place__c = thePlace.Id;
                            newRel.Category_Location__c = cl.Id;
                            newRel.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.RELATION_RT_DEVELOPERNAME).getRecordTypeId();
                            relationsToINS.add(newRel);
                        }
                    }
                    if(!relationsToINS?.isEmpty()) insert relationsToINS;
                    
                    
                    if(mainCityHashCode != null){
                        List<Category_Location__c> mdl_rel = SEL_CategoryAndLocation.getCategoriesAndLocationsByhashCodes(thePlace.Id, new Set<Integer>{mainCityHashCode});
                        if(!mdl_rel.isEmpty()){
                            mdl_rel.get(0).mainLocation__c = true;
                            update mdl_rel[0];
                        }
                    }
                    
                    res.responseBody = Blob.valueOf(JSON.serialize(thePlace));
                    res.statusCode = 200;
                    LogUtility.savePlacePostRequest(RestContext.request,RestContext.response,thePlace,JSON.serialize(thePlace),null);
                }
            }
            catch(Exception e) {
                System.debug(e);
                res.responseBody = Blob.valueOf(e.getMessage());
                res.statusCode = 500;               
                if(!ConstantsUtil.PLACE_TYPE_PRIVATE.equalsIgnoreCase(type)) LogUtility.savePlacePostRequest(RestContext.request,RestContext.response,thePlace,null,e.getMessage());
            }
                
        }
    } 

    public static Map<Integer, Category_Location__c> convertCatLocMap(CategoryLocation mdc, List<CategoryLocation> locations, List<CategoryLocation> categories)
    {
        Map<Integer, Category_Location__c> result = new Map<Integer, Category_Location__c>();
        Category_Location__c el = new Category_Location__c();
        
        if(mdc != null)
        {
            if(!mdc.isEmpty() && !mdc?.Name?.isEmpty())
            {
                el.refId__c = mdc.id;
                el.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId();
                el.Location_it__c = mdc.Name.It;
                el.Location_fr__c = mdc.Name.Fr;
                el.Location_en__c = mdc.Name.En;
                el.Location_de__c = mdc.Name.De;
                el.clHashCode__c = (ConstantsUtil.LOCATION_RT_DEVELOPERNAME + el.Location_it__c.trim().toLowerCase() + el.Location_en__c.trim().toLowerCase() + el.Location_de__c.trim().toLowerCase() + el.Location_fr__c.trim().toLowerCase()).hashCode();
            }
        }
        
        if(el.clHashCode__c != null) result.put(Integer.valueOf(el.clHashCode__c), el);
        
        if(locations != null && !locations.isEmpty())
        {
            for(CategoryLocation loc: locations)
            {
                if(!loc.isEmpty() && !loc?.Name?.isEmpty())
                {
                    el = new Category_Location__c();
                    
                    el.refId__c = loc.id;
                    el.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.LOCATION_RT_DEVELOPERNAME).getRecordTypeId();
                    el.Location_it__c = loc.Name.It;
                    el.Location_fr__c = loc.Name.Fr;
                    el.Location_en__c = loc.Name.En;
                    el.Location_de__c = loc.Name.De;
                    el.clHashCode__c = (ConstantsUtil.LOCATION_RT_DEVELOPERNAME + el.Location_it__c.trim().toLowerCase() + el.Location_en__c.trim().toLowerCase() + el.Location_de__c.trim().toLowerCase() + el.Location_fr__c.trim().toLowerCase()).hashCode();
                    
                    result.put(Integer.valueOf(el.clHashCode__c), el);
                }
            }
        }

        if(categories != null && !categories.isEmpty())
        {
            for(CategoryLocation cat: categories)
            {
                if(!cat.isEmpty() && !cat?.Name?.isEmpty())
                {
                    el = new Category_Location__c();
        
                    el.refId__c = cat.id;
                    el.RecordTypeId = Schema.SObjectType.Category_Location__c.getRecordTypeInfosByDeveloperName().get(ConstantsUtil.CATEGORY_RT_DEVELOPERNAME).getRecordTypeId();
                    el.Category_it__c = cat.Name.It;
                    el.Category_fr__c = cat.Name.Fr;
                    el.Category_en__c = cat.Name.En;
                    el.Category_de__c = cat.Name.De;
                    el.clHashCode__c = (ConstantsUtil.CATEGORY_RT_DEVELOPERNAME + el.Category_it__c.trim().toLowerCase() + el.Category_en__c.trim().toLowerCase() + el.Category_de__c.trim().toLowerCase() + el.Category_fr__c.trim().toLowerCase()).hashCode();
        
                    result.put(Integer.valueOf(el.clHashCode__c), el);
                }
            }
        }

        return result;
    }

    global class CategoryLocation
    {
        public String Id;
        public Boolean Deprecated;
        public NameType Name;

        public CategoryLocation(String id, Boolean deprecated, NameType name)
        {
            this.Id = id;
            this.Deprecated = deprecated;
            this.Name = name;
        }
        
        Boolean isEmpty()
        {
            return (String.isEmpty(this.id) || name.isEmpty());
        }

        public Boolean equals(Object o)
        {
            if(this === o)
            {
                return true;
            }

            if(!(o instanceof CategoryLocation))
            {
                return false;
            }
            
            return (((CategoryLocation)this).Name == ((CategoryLocation)o).Name);
        }
        
        public Integer hashCode()
        {
            return this.Name.hashCode();
        }
    }

    global class NameType
    {
        public String It;
        public String Fr;
        public String De;
        public String En;

        public NameType(String it, String fr, String de, String en)
        {
            this.It = it;
            this.Fr = fr;
            this.De = de;
            this.En = en;
        }
        
        Boolean isEmpty()
        {
            return (String.isEmpty(this.it) || String.isEmpty(this.fr) || String.isEmpty(this.en) || String.isEmpty(this.de));
        }

        public Boolean equals(Object o)
        {
            if(this === o)
            {
                return true;
            }

            if(!(o instanceof NameType))
            {
                return false;
            }
            
            return (((NameType)this).De.equals(((NameType)o).De) && ((NameType)this).It.equals(((NameType)o).It) && ((NameType)this).Fr.equals(((NameType)o).Fr) && ((NameType)this).En.equals(((NameType)o).En));
        }
        
        public Integer hashCode()
        {
            return (this.It + this.En + this.De + this.Fr).hashCode();
        }
    }
    
    global class PoBoxDetails {
        string ZipCode;
        string PoNumber;
        string City;
        
        public PoBoxDetails (String zipCode, string ponumber, string city) {
            this.ZipCode = zipCode;
            this.PoNumber = ponumber;
            this.City = city;
        }
        
        Boolean isEmpty () {
            return (String.isEmpty(this.ZipCode) || String.isEmpty(this.PoNumber) || String.isEmpty(this.City));
        }
    }
}