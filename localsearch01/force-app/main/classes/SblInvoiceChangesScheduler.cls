global class SblInvoiceChangesScheduler implements Schedulable {
	// ***** ldimartino AS-75 START*****
    global void execute(SchedulableContext sc){
        Date today = Date.today();
        Datetime endDate = (DateTime) today.addDays(1);
        Datetime startDate = (DateTime) today.addDays(-1);
        System.enqueueJob(new SblInvoiceChangesJob(startDate, endDate));
    }
    // ***** ldimartino AS-75 END*****
}