public with sharing class UploadDocumentToWindream {
 	public static List<Log__c> integrationLogs = new List<Log__c>();
    public static String message = '';
    public static SBQQ__QuoteDocument__c selectedQuoteDocument = null;
    public static Contract selectedContract = null;       
    
    @invocableMethod
    public static void uploadDocumnetCalledByProcessBuilder(List<String> quoteDocumntRecordIds){     
        if(quoteDocumntRecordIds.size() > 0){
            callUploadDocument(quoteDocumntRecordIds[0]);
        }      
    }
    
    @future(callout=true)
    public static void callUploadDocument(Id quoteDocumntRecordId){
        UploadDocument(quoteDocumntRecordId);
    }
    
    @auraEnabled
    public static String UploadDocument(Id quoteDocumntRecordId) {
        
        QuoteDocInfo quoteDocInfo = getRequestObject(quoteDocumntRecordId);
        System.debug('returned doc info --> '+quoteDocInfo);
                   
        try{
            if(quoteDocInfo != null){
                return callWindream(quoteDocumntRecordId, quoteDocInfo, selectedContract, selectedQuoteDocument, integrationLogs);
            }
        }catch(Exception e){
            //ErrorHandler.logError(ConstantsUtil.WINDREAM_BP_NAME, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, e, ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT, e.getMessage(), null, quoteDocumntRecordId+'');
            LogUtility.saveWinDreamLog(null, null, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT, e, integrationLogs);
            return e.getMessage();
        }
        
        if(integrationLogs != NULL && integrationLogs.size() > 0)
            insert integrationLogs;
        
        return message;            
    }
    
    public static QuoteDocInfo getRequestObject(Id quoteDocumntRecordId){
        List<SBQQ__QuoteDocument__c> quoteDocumentList;
        List<Document> documentList;
        List<SBQQ__Quote__c> quoteList;
        List<Contract> contractList;        
        QuoteDocInfo quoteDocInfo = null;
        Contract selectedContract = null;
        message ='';
        Id versId = null;
        String docName = '';
        String contract_type = '';
        Blob file_content_in_base64 = null;
        String file_format = '';
        try{
            quoteDocumentList = [SELECT Id, Name,Url__c, SBQQ__SignatureStatus__c, SBQQ__Quote__c,WindreamUploadStatus__c, SBQQ__Quote__r.Name, SignatureDate__c, SBQQ__Quote__r.SBQQ__Account__r.Customer_Number__c
                                 FROM SBQQ__QuoteDocument__c WHERE Id =: quoteDocumntRecordId LIMIT 1];
            if(quoteDocumentList.size() > 0){
                    //Get ContentVersion base64 body
                List<ContentDocumentLink> contentDocId = [SELECT ContentDocumentId, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId =: quoteDocumentList[0].Id];
                if(contentDocId != NULL && contentDocId.size() > 0){
                    if(quoteDocumentList[0].SBQQ__SignatureStatus__c != ConstantsUtil.NAMIRIAL_DOCUMENT_SIGNATURESTATUS_SIGNED){
                        message = Label.Document_Not_Signed;
                        return null;
                    }
                    System.debug('Content Doc Id --> '+contentDocId);
                    List<ContentVersion> docVersion = new List<ContentVersion>();
                    versId = contentDocId[0].ContentDocument.LatestPublishedVersionId;
                    docVersion = [SELECT Id, VersionData, ContentSize  FROM ContentVersion WHERE Id =: versId];
                    System.debug('content version --> '+docVersion);
                    
                    selectedQuoteDocument = quoteDocumentList[0];
                    
                    docName = quoteDocumentList[0].Name + '.pdf';
                    String quoteId;
                    
                    System.debug('docVersion[0].ContentSize '+docVersion[0].ContentSize);
                    if(docVersion.size() > 0){
                        quoteId = quoteDocumentList[0].SBQQ__Quote__c;
                        //contractList = [SELECT Id, SBQQ__Quote__c, ContractNumber, CustomerSignedDate, Account.Customer_Number__c FROM Contract WHERE SBQQ__Quote__c =: quoteId LIMIT 1]; 
                        
                        //if(contractList.size() > 0){   
                        //selectedContract = contractList[0];
                        
                        contract_type = ConstantsUtil.SALESFORCE_TEXT;
                        file_format = ConstantsUtil.WINDREAM_FILE_FORMAT;
                        file_content_in_base64 = docVersion[0].VersionData;
                        // Date contract_conclusion_date = contractList[0].CustomerSignedDate;
                        Date contract_conclusion_date = Date.valueOf(quoteDocumentList[0].SignatureDate__c);
                        
                        Integer customer_number = -1;
                        Integer contract_number = -1;
                        contract_number = Integer.Valueof(quoteDocumentList[0].SBQQ__Quote__r.Name.split('-')[1]);
                        
                        if(quoteDocumentList[0].SBQQ__Quote__r.SBQQ__Account__r.Customer_Number__c	 != null){
                            customer_number = Integer.Valueof(quoteDocumentList[0].SBQQ__Quote__r.SBQQ__Account__r.Customer_Number__c);
                        }
                        quoteDocInfo = new QuoteDocInfo(customer_number, contract_number, contract_type, contract_conclusion_date, file_format, file_content_in_base64);
                        return quoteDocInfo;
                        //}
                                                    
                                                    /*}else{
                            message = ConstantsUtil.NO_RELATED_CONTRACT_FOUND;
                            return null;
                            }*/
                    }
                    else{
                        message = ConstantsUtil.NO_DOUMENT_FOUND;
                        return null;
                    }
                }
                else if(contentDocId.isEmpty()
                        && ConstantsUtil.WINDREAM_UPLOAD_SUCCESS_STATUS.equalsIgnoreCase(quoteDocumentList[0].WindreamUploadStatus__c) 
               			&& !String.isBlank(quoteDocumentList[0].Url__c)){
                    message = Label.Windream_Error_Document_already_uploaded;
                    return null;
                }
                else{
                    message = Label.Windream_No_Document_Found;
                    return null;
                }
            }
        }
        catch(Exception e){
            ErrorHandler.logError(ConstantsUtil.WINDREAM_BP_NAME, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, e, ErrorHandler.ErrorCode.E_DML_FAILED, e.getMessage(), null, quoteDocumntRecordId+'');
            message = e.getMessage();
            return null;
        }   
        return null;
    }
    
    public static String callWindream(Id quoteDocumntRecordId, QuoteDocInfo quoteDocInfo, Contract contractData, SBQQ__QuoteDocument__c quoteDoc, List<Log__c> integrationLogs){
        String result = '';
        
        //get json string
        string jsonBody = quoteDocInfo.getJson();
        
        if(!Test.isRunningTest()){
        	result = callHttpRequest(jsonBody, quoteDocumntRecordId, quoteDocInfo, contractData, quoteDoc, integrationLogs);
        }
        
        if(integrationLogs != NULL && integrationLogs.size() > 0)
        {
            List<Database.SaveResult> srs = Database.insert(integrationLogs);
            if(srs[0].isSuccess())
                integrationLogs.clear();
        }
        
        return result;
    }
    
    //@future(callout=true)
    public static String callHttpRequest(String body, Id quoteDocumntRecordId, QuoteDocInfo quoteDocInfo,  Contract contractData, SBQQ__QuoteDocument__c quoteDoc, List<Log__c> integrationLogs){
        String result = ''; 
        UtilityToken.masheryTokenInfo token = new UtilityToken.masheryTokenInfo();
        
   		List<Profile> p = [select Id from profile where name = :ConstantsUtil.SysAdmin limit 1];
		
        Mashery_Setting__c c2;
        
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);
        }
        else c2 = Mashery_Setting__c.getOrgDefaults();
        
        decimal cached_millisecs;
       	if(c2.LastModifiedDate !=null){
            cached_millisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
            system.debug('millisecs: ' +cached_millisecs);
       }
       Boolean IsNewToken=False;
       if(c2.Token__c == null || c2.Token__c == '' ||((cached_millisecs/1000).round()>=(Integer)c2.Expires__c)){
            IsNewToken=True;
            token = UtilityToken.refreshToken();
       } else{
           token.access_token= c2.Token__c;
           token.token_type= c2.Token_Type__c;
           token.expires_in= (Integer)c2.Expires__c;           
           system.debug('nonEmpty masheryTokenInfo>profileId'+c2);
		}
        String windreamMethod = CustomMetadataUtil.getMethodName(ConstantsUtil.SERVICE_WINDREAM_API_END_POINT);
		
        if(String.isNotBlank(windreamMethod)){
            HttpRequest request = new HttpRequest();
            request.setEndpoint(c2.Endpoint__c+windreamMethod);
            request.setMethod('POST');
            request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_TYPE, 'application/json');
            request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_CHARSET, 'UTF-8');
            request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_ACCEPT, 'application/json');
            request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_AUTHORIZATION, 'Bearer '+token.access_token);       
            request.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, quoteDocumntRecordId + '');
            request.setTimeout(120000);
            System.debug('body:  '+body);
            request.setBody(body);
                   
            try {
                Http http = new Http();
                System.debug('request to send -->'+request);
                HttpResponse response = http.send(request);
                result = response.getStatusCode() + '';
                
                if(response.getStatusCode() == Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS)){               
                    System.debug('response ok:'+response);
                    LogUtility.saveWinDreamLog(request, response, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, null, null, integrationLogs);                
                    String endpoint = c2.Endpoint__c;
                    
                    result = getUploadedDocUrl(quoteDocumntRecordId, endpoint+windreamMethod, token, quoteDocInfo, quoteDoc, integrationLogs);
                    
                } else{
                    LogUtility.saveWinDreamLog(request, response, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, null, null, integrationLogs);
                    result = response.toString();
                }
                
            } catch(exception e) {
                LogUtility.saveWinDreamLog(request, null, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_UPLOAD_DOC_CALL_LOG, ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT, e, integrationLogs);
                result = e.getMessage();
            }
            
            if(IsNewToken==True && !Test.isRunningTest()){
                c2.Token__c = token.access_token; 
                c2.Expires__c = token.expires_in; 
                upsert c2;
                database.upsert(c2, true);
            }
        } else {
            result = Label.WinDream_Not_Active;
        }
               
        return result;
    }
    
    public static String getUploadedDocUrl(Id quoteDocumntRecordId, String endPoint, UtilityToken.masheryTokenInfo token, QuoteDocInfo quoteDocInfo, SBQQ__QuoteDocument__c quoteDoc, List<Log__c> integrationLogs){
        String result = '';
        HttpRequest request = new HttpRequest();
        endPoint = endPoint + '?contract_number=' + quoteDocInfo.contract_number + '&contract_type=' + quoteDocInfo.contract_type;
        
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_TYPE, 'application/json');
        request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_CHARSET, 'UTF-8');
        request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_ACCEPT, 'application/json');
        request.setHeader(ConstantsUtil.WEB_SERVICE_HEADER_CONTENT_AUTHORIZATION, 'Bearer '+token.access_token);     
        request.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, quoteDocumntRecordId + '');
              
        try {
            Http http = new Http();
            System.debug('request to send windream --> '+request);
            HttpResponse response = http.send(request);
            result = response.getStatusCode() + '';
            
            if(response.getStatusCode() == Integer.Valueof(ConstantsUtil.STATUSCODE_SUCCESS)){
                System.debug('response_url ok:'+response); 
                               
                QuoteDocUrl qDocurl = (QuoteDocUrl) System.JSON.deserialize(response.getBody(), QuoteDocUrl.class);
                System.debug('response_url url:'+qDocurl.url);
                
                quoteDoc.URL__c = qDocurl.url;
                update quoteDoc;
                LogUtility.saveWinDreamLog(request, response, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_GET_LINK_CALL_LOG, null, null, integrationLogs);
                //IntegrationLog log = new IntegrationLog(ConstantsUtil.WINDREAM_GET_LINK_CALL_LOG, quoteDocumntRecordId+'', response.toString(), null);               
                //logList.add(log);
                //insertIntegrationLogList(logList);
                
                //System.debug('logList:'+logList); 
                
            } else{
                LogUtility.saveWinDreamLog(request, response, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_GET_LINK_CALL_LOG, null, null, integrationLogs);
            }
            
        } catch(exception e) {
            LogUtility.saveWinDreamLog(request, null, quoteDocumntRecordId, quoteDocumntRecordId, ConstantsUtil.WINDREAM_GET_LINK_CALL_LOG, ErrorHandler.ErrorCode.E_INTEGRATION_PEER_FAULT, e, integrationLogs);
        }
        return result;
	}
    public class QuoteDocInfo{
        public Integer customer_number {get; set;}
        public Integer contract_number {get; set;}
        public String contract_type {get; set;}
        public String contract_conclusion_date {get; set;}
        public String file_format {get; set;}
        public Blob file_content_in_base64 {get; set;}
        
        public QuoteDocInfo(Integer customer_number, Integer contract_number, String contract_type, Date contract_conclusion_date, String file_format, Blob file_content_in_base64){
            this.customer_number = customer_number;
            this.contract_number = contract_number;
            this.contract_type = contract_type;
            this.contract_conclusion_date = contract_conclusion_date + '';
            this.file_format = file_format;
            this.file_content_in_base64 = file_content_in_base64;
        }
        
        public String getJson(){
            try{
                return JSON.serialize(this, true);
            }
            catch(Exception e){
                message = Label.Document_Too_Large;
                return null;
            }
        }
        
        public String getPrettyPrint(){
             List<String> informations = new String[]{customer_number+'', contract_number+'', contract_type, contract_conclusion_date+'', file_format};
             return String.format(Label.Integration_Task_Windream_Send_Info, informations); 
        }
       	
    }
                             
    public class QuoteDocUrl {
        public String url;
    }                       

}