/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author      : Mara Mazzarella <mamazzarella@deloitte.it>
* Date	       : 2019-11-06
* Sprint      : Sprint 7
* Work item   : SF2-300, Sprint7, Order Management.
* Testclass   :
* Package     : 
* Description : 
* Changelog   :
*/

global without sharing class setFieldsOnSubscriptionForMigrated implements Database.Batchable<sObject>, Schedulable{
    
    global void execute(SchedulableContext sc) {
        setFieldsOnSubscriptionForMigrated setNextInvoiceDate = new setFieldsOnSubscriptionForMigrated(); 
        Database.executeBatch(setNextInvoiceDate, 500);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){  
        List<String> fieldNames = new List<String>(  Schema.getGlobalDescribe().get('SBQQ__Subscription__c').getDescribe().fields.getMap().keySet() );
		String query =' SELECT ' +String.join( fieldNames, ',' ) +
           ', SBQQ__OrderProduct__r.Order.ContractSAMBA_Key__c, SBQQ__OrderProduct__r.Order.SambaIsStartConfirmed__c FROM SBQQ__Subscription__c '+
           ' WHERE SBQQ__OrderProduct__r.Order.ContractSAMBA_Key__c != NULL AND SBQQ__OrderProduct__r.Order.SambaIsStartConfirmed__c = TRUE'+
           ' AND  Next_Invoice_Date__c = NULL and SBQQ__OrderProduct__c != NULL LIMIT 50000';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext info, List<SBQQ__Subscription__c> scope){
        System.debug('scope '+scope);
        try{
            setFieldsOnSubscription(scope);
            update scope;
        }
        catch(Exception e){
            ErrorHandler.log(System.LoggingLevel.ERROR, 'setFieldsOnSubscriptionForMigrated', 'setFieldsOnSubscriptionForMigrated.execute', e,ErrorHandler.ErrorCode.E_DML_FAILED, null, null, null, null, null, false);
        }
    }
    
    public static void setFieldsOnSubscription(List<SBQQ__Subscription__c> newItems){
        System.debug('setFieldsOnSubscription'+Userinfo.getName()+' '+UserInfo.getUserId());
        
        try{
            Map<Id,SBQQ__Subscription__c> orderItemToSub = new Map<Id,SBQQ__Subscription__c>();
            Map<Id,OrderItem> idToOrderItem = new Map<Id,OrderItem>();
            
            for(SBQQ__Subscription__c currentSub : newItems){
                system.debug('current sub order product --> '+currentSub.SBQQ__OrderProduct__c+' current sub --> '+currentSub);
            	orderItemToSub.put(currentSub.SBQQ__OrderProduct__c, currentSub);      
            }
            System.debug('orderItemToSub '+orderItemToSub);
            
            if(!orderItemToSub.isEmpty()){
                List<OrderItem> orderItems = SEL_OrderItem.getOrderItemsByIds(orderItemToSub.keySet());
                
                System.debug('orderItemToSub.keySet() '+orderItemToSub.keySet());
                System.debug('orderItems size'+orderItems.size());
                
                System.debug('order items --> '+orderItems);
                for(OrderItem currentOI : orderItems){
                    idToOrderItem.put(currentOI.Id, currentOI);
                }
                System.debug('id to order items --> '+idToOrderItem);
                System.debug(' orderItems '+ orderItems);
                Map<String, Integer> mapInvoiceCycle = new  Map<String, Integer> ();
                for(Invoice_Cycle__mdt invc : [SELECT MasterLabel, Number_of_months__c FROM Invoice_Cycle__mdt]){
                    mapInvoiceCycle.put(invc.MasterLabel, (Integer)invc.Number_of_months__c);
                }
				Date next_invoice_date;
                if(!idToOrderItem.isEmpty()){
                    for(Id currentKey : orderItemToSub.keySet()){
                        System.debug('current key order item '+idToOrderItem.get(currentKey));
                        orderItemToSub.get(currentKey).Place__c = idToOrderItem.get(currentKey).Place__c;
                        orderItemToSub.get(currentKey).Location__c = idToOrderItem.get(currentKey).Location__c;
                        orderItemToSub.get(currentKey).Category__c = idToOrderItem.get(currentKey).Category__c;
                        orderItemToSub.get(currentKey).Editorial_Content__c = idToOrderItem.get(currentKey).Editorial_Content__c;
                        orderItemToSub.get(currentKey).Campaign_Id__c = idToOrderItem.get(currentKey).Campaign_Id__c;
                        orderItemToSub.get(currentKey).Url__c = idToOrderItem.get(currentKey).Url__c;
                        orderItemToSub.get(currentKey).Package_Total__c = idToOrderItem.get(currentKey).Package_Total__c;
                        orderItemToSub.get(currentKey).Package_Total__c = idToOrderItem.get(currentKey).Package_Total__c;
                        orderItemToSub.get(currentKey).CategoryID__c = idToOrderItem.get(currentKey).CategoryID__c;
                        orderItemToSub.get(currentKey).LocationID__c = idToOrderItem.get(currentKey).LocationID__c;
                        orderItemToSub.get(currentKey).TLPaket_Region__c = idToOrderItem.get(currentKey).TLPaket_Region__c;
                        orderItemToSub.get(currentKey).Contract_Ref_Id__c = idToOrderItem.get(currentKey).Contract_Ref_Id__c;
                        orderItemToSub.get(currentKey).Activate_Later__c = idToOrderItem.get(currentKey).Activate_Later__c;
						orderItemToSub.get(currentKey).Manual_Activation__c = idToOrderItem.get(currentKey).Manual_Activation__c;
                        orderItemToSub.get(currentKey).Production__c = idToOrderItem.get(currentKey).Production__c; 
                        orderItemToSub.get(currentKey).Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE; 
                        if( orderItemToSub.get(currentKey).SBQQ__ListPrice__c <0){
                        	orderItemToSub.get(currentKey).SBQQ__ListPrice__c = idToOrderItem.get(currentKey).SBQQ__QuotedListPrice__c;
                        }
                        if(idToOrderItem.get(currentKey).Order.ContractSAMBA_Key__c != null
                           && idToOrderItem.get(currentKey).Order.SambaIsStartConfirmed__c == TRUE 
                           && idToOrderItem.get(currentKey).Order.SambaIsBilled__c == TRUE){
                               next_invoice_date = orderItemToSub.get(currentKey).SBQQ__SubscriptionStartDate__c;
                               while (next_invoice_date < date.today()){
                                     next_invoice_date = next_invoice_date.addMonths(mapInvoiceCycle.get(orderItemToSub.get(currentKey).SBQQ__BillingFrequency__c));
                               }
                            orderItemToSub.get(currentKey).Next_Invoice_Date__c = next_invoice_date;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            ErrorHandler.logInfo('SubscriptionTrigger', '', 'setPlaceOnSubscription '+e.getMessage());
        }
    }
    
    
    global void finish(Database.BatchableContext context) {
        
    }
    
}