/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 07-18-2019
 * Sprint      : Sprint 6
 * Work item   : SF2-241 - Product set-up - Multiple values management for Invoice cycle
 * Testclass   : Test_ConfigurationAttributeBuilder
 * Package     : 
 * Description : Script for the generation of Configuration Attributes
 * Changelog   : 
 */

public class ConfigurationAttributeBuilder {
    public static String nameAttribute = 'Invoice Cycle';
    public static Set<Id> productIds = new Set<Id>{'01t5E000003Cmo1QAC','01t5E000003Cmo2QAC','01t5E000003Cmo0QAC'};
    
	public static void start()
    {
        SBQQ__ConfigurationAttribute__c confAttrSetting = new SBQQ__ConfigurationAttribute__c();
        confAttrSetting.SBQQ__ApplyToProductOptions__c = true;
        confAttrSetting.SBQQ__TargetField__c = 'BillingFrequency__c';
        confAttrSetting.SBQQ__Required__c = true;
        confAttrSetting.SBQQ__Position__c = 'Top';
        confAttrSetting.SBQQ__DisplayOrder__c = 1;
        confAttrSetting.SBQQ__ColumnOrder__c = '1';
        confAttrSetting.RecordTypeId = Schema.SObjectType.SBQQ__ConfigurationAttribute__c.getRecordTypeInfosByName().get('Configuration Attribute').getRecordTypeId();
        createConfigurationAttributes(ConfigurationAttributeBuilder.nameAttribute, ConfigurationAttributeBuilder.productIds, confAttrSetting);
    }
	    
    public static List<SBQQ__ConfigurationAttribute__c> createConfigurationAttributes(String attributeName, Set<Id> productIds, SBQQ__ConfigurationAttribute__c confAttrSetting)
    {
        List<SBQQ__ConfigurationAttribute__c> configurationAttributes = new List<SBQQ__ConfigurationAttribute__c>();
        
        if(!String.isEmpty(attributeName) && !productIds.isEmpty() && confAttrSetting != NULL)
        {
            SBQQ__ConfigurationAttribute__c configurationAttribute;
            
            for(Id productId : productIds)
            {
                configurationAttribute = (SBQQ__ConfigurationAttribute__c) confAttrSetting.clone(false,true,false,false);
                configurationAttribute.Name = attributeName;
                configurationAttribute.SBQQ__Product__c = productId;
                configurationAttributes.add(configurationAttribute);
            }
        }
        
        insert configurationAttributes;
        
        return configurationAttributes;
    }
}