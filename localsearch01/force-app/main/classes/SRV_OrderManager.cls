/**
 * Author	   : Vincenzo Laudato <vlaudato@deloitte.it>
 * Date		   : 
 * Sprint      : 
 * Work item   : 
 * Testclass   : Test_OrderManager
 * Package     : 
 * Description :
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @modifiedby    Mara Mazzarella
 * 2020-11-09     SPIII-3794 Context products-Subscription & order relationship with de-allocation
 *				  in CASA - deleteAllocation when Termination order is fulfilled.
 * ─────────────────────────────────────────────────────────────────────────────────────────────────
 *  @changes
 * @modifiedby     Mara Mazzarella
 * 2020-12-29      SPIII-4515 -Fulfilled Orders - New service to republish the product 
				  config JSON (only for localBE and searchBE) - modified in case of "Activated" 
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing class SRV_OrderManager {
    
    public static Boolean orderIsNull(String idCall, ResponseInterface responseInfo, /*Log__c existingLog,*/RestRequest request){
        Boolean isInvalidCorrelationId = false;
        SBQQ__Quote__c quote = SEL_Quote.getQuoteByTrialCallId(idCall);
        String quoteId;
        if(quote != null){
            quoteId = quote.Id;
        }
        if(quoteId != null){
            activateQuoteTrial(responseInfo, quoteId);
        }
        else{
            isInvalidCorrelationId = true;
        }
        LogUtility.saveTrialActivationResponse(request,quoteId ,idCall,isInvalidCorrelationId, responseInfo);
        return isInvalidCorrelationId;
    }
    
    public static String evaluateAndExecuteAction(ResponseInterface responseInfo, Order order,RestRequest request){        
        String result = 'OK';
        
        String status = order.Status;
        String type = order.Custom_Type__c;
        
        Boolean isActivation =  (ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(type) && 
                                 !ConstantsUtil.ORDER_TYPE_AMENDMENT.equalsIgnoreCase(order.Type)/* &&
(ConstantsUtil.ORDER_STATUS_PUBLICATION.equalsIgnoreCase(status) || ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(status))*/);
        
        Boolean isCancellation =    (ConstantsUtil.ORDER_TYPE_CANCELLATION.equalsIgnoreCase(type) /*&&
(ConstantsUtil.ORDER_STATUS_PUBLICATION.equalsIgnoreCase(status) || ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(status))*/);
        
        Boolean isTermination =     (ConstantsUtil.ORDER_TYPE_TERMINATION.equalsIgnoreCase(type) /*&&
(ConstantsUtil.ORDER_STATUS_PUBLICATION.equalsIgnoreCase(status) || ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(status))*/);
        
        Boolean isAmendment =   (ConstantsUtil.ORDER_TYPE_ACTIVATION.equalsIgnoreCase(type) &&
                                 ConstantsUtil.ORDER_TYPE_AMENDMENT.equalsIgnoreCase(order.Type) /*&&
(ConstantsUtil.ORDER_STATUS_PUBLICATION.equalsIgnoreCase(status) || ConstantsUtil.ORDER_STATUS_REJECTED.equalsIgnoreCase(status))*/);
        if(status != ConstantsUtil.ORDER_STATUS_ACTIVATED){
            if(isActivation){
                result = generateContract(responseInfo, order);
            }
            else if(isCancellation){
                result = cancellationTerminationContract(responseInfo, order, ConstantsUtil.ACTION_TYPE_CANCELLATION); 
            }
            else if(isTermination){                    
                result = cancellationTerminationContract(responseInfo, order, ConstantsUtil.ACTION_TYPE_TERMINATION);
            }
            else if(isAmendment){
                result = amendContract(responseInfo, order);
            }
        }
        LogUtility.saveOrderResponse(request,order ,result, responseInfo);
        return result;
    }
    
    @TestVisible
    private static String generateContract(ResponseInterface responseInfo, Order order){
        
        String result = 'OK';
        Integer countOI = 0;
        Integer countOINewStatus = 0;
        
        Boolean updateOrder = false;
        Boolean toContract = false;
        
        Set<String> prodGroups = new Set<String>();
        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();
		
        Set<String> ids = new Set<String>(responseInfo.source_element_ids);
        String status = responseInfo.status;
        
        Savepoint sp;

        List<OrderItem> orderItems = SEL_OrderItem.getOrderItemByOrderId(order.Id);
        
        if(ConstantsUtil.CALL_STATUS_SUCCESS.equalsIgnoreCase(status)){
            
            for(OrderItem oi : orderItems){
                if(orderItems[0].Product2.Product_Group__c == null) prodGroups.add(orderItems[0].SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c);                            
                else prodGroups.add(orderItems[0].Product2.Product_Group__c);
            }
            
            if(!orderItems.isEmpty()){
                
                String currentProductGroup;
                
                if(orderItems[0].Product2.Product_Group__c == null) currentProductGroup = orderItems[0].SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c;                            
                else currentProductGroup = orderItems[0].Product2.Product_Group__c;
                
                String currentOrderStatus = order.Status;
                List<Order_Management__mdt> orderManagementMetadata = [SELECT  	Product_Group__c, Current_Status__c, Next_Status__c
                                                                       FROM    	Order_Management__mdt
                                                                       WHERE   	Product_Group__c = :currentProductGroup
                                                                       AND     	Current_Status__c = :currentOrderStatus
                                                                       AND		IsFeedback__c = TRUE
                                                                       AND		No_Manual_Step_Required__c = TRUE LIMIT 1];
                
                if(!orderManagementMetadata.isEmpty()){
                    
                    for(OrderItem currentOI : orderItems){
                        
                        if(ids.contains(currentOI.External_Id__c) ){
                            currentOI.SBQQ__Status__c = orderManagementMetadata[0].Next_Status__c;
                            orderItemsToUpdate.add(currentOI);
                        }
                        if(currentOI.SBQQ__Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED)){
                            countOI++;
                        }
                        if(currentOI.SBQQ__Status__c.equalsIgnoreCase(orderManagementMetadata[0].Next_Status__c)){
                            countOINewStatus++;
                        }
                    }
                    sp = Database.setSavepoint();
                    
                    Database.update(orderItemsToUpdate, true);
                    
                    if(orderItems.size() == countOI && orderManagementMetadata[0].Next_Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED)){
                        updateOrder = true;
                        order.SBQQ__Contracted__c = true;
                        order.Status = orderManagementMetadata[0].Next_Status__c;
                        toContract = true;
                    }
                    else if(orderItems.size() == countOINewStatus && !orderManagementMetadata[0].Next_Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED)){
                        updateOrder = true;
                        order.Status = orderManagementMetadata[0].Next_Status__c;
                    }
                }
            }
        }
        else{
            Integer countOIRejected = 0;
            List<OrderItem> orderItemsToReject = new List<OrderItem>();
            if(!ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(order.Status)){
                for(OrderItem currentOI : orderItems){
                    if(ids.contains(currentOI.External_Id__c)){
                        currentOI.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_REJECTED;
                        orderItemsToReject.add(currentOI);
                        countOIRejected++;
                    }
                    else if(currentOI.SBQQ__Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_REJECTED)) countOIRejected++;
                    if(!orderItemsToReject.isEmpty()){
                        if(countOIRejected == orderItems.size()){
                            updateOrder = true;
                            order.Status = ConstantsUtil.ORDER_STATUS_REJECTED;
                        }
                    }
                }
                update orderItemsToReject;
            }
        }        
        try{
            if(toContract){

                Boolean createOnboarding = false;
                UserOnBoardingRequest requestWrapper = new UserOnBoardingRequest();
                Map<String,String> languageMap = new Map<String,String>{'German' => 'de','French' => 'fr','Italian' => 'it','English' => 'en'};
                
                for(OrderItem oi : orderItems){

                    Boolean isMaster = String.isBlank(oi.SBQQ__RequiredBy__c);
				    Boolean isLBx = ConstantsUtil.PRODUCT2_PRODUCTGROUP_LBX.equalsIgnoreCase(oi.Product2.Product_Group__c);
                    Boolean isOnePresence = ConstantsUtil.ONEPRESENCE_PRODUCTGROUP.equalsIgnoreCase(oi.Product2.Product_Group__c);

					if(isMaster && (isLBx || isOnePresence)){
                        if(String.isNotBlank(oi.LBx_Contact__c)){
                            if(String.isNotBlank(oi.LBx_Contact__r.Email)){
                                OnboardingUser onboardingUser = new OnboardingUser();
                                onboardingUser.email = oi.LBx_Contact__r.Email;
                                onboardingUser.first_name = oi.LBx_Contact__r.FirstName;
                                onboardingUser.last_name = oi.LBx_Contact__r.LastName;
                                if(String.isNotBlank(oi.LBx_Contact__r.Language__c)) onboardingUser.language = languageMap.get(oi.LBx_Contact__r.Language__c);
                                if(String.isNotBlank(oi.Place__c)) onboardingUser.place_id = oi.Place__r.PlaceID__c;    
                                onboardingUser.claims = isLBx ? ConstantsUtil.ONBOARDINGUSER_CLAIMS_PLACE : ConstantsUtil.ONBOARDINGUSER_CLAIMS_CUSTOMER;
                                requestWrapper.users.add(onboardingUser);
                                createOnboarding = true;
                            }
                        }
					}
				}
                if(createOnboarding){
                    String jsonBody = JSON.serialize(requestWrapper);
                    String correlationId = getUniqueId(order.Id);
                    lbxUserOnBoardingCallout(jsonBody, correlationId, order.Id, order.Account.Customer_Number__c);
                }
            }
            
            if(updateOrder && toContract) System.enqueueJob(new QueueableContractsOrder(order));
            else if(updateOrder && !toContract) update order;
        }
        catch(Exception e){
            Database.rollback(sp);
            result = e.getMessage();
            Error_Log__c errorLog = LogUtility.generateErrorLog('SRV_OrderManager', 'generateContract', e, null, order.Id, JSON.serialize(responseInfo));
            insert errorLog;
        }
        
        if(ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(order.Status)){
            QuoteManager.convertTrial(order.SBQQ__Quote__c);
        }
        
        return result;
    }
    
    @future(callout=true)
    @TestVisible
    private static void lbxUserOnBoardingCallout(String jsonBody, String correlationId, String orderId,String customerNumber){
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        Mashery_Setting__c c2;
        if(!p.isEmpty()){
            c2 = Mashery_Setting__c.getInstance(p[0].Id);    
        } 
        else{
            c2 = Mashery_Setting__c.getOrgDefaults();
        } 
        Decimal cachedMillisecs;
        if(c2.LastModifiedDate !=null){
            cachedMillisecs = decimal.valueOf(datetime.now().getTime() - c2.LastModifiedDate.getTime());
        }
        TokenInfo tokenInfoObject = getValidToken(c2, cachedMillisecs);
        
        HttpRequest request;
        String methodName = CustomMetadataUtil.getMethodName(ConstantsUtil.ONBOARDING_USER_METHOD);
        request = new HttpRequest();
        request.setEndpoint(c2.Endpoint__c + methodName);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('charset', 'UTF-8');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer '+tokenInfoObject.token.access_token);
        request.setHeader('X-LS-Tracing-CorrelationId', correlationId);
        request.setHeader('X-LS-Tracing-BOIdValue', customerNumber);
        request.setBody(jsonBody);
        request.setTimeout(30000);
        
        Http http = new Http();
        
        try{
            if(!Test.isRunningTest()){
                HttpResponse response = http.send(request);
                LogUtility.saveOnBoardingUserCreationLog(c2.Endpoint__c + methodName,jsonBody,response.getBody(),response.getStatusCode(),correlationId,orderId,null);
            }
        } 
        catch(exception e) {
            LogUtility.saveOnBoardingUserCreationLog(c2.Endpoint__c + methodName,jsonBody,null,null,correlationId,orderId,e.getStackTraceString());
        } 
    }
    
    @TestVisible
    private static String cancellationTerminationContract(ResponseInterface responseInfo, Order order, String isCancellationOrTermination){

        String result = 'OK';
        String responseStatus = responseInfo.status;
        
        List<OrderItem> orderItemsToGetProdGroup = SEL_OrderItem.getOrderItemByOrderId(order.Id);

        if(!orderItemsToGetProdGroup.isEmpty()){
            
            Boolean updateOrder = false;
            Integer countOIRejected = 0;
            Integer countOINewStatus = 0;
            Integer countOIActivated = 0;
            Integer countSubscription = 0;
            Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem>();
            
            String currentProductGroup;
            if(orderItemsToGetProdGroup[0].Product2.Product_Group__c == null) currentProductGroup = orderItemsToGetProdGroup[0].SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c;                            
            else currentProductGroup = orderItemsToGetProdGroup[0].Product2.Product_Group__c;
            
            String currentOrderStatus = order.Status;
            Set<String> subsExtId = new Set<String>(responseInfo.source_element_ids);
			Set<Id> subscriptionsToTerminateIds = new Set<Id>();
            
            List<Order_Management__mdt> orderManagementMetadata = SEL_OrderManagementMdt.responseWithoutProductCode(currentProductGroup, currentOrderStatus);
            
            if(!orderManagementMetadata.isEmpty()){
                for(OrderItem oi : orderItemsToGetProdGroup){
                    List<Order_Management__mdt> orderItemManagementMetadata = SEL_OrderManagementMdt.responseWithoutProductCode(currentProductGroup, oi.SBQQ__Status__c);
                    if(!orderItemManagementMetadata?.isEmpty()){
                        if(subsExtId.contains(oi.SBQQ__Subscription__r.External_Id__c)){
                            if(ConstantsUtil.CALL_STATUS_SUCCESS.equalsIgnoreCase(responseStatus)){
                                oi.SBQQ__Status__c = orderItemManagementMetadata[0].Next_Status__c;
                                subscriptionsToTerminateIds.add(oi.SBQQ__Subscription__c);
                            }
                            else oi.SBQQ__Status__c = ConstantsUtil.ORDER_ITEM_STATUS_REJECTED;
                            orderItemsToUpdate.put(oi.Id, oi);
                        }
                    }
                }
                
                for(OrderItem oi : orderItemsToGetProdGroup){
                    if(oi.SBQQ__Status__c.equalsIgnoreCase(orderManagementMetadata[0].Next_Status__c)) countOINewStatus++;
                    else if(oi.SBQQ__Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_REJECTED)) countOIRejected++;
                    else if(oi.SBQQ__Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED)) countOIActivated++;
                }
                
                if(orderItemsToGetProdGroup.size() == countOINewStatus) updateOrder = true;
                else if(orderItemsToGetProdGroup.size() == countOIRejected){
                    order.Status = ConstantsUtil.ORDER_STATUS_REJECTED;
                    updateOrder = true;
                }
                if(orderItemsToGetProdGroup.size() == countOIActivated){
                    order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
                    updateOrder = true;
                }
                
                if(!orderItemsToUpdate.isEmpty()) update orderItemsToUpdate.values();
                
                if(ConstantsUtil.CALL_STATUS_SUCCESS.equalsIgnoreCase(responseStatus)){
                    Boolean isActivated = ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(orderManagementMetadata[0].Next_Status__c);
                    Boolean isActivatedWithCurrentFeedback =  ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(order.Status);
                    Boolean isAdvPrint = ConstantsUtil.PRODUCT2_PRODUCTGROUP_ADV_PRINT.equalsIgnoreCase(orderManagementMetadata[0].Product_Code__c);
                    
                    if(!isActivatedWithCurrentFeedback) order.Status = orderManagementMetadata[0].Next_Status__c;
                    String subscriptionStatus = ConstantsUtil.ACTION_TYPE_CANCELLATION.equals(isCancellationOrTermination) ? ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED : ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
                    
                    if(isActivated || isAdvPrint || isActivatedWithCurrentFeedback){
                        List<SBQQ__Subscription__c> subscriptions = SEL_Subscription.getSubscriptionsByContractId(order.Contract__c);
                        if(!subscriptions.isEmpty()){
                            Date terminationDate = getMaximumTerminationDate(subscriptions);
                            Boolean isUpgrade = ConstantsUtil.ACTION_TYPE_TERMINATION.equals(isCancellationOrTermination) && ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(order.Type);
                            Boolean isTermination = ConstantsUtil.ACTION_TYPE_TERMINATION.equals(isCancellationOrTermination);
                            
                            terminateSubscriptions(subscriptions, subscriptionsToTerminateIds, subscriptionStatus, isUpgrade);
                            update subscriptions;
                            
                            if(isTermination) countSubscription = SEL_Subscription.getSubsCountByContractIdAndNotStatus(order.Contract__c, ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED);
                            else countSubscription = SEL_Subscription.getSubsCountByContractIdAndNotStatus(order.Contract__c, ConstantsUtil.SUBSCRIPTION_STATUS_CANCELLED);
                            
                            if(countSubscription == 0){
                                List<Contract> contract = SEL_Contract.getContractById(order.Contract__c);
                                if(!contract.isEmpty()){
                                    if(ConstantsUtil.ACTION_TYPE_TERMINATION.equals(isCancellationOrTermination)){
                                        terminateContract(contract[0], terminationDate);   
                                    }
                                    else{
                                        cancelContract(contract[0], terminationDate);
                                    }
                                }
                            }
                            
                            if(updateOrder && ConstantsUtil.ORDER_TYPE_UPGRADE.equalsIgnoreCase(order.Type)) sendUpgradeActivationOrder(order.Parent_Order__c);
                            if(updateOrder && isAdvPrint){
                                if(orderManagementMetadata[0].Send_to_Backend__c) sendToBackend(order.Id);
                            }
                        }
                    }
                    else{
                        if(orderManagementMetadata[0].Send_to_Backend__c) sendToBackend(order.Id);
                    }
                }
                if(updateOrder) update order;
            }
        }
        return result;
    }
    
    public class QueueableContractsOrder implements Queueable {
        
        Order orderToUpdate;
        
        public QueueableContractsOrder(Order orderToUpdate) {
            this.orderToUpdate = orderToUpdate;
        }
        public void execute(QueueableContext context) {
            update orderToUpdate;
        }
    }
    
    @TestVisible
    private static String amendContract(ResponseInterface responseInfo, Order order){

        Boolean updateOrder = false;
        String result = 'OK';
        Savepoint sp;
        
        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();
        Set<String> prodGroups = new Set<String>();

        Integer countOI=0;
        Integer countOINewStatus=0;

        Set<String> ids = new Set<String>(responseInfo.source_element_ids);

        String status = responseInfo.status; 

        List<OrderItem> orderItems = SEL_OrderItem.getOrderItemByOrderId(order.Id);

        if(ConstantsUtil.CALL_STATUS_SUCCESS.equalsIgnoreCase(status)){
            for(OrderItem oi : orderItems){
                if(orderItems[0].Product2.Product_Group__c == null){
                    prodGroups.add(orderItems[0].SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c);                            
                }
                else{
                    prodGroups.add(orderItems[0].Product2.Product_Group__c);
                }
                
            }
            if(!orderItems.isEmpty()){

                String currentProductGroup;

                if(orderItems[0].Product2.Product_Group__c == null){
                    currentProductGroup = orderItems[0].SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c;                            
                }
                else{
                    currentProductGroup = orderItems[0].Product2.Product_Group__c;
                }

                String currentOrderStatus = order.Status;

                List<Order_Management__mdt> orderManagementMetadata = [ SELECT  Product_Group__c, Current_Status__c, Next_Status__c
                                                                        FROM    Order_Management__mdt
                                                                        WHERE   Product_Group__c = :currentProductGroup
                                                                        AND     Current_Status__c = :currentOrderStatus
                                                                        AND		IsFeedback__c = TRUE
                                                                        AND		No_Manual_Step_Required__c = TRUE
                                                                        LIMIT   1];
                if(!orderManagementMetadata.isEmpty()){
                    for(OrderItem currentOI : orderItems){
                        if(ids.contains(currentOI.External_Id__c)){
                            currentOI.SBQQ__Status__c = orderManagementMetadata[0].Next_Status__c;
                            orderItemsToUpdate.add(currentOI);
                        }
                        if(currentOI.SBQQ__Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_ITEM_STATUS_ACTIVATED)){
                            countOI++;
                        }
                        if(currentOI.SBQQ__Status__c.equalsIgnoreCase(orderManagementMetadata[0].Next_Status__c)){
                            countOINewStatus++;
                        }
                    }
                    
                    sp = Database.setSavepoint();
                    Database.update(orderItemsToUpdate, true);
                    if(orderItems.size() == countOI && orderManagementMetadata[0].Next_Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED)){
                        updateOrder = true;
                        order.SBQQ__Contracted__c = true;
                        order.Status = orderManagementMetadata[0].Next_Status__c;
                    }
                    else if(orderItems.size() == countOINewStatus && !orderManagementMetadata[0].Next_Status__c.equalsIgnoreCase(ConstantsUtil.ORDER_STATUS_ACTIVATED)){
                        updateOrder = true;
                        order.Status = orderManagementMetadata[0].Next_Status__c;
                    }
                }
            }
            else{
                result = 'KO : '+'No order items found for Order with Id: '+order.Id;
            }
        }
        else{
            if(!ConstantsUtil.ORDER_STATUS_ACTIVATED.equalsIgnoreCase(order.Status)){
                updateOrder = true;
                order.Status = ConstantsUtil.ORDER_STATUS_REJECTED;
            }
        }
        
        try{
            if(updateOrder) update order;
        }
        catch(Exception e){
            Database.rollback(sp);
            result = 'KO : Failed to update order - '+e.getMessage();
        }

        return result;
    }
	
    @TestVisible
    public static void terminateContract(Contract contract, Date terminationDate){
        contract.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;
        contract.TerminateDate__c  = terminationDate;
        contract.In_Termination__c = false;
        Database.SaveResult res = Database.update(contract);
        Billing.cancelcontract(contract.Id);
    }
    
    @TestVisible
    private static void cancelContract (Contract contract, Date cancelDate){
        
        contract.Status = ConstantsUtil.CONTRACT_STATUS_CANCELLED;
        contract.Cancel_Date__c = cancelDate;
        contract.In_Cancellation__c = false;
        //START SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
        Database.SaveResult res = Database.update(contract);
        
        if(res.isSuccess()){
            List<Contract_Setting__mdt> contractCancellation = [SELECT  InvoiceStatus__c 
                                                                FROM    Contract_Setting__mdt 
                                                                WHERE   MasterLabel = 'ContractCancellation_Process']; 
            
            List<String> invoiceStatus = contractCancellation[0].InvoiceStatus__c.split(';');
            
            cancelInvoices(contract.Id, invoiceStatus);
        }
        //END SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
    }
    
    @TestVisible
    private static void cancelInvoices(String contractId, List<String> invoiceStatus){
        //START SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
        Map<Id, Invoice__c> invoiceToCancel = SEL_Invoices.getInvoicesByContractIDAndInvoiceStatus(contractId, invoiceStatus);
        
        if(!invoiceToCancel.isEmpty()){
            List<AggregateResult> results = SEL_InvoiceOrders.countInvoices(invoiceToCancel.keySet());
            for(AggregateResult r : results){
                if(r.get('number') == 1){
                    SRV_Invoice.cancelInvoices(new Set<Id> (invoiceToCancel.keySet())); 
                } 
            }
        }    
        //END SF2-22 - IH: Contract cancellation - gcasola - 06-10-2019
    }
    
    @TestVisible
    private static void terminateSubscriptions(List<SBQQ__Subscription__c> subscriptions, Set<Id> subscriptionToTerminateIds, String subscriptionStatus, Boolean isUpgrade){
        for(SBQQ__Subscription__C currentSubscription : subscriptions){
            Boolean isToTerminate = subscriptionToTerminateIds.contains(currentSubscription.Id);
            if(isToTerminate && String.isNotBlank(currentSubscription.Ad_Context_Allocation__c)) 
                currentSubscription.Tech_Delete_Allocation__c = true;
            if(isToTerminate && isUpgrade){
                currentSubscription.Termination_Generate_Credit_Note__c = true;
            }
            if(isToTerminate && currentSubscription.Termination_Date__c < Date.today()){
                currentSubscription.Subsctiption_Status__c = subscriptionStatus;
                currentSubscription.In_Termination__c = false;
            }
        }
    }
    
    public static void sendUpgradeActivationOrder(Id parentOrderId){
        List<Order> upgradeDowngradeOrder = SEL_Order.getOrdersByIds(new List<Id>{parentOrderId});
        System.enqueueJob(new QueueableChangeOrderStatus(upgradeDowngradeOrder));
    }
    
    public static void sendToBackend(Id orderId){
        System.enqueueJob(new QueueableActivateContracts(new List<Id>{orderId}));
    }
    
    public class QueueableChangeOrderStatus implements Queueable {
        
        List<Order> upgradeDowngradeOrder;
        
        public QueueableChangeOrderStatus(List<Order> upgradeDowngradeOrder) {
            this.upgradeDowngradeOrder = upgradeDowngradeOrder;
        }
        public void execute(QueueableContext context) {
            OrderUtility.changeOrderStatus(upgradeDowngradeOrder, true);
        }
    }
    
    public class QueueableActivateContracts implements Queueable {
        
        List<Id> orderIds;
        
        public QueueableActivateContracts(List<Id> orderIds) {
            this.orderIds = orderIds;
        }
        public void execute(QueueableContext context) {
            OrderUtility.activateContracts(orderIds);
        }
    }
    
    @TestVisible
    private static Date getMaximumTerminationDate(List<SBQQ__Subscription__c> subscriptions){
        Date maxDate;
        List<Date> terminationDates = new List<Date>();
        for(SBQQ__Subscription__c currentSubscription : subscriptions){
            if(currentSubscription.Termination_Date__c != null){
                terminationDates.add(currentSubscription.Termination_Date__c);
            }
        }
        if(!terminationDates.isEmpty()){
            maxDate = terminationDates[0];
            for(Date currentDate : terminationDates){
                if(currentDate > maxDate){
                    maxDate = currentDate;
                }
            }
        }
        return maxDate;
    }
    
    /*public static Log__c insertLog(String idCall, String bodyMessage){
        Log__c log = new Log__c();
        log.Name = idCall + ' ' + ConstantsUtil.RESPONSE;
        log.Body_Message__c = bodyMessage;
        log.Communication_Ok__c = true;
        insert log;
        
        return log;
        }
        
        public static String getQuoteId(SBQQ__Quote__c quote, Log__c existingLog){
        
        String quoteId;
        
        if(quote != null){
        quoteId = quote.Id;
        }
        else if(existingLog != null){
        quoteId = existingLog.Quote__c;
        }
        
        return quoteId;
    }*/
    
    public static void activateQuoteTrial(ResponseInterface responseInfo, Id quoteId){ //, Log__c log ){
        //Extract the status
        String status = responseInfo.status;
        
        System.debug('status: ' + status );     
        
        String responseStatus = responseInfo.status;
        Set<String> quoteLineIds = new Set<String>(responseInfo.source_element_ids);

        List<SBQQ__QuoteLine__c> listQuoteLineToUpdate = new List<SBQQ__QuoteLine__c>();
        
        try{
            for(SBQQ__QuoteLine__c qline : [SELECT Id, Name, SBQQ__ProductName__c, Status__c, TrialStatus__c FROM SBQQ__QuoteLine__c 
                                            WHERE SBQQ__Quote__c = :quoteId AND TrialStatus__c!=:ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED]){
                                                
                                                qline.TrialStatus__c = status;
                                                /*A. L. Marotta 24-07-19 Start
* SF2-231 Wave 3, Sprint 6 - Checked flag Trial on Quote Line*/
                                                if(status==ConstantsUtil.QUOTELINE_STATUS_TRIAL_ACTIVATED){
                                                    qline.Trial__c = true; 
                                                }
                                                //A. L. Marotta 24-07-19 End
                                                listQuoteLineToUpdate.add(qline);
                                            }
            if(!listQuoteLineToUpdate.isEmpty()){
                update listQuoteLineToUpdate;
            }
        }
        catch (Exception ex) {}   
    }
    
    public static String getUniqueId(String orderId){
        String hashString = orderId + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        System.debug(EncodingUtil.convertToHex(hash));
        return EncodingUtil.convertToHex(hash);
    }
    
    public class CustomException extends Exception{}
    
    public class UserOnBoardingRequest{
        public List<OnboardingUser> users {get;set;}
        
        public UserOnBoardingRequest(){
            users = new List<OnboardingUser>();
        }
        
    }
    public class OnboardingUser{
        public String first_name {get;set;}
        public String last_name {get;set;}
        public String email {get;set;}
        public String language {get;set;}
        public String place_id {get;set;}
        public String claims {get;set;}
    }
    
    public class TokenInfo{
        public Boolean isNewToken {get;set;}
        public UtilityToken.masheryTokenInfo token {get;set;}
    }
    
    public static TokenInfo getValidToken(Mashery_Setting__c c2, Decimal cachedMillisecs){
        
        TokenInfo tokenInfoObject = new TokenInfo();
        tokenInfoObject.token = new UtilityToken.masheryTokenInfo();
        tokenInfoObject.isNewToken = false;
        
        if(c2.Token__c == null || c2.Token__c == '' || ((cachedMillisecs/1000).round() >= (Integer)c2.Expires__c)){
            tokenInfoObject.isNewToken = true;
            if(!Test.isRunningTest()){
                tokenInfoObject.token = UtilityToken.refreshToken();
            }
        } 
        else{
            tokenInfoObject.token.access_token= c2.Token__c;
            tokenInfoObject.token.token_type= c2.Token_Type__c;
            tokenInfoObject.token.expires_in= (Integer)c2.Expires__c;           
        }
        System.debug('Access Token: ' + tokenInfoObject.token.access_token); 
        System.debug('Is New Token: ' + tokenInfoObject.isNewToken);
        return tokenInfoObject;
    } 
}