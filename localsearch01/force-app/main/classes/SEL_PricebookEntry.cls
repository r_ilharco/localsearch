/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-14-2019
 * Sprint      : 3
 * Work item   : SF2-88 Contract Renewal - US_65 Contract automatically renewed at subscription end date (implicit)
 * Testclass   :
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class SEL_PricebookEntry {
    public static Map<Id, PricebookEntry> getActivePriceBookEntriesByPriceBookId(Set<Id> ids)
    {
        return new Map<Id, PricebookEntry>([
            SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, ProductCode 
            FROM PricebookEntry
            WHERE Pricebook2Id IN :ids
            AND IsActive = true
        ]
        );
    }
    
    public static Map<Id, PricebookEntry> getPriceBookEntriesByPriceBookId(Set<Id> ids)
    {
        return new Map<Id, PricebookEntry>([
            SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, ProductCode 
            FROM PricebookEntry
            WHERE Pricebook2Id IN :ids ]);
    }
    
     public static Map<Id, PricebookEntry> getPriceBookEntriesByProduct(List<Id> priceBookIds, Set<Id> productIds)
    {
        return new Map<Id, PricebookEntry>([
            SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, ProductCode 
            FROM PricebookEntry
            WHERE Pricebook2Id IN :priceBookIds
            AND Product2Id IN :productIds
            AND IsActive = true
        ]
        );
    }

}