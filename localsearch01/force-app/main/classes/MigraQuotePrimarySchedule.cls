global class MigraQuotePrimarySchedule implements Schedulable {

    public integer num;
    public string batch;
    
    
    global MigraQuotePrimarySchedule(string batchName, Integer num){
      this.batch = batchName;
      this.num = num;
    }

    global void execute(SchedulableContext SC) {
      database.executeBatch(new MigraQuotePrimary(batch,num),1);
    }
}