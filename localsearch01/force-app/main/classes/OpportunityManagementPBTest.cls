@isTest
public class OpportunityManagementPBTest {
    
    @isTest
    public static void testOpportunityManagement(){
        try{
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='dh'; 
            acc.BillingCity = 'test city';
            acc.BillingCountry = 'test country';
            acc.BillingPostalCode = '123';
            acc.BillingState = 'test state';
            acc.PreferredLanguage__c = 'German';       
            insert acc;
            System.Debug('Account: '+ acc);   
            
            Pricebook2 prBook = new Pricebook2();
            prBook.Name = 'test price book';
            insert prBook;
            System.Debug('Pr.Book '+ prBook); 
            
            Opportunity opp = new Opportunity();
            opp.Account = acc;
            opp.AccountId = acc.Id;
            opp.Pricebook2Id = prBook.Id;
            opp.StageName = 'Qualification';
            opp.CloseDate = Date.today().AddDays(89);
            opp.Name = 'Test Opportunity';        
            insert opp;
            System.Debug('Opportunity '+ opp); 

        }catch(Exception e){                                
           System.Assert(false, e.getMessage());
        }
    }

}