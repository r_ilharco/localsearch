@isTest
public class Test_ActivateProductionSubsCtrl {
	
    @testSetup
    public static void test_setupData(){
        ByPassFlow__c byPassFlow = new ByPassFlow__c();
		byPassFlow.Name = Userinfo.getProfileId();
		byPassFlow.FlowNames__c = 'Quote Management,Community User Creation,Send Owner Notification';
		insert byPassFlow;
		
        AccountTriggerHandler.disableTrigger = true;
        BillingProfileTriggerHandler.disableTrigger = true;
        List<Product2> productsToInsert = new List<Product2>();
        productsToInsert.add(generteFatherProduct());
        productsToInsert.add(generteChildProduct());
        insert productsToInsert;
        //User user = Test_DataFactory.generateUser();
        //insert user;
        List<Account> accounts = Test_DataFactory.generateAccounts('Test',1,false);
        accounts[0].Active_Contracts__c = 0;
        //accounts[0].SalesUser__c = user.Id;
        insert accounts[0];
        String accId = accounts[0].Id;
		List<Contact> contacts = Test_DataFactory.generateContactsForAccount(accId, 'Test', 1);        
        insert contacts;
        List<String> fieldNames = new List<String>(Schema.getGlobalDescribe().get('Billing_Profile__c').getDescribe().fields.getMap().keySet());
        String query =' SELECT ' +String.join( fieldNames, ',' ) +' FROM Billing_Profile__c WHERE Customer__c = :accId LIMIT 1';
        List<Billing_Profile__c> bps = Database.query(query);
        Billing_Profile__c billProfile = new Billing_Profile__c();       	
                billProfile.Customer__c	= accounts[0].Id;
                billProfile.Billing_Contact__c = Contacts[0].Id;
                billProfile.Billing_Language__c = 'French';
                billProfile.Billing_City__c	= 'test';
                billProfile.Billing_Country__c = 'test';
                billProfile.Billing_Name__c	= 'test bill name';
                billProfile.Billing_Postal_Code__c = '12345';
                billProfile.Billing_Street__c = 'test 123 Secret Street';	
                billProfile.Channels__c	= 'test';
                billProfile.Name = 'Test Bill Prof Name'; 
        Opportunity opportunity = Test_DataFactory.generateOpportunity('Test Opp', accounts[0].Id);
        insert opportunity;
        SBQQ__Quote__c quote = test_dataFactory.createQuotes('draft',opportunity, 1)[0];
        quote.SBQQ__Account__c = accounts[0].Id;
                quote.SBQQ__Opportunity2__c =opportunity.Id;
                quote.SBQQ__PrimaryContact__c = Contacts[0].Id;
                quote.Billing_Profile__c = billprofile.Id;
                quote.SBQQ__Type__c = 'Quote';
                quote.SBQQ__Status__c = 'Draft';
                quote.SBQQ__ExpirationDate__c = Date.today().AddDays(89);
                quote.SBQQ__Primary__c = true;
        
        //quote.SBQQ__SalesRep__c = user.Id;
        quote.Filtered_Primary_Contact__c = contacts[0].Id;
        quote.SBQQ__Primary__c = true;
        insert quote;
        opportunity.SBQQ__PrimaryQuote__c = quote.Id;
        update opportunity;
        
        QuoteLineTriggerHandler.disableTrigger = true;
        SBQQ__QuoteLine__c qlf = generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), null, 'Annual');
		insert qlf;
        SBQQ__QuoteLine__c qlc = generateQuoteLine(quote.Id, productsToInsert[0].Id, Date.today(), qlf.Id, 'Annual');
        insert qlc;
        QuoteLineTriggerHandler.disableTrigger = false;
		
        Order order = new Order();
        order.Custom_Type__c = ConstantsUtil.ORDER_TYPE_ACTIVATION;
        order.Type = ConstantsUtil.ORDER_TYPE_NEW;
        order.AccountId = accId;
        order.EffectiveDate = Date.today();
        order.Filtered_Primary_Contact__c = contacts[0].Id;
        order.SBQQ__Quote__c = quote.Id;
        order.Status = ConstantsUtil.ORDER_STATUS_DRAFT;
        insert order;
		order.Status = ConstantsUtil.ORDER_STATUS_ACTIVATED;        
        
        Contract contract = new Contract();
        contract.AccountId = accId;
        contract.Status = ConstantsUtil.CONTRACT_STATUS_DRAFT; 
        contract.SBQQ__Quote__c = quote.Id;
        contract.SBQQ__Order__c = order.Id;
        insert contract;
		
        SubscriptionTriggerHandler.disableTrigger = true;
        SBQQ__Subscription__c fatherSubscription = new SBQQ__Subscription__c();
        fatherSubscription.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION;
        fatherSubscription.SBQQ__Quantity__c = 1;
        fatherSubscription.Subscription_Term__c = '12';
        fatherSubscription.SBQQ__Product__c = productsToInsert[0].Id;
        fatherSubscription.SBQQ__Contract__c = contract.Id;
        fatherSubscription.SBQQ__Quoteline__c = qlf.Id;
        fatherSubscription.Total__c = 500;
        fatherSubscription.SBQQ__SubscriptionStartDate__c = Date.today();
        insert fatherSubscription;
        
        SBQQ__Subscription__c childSubscription = new SBQQ__Subscription__c();
        childSubscription.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
        childSubscription.SBQQ__RequiredById__c = fatherSubscription.Id;
        childSubscription.SBQQ__Quantity__c = 1;
        childSubscription.Subscription_Term__c = '12';
        childSubscription.SBQQ__Product__c = productsToInsert[1].Id;
        childSubscription.SBQQ__Contract__c = contract.Id;
        fatherSubscription.SBQQ__Quoteline__c = qlc.Id;
		childSubscription.SBQQ__RequiredById__c = fatherSubscription.Id;
        childSubscription.RequiredBy__c = fatherSubscription.Id;
        childSubscription.SBQQ__SubscriptionStartDate__c = Date.today();
        fatherSubscription.Total__c = 500;
        insert childSubscription;
		AccountTriggerHandler.disableTrigger = false;
        BillingProfileTriggerHandler.disableTrigger = false;
        SubscriptionTriggerHandler.disableTrigger = false;
    }
    
    @isTest
    public static void test_activateAllSubs(){
        Test.startTest();
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        List<ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper> results = new List<ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper>();
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        subs = [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__SubscriptionStartDate__c, SBQQ__Product__r.ProductCode, 
                                            RequiredBy__c, Subsctiption_Status__c, SBQQ__SubscriptionType__c, Subscription_Term__c
                                            FROM SBQQ__Subscription__c
                                            WHERE SBQQ__Contract__c = :contract.Id
                                            AND (Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION 
                                                 OR Subsctiption_Status__c = :ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE)];
        ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper result;
        for(SBQQ__Subscription__c s : subs){
            result = new ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper();
            result.subId = s.Id;
            result.productName = s.SBQQ__ProductName__c;
            result.subStatus = s.Subsctiption_Status__c;
            result.oldStartDate = s.SBQQ__SubscriptionStartDate__c;
            result.minDate = s.SBQQ__SubscriptionStartDate__c;
            result.maxDate = s.SBQQ__SubscriptionStartDate__c.addDays(363);
            system.debug('result: ' + result);
            results.add(result);
        }
        ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper result2 = new ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper(results[0]);
        ActivateProductionSubsCtrl.updateSubscriptions( JSON.serialize(results), contract.Id);
        ActivateProductionSubsCtrl.getSubscriptions(contract.Id);
        Test.stopTest();
        Contract contractUpdated = [SELECT Id,Status FROM Contract LIMIT 1];
        contractupdated.status = 'Active';
        update contractUpdated;
    }
    
    @isTest
    public static void test_activateAllSubsInProduction(){
        AccountTriggerHandler.disableTrigger = true;
        Test.startTest();
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        List<ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper> results = new List<ActivateProductionSubsCtrl.ActivateSubscriptionsWrapper>();
        List<SBQQ__Subscription__c> subs = new List<SBQQ__Subscription__c>();
        subs = [SELECT Id, Name, SBQQ__ProductName__c, SBQQ__SubscriptionStartDate__c, SBQQ__Product__r.ProductCode, 
                                            RequiredBy__c, Subsctiption_Status__c, SBQQ__SubscriptionType__c, Subscription_Term__c
                                            FROM SBQQ__Subscription__c
                                            WHERE SBQQ__Contract__c = :contract.Id];
        for(SBQQ__Subscription__c s : subs){
        	s.Subsctiption_Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION ;
        }
        SubscriptionTriggerHandler.disableTrigger = true;
        update subs;
        SubscriptionTriggerHandler.disableTrigger = false;
        ActivateProductionSubsCtrl.hasUserGrant();
        ActivateProductionSubsCtrl.getSubscriptions(contract.Id);        
        Test.stopTest();
        /*Contract contractUpdated = [SELECT Id,Status FROM Contract LIMIT 1];
        contractupdated.status = 'Active';
        update contractUpdated;*/
        AccountTriggerHandler.disableTrigger = false;
    }
    
    public static Product2 generteFatherProduct(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','MyWebsite Basic');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','false');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','false');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionTerm__c','12');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'Renewable');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL AWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Base_term_Renewal_term__c','12');
        fieldApiNameToValueProduct.put('Billing_Group__c',ConstantsUtil.BILLING_FREQUENCY_MONTHLY);
        fieldApiNameToValueProduct.put('Configuration_Model__c', ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN);
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','false');
        fieldApiNameToValueProduct.put('Priority__c','10');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','true');
        fieldApiNameToValueProduct.put('Subscription_Term__c','36');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','MYWEBSITEBASIC001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    public static Product2 generteChildProduct(){
		Map<String,String> fieldApiNameToValueProduct = new Map<String,String>();
        fieldApiNameToValueProduct.put('Family',ConstantsUtil.TST_FAMILY_PRESENCE);
        fieldApiNameToValueProduct.put('Name','Produktion');
        fieldApiNameToValueProduct.put('SBQQ__Component__c','true');
        fieldApiNameToValueProduct.put('SBQQ__NonDiscountable__c','true');
        fieldApiNameToValueProduct.put('SBQQ__SubscriptionType__c', 'One-Time');
        fieldApiNameToValueProduct.put('ExternalKey_ProductCode__c','ONL PWB');
        fieldApiNameToValueProduct.put('KSTCode__c','8934');
        fieldApiNameToValueProduct.put('KTRCode__c','1204020002');
        fieldApiNameToValueProduct.put('KTRDesignation__c','MyWebsite Basic');
        fieldApiNameToValueProduct.put('Downgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Downselling_Disable__c','true');
        fieldApiNameToValueProduct.put('Eligible_for_Trial__c','false');
        fieldApiNameToValueProduct.put('One_time_Fee__c','true');
        fieldApiNameToValueProduct.put('Product_Group__c','MyWebsite');
        fieldApiNameToValueProduct.put('Production__c','false');
        fieldApiNameToValueProduct.put('Subscription_Term__c','1');
        fieldApiNameToValueProduct.put('Upgrade_Disable__c','false');
        fieldApiNameToValueProduct.put('Upselling_Disable__c','false');
        fieldApiNameToValueProduct.put('Credit_Account__c','334301');
        fieldApiNameToValueProduct.put('ProductCode','O_MYWBASIC_PRODUKTION001');
        Product2 product = Test_DataFactory.generateProduct(fieldApiNameToValueProduct);
        product.SBQQ__AssetAmendmentBehavior__c = 'Default';
        product.SBQQ__AssetConversion__c = 'One per quote line';
        return product;        
    }
    
    public static SBQQ__QuoteLine__c generateQuoteLine(String quoteId, String productId, Date startDate, String requiredById, String billingFreq){
		SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__StartDate__c = startDate;
		quoteLine.CategoryID__c = 'testcategoryid';
        quoteLine.Category__c = 'Category Name';
        quoteLine.LocationID__c = 'testlocationid';
        quoteLine.Location__c = 'Location Name';
        quoteLine.SBQQ__Quantity__c= 1;
        quoteLine.SBQQ__ListPrice__c = 1290;
        quoteLine.Total__c = 490;
        quoteLine.Ad_Context_Allocation__c = 'testadcontextallocationid';
        quoteLine.Campaign_Id__c = 'testcampaignid';
        quoteLine.Contract_Ref_Id__c = 'testcontractrefid';
        quoteLine.Language__c = ConstantsUtil.TST_PREF_LANGUAGE;
        quoteLine.SBQQ__RequiredBy__c = requiredById;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.SBQQ__BillingFrequency__c =  billingFreq;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.Subscription_Term__c = '12';
		return quoteLine;
	}
}