/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-29-2019
 * Sprint      : 6
 * Work item   : SF2-270, Sprint 6, Wave C - Quote and Contract Template
 * Testclass   :
 * Package     : 
 * Description : Selector Class for Place__c
 * Changelog   : 
 */

public class SEL_Place {
	public static Map<Id, Place__c> getPlacesById(Set<Id> ids)
    {
        return new Map<Id, Place__c>(
            [
                SELECT Id, Name, Account__c, City__c, Company__c, Country__c, 
                Email__c, FirstName__c, GoldenRecordID__c, LastName__c, MobilePhone__c, 
                Phone__c, PlaceID__c, PlaceIdAliases__c, PlaceStatus__c, Place_Type__c, 
                PostalCode__c, Process__c, Product_Information__c, QuoteItemId__c, Status__c, 
                Street__c, Title__c, Website__c, PO_Box__c, PO_Box_City__c, PO_Box_Zip_Postal_Code__c, 
                Account__r.Name
                FROM Place__c
                WHERE Id IN :ids
            ]
        );
    }
}