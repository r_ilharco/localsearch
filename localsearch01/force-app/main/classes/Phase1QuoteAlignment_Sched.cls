global class Phase1QuoteAlignment_Sched implements Schedulable{
    
    global void execute(SchedulableContext sc){  
        List<String> batchs = new List<String>();
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf((system.now().minute()==59)?(Math.mod(system.now().hour()+1,24)):system.now().hour());
        String second = string.valueOf(system.now().second());
        String minute = string.valueOf(Math.Mod(system.now().minute()+1,60));
        String year = string.valueOf(system.now().year());
        List<AsyncApexJob> async = [select id from AsyncApexJob where status in('Processing','Queued','Preparing','Holding') and JobType in('BatchApex','BatchApexWorker')];
        
        for(CronTrigger ct: [select id,CronJobDetail.name, state from CronTrigger where CronJobDetail.name like 'Phase1ContractAlignment%']){
            if(ct.state == 'DELETED' || ct.state == 'COMPLETED')
                System.abortjob(ct.id);
            else 
                batchs.add(ct.CronJobDetail.name);
        }
        if(batchs.isEmpty()|| !batchs.contains('Phase1QuoteAlignment0')){
            Phase1QuoteAlignment b0 = new Phase1QuoteAlignment(new List<String>{'0','1','a','A','b','B'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' +  year;
            System.schedule('Phase1QuoteAlignment0', strSchedule, b0);
        } 
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment1')){ 
            Phase1QuoteAlignment b1 = new Phase1QuoteAlignment(new List<String>{'2','3','e','E','f','F'});
            String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment1', strSchedule,b1);
        } 
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment2')){
            Phase1QuoteAlignment b2 = new Phase1QuoteAlignment(new List<String>{'4','5','I','i','l','L'});
            String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment2', strSchedule,b2);
        } 
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment3')){
            Phase1QuoteAlignment b3 = new Phase1QuoteAlignment(new List<String>{'6','7','o','O','p','P'});
            String strSchedule = '0 ' +  minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment3', strSchedule, b3); 
        } 
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment4')){
            Phase1QuoteAlignment b4 = new Phase1QuoteAlignment(new List<String>{'8','9','s','S','t','T'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment4', strSchedule,b4);
        }
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment5')){
            Phase1QuoteAlignment b5 = new Phase1QuoteAlignment(new List<String>{'u','U','v','V','z','Z'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment5', strSchedule,b5);
        }
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment6')){
            Phase1QuoteAlignment b6 = new Phase1QuoteAlignment(new List<String>{'c','C','d','D','x','X','w','W'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment6', strSchedule,b6);
        }
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment7')){
            Phase1QuoteAlignment b7 = new Phase1QuoteAlignment(new List<String>{'g','G','h','H','y','Y'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment7', strSchedule,b7);
        }
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment8')){
            Phase1QuoteAlignment b8 = new Phase1QuoteAlignment(new List<String>{'m','M','n','N','j','J'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment8', strSchedule,b8);
        }
        if(batchs.isEmpty() || !batchs.contains('Phase1QuoteAlignment9')){
            Phase1QuoteAlignment b9 = new Phase1QuoteAlignment(new List<String>{'q','Q','r','R','k','K'});
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule('Phase1QuoteAlignment9', strSchedule,b9);
        }
    }
}