/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Giovanni Prota <gprota@deloitte.it>
 * Date		   : 15-11-2019
 * Sprint      : 
 * User Story  : 
 * Testclass   : 
 * Package     : 
 * Description : 
 * Changelog   : 
 */

public class SRV_ActivationContractBatch {
    
    public static List<Contract> activateSubscriptionContract(List<Contract> contracts){
        
        Boolean toUpdate;
        for(Contract c: contracts){
            toUpdate = true;    
            for(SBQQ__Subscription__c s: c.SBQQ__Subscriptions__r){
                if(s.SBQQ__SubscriptionStartDate__c <= System.today())
                    s.Status__c = ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE;
                else if(s.Status__c != ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE){
                    toUpdate = false;
                    break;
                }
            }
            if(toUpdate)
                c.Status = ConstantsUtil.CONTRACT_STATUS_ACTIVE;
        }
        
        try{
        	Database.update(contracts, false);
        }
        catch(DmlException e) {
    		System.debug('The following exception has occurred: ' + e.getMessage());
		}

        return contracts;
    }
 
}