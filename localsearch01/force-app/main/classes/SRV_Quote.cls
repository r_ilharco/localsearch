/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 06-24-2019
 * Sprint      : 4
 * Work item   : SF2-190 - Immediate downgrade through approval process 
 * Testclass   :
 * Package     : 
 * Description : Service Class for SBQQ__Quote__c
 * Changelog   : 
 */

public class SRV_Quote {
    
    public static  Map<String, List<SBQQ__Quote__c>> constructQuoteMapByType(List<SBQQ__Quote__c > listNew)
    {
        Map<String, List<SBQQ__Quote__c>> result = new Map<String, List<SBQQ__Quote__c>>();
    	
        for(SBQQ__Quote__c quote : listNew)
        {
            if(!result.containsKey(quote.SBQQ__Type__c))
            {
                result.put(quote.SBQQ__Type__c, new List<SBQQ__Quote__c>());
            }
            
            result.get(quote.SBQQ__Type__c).add(quote);
        }
        
        return result;
    }
}