@isTest
public class PlaceNamePBTest {

    @isTest
    static void testPlaceName(){
        
        try{
        	Account acc = new Account();
            acc.Name = 'test acc';
            acc.GoldenRecordID__c = 'GRID';       
            acc.POBox__c = '10';
            acc.P_O_Box_Zip_Postal_Code__c ='101';
            acc.P_O_Box_City__c ='Dhaka';    
                       
            insert acc;
        	System.debug('Account' +acc);

        	Place__c place = new Place__c();
            place.Name = 'test place';
            place.LastName__c = 'test last';
            place.Company__c = 'test company';
            place.City__c = 'Dhaka';
            place.Country__c = 'test coountry';
            place.PostalCode__c = '101';
            place.Account__c = acc.Id;
        	//Place.Product_Information__c='SLT001';
            
           
            insert place;
            
            System.debug('Place: '+ place);
       
            //Place__c pc=[Select SearchField_Name__c from Place__c ];
           // system.debug('SF Name' +pc);
           // 
    	}catch(Exception e)
        {
            System.assert(false, e.getMessage());
        }
        
    }
  
   
}