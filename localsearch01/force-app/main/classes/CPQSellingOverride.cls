/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Maturo Fabio <fmaturo@deloitte.it>
 * Date		   : 06-02-2019
 * Sprint      : Sprint 2
 * Work item   : US_74 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (upgrade) to replace a product with another one.
 * 				 US_75 SF-38, Sprint2, Wave 1, Customer asks for a contract amendment (downgrade) to replace a product with another one
 * Testclass   :
 * Package     : 
 * Description : Search Custom - It overrides the standard product search in Edit Lines  
 * Changelog   : 
 * 				#1 SF2-179 Compatibility rules between Lbx e Swisslist during upgrade/downgrade - gcasola - 06-26-2019
 */

global class CPQSellingOverride implements SBQQ.ProductSearchPlugin{
	/**
	 * Constructor. Not required for implementation
	 */
	global CPQSellingOverride(){
		System.debug('METHOD CALLED: ExampleProductSearchPlugin Constructor');
	}
    
    //Metodo non presente nel esempio del plugin ma richiesta come override per fare l'implement della classe
    global String getAdditionalSuggestFilters(SObject quote, Map<String,Object> fieldValuesMap){
        
        return NULL;
    }
    
    //Metodo non presente nel esempio del plugin ma richiesta come override per fare l'implement della classe
    global List<PriceBookEntry> suggest(SObject quote, Map<String,Object> fieldValuesMap){
        
        return NULL;
    }
	
    global Boolean isFilterHidden(SObject quote, String fieldName){
		// This would hide Product Code filter if Quote Status is Approved
		//return quoteObj.SBQQ__Status__c == 'Approved';
		
		return false;
	}
    
    global String getFilterDefaultValue(SObject quote, String fieldName){
		System.debug('METHOD CALLED: getFilterDefaultValue');
		// This would set Product Family filter to Service if Quote Type is Quote
		//return (fieldName == 'Family' && quoteObj.SBQQ__Type__c == 'Upgrade') ? 'Advertising' : NULL;
		
		return NULL;
	}
    
    global Boolean isSearchCustom(SObject quote, Map<String,Object> fieldValuesMap){
		System.debug('METHOD CALLED: isSearchCustom');
		/*
		// This would use CUSTOM mode if the Quote Type is Upgrade or Downgrade
		*/
        
        //Boolean isOnePresence = false;
        Boolean isUpgradeOrDowngradeOrAmendment = false;
        
        SBQQ__Quote__c quoteObj = (SBQQ__Quote__c) quote;
        //List<Pricebook2> onePresencePricebook = [SELECT Id FROM Pricebook2 WHERE ExternalId__c = :ConstantsUtil.ONEPRESENCE_PRICEBOOK];
        //if(!onePresencePricebook.isEmpty() && onePresencePricebook[0].Id == quoteObj.SBQQ__PricebookId__c) isOnePresence = true;
        
        //System.debug('Is One Presence: ' + isOnePresence);
        //System.debug('Quote: ' + quoteObj);
        
        isUpgradeOrDowngradeOrAmendment = ( ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(quoteObj.SBQQ__Type__c)
                                || ConstantsUtil.Quote_StageName_Accepted.equalsIgnoreCase(quoteObj.SBQQ__Status__c)         
                                || ConstantsUtil.QUOTE_TYPE_AMENDMENT.equalsIgnoreCase(quoteObj.SBQQ__Type__c)
                                || ConstantsUtil.QUOTE_TYPE_DOWNGRADE.equalsIgnoreCase(quoteObj.SBQQ__Type__c)
                                || ConstantsUtil.QUOTE_TYPE_RENEWAL.equalsIgnoreCase(quoteObj.SBQQ__Type__c) ) ? true : false;
		
        return isUpgradeOrDowngradeOrAmendment;
	}
    
    global String getAdditionalSearchFilters(SObject quote, Map<String,Object> fieldValuesMap){
		System.debug('METHOD CALLED: getAdditionalSearchFilters');
        //Boolean isOnePresence = false;
        SBQQ__Quote__c quoteObj = (SBQQ__Quote__c) quote;
        //List<Pricebook2> onePresencePricebook = [SELECT Id FROM Pricebook2 WHERE ExternalId__c = :ConstantsUtil.ONEPRESENCE_PRICEBOOK];
        //if(!onePresencePricebook.isEmpty() && onePresencePricebook[0].Id == quoteObj.SBQQ__PricebookId__c) isOnePresence = true;
        
        Boolean isRenewal = false;
        Boolean isUpgrade = false;
        String additionalFilter = NULL;
        
        //SBQQ__Quote__c quoteObj = (SBQQ__Quote__c) quote;
        isRenewal = (ConstantsUtil.QUOTE_TYPE_RENEWAL.equalsIgnoreCase(quoteObj.SBQQ__Type__c)) ? true : false;
        isUpgrade = (ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(quoteObj.SBQQ__Type__c)) ? true : false;
        
		if(isRenewal){
			additionalFilter = 'Product2.Family != \'' + ConstantsUtil.PRODUCT2_FAMILY_DISUSED + '\'';
        }/*else if(isUpgrade && isOnePresence){
            additionalFilter = 'Product2.ProductCode NOT IN (\'' + ConstantsUtil.MYWEBFREE + '\',\'' + ConstantsUtil.MCOFREE + '\',\'' +  ConstantsUtil.LOCALINAFREE + '\')'; 
        }*/
        
		System.debug('additionalFilter '+additionalFilter);
		return additionalFilter;
        
        
		/*
		// This would add an extra inventory filter if the family is Hardware
		String additionalFilter = NULL;

		if(fieldValuesMap.get('Family') == 'Hardware'){
			additionalFilter = 'AND Product2.Inventory_Level__c > 3';
		}

		return additionalFilter;
		*/
		//return NULL;
	}
    
    global List<PriceBookEntry> search(SObject quote, Map<String,Object> fieldValuesMap){
		System.debug('METHOD CALLED: search');
        
		//GET ALL POSSIBLE FILTER FIELDS FROM THE SEARCH FILTER FIELD SET
		List<Schema.FieldSetMember> searchFilterFieldSetFields = SObjectType.Product2.FieldSets.SBQQ__SearchFilters.getFields();

		//GET ALL POSSIBLE FIELDS FROM THE SEARCH RESULTS FIELD SET
		List<Schema.FieldSetMember> searchResultFieldSetFields = SObjectType.Product2.FieldSets.SBQQ__SearchResults.getFields();
        
        List<String> relatedGroups = new List<String>();
        List<String> includedGroupsInSearch = new List<String>();
        List<SBQQ__Subscription__c> relatedSubs = new List<SBQQ__Subscription__c>();
        List<PriceBookEntry> allowedPbes = new List<PriceBookEntry>();
        Map<String, String> existingProducts = new Map<String, String>();
        
        SBQQ__Quote__c quoteObj = (SBQQ__Quote__c) quote;
        
        //Get subscription related to this quote                
        relatedSubs = getRelatedSubscriptions(quoteObj); 
        //Get Product Group of all Items of the Contract  
        relatedGroups = getProductGroupOfContract(relatedSubs);
        
        Set<String> relatedConfigModel = new Set<String>();
        for(SBQQ__Subscription__c aSub : relatedSubs){
            if( (ConstantsUtil.PRODUCT2_SUBCONFIGMODEL_AUTO_REN).equalsIgnoreCase(aSub.SBQQ__Product__r.Configuration_Model__c) ){
            	relatedConfigModel.add(aSub.SBQQ__Product__r.Configuration_Model__c);
            }
        }

        includedGroupsInSearch.addAll(relatedGroups);
        
        List<Upgrade_Downgrade_Contract__mdt> upgradeSettings = [SELECT MasterLabel, CrossGroup__c 
                                                                 FROM 	Upgrade_Downgrade_Contract__mdt 
                                                                 WHERE MasterLabel IN :relatedGroups];
        
        Map<String,String> allowedCrossGroupsMap = new Map<String,String>();
        
        if(!upgradeSettings.isEmpty()){
            for(Upgrade_Downgrade_Contract__mdt upgradeSetting : upgradeSettings){
                if(String.isNotBlank(upgradeSetting.CrossGroup__c)){
                    List<String> allowedCrossGroups = upgradeSetting.CrossGroup__c.split(';');
                    includedGroupsInSearch.addAll(allowedCrossGroups);
                    for(String allowedCrossGroup : allowedCrossGroups) allowedCrossGroupsMap.put(allowedCrossGroup, upgradeSetting.MasterLabel);   
                }
            }
        }
        
        Boolean disused = false;
        if(!relatedSubs.isEmpty()){
            for(SBQQ__Subscription__c currentSubscription : relatedSubs){
                if(String.isNotBlank(currentSubscription.SBQQ__Product__r.Priority__c)){
                    
                    String priority = currentSubscription.SBQQ__Product__r.Priority__c;
                    String productGroup = currentSubscription.SBQQ__Product__r.Product_Group__c;
                    Boolean isDisused = ConstantsUtil.PRODUCT2_FAMILY_DISUSED.equalsIgnoreCase(currentSubscription.SBQQ__Product__r.Family);
                    Boolean isMigrated = currentSubscription.SBQQ__OrderProduct__r.SambaMigration__c;
                    
                    if(isMigrated &&  isDisused) disused = true;
                    if(!existingProducts.containsKey(productGroup)) existingProducts.put(productGroup, priority);
                    else if(existingProducts.get(productGroup) > priority) existingProducts.put(productGroup, priority);
                }               
            }
        }
        if(ConstantsUtil.QUOTE_TYPE_RENEWAL.equalsIgnoreCase(quoteObj.SBQQ__Type__c)){
            allowedPbes = new List<PriceBookEntry>();
        }
        else if (!ConstantsUtil.QUOTE_TYPE_AMENDMENT.equalsIgnoreCase(quoteObj.SBQQ__Type__c) && !ConstantsUtil.Quote_StageName_Accepted.equalsIgnoreCase(quoteObj.SBQQ__Status__c)) {
            
            String selectClause = 'SELECT ';
            
            for(Schema.FieldSetMember field : searchResultFieldSetFields){
                selectClause += 'Product2.' + field.getFieldPath() + ', ';
            }
            selectClause += 'Id, UnitPrice, PriceBook2Id, Product2Id, Product2.Id, Product2.Priority__c, Product2.Product_Group__c, Product2.ProductCode';
            
            String whereClause = '';
            String disusedFamily = 'Disused';
            for(Schema.FieldSetMember field : searchFilterFieldSetFields){
                if(!fieldValuesMap.containsKey(field.getFieldPath())){
                    continue;
                }
                
                if(field.getType() == Schema.DisplayType.String || field.getType() == Schema.DisplayType.Picklist){
                    whereClause += 'Product2.' + field.getFieldPath() + ' LIKE \'%' + fieldValuesMap.get(field.getFieldPath()) + '%\' AND ';
                }
            }
            
            whereClause += 'IsActive = TRUE AND PriceBook2Id = \'' + quote.get('SBQQ__PricebookId__c') + '\'';
            
            if(!includedGroupsInSearch.isEmpty()){
                whereClause += ' AND Product2.Product_Group__c IN: includedGroupsInSearch and Product2.Family != \'' + ConstantsUtil.PRODUCT2_FAMILY_DISUSED + '\' '; 
            }
            
            if(!relatedConfigModel.isEmpty() && !includedGroupsInSearch.contains(ConstantsUtil.PRODUCT2_PRODUCTGROUP_SWISSLIST)){
                whereClause += ' AND Product2.Configuration_Model__c IN: relatedConfigModel';
            }
            
            String query = selectClause + ' FROM PriceBookEntry WHERE ' + whereClause;

            
            List<PriceBookEntry> pbes = new List<PriceBookEntry>();
                         
            System.debug('includedGroupsInSearch -- '+includedGroupsInSearch);
            System.debug('search custom for product -- '+query); 
            pbes = Database.query(query);

            System.debug('price book entry -- '+pbes);
            
            for(PriceBookEntry aPbe : pbes){
                if(String.isNotBlank(aPbe.Product2.Priority__c)){
                    
                    Integer existingPriority;
                    if(relatedGroups.contains(aPbe.Product2.Product_Group__c)) existingPriority = Integer.valueOf(existingProducts.get(aPbe.Product2.Product_Group__c));          
                    else existingPriority = Integer.valueOf(existingProducts.get(allowedCrossGroupsMap.get(aPbe.Product2.Product_Group__c)));
                    
                    System.debug('existingPriority '+existingPriority);
                    if(ConstantsUtil.QUOTE_TYPE_UPGRADE.equalsIgnoreCase(quoteObj.SBQQ__Type__c)){  
                        if(disused && Integer.valueOf(aPbe.Product2.Priority__c) >= existingPriority) allowedPbes.add(aPbe); 
                        else if(!disused && !relatedConfigModel.isEmpty()){
                            if(ConstantsUtil.ONEPRESENCE_PRODUCTGROUP.equalsIgnoreCase(aPbe.Product2.Product_Group__c)){
                                if(Integer.valueOf(aPbe.Product2.Priority__c) >=  existingPriority) allowedPbes.add(aPbe);
                            }
                            else if(Integer.valueOf(aPbe.Product2.Priority__c) >  existingPriority) allowedPbes.add(aPbe);
                        }
                        else if(!disused){
                            if(Integer.valueOf(aPbe.Product2.Priority__c) >=  existingPriority) allowedPbes.add(aPbe);
                        }
                            
                    }
                    else if(ConstantsUtil.QUOTE_TYPE_DOWNGRADE.equalsIgnoreCase(quoteObj.SBQQ__Type__c)){                                             
                        if(Integer.valueOf(aPbe.Product2.Priority__c) <  existingPriority){            
                            allowedPbes.add(aPbe); 
                        }
                    }
                }
            }
        }
		System.debug('returned values -- '+allowedPbes);
		return allowedPbes;
	}
    
    global Boolean isInputHidden(SObject quote, String input){
		System.debug('METHOD CALLED: isInputHidden');
		/*
		// This would hide an Input called 'Urgent Shipment' on Fridays.
		return input == 'Urgent Shipment' && Datetime.now().format('F') == 5;
		*/
		return false;
	}
    
    global String getInputDefaultValue(SObject quote, String input){
		System.debug('METHOD CALLED: getInputDefaultValue');

		return NULL;
	}
      
	global Boolean isSuggestCustom(SObject quote, Map<String,Object> inputValuesMap){
		return true;
	}
    
	@TestVisible
    private List<SBQQ__Subscription__c> getRelatedSubscriptions(SBQQ__Quote__c quote){
        id pricebook = quote?.SBQQ__PricebookId__c;
        List<SBQQ__Subscription__c> relatedSub = new List<SBQQ__Subscription__c>();
        
        //Get All Subscriptions related to the starting Contract
        relatedSub = [ SELECT Id, SBQQ__ProductId__c, SBQQ__Product__r.Configuration_Model__c ,SBQQ__Product__r.Priority__c, SBQQ__Product__r.Product_Group__c,SBQQ__Product__r.Family, SBQQ__OrderProduct__r.SambaMigration__c 
                       FROM SBQQ__Subscription__c 
                       WHERE (SBQQ__Product__r.PlaceIDRequired__c = 'Yes' OR SBQQ__RequiredByProduct__c = NULL)
                       AND SBQQ__Contract__c IN (SELECT Upgrade_Downgrade_Contract__c 
                                                 FROM SBQQ__Quote__c 
                                                 WHERE Id=: quote.Id)
                       AND SBQQ__OrderProduct__r.PricebookEntry.IsActive = true
                       AND SBQQ__Product__c not in (select Product2Id from PricebookEntry where IsActive = false and Pricebook2Id = :pricebook)
                       AND Subsctiption_Status__c =: ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE
                     ];
        
        return relatedSub;
    }
    @TestVisible
    private List<String> getProductGroupOfContract(List<SBQQ__Subscription__c> relatedSub){
        
        List<String> relatedGroups = new List<String>();

        if(!relatedSub.isEmpty()){
            
            for(SBQQ__Subscription__c aSub : relatedSub){
                 if(aSub.SBQQ__Product__r.Product_Group__c != null){
                 	relatedGroups.add(aSub.SBQQ__Product__r.Product_Group__c);  
                 }
            }
            
        }
        
        return relatedGroups;  
    }
    
}