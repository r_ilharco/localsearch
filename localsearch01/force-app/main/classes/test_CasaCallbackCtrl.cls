@isTest
public class test_CasaCallbackCtrl {
    @isTest 
    static void test_SuccessPublish(){
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken');
        Test_AdContextReservationMock mock = new Test_AdContextReservationMock();
        mock.simulateError = false;
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        PageReference callbackFromCasa = Page.callbackFromCasa;
        Test.setCurrentPage(callbackFromCasa);
        ApexPages.currentPage().getParameters().put('category','testCategoryId');
        ApexPages.currentPage().getParameters().put('region','testRegionId');
        ApexPages.currentPage().getParameters().put('slot','testSlot');
        ApexPages.currentPage().getParameters().put('language','testLanguage');
        ApexPages.currentPage().getParameters().put('quote_item','testQuoteLineId');
        ApexPages.currentPage().getParameters().put('price','129000');
        ApexPages.currentPage().getParameters().put('ad_context_allocation','testAllocationId');
        ApexPages.currentPage().getParameters().put('product_code','testProductCode');
        ApexPages.currentPage().getParameters().put('start_date',String.valueOf(Date.today()));
        ApexPages.currentPage().getParameters().put('end_date',String.valueOf(Date.today()));
        ApexPages.currentPage().getParameters().put('expiration_date',String.valueOf(Date.today()));
        CasaCallbackCtrl ctrl = new CasaCallbackCtrl();
        ctrl.init();
        Test.stopTest();
    }
    
    @isTest 
    static void test_FailedPublish(){
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = :ConstantsUtil.SysAdmin LIMIT 1];
        insert new Mashery_Setting__c(SetupOwnerId=p[0].Id, Token__c = 'testToken');
        Test_AdContextReservationMock mock = new Test_AdContextReservationMock();
        mock.simulateError = true;
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        PageReference callbackFromCasa = Page.callbackFromCasa;
        Test.setCurrentPage(callbackFromCasa);
        ApexPages.currentPage().getParameters().put('category','testCategoryId');
        ApexPages.currentPage().getParameters().put('region','testRegionId');
        ApexPages.currentPage().getParameters().put('slot','testSlot');
        ApexPages.currentPage().getParameters().put('language','testLanguage');
        ApexPages.currentPage().getParameters().put('quote_item','testQuoteLineId');
        ApexPages.currentPage().getParameters().put('price','129000');
        ApexPages.currentPage().getParameters().put('ad_context_allocation','testAllocationId');
        ApexPages.currentPage().getParameters().put('product_code','testProductCode');
        ApexPages.currentPage().getParameters().put('start_date',String.valueOf(Date.today()));
        ApexPages.currentPage().getParameters().put('end_date',String.valueOf(Date.today()));
        ApexPages.currentPage().getParameters().put('expiration_date',String.valueOf(Date.today()));
        CasaCallbackCtrl ctrl = new CasaCallbackCtrl();
        ctrl.init();
        Test.stopTest();
    }
}