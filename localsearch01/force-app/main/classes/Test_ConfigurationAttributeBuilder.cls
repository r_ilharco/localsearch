/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 07-18-2019
 * Sprint      : Sprint 6
 * Work item   : SF2-241 - Product set-up - Multiple values management for Invoice cycle
 * Testclass   : 
 * Package     : 
 * Description : Test for the Script for the generation of Configuration Attributes
 * Changelog   : 
 */

@isTest
public class Test_ConfigurationAttributeBuilder {
	@isTest
    public static void test_generateConfigurationAttributes_start()
    {
        Swisslist_CPQ.config();
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id, ProductCode, Subscription_Term__c FROM Product2]);
        Map<String, Id> productIdByCode = new Map<String, Id>();
        
        for(Product2 prod : products.values())
        {
            productIdByCode.put(prod.ProductCode, prod.Id);
        }
        
        ConfigurationAttributeBuilder.productIds = new Set<Id>{productIdByCode.get('SLS001')};
        
        Test.startTest();
        	ConfigurationAttributeBuilder.start();
        Test.stopTest();
        
        List<SBQQ__ConfigurationAttribute__c> configurationAttributes = [SELECT Id, SBQQ__Product__c FROM SBQQ__ConfigurationAttribute__c];
        
        System.assertEquals(productIdByCode.get('SLS001'), configurationAttributes[0].SBQQ__Product__c);
        System.assertEquals(1,configurationAttributes.size());
        
    }
}