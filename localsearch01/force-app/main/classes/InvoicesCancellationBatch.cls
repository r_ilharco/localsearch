/**
 * Copyright (c), Deloitte Digital
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the Deloitte Digital nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * Author	   : Luciano di Martino <ldimartino@deloitte.it>
 * Date		   : 2020-08-21
 * Sprint      : Sprint 4
 * Work item   : 
 * Testclass   : 
 * Package     : 
 * Description : Massive invoices cancellation
 * Changelog   : 
 */

public without sharing class InvoicesCancellationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    private List<String> invoiceStatusList;
    
    public InvoicesCancellationBatch(List<String> statusList) {
        this.invoiceStatusList = new List<String>(statusList);
        system.debug('statusList in constructor: ' + statusList);
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext){
        system.debug('invoiceStatusList: '+ this.invoiceStatusList);
        String query = 'SELECT Id, Correlation_Id__c, Name, Payment_Status__c, Registered__c, Status__c, Customer__c, Customer__r.Name, Customer__r.Customer_Number__c, Open_Amount__c, Total__c FROM Invoice__c WHERE Technical_Issue__c = TRUE AND Status__c IN :invoiceStatusList';
        return Database.getQueryLocator(query);
                                     
    }

    public void execute(Database.BatchableContext batchableContext, List<Invoice__c> scope) {
        List<Error_Log__c> errors = new List<Error_Log__c>();
        List<Id> idsToCancel = new List<Id>();
        List<Invoice__c> invoicesToUpdate = new List<Invoice__c>();
        system.debug('scope: ' + scope);
        for(Invoice__c invoice : scope) { 
            try{
                //This batch will be launched only in emergency situations, it is not daily scheduled. 
                //So even if it is not a best practice we put the function in a for loop in order to manage the invoices  
                //because the inner function can handle only one invoice at time. 
                
                //if(!Test.isRunningTest()) CancelInvoiceController.cancelByInvoiceId(invoice.Id);
                //Swissbilling START
                string correlationId = BillingHelper.getGUID(); // Not the correlation id of the invoice (Swissbilling will use docNumber as reference)
                SwissbillingHelper.ResponseWrapper responseToReturn = new SwissbillingHelper.ResponseWrapper();
                Map<string, Object> documentData = new Map<string, Object> {
                    'date' => SwissbillingHelper.toISO8601String(DateTime.now()),
                        'docGeneral' => new Map<string, object> {
                            'docId' => correlationId,
                                'clientId' => invoice.Customer__r.Customer_Number__c,
                                'clientName' => Invoice.Customer__r.Name,
                                'docType' => 'cancellation'
                                },
                                    'docHeader' => new Map<string, object> {
                                        'customerNumber' => SwissbillingHelper.getCustomerNumber(invoice.Customer__r.Customer_Number__c),
                                            'docNumber' => long.valueOf(invoice.Name.replaceAll('[^0-9]', ''))
                                            }
                };
				string documentJson = JSON.serialize(documentData, true);
                system.debug('documentJson: ' + documentJson);
                HttpRequest req = new HttpRequest();
                req.setEndpoint(BillingHelper.billingEndpoint + '/jobs/transfer');
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CORRELATION_ID, correlationId);
                req.setHeader(ConstantsUtil.TRACE_HEAD_BP_NAME, 'SwissbillingInvoiceCancellation');
                req.setHeader(ConstantsUtil.TRACE_HEAD_INITIATOR, 'Salesforce');
                req.setHeader(ConstantsUtil.TRACE_HEAD_CALLING_APP, 'Billing');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_NAME, 'Invoice__c');
                req.setHeader(ConstantsUtil.TRACE_HEAD_BO_ID, invoice.Id);         
                req.setTimeout(60000);
                req.setBody(documentJson);
                HttpResponse res = new Http().send(req);
                System.debug(documentJson);
                integer statusCode = res.getStatusCode();
                string returnValue = res.getBody();
                system.debug('swissbilling response: ' + responseToReturn);
                if(statusCode == 200) {
					errors.add(ErrorHandler.createLogInfo('Billing', 'Massive invoices cancellation OK', 'Bill cancelled on Swissbilling', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice));
                    invoice.Status__c = ConstantsUtil.INVOICE_STATUS_CANCELLED; // from sb status update?
                    invoicesToUpdate.add(invoice);
                    idsToCancel.add(invoice.Id);
                }else{
					throw new SwissbillingHelper.SwissbillingHelperException('Invoice Cancellation (Swissbilling) ' + res.getStatusCode() + '\n' + res.getBody() + '\n'+ documentJson , ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                }
                
                //ABACUS START
                AbacusCancellationJsonWrapper abacusWrapper = new AbacusCancellationJsonWrapper(invoice.Name , Date.today());
                String AbacusPayload = JSON.serialize(abacusWrapper);
                HttpResponse abacusResponse = SRV_Abacus.postCallout(AbacusPayload, invoice.Correlation_Id__c, ConstantsUtil.ABACUS_CREDIT_NOTE);
                system.debug('abacusResponse: ' + abacusResponse);
                if(abacusResponse.getStatusCode() == 200){
					errors.add(ErrorHandler.createLogInfo('Billing', 'Massive invoices cancellation OK', 'Bill cancelled on Abacus', 'Invoice number: ' + invoice.Name + ' Invoice Id:' + invoice.Id, invoice.Correlation_Id__c, invoice));
                }else{
                    throw new SwissbillingHelper.SwissbillingHelperException('Invoice Cancellation (ABACUS) ' + abacusResponse.getStatusCode() + '\n' + abacusResponse.getBody() + '\n'+ AbacusPayload , ErrorHandler.ErrorCode.E_HTTP_BAD_RESPONSE);
                }
                //ABACUS END
            }catch(Exception ex){
                errors.add(ErrorHandler.createLogError('Billing', 'Massive invoices cancellation failure', ex, ErrorHandler.ErrorCode.E_UNKNOWN, null,ex.getMessage() + '/n' + ex.getStackTraceString(), invoice.Correlation_Id__c));
            }
        }
		if(invoicesToUpdate.size()> 0) {
            update invoicesToUpdate;
        }
        if(idsToCancel.size()> 0) {
			Billing.resetSubscriptions(idsToCancel);
        }
        if(errors.size()> 0) {
            insert errors;
        }
	}

    public void finish(Database.BatchableContext batchableContext){
        
    }
}