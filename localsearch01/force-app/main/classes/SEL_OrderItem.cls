/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author       : Vincenzo Laudato <vlaudato@deloitte.it>
* Date         : 22-07-2019
* Sprint      : 5
* User Story  : SF-210
* Testclass   : 
* Package     : 
* Description : Order Management
* Changelog   : 
*/

public without sharing class SEL_OrderItem {
    
    //Query All Feature_Toggle__mdt 
    public static Map<String,Boolean> ReadFeatureToggle(){
        Map<String, Boolean> features = new Map<String, Boolean>();
        for (Feature_Toggle__mdt  feature : [SELECT DeveloperName, Active__c FROM Feature_Toggle__mdt ]) {
            features.put(feature.DeveloperName, feature.Active__c);
            system.debug('features:'+feature);
        }

        return features;
    }
       
    
    public static List<OrderItem> getOrderItemsByPlaceProductAndStatus(Set<Id> placeIds, Set<String> invalidStatuses){
        return [SELECT  Id,
                Place__c,
                Order.AccountId,
                ServiceDate,
                Order.OrderNumber,
                Product2Id,
                Product2.Product_Group__c
                FROM    OrderItem
                WHERE   Place__c IN :placeIds
                AND     Order.Status NOT IN :invalidStatuses];
    }

    public static List<OrderItem> getOrderItemsByProdAccountAndStatus(Set<Id> accountIds){
        return [SELECT 	Id,
                		Order.AccountId,
                		CategoryID__c,
                		LocationID__c,
                		Language__c,
                		Product2.Product_Group__c
                FROM	OrderItem
                WHERE	Order.AccountId IN :accountIds
                AND		Product2.Product_Group__c IN (:ConstantsUtil.PRODUCT_GROUP_LOCALBANNER, :ConstantsUtil.PRODUCT_GROUP_SEARCHBANNER)
                AND 	Order.Status NOT IN (:ConstantsUtil.ORDER_STATUS_ACTIVATED, :ConstantsUtil.ORDER_STATUS_CANCELLED)];
    }
    
    public static List<OrderItem> getOrderItemsByIds(Set<Id> orderItemIds){
        return [SELECT  Id,
                Place__c,
                Campaign_Id__c,
                Location__c,
                Category__c,
                CategoryID__c,
                LocationID__c,
                TLPaket_Region__c,
                Contract_Ref_Id__c,
                Editorial_Content__c,
                SambaMigration__C,
                Url__c,
                Url2__c,
                Url3__c,
                Url4__c,
                SBQQ__RequiredBy__c,
                Package_Total__c,
                Activate_Later__c, 
                SBQQ__QuotedListPrice__c,
                Manual_Activation__c,
                Production__c,
                Order.SambaIsBilled__c,
                End_date__c,
                Order.SambaIsStartConfirmed__c,
                Order.ContractSAMBA_Key__c,
                Ad_Context_Allocation__c,
                Order.Contract__c,
                Keywords__c,
                Placement_Phone__c,
                Manual_Termination_Product__c,
                Manual_Amendment_Product__c,
                ListPrice,
                Manuscript__c,
                Product2.ProductCode,
                Product2.ExternalKey_ProductCode__c,
                ServiceDate,
                SBQQ__SubscriptionType__c,
                Language__c,
                SBQQ__QuoteLine__r.SBQQ__Quote__c,
                SBQQ__QuoteLine__r.SBQQ__ListPrice__c,
                SBQQ__QuoteLine__r.SBQQ__Quote__r.SBQQ__CustomerDiscount__c,
                Simulation_Number__c,
                LBx_Contact__c,
                Order.EffectiveDate,
                NonPriceRelevant__c,
                OrderId
                FROM    OrderItem
                WHERE   Id IN :orderItemIds];
    }
    
    public static List<OrderItem> getSubsToTerminateOrderId(Set<Id> orderIds){
        return [SELECT  Id,
                SBQQ__Status__c,
                Quantity,
                Subscription_To_Terminate__c,
                OrderItemNumber
                FROM    OrderItem
                WHERE   OrderId = :orderIds
                and Subscription_To_Terminate__c != null];
    }
    
    public static List<OrderItem> getOrderItemByOrderIds(Set<Id> orderId){
        return [SELECT  Id,
                SBQQ__RequiredBy__c,
                SBQQ__QuoteLine__r.SBQQ__RequiredBy__c, 
                SambaIsBilled__c, 
                SambaIsStartConfirmed__c,
                OrderId,
                SBQQ__Subscription__c,
                Subscription_To_Terminate__c
                FROM    OrderItem
                WHERE   OrderId in:orderId
                ORDER BY SBQQ__RequiredBy__c];
    }
    
    public static List<OrderItem> getParentOrderItemByOrderIds(Set<Id> orderId){
        return [SELECT  Id,
                SBQQ__RequiredBy__c,
                SBQQ__QuoteLine__r.SBQQ__RequiredBy__c, 
                SambaIsBilled__c, 
                SambaIsStartConfirmed__c,
                OrderId,
                SBQQ__Subscription__c,
                Subscription_To_Terminate__c
                FROM    OrderItem
                WHERE   OrderId in:orderId
                and  SBQQ__RequiredBy__c = NULL];
    }
    
    public static List<OrderItem> getOrderItemByOrderId(Id orderId){
        return [SELECT  Id, 
                SBQQ__Status__c,
                Quantity,
                PricebookEntry.Product2.Name,
                PricebookEntry.Product2.ProductCode,
                Product2Id,
                SBQQ__RequiredBy__c,
                UnitPrice,
                PricebookEntryId,
                SBQQ__Contract__c,
                SBQQ__QuoteLine__c,
                External_Id__c,
                SBQQ__Subscription__c,
                SBQQ__Subscription__r.External_Id__c,
                SBQQ__Subscription__r.SBQQ__Product__r.Base_term_Renewal_term__c,
                SBQQ__Subscription__r.Subscription_Term__c,
                SBQQ__Subscription__r.SBQQ__ListPrice__c,
                SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c,
                SBQQ__Subscription__r.Promotion_Type__c,
                SBQQ__Subscription__r.SBQQ__QuoteLine__r.SBQQ__ListPrice__c,
                Total__c,
                OrderItemNumber,
                Product2.Name,
                Product2.ProductCode,
                Product2.Product_Group__c,
                Ad_Context_Allocation__c,
                OrderId,
                SBQQ__QuoteLine__r.SBQQ__RequiredBy__r.Product_Group__c,
                LBx_Contact__c,
                LBx_Contact__r.FirstName,
                LBx_Contact__r.LastName,
                LBx_Contact__r.Email,
                LBx_Contact__r.Language__c,
                Place__c,
                Place__r.PlaceID__c,
                SBQQ__QuoteLine__r.Base_term_Renewal_term__c,
                SBQQ__QuoteLine__r.Subscription_Term__c,
                SBQQ__QuoteLine__r.SBQQ__ListPrice__c,
                SBQQ__QuoteLine__r.SBQQ__TotalDiscountRate__c,
                Order.Status,
                Order.Type,
                Order.SBQQ__Quote__r.SBQQ__Type__c,
                Partial_Termination__c,
                NonPriceRelevant__c
                FROM    OrderItem
                WHERE   OrderId = :orderId
                ORDER BY SBQQ__RequiredBy__c];
    }
    
    //A.L. Marotta 17-10-19, Wave F, Sprint 11 - method to get parent order items by order id
    public static List<OrderItem> getParentOrderItemsByOrderId(Id orderId){
        return [SELECT  Id,
                SBQQ__Status__c,
                Quantity,
                PricebookEntry.Product2.Name,
                PricebookEntry.Product2.ProductCode,
                Product2Id,
                SBQQ__RequiredBy__c,
                UnitPrice,
                PricebookEntryId,
                SBQQ__Contract__c,
                SBQQ__QuoteLine__c,
                External_Id__c,
                SBQQ__Subscription__c,
                SBQQ__Subscription__r.External_Id__c,
                Total__c,
                SambaIsBilled__c, 
                SambaIsStartConfirmed__c,
                OrderId,
                OrderItemNumber,
                Product2.Name,
                Product2.ProductCode,
                Product2.Product_Group__c, 
                Product2.Family, 
                Order.Type,
                Order.Custom_Type__c,
                Order.Status,
                Merchant_ID__c,
                RequiredByProductCode__c
                FROM    OrderItem
                WHERE   OrderId = :orderId
                AND     SBQQ__RequiredBy__c = null];
    }
    
    
    public static List<OrderItem> getOrderItemsByOrderIds(Set<Id> orderIds, Set<String> queryFields){
        List<OrderItem> orderItems = new List<OrderItem>();
        List<String> queryFieldsList = new List<String>(queryFields);
        String query =' SELECT ' +String.join(queryFieldsList, ',') +' FROM OrderItem WHERE OrderId IN :orderIds';
        orderItems = Database.query(query);
        return orderItems;
    }

    public static List<OrderItem> getOrderItemsByIds(Set<Id> orderItemIds, Set<String> queryFields){
        List<OrderItem> orderItems = new List<OrderItem>();
        List<String> queryFieldsList = new List<String>(queryFields);
        String query =' SELECT ' +String.join(queryFieldsList, ',') +' FROM OrderItem WHERE Id IN :orderItemIds';
        orderItems = Database.query(query);
        return orderItems;
    }
    
    public static List<OrderItem> dynamicGetOrderItemFromIds(Set<Id> orderItemIds , List<String> fields) {
        String query = 'SELECT '+String.join(fields, ',') +' FROM OrderItem WHERE Id IN :orderItemIds ';
        return Database.query(query);
    }
    
    public static List<OrderItem> getOrderItemsByParent (Set<Id> parentIds , List<String> fields,Boolean isActivateLater) {
        String query = 'SELECT '+String.join(fields, ',') +' FROM OrderItem WHERE SBQQ__RequiredBy__c IN :parentIds ';
        if(isActivateLater){
            query += 'And  Activate_Later__c = true';
        }
        return Database.query(query);
    }
    
    public static List<OrderItem> getOrderItemsByOrderIds(Set<Id> orderIds){
        return [SELECT  Id, Ad_Context_Allocation__c, OrderId
                FROM    OrderItem
                WHERE   OrderId IN :orderIds];
    }
}