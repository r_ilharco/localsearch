/**
* Copyright (c), Deloitte Digital
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
*   are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice, 
*      this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
* - Neither the name of the Deloitte Digital nor the names of its contributors
*      may be used to endorse or promote products derived from this software without
*      specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
*  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
*  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
*  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
* Author      : Vincenzo Laudato <vlaudato@deloitte.it>
* Date        : 2020-06-12
* Sprint      : 
* Work item   : SFRO-533 - Workaround for Apex CPU Time Exceeded
* Testclass   : Test_OrderUtilityByIdBatch
* Package     : 
* Description : 
* Changelog   :
*/

global class OrderUtilityByIdBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful{
    
    global List<String> orderIds = new List<String>();
    
    global OrderUtilityByIdBatch(List<String> orderIds){
        this.orderIds = orderIds;
    }
    
    global void execute(SchedulableContext sc) {
        OrderUtilityByIdBatch b = new OrderUtilityByIdBatch(this.orderIds); 
        Database.executeBatch(b, 1); 
    }
    
	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Order WHERE Id IN :orderIds');
    }
    
    global void execute(Database.BatchableContext info, List<Order> scope){
        List<Id> orderIds = new List<Id>();
        for(Order o : scope){
            orderIds.add(o.Id);
        }
        if(!Test.isRunningTest()) OrderUtility.activateContracts(orderIds);
    }     
    global void finish(Database.BatchableContext info){     
    }
    
}