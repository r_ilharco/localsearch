/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Order Management - Terminate pending termination/cancellation orders - Manual step needed for
* SPIII-75
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vincenzo Laudato   <vlaudato@deloitte.it>
* @created        2020-07-09
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class FutureTerminationOrderBatch implements Database.batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful{
    
    global List<String> staticValues = new List<String>();
    
    global FutureTerminationOrderBatch(List<String> val){
        this.staticValues = val;
    }
    
    global void execute(SchedulableContext sc) {
        FutureTerminationOrderBatch b = new FutureTerminationOrderBatch(this.staticValues); 
        database.executebatch(b, 1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator('SELECT Id, MigratedTermination__c, Contract__c, Contract__r.Phase1Source__c FROM Order WHERE Cluster_Id__c IN :staticValues AND Id IN (' +
            							'SELECT OrderId ' +
                                        'FROM OrderItem ' +
                                        'WHERE ServiceDate > TODAY ' +
                                        'AND ( Order.Type = \'' + ConstantsUtil.ORDER_TYPE_CANCELLATION + '\' OR Order.Custom_Type__c = \'' + ConstantsUtil.ORDER_TYPE_TERMINATION + '\') ' +
                                        'AND Order.Status = \'' + ConstantsUtil.ORDER_STATUS_DRAFT + '\'' +
                                        ')'
                                       );
    }
    
    global void execute(Database.BatchableContext info, List<Order> scope){
        
        Set<Id> orderIds = new Set<Id>();
        Set<Id> orderFullfilledIds = new Set<Id>();
		Set<Id> contractIds = new Set<Id>();
        for(Order currentOrder : scope){
            if(!currentOrder.MigratedTermination__c || currentOrder.Contract__r.Phase1Source__c)
            	orderIds.add(currentOrder.Id);
            else{
                orderFullfilledIds.add(currentOrder.Id);
                contractIds.add(currentOrder.Contract__c);
                currentOrder.status = ConstantsUtil.ORDER_STATUS_ACTIVATED;
            }
        }
        if(!orderIds.isEmpty()){
            if(!Test.isRunningTest()) OrderUtility.activateContracts(new List<Id>(orderIds));
        }
        else if(!orderFullfilledIds.isEmpty() && !contractIds.isEmpty()){
			terminateSubscription(orderFullfilledIds,contractIds);
            List<Database.SaveResult> resultsOrder = Database.update(scope, false);
			for (Database.SaveResult sr : resultsOrder) {
                if (!sr.isSuccess()) {              
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Order fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    } 
    
    global void finish(Database.BatchableContext info){}
    
    public static void terminateSubscription (Set<Id> orderIds,Set<Id>contractIds){
        Map<Id,SBQQ__Subscription__c> terminatedSubscription = SEL_Subscription.getSubscriptionsByContractsInTerminationSetAs(contractIds, true);
        for(SBQQ__Subscription__c s :terminatedSubscription.values()){
            s.In_termination__c = false;
            s.Subsctiption_Status__c  = ConstantsUtil.SUBSCRIPTION_STATUS_TERMINATED;
        }
        update terminatedSubscription.values();
       	List<Contract> terminatedContract = [select id from contract where id in: contractIds and id not in
                                             (select sbqq__contract__c from SBqq__Subscription__c 
                                              where Subsctiption_Status__c =: ConstantsUtil.SUBSCRIPTION_STATUS_ACTIVE
                                              OR Subsctiption_Status__c =: ConstantsUtil.SUBSCRIPTION_STATUS_PRODUCTION )];
        for(Contract c : terminatedContract){
            c.In_Termination__c= false;
         	c.Status = ConstantsUtil.CONTRACT_STATUS_TERMINATED;   
        }
        update terminatedContract; 
    }
    
}