({ 
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        switch (action.name) {
            case 'TerminateSubscription':
                helper.terminateAction(cmp, row, action);
                break;
        }
    },
    init: function (cmp, event, helper) {
        var profileName = '';
        var getProfileNameAction = cmp.get('c.getProfileName');
        getProfileNameAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State: '+state);
            if (state === "SUCCESS") {
                profileName = response.getReturnValue();
                console.log('Profile Name: '+profileName);
            }
            else {
                console.log("Failed with state: " + state);
                return null;
            }
            var columns;
            console.log('profileName: ' + profileName);
            if(profileName == 'System Administrator'){
                columns = [
                    { type: 'url' ,initialWidth: 200, fieldName: 'id', label: 'Subscription #', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } },
                    { type: 'url' ,initialWidth: 200, fieldName: 'productId', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' } , target: '_parent' } },
                    { type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
                    { type: 'text', initialWidth: 200,fieldName: 'billingStatus', label: 'Billing Status' },
                    { type: 'url' ,initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
                    { type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
                    { type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
                    { type: 'number' ,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
                    { type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
                    { type: 'percent',initialWidth: 200, fieldName: 'discount',label: 'Discount'},
                    { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } }//,
                    // { type: "button", label: 'Action', initialWidth:120,  fixedWidth: 120 ,typeAttributes: { label: 'Terminate', name: 'TerminateSubscription', disabled: { fieldName: 'showOptions' }}},
                ];
                console.log('columns for Admin');
            }else if(profileName == 'Finance'){
                 columns = [
                    { type: 'url' ,initialWidth: 200, fieldName: 'id', label: 'Subscription #', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } },
                    //{ type: 'url' ,initialWidth: 200, fieldName: 'productId', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' } , target: '_parent' } },
                    { type: 'text', initialWidth: 200,fieldName: 'productName', label: 'Product Name' },
                    { type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
                    { type: 'text', initialWidth: 200,fieldName: 'billingStatus', label: 'Billing Status' },
                    { type: 'url' ,initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
                    { type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
                    { type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
                    { type: 'number' ,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
                    { type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
                    { type: 'percent',initialWidth: 200, fieldName: 'discount',label: 'Discount'},
                     { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } }//,
                    // { type: "button", label: 'Action', initialWidth:120,  fixedWidth: 120 ,typeAttributes: { label: 'Terminate', name: 'TerminateSubscription', disabled: { fieldName: 'showOptions' }}},
                ];
                console.log('columns for Finance');
            }else{
                columns = [
                    { type: 'url' ,initialWidth: 200, fieldName: 'id', label: 'Subscription #', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } },
                    //{ type: 'url' ,initialWidth: 200, fieldName: 'productId', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' } , target: '_parent' } },
                    { type: 'text', initialWidth: 200,fieldName: 'productName', label: 'Product Name' },
                    { type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
                    { type: 'url' ,initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
                    { type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
                    { type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
                    { type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
                    { type: 'number' ,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
                    { type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
                    { type: 'percent',initialWidth: 200, fieldName: 'discount',label: 'Discount'},
                     { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } }//,
                    // { type: "button", label: 'Action', initialWidth:120,  fixedWidth: 120 ,typeAttributes: { label: 'Terminate', name: 'TerminateSubscription', disabled: { fieldName: 'showOptions' }}},
                ];
                console.log('columns for other profiles');
            } 
            console.log('columns: ' + columns);
            cmp.set('v.gridColumns', columns);
            helper.getSubscription(cmp, event);
        });    
        $A.enqueueAction(getProfileNameAction); 
    }  
})
