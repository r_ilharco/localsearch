({
	getSubscription: function(component, event) {
        var terminateContract = component.get('v.terminateContract');
        var recordId = component.get('v.recordId');
        var action = component.get("c.getSubscriptionByContract");
        var params = { contractId : recordId };
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                data.subscriptions.forEach(function(element) {
                  element._children = element.childrens;
                    
                  element.childrens.forEach(function(childrenElement) {
                      childrenElement.id = '/' + childrenElement.id;
                      childrenElement.productId = '/' + childrenElement.productId;                                        
                  });
                  
                  element.id = '/' + element.id;
                  element.productId = '/' + element.productId;
                  if(element.placeId){
                  	element.placeId = '/' + element.placeId;
                  }
                });
                
                
                console.log('subscriptions -> ' + JSON.stringify(data.subscriptions));
				component.set('v.gridData', data.subscriptions);
                
                var expandedRows = [];
				component.set('v.gridExpandedRows', expandedRows);
              //  component.set('v.loadingData', false);
            } else {
                this.showNotifError(component, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    }
    ,
    terminateAction: function (cmp, row) {
        var that = this;
        var modalBody;
        var recId= row.id.replace('/', '');
        $A.createComponent("c:CancellationTerminationContract", {recordId:recId},
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   cmp.find('overlayLib').showCustomModal({
                       body: modalBody, 
                       showCloseButton: false,
                       closeCallback: function() {
                       }
                   })
               }                               
           });
    }
})