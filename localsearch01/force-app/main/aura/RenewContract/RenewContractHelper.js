({
	renewContract : function(recordId, component) {
        var action = component.get('c.renewContract');
        action.setParams({
            'recordId': recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get('e.force:closeQuickAction').fire();  
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                if(result){
                    if(result.success){
                        if(result.renewalOpportunityUrl){
                            this.showToastHyperlink($A.get('$Label.c.Success'), 'success', $A.get('$Label.c.RENEW_OpportunityCreated'), result.renewalOpportunityUrl);
                        }
                    }
                    else{
                        if(result.errorMessage){
                            if(result.errorMessage.includes('{0}')){
                                this.showToastHyperlink($A.get('$Label.c.Error'), 'error', result.errorMessage, result.renewalOpportunityUrl);
                            }
                            else{
                                this.showToast($A.get('$Label.c.Error'), 'error', result.errorMessage);
                            }
                        }
                        else{
                            this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                        }
                    }
                }
                else{
                    this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                }
            }
            else{
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            }
        });
        $A.enqueueAction(action);
	},
    
    showToast : function(title, type, message){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: 'Sticky'
        });
        toastEvent.fire();
    },
    
    showToastHyperlink : function(title, type, message, url) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            type : type,
            mode: 'Sticky',
            message: message,
            messageTemplate: message,
            messageTemplateData: [{
                label: $A.get('$Label.c.RENEW_RenewalOpp'),
                url: url
            }]
        });
        toastEvent.fire();
    }
})