/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 10-29-2019
 * Sprint      : Sprint 12
 * Work item   : SF2-462 - Change Owner Quick Action
 * Package     : 
 * Description : 
 * Changelog   : 
 */

({
    doInitHandler: function(component, event, helper){
        var recordId = component.get('v.recordId');
        console.log('recordId: ' + recordId);
        console.log('account list: ' + component.get('v.accountIds'));
        if(recordId){
            helper.doInit(component, event);
        }else{
            helper.doInitMassive(component, event);
        }
    },    
    onClickSaveHandler: function(component, event, helper) {
        var isMassive = component.get('v.isMassive');
        if(isMassive == false){
            helper.onClickSave(component, event);
        }else{
            helper.onClickSaveMassive(component, event);
        }
    },
    onClickAssignToMeHandler: function(component,event,helper){
        var ApprovalNeeded = component.get("v.approvalNeeded");
        console.log("ApprovalNeeded : "+ ApprovalNeeded);
        var myId = $A.get("$SObjectType.CurrentUser.Id");
        var Jsontest = {
            "Id": myId,
            "Name" : " "
        };
        var stringTest = JSON.stringify(Jsontest);
        console.log("stringTest: "+ stringTest);
        component.set("v.selectedLookUpRecord",Jsontest);
        helper.onClickSave(component, event);
    },
    handleError: function(component, event, helper){
        var mess;
        console.log('error');
        console.log('event: ' + event);
        var error = event.getParam("error");
        var message = event.getParam("message");
        var detail = event.getParam("detail");
        var output = event.getParam("output");
        console.log('error ' + JSON.stringify(event.getParam("error")));
        console.log('message ' + JSON.stringify(event.getParam("message")));
        console.log('detail ' + JSON.stringify(event.getParam("detail")));
        console.log('output ' + JSON.stringify(event.getParam("output")));
        /*if(detail != ''){
            console.log('detail not null');
            mess = detail;
        }else if(output != ''){
            console.log('output not null');
            if(output.fieldErrors != null){
                console.log('fielderrors not null');
                if(output.fieldErrors.LegalEntity__c != null){
                    console.log('legal entity not null');
                    console.log('output.fieldErrors.LegalEntity__c.message: ' +output.fieldErrors.LegalEntity__c[0].message);
                    mess = output.fieldErrors.LegalEntity__c[0].message;
                }
            }
        }else{
            mess = message;
        }*/
        console.log('mess: ' + mess );
    },    
	closeModal : function(component, event, helper) {
        var isMassive = component.get('v.isMassive');
        if(isMassive){
            var myEvent = $A.get("e.c:AccountChangeOwnerVFPEvent");
            myEvent.setParams({
                closePage: true
            });
            myEvent.fire();
        }
        else{
            $A.get("e.force:closeQuickAction").fire();
        }
    }
 })