({
	doInit: function(component, event){
        console.log("doInit");
        var userEmail = $A.get("$SObjectType.CurrentUser.Email");
        component.find("approvalRequester").set('v.value',userEmail);
        var recordId = component.get('v.recordId');
        var params = {accountId:recordId};
        var action = component.get("c.hasUserGrant");
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('res["approved"]: ' + res["approved"]);
                console.log('SUCCESS');
                //console.log('>>> ' + JSON.stringify(res));
                if(res["territoryAllowed"] == false){
                    component.set('v.approvalNeeded', true);  
                	/*console.log('allowed = false');
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": "warning",
                        "message": $A.get("$Label.c.ChangeOwner_quickaction_territoryError_msg")
                    });
                    toastEvent.fire();*/
                }
                if(res["errorDMC"]==true){
                    console.log('errorDMC = true');
                    msg=$A.get("$Label.c.ChangeOwner_errorDMC");
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": "error",
                        "message": msg
                    });
                    toastEvent.fire();
                }else if(res["allowed"] == false){
                    var msg;
                    if(res["alreadyInApproval"] == true){
                        msg = $A.get("$Label.c.ChangeOwner_quickaction_alreadyApproval_msg");
                    }else{
                        msg = $A.get("$Label.c.changeOwner_quickaction_InsufficientPermissions_msg");
                    }
                    console.log('allowed = false');
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": "warning",
                        "message": msg
                    });
                    toastEvent.fire();
                }           
                if(res["approved"] == true){
                    console.log('approved = true');
                    component.find("approved").set('v.value','approved');
                }
            }
        });
        $A.enqueueAction(action);
        console.log('SUCCESS-end');
    },
    
    doInitMassive: function(component, event){
        console.log("doInitMassive");
        var accountIds = component.get('v.accountIds');
        console.log('accountIds:::::' + accountIds);
        var accountIdsList = accountIds.toString().replace(/\s+/g,'');//.split(';');
        //accountIdsList = accountIds;
        console.log('accountIdsList -> ' + JSON.stringify(accountIdsList));
        var params = {accountIds:accountIdsList};
        var action = component.get("c.hasUserGrantMassive");
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res["territoryAllowed"] == false){
                    console.log('territoryAllowed: false');
                    component.set('v.errorMessageForMassive', true);  
                    var mess = $A.get("$Label.c.ChangeOwner_quickaction_territoryError_msg");
                    component.set('v.errorMessage', mess);
                    console.log('territory error: ' + component.get('v.errorMessage'));
                    component.set('v.exitForMassive', true);
                }else if(res["allowed"] == false){
                    component.set('v.errorMessageForMassive', true);
                    component.set('v.exitForMassive', true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    onClickSave: function(component, event) {
        var userObj = JSON.stringify(component.get("v.selectedLookUpRecord"));
        console.log('userObj:::::' + userObj);
        var recordId = component.get("v.recordId");
        console.log('recordId:::::' + recordId);
        var approvalNeeded = component.get('v.approvalNeeded');  
        var action = component.get("c.updateOwner");
        action.setParams({
            "recordId" : recordId,
            "newOwner": userObj,
            "approvalNeeded" : approvalNeeded
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res != null){
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "error",
                        "message": res
                    });
                    toastEvent.fire();
                }else{
                    var mess;
                    console.log('approved: ' + component.find("approved").get('v.value'));
                    if(approvalNeeded){
                        mess = $A.get("$Label.c.changeOwner_quickaction_submitApprovalRequest_msg");
                    }else{
                        mess = $A.get("$Label.c.changeOwner_quickaction_submitApprovalRequest_msg_ADMIN");
                    }
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Success"),
                        "type": "success",
                        "message": mess
                    });
                    toastEvent.fire();
                }
            }else{
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Warning"),
                    "type": "warning",
                    "message": $A.get("$Label.c.Error")
                });
                toastEvent.fire();
            } 
        });
        $A.enqueueAction(action);
    },
    onClickSaveMassive: function(component, event) {
        component.set('v.isSpinnerActive', true);  
        var userObj = JSON.stringify(component.get("v.selectedLookUpRecord"));
        console.log('userObj:::::' + userObj);
        var recordIds = component.get('v.accountIds');
        console.log('recordIds:::::' + recordIds);
        var params = {accountIds:accountIds};
        var action = component.get("c.updateOwnerMassive");
        action.setParams({
            "recordIds" : recordIds,
            "newOwner": userObj
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('onClickSaveMassive response state: ' + state);
            if(state === "SUCCESS"){
                component.set('v.successMessageForMassive', true);  
                component.set('v.isSpinnerActive', false);  
                component.set('v.exitForMassive', true); 
            } 
        });
        $A.enqueueAction(action);
    }
})
