/*
CHANGELOG:
            #1 - SF2-253 - Validate Account and Billing Address on Quote Creation
                 SF2-264 - Legal Address Validation
                 gcasola 07-22-2019
			#2 - SF2-218, Wave C, Sprint 6, Logging - Error Handling - 07-24-2019 gcasola
*/
({
	doInit : function(component, event, helper) {
        component.find("forceBpRecord").getNewRecord(
            "Billing_Profile__c",
            null,
            false,
            $A.getCallback(function() {
                component.set('v.bpRec.Billing_Language__c', '');
                component.set('v.bpRec.Channels__c', 'Print;Mail');
                component.set('v.bpRec.Is_Default__c', true);
                var recId = component.get('v.recordId');
                console.log('[BillinProfileController.js - doInit] recordId: ' + recId);
                if(recId) {
                    var action = component.get('c.getDefaultBPValues');
                    action.setParams({relatedId:recId});
                    action.setCallback(this, function(response) {
                        var map = response.getReturnValue();
                        console.log('id '+map.id);
                        console.log('[BillinProfileController.js - doInit] map: ' + JSON.stringify(map) );
                        console.log("map['Id']: " + map['Id']);
                        console.log("component.get('v.disabled'): " + component.get('v.disabled'));
                        
                        console.log("Before component.get('v.disabled'): " + component.get('v.disabled'));
                        component.set("v.disabled",false);
                        console.log("After component.get('v.disabled'): " + component.get('v.disabled'));
                        
                        component.set('v.bpRec.Customer__c', map['Id']);
                        component.set('v.bpRec.Billing_Name__c', map['AccountName']);
                        component.set('v.bpRec. Recipient_Line_2__c', map['Recipient_Line_2__c']);
                        component.set('v.bpRec.Name', map['Name']);
                        component.set('v.bpRec.Is_Default__c', map['Is_Default__c']);	
                        component.set('v.bpRec.Customer_Reference_1__c', map['Customer_Reference_1__c']);
                        component.set('v.bpRec.Customer_Reference_2__c', map['Customer_Reference_2__c']);
                        component.set('v.bpRec.Customer_Reference_3__c', map['Customer_Reference_3__c']);
                        // START GK5
                        console.log('map[\'IdBPDefault\']: ' + map['IdBPDefault']);
                        component.set('v.IdBPDefault', map['IdBPDefault']);
                        component.set('v.Is_Default__c', map['Is_Default__c']);
                        // END GK5
                        component.set('v.bpRec.Billing_Language__c', map['Billing_Language__c']);
                        if(map['Billing_Contact__c'] != null) {
                            component.set('v.bpRec.Billing_Contact__c', map['Billing_Contact__c']);
                            //helper.setValue(component, map, 'Billing_Name__c');
                            helper.setValue(component, map, 'Mail_address__c');
                            helper.setValue(component, map, 'Customer_Reference_1');
                            helper.setValue(component, map, 'Customer_Reference_2');
                            helper.setValue(component, map, 'Customer_Reference_3');
                        }
                        helper.setValue(component, map, 'Billing_Street__c');
                        helper.setValue(component, map, 'Billing_Postal_Code__c');
                        helper.setValue(component, map, 'Billing_Country__c');
                        helper.setValue(component, map, 'Billing_City__c');
                        helper.setValue(component, map, 'P_O_Box__c');
                        helper.setValue(component, map, 'P_O_Box_City__c');
                        helper.setValue(component, map, 'P_O_Box_Zip_Postal_Code__c');
                        
                        if(component.get("v.sObjectName")=="Billing_Profile__c" && component.get("v.recordId")!=null)
                        {
                            component.set("v.bpRec.Id", component.get("v.recordId"));
                        }
                        console.log("component.get(\"v.bpRec.Id\")" + component.get("v.bpRec.Id"));
                    });
                    $A.enqueueAction(action);
                }
            })
        );
        component.set("v.bRec.Id", component.get("v.recordId"));
        
        //Vincenzo Laudato 2019-05-29 US #92 Sprint 2, Wave 1 *** START
        helper.checkIntegrationStatus(component);
             
	},
    cancelDialog : function(component, helper) {
        var closeAction = component.get('v.closeAction');
        if(closeAction) {
        	$A.enqueueAction(closeAction);
        } else {
        	$A.get("e.force:closeQuickAction").fire();
        }
    },
    accountChanged : function(component, event, helper) {
    	var newVal = event.getParam('value');
        if(newVal[0] == null) return;
		var action = component.get('c.getAccountBPValues');
        action.setParams({accountId:newVal[0]});
        action.setCallback(this, function(response) {
            var map = response.getReturnValue();
            component.set('v.bpRec.Name', map['Name']);
            component.set('v.bpRec.Is_Default__c', map['Is_Default__c']);
            component.set('v.bpRec.Billing_Language__c', map['Billing_Language__c']);
            if(map['Billing_Contact__c'] != null) {
                component.set('v.bpRec.Billing_Contact__c', map['Billing_Contact__c']);
                helper.setValue(component, map, 'Billing_Name__c');
                helper.setValue(component, map, 'Recipient_Line_2__c');
                helper.setValue(component, map, 'Mail_address__c');
				helper.setvalue(component, map, 'Customer_Reference_1');
                helper.setvalue(component, map, 'Customer_Reference_2');
                helper.setvalue(component, map, 'Customer_Reference_3');                
            }
			helper.setValue(component, map, 'Billing_Street__c');
			helper.setValue(component, map, 'Billing_Postal_Code__c');
			helper.setValue(component, map, 'Billing_Country__c');
			helper.setValue(component, map, 'Billing_City__c');
			helper.setValue(component, map, 'P_O_Box__c');
			helper.setValue(component, map, 'P_O_Box_City__c');
			helper.setValue(component, map, 'P_O_Box_Zip_Postal_Code__c');
        });
        $A.enqueueAction(action);
	},
    contactChanged : function(component, event, helper) {
    	var newVal = event.getParam('value');
        if(newVal[0] == null) return;
        var accountId = component.get('v.bpRec.Customer__c');
        var action = component.get('c.getContactBPValues');
        var params = {accountId:accountId[0], contactId:newVal[0]};
        action.setParams(params);
        action.setCallback(this, function(response) {
            var map = response.getReturnValue();
            if(map != null) {
                helper.setValue(component, map, 'Billing_Name__c');
                helper.setValue(component, map, 'Recipient_Line_2__c');
                helper.setValue(component, map, 'Mail_address__c');
                helper.setValue(component, map, 'Phone__c');
                helper.setvalue(component, map, 'Customer_Reference_1');
				helper.setvalue(component, map, 'Customer_Reference_2');
	            helper.setvalue(component, map, 'Customer_Reference_3');
            }
        });
        $A.enqueueAction(action); 
    },

    // START GK5
    is_DefaultChanged: function(component, event, helper)
    {
        component.set("v.bpRec.Is_Default__c",event.getParam("checked"));
        console.log('component.get(\'v.bpRec.Is_Default__c\') ' + component.get('v.bpRec.Is_Default__c') );
        console.log('component.get(\'v.Is_Default__c\')' + component.get('v.Is_Default__c'));
        console.log('event' + JSON.stringify(event));
        console.log('event[\'ro\'][\'checked\'] ' + event.getParam("checked"));
        var recordPage =  'https://' + window.location.hostname + '/' + component.get('v.IdBPDefault');
        console.log('Site: ' + recordPage);
        
        if(component.get('v.Is_Default__c')!=event.getParam("checked") && component.get('v.Is_Default__c')==true)
        {
            console.log("Be careful, you don\'t have any Billing Profile as Default.");
            var content = document.getElementById('notify__content');
            var container = document.getElementById('container');

            content.innerHTML = "";
            var textnode = document.createTextNode("Be careful, you don't have any Billing Profile as Default.");
            content.appendChild(textnode);
            container.classList.remove("slds-hide");
            //setTimeout(function(){}, 5000);
            //container.classList.add("slds-hide");


            /*var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({"title": "Warning",
                        "message": "Be careful, you don't have any Billing Profile as Default.",
                        "type": "warning",
                        "mode" : "sticky"
                    });
            toastEvent.fire();*/
            //component.set('v.contents','Be careful, you don\'t have any Billing Profile as Default.');
            //component.set('v.visible', true);

        }else if (component.get('v.Is_Default__c')!=event.getParam("checked") && component.get('v.Is_Default__c')==false)
        {
            console.log("Be careful, you have already a Billing Profile as Default.");
            /*var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({"title": "Warning",
                                "message": "Be careful, you have already a Billing Profile as Default.",
                                "type": "warning",
                                "messageTemplate": "Be careful, you have already a Billing Profile as Default. Go to its {0}.",
                                "messageTemplateData": [
                                    {
                                        url: recordPage,
                                        label: 'page',
                                    }
                                ],
                                "mode" : "sticky"
                            
                            });
            toastEvent.fire();*/
            var content = document.getElementById('notify__content');
            var container = document.getElementById('container');
            

            content.innerHTML = "";
            var textnode = document.createTextNode('You already have a default billing profile.');
            var br = document.createElement("br");
            var textnode2 = document.createTextNode(" Click Submit if you want to set this one as the new default.");
            /*var linktext = document.createTextNode("billing profile");
            var anchor = document.createElement("a");
            anchor.appendChild(linktext);
            anchor.title = "billing profile";
            anchor.href = recordPage;
            anchor.target = "_blank";
            anchor.rel = "noreferrer noopener"
            */
            content.appendChild(textnode);
            //content.appendChild(anchor);
            //content.appendChild(document.createTextNode("."));
            content.appendChild(br);
            content.appendChild(textnode2);

            container.classList.remove("slds-hide");
            //setTimeout(function(){}, 5000);
            //container.classList.add("slds-hide");

            
            //component.set('v.contents','Be careful, you have already a Billing Profile as Default. Go to its <a href="' + recordPage + '">page</a>.');
            //component.set('v.visible', true);

        }else
        {
            var container = document.getElementById('container');
            container.classList.add("slds-hide");
            //component.set('v.visible', false);
        }
    },
    //END GK5

	handleSuccess : function(component, event, helper) {
        event.preventDefault();
        var action = component.get('c.checkDefault');
        var resp = event.getParams().response;
        action.setParams({bpId:resp.id, relatedId:component.get('v.recordId')});
        //console.log('[BillingProfileController.js.handleSuccess]Billing Profile Id: ' + resp.id);
        //console.log('[BillingProfileController.js.handleSuccess]Quote Id: ' + component.get('v.recordId'));
        action.setCallback(this, function(response) {
        	var closeAction = component.get('v.closeAction');
            if(closeAction) {
                $A.enqueueAction(closeAction);
            } else {
                $A.get("e.force:closeQuickAction").fire();
            }
            $A.get('e.force:refreshView').fire();
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({"title": "Record Saved","message": "The Billing profile has been created.","type": "success"});
            toastEvent.fire();
            return true;
        });
		$A.enqueueAction(action);
        return false;
        /*
        var closeAction = component.get('v.closeAction');
            if(closeAction) {
                $A.enqueueAction(closeAction);
            } else {
                $A.get("e.force:closeQuickAction").fire();
            }
            $A.get('e.force:refreshView').fire();
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({"title": "Record Saved","message": "The Billing profile has been created.","type": "success"});
            toastEvent.fire();
            return true;*/
    },
    handleError : function(component, event, helper) {
        var resultsToast = $A.get("e.force:showToast");
        var resp = event.getParams();
        resultsToast.setParams({
            "title": "Error",
            "message": resp.errorCode + '\n' + resp.message,
            "type": "error"
        });
        resultsToast.fire();
        return false;
    },
  
    handleSubmit: function(component, event, helper) {
        event.preventDefault();
        var fields = event.getParam('fields');
        console.log("[BillingProfileController.js - handleSubmit] fields: " + JSON.stringify(fields));
        console.log("[BillingProfileController.js - handleSubmit] v.bpRec: " + JSON.stringify(component.get("v.bpRec")));
        var msg = '';

        if(!fields.Customer__c) {
            msg += 'Customer is missing\n';
        }
       /* if(!fields.Billing_Contact__c) {
            msg += 'Billing Contact is missing\n';
        }     */   
        if(!fields.Channels__c) {
            msg += 'No Channel provided\n';
        }
        if(fields.Channels__c.indexOf('Mail')>=0 && !fields.Mail_address__c) {
            msg += 'No mail address provided for channel \'Mail\'';
        }
        if(!fields.Billing_Street__c || !fields.Billing_City__c || !fields.Billing_Postal_Code__c || !fields.Billing_Country__c  ) {
            msg += $A.get("$Label.c.BP_AddressValidation")+'\n';
        }
        if(msg) {
            component.set('v.error', msg);
            return false;
        }
        
		//A. Marotta 27-05-19 Start
        //US #92, Wave 1, Sprint 2 - Address validation check
		var channels = fields.Channels__c;
        var isPrint = channels.includes($A.get("$Label.c.Billing_Channel_Print"));
        var isActive = component.get('v.isActive');
        
        if(isPrint && isActive){
            var action = component.get('c.checkBillingProfileLegalAddress');
            var companyId = fields.Customer__c;
            var street = fields.Billing_Street__c;
            var zip = fields.Billing_Postal_Code__c;
            var city = fields.Billing_City__c;
            var email = fields.Mail_address__c;
            var phone = fields.Phone__c;
            
            action.setParams({
                'recordId' : component.get('v.recordId'),
                'companyId' : companyId,
                'street' : street,
                'postalCode' : zip,
                'city' : city,
                'uid' : null,
                'email' : email,
                'phone' : phone,
                'editMode': false
            });
            action.setCallback(this, function(response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                if (state === "SUCCESS") {
                    var avResult = JSON.parse(response.getReturnValue());
                    console.log('avResult: ' + JSON.stringify(avResult));
                    var returnString = avResult.result;
                    if(returnString=="OK"){
                        helper.handleValidationResult(component,event,fields);
                    }else{
                        if(avResult && avResult.error && avResult.error.error_code){
                              component.set('v.error', avResult.error.error_code);
                        }else{
                              component.set('v.error', $A.get("$Label.c.Address_not_validated"));
                        }                 
                        return false;
                    }
                }else{
                    component.set('v.error','Internal error');
                    return false;
                }
    
            });
            helper.showSpinner(component, event, helper);
            $A.enqueueAction(action);
        }
        else{
            fields.AddressValidated__c = $A.get("$Label.c.Address_Validated_NotValidated");
            component.find('reForm').submit(fields); 
        }
        //A. Marotta 27-05-19 End
    },
    handleCloseClick: function (cmp, event, helper) {
        //cmp.set("v.visible", false);
        var container = document.getElementById('container');
        container.classList.add("slds-hide");
    }
})