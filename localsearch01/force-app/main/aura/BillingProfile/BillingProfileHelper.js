/*
CHANGELOG:
            #1 - SF2-253 - Validate Account and Billing Address on Quote Creation
                 SF2-264 - Legal Address Validation
                 gcasola 07-22-2019
                 */
({
	setValue : function(cmp, map, field) {
        if(map[field] != null) {
            cmp.set('v.bpRec.' + field, map[field]);
        }
    },
    //A. L. Marotta 28-05-19 US #92 #93, Wave 1, Sprint 2

    handleValidationResult : function(component, event, fields){
        //A. L. Marotta 27-05-19 US #92
        console.log('[BillingProfileController.js - handleSubmit] fields helper: ' + JSON.stringify(fields));
        fields.AddressValidated__c = $A.get("$Label.c.Address_Validated_Validated");
        component.find('reForm').submit(fields);    
        //A. Marotta 27-05-19 End
    },
    
    //Vincenzo Laudato 2019-05-29 US #92 Sprint 2, Wave 1 *** START
    checkIntegrationStatus : function(component){
        var action = component.get('c.checkIntegrationStatus');
        action.setCallback(this, function(response) {
       		var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
            		var isActive = response.getReturnValue();  
 					component.set('v.isActive', isActive);
                }
            }
        });
        $A.enqueueAction(action);
    },
        showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
    //END
})