({
    init: function (cmp) {
        var recordId = cmp.get('v.recordId');
        console.log('recordId '+recordId);
        var action = cmp.get("c.getQuoteLineItems");
        var params = { recordId : recordId};
        action.setParams(params);
        
        action.setCallback(this, function(response) {
            console.log(response);
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                //console.log(data);
                for(var i=0; i < data.listQuoteLines.length; i++) {
                    console.log(data.listQuoteLines[i].children);
                    data.listQuoteLines[i]._children = data.listQuoteLines[i].children;
                }
                cmp.set('v.gridData', data.listQuoteLines);
                //cmp.find('treeGrid').expandAll();
            } else {
                //this.showNotifError(cmp, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
        
        var columns = [
            {
                type: 'text',
                fieldName: 'productCode',
                label: 'PRODUCT CODE'
                //,initialWidth: 300
            },
            {
                type: 'text',
                fieldName: 'productName',
                label: 'PRODUCT NAME'
            },
            {
                type: 'text',
                fieldName: 'placeName',
                label: 'PLACE NAME'
            },
            {
                type: 'text',
                fieldName: 'trialStartDate',
                label: 'TRIAL START DATE'
            },
            //A.L.Marotta 01-11-19 Start - Wave F, Sprint 12 - trial end date
            /*{ 
                type: 'text',
                fieldName: 'trialEndDate',
                label: 'STATUS TRIAL END DATE'
            },*/
            { 
                type: 'text',
                fieldName: 'trialEndDate',
                label: 'TRIAL END DATE'
            },
            //A.L.Marotta 01-11-19 End
            {
                type: 'text',
                fieldName: 'status',
                label: 'STATUS'
            }
        ];

        cmp.set('v.gridColumns', columns);
    },
    handleStartTrialClick: function(cmp, event, helper) {
        
        var treeGridCmp = cmp.find('treeGrid');
        var selectedRows = treeGridCmp.getSelectedRows();
        var selectedQuoteLines = helper.validateTrial(cmp, event, selectedRows);
        if(selectedQuoteLines !== false && cmp.get('v.sentSuccessfully') == false && cmp.get('v.responseOK') == false) {
            var action = cmp.get("c.saveTriaActivation");
            var params = { strQLineIds : selectedQuoteLines.join(",")};
            action.setParams(params);
            
            action.setCallback(this, function(response) {	
                var responseOK = false;
                var disabled = false;
                console.log(response);
                var state = response.getState();
                cmp.set('v.sentSuccessfully',false);
                if (state === "SUCCESS") {
                    var data = response.getReturnValue();
                    if(data=="OK") {
                        responseOK = true;
               			disabled = true;
                        helper.showSuccess(cmp, $A.get("$Label.c.Trial_success"), 'Request sent successfully');
                        $A.get("e.force:closeQuickAction").fire();
                        $A.get('e.force:refreshView').fire();
                    } else {
                        helper.showError(cmp, "Failure to save", "Unable to process request.");
                        responseOK = false;
               			disabled = false;
                    }
                } else {
                    //this.showNotifError(cmp, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error");
                    }
                    responseOK = false;
               		disabled = false;
                }
                
                cmp.find("startTrialBtn").set("v.disabled", disabled);
                cmp.set('v.responseOK',responseOK);
            });
            
            $A.enqueueAction(action);
            cmp.find("startTrialBtn").set("v.disabled", true);
            cmp.set('v.sentSuccessfully', true);
        }
    },
    handleCancelClick: function(cmp, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
	}
});