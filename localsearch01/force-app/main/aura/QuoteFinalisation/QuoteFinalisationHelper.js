({
    /*A. L. Marotta 23-07-19 Start
     * SF2-231 Wave 3, Sprint 6 - Free Trial Management, fixed and added error checks*/
	validateTrial : function(cmp, event, selectedRows) {
        var selectedQuoteLines = [];
        for(var i=0; i<selectedRows.length; i++) {
            console.log('selectedRows[i].quoteLineId --> '+selectedRows[i].quoteLineId);
            if(!selectedRows[i].hasChildren) continue; // ignoring child items
            if(!selectedRows[i].placeName) { 
                // Place must be associated
                this.showError(cmp, "Items with place assigned are only allowed to start trial.", "No place associated with the selected item.");
                return false;
            }
            if(selectedRows[i].status != 'Draft') {
                /*A.L. Marotta 25-09-19 Start
                 * SF2-371 Wave E, Sprint 10 - Error message updated*/
                //this.showWarning(cmp, "Item status must be in 'Draft'", 'Only Draft status items can be requested for trial');
                this.showWarning(cmp, "This Quote is already in status 'Trial'", 'Only Draft status items can be requested for trial');
                //A.L. Marotta 25-09-19 End
				return false;
            }
            console.log('Trial start date: '+selectedRows[i].trialStartDate);
            if(selectedRows[i].trialStartDate != null){
            this.showWarning(cmp, "Already activated", 'Already activated');
                return false;
        	}
            //A. L. Marotta 23-07-19 End
            selectedQuoteLines.push(selectedRows[i].quoteLineId);
            if(selectedRows[i].children){
                selectedRows[i].children.forEach(element => selectedQuoteLines.push(element.quoteLineId));
            }            
        }
        if(selectedQuoteLines.length == 0) { // No items
            /*A. L. Marotta 26-07-19 Start
             * Wave 3, Sprint 6 - Fixed error message for missing check on master quote line*/
            
            //this.showWarning(cmp, "No lead items selected.", "Only lead items can be requested for trial activation.");
            //this.showWarning(cmp, $Label.c.No_bundle_selected, "No bundle selected."); //OAVERSANO 20191018 fix
            this.showWarning(cmp, $A.get("$Label.c.No_bundle_selected"), "No bundle selected."); //OAVERSANO 20191018 fix
            
            //A. L. Marotta 26-07-19 End
            return false;
        }
		return selectedQuoteLines;
	},
    showError : function(cmp, message, header) {
        cmp.find('notifLibBlock').showToast ({
            "variant": "error",
            "header": header,
            "message": message
        });
    },
    showWarning : function(cmp, message, header) {
        cmp.find('notifLibBlock').showToast ({
            "variant": "warning",
            "header": header,
            "message": message
        });
    },
    showSuccess : function(cmp, message, header) {
        cmp.find('notifLibBlock').showToast ({
            "variant": "success",
            "header": header,
            "message": message
        });
    }
})