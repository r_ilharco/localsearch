({
	doInit : function(component, event, helper) {
        
		var inputValue = component.get('v.inputValue');
        if(inputValue.includes($A.get("$Label.c.ENH_PlaceRelated_Label"))) component.set('v.inputValueToShow', inputValue.replace($A.get("$Label.c.ENH_PlaceRelated_Label"), ''));
        
	}
})