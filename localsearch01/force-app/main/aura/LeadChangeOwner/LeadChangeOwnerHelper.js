({
//A.L. Marotta 09-12-19 Wave E, Sprint 13 - update lead owner on "Save" button click
updateLeadOwner : function(component, event, helper){
    let recordId = component.get('v.recordId')
		var action = component.get("c.updateLeadOwner");
		action.setParams({'leadId' : recordId});
		console.log('recordId = '+recordId);
        action.setCallback(this, function(response) {
			var state = response.getState();
			console.log('updateLeadOwner, callback state: '+state);
			if(state == "SUCCESS"){
                var result = response.getReturnValue();
				console.log('res is '+result);
				var toastEvent = $A.get("e.force:showToast");
				console.log('result of update is '+result);
				if(result["allowed"]){
					toastEvent.setParams({
						"title": $A.get("$Label.c.Success"),
						"type": "success",
						"message": $A.get("$Label.c.ChangeLeadOwner_Success")
					});
				}else if(!result["allowed"]){
					toastEvent.setParams({
						"title": $A.get("$Label.c.Error"),
						"type": "error",
                        "message": $A.get("$Label.c.ChangeLeadOwner_Failure") + ', reason: '+result["errorMessage"]
					});
				}
				toastEvent.fire();
				$A.get("e.force:closeQuickAction").fire();
				$A.get("e.force:refreshView").fire();
			}
		});
		$A.enqueueAction(action);
		
	}
})