({
	//A.L. Marotta 09-12-19 Wave E, Sprint 13 - check if User has the right permissions to change Lead owner
    doInit: function(component, event, helper)
    {
        var action = component.get("c.hasUserGrant_Lead");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                //console.log('>>> ' + JSON.stringify(res));
                if(res["allowed"] == false)
                {
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": "warning",
                        "message": $A.get("$Label.c.changeOwner_quickaction_InsufficientPermissions_msg")
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
	},
	
	
    
	closeModal : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	},
    
    handleSuccess: function(component, event, helper) {
		//A.L. Marotta 09-12-19 Wave E, Sprint 13 - change Lead owner on 'Save' button click
		helper.updateLeadOwner(component, event, helper);
	},
    
    handleError: function(component, event, helper)
    {
        //console.log('event: ' + JSON.stringify(event));
        $A.get("e.force:closeQuickAction").fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.c.Error"),
            "type": "error",
            "message": event.getParam("detail")
        });
        toastEvent.fire();
    }
})