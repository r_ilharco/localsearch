({
    /*doInit: function(component, event, helper) {
       var recordId = component.get("v.recordId");
        console.log('doInit - recordId:::::' + recordId);
        var action = component.get("c.getAccount");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            component.set("v.accounts", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    searchKeyChange: function(component, event, helper) {
        var searchKey = component.find("searchKey").get("v.value");
        console.log('searchKey:::::'+searchKey);
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {
            component.set("v.accounts", a.getReturnValue());
        });
        $A.enqueueAction(action);
    }, */
        
    onClickSave: function(component, event, helper) {
        var accountObj = JSON.stringify(component.get("v.selectedLookUpRecord"));
        console.log('accountObj:::::' + accountObj);
        var recordId = component.get("v.recordId");
        console.log('recordId:::::' + recordId);
        var action = component.get("c.updateAccount");
        action.setParams({
            "recordId" : recordId,
            "acc": accountObj
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Success"),
                    "type": "success",
                    "message": "SUCCESS"
                });
                toastEvent.fire();
            }else{
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Warning"),
                    "type": "warning",
                    "message": $A.get("$Label.c.")
                });
                toastEvent.fire();
            } 
        });
        $A.enqueueAction(action);
    },
    
    closeModal : function(component, event, helper) {
        console.log('closeModal');
		$A.get("e.force:closeQuickAction").fire();
	}
})