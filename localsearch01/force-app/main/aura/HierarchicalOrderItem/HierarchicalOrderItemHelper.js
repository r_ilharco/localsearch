({
	helperMethod : function(component) {
        var profileName = '';
        var getProfileNameAction = component.get('c.getProfileName');
        getProfileNameAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State: '+state);
            if (state === "SUCCESS") {
                profileName = response.getReturnValue();
                console.log('Profile Name: '+profileName);
                return profileName;
            }
            else {
                console.log("Failed with state: " + state);
                return null;
            }
            
        });
        $A.enqueueAction(getProfileNameAction); 
    }
})