// Sprint 7 - SF2 274 - Product Option Name translation 

({ // eslint-disable-line
    init: function (component, event, helper) {
        var action = component.get('c.getItemById');
        action.setParams({ id : component.get('v.recordId')});
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var data = response.getReturnValue();
         		
               // console.log('VAr DATA : ' + data.orderitem.length );
                component.set('v.ProductsCounter', data.orderitem.length);
                data.orderitem.forEach(function(element) {
                  element._children = element.childrens;
                    
                  element.childrens.forEach(function(childrenElement) {
              		 childrenElement.id = '/' + childrenElement.id;
                      childrenElement.product2Id = '/' + childrenElement.product2Id;
                      //vlaudato
                      //childrenElement.orderItemNumber = '/' + childrenElement.id;
                  });
                  
                  element.id = '/' + element.id;
                  element.product2Id = '/' + element.product2Id;
                  //vlaudato
                  //element.orderItemNumber = '/' + element.id;
                });
                 
                console.log('data.orderitem json : ' + JSON.stringify(data.orderitem));
                component.set('v.gridData',  data.orderitem);
                var expandedRows = [];
                component.set('v.gridExpandedRows', expandedRows);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        var profileName = '';
        var getProfileNameAction = component.get('c.getProfileName');
        getProfileNameAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State: '+state);
            if (state === "SUCCESS") {
                profileName = response.getReturnValue();
                console.log('Profile Name: '+profileName);
            }
            else {
                console.log("Failed with state: " + state);
                return null;
            }
            var columns;
            console.log('profileName: ' + profileName);
            if(profileName == 'System Administrator'){
                columns = [
                    { type: 'url', fieldName: 'id', label: 'Order Item', typeAttributes: { label: { fieldName: 'orderItemNumber'}, target: '_parent'}},
                    { type: 'url' , fieldName: 'product2Id', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' }, target: '_parent' } },
                    //{ type: 'text', fieldName: 'productName', label: 'Product Name' },
                    { type: 'text', fieldName: 'status', label: 'Status' },
                    { type: 'number' , fieldName: 'quantity', label: 'Quantity' } ,
                    { type: 'number' , fieldName: 'baseTerm', label: 'Base Term' } ,
                    { type: 'number' , fieldName: 'subTerm', label: 'Subscription Term' } ,
                    { type: 'currency' , fieldName: 'baseTermPrice', label: 'Base Term Price' } ,
                    { type: 'percent' , fieldName: 'discount', label: 'Discount' } ,
                    { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } }}
                ];
                console.log('columns for Admin');
            }else{
                columns = [
                    { type: 'url', fieldName: 'id', label: 'Order Item', typeAttributes: { label: { fieldName: 'orderItemNumber'}, target: '_parent'}},
                    //{ type: 'url' , fieldName: 'product2Id', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' }, target: '_parent' } },
                    { type: 'text', fieldName: 'productName', label: 'Product Name' },
                    { type: 'text', fieldName: 'status', label: 'Status' },
                    { type: 'number' , fieldName: 'quantity', label: 'Quantity' } ,
                    { type: 'number' , fieldName: 'baseTerm', label: 'Base Term' } ,
                    { type: 'number' , fieldName: 'subTerm', label: 'Subscription Term' } ,
                    { type: 'currency' , fieldName: 'baseTermPrice', label: 'Base Term Price' } ,
                    { type: 'percent' , fieldName: 'discount', label: 'Discount' } ,
                    { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } }}
                ];
                console.log('columns for other profiles');
            } 
            console.log('columns: ' + columns);
            component.set('v.gridColumns', columns);
            
        });
        $A.enqueueAction(action);
        $A.enqueueAction(getProfileNameAction); 
    }
});