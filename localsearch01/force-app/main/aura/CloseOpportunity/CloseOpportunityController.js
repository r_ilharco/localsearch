({
    init: function(component, event, helper) {
        helper.checkOwner(component, event, helper);
        helper.checkOppStage(component, event, helper);
        helper.getStage(component, event, helper);
    },
 
    onStageChange: function(component, event, helper) {
        //get the value of select option
        var isLost = component.get("v.isLost");
        var options = component.get("v.optionsStage");
        component.set("v.valStage", event.getParam("value"));
        component.set("v.stageLabel", options.find(opt =>  opt.value ===  event.getParam("value")).label);
        if(component.get("v.valStage")){
            component.set("v.buttonDisabled", false);
        }

        if(event.getParam("value") == 'Closed Lost'){
        	component.set("v.isLost", true);
            helper.getCloseReason(component, event, helper);
        } else {
            component.set("v.isLost", false);
            component.set("v.reasonVal", '')
        }
        
    },
        
    onReasonChange: function(component, event, helper){
       
        var options= component.get("v.optionsReason");
        component.set("v.reasonVal", event.getParam("value"));
        component.set("v.reasonLabel", options.find(opt => opt.value ===  event.getParam("value")).label);
      
    },
            
    handleSave: function(component, event, helper){
        var closedReason = component.get("v.reasonVal");        
        var isLost = component.get("v.isLost");
        var inputReason = component.find("reasonId");
        debugger;
        if(isLost && !closedReason){
            inputReason.setCustomValidity($A.get("$Label.c.Complete_Field_Error_Message"));
            inputReason.reportValidity();
        } else {
        component.set("v.showMainModal", false);
        component.set("v.showConfirmationModal", true);
        }
    },
                
    handleCancel: function(component, event, helper){
        var closeAction = $A.get("e.force:closeQuickAction");
        closeAction.fire();
        $A.get('e.force:refreshView').fire();
    },
        
    handleBack: function(component, event, helper){
		component.set("v.showMainModal", true);
        component.set("v.showConfirmationModal", false);
        
    },
    
    handleConfirmationSave: function(component, event, helper){
		helper.updateOpporunity(component);    
    }
})