({
   checkOwner: function(component, event, helper){
        var action = component.get("c.compareOwner");
        var quoteId = component.get("v.recordId");  
        var currentUser = $A.get("$SObjectType.CurrentUser.Id");
        action.setParams({recordId: quoteId, currentUser: currentUser});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('response.getState()' + response.getState());
            console.log('response.getReturnValue() '+response.getReturnValue());
            if(state === 'SUCCESS'){
           	var isEqual = response.getReturnValue();
                if(!isEqual){
                    var notEqualErrorMessage = "Error!";
                    this.showToast(component, "Warning", notEqualErrorMessage, "warning");
                     $A.get("e.force:closeQuickAction").fire();
                     $A.get('e.force:refreshView').fire();
                } else {
                    component.set("v.displayModal", true);
                }
            } else if(state === 'ERROR'){
                    console.log(JSON.stringify(response.getError()));
                }
        });
        $A.enqueueAction(action);
                           
    },
    
    checkOppStage: function(component, event, helper){
        var action = component.get("c.getOpportunityData");
        var quoteId = component.get("v.recordId");        
        action.setParams({recordId: quoteId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
           	component.set("v.oppFromQuote", response.getReturnValue());
            var oppQuote = component.get("v.oppFromQuote");
                if(!oppQuote.SBQQ__Primary__c){
                    var oppPrimaryQuoteMessage = $A.get("$Label.c.Not_Primary_Quote_Warning");
                    this.showToast(component, "Warning", oppPrimaryQuoteMessage, "warning");
                     $A.get("e.force:closeQuickAction").fire();
                     $A.get('e.force:refreshView').fire();
                }
                var oppStageMessage = $A.get("$Label.c.Closed_Opportunity_Warning");
                if(oppQuote.SBQQ__Opportunity2__r.StageName == $A.get("$Label.c.OppStage_CloseWon") ||
                   oppQuote.SBQQ__Opportunity2__r.StageName == $A.get("$Label.c.OppStage_CloseLost")){
                    this.showToast(component, "Warning", oppStageMessage, "warning");
                     $A.get("e.force:closeQuickAction").fire();
                     $A.get('e.force:refreshView').fire();
                }  else if(state === 'ERROR'){
                    console.log(JSON.stringify(response.getError()));
                }
            } else if(state === 'ERROR'){
                    console.log(JSON.stringify(response.getError()));
                }
            
        });
        $A.enqueueAction(action);
    },
    
    getStage: function(component, event, helper){
        var action = component.get("c.getStage");
        var inputStage = component.get("v.optionsStage");
        var opts=[];
        action.setCallback(this, function(response) {
            
            var pickListStageValues = response.getReturnValue();
            console.log('pickListValues  ' + pickListStageValues);
            for(var key in pickListStageValues){
				console.log(key);
				console.log(pickListStageValues[key]);
                opts.push({label:pickListStageValues[key], value:key});
                } 
           	component.set("v.optionsStage", opts);
        });
        $A.enqueueAction(action); 
    },
    
	  getCloseReason: function(component, event, helper){
    	var action = component.get("c.getCloseReason");
        var inputStage = component.get("v.optionsReason");
        var opts=[];
        action.setCallback(this, function(response) {            
                var picklistValues = response.getReturnValue();
				console.log(picklistValues);
                for(var key in picklistValues){
				console.log(key);
				console.log(picklistValues[key]);
                    opts.push({label:picklistValues[key], value:key});
                }
          component.set("v.optionsReason", opts);
        });
        $A.enqueueAction(action);
    },
        
        updateOpporunity: function(component){
        var action = component.get("c.closeOpp");
        var oppId = component.get("v.oppFromQuote.SBQQ__Opportunity2__r.Id");
        var oppName = component.get("v.oppFromQuote.SBQQ__Opportunity2__r.Name");
        var oppStage = component.get("v.valStage");
        var closedReason = component.get("v.reasonVal");
        action.setParams({oppId: oppId,
                          closeStage: oppStage,
                          closeLostReason: closedReason});
            action.setCallback(this, function(response){
        	var state = response.getState();
            var messageFields = [oppName, oppStage];
            var customLabel = this.formatLabel(component,$A.get("$Label.c.Toast_Message_for_Closed_Lost_Opportunity"),messageFields);
            if (state === "SUCCESS") {
                this.showToast(component, state, customLabel, "success");
                 $A.get("e.force:closeQuickAction").fire();
                 $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR'){
                var errors = response.getError();
                console.log('******* ' + JSON.stringify(errors));
                this.showToast(component, state, errors[0].message, "error");
            }
        });
        $A.enqueueAction(action);
        },
            
         showToast : function(component, state, message, type) {
   			var toastEvent = $A.get("e.force:showToast");
    		toastEvent.setParams({
        	"title": state,
        	"message": message,
            "type": type,
    		});
    		toastEvent.fire();
		},
    
    formatLabel : function (component,customLabel,messageFields) {
        for (var field in messageFields) {
            customLabel = customLabel.replace(new RegExp("\\{" + field + "\\}", 'g'), messageFields[field]);
        }
        return customLabel;
    }
 
})