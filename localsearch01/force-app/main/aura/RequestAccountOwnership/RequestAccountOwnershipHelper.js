({
    
    validateUserAndRecord : function(recordId, component){
        
        var action = component.get('c.validateUserAndRecord');
        
        action.setParams({
            'accountId': recordId
        });
        
        action.setCallback(this, function(response){
            
            var state = response.getState();
            
            if(state === 'SUCCESS'){
                
                var result = response.getReturnValue();
                if(result){
                    
                    if(result.noDMC){
                        
                        this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.RequestAccountOwnership_NoDMC'));
                    	$A.get('e.force:closeQuickAction').fire();
                        
                    }
                    else if(result.inApproval){
                        
                        this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.RequestAccountOwnership_InApproval'));
                    	$A.get('e.force:closeQuickAction').fire();
                        
                    }
                }
				else{
                    
                    this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                    $A.get('e.force:closeQuickAction').fire(); 
                    
				}
            }
            else{
                
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                $A.get('e.force:closeQuickAction').fire(); 
                
            }
        });
        
        $A.enqueueAction(action);
    },
    
	requestOwnership : function(recordId, component){
        
        var action = component.get('c.requestAccountOwnership');
        
        action.setParams({
            'accountId': recordId
        });
        
        action.setCallback(this, function(response){
            
            var state = response.getState();
            
            if(state === 'SUCCESS'){
                
                var result = response.getReturnValue();
                if(result){
                    
                    if(result.isSameOwner) this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.RequestAccountOwnership_AlreadyOwner'));
                    else if(result.noTerritory) this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.DMC_cannot_request_the_assignment'));
                    else if(result.genericError) this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                    else this.showToast($A.get('$Label.c.Success'), 'success', $A.get('$Label.c.RequestAccountOwnership_Requested'));
                
                }
				else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));

            }   
            else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            
            $A.get('e.force:closeQuickAction').fire(); 
            
        });
        
        $A.enqueueAction(action);
	},
    
    requestAccountTeamMembership : function(recordId, component){
        
        var action = component.get('c.requestAccountTeamMembership');
        
        action.setParams({
            'accountId': recordId
        });
        
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if(state === 'SUCCESS'){
                
                var result = response.getReturnValue();
                if(result){
                    
                    if(result.noTerritory) this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.DMC_cannot_request_the_assignment'));
                    else if(result.genericError) this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                    else this.showToast($A.get('$Label.c.Success'), 'success', $A.get('$Label.c.RequestAccountOwnership_RequestedToSell'));
                    
                }
				else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                
            }   
            else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            
            $A.get('e.force:closeQuickAction').fire(); 
            
        });
        
        $A.enqueueAction(action);
    },
    
    showToast : function(title, type, message){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: 'Sticky'
        });
        toastEvent.fire();
    }
    
})