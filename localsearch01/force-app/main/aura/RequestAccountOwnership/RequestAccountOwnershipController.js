({
	doInit : function(component, event, helper){
        
        var recordId = component.get('v.recordId');
		helper.validateUserAndRecord(recordId, component);
        
	},
    
    requestAssignment : function(component, event, helper){
        
        var recordId = component.get('v.recordId');
        helper.requestOwnership(recordId, component);
        
    },
    
    requestToSell:function(component, event, helper){
        
        var recordId = component.get('v.recordId');
        helper.requestAccountTeamMembership(recordId, component);
        
    },
    
    cancel : function(component, event, helper){
        
        $A.get('e.force:closeQuickAction').fire();
        
    },
    
    showSpinner: function(component, event, helper){
        
        component.set('v.Spinner', true); 
        
    },
    
    hideSpinner : function(component,event,helper){
        
        component.set('v.Spinner', false);
        
    }
})