({
    
    checkUserGrant: function(component, event)
    {
        var action = component.get("c.hasUserGrant");
        action.setParams(
            {
                'cpname' : $A.get("$Label.c.terminateSubscriptionQAenabled_customPermission_Name")
            }
        );
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if(res["allowed"] == false)
                {
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": "warning",
                        "message": $A.get("$Label.c.terminateSubscription_quickAction_insufficientPermission_ErrorMsg")
                    });
                    toastEvent.fire();
                }
            }
        }
       );
       $A.enqueueAction(action);
    },
    getSubscription: function (component, event) {
        var terminateContract = component.get('v.terminateContract');
        var recordId = component.get('v.recordId');
        var action = component.get("c.getContractAndSubscriptionAndReasons");
        var params = { recordId: recordId, terminateContract: terminateContract };
        action.setParams(params);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.optionsSubscriptions', data.listSubscriptionsOptions);
                component.set('v.singleTermination', data.singleSubscription);
                component.set('v.terminationReason', data.listReasonOptions);
                component.set('v.contractId', data.contractId);
                component.set('v.showdualListbox', !data.singleSubscription && !terminateContract);
                if (!data.singleSubscription && data.contractStatus != $A.get("$Label.c.Contract_Active")) {
                    this.showNotifWarning(component, event, $A.get("$Label.c.Contract_not_active_cannot_terminate"), "");
                    this.closeModal(component, event);
                    return;
                }
                else if (data.singleSubscription) {
                    var subscription = data.listSubscriptionsOptions[0];
                    if (subscription.Subsctiption_Status__c != $A.get("$Label.c.Subscription_Active")) {
                        this.showNotifWarning(component, event, $A.get("$Label.c.Subscription_not_active_cannot_terminate"), "");
                        this.closeModal(component, event);
                        return;
                    } 
                    else if (subscription.bundled) {
                        this.showNotifWarning(component, event, $A.get("$Label.c.Subscription_inside_bundle"), "");
                        this.closeModal(component, event);
                        return;
                    }
                } 
				else if (typeof data.listSubscriptionsOptions === "undefined" || $A.util.isEmpty(data.listSubscriptionsOptions)){
                    this.showNotifWarning(component, event, $A.get("$Label.c.Only_one_time_fee_sub"), "");
                    this.closeModal(component, event);
                    return;
                }

                if (data.singleSubscription) {
                    component.set('v.selectedSubscriptions', data.listSubscriptionsOptions);
                } 
                else {
                    component.set('v.columns', [
                        { label: 'Subscription', fieldName: 'Name', type: 'text' },
                        { label: 'Product name', fieldName: 'SBQQ__ProductName__c', type: 'text' },
                        { label: 'Product Code', fieldName: 'Product_Code__c', type: 'text' },
                        { label: 'Place', fieldName: 'PlaceName', type: 'text'},
                        { label: 'Status', fieldName: 'Subsctiption_Status__c', type: 'text' }
                    ]);
                    data.listSubscriptionsOptions.forEach(element => {if(typeof element.Place__c !== "undefined"){element.PlaceName = element.Place__r.Name;}else{element.PlaceName = '';}});
                    component.set('v.data', data.listSubscriptionsOptions);
                }
                component.set('v.loadingData', false);
            } 
            else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    if(errors[0].message == $A.get("$Label.c.samba_validation_termination_subscr")){
                        this.showNotifWarning(component, event, $A.get("$Label.c.samba_validation_termination_subscr"), $A.get("$Label.c.samba_validation_termination_subscr"));
                    }
                } else {
                	this.showNotifError(component, event, $A.get("$Label.c.Error_sending_information"), $A.get("$Label.c.Operation_failed"));
                }
            }
        });

        $A.enqueueAction(action);
    },
    
    terminationContract: function (component, event) {
        var mindate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var maxDate = new Date();

        var checkCmp = component.find("TerminateAtEndOfInvoiceToggle");
        var isDateLocked = component.get("v.isDateLocked");
        var finalInvoice = component.find('finIvoice');
        
        
        if(!component.get('v.isEvergreen')){
            maxDate.setDate(maxDate.getDate() + 1825);
        	maxDate.setHours(0, 0, 0);
        }
        else{
            var endDate = component.get('v.contractEndDate')
            maxDate = new Date(endDate);
            maxDate.setHours(0, 0, 0);
        }

        var subscription = component.get('v.selectedSubscriptions');
        var subTermination = component.find('subTermination');
        var reasonTermination = component.find("reasonTermination").get("v.value");
        var cmpDate = component.find('terminationDate');
        var terminationDate = cmpDate.get("v.value");
        var creditNote = component.get("v.sendCreditNote");
        var generateFinalInvoice = component.get("v.generateFinalInvoice");
        var terminateContract = component.get('v.terminateContract');
        var terminusDate = new Date(terminationDate);
        terminusDate.setHours(0, 0, 0);
        var noClawback = component.get("v.noClawback");
        
        var subscriptionIds = [];
        for (var i = 0; i < subscription.length; i++) subscriptionIds.push(subscription[i].Id);
        
        var components = ["reasonTermination", "terminationDate"];
        
        if (terminusDate < mindate) cmpDate.setCustomValidity($A.get("$Label.c.Date_cannot_be_in_the_past"));
        else if (!component.get('v.isEvergreen') && terminusDate > maxDate) cmpDate.setCustomValidity("Date cannot be greater than the contract end date days");
        
        else cmpDate.setCustomValidity("");
        cmpDate.reportValidity();
        
        var countSubsError = false;
        if (!terminateContract && subscriptionIds.length == 0) {
            component.set('v.data_errors', {
                table: {
                    title: 'No subscription Selected',
                    messages: ['You must select at least one subscription']
                }
            });
            component.set('v.data_errors2', 'You must select at least one subscription');
            countSubsError = true
        } 
        else component.set('v.data_errors', null);
        
        if (!this.validModal(component, event, components) || countSubsError) {
            component.set('v.disableSaveButton', false);
            return;
        }

        var action;
        var params;
        
        var callServerAction = true;
        
        if(terminateContract){
            var isEvergreen = component.get('v.isEvergreen');
            var minEvergreenEndDate = component.get('v.minEvergreenEndDate');
            var isConfirmationOpen = component.get('v.confirmationDialogOpen');
            
            if(isEvergreen && terminationDate > minEvergreenEndDate && !isConfirmationOpen){
                callServerAction = false;
                component.set('v.confirmationDialogOpen', true);
                this.openConfirmationModal(component);
            }
            else{
                action = component.get("c.terminationContract");
                params = {
                    		contractId : component.get("v.contractId"),
                          	endDate : JSON.stringify(terminationDate),
                          	reason : reasonTermination,
                          	creditNote : creditNote,
                          	isDateLocked : isDateLocked,
                          	noClawback : noClawback
                         };
            }
        } 
        else {
            var contractIds = [];
            contractIds.push(component.get("v.contractId"));
            action = component.get("c.terminationSubscriptions");
            params = {  contractIds : contractIds ,
                      subscriptionIds : subscriptionIds ,
                      endDate : JSON.stringify(terminationDate) ,
                      reason : reasonTermination ,
                      creditNote : creditNote,
                      terminatedQuantity : 0 ,
                      isDateLocked : isDateLocked,
                      noClawback : noClawback    
                     }; 
        }
        
        if(callServerAction){
            
            action.setParams(params);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data = response.getReturnValue();
                    if(data.valid){
                        this.showNotifSuccess(component, event, $A.get("$Label.c.Information_send_with_Success"), $A.get("$Label.c.Operation_Success"));
                        return;
                    }
                    else{
                    	if(data.validations.length > 0){
                            var msg = '';
                            for(var i = 0; i < data.validations.length; i++) msg = msg + data.validations[i] + ',';
                            this.showNotifWarning(component, event, msg, "");
                            return;
                        }
                        else if(data.errors.length > 0){
                            var msg = '';
                            for(var i = 0; i < data.errors.length; i++) msg = msg + data.errors[i] + ',';
                            this.showNotifError(component, event, msg, $A.get("$Label.c.Operation_failed"));
                            return;
                        }
                    }
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) console.log("Error message: " + errors[0].message);
                    else console.log("Unknown error");
                    component.set('v.disableSaveButton', false);
                }
                this.showNotifError(component, event, $A.get("$Label.c.Error_sending_information"), $A.get("$Label.c.Operation_failed"));
            });
            $A.enqueueAction(action);
        }
    },
    showNotifError: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "error",
            "header": header,
            "message": message
        });
        component.set('v.disableSaveButton', false);
    },
    showNotifWarning: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "warning",
            "header": header,
            "message": message
        });
    },
    showNotifSuccess: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "success",
            "header": header,
            "message": message
        });
        $A.get('e.force:refreshView').fire();
        this.closeModal(component, event);
    },
    closeModal: function (component, event) {
        $A.get("e.force:closeQuickAction").fire();
        component.find("overlayLib2").notifyClose();
    },
    validModal: function (component, event, listComponents) {
        var valid = true;
        for (var i = 0; i < listComponents.length; i++) {
            var field = component.find(listComponents[i]);
            if (field) {
                field.showHelpMessageIfInvalid();
                if (!field.get("v.validity").valid) {
                    valid = false;
                }
            }
        }
        return valid;
    },

    lockEndDate : function(component,event){
        var checkCmp = component.find("TerminateAtEndOfInvoiceToggle");
        var dateCmp = component.find("terminationDate");
        var isDateLocked = checkCmp.get("v.checked");
        var todayDate = new Date();
        var today = todayDate.getFullYear() + "." + todayDate.getMonth() + "." + todayDate.getDate();
        console.log('today attribute:' + today);
        dateCmp.set("v.disabled",isDateLocked);
        component.set("v.isDateRequired",!isDateLocked);
        component.set("v.today",today);
        component.set("v.isDateLocked",isDateLocked);
        if(isDateLocked)
            dateCmp.set("v.value","");

        console.log("Checkbox is "+isDateLocked);
    },
    
    isEvergreen: function (component, event) {
        var action = component.get("c.isRenewableContract");
        var recordId = component.get('v.recordId');
        var params = { contractId: recordId};
        action.setParams(params);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var maxDate = new Date();
                var res = response.getReturnValue();
                if(res){
                    component.set('v.isEvergreen', res.isEvergreen);
                    if(res.isEvergreen && res.maxEndDate && res.minEndDate){
                        component.set('v.minEvergreenEndDate', res.minEndDate);
                        component.set('v.maxDate', $A.localizationService.formatDate(res.maxEndDate, "YYYY-MM-DD"));
                        console.log('Max. End Date: ' + component.get('v.maxDate'));
                        console.log('Min. End Date: ' + component.get('v.minEvergreenEndDate'));
                    }
                }
                
                if(!component.get('v.isEvergreen')){
                	var action2 = component.get("c.getContractEndDate");
                    action2.setParams(params);
                    action2.setCallback(this, function (responseEndDate) {
                        var state2 = responseEndDate.getState();
                        if (state2 === "SUCCESS") {
                            var endDate = responseEndDate.getReturnValue();
                            component.set('v.maxDate', endDate);
                            console.log('>>>maxDate: ' + component.get('v.maxDate'));
                            component.set('v.contractEndDate', endDate);
                        }
                    });
                    $A.enqueueAction(action2);
                }
            }
       });
       $A.enqueueAction(action);
    },
    openConfirmationModal : function(component) {
        var modal = component.find("confirmationModal");
        var modalBackdrop = component.find("confirmationModal-Back");
        $A.util.addClass(modal, 'slds-fade-in-open');
        $A.util.addClass(modalBackdrop, 'slds-fade-in-open');
    },
    closeConfirmationModal : function(component){
        console.log('Closing confirmation modal')
        var modal = component.find("confirmationModal");
        var modalBackdrop = component.find("confirmationModal-Back");
        $A.util.removeClass(modal, 'slds-fade-in-open');
        $A.util.removeClass(modalBackdrop, 'slds-fade-in-open');
    }
})