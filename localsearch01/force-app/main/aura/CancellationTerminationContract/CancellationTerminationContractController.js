({
    doInit : function(component, event, helper) {
        helper.fetchTerminationReasonPicklist(component);
        helper.isEvergreen(component,event);
        helper.checkUserGrant(component, event);
        //helper.isGenerateFinalInvoiceAccessible(component, event);
        var today = new Date();
        var todayString = $A.localizationService.formatDate(today, "YYYY-MM-DD");
        component.set('v.today', todayString);
        component.set('v.minDate', todayString);
        helper.getSubscription(component, event);
    },
    handleTerminationClick: function(component, event, helper) {
        component.set('v.disableSaveButton', true);
        helper.terminationContract(component, event);     
    },
    handleChangeSubscriptions: function (component, event) {
        var selectedOptionValue = event.getParam("value");
        component.set('v.selectedSubscriptions', selectedOptionValue.toString().split(','));
    },
    updateSelectedSubscriptions: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        var allSubs = component.get('v.data');
        var subsToAutoSelect = [];
        //console.log('ALL SUBS --> '+JSON.stringify(allSubs));
        if(selectedRows && allSubs){
            selectedRows.forEach(function(currentRow){
                if(!subsToAutoSelect.includes(currentRow)) subsToAutoSelect.push(currentRow);
                if(!currentRow.RequiredBy__c){
                    console.log('current row is master product');
                    allSubs.forEach(function(currentSub){
                        if(currentSub.RequiredBy__c){
                            if(currentSub.RequiredBy__c == currentRow.Id){
                                if(!subsToAutoSelect.includes(currentSub)) subsToAutoSelect.push(currentSub);
                            }
                        }
                    });
                }
            });
        }
        console.log('selected --> '+JSON.stringify(subsToAutoSelect));
        component.set('v.selectedSubscriptions', subsToAutoSelect);
    },
    handleCancelClick: function(component, event, helper) {
        helper.closeModal(component, event);
    },
    handleToggle: function(component, event, helper){
        helper.lockEndDate(component, event);
    },
    confirmationModalProceed : function(component,event,helper){
        helper.closeConfirmationModal(component);
        helper.terminationContract(component, event);
    },
    confirmationModalCancel : function(component,event,helper){  
        helper.closeConfirmationModal(component);
        component.set('v.confirmationDialogOpen', false);
        component.set('v.disableSaveButton', false);
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
})