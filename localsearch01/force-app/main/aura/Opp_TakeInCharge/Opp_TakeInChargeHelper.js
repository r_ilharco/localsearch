({
	takeInCharge : function(recordId, component) {
        var action = component.get('c.changeOwner');
        action.setParams({
            'recordId': recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get('e.force:closeQuickAction').fire();  
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                if(result){
                    if(result.success){
                            this.showToast($A.get('$Label.c.Success'), 'success', $A.get('$Label.c.Operation_Success'));
                    }
                    else{
                        if(result.errorMessage){
                                this.showToast($A.get('$Label.c.Error'), 'error', result.errorMessage);
                        }
                        else{
                            this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                        }
                    }
                }
                else{
                    this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                }
           $A.get('e.force:refreshView').fire();
            }
            else{
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            }
        });
       
        $A.enqueueAction(action);
	},
    
    showToast : function(title, type, message){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: 'Sticky'
        });
        toastEvent.fire();
    },
})