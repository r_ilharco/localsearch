({
	doInit : function(component, event, helper) {
        console.log('recordId :: '+component.get('v.recordId'));
        console.log('gridData :: '+component.get('v.gridData'));
        var language = [
            {label : "English", value :'en'},
            {label : "German", value :'de'},
            {label : "Italian", value :'it'},
            {label : "French", value :'fr'}
        ];
        component.set('v.selectedLanguage', component.get("v.languageValue")); 
        component.set('v.options', language);
        
	},
    showSpinner : function(component, event, helper) {
        component.set('v.Spinner', true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set('v.Spinner', false);
    },
    handleCancel : function(component, event, helper) {
        component.destroy();
    },
    handleChange: function (component, event) {
       component.set("v.selectedLanguage",event.getParam("value"));   
    },
    handleProceed : function (component, event) {

         var compEvent = component.getEvent("pdfLanguageSel");
         compEvent.setParams({"language" : component.get("v.selectedLanguage") });   
         compEvent.fire();
        
        
        console.log(component.get("v.selectedLanguage"));
        component.destroy();
    },
})