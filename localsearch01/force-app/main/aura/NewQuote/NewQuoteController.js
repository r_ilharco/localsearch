({
	doInit : function(cmp, event, helper) {
        var createQuote = cmp.get('c.createQuote');
        var oppId = cmp.get("v.recordId");
        createQuote.setParams({oppId:oppId});
        var data;
        createQuote.setCallback(this, function(response) {
            data = response.getReturnValue();
            console.log('data: ' + data);
            if(data.quoteId != null){        
                cmp.set('v.quoteId', data.quoteId);
                helper.gotoURL(cmp);
            }else{
                helper.showToast(cmp,event,'ERROR', data.message ,'ERROR');
            }
        });
        $A.enqueueAction(createQuote); 
	}
})