({
	doInit : function(component, event, helper) {                              
        component.find("forceCreditNoteRecord").getNewRecord(
            "Credit_Note__c",
            null,
            false,
            $A.getCallback(function() {
                var invoiceItemRecordId = component.get('v.recordId');
                if(invoiceItemRecordId) {
                    helper.isAllowedCheck(component, event, helper, invoiceItemRecordId);
                    console.log('component.sub'+component.find("Subscription__c").get('v.value'));
                    helper.getDefaultValues(component, event, helper, invoiceItemRecordId);
                }
            })
        );   
        
	},
    invoiceValueChanged : function(component, event, helper) {
    	 component.set('v.error', null);  
	},
    handleSubmit: function(component, event, helper) {
        event.preventDefault();
        var fields = event.getParam('fields');       
        var invoiceAmount = component.get("v.creditNoteRecord.Max_Possible_Amount");
        var errorMessage = '';
        var periodFrom = component.get("v.creditNoteRecord.Period_From__c");
        var periodTo = component.get('v.creditNoteRecord.Period_To__c');
        var periodFromTemp = component.get('v.periodFromTemp');
        
        if(!fields.Reimbursed_Invoice__c){
            errorMessage += $A.get("$Label.c.No_Invoice") + '\n';
        }
        
        /*if(!fields.Description__c || fields.Description__c.trim() == ''){
            errorMessage += $A.get('$Label.c.Credit_note_Description_required') + '\n';
        }*/       
        
        if(fields.Value__c > invoiceAmount) {
            errorMessage += $A.get("$Label.c.Credit_Note_Value_Greater_Than_Remaining") + ' '+ invoiceAmount +'\n';
        }
        
        if(fields.Value__c <= 0){
            errorMessage += $A.get("$Label.c.Credit_Note_Value_Less_Than_Equal_Zero") + '\n';
        }
        
        if(periodFrom > periodTo){
            errorMessage += 'The Period From date has to be prior to the Period To date' + '\n';
        }
        console.log('periodFromTemp: ' + periodFromTemp);
        console.log('periodFrom: ' + periodFrom);
        if(periodFrom < periodFromTemp){
            errorMessage += 'The Period From date cannot be prior to: ' + periodFromTemp + '\n';
        }
        
        if(errorMessage != ''){
            component.set('v.error', errorMessage);
            return false;
        }
        
        component.find('reForm').submit(fields);
    },
    handleSuccess: function(component, event, helper) {
        event.preventDefault();
        helper.showNotificationMessage(component, event, 'Record Saved', 'success', $A.get("$Label.c.New_Credit_Note_Created"));
        return true;        
    },
    handleError : function(component, event, helper) {
        var resultsToast = $A.get("e.force:showToast");
        var resp = event.getParams();
        resultsToast.setParams({
            "title": "Error",
            "message": resp.errorCode + '\n' + resp.message,
            "type": "error"
        });
        resultsToast.fire();
        return false;
    },
    cancelDialog : function(component, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
    
})