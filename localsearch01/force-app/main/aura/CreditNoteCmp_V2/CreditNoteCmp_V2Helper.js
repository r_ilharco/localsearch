({
    setValues : function(component, map) {
        if(map != null) {
            Object.keys(map).forEach(function(key) {
                component.set('v.creditNoteRecord.' + key, map[key]);
            });
        }
	},
    getDefaultValues : function(component, event, helper, invoiceItemRecordId) {
        var action = component.get('c.getDefaultCreditNoteValues');
        action.setParams({invoiceItemRecordId:invoiceItemRecordId});
        
        action.setCallback(this, function(response) {
            var map = response.getReturnValue();
            console.log('map: ' + map);
            var errorMessage = map['Error_Message'];
            if(errorMessage != null){
                this.showNotificationMessage(component, event, 'Error', 'error', errorMessage);                            
            }           
            
            this.setValues(component, map);
            console.log('periodTemp: ' + component.get("v.creditNoteRecord.Period_From__c"));
        	component.set('v.periodFromTemp', component.get("v.creditNoteRecord.Period_From__c"));
        });
        $A.enqueueAction(action);
    	       
	},
    isAllowedCheck : function(component, event, helper, invoiceItemRecordId) {
        var action = component.get('c.isAllowedCheck');
        action.setParams({invoiceItemRecordId:invoiceItemRecordId});
        
        action.setCallback(this, function(response) {
            var isAllowed = response.getReturnValue();
            console.log('isAllowed: ' + isAllowed);
            if(isAllowed){
            }else{
                this.showNotificationMessage(component, event, 'Error', 'Error', $A.get("$Label.c.Credit_Note_Not_Allowed"));
            }          
        });
        $A.enqueueAction(action);
        
    },
    showNotificationMessage : function(component, event, title, type, message) {        
        var resultsToast = $A.get("e.force:showToast");
        var resp = event.getParams();
        resultsToast.setParams({
            "title": title,
            "type": type,
            "message": message            
        });
        resultsToast.fire();        
        $A.get("e.force:closeQuickAction").fire();
        //$A.get('e.force:refreshView').fire();
    }
})