({
    showToast : function(component,event,type,message,title){
        console.log(type+message+title);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
         $A.get("e.force:closeQuickAction").fire();
    },
    
    getSubscriptions: function(component, event,helper) {
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        console.log('today: ' + today);
        var d = new Date(today);
		d.setDate(d.getDate() + 364);
        console.log('d: ' + d);
        var maxDate = $A.localizationService.formatDate(d, "YYYY-MM-DD");
        console.log('maxDate: ' + maxDate);
        component.set('v.maxDate', maxDate);
        var contractId = component.get('v.recordId');
        console.log('Contract ID: '+contractId);
        var action = component.get('c.getSubscriptions');
        var params = {contractId : contractId};
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('STATE: '+state);
            if (state === "SUCCESS"){
                var data = response.getReturnValue();
                console.log('Data: '+JSON.stringify(data));
                if(data != null){
                    console.log('Data not null');
                    console.log('subscriptions: ' + data);
                    component.set('v.subscriptions',data);
                    let subscriptions = component.get('v.subscriptions');
                    subscriptions.forEach(function(subElem) {
                        console.log('here for data');
                        console.log('errorMessage: ' + subElem['errorMessage']);
                        if(subElem['errorMessage'] != ''){
                            console.log('Here for errors');
                            console.log('errorMessage: '+  subElem['errorMessage']);
                            component.set('v.isError',true);
                            component.set('v.errorMessage', subElem['errorMessage']);
                        }else{
                            subElem['minDate'] = $A.localizationService.formatDate(subElem['minDate'], "YYYY-MM-DD");
                            subElem['maxDate'] = $A.localizationService.formatDate(subElem['maxDate'], "YYYY-MM-DD");
                            console.log(subElem['minDate']);
                            console.log(subElem['maxDate']);
                            component.set('v.isError',false);
                        }
                    });
                }
            }
            else{
                //State === "ERROR"
                var errors = response.getError();
                if(errors){
                    this.showToast(component,event,'error', errors[0].message, 'ERROR');    
                }
            }            
        });
        $A.enqueueAction(action);
    },
    updateSubscriptions: function(component, event,helper){
        this.showSpinner(component, event,helper);
        var newSubs = JSON.stringify(component.get('v.subscriptions'));
        console.log('newSubs' + JSON.stringify(newSubs));
        var contractId = component.get('v.recordId');
        console.log('contractId' + contractId);
        var action = component.get('c.updateSubscriptions');
        var params = {jsonSubs : newSubs, contractId : contractId};
        console.log('params' + params);
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('STATE: '+state);
            if (state === "SUCCESS"){
                var data = response.getReturnValue();
                console.log('Data: '+JSON.stringify(data));
                if(data.isStatusOk){
                    console.log('isStatusOk: '+data.isStatusOk);
                    this.showToast(component,event,'Success', $A.get("{!$Label.c.ActivateSubscriptionsCmp_SuccessMessage}"), 'SUCCESS');
                    $A.get('e.force:refreshView').fire();
                }else{
                    this.showToast(component,event,'Error', data.errorMessage, 'ERROR');
                }
            }else{
                //State === "ERROR"
                var errors = response.getError();
                if(errors){
                    this.showToast(component,event,'error', errors[0].message, 'ERROR');    
                }
            }   
        })
        $A.enqueueAction(action);        
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
})