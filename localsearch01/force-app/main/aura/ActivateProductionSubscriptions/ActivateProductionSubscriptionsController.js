({
    doInit: function (cmp, event, helper) {
        var action = cmp.get('c.hasUserGrant');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('STATE: '+state);
            if (state === "SUCCESS"){
                var data = response.getReturnValue();
                console.log('Data: '+JSON.stringify(data));
                if(data != null){
                    if(data){
                        helper.getSubscriptions(cmp,event,helper);
                    }else{
                    	helper.showToast(cmp,event,'error', $A.get("{!$Label.c.ActivateSubscriptionsCmp_InsufficientPermission}"), 'ERROR');    
                    }
                }
            }   
        });
        $A.enqueueAction(action);
    },
    
    handleActivate: function (cmp, event, helper){
        helper.updateSubscriptions(cmp,event);
    },    
    handleCancel: function (cmp,event,helper){
        $A.get("e.force:closeQuickAction").fire();
    }
});
