({
	doInit: function(component, event, helper){ 
        var urlString = window.location.href;
        var baseUrl = window.location.origin;

        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        component.set("v.baseURL", baseURL);
    },
    
 	onCheckboxChange : function(component, event, helper) {
        var rowValue = event.getSource().get("v.value");
        if(!rowValue){
            component.set("v.oppRecord.CampaignId",null)
        }else{
            component.set("v.oppRecord.CampaignId",event.getSource().get("v.text")); 
        }
        console.log("event.getSource().get "+ event.getSource().get("v.text"));
        console.log("v.oppRecord.CampaignId :: "+ component.get("v.oppRecord.CampaignId"));
        
        
		var availableCheckboxes = component.find('rowSelectionCheckboxId');
       	console.log('availableCheckboxes :: '+JSON.stringify(availableCheckboxes));
        var resetCheckboxValue  = false;
        if (Array.isArray(availableCheckboxes)) {
            availableCheckboxes.forEach(function(checkbox) {
                checkbox.set('v.value', resetCheckboxValue);
            }); 
        }else{
            availableCheckboxes.set('v.value', resetCheckboxValue);
        }
       	event.getSource().set("v.value", rowValue);
        
	},
    
     handleRecordUpdated: function(component, event, helper) {
           
        var eventParams = event.getParams();
         console.log("eventParams.changeType :: "+eventParams.changeType);
       
        if(eventParams.changeType === "LOADED") {
          
            console.log("Record is loaded successfully.");
            component.set("v.initialCampaingId",component.get("v.oppRecord.CampaignId"));
			helper.getData(component,event,helper);
        	helper.setCols(component,event,helper);
            
            
        } else if(eventParams.changeType === "ERROR") {
             console.log('Error: ' + component.get("v.recordLoadError"));
             helper.showToastMessage(component,event,"Error",component.get("v.recordLoadError"),"error","sticky");
             helper.closeAction(component, event);
        }
    },
    
    
    closeAction : function (component,event,helper){
        helper.closeAction(component,event);
    },
    
     handleSaveRecord: function(component, event, helper) {
          
         var div = document.getElementById("spinnerBox");
         div.style = "text-align: center; height: 300px; padding-top: 150px; display: block;";
         
         var dataTableCMP = component.find("dataTableCamps");
         $A.util.removeClass(dataTableCMP, "slds-show");
         $A.util.addClass(dataTableCMP, "slds-hide");
         
         var initialCamp = component.get("v.initialCampaingId");
         var selCamp = component.get("v.oppRecord.CampaignId");

         console.log("initialCamp :: "+initialCamp+" selCamp :: "+selCamp);
        component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                console.log("Save completed successfully.");
                if((initialCamp == null && selCamp != null) || (initialCamp != null && selCamp != null && initialCamp != selCamp)  ){
                    var spinnerBoxDiv = document.getElementById("spinnerBox");
               		spinnerBoxDiv.style = "display: none;";
                    helper.showToastMessage(component,event,$A.get("$Label.c.Success"),$A.get("$Label.c.CampLink_Success"),"success","sticky"); 
                     helper.closeAction(component, event);
                }else{
                    helper.removeQuotelinePromotions(component,event,helper);
                    
                }
                
               

              
                
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' +
                           JSON.stringify(saveResult.error));
                  helper.showToastMessage(component,event,"Error",JSON.stringify(saveResult.error),"error","sticky"); 
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
     },
   
    
    
    
})