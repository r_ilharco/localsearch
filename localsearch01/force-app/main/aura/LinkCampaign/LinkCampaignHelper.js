({
	getData : function(component,event) {
        
        var div = document.getElementById("spinnerBox");
         div.style = "text-align: center; height: 300px; padding-top: 150px; display: block;";
         
         var dataTableCMP = component.find("dataTableCamps");
         $A.util.removeClass(dataTableCMP, "slds-show");
         $A.util.addClass(dataTableCMP, "slds-hide");
        
        console.log('GET DATA :: ');
		var recordId = component.get("v.recordId");
        
         var action = component.get("c.getActiveCampaingsByType");
        console.log(component.get("v.oppRecord.CampaignId"));
        //console.log('component.get("v.recordId"): ' + component.get("v.recordId"));
        action.setParams({
            campaingId: component.get("v.oppRecord.CampaignId"),
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state ::: '+state);
            //console.log('response: ' + JSON.stringify(response));
            if (state === "SUCCESS") {
				var result = response.getReturnValue();
                console.log('response: ' + JSON.stringify(result));
               	div.style = "display: none;"; 
                var dataTableCMP = component.find("dataTableCamps");
         		$A.util.removeClass(dataTableCMP, "slds-hide");
         		$A.util.addClass(dataTableCMP, "slds-show");
                
                 if(result){
                  
                     if(!result.isUserAllowed){
                         
                         this.showToastMessage(component,event,"Error",result.errorMessages,"error","sticky");
                         
                         
                         	$A.get("e.force:closeQuickAction").fire();
                        	$A.get('e.force:refreshView').fire();
                     }else{
                    	if(result.isEmpty){
                            var showMessageCMP = component.find("showMessageNoCampaings");
                            //component.set("v.message",$A.get("$Label.c.Substitution_No_Products_available"));
                            
                            $A.util.removeClass(showMessageCMP,"slds-hide");
                            $A.util.addClass(showMessageCMP,"slds-show");
                    	}else{
                            if(!result.readOnly){
                              component.set("v.IsOkButtonDisabled",false);  
                            }else{
                              component.set("v.readyOnlyMode",true);    
                            } 
                        
                               for(let i = 0; i < result.campaignList.length; i++){
                                 var camp = result.campaignList[i];
                                 console.log(camp);
                            
                                 camp.campaignUrl = '/' + camp.Id;
                                 camp.campaingName = camp.Name;
                                 camp.campStartDate = camp.StartDate;
                                 camp.campEndDate = camp.EndDate;
                                 camp.campType = camp.Type; 
                                 camp.campStatus = camp.Status;
                                 camp.campSalesChannel = camp.Sales_Channel__c;
                                 camp.selected = camp.Id == component.get("v.oppRecord.CampaignId") ? true : false;
                                
                                }
                             console.log(result.campaignList)
                             component.set("v.dataCamps", result.campaignList);
            
                                const dataCamps = component.find("dataTableCamps");
                                $A.util.addClass(dataCamps,"slds-show");
                                $A.util.removeClass(dataCamps,"slds-hide"); 
                                
                                
                    	}  
                     }
                    
                     
                
                 }
                
            }else{
                this.showToastMessage(component,event,"Error",response.getError()[0].message,"error","sticky");
                $A.get("e.force:closeQuickAction").fire();
                        	$A.get('e.force:refreshView').fire();
            }
       
        });
        
        $A.enqueueAction(action);
    },
    
    closeAction :  function(component,event){
     	$A.get('e.force:refreshView').fire();
     	$A.get("e.force:closeQuickAction").fire();  
    },
    
    showToastMessage :  function (component,event,title,message,type,mode){
       var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": title,
                        "message": message,
                        "type": type,
                        "mode": mode
                    });
                    toastEvent.fire();
    },
     
   removeQuotelinePromotions : function (component,event,helper){
        var actionQLs = component.get("c.removeQLPromotions");
        
        actionQLs.setParams({
            recordId : component.get("v.oppRecord.SBQQ__PrimaryQuote__c")
        });
        actionQLs.setCallback(this, function(response) {
            var spinnerBoxDiv = document.getElementById("spinnerBox");
            spinnerBoxDiv.style = "display: none;";
            
            var state = response.getState();
            console.log('state ::: '+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result){
                    if(result.success){
                       this.showToastMessage(component,event,$A.get("$Label.c.Success"),$A.get("$Label.c.CampLink_Success_Unlink"),"success","sticky");
                       
                    }else{
                       this.showToastMessage(component,event,"Error",result.errorMessages,"error","sticky");
                        component.set("v.oppRecord.CampaignId",component.get("v.initialCampaingId"));
                        this.rollbackOpp(component,event);
                    }
                }
				
            }else{
                if(response.getError()[0].message){
                    this.showToastMessage(component,event,"Error",response.getError()[0].message,"error","sticky");
                }else{
                    this.showToastMessage(component,event,"Error",$A.get("$Label.c.Generic_Error"),"error","sticky");
                } 
                 component.set("v.oppRecord.CampaignId",component.get("v.initialCampaingId"));
                 this.rollbackOpp(component,event);
            }
            
             this.closeAction(component, event);
             	
        });
        
        $A.enqueueAction(actionQLs);
       
   },
    
    rollbackOpp : function (component,event){
         component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log("Save completed successfully."); 
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' +
                           JSON.stringify(saveResult.error));
                  helper.showToastMessage(component,event,"Error",JSON.stringify(saveResult.error),"error","sticky"); 
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    },
    
    setCols : function(component,event,helper){
        component.set("v.columnsCamp", [
            { label: 'Campaign Name', fieldName: 'campaignUrl', type: 'url', initialWidth: 200,
             typeAttributes: {
                 label: { fieldName: 'campaingName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'left' }
            },
           	{ label: 'Start Date', fieldName: 'campStartDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'End Date', fieldName: 'campEndDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Type', fieldName: 'campType', type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Status', fieldName: 'campStatus', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'center' }},
            { label: 'Sales Channel', fieldName: 'campSalesChannel', type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center' }},
        ]);
    },

            
            
          
         
})