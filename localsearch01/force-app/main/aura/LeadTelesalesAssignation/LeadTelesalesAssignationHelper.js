({
	showToast : function(component,event,type,message,title){
        console.log(type+message+title);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
    },
    
    navigateToListView: function(component, event, listViewId){
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
                "listViewId": listViewId,
                "listViewName": null,
                "scope": "Lead"
            });
        navEvent.fire(); 
    },

	assignLead: function(component,event){
		var recordId = component.get("v.recordId");
		var action = component.get("c.assignLeadToTelesalesQueue");
        var params = { leadId : recordId };
        console.log('lead id: '+recordId);
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            var response = response.getReturnValue();
            console.log('callback state: '+state);
            if (state === "SUCCESS") {
				this.showToast(component,event,'success', $A.get("{!$Label.c.Telesales_lead_assignment_success}"),'SUCCESS');    
                //A. L. Marotta 19-12-19 Start, Wave E, Sprint 13 - record visibility for old owner id isn't removed, no need to navigate to lead list view
                /*if(listViewId != null){
                    this.navigateToListView(component,event,listViewId);
                }*/
                if(response!= null){
                    if(response["oldOwnerIsDMC"]){
                        $A.get('e.force:refreshView').fire();
                    }else{
                        if(response["listViewId"] != null){
                            this.navigateToListView(component,event,response["listViewId"]);
                        }
                    }
                }
                //A. L. Marotta 19-12-19 End
            } else {
                var errors = response.getError();
                if(errors){
                	console.log('error');
                    this.showToast(component,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));    
                }
			}
		});
        //$A.get("e.force:closeQuickAction").fire();
        $A.enqueueAction(action);
	}
	

})