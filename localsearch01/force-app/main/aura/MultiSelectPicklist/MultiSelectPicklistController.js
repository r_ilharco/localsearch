({
	doInit : function( component, event, helper ) {
        helper.doInitHelper(component);
	},
 
	filterOptions : function( component, event, helper ) {
        if( !$A.util.isEmpty(component.get('v.searchString')) ) {
		    helper.filterOptionsHelper(component);
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
	},
 
	selectItem : function( component, event, helper ) {
        if(!$A.util.isEmpty(event.currentTarget.id)) {
            helper.selectItemHelper(component, event);
        }
	},
    
    showOptions : function( component, event, helper ) {
        var disabled = component.get("v.disabled");
        if(!disabled) {
        	component.set("v.message", '');
            component.set('v.searchString', '');
            var options = component.get("v.options");
            options.forEach( function(element,index) {
                element.isVisible = true;
            });
            component.set("v.options", options);
            if(!$A.util.isEmpty(component.get('v.options'))) {
                $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
            } 
        }
	},
 
	removePill : function( component, event, helper ){
        if(!component.get("v.disabled")){
            helper.removePillHelper(component, event);
        }
    },
 
    blurEvent : function( component, event, helper ){
        helper.blurEventHelper(component, event);
    }
})