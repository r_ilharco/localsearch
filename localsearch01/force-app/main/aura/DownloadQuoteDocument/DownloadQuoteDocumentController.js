({
	handleRecordUpdated: function(component, event, helper) {
        component.set("v.spinner",true);
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
            if(component.get("v.simpleRecord.SBQQ__SignatureStatus__c") != 'Failed' || component.get("v.simpleRecord.ExpirationDate__c") < today){
                component.set("v.warn",$A.get("$Label.c.QuoteDocumentWarning"));
                component.set("v.spinner",false);
            }else{
             	helper.requestDownload(component,event,helper);   
            }
            
            
        } 
    },
    
    handleClose : function(component,event){
    	$A.get("e.force:closeQuickAction").fire();
        component.find("overlayLib").notifyClose();
	}
})