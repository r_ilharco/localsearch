({
	showToast : function(component,event,title, message, type,mode) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": title,
                    "message": message,
                    "type": type,
                    "mode": mode
                });
                toastEvent.fire();
                
	},
    
        requestDownload: function(component, event){
        
        var action = component.get("c.requestDocumentDownload");
        
        action.setParams({
            recordId: component.get("v.recordId"),
            quoteId : component.get("v.simpleRecord.SBQQ__Quote__c"),
            envelopeId : component.get("v.simpleRecord.EnvelopeId__c")
        });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            var response = response.getReturnValue();
            if (state === "SUCCESS") {
               //console.log('requestDownload->response: ' +JSON.stringify(response));
                if(response.statusCode === 200){
                     this.showToast(component,event,$A.get("$Label.c.Namirial_UI_Message_Success"),$A.get("$Label.c.Namirial_UI_Message_Document_Downloaded"),'success','Sticky');   
               		
                }else{
                    this.showToast(component,event,$A.get("$Label.c.Namirial_UI_Message_Error"),$A.get("$Label.c.Namirial_UI_Message_ErrorOccured") + " " + ((response.statusCode === -1)?$A.get("$Label.c.Namirial_UI_Message_ExceptionThrown"):response.statusCode) + " - " + response.bodyResponse,'error','Sticky');   
                }
            }else{
                this.showToast(component,event,$A.get("$Label.c.Namirial_UI_Message_Error"),$A.get("$Label.c.Namirial_UI_Message_ErrorOccured_TryAgain"),'error','Sticky');
            }
            $A.get('e.force:refreshView').fire();
        	$A.get("e.force:closeQuickAction").fire();
			component.set("v.spinner",false);    
        });
       
        $A.enqueueAction(action);
    },
})