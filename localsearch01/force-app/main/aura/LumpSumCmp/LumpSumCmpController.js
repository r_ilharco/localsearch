({
    init: function (cmp, event, helper) {
        helper.getSubscription(cmp, event);
        /*cmp.set('v.columns', [
            {label: 'Subscription #', fieldName: 'opportunityName', type: 'text'},
            {label: 'Account name', fieldName: 'accountName', type: 'text'},
            {label: 'Remaining Amount to Bill', fieldName: 'amount', type: 'currency', typeAttributes: { currencyCode: 'EUR'}}
        ]);*/
        var columns;
        columns = [
            { type: 'url' , initialWidth: 200, fieldName: 'id', label: 'Subscription #', typeAttributes: {label: {fieldName: 'name' }} },
            { type: 'text', initialWidth: 200,fieldName: 'productName', label: 'Product Name' },
            { type: 'currency' ,initialWidth: 200, fieldName: 'remainingAmount', label: 'Remaining Amount', typeAttributes: { label: { fieldName: 'remainingAmount' } } }
        ];
        console.log('columns: ' + columns);
        cmp.set('v.columns', columns);
    },
    closeModal : function(component, event, helper) {
        console.log('closeModal');
		$A.get("e.force:closeQuickAction").fire();
    },
    onClickSaveHandler : function(component, event, helper) {
        helper.onClickSave(component, event);
    }
});