({
    getSubscription: function(component, event) {
        var action = component.get("c.getSubscriptionToBillByContractId");
        var recordId = component.get('v.recordId');
        console.log('recordId: ' +recordId);
        var params = {contractId:recordId};
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if(data.isSizeZero){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "warning",
                        "message": 'There are no subscriptions for lump request'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }else{
                    console.log('subscriptions -> ' + JSON.stringify(data.subscriptions));
                	component.set('v.data', data.subscriptions);
                	console.log('data -> ' + component.get('v.data'));
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    onClickSave: function(component, event) {
        var action = component.get("c.lumpRequestedUpdate");
        var selectedRows = component.find("partnerTable").getSelectedRows();
		console.log(JSON.stringify(selectedRows));
        console.log('selectedRows: ' +selectedRows);
        var params = {selectedRows:JSON.stringify(selectedRows)};
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Success"),
                    "type": "SUCCESS",
                    "message": $A.get("$Label.c.Success")
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        
        $A.enqueueAction(action);
    }
})
