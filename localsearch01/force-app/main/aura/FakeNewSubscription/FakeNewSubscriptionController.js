({
	init : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": $A.get("$Label.c.Manual_Subscription_Creation_Not_Allowed"),
            "type": "error"
        });
        toastEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
      }
})