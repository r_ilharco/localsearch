({
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        console.log(eventParams.changeType);
        if(eventParams.changeType === "LOADED") {
            if(component.get("v.simpleRecord.Billing_Profile__c")){
         		helper.redirectToRecord(component, event);     
            } else {
                helper.showToast(component, event);
            }
    	}
    }
    
})