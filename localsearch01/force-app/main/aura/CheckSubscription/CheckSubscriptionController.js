({ 
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
    },
    getSelectedRows: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedRowsList" ,event.getParam('selectedRows'));
    },
    
    handleTerminationClick: function(cmp, event, helper)
    {
        helper.selectToTerminate(cmp, event);
    },
    
    handleClickClose: function(cmp, event, helper)
    {
         console.log('handleClickClose');
         $A.get("e.force:closeQuickAction").fire();
         $A.get('e.force:refreshView').fire();
    },
    init: function (cmp, event, helper) {
        var columns = [
            { type: 'url' , fieldName: 'id', label: 'Subscription #', typeAttributes: { label: { fieldName: 'name' } } },
            { type: 'url' , fieldName: 'productId', label: 'Product Name', typeAttributes: { label: { fieldName: 'productName' } } },
            { type: 'text', fieldName: 'status', label: 'Status' },
            { type: 'url' , fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
            { type: 'date' , fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } }
          ];
            
        cmp.set('v.gridColumns', columns);
        /*A.L. Marotta 09-10-19 Start
         * SF2-424, Wave F, Sprint 11 - Call helper method to check for available subscriptions to replace*/
        helper.checkReplaceableSubscriptions(cmp,event);
        //A.L. Marotta 09-10-19 End
        helper.checkQuoteLine(cmp,event);
        helper.getSubscription(cmp,event);
        
        
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
})