({
    checkQuoteLine: function (component, event) {
        console.log('checkQuoteLine');
          var recordId = component.get('v.recordId');
          var action = component.get("c.checkQuoteLines");
          var params = { quoteId : recordId };
          action.setParams(params);
          action.setCallback(this, function(response) {
             var state = response.getState();
              if (state === "SUCCESS"){
                  component.set('v.hasQuoteLine', response.getReturnValue());
              }
              else{
                    var errors = response.getError();
                    if(errors){
                      //A.L. Marotta 02-01-20 - custom label as error message
                       //this.showToast(component,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));
                      this.showToast(component,event,'error', $A.get("{!$Label.c.CheckSubscription_error}"),$A.get("{!$Label.c.Error}"));
                      //A.L. Marotta 02-01-20 End
                    }
              }
         });
         $A.enqueueAction(action);
      },
      getSubscription: function(component, event) {
          console.log('getSubscription');
          var recordId = component.get('v.recordId');
          var action = component.get("c.getSubscriptionByContract");
          var params = { quoteId : recordId };
          action.setParams(params);
          action.setCallback(this, function(response) {
              var state = response.getState();
              if (state === "SUCCESS"){
                  var data = JSON.parse(response.getReturnValue());
                  console.log('VLAUDATO --> '+JSON.stringify(data))
                  if(data != null){
                      data.forEach(
                          function(element)
                          {
                              element.id = '/' + element.id;
                              element.productId = '/' + element.productId;
                              if(element.placeId){
                                  element.placeId = '/' + element.placeId;
                              }
                              if(typeof element._children != 'undefined')
                              {
                                  element._children.forEach(
                                      function(el){
                                          el.id = '/' + el.id;
                                          el.productId = '/' + el.productId;
                                          if(el.placeId){
                                              el.placeId = '/' + el.placeId;
                                          }
                                      }
                                  )
                              }
                          }
                      )
                      component.set('v.gridData', data);               
                      var expandedRows = [];
                      component.set('v.gridExpandedRows', expandedRows);
                  }
                  else{
                      this.showToast(component,event,'warning',$A.get("{!$Label.c.Quote_Upgrade_Downgrade}"),$A.get("{!$Label.c.Warning}"));
                  }
              }
          });
          $A.enqueueAction(action);
      },
      /*A.L. Marotta 09-10-19 Start
       * SF2-424, Wave F, Sprint 11 - extended action with third parameter, null if replacement
       * was chose by the user*/
      //selectToTerminate: function (component, event) {
      selectToTerminate: function (component, event, subId) {
          console.log('subToTerm '+subId);
          var that = this;
          /*var subscription = component.get('v.selectedRowsList');
          var subscriptionIds = [];
          for (var i = 0; i < subscription.length; i++) {
              subscriptionIds.push(subscription[i].id.replace('/',''));
          }*/
          var subscription;
          var subscriptionIds = [];
          
          if(subId){
              subscriptionIds.push(subId);
          }else{
              subscription = component.get('v.selectedRowsList');
              for (var i = 0; i < subscription.length; i++) {
                  subscriptionIds.push(subscription[i].id.replace('/',''));
              }
          }
          
          console.log('subscriptionIds ' + subscriptionIds);
          if(subscriptionIds && subscriptionIds.length > 0)
          {
                  var recordId = component.get('v.recordId');
                  var action = component.get("c.selectToterminate");
                  var data;
                  var params = { subIds : subscriptionIds, quoteId: recordId };
                  action.setParams(params);
                  action.setCallback(this, function(response) {
                      var state = response.getState();
                      if (state === "SUCCESS") {
                          data = response.getReturnValue();
                          console.log('data' + JSON.stringify(data));
                          if(data.trueOrFalse == true){
                              if(subId){
                                  this.showToast(component,event,'SUCCESS', $A.get("{!$Label.c.Single_subscription_replacement_success_message}"),'SUCCESS');             
                              }else{
                                  this.showToast(component,event,'SUCCESS', $A.get("{!$Label.c.Subscription_replacement_success_message}"),'SUCCESS');             
                              }
                          }else{
                              if(Object.getOwnPropertyNames(data).length > 0 
                                  && data.hasOwnProperty('warningMessages') && data['warningMessages'].length > 0)
                              {
                                  for(var i = data['warningMessages'].length - 1; i >= 0; i--)
                                      this.showToast(component,event,'WARNING', data['warningMessages'][i],'WARNING','sticky');
                              }else if(Object.getOwnPropertyNames(data).length > 0 
                                  && data.hasOwnProperty('errorMessage') && data['errorMessage'])
                                  this.showToast(component,event,'ERROR', data.errorMessage,'ERROR');
                          }
                           $A.get("e.force:closeQuickAction").fire();
                           $A.get('e.force:refreshView').fire();
                      } else {
                          var errors = response.getError();
                          console.log('errors' + errors);
                          if(errors){
                              //A.L. Marotta 02-01-20 - custom label as error message
                              //this.showToast(component,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));   
                              this.showToast(component,event,'error', $A.get("{!$Label.c.CheckSubscription_error}"),$A.get("{!$Label.c.Error}"));   
                          }
                           $A.get("e.force:closeQuickAction").fire();
                           $A.get('e.force:refreshView').fire();
                      }
                  });
             
              
              
              $A.enqueueAction(action);
          }else
               this.showToast(component,event,'WARNING', $A.get("{!$Label.c.CheckSubscription_error_SelectAtLeastOneSubscription}"),'WARNING','dismissible',false);
      },
      
      //A.L. Marotta 09-10-19 End
      
      showToast : function(component,event,type,message,title,mode, closeQuickAction){
          console.log(type+message+title);
          if(typeof mode == undefined) {
              mode = "dismissible";
          }
          
          if(typeof closeQuickAction == undefined) {
              closeQuickAction = true;
          }
          
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
              "title": title,
              "type": type,
              "message": message,
              "mode": mode
          });
          toastEvent.fire();
          if(closeQuickAction){
              $A.get("e.force:closeQuickAction").fire();
              $A.get('e.force:refreshView').fire();
          }
      },
      
      /*A.L. Marotta 09-10-19 Start
      * SF2-424, Wave F, Sprint 11 - check for replaceable subscriptions: if there's only one,
      * replace it automatically*/
      checkReplaceableSubscriptions : function(component, event){
          var recordId = component.get('v.recordId');
          var action = component.get("c.getReplaceableSubscriptions");
          var params = { quoteId : recordId };
          console.log('quote id: '+recordId);
          var subscriptionsData;
          action.setParams(params);
          action.setCallback(this, function(response) {
              var state = response.getState();
              console.log('callback state: '+state);
              if (state === "SUCCESS") {
                  subscriptionsData = response.getReturnValue();
              } else {
                  var errors = response.getError();
                  if(errors){
                      console.log('error');
                      //A.L. Marotta 02-01-20 - custom label as error message
                      //this.showToast(component,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));
                      this.showToast(component,event,'error', $A.get("{!$Label.c.CheckSubscription_error}"),$A.get("{!$Label.c.Error}"));    
                  }
              }
              
          console.log('subscriptions Data: '+subscriptionsData);
          if(subscriptionsData != null && subscriptionsData.length == 1){
              this.selectToTerminate(component, event, subscriptionsData[0]["Id"]);
          }
          });
          //$A.get("e.force:closeQuickAction").fire();
          $A.enqueueAction(action);
          
      }
      
      //A.L. Marotta 09-10-19 End
      
      
  })