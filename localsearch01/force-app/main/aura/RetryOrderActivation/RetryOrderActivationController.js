({
	doInit : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        console.log('RECORD ID --> '+recordId);
		helper.retryOrderActivation(recordId, component);
	}
})