({
    retryOrderActivation : function(recordId, component) {
        var action = component.get('c.retryOrderActivation');
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
             
            var objectId;
            if(state === "SUCCESS") {
                var resultValues = response.getReturnValue();
                if(resultValues){
                    resultValues = resultValues.split(";");
                    var result =resultValues[0];
                    if(resultValues.length>1)
                        objectId = resultValues[1];
                }
                if(result == 'OK'){
                    this.showToast('SUCCESS', 'success', $A.get("$Label.c.Retry_Order_Activation_Success"),objectId);
                    if(!objectId || objectId == 'null')
                        window.location.reload();
                }
                else if(result == 'KO'){
                    this.showToast('WARNING', 'warning', $A.get("$Label.c.Retry_Order_Activation_Bad_Status"),null);
                }
            }
            else{
                var errors = response.getError();
                if(errors){
                    if(errors[0]['message']){
                        var messages =errors[0]['message'].split(";");
                        var message = messages[0];
                        if(messages.length>1)
                            objectId = messages[1];
                        this.showToast('WARNING', 'warning',message,objectId);
                    }
                    else this.showToast('ERROR', 'error', $A.get("$Label.c.Generic_Error"), null);
                }
                else
                    this.showToast('ERROR', 'error', $A.get("$Label.c.Generic_Error"), null);
            }
        });
        $A.enqueueAction(action);
        $A.get("e.force:closeQuickAction").fire();
        
    },
    
    showToast : function(title, type, message, objectId){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": "Sticky"
        });
        if(objectId && objectId!= 'null'){
            toastEvent.setParams({
                "title":title,
                "message": message,
                "type": type,
                "mode": "Sticky",
                "messageTemplate":'{0}',
                "messageTemplateData": [{
                    url: 'https://'+window.location.hostname+'/'+objectId,
                    label: message +" "+$A.get("$Label.c.Click_here_for_related_record"),
                }]
            });   
        }
        toastEvent.fire();
    }                     
})