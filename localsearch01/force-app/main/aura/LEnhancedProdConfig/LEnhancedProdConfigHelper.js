({
    
    initConstants : function(component, cpqJSON){
        
        var inicConstants = component.get('c.initConstants');
        inicConstants.setStorable();
        inicConstants.setCallback(this, function(res){
            var state = res.getState();
            if(state === 'SUCCESS'){
                var constantsMap = res.getReturnValue();
                component.set('v.constants', constantsMap);
                this.initData(component, cpqJSON)
            }
        });
        $A.enqueueAction(inicConstants);
    },
    
    initData : function(component, cpqJSON) {
        var cpqObject = this.setExtId(cpqJSON);
        cpqJSON = JSON.stringify(cpqObject);
        component.set('v.cpqJSON', cpqJSON);
        var action = component.get('c.setDefaultData');
        action.setParams({'cpqJSON' : cpqJSON});
        
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state === 'SUCCESS'){
                var configDataWrapper = res.getReturnValue();
                component.set('v.configDataWrapper', configDataWrapper);
                
                if(configDataWrapper.masterProductCode == component.get('v.constants')['WebsiteFreePC'] ||
                   configDataWrapper.masterProductCode == component.get('v.constants')['CockpitFreePC'] ||
                   configDataWrapper.masterProductCode == component.get('v.constants')['LocalinaFreePC']){
                    component.set('v.disableBillingFrequency', true);
                }
                
                var printProducts = [component.get('v.constants')['LGSpalteProductCode'], 
                                     component.get('v.constants')['LGSpeziaProductCode'], 
                                     component.get('v.constants')['LGCSpalteProductCode'], 
                                     component.get('v.constants')['LGCSpeziaProductCode']
                                    ];
                
                var printSpaletninserate = [component.get('v.constants')['LGSpalteProductCode'],
                                           	component.get('v.constants')['LGCSpalteProductCode']
                                           ];

                var printSpezialinserate = [component.get('v.constants')['LGSpeziaProductCode'],
                                           	component.get('v.constants')['LGCSpeziaProductCode']
                                           ];
                
                var websiteProducts = [component.get('v.constants')['MyWebBasicProductCode'],
                                       component.get('v.constants')['MyWebProfProductCode'],
                                       component.get('v.constants')['MyWebStdProductCode']
                                      ];
                
                var myCampSocialProducts = [component.get('v.constants')['MyCSocialAwarePC'],
                                            component.get('v.constants')['MyCSocialBoosterPC'],
                                            component.get('v.constants')['MYCSocialClicksPC'],
                                            component.get('v.constants')['MYCSocialLeadsPC']
                                          ];
                
                if(printProducts.includes(configDataWrapper.masterProductCode)){
					if(configDataWrapper.startDatePrint) component.set('v.startDate', configDataWrapper.startDatePrint);
                    if(configDataWrapper.result && configDataWrapper.result.options){
                        if(printSpaletninserate.includes(configDataWrapper.masterProductCode)){
                            var optionList = configDataWrapper.result.options;
                            optionList.forEach(function(currentOption){
                                if(currentOption.isYellowPage){
                                    configDataWrapper.result.fields = currentOption.fieldsToShow;
                                    component.set('v.configDataWrapper', configDataWrapper);
                                }
                            });	
                        }
                        if(printSpezialinserate.includes(configDataWrapper.masterProductCode)){
                            var optionList = configDataWrapper.result.options;
                            optionList.forEach(function(currentOption){
                                if(currentOption.isCover){
                                    configDataWrapper.result.fields = currentOption.fieldsToShow;
                                    component.set('v.configDataWrapper', configDataWrapper);
                                }
                            });	
                        }
                    }
                    component.set('v.disableASAP', true);
                    component.set('v.disableStartDate', true);
                }
                else if(myCampSocialProducts.includes(configDataWrapper.masterProductCode)){
					component.set('v.disableASAP', true);
                    component.set('v.disableStartDate', true);                    
					component.set('v.startDate', configDataWrapper.startDateFromJSON);                    
                }
                else component.set('v.startDate', configDataWrapper.startDateFromJSON);
                if(configDataWrapper.allocationId){
                    component.set('v.enableDisableAssets', false);
                    component.set('v.disableLanguage',true);
                }
                
                if(configDataWrapper.quoteType == component.get('v.constants')['QuoteTypeRenewal'] && configDataWrapper.pickupNumber) component.set('v.pickNumber', configDataWrapper.pickupNumber);
                if(configDataWrapper.quoteType && configDataWrapper.quoteType == component.get('v.constants')['QuoteTypeAmend']) component.set('v.disabledFatherFields', true);
                
                var mapValOptions = new Map();
                mapValOptions = configDataWrapper.result.mapValuesOption;
                component.set('v.languages', mapValOptions['Language__c']);
                component.set('v.websiteLanguages', mapValOptions['Language_Website__c']);
                component.set('v.campaignLanguages', mapValOptions['Campaign_language__c']);
                component.set('v.listPlace', mapValOptions['Place__c']);
                component.set('v.listContact', mapValOptions['LBx_Contact__c']);
                component.set('v.listEditorial', mapValOptions['Editorial_Content__c']);
                component.set('v.listEdition', mapValOptions['Edition__c']);
                component.set('v.listCategory', mapValOptions['Category__c']);
                
                var cat = component.get('v.listCategory');
                //if(cat && cat.length === 1 && !configDataWrapper.result.fields.includes('Place__c')){
                if(cat && cat.length === 1 && !configDataWrapper.placeIdRequired){
                	component.set('v.warnMessage', $A.get("$Label.c.ENH_No_categories"));
                }
                
                component.set('v.listLocation', mapValOptions['Location__c']);
                var locations = component.get('v.listLocation');
                //if(locations && locations.length === 1 && !configDataWrapper.result.fields.includes('Place__c')){
                if(locations && locations.length === 1 && !configDataWrapper.placeIdRequired){
                    component.set('v.warnMessage', $A.get("$Label.c.ENH_No_locations"));
                }
                
                component.set('v.listTemplate', mapValOptions['Template__c']);
                
                this.setDefaultValues(component, configDataWrapper, cpqJSON);
                
                if(configDataWrapper.guidelineDescription){
                    component.set("v.guidelineDescription",configDataWrapper.guidelineDescription);
                }
                
                var map = new Map();
                map = configDataWrapper.result.mapValuesString;
                if(map){
                    if(map['Location__c']) component.set('v.listLocation', map['Location__c']);
                    var childLocations = component.get('v.listLocation');
                    //if(childLocations && childLocations.length === 1 && !configDataWrapper.result.fields.includes('Place__c')){
                    if(childLocations && childLocations.length === 1 && !configDataWrapper.placeIdRequired){    
                    	component.set('v.warnMessage', $A.get("$Label.c.ENH_No_locations"));
                	}
                    else if(childLocations && childLocations.length > 1){
						component.set('v.warnMessage', '');                        
                    }
                    if(map['Category__c']) component.set('v.listCategory', map['Category__c']);
                    var childCategories = component.get('v.listCategory');
                    //if(childCategories && childCategories.length === 1 && !configDataWrapper.result.fields.includes('Place__c')){
                    if(childCategories && childCategories.length === 1 && !configDataWrapper.placeIdRequired){
                    	component.set('v.warnMessage', $A.get("$Label.c.ENH_No_categories"));
                    }
                    else if(childCategories && childCategories.length > 1){
						component.set('v.warnMessage', '');                        
                    }
                }
                
                component.set('v.showSectionLocCat', true);
                component.set('v.showChildSectionLocCat', true);
                
                var searchPC = [component.get('v.constants')['SBBasicProductCode'],
                                component.get('v.constants')['SBStandardProductCode'],
                                component.get('v.constants')['SBStarterProductCode']
                               ];
                
                if(searchPC.includes(configDataWrapper.masterProductCode)){
                    component.set('v.isSearch', true);
                }
                this.createCmpMapOptions(component, configDataWrapper);
                //this.enableDisableSaveButton(component);
            }
            else{
                console.log('ERROR');
                var errors = res.getError();
                console.log(errors);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    setExtId : function(cpqJSON){
        var cpqObject = JSON.parse(cpqJSON);
        if(cpqObject.product && cpqObject.product.optionConfigurations){
            for(var feature in cpqObject.product.optionConfigurations){
                for(var options in cpqObject.product.optionConfigurations[feature]){
                    for(var currentOption in options){
                        var now = Date.now();
                        while (Date.now() - now < 3)  {
                        }
                        cpqObject.product.optionConfigurations[feature][options].configurationData['External_Configuration_Id__c'] = Date.now();                                     
                    }
                }
            }
        }        
        return cpqObject;
    },
    
    reloadLocations : function(component, placeId){
        
        var configDataWrapper = component.get('v.configDataWrapper');
        
        var action = component.get('c.reloadCatLoc');
        
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'placeId' : placeId
        });
        
        action.setCallback(this, function(res){
            component.set('v.warnMessage', '');
            var state = res.getState();
            if(state==='SUCCESS'){
                var configDataWrapper = res.getReturnValue();
                component.set('v.configDataWrapper', configDataWrapper);
                component.set('v.showChildSectionLocCat', true);
                if(configDataWrapper.placeOfOtherAccount){
                     component.set('v.warnMessage',$A.get("$Label.c.PlaceAgencyValidation"));
                }
                if(configDataWrapper.parentId != null && configDataWrapper.parentId){
                     component.set('v.warnMessage',$A.get("$Label.c.Place_selection_validation"));
                }
                var searchPC = [component.get('v.constants')['SBBasicProductCode'],
                                component.get('v.constants')['SBStandardProductCode'],
                                component.get('v.constants')['SBStarterProductCode'],
                                component.get('v.constants')['OnePresencePC']
                               ];
                if(searchPC.includes(configDataWrapper.masterProductCode) && component.get('v.selectedPlaceId')){
                   
                    var mapValOptions = new Map();
                    mapValOptions = configDataWrapper.result.mapValuesOption;
                    var cat = mapValOptions['Category__c'];
                    var locations = mapValOptions['Location__c'];
                    component.set('v.listCategory', mapValOptions['Category__c']);
                	component.set('v.listLocation', mapValOptions['Location__c']);
                    if(cat && cat.length === 1 && locations && locations.length === 1){
                        var error = $A.get("$Label.c.ENH_No_categories");
                        var errorfinal = error+' '+$A.get("$Label.c.ENH_No_locations");;
                        component.set('v.warnMessage', errorfinal);
                    }
                    else if(cat && cat.length === 1){
                        component.set('v.warnMessage', $A.get("$Label.c.ENH_No_categories"));
                    }
                    else if(locations && locations.length === 1){
                        component.set('v.warnMessage', $A.get("$Label.c.ENH_No_locations"));
                    }
                }
                else if(searchPC.includes(configDataWrapper.masterProductCode) && !component.get('v.selectedPlaceId')){
                    var mapValOptions = new Map();
                    mapValOptions = configDataWrapper.result.mapValuesOption;
                    var cat = mapValOptions['Category__c'];
                    var locations = mapValOptions['Location__c'];
                    component.set('v.listCategory', mapValOptions['Category__c']);
                	component.set('v.listLocation', mapValOptions['Location__c']);
                }
            }
            else{
                console.log('ERROR');
                var errors = res.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    createCmpMapOptions : function(component, configDataWrapper){
        var parsingWrapper = [];
        var optList = configDataWrapper.result.options;
        optList.forEach(function(option) {
            var map = new Map();
            map = option.mapValuesOption;
            Object.keys(map).forEach(function(key) {
                var individualElement = {};
                individualElement.key = key;
                individualElement.value = [];
                var currentValue = map[key];
                currentValue.forEach(function(currentItem) {
                    individualElement.value.push(currentItem);
                });
                parsingWrapper.push(individualElement);
            });
        });
        component.set('v.myMap', parsingWrapper);
    },
    
    createDate : function(dateMs){
    	var myDate = new Date(dateMs);
		let yyyy = myDate.getFullYear();
        let mm = ('0' + (myDate.getMonth() + 1)).slice(-2);
        let dd = ('0' + myDate.getDate()).slice(-2);        
		var dateToShow = yyyy+'-'+mm+'-'+dd;
        
        return dateToShow;
	},
 
    setDefaultValues : function(component, configDatawrapper, cpqJSON){
        var advOnline = [component.get('v.constants')['LBannerProductCode'],
                         component.get('v.constants')['LBannerSwissProductCode'],
                         component.get('v.constants')['LTopListingYProductCode'],
                         component.get('v.constants')['LTopListingWProductCode'],
                         component.get('v.constants')['LMobileFirstProductCode'],
                         component.get('v.constants')['SBannerProductCode']
						];
        var advPrint = [component.get('v.constants')['LGSpalteProductCode'],
                        component.get('v.constants')['LGSpeziaProductCode'],
                        component.get('v.constants')['LGCSpalteProductCode'],
                        component.get('v.constants')['LGCSpeziaProductCode']
						];
        
        if(advOnline.includes(configDatawrapper.masterProductCode)){
            var allocationId = configDatawrapper.allocationId;
            if(allocationId){
                component.set('v.changeConfiguration', true);
            }
            if(configDatawrapper.quoteType == 'Renewal'){
                component.set('v.enableDisableReservation', false);
            }
        }
        var cpqObject = JSON.parse(cpqJSON);
        
        if(!(configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeRenewal'] && advPrint.includes(configDatawrapper.masterProductCode))){
            //Default category
            if(configDatawrapper.defaultCategory){
                component.set('v.selectedCategory', configDatawrapper.defaultCategory);
            }
            //Default location
            if(configDatawrapper.defaultLocation){
                component.set('v.selectedLocation', configDatawrapper.defaultLocation);
            }
            //Default category ID
            if(configDatawrapper.defaultCategoryId){
                component.set('v.selectedCategoryId', configDatawrapper.defaultCategoryId);
            }
            else{
                if(configDatawrapper){
                    if(configDatawrapper.result){
                        if(configDatawrapper.result.mapValuesString){
                            if(configDatawrapper.result.mapValuesString.Category__c){
                                var arrayCategories = configDatawrapper.result.mapValuesString.Category__c;
                                if(arrayCategories){
                                    arrayCategories.forEach(cat => {
                                        if(cat.label == configDatawrapper.defaultCategory){
                                            component.set('v.selectedCategoryId', cat.value);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
            //Default location ID
            if(configDatawrapper.defaultLocationId){
                component.set('v.selectedLocationId', configDatawrapper.defaultLocationId);
            }
            else{
                if(configDatawrapper){
                    if(configDatawrapper.result){
                        if(configDatawrapper.result.mapValuesString){
                            if(configDatawrapper.result.mapValuesString.Location__c){
                                var arrayLocations = configDatawrapper.result.mapValuesString.Location__c;
                                if(arrayLocations){
                                    arrayLocations.forEach(loc => {
                                        if(loc.label == configDatawrapper.defaultLocation){
                                            component.set('v.selectedLocationId', loc.value);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
        //Default Subscription Term
        if(configDatawrapper.result.subterms){
            if(configDatawrapper.defaultSubscriptionTerm){
                  console.log('configDatawrapper.defaultSubscriptionTerm :: '+configDatawrapper.defaultSubscriptionTerm);
                  component.set('v.subTerm', configDatawrapper.defaultSubscriptionTerm);
            }else{
                var subTermValues = [];
                configDatawrapper.result.subterms.forEach(function(element) {
                    subTermValues.push(element.value);
                });
                component.set('v.subTerm',  Math.max(...subTermValues).toString());
       		}
        }
        
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Subscription_Term__c']){
                if(configDatawrapper.result.subterms && configDatawrapper.result.subterms.length > 0 && !configDatawrapper.result.subterms.includes(cpqObject.readOnly.line['Subscription_Term__c'])){
                    var subTermValues = [];
                    configDatawrapper.result.subterms.forEach(function(element) {
                        subTermValues.push(element.value);
                    });
                    component.set('v.subTerm',  Math.max(...subTermValues).toString());
                }else{
                    component.set('v.subTerm',  cpqObject.readOnly.line['Subscription_Term__c']);
                }
            }
        }
        //Default Billing Frequency
        if(configDatawrapper.result.billingFrequency){
            console.log('Defaulkt Billing Frequency');
            console.log('configDatawrapper.defaultBillingFrequency :::'+configDatawrapper.defaultBillingFrequency)
           if(configDatawrapper.defaultBillingFrequency){
                    console.log('configDatawrapper.defaultBillingFrequency :::'+configDatawrapper.defaultBillingFrequency)
                    component.set('v.billingFreq', configDatawrapper.defaultBillingFrequency);
           }else{
            if(configDatawrapper.result.billingFrequency.length > 1){
                component.set('v.billingFreq', '');
            }
            else{
                if(configDatawrapper.result.billingFrequency[0]){
                    var defaultBF = configDatawrapper.result.billingFrequency[0].value;
                	component.set('v.billingFreq', defaultBF);
                }
            }
        } 
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['SBQQ__BillingFrequency__c']){
                component.set('v.billingFreq',  cpqObject.readOnly.line['SBQQ__BillingFrequency__c']);
            }
        }
        //Default Language
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Language__c']){
                var langMap = new Map();
                langMap.set('Italian', 'it');
                langMap.set('English', 'en');
                langMap.set('German', 'de');
                langMap.set('French', 'fr');
                
                if(configDatawrapper.defaultEditionId){
                    var editionIdFromWrapper = configDatawrapper.defaultEditionId
                    var action = component.get('c.getEditionLanguages');
                    action.setParams({
                        'editionId' : editionIdFromWrapper
                    });
                    action.setCallback(this, function(res){
                        var state = res.getState();
                        if(state==='SUCCESS'){
                            var result = res.getReturnValue();
                            component.set('v.printLanguages', result);
                            component.set('v.printLanguage', langMap.get(cpqObject.readOnly.line['Language__c']));
                			component.set('v.disablePrintLanguage', true);
                            if(result.length == 2){
                                component.set('v.disablePrintLanguage', true);
                            }
                            else{
                                component.set('v.disablePrintLanguage', false);
                            }
                        }
                        else{
                            console.log('ERROR');
                        }
                    });
                    $A.enqueueAction(action);
                }
                component.set('v.language',  cpqObject.readOnly.line['Language__c']);
            }
        }
        else if(configDatawrapper.defaultLanguage){
            component.set('v.language',  configDatawrapper.defaultLanguage);
        }
        //Default Start Date
        if(cpqObject && cpqObject.readOnly ){
            var myCampSocialProducts = [component.get('v.constants')['MyCSocialAwarePC'],
                                        component.get('v.constants')['MyCSocialBoosterPC'],
                                        component.get('v.constants')['MYCSocialClicksPC'],
                                        component.get('v.constants')['MYCSocialLeadsPC']
                                       ];
            this.setMaxMinDate(component, configDatawrapper);
            var minDateToShow = component.get("v.minimumStartDate");
            if(cpqObject.readOnly.line && cpqObject.readOnly.line['SBQQ__StartDate__c']){
                if(startDateFromJson < minDateToShow) { 
                    component.set("v.startDate", minDateToShow);
                } else {
                    var startDateFromJson = cpqObject.readOnly.line['SBQQ__StartDate__c'];
                    if(configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeRenewal']){
                        component.set('v.asapChecked', false);
                        //	component.set('v.disableASAP', true);
                        component.set("v.minimumStartDate", startDateFromJson);
                        component.set("v.maxStartDate",startDateFromJson);
                    }
                    component.set("v.startDate",startDateFromJson);
                }
                
                if(myCampSocialProducts.includes(configDatawrapper.masterProductCode)){
                    component.set('v.disableASAP', true);
                    component.set('v.disableStartDate', true);
                    component.set('v.disableSubTerm', true);
                }
                if(!configDatawrapper.asapAllowed){
                    component.set('v.asapChecked', false);
                }
                component.set('v.disableASAP', !configDatawrapper.asapAllowed);
            } 
            else{
                if(myCampSocialProducts.includes(configDatawrapper.masterProductCode)){
                    component.set('v.disableASAP', true);
                    component.set('v.disableStartDate', true);
                    component.set('v.disableSubTerm', true);
                    component.set('v.startDate', minDateToShow);
                }
                else if(!configDatawrapper.asapAllowed){
                    component.set('v.asapChecked', false);
                    component.set('v.disableASAP', true);
                    var defaultStDate = component.get("v.startDate");
                    if(defaultStDate < minDateToShow) component.set("v.startDate", minDateToShow);
                }
                    else{
                        component.set('v.asapChecked', true);
                        component.set('v.disableASAP', false);
                        component.set('v.disableStartDate', true);
                        component.set('v.startDate', minDateToShow);
                    }
            }
            
            if(cpqObject.readOnly.line && cpqObject.readOnly.line['ASAP_Activation__c']){
                component.set('v.asapChecked', true);
                component.set('v.disableASAP', false);
                component.set('v.disableStartDate', true);
            }
        }
        //Default Place
        var selectedPlaceId;
        var selectedPlace;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Place__c){
                selectedPlaceId = cpqObject.readOnly.line.Place__c;
            }
        }
		else{
            selectedPlaceId = configDatawrapper.placeId;
        }
        selectedPlace = configDatawrapper.defaultPlaceName;
        console.log('Init method - Default Place Name: ' + selectedPlace);
        console.log('Init method - Default Place Id: ' + selectedPlaceId);
        if(selectedPlaceId) component.set('v.selectedPlaceId', selectedPlaceId);
		if(selectedPlace) component.set('v.selectedPlace', selectedPlace);
        component.set('v.showPlacePanel',true);
        //Default Contact
        var contactId;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line && cpqObject.readOnly.line.LBx_Contact__c){
        	contactId = cpqObject.readOnly.line.LBx_Contact__c;
        }
        else{
            contactId = configDatawrapper.defaultContact;
        }
        if(contactId){
            component.find('contactSelector').set('v.value', contactId);
        }
        //Default URLs
        var url;
        var url2;
        var url3;
        var url4;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Url__c){
                url = cpqObject.readOnly.line.Url__c;
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Url2__c){
                url2 = cpqObject.readOnly.line.Url2__c;
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Url3__c){
                url3 = cpqObject.readOnly.line.Url3__c;
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Url4__c){
                url4 = cpqObject.readOnly.line.Url4__c;
            }
        }
        if(url) component.find('url').set('v.value', url);
        if(url2) component.find('url2').set('v.value', url2);
        if(url3) component.find('url3').set('v.value', url3);
        if(url4) component.find('url4').set('v.value', url4);
        //Default Campaign Id
        var campaignId;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Campaign_Id__c){
                campaignId = cpqObject.readOnly.line.Campaign_Id__c;
            }
        }
        if(campaignId && !(cpqObject.quote.SBQQ__Type__c == 'Renewal')){
            component.find('campaignId').set('v.value', campaignId);
            component.set('v.disableSubTerm',true);
            component.set('v.disableASAP',true);
            //component.set('v.disableCampaignId',true);
            this.validateCampaign(component,campaignId);
        }
        //Default Edition
        var editionId;
        var edition;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line.Edition__c && !(cpqObject.quote.SBQQ__Type__c === 'Renewal')){
                editionId = cpqObject.readOnly.line.Edition__c;
            }
            if(configDatawrapper.defaultEditionId && ! (configDatawrapper.quoteType === 'Renewal')){
                editionId = configDatawrapper.defaultEditionId;
            }
        }
        else if(configDatawrapper.defaultEditionId && !(configDatawrapper.quoteType === 'Renewal')){
            editionId = configDatawrapper.defaultEditionId;
        }

        if(editionId) component.set('v.selectedEditionId', editionId);

        if(configDatawrapper.defaultEditionName && !(configDatawrapper.quoteType === 'Renewal')){
            component.set('v.selectedEdition', configDatawrapper.defaultEditionName);  
        }
        component.set('v.showEditionPanel',true);
        
        //Setting default Keyword and Description and weblanguage and template
        var desc;
        var keywords;
        var placementPhone;
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Placement_Phone__c']){
                placementPhone = cpqObject.readOnly.line['Placement_Phone__c'];
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['CDescription__c']){
                desc = cpqObject.readOnly.line['CDescription__c'];
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Keywords__c']){
                keywords = cpqObject.readOnly.line['Keywords__c'];
            }
        }
        else if(configDatawrapper.defaultKeywords){
            keywords = configDatawrapper.defaultKeywords;  
        }
        component.set('v.description',desc);
        component.set('v.keywords',keywords);  
        component.set('v.placementPhone',placementPhone);
        
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Website_Language__c']){
                component.set('v.websiteLanguage',  cpqObject.readOnly.line['Website_Language__c']);
            }
        }
        if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Template__c']){
                component.set('v.selectedTemplate',  cpqObject.readOnly.line['Template__c']);
            }
        }
		if(cpqObject && cpqObject.readOnly && cpqObject.readOnly.line){
            if(cpqObject.readOnly.line['Editorial_Content__c']){
                component.find('editorial').set('v.value', cpqObject.readOnly.line['Editorial_Content__c']);
            }
        }  
        //
        if(configDatawrapper){
            if(configDatawrapper.Target_Url){
                component.set('v.Target_URL__c', configDatawrapper.Target_Url);
            }
            if(configDatawrapper.Campaign_language){
                component.set('v.campaignLanguage', configDatawrapper.Campaign_language);
            }
            if(configDatawrapper.Company_location){
                component.set('v.Company_location__c', configDatawrapper.Company_location);
            }
            if(configDatawrapper.Comment_field){
                component.set('v.Comment_field__c', configDatawrapper.Comment_field);
            }
        }
    },
    
    gotULink : function(component){
        var urlGotU;
        
        var enhancedConfigData = component.get('v.configDataWrapper');
        var action = component.get('c.urlGotU');
        action.setParams({'enhancedConfigData' : enhancedConfigData});
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var returnString = res.getReturnValue();
                if(returnString === 'MISSEMAIL'){
                    component.set('v.warnMessage', $A.get("$Label.c.ENH_MyCamp_Missing_Email"));
                }
                else if(returnString === 'MISSCN'){
                    component.set('v.warnMessage', $A.get("$Label.c.ENH_MyCamp_Missing_Customer_Number"));
                }
				else if(returnString === 'MISSACC'){
					component.set('v.warnMessage', $A.get("$Label.c.ENH_MyCamp_Missing_Account"));
				}
				else{
					component.set('v.warnMessage', '');
                    window.open(returnString,'_target');
				}
            }
            else{
                component.set('v.warnMessage', $A.get("$Label.c.Generic_Error"));
            }
        });
        
        $A.enqueueAction(action);
    },
    
    setConfigurationData : function(component, event){
        var cpqJSON = component.get('v.cpqJSON');
        var cpqObject = JSON.parse(cpqJSON);
        var contact;
        var url;
        var url2;
        var url3;
        var url4;
        var campaignId;
        var editorial;
        var template;
        var printLanguage;
        
        var startDate = component.get('v.startDate');
        var subscriptionTerm = component.get('v.subTerm');
        var billingFreq = component.get('v.billingFreq');
        var language = component.get('v.language');
        var asapChecked = component.get('v.asapChecked');
        if(component.find('contactSelector')){
            contact = component.find('contactSelector').get('v.value');
        }
        if(component.find('url')){
            url = component.find('url').get('v.value');
        }
        if(component.find('url2')){
            url2 = component.find('url2').get('v.value');
        }
        if(component.find('url3')){
            url3 = component.find('url3').get('v.value');
        }
        if(component.find('url4')){
            url4 = component.find('url4').get('v.value');
        }
        if(component.find('campaignId')){
            campaignId = component.find('campaignId').get('v.value');
        }
        if(component.find('editorial')){
            editorial = component.find('editorial').get('v.value');
        }
        if(component.find('templateSelect')){
            template = component.find('templateSelect').get('v.value');
        }
        if(component.find('printLanguageSelect')){
            var languageCode = component.find('printLanguageSelect').get('v.value');
            if(languageCode){
                var langMap = new Map();
                langMap.set('it', 'Italian');
                langMap.set('en', 'English');
                langMap.set('de', 'German');
                langMap.set('fr', 'French');
                printLanguage = langMap.get(languageCode);
            }
        }
        
        if(startDate){
            cpqObject.product.configurationData['SBQQ__StartDate__c'] = startDate;
            for(var feature in cpqObject.product.optionConfigurations){
                for(var options in cpqObject.product.optionConfigurations[feature]){
                    for(var currentOption in options){
                        if(cpqObject.product.optionConfigurations[feature][options]){
                            cpqObject.product.optionConfigurations[feature][options].configurationData['SBQQ__StartDate__c'] = startDate;
                        }
                    }
                }
            }
        }
        if(subscriptionTerm){
            cpqObject.product.configurationData['Subscription_Term__c'] = subscriptionTerm;
            for(var feature in cpqObject.product.optionConfigurations){
                for(var options in cpqObject.product.optionConfigurations[feature]){
                    for(var currentOption in options){
                        if(cpqObject.product.optionConfigurations[feature][options]){
                            cpqObject.product.optionConfigurations[feature][options].configurationData['Subscription_Term__c'] = subscriptionTerm;
                        }
                    }
                }
            }
        }
        if(billingFreq){
            cpqObject.product.configurationData['SBQQ__BillingFrequency__c'] = billingFreq;
            for(var feature in cpqObject.product.optionConfigurations){
                for(var options in cpqObject.product.optionConfigurations[feature]){
                    for(var currentOption in options){
                        if(cpqObject.product.optionConfigurations[feature][options]){
                            cpqObject.product.optionConfigurations[feature][options].configurationData['SBQQ__BillingFrequency__c'] = billingFreq;
                        }
                    }
                }
            }
        } 
        //
        if(component.find('Target_URL')){
        	cpqObject.product.configurationData['Target_URL__c'] = component.get("v.Target_URL__c");
        }
        if(component.find('Company_location')){
        	cpqObject.product.configurationData['Company_location__c'] = component.get("v.Company_location__c");
        }
        if(component.find('Comment_field')){
        	cpqObject.product.configurationData['Comment_field__c'] = component.get("v.Comment_field__c");
        }
        if(component.find('campaignLanguageSelect')){
        	cpqObject.product.configurationData['Campaign_language__c'] = component.get("v.campaignLanguage");
        }
        //
        
        cpqObject.product.configurationData['Place__c'] = component.get('v.selectedPlaceId') == undefined?'':component.get('v.selectedPlaceId');
        cpqObject.product.configurationData['ASAP_Activation__c'] = asapChecked;
        if(language) cpqObject.product.configurationData['Language__c'] = language;
        if(printLanguage) cpqObject.product.configurationData['Language__c'] = printLanguage;
        if(contact) cpqObject.product.configurationData['LBx_Contact__c'] = contact;
        if(url) cpqObject.product.configurationData['Url__c'] = url;
        if(url2) cpqObject.product.configurationData['Url2__c'] = url2;
        if(url3) cpqObject.product.configurationData['Url3__c'] = url3;
        if(url4) cpqObject.product.configurationData['Url4__c'] = url4;
        if(campaignId) cpqObject.product.configurationData['Campaign_Id__c'] = campaignId;
        if(editorial) cpqObject.product.configurationData['Editorial_Content__c'] = editorial;
        if(template) cpqObject.product.configurationData['Template__c'] = template;
       	var customId = component.get('v.casaExtId');
        if(customId){
            cpqObject.product.configurationData['Master__c'] = customId;
            for(var feature in cpqObject.product.optionConfigurations){
                for(var options in cpqObject.product.optionConfigurations[feature]){
                    for(var currentOption in options){
                        if(cpqObject.product.optionConfigurations[feature][options]){
                            cpqObject.product.optionConfigurations[feature][options].configurationData['MasterCode__c'] = customId;
                        }
                    }
                }
            }
        }
        //this.setConfigurationDataHelper(component, customId, 'Master__c', null, true);
        var newCpqJSON = JSON.stringify(cpqObject);
        component.set('v.cpqJSON', newCpqJSON);
        
        this.retrieveCategoryLocation(component, cpqObject);
        this.sendDataVFPage(component,event);
    },
    
    setConfigurationDataHelper : function(component, value, field, myKey, isMasterProduct){
        var cpqJSON = component.get('v.cpqJSON');
        var cpqObject = JSON.parse(cpqJSON);
        
        if(field){
            if(value){
                if(isMasterProduct){
                    cpqObject.product.configurationData[field] = value;
                    if(field == 'Master__c'){
                        cpqObject.product.configurationData['Master__c'] = value;
                        for(var feature in cpqObject.product.optionConfigurations){
                            for(var options in cpqObject.product.optionConfigurations[feature]){
                                for(var currentOption in options){
                                    if(cpqObject.product.optionConfigurations[feature][options]){
                                        cpqObject.product.optionConfigurations[feature][options].configurationData['MasterCode__c'] = value;
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    for(var feature in cpqObject.product.optionConfigurations){
                        for(var options in cpqObject.product.optionConfigurations[feature]){
                            for(var currentOption in options){
                                if(cpqObject.product.optionConfigurations[feature][options]){
                                    if(cpqObject.product.optionConfigurations[feature][options].configurationData.External_Configuration_Id__c == myKey){
                                        if(field=='Location__c'){
                                            var finalVal = value;
                                        	if(value.includes($A.get("$Label.c.ENH_PlaceRelated_Label"))) finalVal = value.replace($A.get("$Label.c.ENH_PlaceRelated_Label"), '');
                                            cpqObject.product.optionConfigurations[feature][options].configurationData['Location__c'] = finalVal;
                                        }
                                        if(field=='LocationID__c') cpqObject.product.optionConfigurations[feature][options].configurationData['LocationID__c'] = value;
                                        if(field=='Category__c') cpqObject.product.optionConfigurations[feature][options].configurationData['Category__c'] = value;
                                        if(field=='CategoryID__c') cpqObject.product.optionConfigurations[feature][options].configurationData['CategoryID__c'] = value;
                                        if(field=='Integration_List_Price__c') cpqObject.product.optionConfigurations[feature][options].configurationData['Integration_List_Price__c'] = value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        var newCpqJSON = JSON.stringify(cpqObject);
        component.set('v.cpqJSON', newCpqJSON);
    },
    
    retrieveCategoryLocation : function(component, cpqObject){
        var categoryId = component.get('v.selectedCategoryId');
        var locationId = component.get('v.selectedLocationId');
        var category = component.get('v.selectedCategory');
        var location = component.get('v.selectedLocation');
        var editionId = component.get('v.selectedEditionId');
        var placeId = component.get('v.selectedPlaceId');
        
        if(categoryId) this.setConfigurationDataHelper(component, categoryId, 'CategoryID__c', null, true);
        if(locationId) this.setConfigurationDataHelper(component, locationId, 'LocationID__c', null, true);
        if(category) this.setConfigurationDataHelper(component, category, 'Category__c', null, true);
        if(location){
            if(location.includes($A.get("$Label.c.ENH_PlaceRelated_Label"))){
                location.replace($A.get("$Label.c.ENH_PlaceRelated_Label"), '');
                location.trim();
            }
            this.setConfigurationDataHelper(component, location, 'Location__c', null, true);
        }
        if(editionId) this.setConfigurationDataHelper(component, editionId, 'Edition__c', null, true);
        if(placeId) this.setConfigurationDataHelper(component, placeId, 'Place__c', null, true);
        if(component.find('pickupNumber')){
            var pickupNumber = component.find('pickupNumber').get('v.value');
        	this.setConfigurationDataHelper(component, pickupNumber, 'Pickup_Number_Text__c', null, true);
        }
    },
    
    sendDataVFPage : function(component, event) {
        var cpqJSON = component.get('v.cpqJSON');
        var cpqObject = JSON.parse(cpqJSON);
        
        if(cpqObject){
            var myEvent = $A.get("e.c:EnhancedConfigLightning2VFJson");
            myEvent.setParams({
                cpqJSON: cpqObject
            });
            myEvent.fire();
        }
    },
    
    sendApplicationEvent : function(component, event){
        var appEvent = $A.get('e.c:ApplicationEventEnhanced');
        appEvent.setParams({ 'message' : 'date' });
        appEvent.fire();
    },
    
    getEditionLanguage : function(component, editionId){
        var configDataWrapper = component.get('v.configDataWrapper');
        if(configDataWrapper.result && configDataWrapper.result.mapValuesOption){
            if(configDataWrapper.result.mapValuesOption.Category__c){
               	configDataWrapper.result.mapValuesOption.Category__c = []; 
            }
            if(configDataWrapper.result.mapValuesOption.Location__c){
               	configDataWrapper.result.mapValuesOption.Location__c = []; 
            }
            if(configDataWrapper.result.mapValuesOption.Editorial_Content__c){
               	configDataWrapper.result.mapValuesOption.Editorial_Content__c = [];
                component.set('v.listEditorial', []);
            }
        }
        component.set('v.configDataWrapper', configDataWrapper);
        
        var action = component.get('c.getEditionLanguages');
        action.setParams({
            'editionId' : editionId
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                component.set('v.printLanguages', result);
                if(result.length == 2){
                    component.set('v.disablePrintLanguage', true);
                    component.set('v.printLanguage', result[1].value);
                    this.reloadCategoryEditionLanguage(component, editionId);
                }
                else{
                    component.set('v.disablePrintLanguage', false);
                    component.set('v.printLanguage', '');
                }
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    
    reloadCategoryEditionLanguage : function(component, editionId){
        var configDataWrapper = component.get('v.configDataWrapper');
        //console.log('CONFIG DATA WRAPPER '+JSON.stringify(configDataWrapper));
        if(configDataWrapper.result && configDataWrapper.result.mapValuesOption){
            if(configDataWrapper.result.mapValuesOption.Category__c){
               configDataWrapper.result.mapValuesOption.Category__c = []; 
            }
            if(configDataWrapper.result.mapValuesOption.Location__c){
               configDataWrapper.result.mapValuesOption.Location__c = []; 
            }
            if(configDataWrapper.result.mapValuesOption.Editorial_Content__c){
               	configDataWrapper.result.mapValuesOption.Editorial_Content__c = [];
                component.set('v.listEditorial', []);
            }
        }
        var action = component.get('c.reloadCategoryEditionLanguage');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'editionId' : editionId
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                component.set('v.configDataWrapper', result);
                configDataWrapper = component.get('v.configDataWrapper');
                var listCat = configDataWrapper.result.mapValuesOption.Category__c;
                var listLoc = configDataWrapper.result.mapValuesOption.Location__c;
                var listCatEditorial = configDataWrapper.result.mapValuesOption.Editorial_Content__c;
                component.set('v.listCategory',listCat);
                component.set('v.listLocation',listLoc);
                component.set('v.listEditorial',listCatEditorial)
                component.set('v.startDate',configDataWrapper.startDatePrint);
                var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                appEvent.setParams({ 'message' : 'categoryLanguage' });
                appEvent.fire();
                
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    
    changeSelectedCasaCategory : function(component, returnedCategory, listCategory){
        var returnedCategoryLabel;
        if(listCategory){
            listCategory.forEach(function(currentCategory){
                if(currentCategory.value == returnedCategory){
                    returnedCategoryLabel = currentCategory.label;
                }
            });
        }
        //Send Application Event
        var appEvent = $A.get('e.c:ApplicationEventEnhanced');
        appEvent.setParams({'message' : 'casaCat',
                            'valueId' : returnedCategory,
                            'valueLabel' : returnedCategoryLabel,
                            'languageChange' : false
                           });
        appEvent.fire();
        
        component.set('v.selectedCategory', returnedCategoryLabel);
        component.set('v.selectedCategoryId', returnedCategory);
    },
    
    changeSelectedCasaLocation : function(component, returnedLocation, listLocation){
        var returnedLocationLabel;
        if(listLocation){
            listLocation.forEach(function(currentLocation){
                if(currentLocation.value == returnedLocation){
                    returnedLocationLabel = currentLocation.label;
                }
            });
        }
        //Send Application Event
        var appEvent = $A.get('e.c:ApplicationEventEnhanced');
        appEvent.setParams({'message' : 'casaLoc',
                            'valueId' : returnedLocation,
                            'valueLabel' : returnedLocationLabel
                           });
        appEvent.fire();
        
        component.set('v.selectedLocation', returnedLocationLabel);
        component.set('v.selectedLocationId', returnedLocation);
    },
    
    enableDisableSaveButton : function(component){
        var advOnline = [component.get('v.constants')['LBannerProductCode'],
                         component.get('v.constants')['LBannerSwissProductCode'],
                         component.get('v.constants')['LTopListingYProductCode'],
                         component.get('v.constants')['LTopListingWProductCode'],
                         component.get('v.constants')['LMobileFirstProductCode'],
                         component.get('v.constants')['SBannerProductCode']
						];
        var productWithCampaignValidation = [component.get('v.constants')['MyCSocialAwarePC'],
                                             component.get('v.constants')['MYCSocialClicksPC'],
                                             component.get('v.constants')['MYCSocialLeadsPC']
											];
        
        var configDataWrapper = component.get('v.configDataWrapper');
        var startDate = component.get('v.startDate');
        var subTerm = component.get('v.subTerm');
        var billingFreq = component.get('v.billingFreq');
        var isBillingFreqOk = component.get('v.isBillingFreqOk');
        var allocationSuccess = component.get('v.allocationSuccess');
        if(!allocationSuccess && configDataWrapper.allocationId) allocationSuccess = true;
        var fieldsToCheck = '';
        component.set('v.confirmMessage',"");
        component.set('v.warnMessage',"");
        component.set('v.errorMessage',"");
        var errorMessage = $A.get("$Label.c.ENH_Missing_Information");
       	var proceed = false;
        
        if(configDataWrapper.result.options){
            var listOption = configDataWrapper.result.options;
            listOption.forEach(function(currentOption){
                if(currentOption.mandatoryFields){
                    fieldsToCheck += currentOption.mandatoryFields;
                }
            });              
        }
        if(configDataWrapper.result && configDataWrapper.result.mandatoryFields){
            fieldsToCheck += configDataWrapper.result.mandatoryFields;
        }
        var fieldsToCheckFinal = fieldsToCheck;
        if(fieldsToCheck){
            if(fieldsToCheck.includes('Language_Website__c')){
                let languages = configDataWrapper.result.mapValuesOption['Language_Website__c'];
                if(languages){
                    if(languages.length == 1){
                        fieldsToCheckFinal = fieldsToCheck.replace("Language_Website__c;", "");
                    }
                }
            }
            var splitted = fieldsToCheckFinal.split(";");
            console.log('Mandatory fields: ' + JSON.stringify(splitted));
            var splittedSize = splitted.length;
            var count = 0;
            if(configDataWrapper.result){
                if(configDataWrapper.result.options){
                    var optionsArray = configDataWrapper.result.options;
                    optionsArray.forEach(option => {
                        if(option.mandatoryFields){
                            if(option.mandatoryFields.includes('Location__c')){
                                if(option.selectedLocationId){
                        			console.log('Selected location on option: ' + option.selectedLocationId);
                                    count++;
                                }
                            }
                            else if(option.mandatoryFields.includes('Category__c')){
                                if(option.selectedCategoryId){
                                    count++;
                                }
                            }
                        }
                    });
                }
            }
            splitted.forEach(function(currentVal){
                if(currentVal.includes('Category__c')){
                    var selectedCategoryId = component.get('v.selectedCategoryId');
                    if(selectedCategoryId) count++;
   					else  errorMessage +=  ' Category,';
                } else if(currentVal.includes('Location__c')){
                    var selectedLocationId = component.get('v.selectedLocationId');
                    if(selectedLocationId) count++;
        					else  errorMessage +=  ' Location,';

                } else if(currentVal.includes('Url__c')){
                    var url = component.get('v.url');
                    if(url) count++;
                    else  errorMessage +=  ' Url,';
                } else if(currentVal.includes('Url2__c')){
                    var url = component.get('v.url2');
                    if(url) count++;
                    else  errorMessage +=  ' Url,';
                } else if(currentVal.includes('Url3__c')){
                    var url = component.get('v.url3');
                    if(url) count++;
                    else  errorMessage +=  ' Url,';
                } else if(currentVal.includes('Url4__c')){
                    var url = component.get('v.url4');
                    if(url) count++;
                    else  errorMessage +=  ' Url,';
                } else if(currentVal.includes('Language__c')){
                    var language = component.get('v.language');
                    if(language) count++;
                    else  errorMessage +=  ' Language,';
                } else if(currentVal.includes('LBx_Contact__c')){
                    var contact = component.find('contactSelector').get('v.value');
                    if(contact) count++;
                    else  errorMessage +=  ' Contact,';
                } else if(currentVal.includes('Edition__c')){
                    var selectedEditionId = component.get('v.selectedEditionId');
                    if(selectedEditionId) count++;
                    else  errorMessage +=  ' Edition,';
                } else if(currentVal.includes('Campaign_Id__c')){
                    var campaignId = component.find('campaignId').get('v.value');
                    if(campaignId) count++;
                    else  errorMessage +=  ' Campaign,';
                } else if(currentVal.includes('Editorial_Content__c')){
                    var editorial = component.find('editorial').get('v.value');
                    if(editorial) count++;
                    else  errorMessage +=  ' Editorial Content,';
                } else if(currentVal.includes('Template__c')){
                    var template = component.find('templateSelect').get('v.value');
                    if(template) count++;
                    else  errorMessage +=  ' Template,';
                } else if(currentVal.includes('Language_Website__c')){
                    var webLanguage = component.find('websiteLanguageSelect').get('v.value');
                    if(webLanguage) count++;
                    else  errorMessage +=  ' Language Website,';
                } else if(currentVal.includes('Keywords__c')){
                    var keywords = component.find('keywords').get('v.value');
                    if(keywords) count++;
                    else  errorMessage +=  ' Keywords,';
                } else if(currentVal.includes('Description__c')){
                    var description = component.find('description').get('v.value');
                    if(description) count++;
                     else  errorMessage +=  ' Description,';
                } else if(currentVal.includes('Placement_Phone__c')){
                    var placementPhone = component.find('placementPhone').get('v.value');
                    if(placementPhone) count++;
                     else  errorMessage +=  ' Placement Phone,';
                } else if(currentVal.includes('SBQQ__BillingFrequency__c')){
                    if(billingFreq && isBillingFreqOk) count++;
                    else  errorMessage +=  ' Billing Frequency,';
                } else if(currentVal.includes('Simulation_Number__c')){
                    var simNumber = component.find('simNumber').get('v.value');
                    if(simNumber) count++;
                    else  errorMessage +=  ' Simulation Number,';
                } else if(currentVal.includes('Target_URL__c')){
                    var simNumber = component.find('Target_URL').get('v.value');
                    if(simNumber) count++;
                    else  errorMessage +=  ' '+$A.get("$Label.c.ENH_Target_URL")+',';
                } else if(currentVal.includes('Company_location__c')){
                    var simNumber = component.find('Company_location').get('v.value');
                    if(simNumber) count++;
                    else  errorMessage +=  ' '+$A.get("$Label.c.ENH_Company_location")+',';
                } else if(currentVal.includes('Campaign_language__c')){
                    var simNumber = component.find('campaignLanguageSelect').get('v.value');
                    if(simNumber) count++;
                    else  errorMessage +=  ' '+$A.get("$Label.c.ENH_Campaign_language")+',';
                } else if(currentVal.includes('Comment_field__c')){
                    var simNumber = component.find('Comment_field').get('v.value');
                    if(simNumber) count++;
                    else  errorMessage +=  ' '+$A.get("$Label.c.ENH_Comment_field")+',';
                }

            });
			if(configDataWrapper.placeIdRequired){
                console.log('The current product is Place Required');
                splittedSize = splittedSize+1;
                var selectedPlaceId = component.get('v.selectedPlaceId');
                console.log('Selected Place Id: ' + selectedPlaceId);
                if(selectedPlaceId) count++;
                else  errorMessage +=  ' Place,';
            }
            if(splittedSize-1 === count && !advOnline.includes(configDataWrapper.masterProductCode)) component.set('v.enableDisableSave', false);
            else if(splittedSize-1 === count && advOnline.includes(configDataWrapper.masterProductCode) && allocationSuccess) component.set('v.enableDisableSave', false);
                else{
                    if( splittedSize-1 === count && advOnline.includes(configDataWrapper.masterProductCode) && !allocationSuccess) errorMessage +=  ' Reservation,';
                	component.set('v.enableDisableSave', true);
            }
        }
        else{
            if(configDataWrapper.placeIdRequired){
                var selectedPlaceId = component.get('v.selectedPlaceId');
                if(configDataWrapper.masterProductCode == component.get('v.constants')['LocalinaFreePC'] 
                   || configDataWrapper.masterProductCode == component.get('v.constants')['MyWebFreePC']
                   || configDataWrapper.masterProductCode == component.get('v.constants')['CockpitFreePC']){
                	
                    if(startDate && subTerm && selectedPlaceId) component.set('v.enableDisableSave', false);
                    else {
                        if(!startDate) errorMessage +=  ' Start Date,';
                        if(!subTerm) errorMessage += ' Subscription Term,';
                        if(!selectedPlaceId) errorMessage += ' Place,';
                        component.set('v.enableDisableSave', true);
                    }
            	}
                else{
                    if(startDate && subTerm && billingFreq && selectedPlaceId) component.set('v.enableDisableSave', false);
                	else {
                        if(!startDate) errorMessage +=  ' Start Date,';
                        if(!subTerm) errorMessage += ' Subscription Term,';
                        if(!billingFreq) errorMessage += ' Billing Frequency,';
                        if(!selectedPlaceId) errorMessage += ' Place,';
                        console.log('errorMessage :: '+errorMessage);
                        component.set('v.enableDisableSave', true);
                    }
                }
            }
            else{
                if(configDataWrapper.masterProductCode == component.get('v.constants')['WebsiteFreePC'] ||
                   configDataWrapper.masterProductCode == component.get('v.constants')['CockpitFreePC']){
                    if(startDate && subTerm) component.set('v.enableDisableSave', false);
                    else{
                        if(!startDate) errorMessage +=  ' Start Date,';
                    	if(!subTerm) errorMessage += ' Subscription Term,';
                       component.set('v.enableDisableSave', true); 
                    } 
                }
                else if(startDate && subTerm && billingFreq) component.set('v.enableDisableSave', false);
                else {
                   
                    if(!startDate) errorMessage +=  ' Start Date,';
                    if(!subTerm) errorMessage += ' Subscription Term,';
                    if(!billingFreq) errorMessage += ' Billing Frequency,';
                    console.log('errorMessage :: '+errorMessage);
           
                    component.set('v.enableDisableSave', true);
                }
            }
        }
        
        if(productWithCampaignValidation.includes(configDataWrapper.masterProductCode)){
            var invalidCampaign = component.get('v.invalidCampaign');
            if(invalidCampaign){
                    errorMessage +=  ' Valid Campaign,';
                    component.set('v.enableDisableSave', true);
            }
        }

        console.log('enableDisableSave :: '+component.get('v.enableDisableSave')) ;   
        if(component.get('v.enableDisableSave') && errorMessage.charAt(errorMessage.length-1)==","){
            
            errorMessage = errorMessage.replace(/.$/,".");
            console.log('errorMessage :: ' +errorMessage);
            component.set('v.errorMessage',errorMessage);
            proceed = false;
            
            
            
        }else if(!component.get('v.enableDisableSave')){
            proceed = true;
        }
        return proceed;

    },
        
    enableDisableAllocationButton : function(component){
        var configDataWrapper = component.get('v.configDataWrapper');
        var language = component.get('v.language');
        var category = component.get('v.selectedCategoryId');
        var location = component.get('v.selectedLocationId');
        
        var productCode = configDataWrapper.masterProductCode;
        if(productCode && productCode == 'LBANNERSWISS001' && language && category) component.set('v.enableDisableReservation', false);
        if(language && category && location) component.set('v.enableDisableReservation', false);
        
    },
        
    reloadPlaceList : function(component){
        console.log('reloadPlaceList');
        var configDataWrapper = component.get('v.configDataWrapper');
        var input = document.getElementsByClassName("slds-has-focus")[0].value;        
        var action = component.get('c.reloadPlaceforAgency');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'input' : input
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                component.set('v.configDataWrapper', result);
                configDataWrapper = component.get('v.configDataWrapper');
                var listPlace = configDataWrapper.result.mapValuesOption.Place__c;
                component.set('v.listPlace',listPlace);
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
            
    },
    
    validateCampaign : function(component, campaignId){
        var configDataWrapper = component.get('v.configDataWrapper');
        var optionCode;
        if(configDataWrapper.result.options && configDataWrapper.result.options.length == 0){
            this.validateCampaignCall(component, campaignId, null);
        } else if(configDataWrapper.result.options && configDataWrapper.result.options[0]){
            optionCode = configDataWrapper.result.options[0].optionCode;  
            var fatherProductCode = configDataWrapper.masterProdExternalCode;
            var action = component.get('c.getOptionMonthlyPrice');
            action.setParams({
                'optionCode' : optionCode,
                'fatherProductCode' : fatherProductCode
            });
            action.setCallback(this, function(res){ 
                var state = res.getState();
                if(state==='SUCCESS'){
                    var sfMonthlyPrice = res.getReturnValue();
                    this.validateCampaignCall(component, campaignId, sfMonthlyPrice);            
                }
                else{
                    component.set('v.warnMessage', 'An error occurred during campaign validation');
                    component.set('v.invalidCampaign', true);
                    //this.enableDisableSaveButton(component);
                }
            });
            $A.enqueueAction(action); 
        }
        else{
            component.set('v.warnMessage', 'Please choose an option before the validation of the campaign.');
            component.set('v.invalidCampaign', true);
            //this.enableDisableSaveButton(component);
        }         
    },
        
    validateCampaignCall : function(component, campaignId, sfMonthlyPrice){
        var action = component.get('c.validateCampaign');
        action.setParams({
            'campaignId' : campaignId,
        });
        action.setCallback(this, function(res){ 
            var state = res.getState();
            if(state === 'SUCCESS'){
                var result = res.getReturnValue();
                if(result){
                    console.log('CAMPAIGN VALIDATION RESULT -> ' + JSON.stringify(result));
                    if(result.campaign_name === 'INVALID_CC_ERROR'){
                        component.set('v.warnMessage', 'Invalid campaign code.');
                        component.set('v.confirmMessage', '');
                        component.set('v.invalidCampaign', true);
                        //this.enableDisableSaveButton(component);
                    }
                    else if(result.campaign_name === 'GENERIC_CC_ERROR'){
                        component.set('v.warnMessage', 'Something went wrong. Please try again.');
                        component.set('v.confirmMessage', '');
                        component.set('v.invalidCampaign', true);
                        //this.enableDisableSaveButton(component);
                    }
                        else{
                            var configDataWrapper = component.get('v.configDataWrapper');
                            var monthlyReach = result.monthly_guaranteed_reach;
                            var monthlyPrice = result.monthly_price;
                            var startDateFromGotU = result.start_date;
                            var duration = result.duration;
                            var campaignType = result.campaign_type;
                            var isSocialBooster = (configDataWrapper.masterProductCode == component.get('v.constants')['MyCSocialBoosterPC']);
                            if((duration == 1 && !isSocialBooster) || (duration != 1 && isSocialBooster)){
                                component.set('v.warnMessage', $A.get("$Label.c.ENH_Campaign_Validation_Error"));
                                component.set('v.confirmMessage', '');
                                component.set('v.invalidCampaign', true);
                           } else if(isSocialBooster){
                                //component.find('subTerm').set('v.value', duration);
                                if(startDateFromGotU) { component.set('v.startDate', startDateFromGotU);}
                                else if(campaignType && campaignType == 'ASAP') {component.set('v.asapChecked', true);}
                                component.set('v.confirmMessage', 'Campaign validated');
                                component.set('v.warnMessage', '');
                                component.set('v.invalidCampaign', false);
                                component.set('v.disableSubTerm', true);
                                //component.set('v.disableCampaignId',true);*/
                            } else if(monthlyReach){
                                    var optionReach = "";
                                    if(configDataWrapper.result.options.length > 0){
                                        var optionName = configDataWrapper.result.options[0].name;
                                        optionReach = optionName.replace(/\D/g, "");
                                    }
                                    if(monthlyReach == optionReach){
                                        if(monthlyPrice && monthlyPrice == sfMonthlyPrice){
                                            component.find('subTerm').set('v.value', duration);
                                            //if(duration == component.find('subTerm').get('v.value')){
                                            if(startDateFromGotU) component.set('v.startDate', startDateFromGotU);
                                            else if(campaignType && campaignType == 'ASAP') component.set('v.asapChecked', true);
                                            component.set('v.confirmMessage', 'Campaign validated');
                                            component.set('v.warnMessage', '');
                                            component.set('v.invalidCampaign', false);
                                            component.set('v.disableSubTerm', true);
                                            //component.set('v.disableCampaignId',true);
                                            //this.enableDisableSaveButton(component);                                                    
                                            /*}
                                                else{
                                                    component.set('v.warnMessage', $A.get("$Label.c.ENH_Campaign_Validation_Error"));
                                                    component.set('v.confirmMessage', '');
                                                    component.set('v.invalidCampaign', true);
                                                    //this.enableDisableSaveButton(component);
                                                }*/
                                            }
                                            else{
                                                component.set('v.warnMessage', $A.get("$Label.c.ENH_Campaign_Validation_Error"));
                                                component.set('v.confirmMessage', '');
                                                component.set('v.invalidCampaign', true);
                                                //this.enableDisableSaveButton(component);
                                            }
                                        }
                                        else{
                                            component.set('v.warnMessage', $A.get("$Label.c.ENH_Campaign_Validation_Error"));
                                            component.set('v.invalidCampaign', true);
                                            //this.enableDisableSaveButton(component);
                                        } 
                                    }
                                        else{
                                            component.set('v.warnMessage', 'An error occurred during campaign validation');
                                            component.set('v.invalidCampaign', true);
                                            //this.enableDisableSaveButton(component);
                                        }
                                }
                            }
                        }
                        else{
                            component.set('v.warnMessage', 'An error occurred during campaign validation');
                            component.set('v.invalidCampaign', true);
                            //this.enableDisableSaveButton(component);
                        }                    
                    });
        $A.enqueueAction(action);  
	},
    
    updateReservation : function(component,configDataWrapper,newDate,newSubTerm,oldDate,oldSubTerm){
        var action = component.get('c.updateReservation');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'newDate' : newDate,
            'newSubTerm' : newSubTerm
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                if(result){
                    if(result.isSuccess){
                        var price = result.price;
                        var allocationId = result.allocationId;
                        var parameters = result.parametersForCasa;
                        console.debug('PRICE --> '+price);
                        if(price && allocationId && parameters){
                            configDataWrapper.parametersForCasa = parameters;
                            component.set('v.confirmMessage',"Ad Context Allocation: Updated");
                            component.set('v.enableDisableReservation', true);
                            component.set('v.allocationSuccess', true);
                            this.setConfigurationDataHelper(component, price, 'Integration_List_Price__c', null, true);
                            this.setConfigurationDataHelper(component, allocationId, 'Ad_Context_Allocation__c', null, true);
                            component.set('v.adContextAllocationId',allocationId);
                            configDataWrapper.allocationId = allocationId;
                            if(newDate) configDataWrapper.startDateFromJSON = newDate;
                            if(newSubTerm) configDataWrapper.selectedSubscriptionTerm = newSubTerm;
                            component.set('v.configDataWrapper', configDataWrapper);
                            console.debug('UPDATED CONFIG DATA WRAPPER AFTER RESERVATION --> '+JSON.stringify(configDataWrapper));
                            component.set('v.changeConfiguration', true);
                            component.set('v.enableDisableAssets', false);
                        }
                        else{
                            component.set('v.warnMessage',"Something went wrong, please try again!");
                            component.set('v.startDate',oldDate);
                        }
                    }
                    else{
                        component.set('v.warnMessage',"Ad Context Allocation not updated.");
                        component.set('v.enableDisableReservation', true);
                        component.set('v.enableDisableCasa', true);
                        if(oldDate){
                            component.set('v.startDate',oldDate);
                        }
                        if(oldSubTerm){
                            component.set("v.subTerm", oldSubTerm);
                        }                        
                    }
                }
                else{
                    component.set('v.warnMessage',"Ad Context Allocation not updated.");
                    component.set('v.enableDisableReservation', true);
                    component.set('v.enableDisableCasa', true);
                    if(oldDate){
                        component.set('v.startDate',oldDate);
                    }
                    if(oldSubTerm){
                        component.set("v.subTerm", oldSubTerm);
                    }                                            
                }
            }
            else{
                console.debug('Error --');
                component.set('v.warnMessage',"Ad Context Allocation not updated.");
            }
        }); 
        $A.enqueueAction(action);        
    },

        setMaxMinDate : function(component,configDatawrapper){
            var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
            var todayDateMin = new Date(today);
            var todayDateMax = new Date(today);
            
            var daysToAddMin;
            var daysToAddMax;
            
            if(configDatawrapper.minDate && configDatawrapper.maxDate){
                var daysToAddMin = parseInt(configDatawrapper.minDate); 
                var daysToAddMax = parseInt(configDatawrapper.maxDate);
            }
            else{
                daysToAddMin = 0;
                daysToAddMax = 0;
            }
            
            var minDateMs = todayDateMin.setDate(todayDateMin.getDate() + daysToAddMin);
            var maxDateMs = todayDateMax.setDate(todayDateMax.getDate() + daysToAddMax);
            
            var minDateToShow = this.createDate(minDateMs);
            var maxDateToShow = this.createDate(maxDateMs);
            
            if((configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeUpgrade'] || 
                configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeReplacement']) && minDateToShow == today){
                var minDateToShowDate = new Date(minDateToShow);
                minDateToShow = this.createDate(minDateToShowDate.setDate(minDateToShowDate.getDate() + 1));
            }
            
            component.set("v.minimumStartDate", minDateToShow);
            component.set("v.maxStartDate", maxDateToShow);
        }
})