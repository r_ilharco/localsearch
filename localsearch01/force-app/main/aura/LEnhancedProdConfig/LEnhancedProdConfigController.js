({
	doInit : function(component, event, helper) {
		var cpqJSON = component.get('v.cpqJSON');
        component.set('v.confirmMessage',"");
        component.set('v.warnMessage',"");
        var customId = Date.now();
        component.set('v.casaExtId',customId);
        helper.initConstants(component, cpqJSON);
        //helper.setConfigurationDataHelper(component, customId, 'Master__c', null, true);
	},
    
    redirectGotU : function(component, event, helper){
    	var urlGotU = helper.gotULink(component);        
    },
    
    redirectToPublocity : function(component, event, helper){
        var pubUrl = component.get('v.constants')['Publocity'];
        window.open(pubUrl,'_target');  
    },
    
    changeSubTerm : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        //var oldSubTerm = event.getParam("oldValue");
        var oldSubTerm = configDataWrapper.selectedSubscriptionTerm;
        console.debug('OLD SUBTERM -> '+oldSubTerm);
        var selectedSubTerm = component.find("subTerm").get("v.value");
        
        var selectedFreq = component.find("billingFreq").get("v.value");
        if(selectedFreq){
            var bfMap = new Map();
            bfMap.set('Monthly', 1);
            bfMap.set('Quarterly', 4);
            bfMap.set('Semiannual', 6);
            bfMap.set('Annual', 12);
            
            if(selectedSubTerm % bfMap.get(selectedFreq) != 0){
                component.set('v.warnMessage',"Invalid combination of Subscription Term and Billing Frequency.");
                component.set('v.subTerm', selectedSubTerm);
                component.set('v.isBillingFreqOk', false);
                console.log('isBillingFreqOk:' + component.get('v.isBillingFreqOk'));
				//helper.enableDisableSaveButton(component);
            }
            else{
                component.set('v.subTerm', selectedSubTerm);
                component.set('v.isBillingFreqOk', true);
                console.log('isBillingFreqOk:' + component.get('v.isBillingFreqOk'));
                component.set('v.confirmMessage',"");
                component.set('v.warnMessage',"");
                var advOnline = [component.get('v.constants')['LBannerProductCode'],
                                 component.get('v.constants')['LBannerSwissProductCode'],
                                 component.get('v.constants')['LTopListingYProductCode'],
                                 component.get('v.constants')['LTopListingWProductCode'],
                                 component.get('v.constants')['LMobileFirstProductCode'],
                                 component.get('v.constants')['SBannerProductCode']
                                ];
                if(advOnline.includes(configDataWrapper.masterProductCode) && configDataWrapper.allocationId){
                    helper.updateReservation(component,configDataWrapper,null,selectedSubTerm,null,oldSubTerm);
                }
                //VLAUDATO CHECK
                //helper.enableDisableSaveButton(component);
            }
        }
    },
    
    changeFreq : function(component, event, helper){
        var subTerm = component.get('v.subTerm');
        var selectedFreq = component.find("billingFreq").get("v.value");
        var bfMap = new Map();
        bfMap.set('Monthly', 1);
        bfMap.set('Quarterly', 3);
        bfMap.set('Semiannual', 6);
        bfMap.set('Annual', 12);
        if(selectedFreq && subTerm){
            if(subTerm % bfMap.get(selectedFreq) != 0){
                component.set('v.warnMessage',"Invalid combination of Subscription Term and Billing Frequency.");
                component.set('v.billingFreq', selectedFreq);
                component.set('v.isBillingFreqOk', false);
                console.log('isBillingFreqOk:' + component.get('v.isBillingFreqOk'));
				//helper.enableDisableSaveButton(component);
            }
            else{
                component.set('v.billingFreq', selectedFreq);
                component.set('v.isBillingFreqOk', true);
                console.log('isBillingFreqOk:' + component.get('v.isBillingFreqOk'));
                component.set('v.confirmMessage',"");
                component.set('v.warnMessage',"");
                //VLAUDATO CHECK
                //helper.enableDisableSaveButton(component);
            }
        }
        else{
            component.set('v.billingFreq', selectedFreq);
            component.set('v.confirmMessage',"");
            component.set('v.warnMessage',"");
            //VLAUDATO CHECK
            //helper.enableDisableSaveButton(component);
        }
        
    },
    
    changeCampaignId : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');    
        console.log(configDataWrapper);
        var campaignId = component.find('campaignId').get('v.value');
       // if(campaignId.length >= 13 && configDataWrapper.masterProductCode != 'MYCSOCIALBOOSTER001'){ //remove by SPIII-4648
        if(campaignId.length >= 13 ){
            console.log('changeCampaignId');
            helper.validateCampaign(component, campaignId);      
        }
        else{
            component.set('v.invalidCampaign', true);
            //helper.enableDisableSaveButton(component);
        }
    },

    refreshCampaignInfo : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');    
        var campaignId = component.find('campaignId').get('v.value');
        if(campaignId.length >= 13 ){
            console.log('changeCampaignId');
            helper.validateCampaign(component, campaignId);      
        }
        else{
            component.set('v.invalidCampaign', true);
            component.set('v.warnMessage', $A.get("$Label.c.ENH_Missing_Campaign"));
        }
    },
    
    changeLanguage : function(component, event, helper){
        console.log('CHANGED LANG');
        var configDataWrapper = component.get('v.configDataWrapper');
        
        var selectedCategory = component.get('v.selectedCategory');
        var selectedCategoryId = component.get('v.selectedCategoryId');
        
        var selectedLang = component.find("languageSelect").get("v.value");
        
        if(selectedLang){
            if(configDataWrapper.result && configDataWrapper.result.mapValuesString && configDataWrapper.result.mapValuesString.Category__c){
                configDataWrapper.result.mapValuesString.Category__c = [];
            }
            console.log('SELECTED LANGUAGE LABEL: ');
            configDataWrapper.defaultLanguage = selectedLang;
            configDataWrapper.defaultCategoryId = selectedCategoryId;
            var action = component.get('c.reloadCategoriesLanguage');
            action.setParams({'enhancedConfigData' : configDataWrapper});
            action.setCallback(this, function(res){
                var state = res.getState();
                if(state==='SUCCESS'){
                    var result = res.getReturnValue();
                    component.set('v.configDataWrapper', result);
                    configDataWrapper = component.get('v.configDataWrapper');
                    console.log('NEW WRAPPER: '+JSON.stringify(configDataWrapper));
                    var newListCategory = configDataWrapper.result.mapValuesString.Category__c;
                    component.set('v.listCategory',newListCategory);
                    var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                    appEvent.setParams({ 'message' : 'categoryLanguage' });
                    appEvent.fire();
                    
                    if(configDataWrapper.defaultCategoryId && configDataWrapper.defaultCategory){
                        var appEvent2 = $A.get('e.c:ApplicationEventEnhanced');
                        appEvent2.setParams({'message' : 'casaCat',
                                            'valueId' : configDataWrapper.defaultCategoryId,
                                            'valueLabel' : configDataWrapper.defaultCategory,
                                            'languageChange' : true
                                           });
                        appEvent2.fire();
                        component.set('v.selectedCategory', configDataWrapper.defaultCategory);
                    	component.set('v.selectedCategoryId', configDataWrapper.defaultCategoryId);
                    }
                }
                else{
                    console.log('ERROR');
                }
            });
            $A.enqueueAction(action);
        }
        
        component.set('v.language', selectedLang);
        component.set('v.confirmMessage',"");
        component.set('v.warnMessage',"");
        //VLAUDATO CHECK
        helper.enableDisableAllocationButton(component);
        //helper.enableDisableSaveButton(component);
    },
    
    changeUrl : function(component, event, helper){
        var url = component.find('url').get('v.value');
        console.log('URL '+url);
        //VLAUDATO CHECK
        //helper.enableDisableSaveButton(component);
    },
    
    changePlacenmentPhone : function(component, event, helper){
        var placementPhone = component.find('placementPhone').get('v.value');
        helper.setConfigurationDataHelper(component, placementPhone, 'Placement_Phone__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    
    changeZip : function(component, event, helper){
        //helper.enableDisableSaveButton(component);
    },
    
    changeEditorial : function(component, event, helper){
        //helper.enableDisableSaveButton(component);
    },
    
    handleSelectedValues : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var panelId = event.getParam('panelId');
        var whoIAM = event.getParam('whoIAM');
        var selectedValue = event.getParam('selectedValue');
        var selectedValueId = event.getParam('selectedValueId');
        var panelType = event.getParam('panelType');
       
        if(panelId){
            if(panelId=='location'){
                console.log('RECEIVED EVT FROM LOCATION --> '+selectedValue+' '+selectedValueId);
                component.set('v.selectedLocation', selectedValue);
                component.set('v.selectedLocationId', selectedValueId);
                helper.setConfigurationDataHelper(component, selectedValueId, 'LocationID__c', null, true);
                helper.setConfigurationDataHelper(component, selectedValue, 'Location__c', null, true);
                if(!selectedValue && !selectedValueId){
                    component.set('v.enableDisableCasa', true);
                    component.set('v.enableDisableReservation', true);
                }
                var buttonCasa = component.get('v.enableDisableCasa');
                var buttonReservation = component.get('v.enableDisableReservation');
                if(buttonCasa && !buttonReservation ){
                    component.set('v.enableDisableCasa', true);
                    component.set('v.enableDisableReservation', false);
                }
            }
            else if(panelId=='category'){
                console.log('RECEIVED EVT FROM CATEGORY --> '+selectedValue+' '+selectedValueId);
                component.set('v.selectedCategory', selectedValue);
                component.set('v.selectedCategoryId', selectedValueId);
                helper.setConfigurationDataHelper(component, selectedValueId, 'CategoryID__c', null, true);
                helper.setConfigurationDataHelper(component, selectedValue, 'Category__c', null, true);
                if(!selectedValue && !selectedValueId){
                    component.set('v.enableDisableCasa', true);
                    component.set('v.enableDisableReservation', true);
                }
                var buttonCasa = component.get('v.enableDisableCasa');
                var buttonReservation = component.get('v.enableDisableReservation');
                if(buttonCasa && !buttonReservation){
                    component.set('v.enableDisableCasa', true);
                    component.set('v.enableDisableReservation', false);
                }
            }
			else if(panelId=='edition'){
                console.log('Event for Edition: Label '+selectedValue+' Value '+selectedValueId);
                if(!selectedValue && !selectedValueId){
                    component.set('v.selectedEdition', null);
                    component.set('v.selectedEditionId', null);
                    component.set('v.disablePrintLanguage', false);
                    component.set('v.startDate', null);
                    component.set('v.printLanguages', null);
                    component.set('v.selectedCategory', null);
                    component.set('v.selectedCategoryId', null);
                    component.set('v.selectedLocation', null);
                    component.set('v.selectedLocationId', null);
                    if(configDataWrapper.result && configDataWrapper.result.mapValuesOption){
                        if(configDataWrapper.result.mapValuesOption.Category__c){
                            configDataWrapper.result.mapValuesOption.Category__c = []; 
                        }
                        if(configDataWrapper.result.mapValuesOption.Location__c){
                            configDataWrapper.result.mapValuesOption.Location__c = []; 
                        }
                        if(configDataWrapper.result.mapValuesOption.Editorial_Content__c){
                            configDataWrapper.result.mapValuesOption.Editorial_Content__c = [];
                            component.set('v.listEditorial', []);
                        }
                    }
                    component.set('v.configDataWrapper', configDataWrapper);
                    var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                    appEvent.setParams({ 'message' : 'categoryLanguage' });
                    appEvent.fire();
                }
                else{
                    component.set('v.selectedEdition', selectedValue);
                    component.set('v.selectedEditionId', selectedValueId);
                    helper.getEditionLanguage(component, selectedValueId);
                    //helper.reloadCategoryEditionLanguage(component, selectedValueId);
                }
			}
            else if(panelId=='place'){
                console.log('RECEIVED EVT FROM PLACE PANEL');
				component.set('v.selectedPlace', selectedValue);
				component.set('v.selectedPlaceId', selectedValueId);
                helper.reloadLocations(component, selectedValueId); 
			}   
        }
        else if(whoIAM){
            if(panelType == 'location'){
                //component.set('v.selectedLocationId', selectedValueId);
                if(configDataWrapper.result){
                    if(configDataWrapper.result.options){
                        configDataWrapper.result.options.forEach(option => {
                            if(option.externalConfigurationId){
                                if(option.externalConfigurationId == whoIAM){
                                    option.selectedLocationId = selectedValueId;
                                }
                            }
                        });
                    }
                }
                helper.setConfigurationDataHelper(component, selectedValueId, 'LocationID__c', whoIAM, false);
                helper.setConfigurationDataHelper(component, selectedValue, 'Location__c', whoIAM, false);
                if(configDataWrapper.locationIdToPrice){
                    var priceMap = {};
                    priceMap = configDataWrapper.locationIdToPrice;
                    var price = priceMap[selectedValueId];
                    if(price){
                        console.log('price not undefined: '+price);
						helper.setConfigurationDataHelper(component, price, 'Integration_List_Price__c', whoIAM, false);                        
                    }
                }
            }
            if(panelType== 'category'){
                //component.set('v.selectedCategoryId', selectedValueId);
                component.set('v.errorMessage',"");
                var categoryException = false;
                if(configDataWrapper.result){
                    if(configDataWrapper.result.options){
                        console.log(JSON.stringify(configDataWrapper.result.options));
                        configDataWrapper.result.options.forEach(
                            option => {   
	                            if((option.optionCode == $A.get("{!$Label.c.VIP_PLACE_ONP}") 
                                || option.optionCode == $A.get("{!$Label.c.VIPPLACE_CODE}")) 
                                && option.selectedCategoryId
                                && option.selectedCategoryId == selectedValueId
                                && option.externalConfigurationId != whoIAM){
                                	categoryException = true;
                                }
							});
                    }
                }
                if(categoryException){
                    component.set('v.errorMessage',$A.get("{!$Label.c.VipPlace_Validation_on_Category}"));
                }
                else{
                    if(configDataWrapper.result){
                        if(configDataWrapper.result.options){
                            configDataWrapper.result.options.forEach(option => {
                                if(option.externalConfigurationId){
                                    if(option.externalConfigurationId == whoIAM){
                                        option.selectedCategoryId = selectedValueId;
                                    }
                                }
                            });
                        }
                    }
                    helper.setConfigurationDataHelper(component, selectedValueId, 'CategoryID__c', whoIAM, false);
                    helper.setConfigurationDataHelper(component, selectedValue, 'Category__c', whoIAM, false);
                    if(configDataWrapper.categoryIdToPrice){
                        console.log('MAP NOT EMPTY: '+JSON.stringify(configDataWrapper.categoryIdToPrice));
                        var priceMap = {};
                        priceMap = configDataWrapper.categoryIdToPrice;
                        var price = priceMap[selectedValueId];
                        if(price){
                            console.log('price not undefined: '+price);
                            helper.setConfigurationDataHelper(component, price, 'Integration_List_Price__c', whoIAM, false);                        
                        }
                    }
				}
            }
        }
        component.set('v.configDataWrapper', configDataWrapper);
        //VLAUDATO CHECK
        helper.enableDisableAllocationButton(component);
        //helper.enableDisableSaveButton(component);
    },
    
    templateChange : function(component, event, helper){
        var template = component.find('templateSelect').get('v.value');
        helper.setConfigurationDataHelper(component, template, 'Template__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    
    webLanguageChange : function(component, event, helper){
        var selectedWebLanguage = component.find('websiteLanguageSelect').get('v.value');
        helper.setConfigurationDataHelper(component, selectedWebLanguage, 'Website_Language__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    campaignLanguageChange : function(component, event, helper){
        var selectedLanguage = component.find('campaignLanguageSelect').get('v.value');
        helper.setConfigurationDataHelper(component, selectedLanguage, 'Campaign_language__c', null, true);
    },
    //placeHandler : function(component, event, helper){
      	//var placeId = component.find('placeSelector').get('v.value');
        //console.log('PLACE ID= '+placeId);
      	//helper.reloadLocations(component, placeId);  
    //},
    
    contactHandler : function(component, event, helper){
      	var contactId = component.find('contactSelector').get('v.value');
    },
    
   	setConfigurationData : function(component, event, helper){
        component.set('v.errorMessage',null);
        var proceeed =  helper.enableDisableSaveButton(component);
        console.log('proceeed :: '+proceeed);
        if(proceeed){
           helper.setConfigurationData(component, event);
        }
       
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
    
    redirectSearch : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var searchUrl = configDataWrapper.searchCHurl;
        window.open(searchUrl,'_target')

    },
    
    redirectLocal : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var localUrl = configDataWrapper.localCHurl;
        window.open(localUrl,'_target')
    },
    
    redirectAMA : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var amaUrl = configDataWrapper.AMAurl;
        window.open(amaUrl,'_target')
    },
    
    redirectCC : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var ccUrl = configDataWrapper.cCurl;
        window.open(ccUrl,'_target')
    },
    
    redirectTelEditor : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var telEditorUrl = configDataWrapper.telEditorurl;
        window.open(telEditorUrl,'_target')
    },
    
    redirectCopyFromLocal : function(component, event, helper){
		var configDataWrapper = component.get('v.configDataWrapper');
        var copyLocalUrl = configDataWrapper.copyLocalUrl;
        window.open(copyLocalUrl,'_target');        
    },
  
    redirectPlace : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var placeUrl = configDataWrapper.placeUrl;
        window.open(placeUrl,'_target')
    },

    casaReservation : function(component, event, helper){
        component.set('v.confirmMessage',"");
        component.set('v.warnMessage',"");
        component.set('v.errorMessage',"");
        var startDate = component.get('v.startDate');
        var configDataWrapper = component.get('v.configDataWrapper');
        var selectedLocId = component.get('v.selectedLocationId');
        var selectedCatId = component.get('v.selectedCategoryId');
        var selectedLoc = component.get('v.selectedLocation');
        var selectedCat = component.get('v.selectedCategory');
        var selectedLanguage = component.get('v.language');

        var sameLocationId = selectedLocId == configDataWrapper.defaultLocationId;
        var sameCategoryId = selectedCatId == configDataWrapper.defaultCategoryId;
		var renewalAndNOLocCatChange = (configDataWrapper.quoteType == component.get('v.constants')['QuoteTypeRenewal'] && sameLocationId && sameCategoryId); 

		if(selectedLocId == 'all') selectedLocId = null;
        configDataWrapper.startDateFromJSON = startDate;
        component.set('v.configDataWrapper', configDataWrapper);
        
        var action = component.get('c.adContextAllocationCallout');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'selectedLocId' : selectedLocId,
            'selectedCatId' : selectedCatId,
            'selectedLanguage' : selectedLanguage
        });
        
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                if(result){
                    if(result.isSuccess){
                        var price = result.price;
                        var allocationId = result.allocationId;
                        var parameters = result.parametersForCasa;
                        console.log('PRICEEEE -->>>>'+price);
                        if(price && allocationId && parameters){
                            configDataWrapper.parametersForCasa = parameters;
                            component.set('v.confirmMessage',"Ad Context Allocation: Reserved");
                            component.set('v.enableDisableReservation', true);
                            component.set('v.allocationSuccess', true);
                            var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                            appEvent.setParams({'message' : 'casaLoc',
                                                'valueId' : selectedLocId,
                                                'valueLabel' : selectedLoc
                                               });
                            appEvent.fire();
                            var appEvent2 = $A.get('e.c:ApplicationEventEnhanced');
                            appEvent2.setParams({'message' : 'casaCat',
                                                'valueId' : selectedCatId,
                                                'valueLabel' : selectedCat,
                                                'languageChange' : false
                                               });
                            appEvent2.fire();
                            component.set('v.disableLanguage',true);
                            
                            if(!renewalAndNOLocCatChange) helper.setConfigurationDataHelper(component, price, 'Integration_List_Price__c', null, true);
                            helper.setConfigurationDataHelper(component, allocationId, 'Ad_Context_Allocation__c', null, true);
                            component.set('v.adContextAllocationId',allocationId);
                            configDataWrapper.allocationId = allocationId;
                            component.set('v.configDataWrapper', configDataWrapper);
                            console.log('UPDATED CONFIG DATA WRAPPER AFTER RESERVATION --> '+JSON.stringify(configDataWrapper));
                            //helper.enableDisableSaveButton(component);
                            component.set('v.changeConfiguration', true);
                            component.set('v.enableDisableAssets', false);
                        }
                        else{
                            component.get('v.allocationSuccess',false);
                   			configDataWrapper.allocationId = "";
                            component.set('v.warnMessage',"Something went wrong, please try again!");
                        }
                    }
                    else{
                        component.set('v.warnMessage',"Ad Context Allocation not Reserved");
                        component.set('v.enableDisableReservation', true);
                        component.set('v.enableDisableCasa', false);
                        component.get('v.allocationSuccess',false);
                   		configDataWrapper.allocationId = "";
                    }
                }
            }
            else{
                console.log('Error --');
                component.get('v.allocationSuccess',false);
                configDataWrapper.allocationId = "";
                component.set('v.warnMessage',"Ad Context Allocation not Reserved");
 
            }
        });
        
        $A.enqueueAction(action);
    },
    
    onDateChange : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        console.debug('CONFIG DATA WRAPPER --> '+JSON.stringify(configDataWrapper));
        var oldDate = configDataWrapper.startDateFromJSON;
        component.set('v.confirmMessage',"");
        component.set('v.warnMessage',"");
        var today2 = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        if(component.get('v.startDate')<today2){
            component.set('v.startDate',null);
            component.set('v.warnMessage', $A.get("$Label.c.ENH_StartDate_Validation"));
        }
        console.debug('START DATE '+component.get('v.startDate'));
        var newDate = component.get('v.startDate');
        
        
        var advOnline = [component.get('v.constants')['LBannerProductCode'],
                         component.get('v.constants')['LBannerSwissProductCode'],
                         component.get('v.constants')['LTopListingYProductCode'],
                         component.get('v.constants')['LTopListingWProductCode'],
                         component.get('v.constants')['LMobileFirstProductCode'],
                         component.get('v.constants')['SBannerProductCode']
						];
        if(advOnline.includes(configDataWrapper.masterProductCode) && configDataWrapper.allocationId){
            helper.updateReservation(component,configDataWrapper,newDate,null,oldDate,null);
        }
        else{
            configDataWrapper.startDateFromJSON = component.get('v.startDate');
        	component.set('v.configDataWrapper', configDataWrapper);
        }
        //helper.enableDisableSaveButton(component);
    },
    
    redirectToCasa : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var selectedLocation = component.get('v.selectedLocationId');
        var selectedCategory = component.get('v.selectedCategoryId');
        var selectedLanguage = component.get('v.language');
        var casaExtId = component.get('v.casaExtId');

        configDataWrapper.startDateFromJSON = component.get('v.startDate');

        var action = component.get('c.getCasaUrl');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'selectedCategory' : selectedCategory,
            'selectedLocation' : selectedLocation,
            'selectedLanguage' : selectedLanguage,
            'casaExtId' : casaExtId
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var casaUrl = res.getReturnValue();
                console.log('CASA URL '+casaUrl);
                window.open(casaUrl, '_target');
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    
    handlerPlatformEvent : function(component, event, helper){
        var listCat = component.get('v.listCategory');
        var listLoc = component.get('v.listLocation');
        
        var quoteItemId = component.get('v.casaExtId');
		var configDataWrapper = component.get('v.configDataWrapper');
        
        var productExternalCode = configDataWrapper.masterProdExternalCode;
        var params = event.getParam('arguments');
                
        var casaPricing;
        var locationId;
        var cateogryId;
		var language; 
        var changeLanguage = true;
        var allocationId;
        var quoteId = configDataWrapper.quoteId;
        if(params){
            if(params.message.message.data.payload.Success__c){
                if(params.message.message.data.payload.Quote_Item__c == quoteItemId){
                    component.set('v.startDate',params.message.message.data.payload.Start_Date__c);
                    if(params.message.message.data.payload.Language__c){
                        var returnedLanguage = params.message.message.data.payload.Language__c;
                        if(returnedLanguage == 'it') language = 'Italian';
                        if(returnedLanguage == 'de') language = 'German';
                        if(returnedLanguage == 'fr') language = 'French';
                        if(returnedLanguage == 'en') language = 'English';
                        if(returnedLanguage == 'all') changeLanguage = false;
                    }
                    if(changeLanguage) component.set('v.language', language);
                    if(params.message.message.data.payload.Category__c){
                        var selectedCategory = component.get('v.selectedCategoryId');
                        var returnedCategory = params.message.message.data.payload.Category__c;
                        if(selectedCategory != returnedCategory){
                            helper.changeSelectedCasaCategory(component, returnedCategory, listCat);
                        }
                        
                    }
                    if(params.message.message.data.payload.Region__c){
                        var selectedLocation = component.get('v.selectedLocationId');
                        var returnedLocation = params.message.message.data.payload.Region__c;
                        if(selectedLocation != returnedLocation){
                            helper.changeSelectedCasaLocation(component, returnedLocation, listLoc);
                        }
                    }
                    if(params.message.message.data.payload.Ad_Context_Allocation__c){
                        allocationId = params.message.message.data.payload.Ad_Context_Allocation__c;
                    }
                    casaPricing = params.message.message.data.payload.Price__c;
                }
            }
        }
        if(casaPricing){
            var action = component.get('c.getPriceFromMDT');
            action.setParams({
                'priceLevel' : casaPricing,
                'productCode' : productExternalCode,
                'allocationId' : allocationId,
                'quoteId' : quoteId
            });
            action.setCallback(this, function(res){
                var state = res.getState();
                if(state==='SUCCESS'){
                    var price = res.getReturnValue();
                    helper.setConfigurationDataHelper(component, price, 'Integration_List_Price__c', null, true);
                    helper.setConfigurationDataHelper(component, allocationId, 'Ad_Context_Allocation__c', null, true);
                    component.set('v.adContextAllocationId',allocationId);
                    configDataWrapper.allocationId = allocationId;
                    component.set('v.configDataWrapper', configDataWrapper);
                    component.set('v.warnMessage',"");
                    component.set('v.confirmMessage',"Ad Context Allocation: Reserved");
                    component.set('v.enableDisableCasa', true);
                    component.set('v.allocationSuccess', true);
                    //helper.enableDisableSaveButton(component);
                    component.set('v.changeConfiguration', true);
                    component.set('v.enableDisableAssets', false);
                }
                else{
                    console.log('ERROR');
                    component.set('v.warnMessage',"Something went wrong, please try again!");
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    changeConfiguration : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var allocationId = configDataWrapper.allocationId;
        var quoteId = configDataWrapper.quoteId;
        var action = component.get('c.deallocateReservation');
        action.setParams({
            'allocationId' : allocationId,
            'quoteId' : quoteId
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                if(result){
                    var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                    appEvent.setParams({'message' : 'change',
                                        'valueId' : '',
                                        'valueLabel' : ''
                                       });
                    appEvent.fire();
                    var appEvent2 = $A.get('e.c:ApplicationEventEnhanced');
                    appEvent2.setParams({'message' : 'change',
                                         'valueId' : '',
                                         'valueLabel' : ''
                                        });
                    appEvent2.fire();
                    component.set('v.disableLanguage',false);
                    component.set('v.warnMessage',"");
                    component.set('v.confirmMessage',"Ad Context Allocation: Deleted");
                    component.set('v.enableDisableReservation', true);
                    component.set('v.enableDisableCasa', true);
                    component.set('v.enableDisableSave', true);
                    component.set('v.changeConfiguration', false);
                     // clear values//
                   component.set('v.selectedCategoryId',"");
                   component.set('v.selectedLocationId',"");
                    
                   component.set('v.allocationSuccess',false);
                   configDataWrapper.allocationId = null;
                }
                else{
                    component.set('v.warnMessage',"Something went wrong. Unable to delete reservation");
                    component.set('v.confirmMessage',"");    
                }                   
            }
            else{
                console.log('ERROR');
                component.set('v.warnMessage',"Something went wrong, please try again!");
                component.set('v.confirmMessage',"");
            }
        });
        $A.enqueueAction(action);
    },
    
    redirectTemplateLibrary : function(component, event, helper){
        var language = $A.get("$Locale.language");
        var redirectUrl;
        
        if(language === 'it'){
			redirectUrl = component.get('v.constants')['WebsiteTemplateIT'];
        }
        else if(language === 'fr'){
            redirectUrl = component.get('v.constants')['WebsiteTemplateFR'];
        }
        else{
			redirectUrl = component.get('v.constants')['WebsiteTemplateDE'];
        }
        if(redirectUrl) window.open(redirectUrl,'_target');
    },
    
    redirectSiteSearch : function(component, event, helper){
        var redirectUrl = component.get('v.constants')['WebsiteSiteSearch'];
        window.open(redirectUrl,'_target');
    },
    
    changeKeywords : function(component, event, helper){
        var keywords = component.find('keywords').get('v.value');
        console.log('KEYWORDS -->'+keywords);
        helper.setConfigurationDataHelper(component, keywords, 'Keywords__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    
    changeDescription : function(component, event, helper){
        var description = component.find('description').get('v.value');
        console.log('DESCR -->'+description);
        helper.setConfigurationDataHelper(component, description, 'CDescription__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    
    handleCancelClick : function(component, event, helper){
        var myEvent = $A.get("e.c:EnhancedConfigLightning2VFJson");
        myEvent.setParams({
            cpqJSON: null
        });
        myEvent.fire();  
    },
    
    addAssets : function(component, event, helper){
        var configDataWrapper = component.get('v.configDataWrapper');
        var adContextAllocationId = component.get('v.adContextAllocationId');
        if(!adContextAllocationId){
            adContextAllocationId = configDataWrapper.allocationId;
        }
        if(adContextAllocationId){
            var action = component.get('c.getCasa2Url');
            action.setParams({
                'enhancedConfigData' : configDataWrapper,
                'adContextAllocationId' : adContextAllocationId
            });
            action.setCallback(this, function(res){
                var state = res.getState();
                if(state==='SUCCESS'){
                    var casaUrl = res.getReturnValue();
                    console.log('CASA URL '+casaUrl);
                    window.open(casaUrl, '_target');
                }
                else{
                    console.log('ERROR');
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    changeSimulationNumber : function(component, event, helper){
        var simNumber = component.find('simNumber').get('v.value');
        helper.setConfigurationDataHelper(component, simNumber, 'Simulation_Number__c', null, true);
        //helper.enableDisableSaveButton(component);
    },
    
    changePickupNumber : function(component, event, helper){
        var pickupNumber = component.find('pickupNumber').get('v.value');
        helper.setConfigurationDataHelper(component, pickupNumber, 'Pickup_Number_Text__c', null, true);
    },
    
    printLanguageChange : function(component, event, helper){
        console.log('In controller function printLanguageChange');
        
        var configDataWrapper = component.get('v.configDataWrapper');
		var choosenLanguage = component.find('printLanguageSelect').get('v.value');
        var editionId = component.get('v.selectedEditionId');
        
        if(configDataWrapper.result && configDataWrapper.result.mapValuesOption){
            if(configDataWrapper.result.mapValuesOption.Category__c){
               configDataWrapper.result.mapValuesOption.Category__c = []; 
            }
        }
        if(!component.get('v.selectedLocation') && !component.get('v.selectedLocationId')){
            if(configDataWrapper.result.mapValuesOption.Location__c){
               configDataWrapper.result.mapValuesOption.Location__c = []; 
            }
        }
        if(configDataWrapper.result.mapValuesOption.Editorial_Content__c){
            configDataWrapper.result.mapValuesOption.Editorial_Content__c = [];
            component.find('editorial').set('v.value','');
            component.set('v.listEditorial', []);
            //helper.enableDisableSaveButton(component);
        }
        var action = component.get('c.reloadCategoryEditionLanguage_lang');
        action.setParams({
            'enhancedConfigData' : configDataWrapper,
            'editionId' : editionId,
            'choosenLanguage' : choosenLanguage
        });
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state==='SUCCESS'){
                var result = res.getReturnValue();
                component.set('v.configDataWrapper', result);
                configDataWrapper = component.get('v.configDataWrapper');
                var listCat = configDataWrapper.result.mapValuesOption.Category__c;
                var listCatEditorial = configDataWrapper.result.mapValuesOption.Editorial_Content__c;
                if(!component.get('v.selectedLocation') && !component.get('v.selectedLocationId')){
                    var listLoc = configDataWrapper.result.mapValuesOption.Location__c;
                    component.set('v.listLocation',listLoc);
                }
                component.set('v.listCategory',listCat);
                component.set('v.listEditorial', listCatEditorial);
                console.log('Planned publication date for selected book: '+configDataWrapper.startDatePrint);
                component.set('v.startDate',configDataWrapper.startDatePrint);
                var appEvent = $A.get('e.c:ApplicationEventEnhanced');
                appEvent.setParams({ 'message' : 'categoryLanguagePrint'});
                appEvent.fire();
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    onChangeASAP : function(component, event, helper){
        
        var configDatawrapper = component.get('v.configDataWrapper');
        var previousStartDate = component.get('v.startDate');
        var previousStartDateFromCMP = component.get('v.previousStartDate');
        
        var checked = component.get('v.asapChecked');
        if(!checked){
            component.set('v.disableStartDate', false);
            if(previousStartDateFromCMP) component.set('v.startDate', previousStartDateFromCMP);
        }
        else{
            component.set('v.previousStartDate', previousStartDate);
            component.set('v.disableStartDate', true);
            
            var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
            var todayDateMin = new Date(today);
            
            if(configDatawrapper.minDate) var daysToAddMin = parseInt(configDatawrapper.minDate); 
            else daysToAddMin = 0;            
            
            var minDateMs = todayDateMin.setDate(todayDateMin.getDate() + daysToAddMin);
            var minDateToShow = helper.createDate(minDateMs);
            
            if((configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeUpgrade'] || configDatawrapper.quoteType == component.get('v.constants')['QuoteTypeReplacement']) && minDateToShow == today){
                var minDateToShowDate = new Date(minDateToShow);
                minDateToShow = helper.createDate(minDateToShowDate.setDate(minDateToShowDate.getDate() + 1));
            }
            component.set("v.startDate", minDateToShow);
        }
    }
 })