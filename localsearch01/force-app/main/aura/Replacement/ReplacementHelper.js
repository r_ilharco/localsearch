({
	showToast : function(component, state, message, type) {
   			var toastEvent = $A.get("e.force:showToast");
    		toastEvent.setParams({
        	"title": state,
        	"message": message,
            "type": type,
    		});
    		toastEvent.fire();
		},
})