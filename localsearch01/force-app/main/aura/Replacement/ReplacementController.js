({
	handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            component.set("v.showModal", true);
            var replacementStatus = component.get("v.simpleRecord.Replacement__c");
             component.set("v.simpleRecord.Replacement__c", !replacementStatus);
    	} else if(eventParams.changeType === "CHANGED") {
            var changedFields = eventParams.changedFields;
            // record is changed so refresh the component (or other component logic)
            component.find("recordLoader").reloadRecord();
            } 
    	}
    ,
    
    handleBack: function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },
        
    handleSave : function(cmp, event, helper) {
        var recordLoader = cmp.find("recordLoader");
    	var replacementStatus = cmp.get("v.simpleRecord.Replacement__c");
        recordLoader.saveRecord($A.getCallback(function(saveResult) {
           if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
               helper.showToast(cmp, saveResult.state, $A.get("$Label.c.Updated_Record"), "success");
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            } else  if (saveResult.state === "ERROR") {
                var errMsg = "";
                for (var i = 0; i < saveResult.error.length; i++) {
                    errMsg += saveResult.error[i].message + "\n";
                }
                cmp.set("v.recordSaveError", errMsg);
                helper.showToast(cmp, saveResult.state, errMsg, "error");
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
        }));
    },
})