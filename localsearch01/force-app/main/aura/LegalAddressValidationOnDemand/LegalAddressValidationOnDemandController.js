/*
CHANGELOG:
            #1 - SF2-253 - Validate Account and Billing Address on Quote Creation
                 SF2-264 - Legal Address Validation
                 gcasola 07-22-2019
                 */
({
    doInit : function(component, event, helper) {
        
        var id = component.get("v.recordId");
        var action = component.get('c.validateRecordLegalAddress');
        action.setParams({
            recordId : id
        });
        action.setCallback(this, function(response) {
            component.set("v.loaded", true);
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnString = response.getReturnValue(); 
                console.log('Return String: '+returnString)
                if(returnString=="OK"){ 
                   // helper.showToast(component,event,'success',returnString,'SUCCESS');
                   helper.showToast(component,event,'success',$A.get("{!$Label.c.Address_Validated}"),'SUCCESS');
                }else if(returnString=="SKIPPED"){
                    helper.showToast(component,event,'info',$A.get("{!$Label.c.LegalAddressValidationOnDemand_SkipMessage_Account}"),$A.get("{!$Label.c.InformationMessage_Title}"));
                }else{
                  //  helper.showToast(component,event,'error',returnString,'ERROR');
                  //A.L. Marotta 02-01-20 - returnString appended to error message
                  //helper.showToast(component,event,'error',$A.get("{!$Label.c.Address_not_validated}"),'ERROR');
                  helper.showToast(component,event,'error',$A.get("{!$Label.c.Address_not_validated}") + ' '+ returnString,'ERROR');
                }
            }else{
                helper.showToast(component,event,'error','Internal Error','ERROR');
            }
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
        
        //A. Marotta 27-05-19 End

    }
})