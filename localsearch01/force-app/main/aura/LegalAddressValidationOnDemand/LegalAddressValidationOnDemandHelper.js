({
    //A. L. Marotta 28-05-19 US #92 #93, Wave 1, Sprint 2

    showToast : function(component,event,type,message,title){
        console.log(type+message+title);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message,
            "mode" : "sticky"
        });
        toastEvent.fire();
        console.log('toast fired');
        $A.get("e.force:closeQuickAction").fire();
    }
})