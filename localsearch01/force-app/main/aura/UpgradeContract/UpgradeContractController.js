({
    doInit: function(component, event, helper) {
        var action = component.get("c.upgradeContract");
        action.setParams({
            				"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state '+ JSON.stringify(state));
            $A.get("e.force:closeQuickAction").fire();
            if(state === "SUCCESS") {
                var res = response.getReturnValue();
                if(res != null)
                {   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": $A.get("{!$Label.c.Success}"),
                    "message": $A.get("{!$Label.c.Record_Successfully_Created}"),
                    "type": "success"
                    });
                    toastEvent.fire();
                    var sObjectEvent = $A.get("e.force:navigateToSObject");
                    sObjectEvent.setParams({
                        "recordId": response.getReturnValue(),
                        "slideDevName": "detail"
                    });
                    sObjectEvent.fire();
        		}else
                {
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": $A.get("{!$Label.c.Error}"),
                        "message": $A.get("{!$Label.c.Contract_Not_Active}"),
                        "type": "warning"
                    });
                    toastEvent.fire();
                }
            }else if (state === "ERROR"){
                var errors = response.getError();
                if(errors) {
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": $A.get("{!$Label.c.Error}"),
                        "message": errors[0]['message'],
                        "type": "warning"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})