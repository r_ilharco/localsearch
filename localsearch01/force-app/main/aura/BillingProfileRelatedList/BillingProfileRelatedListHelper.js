({
	closeModal : function(cmp, event) {
        cmp.set("v.body", []);
        var cmpTarget = cmp.find('modalbox');
        var cmpBack = cmp.find('modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 		
	}
})