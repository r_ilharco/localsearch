({
	init : function(cmp, event, helper) {
        cmp.set('v.customActions', [
            { label: 'Custom action', name: 'custom_action' }
        ])        
        cmp.set('v.bpColumns', [
            { label: 'NAME', fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_top'} },
            { label: 'BILLING CONTACT', fieldName: 'Billing_Contact__r_Name', type: 'text' },
            { label: 'CHANNELS', fieldName: 'Channels__c', type: 'text'},
            { label: 'BILLING LANGUAGE', fieldName: 'Billing_Language__c', type: 'text' },
            { label: 'DEFAULT', fieldName: 'Is_Default__c', type: 'boolean' } 
        ])
    },
    customHandler : function(cmp, event, helper) {
        $A.createComponent(
            "c:BillingProfile", { 
                recordId: cmp.get("v.recordId"),
                closeAction: cmp.get("c.closeAction")
            },
            function(newBp, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(newBp);
                    cmp.set("v.body", body);
                    var cmpTarget = cmp.find('modalbox');
                    var cmpBack = cmp.find('modalbackdrop');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open');                    
                }
                else {
                    var resultsToast = $A.get("e.force:showToast");
                    var resp = event.getParams();
                    resultsToast.setParams({
                        "title": "Error",
                        "message": errorMessage,
                        "type": "error"
                    });
                    resultsToast.fire();
                    return false;
                }
            }
        );
 
    },
    closeAction:function(cmp, event, helper) {
        helper.closeModal(cmp, event);
        // $A.get('e.force:refreshView').fire();
        location.reload(); // needed, as it doesn't refresh list components... (Known SF issue:https://success.salesforce.com/issues_view?id=a1p3A000000mDOuQAM)
    },
	closeModal:function(cmp, event, helper) {
        helper.closeModal(cmp, event);
    },    
    refreshView : function (component, event, helper) {
         $A.get('e.force:refreshView').fire();
    }  
})