({
    getAccess: function(component, event)
    {
        var recordId = component.get("v.recordId");
        var urlString = window.location.href;
        var baseUrl = window.location.origin;
        
        component.set('v.recordId',baseUrl.length+28,urlString.lastIndexOf('/'));
        recordId = urlString.substring(baseUrl.length+28,urlString.lastIndexOf('/'));
        var action = component.get("c.getAccess");
        //console.log('component.get("v.recordId"): ' + component.get("v.recordId"));
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log('getAccess->response: ' + JSON.stringify(response));
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //console.log("--->" + JSON.stringify(result));
                
                if(Object.getOwnPropertyNames(result).length > 0 
                   && result.hasOwnProperty('isUserAllowed') && result.hasOwnProperty('isComponentEnabled')){
                    if(!result['isUserAllowed'])
                    {
                        $A.get("e.force:closeQuickAction").fire();
                        $A.get('e.force:refreshView').fire();
                        if(result.hasOwnProperty('info') 
                           && result['info'].hasOwnProperty('errorMessages')
                           && result['info']['errorMessages'].length > 0){
                            for(var i = result['info']['errorMessages'].length - 1; i >= 0; i--)
                            {
                                var errorMessage = result['info']['errorMessages'][i];
                                if(errorMessage){
                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.setParams({
                                        "title": "Warning!",
                                        "message": errorMessage,
                                        "type": "warning",
                                        "mode": "sticky"
                                    });
                                    toastEvent.fire();
                                }
                            }
                        }
                    }else if(result['isUserAllowed'] && result['isComponentEnabled']){
                        
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getFilteredValues : function (component,event,recordId,quoteLineLabelsMap){
        var myarray = component.get("v.QLsOptionsFilter");
        var action = component.get("c.getQuoteLineMapByQuoteId01");
        //console.log('component.get("v.recordId"): ' + component.get("v.recordId"));
        action.setParams({
            quoteId:  recordId
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result '+JSON.stringify(result));

                let finalMap = {};
                
                console.log("quoteLineLabelsMap " + JSON.stringify(quoteLineLabelsMap) );
                for (var k in result){
                    console.log("Key is " + k + ", value is " + result[k]);
                    if (result.hasOwnProperty(k)) {
                        
                        var quoteLinesBySub = [];
                        quoteLinesBySub.push({'label': 'Select Quote Line', 'value': ''});
                        result[k].forEach(function(item){
                            console.log('item ::: '+item);
                            
                            quoteLinesBySub.push({'label': quoteLineLabelsMap[item.Id], 'value': item.Id});
                        });
                    
                        console.log(" k :: "+k+"quoteLinesBySub " + JSON.stringify(quoteLinesBySub) );
                        finalMap[k] = quoteLinesBySub;
                        
                        
                    }
                }
                console.log("finalMap " + finalMap);               
                component.set("v.QLsOptionsFilter",finalMap);
                component.set("v.QLsOptionsFilter_helper",finalMap);
            }
            else
            {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                }
            }
        });
        
        $A.enqueueAction(action);
    },
     
    getData: function(component, event)
    {
        var recordId = component.get("v.recordId");
        var urlString = window.location.href;
        var baseUrl = window.location.origin;
        
        component.set('v.recordId',baseUrl.length+28,urlString.lastIndexOf('/'));
        recordId = urlString.substring(baseUrl.length+28,urlString.lastIndexOf('/'));
        var action = component.get("c.getQuote");
        //console.log('component.get("v.recordId"): ' + component.get("v.recordId"));
        action.setParams({
            quoteId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //console.log("--->" + JSON.stringify(result));
                component.set("v.QuoteObj", result);
                this.prepareDataTable(component, event);
                
            }
            else
            {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    //console.log("Error message: " + errors[0].message);
                }
            }
        });
        
        $A.enqueueAction(action);
        
        var actionQLs = component.get("c.getQuoteLineMapByQuoteId");
        
        actionQLs.setParams({
            quoteId: recordId
        });
        actionQLs.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var orderedQuoteLineLabelsIds = [];
                var quoteLineLabelsMap = {};
                var quoteLineLabelsList = [];
                var quoteLinesMap = {};
                var quoteLinesList = [];
                var QLsOptions = [];
                var quoteLinesByProductGroupAndPlaceMap = {};
                component.set("v.quoteLinesMap", result);
                var minQLStartDate;
                
                if(result){
                    for(var qlId of Object.getOwnPropertyNames(result)){
                        var quoteLineLabel = {};
                        quoteLinesMap[qlId] = result[qlId];
                        quoteLineLabelsMap[qlId] = quoteLinesMap[qlId].Name;
                        quoteLinesMap[qlId].qlName =  quoteLinesMap[qlId].Name;
                        quoteLinesMap[qlId].qlUrl =  '/' + quoteLinesMap[qlId].Id;
                        
                        if((quoteLinesMap[qlId].SBQQ__Product__c && quoteLinesMap[qlId].SBQQ__Product__r.Product_Group__c) 
                           && quoteLinesMap[qlId].Place__c){
                            if(!quoteLinesByProductGroupAndPlaceMap[quoteLinesMap[qlId].SBQQ__Product__r.Product_Group__c])
                                quoteLinesByProductGroupAndPlaceMap[quoteLinesMap[qlId].SBQQ__Product__r.Product_Group__c] = {};
                            quoteLinesByProductGroupAndPlaceMap[quoteLinesMap[qlId].SBQQ__Product__r.Product_Group__c][quoteLinesMap[qlId].Place__c] = qlId;
                        }
                        
                        if(quoteLinesMap[qlId].Place__c){
                            quoteLinesMap[qlId].qlPlaceUrl = '/' + quoteLinesMap[qlId].Place__c;
                            quoteLinesMap[qlId].qlPlaceName = quoteLinesMap[qlId].Place__r.Name;
                            quoteLinesMap[qlId].qlPlaceID = quoteLinesMap[qlId].Place__r.PlaceID__c;
                        }
                        
                        if(quoteLinesMap[qlId].SBQQ__Product__r){
                            quoteLinesMap[qlId].qlProductName = quoteLinesMap[qlId].SBQQ__Product__r.Name;
                            quoteLinesMap[qlId].qlConfigurationModel = quoteLinesMap[qlId].SBQQ__Product__r.Configuration_Model__c;
                        }
                        
                        quoteLinesMap[qlId].qlSubscriptionTerm = quoteLinesMap[qlId].Subscription_Term__c;
                        quoteLinesMap[qlId].qlStartDate = quoteLinesMap[qlId].SBQQ__StartDate__c;
                        quoteLinesMap[qlId].qlEndDate = quoteLinesMap[qlId].SBQQ__EndDate__c;
                        
                        if(!minQLStartDate) minQLStartDate = quoteLinesMap[qlId].SBQQ__StartDate__c;
                        else if(minQLStartDate > quoteLinesMap[qlId].SBQQ__StartDate__c) minQLStartDate = quoteLinesMap[qlId].SBQQ__StartDate__c;
                        
                        if(quoteLinesMap[qlId].SBQQ__Product__c && quoteLinesMap[qlId].SBQQ__Product__r.Name)
                            quoteLineLabelsMap[qlId] += ' - ' + quoteLinesMap[qlId].SBQQ__Product__r.Name;
                        
                        if(quoteLinesMap[qlId].Place__c && quoteLinesMap[qlId].Place__r.Name)
                            quoteLineLabelsMap[qlId] += ' - ' + quoteLinesMap[qlId].Place__r.Name+' - ' +quoteLinesMap[qlId].Place__r.PlaceID__c;
                        quoteLineLabel.id = qlId;
                        quoteLineLabel.label = quoteLineLabelsMap[qlId];
                        quoteLineLabelsList.push(quoteLineLabel);
                        //console.log('ql => ' + JSON.stringify(result[qlId]));
                        quoteLinesList.push(quoteLinesMap[qlId]);
                    }
                    
                    if(minQLStartDate) component.set('v.minQLStartDate', minQLStartDate);
                    
                    quoteLinesList.sort(function(a,b){
                        var labelA=a.Name.toLowerCase();
                        var labelB=b.Name.toLowerCase();
                        
                        if(labelA == labelB){
                            return 0;
                        }else{
                            return (labelA < labelB) ? -1 : 1;
                        }
                    });
                    
                    if(quoteLineLabelsList && Array.isArray(quoteLineLabelsList)){
                        quoteLineLabelsList.sort(function(a,b){
                            var labelA=a.label.toLowerCase();
                            var labelB=b.label.toLowerCase();
                            
                            if(labelA == labelB){
                                return 0;
                            }else{
                                return (labelA < labelB) ? -1 : 1;
                            }
                        });
                        
                        QLsOptions.push({'label': 'Select Quote Line', 'value': ''});
                        for(quoteLineLabel of quoteLineLabelsList){
                            orderedQuoteLineLabelsIds.push(quoteLineLabel.id);
                            QLsOptions.push({'label': quoteLineLabelsMap[quoteLineLabel.id], 'value': quoteLineLabel.id});
                        }
                        
                        this.getFilteredValues(component,event,recordId,quoteLineLabelsMap);
                                                
                    }
                    
                    component.set("v.quoteLinesByProductGroupAndPlaceMap", quoteLinesByProductGroupAndPlaceMap);
                    //console.log(">>>!quoteLinesByProductGroupAndPlaceMap: " + JSON.stringify(component.get("v.quoteLinesByProductGroupAndPlaceMap")));
                    component.set("v.dataQLs", quoteLinesList);
                    component.set("v.QLsOptions", QLsOptions);
                    component.set("v.orderedQLLabelsIds",orderedQuoteLineLabelsIds);
                    //console.log("orderedQuoteLineLabelsIds=> " + JSON.stringify(component.get("v.orderedQLLabelsIds")));
                    component.set("v.quoteLineLabelsMap", quoteLineLabelsMap);
                    //console.log("quoteLineLabelsMap=> " + JSON.stringify(component.get("v.quoteLineLabelsMap")));
                    component.set("v.quoteLinesMap", quoteLinesMap);
                    //console.log("quoteLinesMap=> " + JSON.stringify(component.get("v.quoteLinesMap")));
                }
            }
        });
        
        $A.enqueueAction(actionQLs);
    },
    
    prepareDataTable: function(component, event){
        this.setupDataTable(component, event);
        component.set('v.columns', component.get('v.columnsStep1'));
        //console.log('component.get("v.QuoteObj").SBQQ__StartDate__c '+component.get("v.QuoteObj").SBQQ__StartDate__c);
        var action = component.get("c.getActiveDraftMasterSubsFromAccount");
        action.setParams({
            accountId: component.get("v.QuoteObj").SBQQ__Account__c,
            startDate: component.get("v.QuoteObj").SBQQ__StartDate__c,
            quoteId: component.get("v.QuoteObj").Id //Added for SPIII-4655 + 4643
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let subscriptionsMap = {};
                let allSubscriptionsMap = {};
                var result = response.getReturnValue();
                var subscriptionsArray = [];
                var subscriptionIds = [];
                var subscriptionsByProductGroupAndPlaceMap = {};
                var selectedRows = [];
                //console.log("--->" + JSON.stringify(result));
                if(result){
                    if(result.length == 0){
                        var showMessageCMP = component.find("showMessageNoProduct");
                        component.set("v.message",$A.get("$Label.c.Substitution_No_Products_available"));
                        $A.util.removeClass(showMessageCMP,"slds-hide");
                        $A.util.addClass(showMessageCMP,"slds-show");
                    }
                    for(let i = 0; i < result.length; i++){
                        var available = true;
                        var quoteId = component.get("v.QuoteObj").Id;
                        var subscription = result[i];
                        var subscriptionIds = [];
                        
                        if((subscription.SBQQ__Product__c && subscription.SBQQ__Product__r.Product_Group__c) 
                           && subscription.Place__c){
                            if(!subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c]){
                                subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c] = {};
                                if(!subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c])
                                    subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c] = [subscription.Id];
                                else subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c].push(subscription.Id);
                            }
                            else{
                                if(!subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c])
                                    subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c] = [subscription.Id];
                                else subscriptionsByProductGroupAndPlaceMap[subscription.SBQQ__Product__r.Product_Group__c][subscription.Place__c].push(subscription.Id);
                            }
                        }
                        if(subscription.Replaced_by_Product__r){
                            if(quoteId !== subscription.Replaced_by_Product__r.SBQQ__Quote__c){
                                available = false;
                            }else{
                                selectedRows.push(subscription.Id);
                            }
                            subscription.qlName = subscription.Replaced_by_Product__r.Name;
                            subscription.qlUrl = '/' + subscription.Replaced_by_Product__c;
                            subscription.qlProductName = subscription.Replaced_by_Product__r.SBQQ__Product__r.Name;
                            subscription.qlConfigurationModel = subscription.Replaced_by_Product__r.SBQQ__Product__r.Configuration_Model__c;
                            subscription.qlSubscriptionTerm = subscription.Replaced_by_Product__r.Subscription_Term__c;
                            subscription.qlStartDate = subscription.Replaced_by_Product__r.SBQQ__StartDate__c;
                            subscription.qlEndDate = subscription.Replaced_by_Product__r.SBQQ__EndDate__c;
                            if(subscription.Replaced_by_Product__r.Place__r){
                                subscription.qlPlaceName = subscription.Replaced_by_Product__r.Place__r.Name;
                                subscription.qlPlaceUrl = '/' + subscription.Place__c;
                            }
                        }
                        subscription.recordUrl = '/' + subscription.Id;
                        subscription.contractUrl = '/' + subscription.SBQQ__Contract__c;
                        subscription.contractNumber = subscription.SBQQ__Contract__r.ContractNumber;
                        subscription.subProductName = subscription.SBQQ__Product__r.Name;
                        if(subscription.Place__r){
                            subscription.subPlaceName = subscription.Place__r.Name;
                            subscription.subPlaceUrl= '/' + subscription.Place__c;
                            subscription.placeId = subscription.Place__r.PlaceID__c;
                        }
                        subscription.subConfigurationModel = subscription.SBQQ__Product__r.Configuration_Model__c;
                        subscription.subSubscriptionTerm = subscription.Subscription_Term__c;
                        subscription.subStartDate = subscription.SBQQ__StartDate__c;
                        subscription.inTermination =  subscription.In_Termination__c;
                        subscription.subEndDate = subscription.SBQQ__EndDate__c;
                        subscription.subTerminationDate = subscription.SBQQ__TerminatedDate__c;
                        if(available){
                            subscriptionsMap[subscription.Id] = subscription;
                            subscriptionIds.push(subscription.Id);
                            subscriptionsArray.push(subscription);
                        }
                        allSubscriptionsMap[subscription.Id] = subscription;
                    }
                    
                    component.set("v.subscriptionsByProductGroupAndPlaceMap",subscriptionsByProductGroupAndPlaceMap);
                    component.set("v.subscriptionsMap", subscriptionsMap);
                    component.set("v.allSubscriptionsMap", allSubscriptionsMap);
                    component.set("v.subscriptionIds", subscriptionIds);
                    component.set("v.selectedRows",selectedRows);
                    console.log('VL - Selected Rows -> ' + selectedRows);
                    component.set("v.selectedSubscriptionIds", selectedRows);
                    component.set("v.initialSubscriptionIds", selectedRows);
                    if(selectedRows && selectedRows.length > 0){
                        component.set("v.IsOkButtonDisabled",false);
                    }
                    if(subscriptionsArray.length == 0){
                        var showMessageCMP = component.find("showMessageNoProduct");
                        component.set("v.message",$A.get("$Label.c.Substitution_No_Products_available"));
                        $A.util.removeClass(showMessageCMP,"slds-hide");
                        $A.util.addClass(showMessageCMP,"slds-show");
                    }
                    subscriptionsArray.sort(component.get("v.sortSubs"));
                }
                component.set('v.data', subscriptionsArray);
            }
        });
        
        $A.enqueueAction(action);
    },
    manageNextStep: function(component, event){
        var stepNumber = component.get("v.stepNumber");
        var dataTableCMP = component.find("dataTable");
        var spinnerBoxDiv = document.getElementById("spinnerBox");
        
        switch(stepNumber){
            case 1:
                //console.log("manageNextStep -> case1");
                var selectedSubscriptionIds = component.get("v.selectedSubscriptionIds");
                console.log('VL - Selected Subscription Ids ' + selectedSubscriptionIds);
                var subscriptionsMap = component.get("v.subscriptionsMap");
                //console.log('VL - Subscriptions Map ' + JSON.stringify(subscriptionsMap));
                var replacedSubscriptionIdsByQLIdMap = component.get("v.replacedSubscriptionIdsByQLIdMap");
                //console.log('VL - Replaced Subscription Ids By QL Id Map ' + JSON.stringify(subscriptionsMap));
                var replacedSubscriptionIdsByQLIdMapCLONE = Object.assign({},replacedSubscriptionIdsByQLIdMap);
                var data = [];
                var qlIdsBySubscriptionIdMap = {};
                for(var selectedSubscriptionId of selectedSubscriptionIds){
                    data.push(subscriptionsMap[selectedSubscriptionId]);
                    qlIdsBySubscriptionIdMap[selectedSubscriptionId] = (subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c)?subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c:'';
                    if(subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c){
                        if(!replacedSubscriptionIdsByQLIdMapCLONE[subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c])
                            replacedSubscriptionIdsByQLIdMapCLONE[subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c] = [];
                        replacedSubscriptionIdsByQLIdMapCLONE[subscriptionsMap[selectedSubscriptionId].Replaced_by_Product__c].push(selectedSubscriptionId);
                    }
                }
                data.sort(component.get("v.sortSubs"));
                component.set("v.qlIdsBySubscriptionIdMap", qlIdsBySubscriptionIdMap);
                component.set("v.replacedSubscriptionIdsByQLIdMap", replacedSubscriptionIdsByQLIdMapCLONE);
                
                component.set("v.selectedSubscriptionList", data);
                
                var myArr = component.get("v.QLsOptionsFilter_helper");
                var filteredArr = [];
                var selSubs = component.get("v.selectedSubscriptionList");
                selSubs.forEach(function(item){
                    filteredArr.push({key :item.Id , value: myArr[item.Id] })
                });
                
                component.set("v.QLsOptionsFilter",filteredArr); 
                component.set('v.BackButtonLabel', "Back");
                component.set('v.OkButtonLabel', "Confirm");
                component.set('v.OkButtonVariant', "success");
                //component.set("v.IsOkButtonDisabled",true);
                spinnerBoxDiv.style = "display: none;";
                component.set("v.isSecondStepEnabled", true);
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;
        }
    },
    managePreviousStep: function(component, event){
        var stepNumber = component.get("v.stepNumber");
        var dataTableCMP = component.find("dataTable");
        var dataTableStep2CMP = component.find("dataTable-step2");
        var spinnerBoxDiv = document.getElementById("spinnerBox");
        
        switch(stepNumber){
            case 1:
                this.closeQuickAction(component, event);
                break;
            case 2:
                var subscriptionIds = component.get("v.subscriptionIds");
                var subscriptionsMap = component.get("v.subscriptionsMap");
                
                component.set("v.selectedRows",component.get("v.selectedSubscriptionIds"));
                component.set("v.hideCheckboxColumn",false);
                component.set('v.BackButtonLabel', "Cancel");
                component.set('v.OkButtonLabel', "Proceed");
                component.set('v.OkButtonVariant', "brand");
                spinnerBoxDiv.style = "display: none;";
                component.set("v.IsOkButtonDisabled",false);
                
                $A.util.removeClass(dataTableCMP, "slds-hide");
                $A.util.addClass(dataTableCMP, "slds-show");
                
                break;
            case 3:
                break;
            default:
                this.closeQuickAction(component, event);
                break;
        }
    },
    
    updateReplacedSubscriptions : function(component,event){
        var recordId = component.get("v.QuoteObj").Id;
        var showMessageCMP = component.find("showMessage");
        var action = component.get("c.updateReplacedSubscriptions");
        var initialSubscriptionIds = component.get("v.initialSubscriptionIds");
        const qlIdsBySubIdMap = component.get("v.qlIdsBySubscriptionIdMap");
        
        if(initialSubscriptionIds && initialSubscriptionIds.length > 0){
            for(var initialSubscriptionId of initialSubscriptionIds){
                if(!qlIdsBySubIdMap.hasOwnProperty(initialSubscriptionId)){
                    qlIdsBySubIdMap[initialSubscriptionId] = null;
                }
            }
        }
        action.setParams({
            qlIdsBySubscriptionIdMap: qlIdsBySubIdMap,
            quoteId: recordId
        });
        
        component.set("v.message","");
        $A.util.removeClass(showMessageCMP,"slds-show");
        $A.util.addClass(showMessageCMP,"slds-hide");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //console.log("--->" + JSON.stringify(result));
                if(result.info.status === 'OK'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": $A.get("$Label.c.ProductSubstitution_QuoteLines_Successfully_Assigned"),
                        "type": "success",
                        "mode": "sticky"
                    });
                    toastEvent.fire();
                    this.closeQuickAction(component, event);
                    setTimeout(function(){ window.location.reload();},1000);
                }else{
                    component.set("v.message","Something has gone wrong!");
                    $A.util.removeClass(showMessageCMP,"slds-hide");
                    $A.util.addClass(showMessageCMP,"slds-show");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    confirmationButton: function(component, event){
        var isErrorOnConfirmationBtn = this.isErrorOnConfirmationButton(component, event);
        //console.log("isErrorOnConfirmationBtn -> " + isErrorOnConfirmationBtn);
        if(!isErrorOnConfirmationBtn){
            this.blockSubstitutionByPlace(component,event);
        }else{
            var showMessageCMP = component.find("showMessage");
            //console.log("ERROR THROWN!!");
            component.set("v.message",$A.get("$Label.c.ProductSubstitution_LinkSubscriptionToQuoteline"));
            $A.util.removeClass(showMessageCMP,"slds-hide");
            $A.util.addClass(showMessageCMP,"slds-show");
        }
    },
    quoteLinesValidation: function(component, event){
        const subscriptionsByProductGroupAndPlaceMap = component.get("v.subscriptionsByProductGroupAndPlaceMap");
        const quoteLinesByProductGroupAndPlaceMap = component.get("v.quoteLinesByProductGroupAndPlaceMap");
        const qlIdsBySubscriptionIdMap = component.get("v.qlIdsBySubscriptionIdMap");
        const quoteLinesMap = component.get("v.quoteLinesMap");
        const replacedSubscriptionIdsByQLIdMap = component.get("v.replacedSubscriptionIdsByQLIdMap");
        
        //console.log("+++replacedSubscriptionIdsByQLIdMap-> " + JSON.stringify(replacedSubscriptionIdsByQLIdMap));
        //console.log("+++subscriptionsByProductGroupAndPlaceMap-> " + JSON.stringify(subscriptionsByProductGroupAndPlaceMap));
        //console.log("+++qlIdsBySubscriptionIdMap-> " + JSON.stringify(qlIdsBySubscriptionIdMap));
        //console.log("+++quoteLinesMap-> " + JSON.stringify(quoteLinesMap));
        var isOK = true;
        console.log("VL - Replaced Subscription Ids By QL Id Map " + JSON.stringify(replacedSubscriptionIdsByQLIdMap));
        for(const qlId of Object.getOwnPropertyNames(replacedSubscriptionIdsByQLIdMap)){
            var subscriptionIds = replacedSubscriptionIdsByQLIdMap[qlId];
            var quote = quoteLinesMap[qlId];
            var quotePlaceId = quoteLinesMap[qlId].Place__c;
            var quoteProductGroup;
            
            //console.log(">>>X qlId -> " + qlId);
            //console.log(">>>X subscriptionIds -> " + JSON.stringify(subscriptionIds));
            console.log("VL - Subscriptions By Product Group And Place Map " + JSON.stringify(subscriptionsByProductGroupAndPlaceMap));
            
            var includes = false;
            if(quoteLinesMap[qlId].SBQQ__Product__c)
                quoteProductGroup = quoteLinesMap[qlId].SBQQ__Product__r.Product_Group__c;
            if(subscriptionIds.length > 0 && quoteProductGroup && quotePlaceId && subscriptionsByProductGroupAndPlaceMap[quoteProductGroup] && subscriptionsByProductGroupAndPlaceMap[quoteProductGroup][quotePlaceId]){
                subscriptionsByProductGroupAndPlaceMap[quoteProductGroup][quotePlaceId].forEach(function(currentSub){
                    if(subscriptionIds.includes(currentSub)) includes = true;
                });
            }
            else includes = true;
            
            if(!includes) isOK = false;
        }
        
        var showMessageCMP = component.find("showMessage");
        
        if(!isOK){
            component.set("v.message",$A.get("$Label.c.ProductSubstitution_Relate_subscription_with_same_Place"));
            $A.util.removeClass(showMessageCMP,"slds-hide");
            $A.util.removeClass(showMessageCMP,"slds-show");
        }else{
            component.set("v.message","");
            $A.util.removeClass(showMessageCMP,"slds-show");
            $A.util.addClass(showMessageCMP,"slds-hide");
        }
        
        return isOK;
    },
    
    blockSubstitutionByPlace: function(component, event){
        var recordId = component.get("v.recordId");
        var urlString = window.location.href;
        var baseUrl = window.location.origin;
        
        component.set('v.recordId',baseUrl.length+28,urlString.lastIndexOf('/'));
        recordId = urlString.substring(baseUrl.length+28,urlString.lastIndexOf('/'));
        var isOk = true;
        var qlIdsBySubscriptionIdMap = component.get("v.qlIdsBySubscriptionIdMap"); 
        var quoteLinesMap = component.get("v.quoteLinesMap"); 
        var action = component.get("c.blockProductSellingOnSamePlace");
        var showMessageCMP = component.find("showMessage");
        action.setParams({
            quoteId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                //console.log("blockProductSellingOnSamePlace--->" + JSON.stringify(result));
                //console.log("qlIdsBySubscriptionIdMap--->" + JSON.stringify(qlIdsBySubscriptionIdMap));
                for(var subId of Object.getOwnPropertyNames(result)){
                    if(!qlIdsBySubscriptionIdMap[subId]){
                        component.set("v.message",result[subId][Object.keys(result[subId])]);
                        $A.util.removeClass(showMessageCMP,"slds-hide");
                        $A.util.removeClass(showMessageCMP,"slds-show");
                        isOk = false;
                        break;
                    }
                }
                if(isOk) {
                    isOk = this.quoteLinesValidation(component, event);
                    if(isOk)
                        this.updateReplacedSubscriptions(component, event);
                }
            }
            else{
                component.set("v.message","Something has gone wrong!");
                $A.util.removeClass(showMessageCMP,"slds-hide");
                $A.util.addClass(showMessageCMP,"slds-show");
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    setupDataTable: function(component,event){
        component.set('v.columnsStep1', [
            { label: 'Contract Number', fieldName: 'contractUrl', type: 'url', initialWidth: 150,
             typeAttributes: {
                 label: { fieldName: 'contractNumber' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Subscription Name', fieldName: 'recordUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'Name' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Product Name', fieldName: 'subProductName', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'left' }},
            { label: 'Place', fieldName: 'subPlaceUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'subPlaceName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'center' }
            },
            { label: 'Place ID', fieldName: 'placeId', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'left' }},
            { label: 'Configuration Model', fieldName: 'subConfigurationModel', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'center' }},
            { label: 'Subscription Term', fieldName: 'subSubscriptionTerm', type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Start Date', fieldName: 'subStartDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'End Date', fieldName: 'subEndDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'In Termination', fieldName: 'inTermination', type: 'boolean', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Termination Date', fieldName: 'subTerminationDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line', fieldName: 'qlUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'qlName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Quote Line Product Name', fieldName: 'qlProductName', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'left' }},
            { label: 'Quote Line Place', fieldName: 'qlPlaceUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'qlPlaceName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'center' }
            },
            { label: 'Quote Line Configuration Model', fieldName: 'qlConfigurationModel', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line Subscription Term', fieldName: 'qlSubscriptionTerm', type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line Start Date', fieldName: 'qlStartDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line End Date', fieldName: 'qlEndDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }}
        ]);
        
        component.set("v.columnsQLs", [
            { label: 'Quote Line', fieldName: 'qlUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'qlName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Quote Line Product Name', fieldName: 'qlProductName', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'left' }},
            { label: 'Quote Line Place', fieldName: 'qlPlaceUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                 label: { fieldName: 'qlPlaceName' },
                 target:'_blank'
             },
             cellAttributes: { alignment: 'center' }
            },
            { label: 'Quote Line Place ID', fieldName: 'qlPlaceID', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line Configuration Model', fieldName: 'qlConfigurationModel', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line Subscription Term', fieldName: 'qlSubscriptionTerm', type: 'text', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line Start Date', fieldName: 'qlStartDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }},
            { label: 'Quote Line End Date', fieldName: 'qlEndDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'center' }}
        ]);
    },
    enableConfirmButton: function(component,event, qlIdsBySubscriptionIdMap){
        if(!qlIdsBySubscriptionIdMap)
            qlIdsBySubscriptionIdMap = component.get("v.qlIdsBySubscriptionIdMap");
        
        var isConfirmEnabled = true;
        for(const [subscriptionId, qlId] of Object.entries(qlIdsBySubscriptionIdMap)){
            if(!qlIdsBySubscriptionIdMap[subscriptionId]){
                isConfirmEnabled = false;
                break;
            }
        }
        
        component.set("v.IsOkButtonDisabled", !isConfirmEnabled);
    }, 
    isErrorOnConfirmationButton: function(component, event){
        var qlIdsBySubscriptionIdMap = component.get("v.qlIdsBySubscriptionIdMap");
        
        var isConfirmEnabled = true;
        for(const [subscriptionId, qlId] of Object.entries(qlIdsBySubscriptionIdMap)){
            if(!qlIdsBySubscriptionIdMap[subscriptionId]){
                isConfirmEnabled = false;
                break;
            }
        }
        
        return !isConfirmEnabled;
    }
    ,
    closeQuickAction: function(component, event){
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    }
})