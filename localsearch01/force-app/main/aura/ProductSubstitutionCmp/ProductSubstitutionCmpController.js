({
	doInit: function(component, event, helper)
    {      
     	var recordId = component.get("v.recordId");
        var urlString = window.location.href;
        var baseUrl = window.location.origin;
					
        component.set('v.recordId',baseUrl.length+28,urlString.lastIndexOf('/'));
        recordId = urlString.substring(baseUrl.length+28,urlString.lastIndexOf('/'));
     
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        component.set("v.baseURL", baseURL);
 
        var sortSubs = function(a, b){
            var contractNumberA=a.contractNumber.toLowerCase();
            var contractNumberB=b.contractNumber.toLowerCase();
            var subscriptionNameA=a.Name.toLowerCase();
            var subscriptionNameB=b.Name.toLowerCase();
            
            if(contractNumberA == contractNumberB){
                return (subscriptionNameA < subscriptionNameB) ? -1 : (subscriptionNameA > subscriptionNameB) ? 1 : 0;
            }else{
                return (contractNumberA < contractNumberB) ? -1 : 1;
            }
        };
        
        component.set("v.sortSubs", sortSubs);
        //helper.getAccess(component, event);
        helper.getData(component, event);
    },
    getSelectedRows: function(component, event, helper){
        var minQLStartDate = component.get('v.minQLStartDate');
        var selectedRows = event.getParam('selectedRows');
        var selectedSubscriptionIdsSet = [];
        var IsOkButtonDisabled = true;
        if(selectedRows && selectedRows.length > 0){
            for(let selectedRow of selectedRows){
                if(JSON.stringify(selectedRow.In_Termination__c)=='true' && !selectedRow.Replaced_by_Product__c)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": 'Warning',
                        "message":  $A.get("$Label.c.Substitution_SubInTermination_Validation"),
                        "mode": 'ERROR'
                    });
                    toastEvent.fire();
                    IsOkButtonDisabled = true;     
                }
                else if(selectedRow.SBQQ__SubscriptionType__c == $A.get("$Label.c.Generic_Renewable_SubType") && selectedRow.End_Date__c < minQLStartDate){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Warning"),
                        "type": 'Warning',
                        "message":  $A.get("$Label.c.Substitution_SubEndDate_Validation"),
                        "mode": 'ERROR'
                    });
                    toastEvent.fire();
                    IsOkButtonDisabled = true; 
                }
                else selectedSubscriptionIdsSet.push(selectedRow.Id);
                
            }
            if(selectedSubscriptionIdsSet.length >0) IsOkButtonDisabled = false;  
            component.set("v.selectedSubscriptionIds",selectedSubscriptionIdsSet);
            component.set("v.selectedRows",selectedSubscriptionIdsSet);
            component.set("v.IsOkButtonDisabled",IsOkButtonDisabled);
        }else{
            component.set("v.selectedSubscriptionIds",[]);
            component.set("v.selectedRows",[]);
            component.set("v.IsOkButtonDisabled",IsOkButtonDisabled);
        }
    },
    okButtonOnClick: function(component, event, helper){
        var stepNumber = component.get("v.stepNumber");
        var dataTableCMP = component.find('dataTable');
        var div = document.getElementById("spinnerBox");
        
        switch(stepNumber){
            case 1:
                $A.util.removeClass(dataTableCMP, "slds-show");
                $A.util.addClass(dataTableCMP,"slds-hide");
                div.style = "text-align: center; height: 300px; padding-top: 150px; display: block;";
                window.setTimeout(
                    $A.getCallback(function() {
                        var div = document.getElementById("spinnerBox");
                        helper.manageNextStep(component,event);
                        component.set("v.stepNumber", component.get("v.stepNumber")+1);
                        component.set("v.IsOkButtonDisabled", false);
                    }), 500
                );
                break;
            case 2:
                helper.confirmationButton(component, event);
                break;
            default:
                break;
        }
    },
    backButtonOnClick: function(component, event, helper){
        const showQuoteLines = component.get("v.showQuoteLines");
        const hideQuoteLines = component.get("v.hideQuoteLines");
        const showHideButtonLabel = component.get("v.showHideButtonLabel");
        var stepNumber = component.get("v.stepNumber");
        var div = document.getElementById("spinnerBox");
        component.set("v.showHideButtonLabel", showQuoteLines);
        switch(stepNumber){
            case 1:
                helper.managePreviousStep(component, event);
                break;
            case 2:
                component.set("v.isSecondStepEnabled", false);
                div.style = "text-align: center; height: 300px; padding-top: 150px; display: block;";
                window.setTimeout(
                    $A.getCallback(function() {
                        helper.managePreviousStep(component,event);
                        component.set("v.stepNumber", component.get("v.stepNumber")-1);
                    }), 500
                );
                break;
            default:
                helper.managePreviousStep(component, event);
                break;
        }
    },
    handleChange: function(component, event, helper){
        var target = event.getSource();
        var subscriptionId = target.get("v.name");
        var quoteLineId = target.get("v.value");
        var replacedSubscriptionIdsByQLIdMap = component.get("v.replacedSubscriptionIdsByQLIdMap");
        var qlIdsBySubscriptionIdMap = component.get("v.qlIdsBySubscriptionIdMap");
        var replacedSubscriptionIdsByQLIdMapCLONE = Object.assign({},replacedSubscriptionIdsByQLIdMap);
        var qlIdsBySubscriptionIdMapCLONE = Object.assign({},qlIdsBySubscriptionIdMap);
        
        if(quoteLineId){
            if(!replacedSubscriptionIdsByQLIdMapCLONE[quoteLineId])
                replacedSubscriptionIdsByQLIdMapCLONE[quoteLineId] = [];
            replacedSubscriptionIdsByQLIdMapCLONE[quoteLineId].push(subscriptionId);
            if(qlIdsBySubscriptionIdMapCLONE[subscriptionId]){
                var index = replacedSubscriptionIdsByQLIdMapCLONE[qlIdsBySubscriptionIdMapCLONE[subscriptionId]].indexOf(subscriptionId);
                replacedSubscriptionIdsByQLIdMapCLONE[qlIdsBySubscriptionIdMapCLONE[subscriptionId]].splice(index,1);
            }
            
            qlIdsBySubscriptionIdMapCLONE[subscriptionId] = quoteLineId;
        }else{
            if(qlIdsBySubscriptionIdMapCLONE[subscriptionId]){
                var index = replacedSubscriptionIdsByQLIdMapCLONE[qlIdsBySubscriptionIdMapCLONE[subscriptionId]].indexOf(subscriptionId);
                replacedSubscriptionIdsByQLIdMapCLONE[qlIdsBySubscriptionIdMapCLONE[subscriptionId]].splice(index,1);
                qlIdsBySubscriptionIdMapCLONE[subscriptionId] = '';
            }
        }
        component.set("v.replacedSubscriptionIdsByQLIdMap", replacedSubscriptionIdsByQLIdMapCLONE);
        component.set("v.qlIdsBySubscriptionIdMap", qlIdsBySubscriptionIdMapCLONE);
    },
    showHideButtonLabelHandleClick: function(component, event, helper){
        const showQuoteLines = component.get("v.showQuoteLines");
        const hideQuoteLines = component.get("v.hideQuoteLines");
        const showHideButtonLabel = component.get("v.showHideButtonLabel");
        const dataTableQLsCMP = component.find("dataTableQLs");
        const dataTableStep2 = component.find("dataTable-step2");
        
        if(showHideButtonLabel === hideQuoteLines){
            component.set("v.showHideButtonLabel", showQuoteLines);
            $A.util.removeClass(dataTableQLsCMP,"slds-show");
            $A.util.addClass(dataTableQLsCMP,"slds-hide");
            $A.util.removeClass(dataTableStep2,"slds-hide");
            $A.util.addClass(dataTableStep2,"slds-show");
        }else if(showHideButtonLabel === showQuoteLines){
            component.set("v.showHideButtonLabel", hideQuoteLines);
            $A.util.removeClass(dataTableStep2,"slds-show");
            $A.util.addClass(dataTableStep2,"slds-hide");
            $A.util.removeClass(dataTableQLsCMP,"slds-hide");
            $A.util.addClass(dataTableQLsCMP,"slds-show");
        }
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
})