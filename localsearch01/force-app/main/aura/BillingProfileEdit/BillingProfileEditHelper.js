/*
CHANGELOG:
            #1 - SF2-253 - Validate Account and Billing Address on Quote Creation
                 SF2-264 - Legal Address Validation
                 gcasola 07-22-2019
                 */
({
    //Vincenzo Laudato 2019-05-29 US #93 Sprint 2, Wave 1 *** START
    handleValidationResult : function(component,event,fields, params) {
        //fields.AddressValidated__c = $A.get("$Label.c.Address_Validated_Validated");
        var fieldName = "Billing_Country__c";
        console.log(">>>fields: " + JSON.stringify(fields));
        
        if(params)
        {
            console.log(">>>params: " + JSON.stringify(params));
            for(var fieldName in params)
            {
                if(fieldName == "AddressValidationDate__c")
                {
                    fields[fieldName] = new Date(params[fieldName]).toISOString();
                }else
                	fields[fieldName] = params[fieldName];
            }
            //params.forEach(element => console("element: " + element));
        }
        
        console.log('fields: ' + JSON.stringify(fields)); 
		component.find('reForm').submit(fields);
    },
    
    handleWarningQuoteAndSubscription : function(component,event,helper) {
        var action2 = component.get("c.getQuoteAndSubscription");
        action2.setParams({
            "billingProfileId" : component.get('v.recordId')
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state of action '+state);
            if (state === "SUCCESS") {
                var mapOfV = response.getReturnValue();
                console.log(mapOfV);
                if(typeof mapOfV != 'undefined' && mapOfV){
                    var listOfQuote = mapOfV['Quote'];
                    var listOfContract = mapOfV['Contract'];
                    console.log('list of quote '+listOfQuote);
                    console.log('list of contract '+listOfContract);
                    if(listOfQuote.length >0 || listOfContract.length >0){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "warning",
                            "title": "Warning Message",
                            "message": "There are Contract or Quote related to this Billing Profile"
                        });
                        toastEvent.fire();
                    }
                    if(listOfQuote.length > 0){   
                        component.set("v.quoteEmpty",false);
                        component.set("v.quoteToDisplay",listOfQuote);
                    }
                    if(listOfContract.length > 0){           
                        component.set("v.subEmpty",false);
                        component.set("v.contractToDisplay",listOfContract);
                    }                    
                }
            } else { 
            }
        });
        
        $A.enqueueAction(action2);
    },
    
    
    
    checkIntegrationStatus : function(component){
        var action = component.get('c.checkIntegrationStatus');
        action.setCallback(this, function(response) {
       		var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
            		var isActive = response.getReturnValue();  
 					component.set('v.isActive', isActive);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //END
})