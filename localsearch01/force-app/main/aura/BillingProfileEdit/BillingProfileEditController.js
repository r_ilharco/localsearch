/*
CHANGELOG:
			#1 - SF2-218, Wave C, Sprint 6, Logging - Error Handling - 07-24-2019 gcasola
*/
({
	doInit : function(component, event, helper) {
        helper.handleWarningQuoteAndSubscription(component, event, helper);
        /*
        var action2 = component.get("c.getQuoteAndSubscription");
        action2.setParams({
            
            "billingProfileId" : component.get('v.recordId')
            
        });
        action2.setCallback(this, function(response) {
            
            var state = response.getState();
            console.log('state of action '+state);
            if (state === "SUCCESS") {
            	
                var mapOfV = response.getReturnValue();
                console.log(mapOfV);
                
                if(typeof mapOfV != 'undefined' && mapOfV){
                                       
                    var listOfQuote = mapOfV['Quote'];
                    var listOfContract = mapOfV['Contract'];
                    console.log('list of quote '+listOfQuote);
                    console.log('list of contract '+listOfContract);
                    
                    if(listOfQuote.length >0 || listOfContract.length >0){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "warning",
                            "title": "Warning Message",
                            "message": "There are Contract or Quote related to this Billing Profile"
                        });
                        toastEvent.fire();
                    }
                    
                    if(listOfQuote.length > 0){
                        
                        component.set("v.quoteEmpty",false);
                        component.set("v.quoteToDisplay",listOfQuote);
                    }
                    
                    if(listOfContract.length > 0){
                        
                        component.set("v.subEmpty",false);
                        component.set("v.contractToDisplay",listOfContract);
                    }
                    
                }
            
            } else {
                
                
            }
            
        });        
        $A.enqueueAction(action2);
        */
        component.find("forceBpRecord").getNewRecord(
            "Billing_Profile__c",
            null,
            false,
            $A.getCallback(function() {
                var recId = component.get('v.recordId');
                //console.log('[BillinProfileController.js - doInit] recordId: ' + recId);
                if(recId) {
                    var action = component.get('c.getDefaultBPValues');
                    action.setParams({relatedId:recId});
                    action.setCallback(this, function(response) {
                        var map = response.getReturnValue();
                        // START GK5
                        console.log('map[\'IdBPDefault\']: ' + map['IdBPDefault']);
                        component.set('v.IdBPDefault', map['IdBPDefault']);
                        if(component.get("v.IdBPDefault")==false)
                        {
                            console.log("Be careful, you don\'t have any Billing Profile as Default.");
                            var content = document.getElementById('notify__content');
                            var container = document.getElementById('container');

                            content.innerHTML = "";
                            var textnode = document.createTextNode("Be careful, you don't have any Billing Profile as Default.");
                            content.appendChild(textnode);
                            container.classList.remove("slds-hide");
                        }
                        component.set('v.Is_Default__c', map['Is_Default__c']);
                        // END GK5
                    });
                    $A.enqueueAction(action);
                }
            })
        );
        var action2 = component.get('c.getFieldsProperties');
        action2.setParams({recordId: component.get("v.RecordId"), fieldAPINames: '[]'});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result)
                {
                    component.set("v.FieldsAttributes", result);
                }
            }
        });
        $A.enqueueAction(action2);
        
        component.set("v.bRec.Id", component.get("v.recordId"));
        //Vincenzo Laudato 2019-05-29 US #93 Sprint 2, Wave 1 *** START
        helper.checkIntegrationStatus(component);
	},
    cancelDialog : function(component, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
        "recordId": component.get("v.recordId"),
        "slideDevName": "related"
        });
        navEvt.fire();
    },
    // START GK5
    is_DefaultChanged: function(component, event, helper)
    {
        console.log('v.IdBPDefault' + component.get("v.IdBPDefault"));
        console.log('component.get(\'v.bpRec.Is_Default__c\') ' + component.get('v.bpRec.Is_Default__c') );
        console.log('component.get(\'v.Is_Default__c\')' + component.get('v.Is_Default__c'));
        console.log('event' + JSON.stringify(event));
        console.log('event.getParam("checked")' + event.getParam("checked"));
        var recordPage =  'https://' + window.location.hostname + '/' + component.get('v.IdBPDefault');
        console.log('Site: ' + recordPage);
        
        if((component.get('v.Is_Default__c')!=event.getParam("checked") && component.get('v.Is_Default__c')==true && component.get("v.IdBPDefault")!=false) ||
        (event.getParam("checked")==false && component.get('v.Is_Default__c')==false && component.get("v.IdBPDefault")==false))
        {
            console.log("Be careful, you don\'t have any Billing Profile as Default.");
            var content = document.getElementById('notify__content');
            var container = document.getElementById('container');

            content.innerHTML = "";
            var textnode = document.createTextNode("Be careful, you don't have any Billing Profile as Default.");
            content.appendChild(textnode);
            container.classList.remove("slds-hide");

        }else if (component.get('v.Is_Default__c')!=event.getParam("checked") && component.get("v.IdBPDefault")!=false)
        {
            console.log("Be careful, you have already a Billing Profile as Default.");
            var content = document.getElementById('notify__content');
            var container = document.getElementById('container');
            

            content.innerHTML = "";
            var textnode = document.createTextNode('You already have a default billing profile.');
            var br = document.createElement("br");
            var textnode2 = document.createTextNode(" Click Submit if you want to set this one as the new default.");
            /*var linktext = document.createTextNode("billing profile");
            var anchor = document.createElement("a");
            anchor.appendChild(linktext);
            anchor.title = "billing profile";
            anchor.href = recordPage;
            anchor.target = "_blank";
            anchor.rel = "noreferrer noopener"
            */
            content.appendChild(textnode);
            //content.appendChild(anchor);
            //content.appendChild(document.createTextNode("."));
            content.appendChild(br);
            content.appendChild(textnode2);

            container.classList.remove("slds-hide");

        }else
        {
            var container = document.getElementById('container');
            container.classList.add("slds-hide");
        }
    },
    //END GK5
    handleSuccess : function(component, event, helper) {
        //helper.handleWarningQuoteAndSubscription(component, event, helper);
        event.preventDefault();
        $A.get('e.force:refreshView').fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({"title": "Record Saved","message": "The Billing profile has been saved.","type": "success"});
        toastEvent.fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
        "recordId": component.get("v.recordId"),
        "slideDevName": "related"
        });
        navEvt.fire();
        return true;
    },
    handleError : function(component, event, helper) {
        var resultsToast = $A.get("e.force:showToast");
        var resp = event.getParams();
        console.log('response error'+JSON.stringify(event));
        var scrollOptions = {left: 0,top: 0,behavior: 'smooth'};
        if(!$A.util.isEmpty(resp.errorCode)){
            console.log('error0');
            resultsToast.setParams({
                "title": "Error",
                "message": resp.errorCode + '\n' + resp.message,
                "type": "error"
            });
            resultsToast.fire();
        }else if(!$A.util.isEmpty(event._params.error.body.message)){
            console.log('error01');
            component.set('v.privilegeError',false);          
            window.scrollTo(scrollOptions);
        }else{
            console.log('error02');
            component.set('v.privilegeError',true);
        }
        component.set('v.isLoading',false);
        return false;
    },

    handleSubmit: function(component, event, helper) {
        event.preventDefault();
        var fields = event.getParam('fields');
        //console.log("[BillingProfileController.js - handleSubmit] fields: " + JSON.stringify(fields));
        //console.log("[BillingProfileController.js - handleSubmit] v.bpRec: " + JSON.stringify(component.get("v.bpRec")));
        //console.log('[BillingProfileController.js - handleSubmit] fields: ' + JSON.stringify(fields));

        var addrChanged = component.get('v.changedAddr');
        var isActive = component.get('v.isActive');
        var channels = fields.Channels__c;
        var isPrint = channels.includes($A.get("$Label.c.Billing_Channel_Print"));     
        var POBoxPostalCode = fields.P_O_Box_Zip_Postal_Code__c; 

        //A. Marotta 27-05-19 Start
        //US #93, Wave 1, Sprint 2 - Address validation check
        if(addrChanged && isPrint && isActive){
            var action = component.get('c.checkBillingProfileLegalAddress');
            var companyId = component.find('Customer__c').get('v.value');
            var street = fields.Billing_Street__c;
            var zip = fields.Billing_Postal_Code__c;
            var city = fields.Billing_City__c; 
            var country = fields.Billing_Country__c ; 
            console.log('street : '+street+' zip: '+zip+' city: '+city)
            /**** Sprint 8  - SF2 - 333 Validation on Zip Code ****/

            /*A.L. Marotta 25-09-19 Start
            SF2-371 Wave E, Sprint 10 - errors on wrong zip or POBoxPostalCode are now thrown separatedly*/
            
            /*if(((zip != null && zip !='') && ( zip[0] == 0 || zip.length > 4 || zip.length < 4 || isNaN(zip))) || 
               ((POBoxPostalCode !=null && POBoxPostalCode!='' ) && ( POBoxPostalCode[0]==0 || POBoxPostalCode.length < 4 || POBoxPostalCode.length > 4 || isNaN(POBoxPostalCode)))){
                component.set('v.error','Billing Postal Code and P.O. Box Zip/Postal Code must never starts with a 0 and must have 4 digits');
                    return false;            
            }*/
            
            if((zip != null && zip !='') && ( zip[0] == 0 || zip.length > 4 || zip.length < 4 || isNaN(zip))){
                component.set('v.error','Billing Postal Code must never start with a 0 and must have 4 digits');
                return false;            
            }
            if((POBoxPostalCode !=null && POBoxPostalCode!='' ) && ( POBoxPostalCode[0]==0 || POBoxPostalCode.length < 4 || POBoxPostalCode.length > 4 || isNaN(POBoxPostalCode))){
                component.set('v.error','P.O. Box Zip/Postal Code must never start with a 0 and must have 4 digits');
                return false;
            }
            if(!street || !zip || !city || !country){
                component.set('v.error',$A.get("$Label.c.BP_AddressValidation") );
               	return false;
            }

            //A.L. Marotta 25-09-19 End
            
            /**** Sprint 8  - SF2 - 333 Validation on Zip Code ****/
            
            
            var email = fields.Mail_address__c;
            var phone = fields.Phone__c;
            
            action.setParams({
                'recordId' : component.get('v.recordId'),
                'companyId' : companyId,
                'street' : street,
                'postalCode' : zip,
                'city' : city,
                'uid' : null,
                'email' : email,
                'phone' : phone,
                'editMode' : true
            });
            //var validationResult = false; 
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state: ' + state);
                if (state === "SUCCESS") {
                    var avResult = JSON.parse(response.getReturnValue());
                    console.log('avResult: ' + JSON.stringify(avResult));
                    var returnString = avResult.result;
                    if(returnString=="OK"){
                        helper.handleValidationResult(component,event,fields, avResult.params);
                    }else{
                         if(avResult && avResult.error && avResult.error.error_code){
                              component.set('v.error', avResult.error.error_code);
                        }else{
                              component.set('v.error', $A.get("$Label.c.Address_not_validated"));
                        }                
                        return false;
                    }
                }else{
                    component.set('v.error','Internal error');
                    return false;
                }
            });
            $A.enqueueAction(action);
            
        }
        else{
            component.find('reForm').submit(fields);
        }
        //A. Marotta 27-05-19 End
    },
    //Vincenzo Laudato 2019-05-29 User Story #93 Sprint 2, Wave 1 *** Start
    recordUpdate : function (component, event, helper){
        var eventParams = event.getParams();
        var loaded = component.get('v.loaded');
        if(eventParams.changeType === "LOADED" && loaded){
			var loadedRecord = component.get('v.bpRec');
            console.log('****************LR'+JSON.stringify(loadedRecord));
            console.log('*********'+loadedRecord.Customer__c);
            component.set('v.customerId', loadedRecord.Customer__c);
            component.set('v.loaded', false);
        }
    },
    
    setAddressChanged : function(component, event, helper){
        if(!component.get("v.changedAddr")){
        	component.set("v.changedAddr",true);
        }
    },
    //End

    handleCloseClick: function (cmp, event, helper) {
        //cmp.set("v.visible", false);
        var container = document.getElementById('container');
        container.classList.add("slds-hide");
	},
    handleLoad: function (cmp, event, helper) {
        cmp.set("v.isLoading", false);
	},
    handleShowActiveSectionName: function (cmp, event, helper) {
        alert(cmp.find("accordion").get('v.activeSectionName'));
    },
    handleSetActiveSectionC: function (cmp) {
        cmp.find("accordion").set('v.activeSectionName', 'C');
    },
    
    
})