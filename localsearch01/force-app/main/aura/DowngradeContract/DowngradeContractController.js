({
    doInit: function(component, event, helper) {
        var action = component.get("c.downgradeContract");
        console.log("downgradeContract");
        action.setParams({
            				"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get("e.force:closeQuickAction").fire();
            if(state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log("res: "+res);
                if(res != null)
                {   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": $A.get("{!$Label.c.Success}"),
                    "message": $A.get("{!$Label.c.Record_Successfully_Created}"),
                    "type": "success"
                    });
                    toastEvent.fire();
                    var sObjectEvent = $A.get("e.force:navigateToSObject");
                    sObjectEvent.setParams({
                        "recordId": response.getReturnValue(),
                        "slideDevName": "detail"
                    });
                    sObjectEvent.fire();
        		}else
                {
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": $A.get("{!$Label.c.Error}"),
                        "message": $A.get("{!$Label.c.Contract_Not_Active}"),
                        "type": "warning"
                    });
                    toastEvent.fire();
                }
            }else if (state === "ERROR"){
                var errors = response.getError();
                console.log('Errors: ' + JSON.stringify(errors));
                if(errors) {
                     var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": $A.get("{!$Label.c.Error}"),
                        "message": $A.get("{!$Label.c.Downgrade_not_allowed}"),
                        "type": "warning"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})