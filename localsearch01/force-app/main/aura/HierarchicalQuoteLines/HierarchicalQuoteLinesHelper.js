({
	helperMethod : function(component) {
        //A.L. Marotta 28-10-19 
        //Wave F, Sprint 12 - helper method to set related list columns
        var profileName = '';
        var getProfileNameAction = component.get('c.getProfileName');
        getProfileNameAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State: '+state);
            if (state === "SUCCESS") {
                profileName = response.getReturnValue();
                console.log('Profile Name: '+profileName);
                this.setColumns(component,profileName);
            }
            else {
                console.log("Failed with state: " + state);
                return null;
            }
            
        });
        $A.enqueueAction(getProfileNameAction); 
    },
    
    setColumns : function(component,profileName){
        //A.L. Marotta 28-10-19 
        //Wave F, Sprint 12 - set displayed columns, basing on current user profile name 
        var textType = '';
        var productName = '';
        var placeName = '';
        var quoteLineName = '';
		        
        //if(profileName){
       //    if(profileName == 'System Administrator'){
                textType = 'url';
                placeName = 'Place__c';
               	quoteLineName = 'Id';
               	productName = 'SBQQ__Product__c';
            /*}else{
                textType = 'string';
                placeName = 'Place_Name__c';
               	quoteLineName = 'Name';
               	productName = 'SBQQ__ProductName__c';
            } */
     //   }
       // else{ 
         //   console.log('profile name undefinded');
        //}
        var userInfo = profileName.split(';');
        if((userInfo.includes('Customer Care') && userInfo.includes('Order_Management'))  || userInfo.includes('System Administrator')){
             var columns = [
                {type: textType,fieldName: productName,label: 'Product',initialWidth: 200,typeAttributes: {label: {fieldName: 'SBQQ__ProductName__c'},target: '_parent'}},
                {type: textType,fieldName: placeName,initialWidth: 200,label: 'Place',typeAttributes: {label: {fieldName: 'Place_Name__c'},target: '_parent'}},
                {type: 'text',fieldName: 'PlaceID__c',initialWidth: 200,label: 'PlaceID',typeAttributes: {label: {fieldName: 'PlaceID__c'},target: '_parent'}},
                {type: 'currency',initialWidth: 200,fieldName: 'SBQQ__ListPrice__c',label: 'Base Term Price'},
                {type: 'percent',initialWidth: 200,fieldName: 'SBQQ__TotalDiscountRate__c',label: 'Additional Disc.'}, 
                {type: 'currency',initialWidth: 200,fieldName: 'Total__c',label: 'Total'},
                {type: textType,initialWidth: 200,fieldName: quoteLineName,label: 'Line Name',initialWidth: 200,typeAttributes: {label: {fieldName: 'Name'},target: '_parent' }},
                {type: 'boolean',initialWidth: 200,fieldName: 'ASAP_Activation__c',label: 'ASAP'},
                {type: 'date',initialWidth: 200,fieldName: 'SBQQ__StartDate__c',label: 'Start Date'},
                {type: 'number',initialWidth: 200,fieldName: 'SBQQ__Quantity__c',label: 'Quantity'},  
                {type: 'number',initialWidth: 200,fieldName: 'Base_term_Renewal_term__c',label: 'Base term / Renewal term'},
                {type: 'number',initialWidth: 200,fieldName: 'Subscription_Term__c',label: 'Subscription Term'},
                {type: 'button', label: 'Action', initialWidth:180,  fixedWidth: 180, fixedHeight: 30 ,typeAttributes: { label: $A.get("{!$Label.c.Set_Customer_Center_User}"), name: 'SetContact', disabled: { fieldName: 'showLBx' }}}
            ];
        }
        else{
            var columns = [
                {type: textType,fieldName: productName,label: 'Product',initialWidth: 200,typeAttributes: {label: {fieldName: 'SBQQ__ProductName__c'},target: '_parent'}},
                {type: textType,fieldName: placeName,initialWidth: 200,label: 'Place',typeAttributes: {label: {fieldName: 'Place_Name__c'},target: '_parent'}},
                {type: 'currency',initialWidth: 200,fieldName: 'SBQQ__ListPrice__c',label: 'Base Term Price'},
                {type: 'percent',initialWidth: 200,fieldName: 'SBQQ__TotalDiscountRate__c',label: 'Additional Disc.'}, 
                {type: 'currency',initialWidth: 200,fieldName: 'Total__c',label: 'Total'},
                {type: textType,initialWidth: 200,fieldName: quoteLineName,label: 'Line Name',initialWidth: 200,typeAttributes: {label: {fieldName: 'Name'},target: '_parent' }},
                {type: 'boolean',initialWidth: 200,fieldName: 'ASAP_Activation__c',label: 'ASAP'},
                {type: 'date',initialWidth: 200,fieldName: 'SBQQ__StartDate__c',label: 'Start Date'},
                {type: 'number',initialWidth: 200,fieldName: 'SBQQ__Quantity__c',label: 'Quantity'},  
                {type: 'number',initialWidth: 200,fieldName: 'Base_term_Renewal_term__c',label: 'Base term / Renewal term'},
                {type: 'number',initialWidth: 200,fieldName: 'Subscription_Term__c',label: 'Subscription Term'},
                {type: 'button', label: 'Action', initialWidth:180,  fixedWidth: 180, fixedHeight: 30 ,typeAttributes: { label: $A.get("{!$Label.c.Set_Customer_Center_User}"), name: 'SetContact', disabled: { fieldName: 'showLBx' }}}
            ];
        }
        component.set('v.gridColumns', columns);
    },
    
    setContactOnSubscription : function(component, row, action){
        var that = this;
        var modalBody;
        var recId = row.Id.replace('/', '');
        $A.createComponent("c:SetLBxContact", {recordId:recId},
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       body: modalBody, 
                       showCloseButton: false,
                       closeCallback: function() {
                       }
                   })
               }                               
           });
    }
})