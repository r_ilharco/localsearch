/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 04-01-2019
 * Sprint      : 1
 * Work item   : 19
 * Package     : 
 * Description : 
 * Changelog   : 
 */

({ // eslint-disable-line
    init: function (component, event, helper) {
        var profileName;
        var action = component.get('c.getQuoteLinesByQuoteId');
        action.setParams({ id : component.get('v.recordId')});
        //action.setParams({  id : 'a0q9E000003p8zUQAQ' });
        //action.setParams({  id : 'a0q9E000003zjsUQAQ' });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                component.set('v.ProductsCounter', data.length);
                console.log('Data: ' + JSON.stringify(data));
                data.forEach(
                    function(element)
                    {
                        element.Id = '/' + element.Id;
                        element.SBQQ__Product__c = '/' + element.SBQQ__Product__c;
                        if(element.Place__c)
                        {
                            element.Place__c = '/' + element.Place__c;
                        }
                        
                        if(typeof element._children != 'undefined')
                        {
                            element._children.forEach(
                                function(el)
                                {
                                    el.Id = '/' + el.Id;
                                    el.SBQQ__Product__c = '/' + el.SBQQ__Product__c;
                                    if(el.Place__c)
                                    {
                                        el.Place__c = '/' + el.Place__c;
                                    }
                                }
                            )
                        }
                    }
                )
                //A.L. Marotta 29-10-19, Wave F, Sprint 12 - call helper method to set related list columns
                helper.helperMethod(component);
                component.set('v.gridData', data);
                //component.set('v.gridData', JSON.parse(response.getReturnValue()));
                console.log('gridData: ' + JSON.stringify(component.get('v.gridData')));
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        
        
        

        $A.enqueueAction(action);
    },
    
    handleRowAction : function(component, event, helper){
    	var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'SetContact':
                helper.setContactOnSubscription(component, row, action);
                break;
        }
	}
});