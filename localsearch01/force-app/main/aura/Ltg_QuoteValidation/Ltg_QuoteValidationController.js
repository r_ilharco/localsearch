({
	doInit : function(component, event, helper) {
        var action = component.get("c.validateQuoteBeforeGeneratingProposal");
		action.setParams({
            "recordId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            component.set("v.loaded", true);
            var state = response.getState();
            var showModal = false;
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                var isSuccess = true;
                var checkQuoteStatus = true;
                var errorMessages = [];
                var myErrorMap = new Map();
                console.log('result: ' + JSON.stringify(result));
                if(Array.isArray(result))
                {
                    for(var i = 0; i < result.length; i++)
                    {
                        var element = result[i];
                        if(element.processName && element.processName == "checkForContact")
                        {
                            if(element.results["checkLBxContacts"] && element.results["checkLBxContacts"].isError == true || element.results["checkLBxContacts"].isSuccess == false)
                            {
                                isSuccess = false;
                                //errorMessages.push($A.get("{!$Label.c.LBx_Contact_Missing}"));'https://'+window.location.hostname+"/"+component.get("v.record.SBQQ__Account__c"))'https://'+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/sb?Id="+component.get("v.recordId")
                                myErrorMap.set($A.get("$Label.c.LBx_Contact_Missing")+ '. Update contact on Product Included related list','');


                       		 }
                    	}else if(element.processName && element.processName == "checkPrimaryContact")
                        {
                        	Object.keys(element.results).forEach( function(key)
                        	{	
                        		var value = element.results[key];
                        		if(value.isError == true || value.isSuccess == false)
                        		{
                        			isSuccess = false;
                        			if(value.errorMessages && Array.isArray(value.errorMessages) && value.errorMessages.length > 0)
                        			{
                        				for(var key in value.errorMessages)
                        				{
                        					if(value.errorMessages[key])
                        					{
                        						//errorMessages.push(value.errorMessages[key]);
                                                myErrorMap.set(value.errorMessages[key],'');
                    						}
                    					}
                    				}
                    			}
                    		});
                    	}else if(element.processName && element.processName == "checkBillingProfileOnQuote")
                        {
                            if(element.results['checkBillingProfileOnQuote'].isError == true || element.results['checkBillingProfileOnQuote'].isSuccess == false)
                            {
                                isSuccess = false;
                                if(element.results['checkBillingProfileOnQuote'].errorMessages && Array.isArray(element.results['checkBillingProfileOnQuote'].errorMessages) && element.results['checkBillingProfileOnQuote'].errorMessages.length > 0)
                                {
                                    console.log('element.errorMessages: ' + JSON.stringify(element.results['checkBillingProfileOnQuote'].errorMessages));
                                    for(var key in element.results['checkBillingProfileOnQuote'].errorMessages)
                                    {
                                        if(element.results['checkBillingProfileOnQuote'].errorMessages[key])
                                        {
                                            //errorMessages.push(element.results['checkBillingProfileOnQuote'].errorMessages[key]);
                                            myErrorMap.set(element.results['checkBillingProfileOnQuote'].errorMessages[key],'');
                                        }
                                    }
                                }
                            }
                        }else if(element.processName && element.processName == "checkSalesRepOnQuote")
                        {
                            if(element.results['checkSalesRepOnQuote'].isError == true || element.results['checkSalesRepOnQuote'].isSuccess == false)
                            {
                                isSuccess = false;
                                if(element.results['checkSalesRepOnQuote'].errorMessages && Array.isArray(element.results['checkSalesRepOnQuote'].errorMessages) && element.results['checkSalesRepOnQuote'].errorMessages.length > 0)
                                {
                                    console.log('element.errorMessages: ' + JSON.stringify(element.results['checkSalesRepOnQuote'].errorMessages));
                                    for(var key in element.results['checkSalesRepOnQuote'].errorMessages)
                                    {
                                        if(element.results['checkSalesRepOnQuote'].errorMessages[key])
                                        {
                                            //errorMessages.push(element.results['checkSalesRepOnQuote'].errorMessages[key]);
                                            myErrorMap.set(element.results['checkSalesRepOnQuote'].errorMessages[key],'');
                                        }
                                    }
                                }
                            }
                        }else if(element.processName && element.processName == "checkReplaceSubscriptions")
                        {
                        	var check = true;
                        	Object.keys(element.results).forEach( function(key)
                        	{	
                        		var value = element.results[key];
                        		if(value.isError == true || value.isSuccess == false)
                        		{
                        			isSuccess = false;
                        			check = false;
                        			if(value.errorMessages && Array.isArray(value.errorMessages) && value.errorMessages.length > 0)
                        			{
                        				for(var key in value.errorMessages)
                        				{
                        					if(value.errorMessages[key])
                        					{
                        						//errorMessages.push(value.errorMessages[key]);
                                               	myErrorMap.set(value.errorMessages[key],'https://'+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/sb?Id="+component.get("v.recordId"));

                    						}
                    					}
                    				}
                    			}
                    		});
                    	}else if(element && element.processName && element.processName == "checkQuoteStatus")
                        {
                        	if(element.results["checkQuoteStatus"] && element.results["checkQuoteStatus"].isError == true || element.results["checkQuoteStatus"].isSuccess == false)
                            {
                        		checkQuoteStatus = false;
                                isSuccess = false;
                                errorMessages = [];
                                //errorMessages.push($A.get("{!$Label.c.GenerateProposal_QuoteStatus_Error_msg}"));
                                myErrorMap.set("With this current status is not possible to generate a proposal.",'');
                                break;
                       		}
                    	}
                        //Vincenzo Laudato - Check QuoteLines start date
                        else if(element.processName){
                            if(element.processName == "checkQLStartDate"){
                                var checkQLStartDate = element.results["checkQLStartDate"];
                                if(checkQLStartDate){
                                    if(checkQLStartDate.isError){
                                        isSuccess = false;
                                        if(checkQLStartDate.errorMessages){
                                            //checkQLStartDate.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                            checkQLStartDate.errorMessages.forEach(currentError => myErrorMap.set(currentError,'https://'+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/sb?Id="+component.get("v.recordId")));
                                            

                                        }
                                    }
                                }
                            }
                            else if(element.processName == "validateQLs"){
                                var validateQLs = element.results["validateQLs"];
                                if(validateQLs){
                                    if(validateQLs.isError){
                                        isSuccess = false;
                                        if(validateQLs.errorMessages){
                                            //validateQLs.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                            validateQLs.errorMessages.forEach(currentError => myErrorMap.set(currentError,'https://'+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/sb?Id="+component.get("v.recordId")));

                                        }
                                    }
                                }
                            }
                            //Vincenzo Laudato - Account and BP Legal Address Validation
                            else if(element.processName == "validateAccountAddress"){
                                var validateAccountAddress = element.results["validateAccountAddress"];
                                if(validateAccountAddress){
                                    if(validateAccountAddress.isError){
                                        isSuccess = false;
                                        if(validateAccountAddress.errorMessages){
                                            //validateAccountAddress.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                            //validateAccountAddress.errorMessages.forEach(currentError => myErrorMap.set(currentError,'/one/one.app?#/sObject/'+ component.get("v.record.SBQQ__Account__c") + '/view'));
                                             validateAccountAddress.errorMessages.forEach(currentError => myErrorMap.set(currentError,'https://'+window.location.hostname+"/"+component.get("v.record.SBQQ__Account__c")));
                                            
                                        }
                                    }
                                }
                            }
                            else if(element.processName == "validateBPAddress"){
                                var validateBPAddress = element.results["validateBPAddress"];
                                if(validateBPAddress){
                                    if(validateBPAddress.isError){
                                        isSuccess = false;
                                        if(validateBPAddress.errorMessages){
                                            //validateBPAddress.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                             validateBPAddress.errorMessages.forEach(currentError =>  myErrorMap.set(currentError,'/one/one.app?#/sObject/'+ component.get("v.record.Billing_Profile__c") + '/view'));
                                        }
                                    }
                                }
                            }
                            else if(element.processName == "salesRepUpdateOnQuote"){
                                var salesRepUpdate = element.results["salesRepUpdateOnQuote"];
                                if(salesRepUpdate){
                                    if(salesRepUpdate.isError){
                                        isSuccess = false;
                                        if(salesRepUpdate.errorMessages){
                                            //salesRepUpdate.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                            salesRepUpdate.errorMessages.forEach(currentError =>  myErrorMap.set(currentError,''));
                                        }
                                    }
                                }
                            }
                            else if(element.processName == "checkSalesManager"){
                                var salesManagerCheck = element.results["checkSalesManager"];
                                if(salesManagerCheck){
                                    if(salesManagerCheck.isError){
                                        isSuccess = false;
                                        if(salesManagerCheck.errorMessages){
                                            //salesManagerCheck.errorMessages.forEach(currentError => errorMessages.push(currentError));
                                            salesManagerCheck.errorMessages.forEach(currentError =>  myErrorMap.set(currentError,''));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if(isSuccess && checkQuoteStatus){
                    var toastEvent = $A.get("e.force:showToast");
                	toastEvent.setParams({
                        
                    "title": $A.get("$Label.c.Success"),
                    "message":'Quote validation passed successfully',
                    "type": "success"
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
                }
                else{
                   /* for(var key in errorMessages)
                    {	
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": $A.get("{!$Label.c.Warning}"),
                            "message": errorMessages[key],
                            "type": "warning",
                            "mode": "Sticky",
                            "messageTemplate": errorMessages[key]
                        });
                        toastEvent.fire();
                    }*/
                    $A.get("e.force:closeQuickAction").fire(); 
                    $A.get('e.force:refreshView').fire();
                    myErrorMap.forEach(function(value, key) {
                        console.log(key + " = " + value);
                        var toastEvent = $A.get("e.force:showToast");
                        if(value){
                          toastEvent.setParams({
                            "title": $A.get("$Label.c.Warning"),
                            "message": key,
                            "type": "warning",
                            "mode": "Sticky",
                            "messageTemplate":'{0}',
                            "messageTemplateData": [{
                                url: value,
                                label: key,
                            }]
                        });   
                        }else{
                             toastEvent.setParams({
                            "title": $A.get("$Label.c.Warning"),
                            "message": key,
                            "type": "warning",
                            "mode": "Sticky"
                        });   
                        }
                       
                        toastEvent.fire();
                    }) 
                    
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
            }
            else{
                console.log('ERROR');
                console.log('some problem '+ JSON.stringify(response.getError()) );
                var errorm= response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Warning"),
                    "message": errorm[0].message,
                    "type": "warning"
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    }
    
})