({    doInit : function(component, event, helper) {
    
    // Get the url attribute
    var quoteId = component.get("v.recordId");
    var action = component.get("c.getQuoteStatus"); 
    action.setParams({
        'recordId' : quoteId
    });
    action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var res = response.getReturnValue();
            console.log('RES: ' + JSON.stringify(res));
            if(res === "Expired")
            {
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Edit_Lines_not_Available"),
                    "type": "Warning",
                    "message": $A.get("$Label.c.Quote_Status_Is_Expired")
                });
                toastEvent.fire();
            }
            else if(res === "Accepted" || res === "Rejected" || res === "In Review")
            {
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Edit_Lines_not_Available"),
                    "type": "Warning",
                    "message": $A.get("$Label.c.Quote_not_configurable_anymore")
                });
                toastEvent.fire();
            }else if(res === "Presented")
            {
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Edit_Lines_not_Available"),
                    "type": "Warning",
                    "message": $A.get("$Label.c.Quote_not_configurable_anymore_in_presented"),
                    "mode": "sticky"
                });
                toastEvent.fire();
                toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Edit_Lines_not_Available"),
                    "type": "Warning",
                    "message": $A.get("$Label.c.Quote_not_configurable_anymore"),
                    "mode": "sticky"
                });
                toastEvent.fire();
            }
                else {
                    
                    var RedirectUrl = '/apex/sbqq__sb?scontrolCaching=1&id='+quoteId+'#quote/le?qId='+quoteId+'}';
                    var redirect = $A.get("e.force:navigateToURL");
                    redirect.setParams({ "url": RedirectUrl });
                    redirect.fire();
                }
        }
    }
                      ); 
    $A.enqueueAction(action);
}
  
 })