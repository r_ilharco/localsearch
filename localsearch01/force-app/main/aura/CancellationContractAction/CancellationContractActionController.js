({
	doInit : function(component, event, helper) {
        var today = new Date();
        var todayString = $A.localizationService.formatDate(today, "YYYY-MM-DD");
        component.set('v.today', todayString);
        component.set('v.minDate', todayString);
        component.set('v.maxDate', todayString);
        helper.getCancellationInformation(component, event);
    },
    
    handleCancellationClick: function(component, event, helper) {
        helper.cancellationContract(component, event);
    },

	handleCancelClick: function(component, event, helper) {
        helper.closeModal(component, event);
    },

    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
})