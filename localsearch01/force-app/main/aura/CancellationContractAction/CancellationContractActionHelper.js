({
	getCancellationInformation: function (component, event) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getContractCancellationInformation");
        var params = { 
            recordId: recordId
        };
		
		action.setParams(params);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.cacellationReason', data.listReasonOptions);
                component.set('v.countBilledSubscription', data.countBilledSubscription);
                component.set('v.contractEndDate', data.contractEndDate);
                component.set('v.countInvoiceReference', data.countInvoiceReference);

                var validContractstatusList = [];
                validContractstatusList.push($A.get("$Label.c.Contract_Active"));
                validContractstatusList.push($A.get("$Label.c.Contract_Production"));
                validContractstatusList.push($A.get("$Label.c.Contract_Draft"));
                
                var contractEndDate = component.get('v.contractEndDate');
                var mindate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");

                if(contractEndDate != null && contractEndDate != '' && contractEndDate < mindate){
                    this.showNotifWarning(component, event, $A.get("$Label.c.Contract_check_end_date"), "");
                    this.closeModal(component, event);
                    return;
                }
				else if (!validContractstatusList.includes(data.contractStatus)) {
                    var warningMessage = this.format($A.get("$Label.c.Contract_check_status_cancel"), validContractstatusList);
                    this.showNotifWarning(component, event, warningMessage, "");
                    this.closeModal(component, event);
                    return;
                }
                else if(component.get('v.countBilledSubscription') > 0){
                    this.showNotifWarning(component, event, $A.get("$Label.c.Contract_cannot_cancel_billed"), "");
                    this.closeModal(component, event);
                    return;
                }
                else if(component.get('v.countInvoiceReference') > 1){
                    this.showNotifWarning(component, event, $A.get("$Label.c.Invoice_referenced_in_more_contracts"), "");
                    this.closeModal(component, event);
                    return;
                }

                component.set('v.loadingData', false);
            } 
            else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
					this.showNotifError(component, event, errors[0].message, $A.get("$Label.c.Operation_failed"));
                } else {
                	this.showNotifError(component, event, $A.get("$Label.c.Error_sending_information"), $A.get("$Label.c.Operation_failed"));
                }

                component.set('v.loadingData', false);
            }
        });

        $A.enqueueAction(action);
	},
	
    cancellationContract: function (component, event) {
        var recordId = component.get('v.recordId');
        var reasonCancellation = component.find("reasonCancellation").get("v.value");
        var noClawback = component.get("v.noClawback");
        var cancellationDateCmp = component.find('cancellationDate');
        var cancellationDate = cancellationDateCmp.get("v.value");
        //var cancellationDateTime = new Date(cancellationDateCmp.get("v.value"));
        //cancellationDateTime.setHours(0, 0, 0);

        var mindate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");

        var componentsToCheckValidity = ["reasonCancellation", "cancellationDate"];
        
        if (cancellationDate < mindate) {
            cancellationDateCmp.setCustomValidity($A.get("$Label.c.Date_cannot_be_in_the_past"));
        } else {
            cancellationDateCmp.setCustomValidity("");
        }
        cancellationDateCmp.reportValidity();

        if (!this.validModal(component, event, componentsToCheckValidity)) {
            return;
        }

        var action;
        var params;

        action = component.get("c.cancellationContractAction");
        params = { 
            contractId : recordId ,
            reason : reasonCancellation,
            noClawback : noClawback,
            updateClawback : true
        };

        action.setParams(params);

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if (data.valid) {
                    this.showNotifSuccess(component, event, $A.get("$Label.c.Information_send_with_Success"), $A.get("$Label.c.Operation_Success"));
                    return;
                } else {
                    if (data.validations.length > 0) {
                        var msg = '';
                        for (var i = 0; i < data.validations.length; i++) {
                            msg = msg + data.validations[i] + ',';
                        }
                        this.showNotifWarning(component, event, msg, "");
                        return;
                    } else if (data.errors.length > 0) {
                        var msg = '';
                        for (var i = 0; i < data.errors.length; i++) {
                            msg = msg + data.errors[i] + ',';
                        }
                        this.showNotifError(component, event, msg, $A.get("$Label.c.Operation_failed"));
                        return;
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
            this.showNotifError(component, event, $A.get("$Label.c.Error_sending_information"), $A.get("$Label.c.Operation_failed"));
        });

        $A.enqueueAction(action);
		
	},
	
    showNotifError: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "error",
            "header": header,
            "message": message
        });
	},
	
    showNotifWarning: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "warning",
            "header": header,
            "message": message
        });
	},
	
    showNotifSuccess: function (component, event, message, header) {
        component.find('notifLib').showToast({
            "variant": "success",
            "header": header,
            "message": message
        });
        $A.get('e.force:refreshView').fire();
        this.closeModal(component, event);
	},
	
    closeModal: function (component, event) {
        $A.get("e.force:closeQuickAction").fire();
        component.find("overlayLib2").notifyClose();
	},
	
    validModal: function (component, event, listComponents) {
        var valid = true;
        for (var i = 0; i < listComponents.length; i++) {
            var field = component.find(listComponents[i]);
            if (field) {
                field.showHelpMessageIfInvalid();
                if (!field.get("v.validity").valid) {
                    valid = false;
                }
            }
        }
        return valid;
    },

    format: function(text, values) {
        var index;
        for (index in values) {
            text = text.replace(new RegExp("\\{" + index + "\\}", 'g'), values[index]);
        }
        return text;
    },

})