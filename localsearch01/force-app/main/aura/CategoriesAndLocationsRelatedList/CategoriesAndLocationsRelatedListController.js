/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 05-15-2019
 * Sprint      : Sprint 1
 * Work item   : US_19
 * Package     : 
 * Description : JS Controller for CategoriesAndLocationsRelatedListController
 * Changelog   : 
 */

({
    init: function (cmp, event, helper) {
        var langLocale = $A.get("$Locale.langLocale");
        console.log("$Locale.langLocale: " + langLocale);
        var action = cmp.get("c.getCategoriesAndLocationByPlace");
        action.setParams(
            {
                obj: cmp.get("v.recordId"),
                sObjName: cmp.get("v.sObjectName"),
                locale: langLocale
            }
        );
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('XPrint');
                console.log('response.getReturnValue(): ' + JSON.stringify(response.getReturnValue()));
                console.log("response['C']: " + res['C']);
                console.log("response['L']: " + res['L']);

                cmp.set("v.categories", res['C']);
                cmp.set("v.locations", res['L']);

            }
        });

        $A.enqueueAction(action);
    }
})