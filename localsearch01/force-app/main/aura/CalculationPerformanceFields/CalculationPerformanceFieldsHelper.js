({
	RecalculateFields : function(component, event) {
        console.log("RecalculateFields");
        component.set('v.showSpinner',true);
        var recordId = component.get('v.recordId');
        var params = { 
            quoteId: recordId,
            updateQuote: false
                     };
        var action = component.get("c.CalculateQuoteDiff");
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('SUCCESS'+res);
                debugger;
                if(res != null){
                component.set('v.quote',res.Quote);
                component.set('v.refreshAllow',res.AllowRefresh);
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
        console.log('SUCCESS-end');
    },
    
})