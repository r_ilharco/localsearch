({
	onInit : function(component, event, helper) {
		helper.RecalculateFields(component, event);
	},
    onRefresh : function(component, event, helper) {
		helper.RecalculateFields(component, event);
	},
    matrixClick: function(component, event, helper) {
		var url = $A.get("$Label.c.CPF_Matrix_Replacement_Link");
        window.open(url);
	},
    DocumentClick: function(component, event, helper) {
        var url = component.get("v.quote.Signed_Document_URL__c");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url": url });
        urlEvent.fire();
    }
})