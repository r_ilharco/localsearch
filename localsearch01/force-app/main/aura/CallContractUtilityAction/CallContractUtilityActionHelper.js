({
    doInit: function(component, event) {
            var methodCall = component.get("v.methodCall");
            var recordId = component.get("v.recordId");
            var action = component.get("c."+methodCall);
            var params = {  contractId : recordId };
            action.setParams(params);
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data = response.getReturnValue();
                    if(data.valid){
                        console.log('******** LS '+JSON.stringify(data.info[0]));
                        if(data.info.length > 0 ) {
                            this.showNotifSuccess(component, event,data.info[0],$A.get("$Label.c.Operation_Success"));
                        }
                        else {
                            this.showNotifSuccess(component, event,$A.get("$Label.c.Information_send_with_Success"),$A.get("$Label.c.Operation_Success"));
                        }
                       // this.showNotifSuccess(component, event,$A.get("$Label.c.Information_send_with_Success"),$A.get("$Label.c.Operation_Success"));
                        return;
                    } else {
                        if(data.validations.length > 0){
                            var msg = '';
                            for(var i = 0; i < data.validations.length ; i++){
                                msg = msg + data.validations[i]+',';
                            }
                            this.showNotifWarning(component, event,msg,"");
                            return;
                        }else if(data.errors.length > 0){
                            var msg = '';
                            for(var i = 0; i < data.errors.length ; i++){
                                msg = msg + data.errors[i]+',';
                            }
                            this.showNotifError(component, event,msg,$A.get("$Label.c.Operation_failed"));
                            return;
                        }
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error");
                    }
                }
                this.showNotifError(component, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));  
            });
            
            $A.enqueueAction(action);
    },
    showNotifError : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "error",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
    },
    showNotifWarning : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "warning",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
    },
    showNotifSuccess : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "success",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
        $A.get('e.force:refreshView').fire();
    },
    closeModal : function(component, event){
        $A.get("e.force:closeQuickAction").fire();
    }
        
})