({
    doInit : function(component, event, helper) {
        var mapKeySelectedValues = new Map();
        
        var map = component.get("v.mapToTransform");
		var parsingWrapper = [];
        
        Object.keys(map).forEach(function(key) {
            var individualElement = {};
            individualElement.key = key;
            individualElement.value = [];
            var currentValue = map[key];
            currentValue.forEach(function(currentItem) {
                individualElement.value.push(currentItem);
            });
            parsingWrapper.push(individualElement);
        });
        component.set('v.mapToIterate', parsingWrapper);
    }
})