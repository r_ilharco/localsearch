({
    doInit : function(component, event, helper){
        var opportunityId = component.get('v.recordId');
        helper.checkValidity(component, opportunityId);
    },
    
    cancel : function(component, event, helper){
        $A.get('e.force:closeQuickAction').fire();
    },
    
    proceed : function(component, event, helper){
        var opportunityId = component.get('v.recordId');
        helper.resetQuote(component, opportunityId);
    },
    
	showSpinner : function(component, event, helper) {
        component.set('v.Spinner', true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set('v.Spinner', false);
    }
})