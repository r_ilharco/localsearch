({
	checkValidity : function(component, opportunityId) {
        
		var action = component.get('c.checkValidity');
        
        action.setParams({
            'opportunityId': opportunityId
        });
        
        action.setCallback(this, function(response) {
            var isError = false;
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                if(result){
                    if(result.isExpired){
                        this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.ResetQuoteExpired'));
                        isError = true;   
					}
                    else if(result.optyClosed){
                        this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.ResetQuoteOptyClosed'));
                        isError = true;
                    }
                    else if(!result.quoteAccepted){
                        this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.ResetQuoteNotAccepted'));
                        isError = true;
                    }
                }
                else{
                    this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                    isError = true;
                }
            }
            else{
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                isError = true;
            }
            
            if(isError) $A.get('e.force:closeQuickAction').fire();
            
        });
        $A.enqueueAction(action);
	},
    
    resetQuote : function(component, opportunityId){
        var action = component.get('c.resetQuote');
        
        action.setParams({
            'opportunityId': opportunityId
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                if(result){
                	this.showToast($A.get('$Label.c.Success'), 'success', $A.get('$Label.c.Success'));
                }
                else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            }
            else this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
            
            $A.get('e.force:closeQuickAction').fire();
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(title, type, message){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: 'pester',
            duration : 3000
        });
        toastEvent.fire();
    }
})