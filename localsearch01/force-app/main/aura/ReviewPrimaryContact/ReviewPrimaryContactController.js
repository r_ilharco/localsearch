({
    
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        console.log(eventParams.changeType);
        if(eventParams.changeType === "LOADED") {
            console.log('record:::: '+ component.get("v.simpleRecord.Filtered_Primary_Contact__c"));
            
            if(component.get("v.simpleRecord.Filtered_Primary_Contact__c")){
         		helper.redirectToRecord(component, event);     
            } else {
                helper.showToast(component, event);
            }
    	}
    }
    
})