({  
    redirectToRecord : function(component, event) {
        var redirect = $A.get("e.force:navigateToSObject");
        
        // Pass the record ID to the event
        redirect.setParams({
            "recordId": component.get("v.simpleRecord.Filtered_Primary_Contact__c")
        });
        
        // Open the record
        redirect.fire();
    },
  
    
    showToast : function(component, event) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": $A.get("$Label.c.Insert_Primary_Contact_Error"),
            "type": "error"
        });
        toastEvent.fire();
    }
})