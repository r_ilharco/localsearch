({
	doInit : function(component, event, helper) {
        helper.init(component);
	},
    
    cancel : function(component, event, helper){
        $A.get('e.force:closeQuickAction').fire();
        $A.get('e.force:refreshView').fire();
    },
    
    proceed : function(component, event, helper){
        var quickActionClose = $A.get('e.force:closeQuickAction');
        var modalBody;
        $A.createComponent("c:CustomGenerateProposal", {recordId : component.get('v.recordId')},
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       header: "Generate Proposal",
                       body: modalBody,
                       showCloseButton: false,
                       cssClass: "popoverclass generatePrpClass",
                       closeCallback: function() {
                           console.log('Close Callback');
                           quickActionClose.fire();
                           $A.get('e.force:refreshView').fire();
                       }
                   })
               }
           });
    },
    
    showSpinner : function(component, event, helper) {
        component.set('v.Spinner', true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set('v.Spinner', false);
    }
})