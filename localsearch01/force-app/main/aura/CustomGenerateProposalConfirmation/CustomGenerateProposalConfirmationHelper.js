({
	init : function(component) {
		var action = component.get("c.getQuoteType");
		action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var quote = response.getReturnValue();
                if(quote.SBQQ__Type__c == $A.get('$Label.c.QuoteTypeReplacement')){
                    this.checkReplacementSubscriptions(component);
                }
                else component.set('v.isReplacement', false);
            }
            else{
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                $A.get('e.force:closeQuickAction').fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
	},

    checkReplacementSubscriptions : function(component, quote){
        var action = component.get("c.checkReplacementSubscriptions");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var missingReplacement = response.getReturnValue();
                if(!missingReplacement){
                    this.getReplacedSubscriptions(component);
                }
                else{
                    this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.CustomGenerateProposal_ProductSubstitution'));
                    $A.get('e.force:closeQuickAction').fire();
                    $A.get('e.force:refreshView').fire();
                }
            }
            else{
                this.showToast($A.get('$Label.c.Error'), 'error', $A.get('$Label.c.Generic_Error'));
                $A.get('e.force:closeQuickAction').fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    getReplacedSubscriptions : function(component){
        var action = component.get("c.getReplacedSubscriptions");
		action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                this.setupDataTable(component);
                var subscriptions = response.getReturnValue();
                if(subscriptions){
                    var subscriptionsArray = [];
                    
                    subscriptions.forEach(currentSub => {
                    	currentSub.recordUrl = '/' + currentSub.Id;
                        currentSub.subProductName = currentSub.SBQQ__Product__r.Name;
                        if(currentSub.Place__r){
                            currentSub.subPlaceName = currentSub.Place__r.Name;
                            currentSub.subPlaceUrl= '/' + currentSub.Place__c;
                        }
                        currentSub.subStartDate = currentSub.SBQQ__SubscriptionStartDate__c;
                        currentSub.subEndDate = currentSub.End_Date__c;
                        subscriptionsArray.push(currentSub);
                	});
                    component.set('v.data', subscriptionsArray);
                }
            }
            else{
                this.showToast($A.get('$Label.c.Error'), $A.get('$Label.c.Generic_Error'), 'error');
                $A.get('e.force:closeQuickAction').fire();
                $A.get('e.force:refreshView').fire();        
            }
        });
        $A.enqueueAction(action);
    },
    
    setupDataTable: function(component){
        component.set('v.columns', [
            { label: 'Subscription Name', fieldName: 'recordUrl', type: 'url', initialWidth: 150,
             typeAttributes: {
                    label: { fieldName: 'Name' },
                	target:'_blank'
                  },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Product Name', fieldName: 'subProductName', type: 'text', initialWidth: 200, cellAttributes: { alignment: 'left' }},
            { label: 'Place', fieldName: 'subPlaceUrl', type: 'url', initialWidth: 180,
             typeAttributes: {
                    label: { fieldName: 'subPlaceName' },
                	target:'_blank'
                  },
             cellAttributes: { alignment: 'left' }
            },
            { label: 'Start Date', fieldName: 'subStartDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'left' }},
            { label: 'End Date', fieldName: 'subEndDate', type: 'date', initialWidth: 180, cellAttributes: { alignment: 'left' }},
        ]);
    },
    
    showToast : function(title, type, message){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
            mode: 'pester',
            duration : 3000
        });
        toastEvent.fire();
    }
})