({
    doInit: function(component, event) {
        
        var methodCall = component.get("v.methodCall");
        var recordId = component.get("v.recordId");
        //alert(recordId + " " + methodCall);
       
        var action = component.get("c."+methodCall);
        var params = {  quoteDocumntRecordId : recordId };
        action.setParams(params);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State: ' + response.getState());
            console.log('Response: ' + JSON.stringify(response));
            console.log('response.getReturnValue(): ' + response.getReturnValue());
            if (state === $A.get("$Label.c.WebService_Response_Success")) {
				
                //alert("From server: " + response.getReturnValue());
                if(response.getReturnValue() === $A.get("$Label.c.Status_code_Success"))
                	this.showNotifSuccess(component, event, $A.get("$Label.c.Windream_Upload_Document_Successfull"), $A.get("$Label.c.Operation_Success"));
                else 
                    this.showNotifWarn(component,event,response.getReturnValue(),$A.get("$Label.c.Warning"));
                return;
            }
            else if (state === $A.get("$Label.c.WebService_Response_Incomplete")) {
                this.showNotifError(component, event, $A.get("$Label.c.WebService_Response_Incomplete") + " " + response.getReturnValue(), $A.get("$Label.c.Operation_failed"));             
            }
            else if (response.getReturnValue()){
                this.showNotifError(component, event, response.getReturnValue() , $A.get("$Label.c.Operation_failed"));
            }
            else {
                var errors = response.getError();
                 if (errors && errors[0] && errors[0].message) {
                    if(errors[0].message == $A.get("$Label.c.JSONHeapSize"))
                        this.showNotifError(component, event, $A.get("$Label.c.Document_Too_Large"), $A.get("$Label.c.Operation_failed")); 
                    else
                    	this.showNotifError(component, event, $A.get("$Label.c.Error_Message") + errors[0].message, $A.get("$Label.c.Operation_failed")); 
                } else {
                    this.showNotifError(component, event, $A.get("$Label.c.Unknown_Error"), $A.get("$Label.c.Operation_failed")); 
                }
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    showNotifSuccess : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "success",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
        $A.get('e.force:refreshView').fire();
    },
    
    showNotifError : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "error",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
    },
    showNotifWarn : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "warning",
            "header": header,
            "message": message
        });
        this.closeModal(component, event);
    },
    
    closeModal : function(component, event){
        $A.get("e.force:closeQuickAction").fire();
    }
    

        
})