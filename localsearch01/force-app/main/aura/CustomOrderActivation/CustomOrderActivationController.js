({
    doInit: function (cmp, event, helper) {
        
        helper.getOrderItems(cmp,event,helper);
    },
    
    handleActivate: function (cmp, event, helper){
        helper.activateOrder(cmp,event);
    },
    
    handleCancel: function (cmp,event,helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
    
});