({
    
    showToast : function(component,event,type,message,title){
        console.log(type+message+title);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
         $A.get("e.force:closeQuickAction").fire();
        // $A.get("e.force:closeQuickAction").fire();
    },
    
    getOrderItems: function(component, event,helper) {
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        var recordId = component.get('v.recordId');
        console.log('Order ID: '+recordId);
        var action = component.get('c.getOrderItems');
        var params = { orderId : recordId };
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('STATE: '+state);
            if (state === "SUCCESS"){
                var data = response.getReturnValue();
                console.log('Data: '+JSON.stringify(data));
                if(data != null && data.length > 0){
                    if(data[0].status == $A.get("{!$Label.c.OrderActivation_ErrorNoManualItem}")){
                    //if(data[0].ordStatus == $A.get("{!$Label.c.OrderActivation_ErrorNoManualItem}")){
                        component.set('v.isError',true);
                        component.set('v.errorMessage',$A.get("{!$Label.c.OrderActivation_ErrorNoManualItem}"));
                    }else{
                     /* if(data[0].ordStatus.toUpperCase() == $A.get("{!$Label.c.OrderActivation_Error}")){
                            component.set('v.isError',true);
                            component.set('v.errorMessage',$A.get("{!$Label.c.OrderActivation_ErrorStatus}"));
                            
                        }else{*/
                            data = helper.createHeader(component, event,helper,data);
                            if(!data[0].isFullActivation){
                                data = helper.adjustValues(component, event,helper,data);
                                component.set('v.isOrderActive',data[0].isSecondActivation); //change logic
                            }
                            component.set('v.orderItems',data);
                            let statusValues = helper.buildStatusOptions(component, event,helper,data[0].status);
                            component.set('v.statusOption', statusValues);
                            component.set('v.isError',false);
                        //}
                    }
                    
                }else{
                    component.set('v.isError',true);
                    component.set('v.errorMessage',$A.get("{!$Label.c.OrderActivation_ErrorNoProduct}"));
                }
            }
            else{
                //State === "ERROR"
                var errors = response.getError();
                if(errors){
                    this.showToast(component,event,'error', errors[0].message,$A.get("{!$Label.c.OrderActivation_Error}"));    
                }
            }
            
            
            
        });
        $A.enqueueAction(action);
    },
    
    activateOrder: function(component, event){
        var inputCmp = component.find('editableField');
        var validExpense = true;
        if(!$A.util.isEmpty(inputCmp)){
            if(!Array.isArray(inputCmp)){
                inputCmp.showHelpMessageIfInvalid();
                validExpense = inputCmp.get('v.validity').valid
            }else{
                validExpense = inputCmp.reduce(function (validSoFar, inputCmp) {
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid;
                }, true);
            }
        }
        
        if(validExpense){
            let ordersItem = component.get('v.orderItems');
            if(!$A.util.isEmpty(ordersItem) &&  !$A.util.isEmpty(ordersItem[0].isFullActivation) && ordersItem[0].isFullActivation){
              //!$A.util.isEmpty(ordersItem[0].isAmendment) && ordersItem[0].isAmendment){
                //Gestione delle full activation -- isFullActivation
                var recordId = component.get('v.recordId');
                console.log('Order ID: '+recordId);
                //let ordStatus = ordersItem[0].status;
                var action = component.get('c.activateAllOrder');
                var params = { orderId : recordId,
                              orderStatus: ordersItem[0].status};
                action.setParams(params);
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log('STATE: '+state);
                    if (state === "SUCCESS"){
                        var result = response.getReturnValue();
                        if(!$A.util.isEmpty(result)){
                            if(result.result){
                                setTimeout(function(){ location.reload();}, 1000);
                                this.showToast(component,event,'success', result.message ,result.title);
                               // $A.get("e.force:closeQuickAction").fire();
                            }else{
                                this.showToast(component,event,'error', result.message ,result.title);
                            }  
                        }else{
                            this.showToast(component,event,'error', $A.get("$Label.c.OrderActivation_genericErrorActivation"),$A.get("$Label.c.OrderActivation_Error"));
                        }                        
                    }else{
                        this.showToast(component,event,'error', $A.get("$Label.c.OrderActivation_genericErrorActivation"),$A.get("$Label.c.OrderActivation_Error"));
                    }
                });
                $A.enqueueAction(action);
            }else{
                ordersItem.forEach(function(ordItemElem) {
                    ordItemElem.ordItemId = ordItemElem.ordItem.Id;
                    if(!$A.util.isEmpty(ordItemElem.ordItemElement)){
                        ordItemElem.ordItemElement.forEach(function(field){
                            if (typeof field.value === "boolean"||typeof field.value === "date"||typeof field.value === "number") {
                                field.value=String(field.value);
                            }
                        });
                        ordItemElem.serializedordItemElement = JSON.stringify(ordItemElem.ordItemElement);                    
                    }
                    ordItemElem.childElement.forEach(function(child){
                        delete child.items;
                    });
                    ordItemElem.serializedChildElement = JSON.stringify(ordItemElem.childElement);
                    delete ordItemElem.ordItemElement;
                    delete ordItemElem.childElement;
                    delete ordItemElem.ordItem;
                });
                var recordId = component.get('v.recordId');
                console.log('Order ID: '+recordId);
                var action = component.get('c.activateOrder');
                var params = { orderId : recordId,
                              orderItems : JSON.stringify(ordersItem)};
                action.setParams(params);
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log('STATE: '+state);
                    if (state === "SUCCESS"){
                        var result = response.getReturnValue();
                        if(!$A.util.isEmpty(result)){
                            if(result.result){
                                this.showToast(component,event,'success', result.message ,result.title);
                                setTimeout(function(){ location.reload();}, 1000);
                                $A.get("e.force:closeQuickAction").fire();
                            }else{
                                this.showToast(component,event,'error', result.message ,result.title);
                            }  
                        }else{
                            this.showToast(component,event,'error', $A.get("$Label.c.OrderActivation_genericErrorActivation"),$A.get("$Label.c.OrderActivation_Error"));
                        }                        
                    }else{
                        this.showToast(component,event,'error', $A.get("$Label.c.OrderActivation_genericErrorActivation"),$A.get("$Label.c.OrderActivation_Error"));
                    }
                });
                $A.enqueueAction(action);
            }
            
        }   
    },
    createHeader: function(component, event,helper,rows){
        rows.forEach(function(orderItemMaster) {
            console.log(orderItemMaster);
            let header = [];
            let headerChild = [];
            let orderItemChild = orderItemMaster.childElement;
            //Master Header
            if(!$A.util.isEmpty(orderItemMaster.ordItemElement)){
                let fieldsElement = orderItemMaster.ordItemElement;
                fieldsElement.forEach(function(field){
                    var headerElem = [];
                    headerElem.label = field.fieldLabel;
                    headerElem.helpText	= field.helpText;
                    header.push(headerElem);
                });
            }
            if(header.length >0 ){
                //|| (!$A.util.isEmpty(orderItemMaster.isAmendment) && orderItemMaster.isAmendment)){  
                var headerElem = [];
                headerElem.label = $A.get("{!$Label.c.OrderActivation_activationStatusHeader}");
                header.push($A.get("{!$Label.c.OrderActivation_activationStatusHeader}"));
                orderItemMaster.header = header;
            }
            //
            //Child header
            if(!$A.util.isEmpty(orderItemChild)){
                let fieldsElement = orderItemChild[0].items;
                fieldsElement.forEach(function(field){
                    headerChild.push(field.fieldLabel);
                });
            }
            if(headerChild.length >0){
                orderItemMaster.headerChild = headerChild;
            }
            
            
        });
        return rows;
        
    },
    buildStatusOptions: function(component, event,helper,status){
        //USE CUSTOM LABEL
        let statusValues = [status,'Rejected']; 
        let statusLabels = [status,$A.get("{!$Label.c.OrderActivation_orderRejected}")];
        let statusOptions = [];
        var i;
        for (i = 0; i < statusValues.length; i++) { 
            let statusEl = [];
            statusEl.id = statusValues[i];
            statusEl.label = statusLabels[i];
            statusOptions.push(statusEl);
        }
        return statusOptions;
    },
    adjustValues: function(component, event,helper,rows){
        rows.forEach(function(orderItemMaster) {
            console.log(orderItemMaster);
            if(!$A.util.isEmpty(orderItemMaster.ordItemElement)){
                let fieldsElement = orderItemMaster.ordItemElement;
                var helpText = [];
                fieldsElement.forEach(function(field){
                    if(field.isEditable){
                        switch (field.fieldType) {
                            case 'DATE':
                                if(!$A.util.isEmpty( field.value)){
                                    field.value = field.value.split(" ")[0];
                                }
                                break;
                            case 'BOOLEAN':
                                field.value = (field.value.toUpperCase() == 'TRUE');  
                                break;     
                        }
                    }
                    
                });
            } 
        });
        return rows;
    }
    
    
})