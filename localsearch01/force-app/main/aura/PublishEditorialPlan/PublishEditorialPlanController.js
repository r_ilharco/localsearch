({
    handleSuccess: function(cmp, event, helper) {
        var actionOnOrders = cmp.get('c.dateCheckAndUpdates');
        var id = cmp.get("v.recordId");
        actionOnOrders.setParams({editionId:id});
        var data;
        actionOnOrders.setCallback(this, function(response) {
            data = response.getReturnValue();
            console.log('data: ' + data);
            if(data == 'ok'){
                helper.showToast(cmp,event,'SUCCESS', 'Edition Successfully Published!','SUCCESS');             
                
            }else{
                helper.showToast(cmp,event,'ERROR', data,'ERROR');
            }
        });
        $A.enqueueAction(actionOnOrders); 
    },
    handleLoad: function(cmp, event, helper) {
        var fields = event.getParam("recordUi")["record"]["fields"];
        var publicationDate = fields["Publication_Date__c"].value;
        var plannedPublicationDate = fields["Planned_Publication_Date__c"].value;
        if(!publicationDate){
            cmp.find('pubDate').set('v.value',plannedPublicationDate); 
        }
    },

    handleSubmit: function(cmp, event, helper) {

    },

 

    handleError: function(cmp, event, helper) {
    }
})