({
    init: function (component, event, helper) {
        console.log('RECORDSIDS ::: '+component.get("v.caseIdList"));
       component.set("v.isSpinnerActive", true);
       	Promise.all([            
            helper.getPicklistValues(component,event,'Case','Status'),
            helper.getPicklistValues(component,event,'Case','Closure_Reason__c')
        ]).then($A.getCallback(function(res){

            
            res[0].forEach(function(elem){
                 if(elem.value == component.get("v.statusValue")) elem.selected = true;    	
            })
            component.set("v.statusOptions",res[0]);
            
            component.set("v.closeReasonOptions",res[1]);
            
            component.set("v.isSpinnerActive", false);
        }))
        .catch($A.getCallback(function(err){
			console.log(err);
            if(Array.isArray(err)) component.set("v.errorMessage",err[0].message);
            else component.set("v.errorMessage",err.message);
            component.set("v.IsOkButtonDisabled",true);
            component.set("v.isErrorMessage",true);
            component.set("v.isSpinnerActive", false);
           

        })); 
	

    },
    onClickSaveHandler : function (component,event,helper){
        component.set("v.isSpinnerActive", true);
        component.set("v.isErrorMessage",false);
        component.set("v.errorMessage",'');
        component.set("v.successMessage",'');
        component.set("v.isSuccessMessage",false);  
        helper.closeCasesHelper(component).then($A.getCallback(function(res){
            debugger;
            if(res.isSuccess && !res.partialSucces){

                helper.closeActionHelper(component,event,false,true,res.message,false,'');
            }else if(res.isSuccess && res.partialSucces){

                helper.closeActionHelper(component,event,false,true,res.message,true,res.errorMessages);
            }else{
              	component.set("v.errorMessage",res.message);
                component.set("v.isErrorMessage",true);  
            }
            
            
            component.set("v.isSpinnerActive", false);
            }))
            .catch($A.getCallback(function(err){
               	console.log(err);
                if(Array.isArray(err)) component.set("v.errorMessage",err[0].message);
                else component.set("v.errorMessage",err.message);
                component.set("v.isErrorMessage",true);
                component.set("v.isSpinnerActive", false);
            }));
    },

    closeReasonChange : function (component,event,helper){
        var selectReason = component.find('caseClosureReason').get('v.value');
        component.set("v.closeReasonValue",selectReason);
        if(selectReason && component.get("v.caseTotalSize") > 0){
            component.set("v.IsOkButtonDisabled",false);
        }else{
           component.set("v.IsOkButtonDisabled",true); 
        }
    },
    
	closeAction : function(component, event, helper) {
        helper.closeActionHelper(component,event,true,false,'',false,'');
    }
})