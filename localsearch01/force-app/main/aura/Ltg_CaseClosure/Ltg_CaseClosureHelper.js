({    
    getPicklistValues: function (component,event,objectName, fieldName){
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get("c.getPicklistOptions");

            action.setParams({
                objectName : objectName,
            	fieldName : fieldName
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if(state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else {
                    reject(response.getError());
                }
            });

            $A.enqueueAction(action);
        }));
    },
    
    closeActionHelper : function(component, event, closePage,closeWithSuccesMessage,message,partialSuccess,partialeErrorMessage) {
            var myEvent = $A.get("e.c:MassCaseClosureVFPEvent");
            myEvent.setParams({
                closePage: closePage,
                closeWithSuccesMessage : closeWithSuccesMessage,
                sucessMessage : message,
                partialSuccess : partialSuccess,
                partialeErrorMessage : partialeErrorMessage
            });
            myEvent.fire();
    },
    
    closeCasesHelper: function (component, recordId){
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get("c.closeCaseshandler");
			action.setParams({
                caseIdList : component.get("v.caseIdList"),
                caseStatus : component.get("v.statusValue"),
                caseReason : component.get("v.closeReasonValue")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else {
                    reject(response.getError());
                }
            });

            $A.enqueueAction(action);
        }));
    },
})