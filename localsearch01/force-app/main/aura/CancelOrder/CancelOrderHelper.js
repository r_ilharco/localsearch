({
	showToast : function(component,event,type,message,title){
        console.log(type+message+title);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
    }
})