({
    doInit : function(cmp, event, helper) {
    	var oldStatus = cmp.find("statusField").get("v.value");
        cmp.set("v.title",  $A.get("{!$Label.c.Cancellation_of_order}").replace("{OrderNumber}", cmp.find("orderNumberField").get("v.value")));
        cmp.set("v.message",  $A.get("{!$Label.c.Confirm_Cancellation}").replace("{OrderNumber}", cmp.find("orderNumberField").get("v.value")));
        if(oldStatus == 'Activated'){
            helper.showToast(cmp,event,'error', $A.get("{!$Label.c.CANT_CANCEL_ACTIVE_ORDER}"), 'ERROR');  
        }
    },

    cancelDialog : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleSuccess: function(cmp, event, helper) {
        var actionOnOrders = cmp.get('c.cancelOrder');
        var id = cmp.get("v.recordId");
        actionOnOrders.setParams({orderId:id});
        var data;
        actionOnOrders.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('res: ' + JSON.stringify(response.getReturnValue()));
                data = response.getReturnValue();
                console.log('data' + JSON.stringify(data));
                if(data.isOk == true){
                    helper.showToast(cmp,event,'SUCCESS', data.mex ,'SUCCESS');             
                }else{
                    helper.showToast(cmp,event,'ERROR', data.mex ,'ERROR');             
                }
            }else{
                var errors = response.getError();
                console.log('errors' + errors);
                if(errors){
                    helper.showToast(cmp,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));    
                }
            }
        });
        $A.enqueueAction(actionOnOrders); 
    },

    handleLoad: function(cmp, event, helper) {
        console.log('StartLoad');
        cmp.set("v.title",  $A.get("{!$Label.c.Cancellation_of_order}").replace("{OrderNumber}", cmp.find("orderNumberField").get("v.value")));
        cmp.set("v.message",  $A.get("{!$Label.c.Confirm_Cancellation}").replace("{OrderNumber}", cmp.find("orderNumberField").get("v.value")));
        if(cmp.find("cancellationDateField").get("v.value") != null || cmp.find("cancelledByField").get("v.value") != null){
           cmp.set("v.ro", true);
           cmp.set("v.message", $A.get("{!$Label.c.ORDER_CANCELLATION_SUCCESS}") +' ');
        }  
        var oldStatus = cmp.find("statusField").get("v.value");
        var oldStatus = cmp.find("statusField").get("v.value");
        console.log('oldStatusForInit: ' + oldStatus);
        if(oldStatus == 'Activated'){
            helper.showToast(cmp,event,'error', $A.get("{!$Label.c.CANT_CANCEL_ACTIVE_ORDER}"), 'ERROR');  
        }else{
            console.log('oldStatus: ' + oldStatus);
            cmp.set("v.oldStatus", oldStatus);
            var orderNumber = cmp.find("orderNumberField").get("v.value");
            console.log('orderNumber: ' + orderNumber);
            cmp.set("v.orderNumberValue", orderNumber);
            cmp.find("statusField").set("v.value", "Cancelled");   
            var loggedUser = $A.get("$SObjectType.CurrentUser.Id");
            cmp.find("cancelledByField").set("v.value", loggedUser);
            var now = $A.localizationService.formatDateTime(new Date(), "YYYY-MM-DDThh:mm:ssZ");
            if(cmp.find("cancellationDateField").get("v.value") == null ){
                cmp.find("cancellationDateField").set("v.value",now); 
            }
            if(cmp.find("orderCancellationReasonField").get("v.value") == null ){
            	cmp.find("orderCancellationReasonField").set("v.value","Sales Error"); 
            }             
        }
    },

    handleSubmit: function(cmp, event, helper) {
        console.log('StartSubmit');
        console.log('endSubmit');
    },

    handleError: function(cmp, event, helper) {
        console.log('StartError');
        console.log('endError');
    },
    
    checkStatus : function(cmp, event, helper) {
        var actionOnStatus = cmp.get('c.checkOrderStatus');
        var id = cmp.get("v.recordId");
        actionOnStatus.setParams({orderStatus:cmp.get("v.oldStatus")});
        var data;
        actionOnStatus.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('res: ' + JSON.stringify(response.getReturnValue()));
                data = response.getReturnValue();
                console.log('data' + JSON.stringify(data));
                if(data.isOk == false){
                    helper.showToast(cmp,event,'ERROR', data.mex ,'ERROR');             
                }
            }else{
                var errors = response.getError();
                console.log('errors' + errors);
                if(errors){
                    helper.showToast(cmp,event,'error', errors[0].message,$A.get("{!$Label.c.Error}"));    
                }
            }
        });
        $A.enqueueAction(actionOnStatus); 
    }
})