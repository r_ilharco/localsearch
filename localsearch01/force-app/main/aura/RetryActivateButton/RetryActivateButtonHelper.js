({
    showNotifError : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "error",
            "header": header,
            "message": message
        });
    },
    showNotifWarning : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "warning",
            "header": header,
            "message": message
        });
    },
    showNotifSuccess : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "success",
            "header": header,
            "message": message
        });
    },
    closeModal : function(component, event){
        $A.get("e.force:closeQuickAction").fire();
    },
})