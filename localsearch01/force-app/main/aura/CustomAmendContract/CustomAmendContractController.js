({
    doInit: function(component, event, helper) {
        var action = component.get("c.checkForDownsellingUpselling");
        action.setParams({
            				"recordId": component.get("v.recordId")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state '+ JSON.stringify(state));
            $A.get("e.force:closeQuickAction").fire();
            if(state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('Mara MM');
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "https://"+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/AmendContract?Id="+component.get("v.recordId")
                });
                urlEvent.fire();
            }else if (state === "ERROR"){
                var errors = response.getError();
                if(errors) {
                   console.log(errors[0].message);
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": $A.get("{!$Label.c.Warning}"),
                        "message": errors[0].message,
                        "type": "warning"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})