/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 10-11-2019
 * Sprint      : Sprint 11
 * Work item   : SF2-425 - Namirial - Digital Signature integration Enhancement
 * Package     : 
 * Description : 
 * Changelog   : 
 */

({  
    getAccess: function(component, event){
        var action = component.get("c.getAccess");
        action.setParams({recordId: component.get("v.recordId")});
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("--->" + JSON.stringify(result));
                if(result.hasOwnProperty('isSendEmailDisabled')){
                    component.set("v.isSendEmailDisabled",result["isSendEmailDisabled"]);
                    if(result["isSendEmailDisabled"] && !result['isAnotherEnvelopeActivated']){
                        var signatureMethodWarningMessageCMP = component.find("signatureMethodWarningMessage");
                        $A.util.removeClass(signatureMethodWarningMessageCMP,"slds-hide");
                        $A.util.addClass(signatureMethodWarningMessageCMP,"slds-show");
                    }
                }
                    
                this.getAllowedSignatureMethods(component, event);
                
                if(result.hasOwnProperty('envelopeId')) component.set("v.envelopeId", result["envelopeId"]);
                
                if(result.hasOwnProperty('workStepRedirectUrl')) component.set("v.workStepRedirectUrl", result["workStepRedirectUrl"]);
                
                if(result.hasOwnProperty('processType')) component.set("v.processName", result["processType"]);
                if(result.hasOwnProperty('isSecondStep')) component.set("v.isSecondStep", result["isSecondStep"]);
                
                if(Object.getOwnPropertyNames(result).length > 0 && result.hasOwnProperty('isUserAllowed') && result.hasOwnProperty('isComponentEnabled')){
                    if(!result['isUserAllowed']) {
                        $A.get("e.force:closeQuickAction").fire();
                        if(result.hasOwnProperty('info') && result['info'].hasOwnProperty('errorMessages') && result['info']['errorMessages'].length > 0){
                            for(var i = result['info']['errorMessages'].length - 1; i >= 0; i--) {
                                var errorMessage = result['info']['errorMessages'][i];
                                if(errorMessage){
                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.setParams({
                                        "title": "Warning!",
                                        "message": errorMessage,
                                        "type": "warning",
                                        "mode": "sticky"
                                    });
                                    toastEvent.fire();
                                }
                            }
                        }
                    }else if(result['isUserAllowed'] && result['isComponentEnabled']) {
                        if(result['isAnotherEnvelopeActivated'] ) {
                            
    
                                var showMessageCMP = component.find("showMessage");
                                var confirmationDialogCMP = component.find("confirmationDialog");
                                var signatureMethodComboBoxCMP = component.find("signatureMethodComboBox");
                            	var resumeBtn = component.find("resumeButton");
                            	var questionWithoutResume = component.find("questionWithoutResume");
                            	var questionWithResume = component.find("questionWithResume");
                                
                                component.set("v.message",result['info']['messages'][0]); 
                                
                                $A.util.removeClass(signatureMethodComboBoxCMP, "slds-show");
                                $A.util.addClass(signatureMethodComboBoxCMP,"slds-hide");
                                
                                component.set("v.messageSeverity","warning");
                                component.set("v.messageClosable","false");
                                component.set("v.messageTitle",$A.get("$Label.c.Warning"));
                                $A.util.removeClass(showMessageCMP, "slds-hide");
                                $A.util.addClass(showMessageCMP,"slds-show");
                                
                            if(component.get("v.envelopeId") && component.get("v.workStepRedirectUrl")){
                                $A.util.removeClass(resumeBtn, "slds-hide");
								$A.util.removeClass(questionWithResume, "slds-hide");
                            } else {
                                $A.util.removeClass(questionWithoutResume, "slds-hide");
                            }
                                $A.util.removeClass(confirmationDialogCMP, "slds-hide");
                                //$A.util.addClass(confirmationDialogCMP,"slds-show");
                        }
                        else{
                                var signatureMethodComboBoxCMP = component.find("signatureMethodComboBox");
                                $A.util.removeClass(signatureMethodComboBoxCMP, "slds-hide");
                                $A.util.addClass(signatureMethodComboBoxCMP,"slds-show");
                    	}
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
        this.subscribeOnNamirialCallbackChannel(component, event);
    },
  
    showSpinner : function(){
        var div = document.getElementById("spinnerBox");
        div.style = "padding-top: 10rem; text-align: center; display: block;";
        var div = document.getElementById("iframe-container");
        div.style.display = "none";
    },

    hideSpinner : function(){
        var div = document.getElementById("spinnerBox");
        div.style = "padding-top: 10rem; text-align: center; display: none;";
    },

    goToNextStep : function(component, eventReceived){

        this.showSpinner();

        var div = document.getElementById("iframe-container");
        div.style.display = "block";
        this.updateProgressIndicatorToNextStep(component, event);
        div.removeChild(div.childNodes[0]);
        var ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", eventReceived.data.payload.workStepRedirectUrl__c);
        //ifrm.style.width = "1055.28px";
        ifrm.class = "slds-size_12-of-12";
        ifrm.style.width = "inherit";
        ifrm.style.height = "850px";
        ifrm.style.border = "1px solid";
        div.appendChild(ifrm);

        this.hideSpinner();
    },

    showToast : function(title, message, type){
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'title' : title,
            'message' : message,
            'type' : type,
            'mode' : 'sticky'
        });
        toastEvent.fire();
    },

    closeModalRefresh : function(){
        $A.get('e.force:closeQuickAction').fire();
        $A.get('e.force:refreshView').fire();
    },

    evaluateAction : function(component, eventReceived){

        var isSignNow = eventReceived.data.payload.actionCode__c == $A.get("$Label.c.SIGNNOW_NAMIRIALSIGNATUREMETHOD_ACTIONCODE");
        var isSendEmail = eventReceived.data.payload.actionCode__c == $A.get("$Label.c.SENDMAIL_NAMIRIALSIGNATUREMETHOD_ACTIONCODE");
        
        this.showSpinner();
        
        var action = component.get("c.manageCallback");
        action.setParams({
            envelopeId : eventReceived.data.payload.envelopeId__c,
            action : eventReceived.data.payload.action__c,
            quoteDocumentId : eventReceived.data.payload.quoteDocumentId__c,
            actionCode : eventReceived.data.payload.actionCode__c,
            status : eventReceived.data.payload.status__c,
            workStepRedirectUrl : eventReceived.data.payload.workStepRedirectUrl__c
        });
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if(state === "SUCCESS"){
                this.hideSpinner();
                
                var result = response.getReturnValue();

                console.log('Server Side Result: ' + JSON.stringify(result));

                if(result.errorMessage || (isSignNow && result.quoteDocumentStatus == 'Failed')){
                    this.showToast($A.get('$Label.c.Error'), $A.get('$Label.c.Generic_Error'), 'error');
                    this.closeModalRefresh();
                }
                else if(isSignNow && result.quoteDocumentStatus == $A.get('$Label.c.Namirial_DocumentStatus_Waiting')){
                    this.showToast($A.get('$Label.c.Success'), $A.get('$Label.c.Namirial_Process_Completed'), 'success');
                    this.closeModalRefresh();
                }
                else if(isSendEmail){
                    var message = $A.get("$Label.c.MEX_DOCUMENT_SENT");
                    message = message.replace("{salutation}",result.nextRecipient.salutation).replace("{firstName}",result.nextRecipient.firstName).replace("{lastName}",result.nextRecipient.lastName).replace("{email}",result.nextRecipient.email);
                    this.showToast($A.get('$Label.c.Success'), message, 'success');
                    this.closeModalRefresh();
                }
            }
            else{
                this.showToast($A.get('$Label.c.Error'), $A.get('$Label.c.Generic_Error'), 'error');
                this.closeModalRefresh();
            }
        });
        $A.enqueueAction(action);
    },

    subscribeOnNamirialCallbackChannel: function(component, event){
        
        const empApi = component.find('empApi');
        const channel = '/event/Namirial_Callback_Event__e';
        const replayId = -1;
        
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {

            console.log('Received Event --> ' + JSON.stringify(eventReceived));

            var isSignNow = eventReceived.data.payload.actionCode__c == $A.get("$Label.c.SIGNNOW_NAMIRIALSIGNATUREMETHOD_ACTIONCODE");
            var isSendEmail = eventReceived.data.payload.actionCode__c == $A.get("$Label.c.SENDMAIL_NAMIRIALSIGNATUREMETHOD_ACTIONCODE");
            var isSendSignNotification = eventReceived.data.payload.action__c.toLowerCase() == $A.get("$Label.c.NAMIRIAL_ENVELOPESTATUSCALLBACK_SENDSIGNNOTIFICATION").toLowerCase();
            var isWorkstepFinished = eventReceived.data.payload.envelopeId__c == component.get("v.envelopeId") && eventReceived.data.payload.action__c.toLowerCase() === $A.get("$Label.c.NAMIRIAL_ENVELOPESTATUSCALLBACK_WORKSTEPFINISHED").toLowerCase();

            var isRelatedToCurrentEnvId = eventReceived.data.payload.envelopeId__c == component.get("v.envelopeId");
            var isRelatedToCurrentRecord = eventReceived.data.payload.quoteDocumentId__c == component.get("v.recordId");
            
            if(isRelatedToCurrentEnvId && isRelatedToCurrentRecord){
                if(isSignNow){
                    if(isSendSignNotification && eventReceived.data.payload.workStepRedirectUrl__c) this.goToNextStep(component, eventReceived);
                    else if(isWorkstepFinished && eventReceived.data.payload.status__c == 'Completed') this.evaluateAction(component, eventReceived);
                }
                else if(isSendEmail) this.evaluateAction(component, eventReceived);
        	}
 		}));
	},
    
    getAllowedSignatureMethods: function(component, event){
        var action = component.get("c.getAllowedSignatureMethods");
        action.setParams({
            isSendEmailDisabled:  component.get("v.isSendEmailDisabled")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.length > 0)
                    component.set("v.signatureMethodsList", result);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateProgressIndicatorToNextStep: function(component, event){
        var currentStepNumber = component.get("v.progressIndicatorCurrentStep");
        var progressIndicatorStepsSize = component.get("v.progressIndicatorStepsSize");
        
        var li_path_x;
        for(var i = 1; i <= currentStepNumber && i <= progressIndicatorStepsSize; i++)
        {
            li_path_x = document.getElementById("li-path-" + i);
            //console.log(">>>li-path-" + i);
            li_path_x.setAttribute("class","slds-path__item slds-is-active");
        }
        li_path_x = document.getElementById("li-path-" + (currentStepNumber+1));
        //console.log(">>>li-path-" + (currentStepNumber+1));
        li_path_x.setAttribute("class","slds-path__item slds-is-current slds-is-active");
        component.set("v.progressIndicatorCurrentStep", currentStepNumber+1);
    },
    
    deleteEnvelopeRemoteCall: function(component, event){
        //console.log('Helper -> v.recordId -> ' + component.get("v.recordId"));
        //console.log('Helper -> v.envelopeId -> ' + component.get("v.envelopeId"));
        
        let isDeleteEnvelopeStarted = component.get("v.isDeleteEnvelopeStarted");
        
        if(!isDeleteEnvelopeStarted){
            var confirmationDialogCMP = component.find("confirmationDialog");
            var terminateButton = component.find("TerminateButton");
            var showMessageCMP = component.find("showMessage");
            component.set("v.isDeleteEnvelopeStarted", true);
            terminateButton.set("v.disabled", true);
            var div = document.getElementById("spinnerBox");
        	div.style = "padding-top: 10rem; text-align: center; display: block;";
            $A.util.removeClass(confirmationDialogCMP, "slds-show");
            $A.util.addClass(confirmationDialogCMP,"slds-hide");
            $A.util.removeClass(showMessageCMP,"slds-show");
            $A.util.addClass(showMessageCMP,"slds-hide");
            
            var action = component.get("c.deleteEnvelopeCallout_aura");
            action.setParams({
                recordId: component.get("v.recordId"),
                envelopeId: component.get("v.envelopeId")
            });
            
            action.setCallback(this, function(response) {
                //console.log(JSON.stringify(response.getReturnValue()));
                var state = response.getState();
                var response = response.getReturnValue();
                var div = document.getElementById("spinnerBox");
                div.style = "padding-top: 10rem; text-align: center; display: none;";
                component.set("v.isDeleteEnvelopeStarted", false);
                if(state === "SUCCESS"){
                    if(response.statusCode === 200 || response.statusCode === 404){
                        var signatureMethodComboBoxCMP = component.find("signatureMethodComboBox");
                        
                        component.set("v.message",$A.get("$Label.c.Namirial_UI_Message_EnvelopeSuccessfullyDeleted"));
                        component.set("v.messageSeverity","confirm");
                        component.set("v.messageClosable","true");
                        component.set("v.messageTitle",$A.get("$Label.c.Namirial_UI_Message_Success"));
                        $A.util.removeClass(showMessageCMP, "slds-hide");
                        $A.util.addClass(showMessageCMP,"slds-show");
                        
                        var isSendEmailDisabled = component.get("v.isSendEmailDisabled");
            
                        if(isSendEmailDisabled){
                            var signatureMethodWarningMessageCMP = component.find("signatureMethodWarningMessage");
                            $A.util.removeClass(signatureMethodWarningMessageCMP,"slds-hide");
                            $A.util.addClass(signatureMethodWarningMessageCMP,"slds-show");
                        }
                        
                        $A.util.removeClass(signatureMethodComboBoxCMP, "slds-hide");
                        $A.util.addClass(signatureMethodComboBoxCMP,"slds-show");
                    }else{
                        terminateButton.set("v.disabled", false);
                        component.set("v.message",$A.get("$Label.c.Namirial_UI_Message_ErrorOccured") + " " + ((response.statusCode === -1)?$A.get("$Label.c.Namirial_UI_Message_ExceptionThrown"):response.statusCode) + " - " + response.bodyResponse);
                        component.set("v.messageSeverity","error");
                        component.set("v.messageClosable","false");
                        component.set("v.messageTitle",$A.get("$Label.c.Namirial_UI_Message_Error"));
                        $A.util.removeClass(showMessageCMP, "slds-hide");
                        $A.util.addClass(showMessageCMP,"slds-show");
                        $A.util.removeClass(confirmationDialogCMP, "slds-hide");
                        $A.util.addClass(confirmationDialogCMP,"slds-show");
                    }	
                }else{
                    terminateButton.set("v.disabled", false);
                    component.set("v.message",$A.get("$Label.c.Namirial_UI_Message_ErrorOccured_TryAgain"));
                    component.set("v.messageSeverity","error");
                    component.set("v.messageClosable","false");
                    component.set("v.messageTitle",$A.get("$Label.c.Namirial_UI_Message_Error"));
                    $A.util.removeClass(showMessageCMP, "slds-hide");
                    $A.util.addClass(showMessageCMP,"slds-show");
                    $A.util.removeClass(confirmationDialogCMP, "slds-hide");
            		$A.util.addClass(confirmationDialogCMP,"slds-show");
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
	resumeEnvelope: function(component, event){
        var action = component.get("c.requestStepUrl");
        action.setParams({
            'recordId': component.get("v.recordId"),
            'envelopeId': component.get("v.envelopeId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if(res.processFailed)
                {
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Warning!",
                        "message": res.errorMessage,
                        "type": "warning",
                        "mode": "sticky"
                    });
                    toastEvent.fire();
                    return;
                } else {
                    
                    if(res.processStatus == 'Completed'){
                        var eventReceived = {};
                        eventReceived.data = {};
                        eventReceived.data.payload = res.eventReceived;
                        this.evaluateAction(component, eventReceived);
                    	return;
                    }
                    
                    
                	component.set("v.workStepRedirectUrl", res.workStepRedirectUrl);
                    component.set("v.isSecondStep",(res.step > 1));
                }
                var showMessageCMP = component.find("showMessage");
                var confirmationDialogCMP = component.find("confirmationDialog");
                $A.util.removeClass(showMessageCMP, "slds-show");
                $A.util.addClass(showMessageCMP,"slds-hide");
                $A.util.removeClass(confirmationDialogCMP, "slds-show");
                $A.util.addClass(confirmationDialogCMP,"slds-hide");
                this.urlExistsCall(component, event);
            } else {
                component.set("v.message",$A.get("$Label.c.Namirial_UI_Message_ErrorOccured_TryAgain"));
                component.set("v.messageSeverity","error");
                component.set("v.messageClosable","false");
                component.set("v.messageTitle",$A.get("$Label.c.Namirial_UI_Message_Error"));
                $A.util.removeClass(showMessageCMP, "slds-hide");
                $A.util.addClass(showMessageCMP,"slds-show");
                $A.util.removeClass(confirmationDialogCMP, "slds-hide");
                $A.util.addClass(confirmationDialogCMP,"slds-show");
            }
        });
        $A.enqueueAction(action);
        
        
        
        /*var showMessageCMP = component.find("showMessage");
        var confirmationDialogCMP = component.find("confirmationDialog");
        $A.util.removeClass(showMessageCMP, "slds-show");
        $A.util.addClass(showMessageCMP,"slds-hide");
        $A.util.removeClass(confirmationDialogCMP, "slds-show");
        $A.util.addClass(confirmationDialogCMP,"slds-hide");
        this.urlExistsCall(component, event);*/
    },
    
    urlExistsCall: function(component, event){
    				var signNowStepNames = ["Customer Signature","Sales Agent Signature"];
                    var manualStepNames = ["Sales Agent Signature","Send Email"];
                    var sendEmailStepNames = ["Sales Agent Signature","Send Email"];
                    
                    var stepNames = [];
                    switch(component.get("v.processName"))
                    {
                        case $A.get("$Label.c.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = signNowStepNames;
                            break;
                        case $A.get("$Label.c.SENDMAIL_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = sendEmailStepNames;
                            break;
                        case $A.get("$Label.c.MANUAL_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = manualStepNames;
                            break;
                    }
                                        
                    //console.log('stepNames: ' + JSON.stringify(stepNames));
                    
                    var unorderedlist = document.getElementById("unorderedlist");
                    var i = 1;
                    stepNames.forEach(function(element){
                        	var li = document.createElement("li");
                        	var liclasses = "slds-path__item slds-is-incomplete";
                        	li.setAttribute("class",liclasses);
                        	li.setAttribute("role","presentation");
                        	li.setAttribute("id","li-path-"+i);
                        	var a = document.createElement("a");
                        	a.setAttribute("aria-selected","true");
                        	a.setAttribute("class","slds-path__link");
                        	//a.href = "javascript:void(0);";
                        	a.setAttribute("id","path-"+i);
                        	a.setAttribute("role","option");
                        	a.setAttribute("tabindex","0");
                        	var span1 = document.createElement("span");
                        	span1.setAttribute("class", "slds-path__stage");
                        	var span2 = document.createElement("span");
                        	span2.setAttribute("class", "slds-path__title");
                        	span2.setAttribute("style", "font-size: 12px")
                        	span2.innerHTML = element;
                        	a.appendChild(span1);
                        	a.appendChild(span2);
                        	li.appendChild(a);
                        	unorderedlist.appendChild(li);
                        	i++;
                    	}
                    );

                    component.set("v.progressIndicatorCurrentStep",1);
                    component.set("v.progressIndicatorStepsSize", stepNames.length);
                    
        			var li_path = document.getElementById("li-path-"+component.get("v.progressIndicatorCurrentStep"));
        			li_path.setAttribute("class","slds-path__item slds-is-current slds-is-active");
               		
        			var isSecondStep = component.get("v.isSecondStep");
                    if(isSecondStep){
                        component.set("v.progressIndicatorCurrentStep",2);
                        li_path = document.getElementById("li-path-"+component.get("v.progressIndicatorCurrentStep"));
        				li_path.setAttribute("class","slds-path__item slds-is-current slds-is-active");
                    }

        			var progressIndicatorCMP = component.find("progressIndicator");
                    $A.util.removeClass(progressIndicatorCMP, "slds-hide");
        
                    var div = document.getElementById("iframe-container");
                    
                    var ifrm = document.createElement("iframe");
                    ifrm.setAttribute("src", component.get("v.workStepRedirectUrl"));
                    ifrm.class = "slds-size_12-of-12";
                    ifrm.style.width = "inherit";
                    ifrm.style.height = "850px";
                    ifrm.style.border = "1px solid";
                    div.appendChild(ifrm);
    }        
            
})