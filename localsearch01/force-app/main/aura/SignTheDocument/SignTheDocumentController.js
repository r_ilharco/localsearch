/**
 * Author	   : Gennaro Casola <gcasola@deloitte.it>
 * Date		   : 10-11-2019
 * Sprint      : Sprint 11
 * Work item   : SF2-425 - Namirial - Digital Signature integration Enhancement
 * Package     : 
 * Description : 
 * Changelog   : 
 */

({
    doInit: function(component, event, helper)
    {
        helper.getAccess(component, event);
    },
    
    handleChange: function(component, event, helper)
    {
       /* var signatureMethodComboBoxCMP = component.find("signatureMethodComboBox");
        $A.util.removeClass(signatureMethodComboBoxCMP, "slds-show");
        $A.util.addClass(signatureMethodComboBoxCMP,"slds-hide");*/
        var signatureMethodComboBoxCMP = component.find("signatureMethodComboBox");
        $A.util.removeClass(signatureMethodComboBoxCMP, "slds-show");
        $A.util.addClass(signatureMethodComboBoxCMP,"slds-hide");
        
        var isSendEmailDisabled = component.get("v.isSendEmailDisabled");
            
        if(isSendEmailDisabled){
            var signatureMethodWarningMessageCMP = component.find("signatureMethodWarningMessage");
            $A.util.removeClass(signatureMethodWarningMessageCMP,"slds-show");
            $A.util.addClass(signatureMethodWarningMessageCMP,"slds-hide");
        }
        
        var div = document.getElementById("spinnerBox");
        div.style = "padding-top: 10rem; text-align: center; display: block;";
        component.set("v.processName",signatureMethodComboBoxCMP.get("v.value"));
        //console.log('component.get("v.recordId"): '+ component.get("v.recordId"));
        //console.log('signatureMethodComboBoxCMP.get("v.value"): ' + signatureMethodComboBoxCMP.get("v.value"));
        var action = component.get("c.namirialSignatureProcess_v2");
        action.setParams({
            'recordId': component.get("v.recordId"),
            'method': signatureMethodComboBoxCMP.get("v.value")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log('response.getState(): ' + response.getState());
            //console.log('response: ' + JSON.stringify(response));
            debugger;
            if (state === "SUCCESS") {
                //helper.subscribeOnNamirialCallbackChannel(component, event);
                //console.log('response');
                //console.log('response: ' + JSON.stringify(response));
                var res = response.getReturnValue();
                //console.log('response.getReturnValue(): ' + JSON.stringify(response.getReturnValue()));
                //console.log('response: ' + JSON.stringify(response.getReturnValue()));
                component.set("v.workStepRedirectUrl", res.workStepRedirectUrl);
                component.set("v.envelopeId", res.envelopeId);
                
                //console.log("v.workStepRedirectUrl: " + component.get("v.workStepRedirectUrl"));
                
                var div = document.getElementById("spinnerBox");
                div.style = "display: none;";
                
                if(res.processFailed)
                {
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Warning!",
                        "message": res.errorMessage,
                        "type": "warning",
                        "mode": "sticky"
                        
                    });
                    toastEvent.fire();
                    return;
                }
                
                if(typeof component.get("v.workStepRedirectUrl") !== "undefined")
                {
                    helper.urlExistsCall(component, event);
                    /*var signNowStepNames = ["Customer Signature","Sales Agent Signature"];
                    var manualStepNames = ["Sales Agent Signature","Send Email"];
                    var sendEmailStepNames = ["Sales Agent Signature","Send Email"];
                    
                    var stepNames = [];
                    switch(component.get("v.processName"))
                    {
                        case $A.get("$Label.c.SIGNNOW_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = signNowStepNames;
                            break;
                        case $A.get("$Label.c.SENDMAIL_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = sendEmailStepNames;
                            break;
                        case $A.get("$Label.c.MANUAL_NAMIRIALSIGNATUREMETHOD_LABEL"):
                            stepNames = manualStepNames;
                            break;
                    }
                                        
                    //console.log('stepNames: ' + JSON.stringify(stepNames));
                    
                    var unorderedlist = document.getElementById("unorderedlist");
                    var i = 1;
                    stepNames.forEach(function(element){
                        	var li = document.createElement("li");
                        	var liclasses = "slds-path__item slds-is-incomplete";
                        	li.setAttribute("class",liclasses);
                        	li.setAttribute("role","presentation");
                        	li.setAttribute("id","li-path-"+i);
                        	var a = document.createElement("a");
                        	a.setAttribute("aria-selected","true");
                        	a.setAttribute("class","slds-path__link");
                        	//a.href = "javascript:void(0);";
                        	a.setAttribute("id","path-"+i);
                        	a.setAttribute("role","option");
                        	a.setAttribute("tabindex","0");
                        	var span1 = document.createElement("span");
                        	span1.setAttribute("class", "slds-path__stage");
                        	var span2 = document.createElement("span");
                        	span2.setAttribute("class", "slds-path__title");
                        	span2.setAttribute("style", "font-size: 12px")
                        	span2.innerHTML = element;
                        	a.appendChild(span1);
                        	a.appendChild(span2);
                        	li.appendChild(a);
                        	unorderedlist.appendChild(li);
                        	i++;
                    	}
                    );
                    
                    var li_path_1 = document.getElementById("li-path-1");
                    li_path_1.setAttribute("class","slds-path__item slds-is-current slds-is-active");
                    component.set("v.progressIndicatorCurrentStep",1);
                    component.set("v.progressIndicatorStepsSize", stepNames.length);
                    
                    var progressIndicatorCMP = component.find("progressIndicator");
                    $A.util.removeClass(progressIndicatorCMP, "slds-hide");
                    $A.util.addClass(progressIndicatorCMP,"slds-show");
                    
                    var div = document.getElementById("iframe-container");
                    
                    var ifrm = document.createElement("iframe");
                    ifrm.setAttribute("src", component.get("v.workStepRedirectUrl"));
                    ifrm.class = "slds-size_12-of-12";
                    ifrm.style.width = "inherit";
                    ifrm.style.height = "850px";
                    ifrm.style.border = "1px solid";
                    div.appendChild(ifrm);*/
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteEnvelope: function(component, event, helper){
        helper.deleteEnvelopeRemoteCall(component, event);
    },
    resumeEnvelope: function(component, event, helper){
        helper.resumeEnvelope(component, event);
    },
    
    
    closeQuickAction: function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    }
})