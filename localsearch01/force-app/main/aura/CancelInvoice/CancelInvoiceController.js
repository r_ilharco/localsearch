({
    cancelDialog : function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
    },
    cancelInvoice : function(component, event, helper) {
        event.preventDefault();
        component.set('v.showSpinner', true);
        var action = component.get('c.cancelByInvoiceId');
        action.setParams({invoiceId:component.get('v.recordId')});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var msg = response.getReturnValue();
                if( msg == null ){
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    var toastEvent = $A.get("e.force:showToast");
                    var invNum = component.get('v.invoiceRec.Name');
                    toastEvent.setParams({"title": "Invoice cancellation","message": "Invoice # " + invNum + " has been cancelled.","type": "info"});
                    toastEvent.fire();
                    return false;
                } else {
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    var toastEvent = $A.get("e.force:showToast");
                    var invNum = component.get('v.invoiceRec.Name');
                    toastEvent.setParams({"title": "Invoice cancellation","message": msg ,"type": "error"});
                    toastEvent.fire();
                }
            } else {
                component.set('v.error', response.getState());
            }
			component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
        return true;
    },
    doInit : function(component, event, helper) {
		var action = component.get('c.getInvoice');
        action.setParams({invoiceId:component.get('v.recordId')});
        action.setCallback(this, function(response) {
            var invoice = response.getReturnValue();
            if(invoice) {
            	component.set('v.invoiceRec', invoice);
                // Check cancellability
                if([$A.get("$Label.c.Invoice_Status_Draft"), 
                    $A.get("$Label.c.Invoice_Status_In_Collection")]
                   .indexOf(invoice.Status__c) >=0) {
                    component.set('v.warn', "Invoice can't be cancelled as it was never sent.\nPlease cancel the subscription, contract or credit note to remove the invoice.");
                    return;                    
                } else if(invoice.Status__c == $A.get("$Label.c.Invoice_Status_Cancelled")) {
                    component.set('v.warn', "Invoice is already cancelled.");
                    return;
                }
                var msg = "";
                if(invoice.Registered__c === true) {
                    // Extra reminder: remove from Abacus
                    msg = "Invoice is registered in Abacus. Ensure you properly remove the invoice also in Abacus.";                    
                }
                if(invoice.Open_Amount__c != null && invoice.Open_Amount__c != invoice.Total__c) {
                    // Payed (at least partially) -> storno!! (or not cancellable)
                    if(msg!='') msg += '\n';
                    msg += "Invoice has been partially or totally payed. The payed amount should be returned to the customer.";
                }
                if(msg!='') {
                    component.set('v.warn', msg);
                }
                component.set('v.isCancellable', true);
            } else {
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"title": "Invoice cancellation","message": "No valid invoice selected.","type": "error"});
                toastEvent.fire();                
            }
        });
        $A.enqueueAction(action);
    }
})
