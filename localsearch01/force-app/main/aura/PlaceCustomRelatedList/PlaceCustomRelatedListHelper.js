({
    fetchPlaceHelper : function(component, event, helper) {
        component.set('v.count',0);
        var action = component.get("c.getRecordList");
        action.setParams({ recId: component.get('v.recordId') });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('result '+JSON.stringify(response.getReturnValue()));
                if(response.getReturnValue().length>0){
                    component.set('v.count',response.getReturnValue().length);
                    var rows = response.getReturnValue();
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                       	row.AccountName = row.Account__r.Name;
                        row.AccountId = '/'+ row.Account__c;
                        row.PlaceId = '/'+ row.Id;
                    }
                    component.set('v.mycolumns', [
                        {label: 'Name', fieldName: 'PlaceId',type: 'url',typeAttributes: { label: { fieldName: 'Name' ,target: '_self'}}},
                        {label: 'Account', fieldName: 'AccountId',type: 'url',typeAttributes: { label: { fieldName: 'AccountName' ,target: '_self'}}},
                        {label: 'Address', fieldName: 'SearchField_Address__c', type: 'text'},
                        {label: 'Phone', fieldName: 'Phone__c', type: 'text'},
                        {label: 'Email', fieldName: 'Email__c', type: 'text'}
                    ]);
                    component.set("v.mydata", response.getReturnValue());
                }
            }
            else{
                 var errors = response.getError();
                 if(errors) console.log(errors[0].message);
            }
        });
        $A.enqueueAction(action);
    }
})