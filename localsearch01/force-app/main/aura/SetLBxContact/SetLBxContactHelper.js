({
	getContactsToChoose : function(component) {
		var quoteLineId = component.get('v.recordId');
        var action = component.get('c.contactToChoose');
        action.setParams({'quoteLineId' : quoteLineId});        
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state === 'SUCCESS'){
                var contacts = res.getReturnValue();
                if(contacts){
                    component.set('v.listContact', contacts);
                }
                else{
                    console.log('No Contacts');
                }
            }
            else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
	},
    
    setContactOnQL : function(component, selectedContactId) {
		var quoteLineId = component.get('v.recordId');
        var action = component.get('c.updateQLWithContact');
        action.setParams({
            'quoteLineId' : quoteLineId,
            'contactId' : selectedContactId
        });        
        action.setCallback(this, function(res){
            var state = res.getState();
            if(state === 'SUCCESS'){
               
                var saveResult = res.getReturnValue();
                
                if(saveResult.isSuccess){
                    this.showToast(component,'Success!' ,'success', 'Record updated.');
                }
                else{
                    //this.showToast(component, 'error', 'Something went wrong, please try again.');
                    this.showToast(component,'Error!' ,'error', saveResult.errorMessage);
                }
            }
            else{
                this.showToast(component, 'error', 'Something went wrong, please try again.');
            }
            component.find("overlayLib").notifyClose();
        });
        $A.enqueueAction(action);
	},
    
    showToast : function(component,title, level, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : level,
            "message": message
        });
        toastEvent.fire();
    }
})