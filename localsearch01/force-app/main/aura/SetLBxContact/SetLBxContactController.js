({
    doInit : function(component, event, helper) {
       helper.getContactsToChoose(component);
    },
    
    contactHandler : function(component, event, helper){
      	var contactId = component.find('contactSelector').get('v.value');
        console.log('CONTACT ID: '+contactId);
        component.set('v.selectedContactId', contactId);
    },
    
	showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },

    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
    
    handleCancelClick : function(component,event,helper){
        component.find("overlayLib").notifyClose();
    },
    
    handleSaveClick : function(component,event,helper){
        console.log('SAVE CLICKED');
        var selectedContactId = component.get('v.selectedContactId');
        if(selectedContactId){
            helper.setContactOnQL(component, selectedContactId);
        }
    }
    
})