({
    doInit : function (component,event,helper) {
        component.set("v.loading",false);        
    },
    
    handleClose : function(component,event,helper) {
        $A.get("e.force:closeQuickAction").fire();     
    },

    handleEditLines :  function(component,event,helper){
      helper.navigateToURL(component,event,'https://'+window.location.hostname.replace(".lightning.force.com","")+"--sbqq.visualforce.com/apex/sb?Id="+component.get("v.recordId"));

    },
})