({  doInit: function (cmp, event, helper) {    
    var profileName = '';
    var getProfileNameAction = cmp.get('c.getProfileName');
    getProfileNameAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log('State: '+state);
        if (state === "SUCCESS") {
            profileName = response.getReturnValue();
            console.log('Profile Name: '+profileName);
        }
        else {
            console.log("Failed with state: " + state);
            return null;
        }
        var columns;
        var userInfo = profileName.split(';');
        if((userInfo.includes('Customer Care') && userInfo.includes('Order_Management')) || userInfo.includes('System Administrator')){
            columns = [
            { type: 'text', initialWidth: 180,fieldName: 'qlProductName', label: 'New Product' },
            { type: 'text', initialWidth: 180,fieldName: 'productName', label: 'Product Name' },
            { type: 'url' ,initialWidth: 100, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
            { type: 'text' ,initialWidth: 100, fieldName: 'place_ID', label: 'PlaceID', typeAttributes: { label: { fieldName: 'place_ID' } } },
            //{ type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
            { type: 'percent',initialWidth: 100, fieldName: 'discount',label: 'Discount'},
            { type: 'currency' ,initialWidth: 100, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } },
            { type: 'url' ,initialWidth: 100, fieldName: 'qlUrl', label: 'Line Name', typeAttributes: { label: { fieldName: 'lineName' }, target: '_parent' } },
            //{ type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
            //{ type: 'text', initialWidth: 200,fieldName: 'billingStatus', label: 'Billing Status' },
            { type: 'number' ,initialWidth: 80, fieldName: 'quantity', label: 'Quantity', typeAttributes: { label: { fieldName: 'quantity' } } },
            //{ type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
            { type: 'date' ,initialWidth: 100, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
            { type: 'date' ,initialWidth: 100, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
            //{ type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
            // { type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
            { type: 'number' ,initialWidth: 100, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
            { type: 'url' ,initialWidth: 100, fieldName: 'contractUrl', label: 'Linked Contract', typeAttributes: { label: { fieldName: 'contractNumber' }, target: '_parent' } },	 
            { type: 'url' ,initialWidth: 100, fieldName: 'id', label: 'Linked Subscription', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } }
        	];
        }
        else{
        //if(profileName == 'System Administrator'){
        columns = [
            { type: 'text', initialWidth: 180,fieldName: 'qlProductName', label: 'New Product' },
            { type: 'text', initialWidth: 180,fieldName: 'productName', label: 'Product Name' },
            { type: 'url' ,initialWidth: 100, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
            //{ type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
            { type: 'percent',initialWidth: 100, fieldName: 'discount',label: 'Discount'},
            { type: 'currency' ,initialWidth: 100, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } },
            { type: 'url' ,initialWidth: 100, fieldName: 'qlUrl', label: 'Line Name', typeAttributes: { label: { fieldName: 'lineName' }, target: '_parent' } },
            //{ type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
            //{ type: 'text', initialWidth: 200,fieldName: 'billingStatus', label: 'Billing Status' },
            { type: 'number' ,initialWidth: 80, fieldName: 'quantity', label: 'Quantity', typeAttributes: { label: { fieldName: 'quantity' } } },
            //{ type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
            { type: 'date' ,initialWidth: 100, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
            { type: 'date' ,initialWidth: 100, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
            //{ type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
            // { type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
            { type: 'number' ,initialWidth: 100, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
            { type: 'url' ,initialWidth: 100, fieldName: 'contractUrl', label: 'Linked Contract', typeAttributes: { label: { fieldName: 'contractNumber' }, target: '_parent' } },	 
            { type: 'url' ,initialWidth: 100, fieldName: 'id', label: 'Linked Subscription', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } }
        ];
        }
        //}
        /*else if(profileName == 'Finance'){
            columns = [
                { type: 'text', initialWidth: 200,fieldName: 'productName', label: 'Product Name' },
                { type: 'url' ,initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
                //{ type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
                { type: 'percent',initialWidth: 200, fieldName: 'discount',label: 'Discount'},
                { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } },
                { type: 'url' ,initialWidth: 200, fieldName: 'lineId', label: 'Line Name', typeAttributes: { label: { fieldName: 'lineName' }, target: '_parent' } },
                { type: 'number' ,initialWidth: 250, fieldName: 'quantity', label: 'Quantity', typeAttributes: { label: { fieldName: 'quantity' } } },
                //{ type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
                //{ type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
                //{ type: 'text', initialWidth: 200,fieldName: 'billingStatus', label: 'Billing Status' },
                { type: 'date' ,initialWidth: 200, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
                { type: 'date' ,initialWidth: 200, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
                //{ type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
                //{ type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
                { type: 'number' ,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
                { type: 'url' ,initialWidth: 200, fieldName: 'contractUrl', label: 'Linked Contract', typeAttributes: { label: { fieldName: 'contractNumber' }, target: '_parent' } },
                { type: 'url' ,initialWidth: 200, fieldName: 'id', label: 'Linked Subscription', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } }
            ];
            console.log('columns for Finance');
        }else{
            columns = [
                { type: 'text', initialWidth: 200,fieldName: 'productName', label: 'Product Name' },
                { type: 'url' ,initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
                //{ type: 'currency' ,initialWidth: 200, fieldName: 'baseTermPrice', label: 'Base Term Price' },
                { type: 'percent',initialWidth: 200, fieldName: 'discount',label: 'Discount'},
                { type: 'currency' ,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } },
                { type: 'text' ,initialWidth: 200, fieldName: 'lineName', label: 'Line Name'},
                { type: 'number' ,initialWidth: 250, fieldName: 'quantity', label: 'Quantity', typeAttributes: { label: { fieldName: 'quantity' } } },
                //{ type: 'number' ,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
                // { type: 'text', initialWidth: 200,fieldName: 'status', label: 'Status' },
                { type: 'date' ,initialWidth: 200, fieldName: 'startDate', label: 'Start Date', typeAttributes: { label: { fieldName: 'startDate' } } },
                { type: 'date' ,initialWidth: 200, fieldName: 'endDate', label: 'End Date', typeAttributes: { label: { fieldName: 'endDate' } } },
                //{ type: 'text' ,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
                //{ type: 'date' ,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
                { type: 'number' ,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
                { type: 'url' ,initialWidth: 200, fieldName: 'contractUrl', label: 'Linked Contract', typeAttributes: { label: { fieldName: 'contractNumber' }, target: '_parent' } },
                { type: 'url' ,initialWidth: 200, fieldName: 'id', label: 'Linked subscription', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } }
            ];
            console.log('columns for other profiles');
        }*/ 
        console.log('columns: ' + columns);
        cmp.set('v.gridColumns', columns);
        helper.getSubscription(cmp, event);
    });    
    $A.enqueueAction(getProfileNameAction); 
	},
   handleModalOpen: function(component) {
        console.log('open model record id ' + component.get('v.recordId'));
  		var recordId = component.get("v.recordId");
        var action = component.get("c.getAccess");
       
        console.log('component.get("v.recordId"): ' + component.get("v.recordId"));
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getAccess->response: ' + JSON.stringify(response));
            console.log('state: ' + state);            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("--->" + JSON.stringify(result));
                
                if(Object.getOwnPropertyNames(result).length > 0 
                   && result.hasOwnProperty('isUserAllowed') && result.hasOwnProperty('isComponentEnabled')){
                    if(!result['isUserAllowed'])
                    {
                        $A.get("e.force:closeQuickAction").fire();
                        if(result.hasOwnProperty('info') 
                           && result['info'].hasOwnProperty('errorMessages')
                           && result['info']['errorMessages'].length > 0){
                            for(var i = result['info']['errorMessages'].length - 1; i >= 0; i--)
                            {
                                var errorMessage = result['info']['errorMessages'][i];
                                if(errorMessage){
                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.setParams({
                                        "title": "Warning!",
                                        "message": errorMessage,
                                        "type": "warning",
                                        "mode": "sticky"
                                    });
                                    toastEvent.fire();
                                }
                            }
                        }
                    }
                    else{component.set('v.showModalPanel', true);}
                }
            }
        });
        
        $A.enqueueAction(action);
     },

     handleModalCancel: function(component) {
         component.set('v.showModalPanel', false);
     },

});