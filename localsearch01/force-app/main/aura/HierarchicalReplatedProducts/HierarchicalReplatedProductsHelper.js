({
	  getSubscription: function (component, event, helper) {
        var profileName;
        var action = component.get('c.getReplacedByQuoteId');
        action.setParams({ quoteId : component.get('v.recordId')});        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                var count = 0;
                data.subscriptions.forEach(function(element) {
                  element._children = element.childrens;
                  count ++;  
                  element.childrens.forEach(function(childrenElement) {
                      childrenElement.id = '/' + childrenElement.id;
                      childrenElement.productId = '/' + childrenElement.productId;                                        
                  });
                  
                  element.id = '/' + element.id;
                  element.productId = '/' + element.productId;
                  if(element.placeId){
                  	element.placeId = '/' + element.placeId;
                  }
                  element.qlUrl = '/' + element.lineId;
                  element.contractUrl = '/' + element.contractId;
                });
                
                
                console.log('subscriptions -> ' + JSON.stringify(data.subscriptions));
				component.set('v.gridData', data.subscriptions);
				component.set('v.ProductsCounter',count);	                
                var expandedRows = [];
				component.set('v.gridExpandedRows', expandedRows);
            } else {
                this.showNotifError(component, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})