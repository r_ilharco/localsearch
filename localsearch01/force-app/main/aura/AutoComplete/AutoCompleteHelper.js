({
    searchRecords : function(component, searchString) {

            const serverResult = component.get("v.listToSearch");//response.getReturnValue();
        	if(serverResult){
        		//console.log('list to search'+JSON.stringify(component.get("v.listToSearch")));
                const results = [];
                //Vincenzo Laudato - SF2-691
                /*if(!serverResult){
                    var compEvent = component.getEvent("selectionEvent");
                    compEvent.setParams({
                        'panelId' : panelId,
                        'selectedValue' : 'EMPTY'
                    });
                    compEvent.fire();
                }*/
                serverResult.forEach(element => {
                    //console.log(element['label']);
                    if(element['label'].toUpperCase().includes(searchString.toUpperCase())){
                    	const result = {id : element['value'], value : element['label']};
                    	results.push(result);
    				}
                });
                component.set("v.results", results);
                if(results.length>0){
                    component.set("v.openDropDown", true);
				} 
                var labelEd = $A.get("$Label.c.ENH_Choose_the_Edition");
                var labelCat = $A.get("$Label.c.ENH_Choose_the_Category");
                var labelLoc = $A.get("$Label.c.ENH_Choose_the_Location");
				var labelPlace = $A.get("$Label.c.ENH_Choose_the_Place");
				//console.log('PANELID: '+component.get('v.panelId'));
 				if(results.length == 0 && component.get("v.panelId") == 'edition'){
 					document.getElementById("message1").style.display = "contents";
 				}
                else if(results.length == 0 && component.get("v.panelId") == 'category'){
                    document.getElementById("message3").style.display = "contents";
                }
				else if(results.length == 0 && component.get("v.label") == labelLoc){
                    document.getElementById("message2").style.display = "contents";
                }
				else if(results.length == 0 && component.get("v.label") == labelPlace){
                    document.getElementById("message4").style.display = "contents";
                }
 			}	

    },
	
    sendSelectedValue : function (component, selectedId, selectedValue){
        var panelId = component.get('v.panelId');
		//var selectedValue = component.get('v.selectedOption'); 
        console.log('Firing event for panel '+panelId+'; value id: '+selectedId+ '; myKey: '+component.get("v.myKey"));
        var compEvent = component.getEvent("selectionEvent");
        compEvent.setParams({
            'panelId' : panelId,
            'selectedValue' : selectedValue,
            'selectedValueId' : selectedId,
            'whoIAM' : component.get("v.myKey"),
            'panelType' : component.get('v.panelType')
        });
        compEvent.fire();
    }
})