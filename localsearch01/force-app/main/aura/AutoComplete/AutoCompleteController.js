({
   
    doInit : function(component, event, helper){
        component.set("v.myInputValue",component.get("v.inputValue"));
        component.set("v.mySelectedOpt",component.get("v.selectedOption"));
        
        if(component.get('v.inputValue') && component.get('v.selectedOption')){
            var advOnline = ["LBANNER001", "LBANNERSWISS001", "LTOPLY001", "LTOPLW001", "LMFIRST001", "SBANNER001"];
            var prodCode = component.get('v.productCode');
            if(prodCode && advOnline.includes(prodCode)){
                component.set('v.disablePanel', true);
            }
        }
    },
    
    turnOffAutocomplete : function(component, event) {
        if(event && event.target && event.target.getAttribute("autocomplete") !== "off"){
            event.target.setAttribute("autocomplete","off");
        }
    },
    
    searchHandler : function (component, event, helper) {
        var labelEd = $A.get("$Label.c.ENH_Choose_the_Edition");
        var labelCat = $A.get("$Label.c.ENH_Choose_the_Category");
		var labelLoc = $A.get("$Label.c.ENH_Choose_the_Location");
        var labelPlace = $A.get("$Label.c.ENH_Choose_the_Place");
        const searchString = event.target.value;
        if(document.getElementById("message1") && component.get('v.label') == labelEd) document.getElementById("message1").style.display = "none";
        if(document.getElementById("message3") && component.get('v.label') == labelCat) document.getElementById("message3").style.display = "none";
        if(document.getElementById("message2") && component.get('v.label') == labelLoc) document.getElementById("message2").style.display = "none";
        if(document.getElementById("message4") && component.get('v.label') == labelPlace) document.getElementById("message4").style.display = "none";
        if (searchString.length >= 1) {
            //Ensure that not many function execution happens if user keeps typing
            if (component.get("v.inputSearchFunction")) {
                clearTimeout(component.get("v.inputSearchFunction"));
            }

            var inputTimer = setTimeout($A.getCallback(function () {
                helper.searchRecords(component, searchString);
            }), 1000);
            component.set("v.inputSearchFunction", inputTimer);
        } else{
            component.set("v.results", []);
            component.set("v.openDropDown", false);
        }
    },

    optionClickHandler : function (component, event, helper) {
        const selectedId = event.target.closest('li').dataset.id;
        const selectedValue = event.target.closest('li').dataset.value;
        component.set("v.myInputValue", selectedValue);
        component.set("v.openDropDown", false);
        component.set("v.mySelectedOpt", selectedId);
        
        helper.sendSelectedValue(component, selectedId, selectedValue);
    },

    clearOption : function (component, event, helper) {
        component.set("v.results", []);
        component.set("v.openDropDown", false);
        component.set("v.myInputValue", "");
        component.set("v.mySelectedOpt", "");
        helper.sendSelectedValue(component, "", "");
    },
    
    disableList : function (component, event, helper){
        
        component.set("v.openDropDown",false);
    },
    
    handleApplicationEvent : function(component, event, helper){
        console.log('Received event '+event.getParam("message")+' from panel '+component.get('v.panelId'));
        var message = event.getParam("message");
        var panelId = component.get('v.panelId');
        if( (message == 'date' && panelId == 'edition') || (message == 'categoryLanguage' && panelId == 'category') || (message == 'categoryLanguage' && panelId == 'location') ){
            component.set("v.results", []);
            component.set("v.openDropDown", false);
            component.set("v.myInputValue", "");
            component.set("v.mySelectedOpt", "");
        }
        if( (message == 'categoryLanguagePrint' && panelId == 'category')){
            component.set("v.results", []);
            component.set("v.openDropDown", false);
            component.set("v.myInputValue", "");
            component.set("v.mySelectedOpt", "");
        }
        if(message == 'casaCat' && panelId == 'category'){
            var valueId = event.getParam("valueId");
            var valueLabel = event.getParam("valueLabel");
            component.set("v.myInputValue", valueLabel);
            component.set("v.mySelectedOpt", valueId);
            //vlaudato to check
            var langCheck = event.getParam("languageChange");
            if(langCheck === false) component.set('v.disablePanel', true);
        }
        if(message == 'casaLoc' && panelId == 'location'){
            var valueId = event.getParam("valueId");
            var valueLabel = event.getParam("valueLabel");
            component.set("v.myInputValue", valueLabel);
            component.set("v.mySelectedOpt", valueId);
            //vlaudato to check
            component.set('v.disablePanel', true);
        }
        if(message == 'date' && panelId == 'category'){
            component.set("v.openDropDown", false);
            component.set("v.myInputValue", "");
            component.set("v.mySelectedOpt", "");
        }
        if(message == 'change' && panelId == 'category'){
            component.set("v.openDropDown", false);
            component.set("v.myInputValue", "");
            component.set("v.mySelectedOpt", "");
            component.set('v.disablePanel', false);
        }
        if(message == 'change' && panelId == 'location'){
            component.set("v.openDropDown", false);
            component.set("v.myInputValue", "");
            component.set("v.mySelectedOpt", "");
            component.set('v.disablePanel', false);
        }
    }
})