({
    init: function (cmp, event, helper) {
        var timePeriods = [
            {label : '', value :''},
            //{label : $A.get("$Label.c.CtrOverview_ThisMonth"), value :'THIS_MONTH'},
            {label : $A.get("$Label.c.CtrOverview_ThisQuarter"), value :'THIS_QUARTER'},
            {label : $A.get("$Label.c.CtrOverview_Last6Month"), value :'LAST_N_MONTHS:6'},
            {label : $A.get("$Label.c.CtrOverview_ThisYear"), value :'THIS_YEAR'}
        ];
        
        cmp.set('v.periods', timePeriods);
        console.log('oeriods' + cmp.get('v.periods'));
        helper.getPicklistValues(cmp, event);
        //helper.getSubscription(cmp, event);
        //helper.setColumns(cmp, event, helper);
    },
    handleKeyUp: function (cmp, event, helper) {
       //var isEnterKey = event.keyCode === 13;
        
        var queryTerm = cmp.find('subscriptionsFilter01').get('v.value');
        console.log('queryTerm1.1 :: '+queryTerm);

        if (/*isEnterKey &&*/ (queryTerm.length>=3)) {
            console.log('queryTerm :: '+queryTerm);
            cmp.set('v.issearching', true);
            helper.getSubscription(cmp, event);

        }else if(!queryTerm && queryTerm.length==0){
            helper.getSubscription(cmp, event);
        }
    },
    setStartDate: function(cmp, event, helper) {
        cmp.set('v.issearching', true);
        helper.getSubscription(cmp, event);
        //helper.setColumns(cmp, event, helper);
    },
    setEndDate: function(cmp, event, helper) {
        cmp.set('v.issearching', true);
        helper.getSubscription(cmp, event);
        //helper.setColumns(cmp, event, helper);
    },
    handleSelectedValues : function(cmp, event, helper){
        cmp.set('v.issearching', true);
        var selectedValues = event.getParam("selectedValues");
        cmp.set("v.selectedStatuses", selectedValues);
        helper.getSubscription(cmp, event);
    },
    handleTypeChange : function(cmp, event, helper){
        cmp.set('v.issearching', true);
        helper.getSubscription(cmp, event);
    },
    handlePeriodChange : function(cmp, event, helper){
        cmp.set('v.issearching', true);
        helper.getSubscription(cmp, event);
    },
    showSpinner: function(cmp, event, helper) {
        cmp.set("v.Spinner", true); 
    },

    hideSpinner : function(cmp,event,helper){  
        cmp.set("v.Spinner", false);
    },
    
    handleActive: function (component, event, helper) {
       var tab = event.getSource();
        switch (tab.get('v.id')) {
            case 'ordersOverview' :
                if(!component.get('v.initalLoadDone')){
                    helper.getOrderItems(component, event);
                   	component.set('v.initalLoadDone',true);
                }
                break;
            case 'ContractsOverview' :
               console.log('ContractsOverview');
                break;
        }
    },
    
    setStartDate_order : function (component,event,helper){
        component.set('v.issearching_order', true); 
        helper.getOrderItems(component, event);
    },
    
    setEndDate_order : function (component,event,helper){
        component.set('v.issearching_order', true); 
        helper.getOrderItems(component, event);
    },
    
    handlePeriodChange_order : function (component,event,helper){
        component.set('v.issearching_order', true); 
        helper.getOrderItems(component, event);
    },
    
    handleKeyUp2: function (component, event, helper) {
        //var isEnterKey = event.keyCode === 13;
        var queryTerm = component.find('orderItemFilter').get('v.value');
        console.log('isEnterKey ::: '+isEnterKey+ ' queryTerm ::: '+queryTerm);
        if (/*isEnterKey && */(queryTerm.length>=3)) {
            component.set('v.issearching_order', true);
            helper.getOrderItems(component, event);
        }else if(!queryTerm){
            helper.getOrderItems(component, event);
        }
    },
    
    
    handleSort_crtc : function(component,event,helper){
        var mapIdFields = component.get("v.mapIdFields");
        var sortBy = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");
        
        component.set("v.sortBy_crtc",sortBy);
        component.set("v.sortDirection_crtc",sortDirection);
        
      	if(mapIdFields[sortBy]){sortBy = mapIdFields[sortBy];}
        
       	helper.sortData(component,sortBy,sortDirection,'contractOverview');
    },
    
    handleSort_order : function(component,event,helper){
        var mapIdFields = component.get("v.mapIdFields");
        var sortBy = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");
        
        component.set("v.sortBy_order",sortBy);
        component.set("v.sortDirection_order",sortDirection);
        
      	if(mapIdFields[sortBy]){sortBy = mapIdFields[sortBy];}
        
       	helper.sortData(component,sortBy,sortDirection,'orderOverview');
    },
    

    
     viewPdf : function (component, event, helper){
         if(component.get("v.gridData").length != 0){

           var quickActionClose = $A.get('e.force:closeQuickAction');
        var modalBody;
         $A.createComponent("c:pdfDonwload", {recordId : component.get('v.recordId'),gridData : component.get("v.gridData")},
           function(content, status,errorMessage) {

               if (status === "SUCCESS") {
                   var body = component.get("v.body");
                    body.push(content);
                    component.set("v.body", body);
                    
     
                               }
                else {
                     console.log('errorMessage ::: '+errorMessage)
                }
           });  
         }
        
    },
    
    handleComponentEvent : function (component,event,helper){
        console.log('handleComponentEvent :: '+event.getParam("language"));
        var sendDataProc = component.get("v.sendData");
       
         sendDataProc(component.get("v.gridData"),event.getParam("language"), function(){
                    console.log('MY CALBACK');
                });
        console.log('to call vf method');
        //invoke vf page js method
    }
})