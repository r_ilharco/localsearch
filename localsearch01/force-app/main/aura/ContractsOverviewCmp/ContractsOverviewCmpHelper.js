({    
    getPicklistValues: function(component, event){
        if(component.get('v.recordId')){
        var getAvailableStatuses = component.get("c.retrievePicklistValues");
        var getAvailableStatusesParams = {sObjectApiName : 'SBQQ__Subscription__c', fieldApiName : 'Subsctiption_Status__c'};
        getAvailableStatuses.setParams(getAvailableStatusesParams);
		getAvailableStatuses.setCallback(this, function(statusResponse) {
            var statusState = statusResponse.getState();
            if (statusState === "SUCCESS"){
                var availableStatuses = statusResponse.getReturnValue();
                component.set('v.statuses', availableStatuses);
                
                var getAvailableTypes = component.get("c.retrievePicklistValues");
        		var getAvailableTypesParams = {sObjectApiName : 'SBQQ__Subscription__c', fieldApiName : 'SBQQ__SubscriptionType__c'};
                getAvailableTypes.setParams(getAvailableTypesParams);
                getAvailableTypes.setCallback(this, function(typeResponse) {
                    var typeState = typeResponse.getState();
                    if (typeState === "SUCCESS"){
                        var availableTypes = typeResponse.getReturnValue();
                		component.set('v.types', availableTypes);
                        this.getSubscription(component, event);
						this.getOrderStatus(component,event);
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": $A.get("$Label.c.Error"),
                            "type": "error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    }
                });
                
                $A.enqueueAction(getAvailableTypes);
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Error"),
                    "type": "error",
                    "message": errors[0].message
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(getAvailableStatuses);
        }
    },
    getSubscription: function(component, event) {
        var action = component.get("c.getSubscriptionByAccount");
        var recordId = component.get('v.recordId');

        var isInit;
        if(component.get('v.issearching') == true) isInit = false;
        else isInit = true;
        
       var queryParam = component.find('subscriptionsFilter01').get('v.value');
        console.log('queryParam - Get subs :: '+queryParam);
        //console.log('queryParam :: '+queryParam);
        var startDate = component.get('v.startDateParam');
        if(component.find('startDate').get('v.value') != null) startDate = component.find('startDate').get('v.value');
        
        var endDate = component.get('v.endDateParam');
        if(component.find('endDate').get('v.value') != null) endDate = component.find('endDate').get('v.value');
        
        var selectedStatuses = component.get('v.selectedStatuses');
        var selectedType = component.find('subTypeSelector').get('v.value');
        var selectedPeriod = component.find('timePeriodSelector').get('v.value');
        
        if(selectedPeriod){
        	component.find('startDate').set('v.value',null)        
        	component.find('endDate').set('v.value',null)
        }
        
        var params = {
            			accountId:recordId,
            			isInit:isInit,
            			queryParam:queryParam,
            			startDate:startDate,
            			endDate:endDate,
            			statuses:selectedStatuses,
            			selectedType:selectedType,
            			selectedPeriod:selectedPeriod
        			  };
        
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.showMessage', data.isSizeZero);

                data.subscriptions.forEach(function(element) {
                    element._children = element.childrens;
                    
                    element.childrens.forEach(function(childrenElement) {
                        childrenElement.id = '/' + childrenElement.id;
                        childrenElement.productId = '/' + childrenElement.productId;                                        
                    });
                    
                    element.id = '/' + element.id;
                    element.productId = '/' + element.productId;
                    if(element.placeId){
                        element.placeId = '/' + element.placeId;
                    }
                    if(element.contractId){
                        element.contractId = '/' + element.contractId;
                    }
                    if(element.quoteId){
                        element.quoteId = '/' + element.quoteId;
                    }
                    if(element.opportunityId){
                        element.opportunityId = '/' + element.opportunityId;
                    }
                    if(element.accountId){
                        element.accountId = '/' + element.accountId;
                    }
                });
                
                component.set('v.gridData', data.subscriptions);
                
                var expandedRows = [];
                component.set('v.gridExpandedRows', expandedRows);
                component.set('v.issearching', false);
                this.setColumns(component, event);
                
                if(isInit && data.totalRevenueAmount) component.set('v.revenue', data.totalRevenueAmount);
            } 
            else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                } 
                else {
                    console.log("Unknown error");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                }
                
            }
        });
        
        $A.enqueueAction(action);
    },
    setColumns: function(cmp, event) {
        var columns;
        columns = [
           // { type: 'url',sortable: true, initialWidth: 200, fieldName: 'accountId', label: 'Account', typeAttributes: { label: { fieldName: 'accountName' } } },
            //{ type: 'text',sortable: true, initialWidth: 200,fieldName: 'customerNumber', label: 'Customer Number' },
            { type: 'url',sortable: true,  initialWidth: 200, fieldName: 'contractId', label: $A.get("$Label.c.CtrOverview_CtrNumber"), typeAttributes: { label: { fieldName: 'contractNumber' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'status', label: 'Status' },
            { type: 'date',sortable: true,initialWidth: 200, fieldName: 'startDate', label: $A.get("$Label.c.CtrOverview_StartDateCol"), typeAttributes: { label: { fieldName: 'startDate' } } },
            { type: 'date',sortable: true,initialWidth: 200, fieldName: 'endDate', label: $A.get("$Label.c.CtrOverview_EndDateCol"), typeAttributes: { label: { fieldName: 'endDate' } } },
            { type: 'url',sortable: true, initialWidth: 200, fieldName: 'id', label: 'Subscription #', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } },
           	{ type: 'url',sortable: true, initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'productName', label: $A.get("$Label.c.CtrOverview_ProductName")},
            { type: 'currency',sortable: true, initialWidth: 200, fieldName: 'annualTotal', label: $A.get("$Label.c.CtrOverview_AnnualTotal")},  
            { type: 'currency',sortable: true, initialWidth: 200, fieldName: 'bundleTotal',label: $A.get("$Label.c.CtrOverview_BundleTotal") },
            { type: 'currency',sortable: true, initialWidth: 200, fieldName: 'total', label:  $A.get("$Label.c.CtrOverview_Total"), typeAttributes: { label: { fieldName: 'total' } } },
            { type: 'url',sortable: true,  initialWidth: 200, fieldName: 'opportunityId', label: $A.get("$Label.c.CtrOverview_OptyNumber"), typeAttributes: { label: { fieldName: 'opportunityName' } } },
            { type: 'url',sortable: true, initialWidth: 200, fieldName: 'quoteId', label: $A.get("$Label.c.CtrOverview_QuoteNumber"), typeAttributes: { label: { fieldName: 'quoteNumber' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'periodsOfNotice', label: 'Periods of Notice'},
            { type: 'number',sortable: true,initialWidth: 200, fieldName: 'quantity', label: $A.get("$Label.c.CtrOverview_QtyCol") },
            { type: 'text',sortable: true,initialWidth: 200,fieldName: 'subType', label: $A.get("$Label.c.CtrOverview_TypeCol") },
            { type: 'boolean',sortable: true,initialWidth: 200, fieldName: 'inTermination', label: 'In Termination', typeAttributes: { label: { fieldName: 'inTermination' } } },
            { type: 'date',sortable: true,initialWidth: 200, fieldName: 'terminationDate', label: 'Termination Date', typeAttributes: { label: { fieldName: 'terminationDate' } } },
            { type: 'number',sortable: true,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
            { type: 'number',sortable: true,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
            { type: 'percent',sortable: true,initialWidth: 200, fieldName: 'discount',label: 'Discount'},
            { type: 'url',sortable: true, initialWidth: 200, fieldName: 'accountId', label: 'Account', typeAttributes: { label: { fieldName: 'accountName' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'customerNumber', label: 'Customer Number' },
        ];
        cmp.set('v.gridColumns', columns);
    },
    
    
    getOrderItems : function(component, event) {
        var action = component.get("c.getOrderItemsByAccount");
        var recordId = component.get('v.recordId');

        var isInit;
        if(component.get('v.issearching_order') == true) isInit = false;
        else isInit = true;
        

        var queryParam = component.find('orderItemFilter').get('v.value');
        
        var selectedPeriod = component.find('timePeriodSelector_order').get('v.value');
        if(selectedPeriod){
        	component.find('startDate_order').set('v.value',null)        
        	component.find('endDate_order').set('v.value',null)
        }
        
        var startDate = component.get('v.startDateParam_order');
        if(component.find('startDate_order').get('v.value') != null) startDate = component.find('startDate_order').get('v.value');
        
        var endDate = component.get('v.endDateParam_order');
        if(component.find('endDate_order').get('v.value') != null) endDate = component.find('endDate_order').get('v.value');
        
        var selectedStatuses = component.get('v.statusOrdSelected');
         console.log('selectedStatuses :: '+selectedStatuses)
        console.log('startDate :: '+startDate+' endDate:::  '+endDate+' selectedPeriod :: '+selectedPeriod)
        
        var params = {
            			accountId:recordId,
            			isInit:isInit,
            			queryParam:queryParam,
            			startDate:startDate,
            			endDate:endDate,
            			selectedPeriod:selectedPeriod,
            			statuses:selectedStatuses,
        			  };
        
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.showMessage_order', data.isSizeZero);
                data.orderItems.forEach(function(element) {
                    element._children = element.childrens;
                    
                    element.childrens.forEach(function(childrenElement) {
                        childrenElement.id = '/' + childrenElement.id;
                        childrenElement.productId = '/' + childrenElement.productId;                                        
                    });
                    
                    element.id = '/' + element.id;
                    element.productId = '/' + element.productId;
                    if(element.placeId){
                        element.placeId = '/' + element.placeId;
                    }
                    if(element.contractId){
                        element.contractId = '/' + element.contractId;
                    }
                    if(element.orderId){
                        element.orderId = '/' + element.orderId;
                    }
                    if(element.quoteId){
                        element.quoteId = '/' + element.quoteId;
                    }
                    if(element.opportunityId){
                        element.opportunityId = '/' + element.opportunityId;
                    }
                    if(element.accountId){
                        element.accountId = '/' + element.accountId;
                    }
                });
                
                component.set('v.gridData_order', data.orderItems);
                
                var expandedRows = [];
                component.set('v.gridExpandedRows_order', expandedRows);
                component.set('v.issearching_order', false);
                this.setColumns_order(component, event);
                
            } 
          	else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                } 
                else {
                    console.log("Unknown error");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": $A.get("$Label.c.Error"),
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                }
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    setColumns_order: function(cmp, event) {
        var columns;
        columns = [
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'orderId', label: 'Order Number', typeAttributes: { label: { fieldName: 'orderNumber' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'orderStatus', label: $A.get("$Label.c.CtrOverview_OrderStatus")},
            { type: 'date' ,sortable: true,initialWidth: 200, fieldName: 'startDate', label: $A.get("$Label.c.CtrOverview_StartDateCol"), typeAttributes: { label: { fieldName: 'startDate' } } },
            { type: 'date' ,sortable: true,initialWidth: 200, fieldName: 'endDate', label: $A.get("$Label.c.CtrOverview_EndDateCol"), typeAttributes: { label: { fieldName: 'endDate' } } },
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'id', label: 'Order Item #', typeAttributes: { label: { fieldName: 'name' }, target: '_parent' } },
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'placeId', label: 'Place', typeAttributes: { label: { fieldName: 'placeName' } } },

            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'productName', label: $A.get("$Label.c.CtrOverview_ProductName") },
            { type: 'currency' ,sortable: true,initialWidth: 200, fieldName: 'bundleTotal', label: 'Bundle Total', typeAttributes: { label: { fieldName: 'bundleTotal' } } },
            { type: 'currency' ,sortable: true,initialWidth: 200, fieldName: 'total', label: 'Total', typeAttributes: { label: { fieldName: 'total' } } },
            //{ type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'contractId', label: $A.get("$Label.c.CtrOverview_CtrNumber"), typeAttributes: { label: { fieldName: 'contractNumber' } } },
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'opportunityId', label:  $A.get("$Label.c.CtrOverview_OptyNumber") , typeAttributes: { label: { fieldName: 'opportunityName' } } },
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'quoteId', label:  $A.get("$Label.c.CtrOverview_QuoteNumber"), typeAttributes: { label: { fieldName: 'quoteNumber' } } },
           // { type: 'text',sortable: true, initialWidth: 200,fieldName: 'status', label: 'Status' },
            { type: 'number' ,sortable: true,initialWidth: 200, fieldName: 'quantity', label: $A.get("$Label.c.CtrOverview_QtyCol") },
            { type: 'number' ,sortable: true,initialWidth: 250, fieldName: 'BasetermRenewalTerm', label: 'Base term/Renewal Term', typeAttributes: { label: { fieldName: 'BasetermRenewalTerm' } } },
            { type: 'number' ,sortable: true,initialWidth: 200, fieldName: 'SubscriptionTerm', label: 'Subscription Term' },
            { type: 'percent',sortable: true,initialWidth: 200, fieldName: 'discount',label: 'Discount'},
            { type: 'url' ,sortable: true, initialWidth: 200, fieldName: 'accountId', label: 'Account', typeAttributes: { label: { fieldName: 'accountName' } } },
            { type: 'text',sortable: true, initialWidth: 200,fieldName: 'customerNumber', label: 'Customer Number' },
        ];
        cmp.set('v.gridColumns_order', columns);
    },
    
    getOrderStatus : function(component,event){
        var getAvailableSatus = component.get("c.retrievePicklistValues");
        var getAvailableSatusParams = {sObjectApiName : 'Order', fieldApiName : 'Status'};
        getAvailableSatus.setParams(getAvailableSatusParams);
        getAvailableSatus.setCallback(this, function(statusResponse) {
            if (statusResponse.getState() === "SUCCESS"){
                var availableStatus = statusResponse.getReturnValue();
                component.set('v.statusOrd', availableStatus);
            }
        });
        
        $A.enqueueAction(getAvailableSatus);
    },
    
    sortData : function(component,fieldName,sortDirection,type){
        var data;
        var numberFields = component.get("v.nonTextFields");
        
        if(type == 'contractOverview'){data = component.get("v.gridData");}
        else if( type == 'orderOverview') {data = component.get("v.gridData_order");}
        
        
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;

        // NUMBER FIELDS
        if(numberFields.includes(fieldName)) { 
        	data.sort(function(a,b){
                var a = key(a) ? key(a) : '';
                var b = key(b) ? key(b) : '';
                return reverse * ((a>b) - (b>a));
            }); 
        }
        else{
            data.sort(function(a,b){ 
                var a = key(a) ? key(a).toLowerCase() : '';
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        }
        if(type == 'contractOverview'){component.set("v.gridData",data);}
        else if( type == 'orderOverview') {component.set("v.gridData_order",data);}
    }
    
})