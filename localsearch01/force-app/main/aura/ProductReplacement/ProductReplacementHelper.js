({
    getProducts: function(component, event) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getQuoteProducts");
        action.setParams({ quoteId : recordId });
        action.setCallback(this, function(response) {
            component.set('v.loaded', true);
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if(data.valid){
                    var productsToReplace = [];
                    data.products.forEach(function(element) {
                        productsToReplace.push(element);
                    });
                    component.set('v.productsToReplace',productsToReplace);
                } else if(data.validations.length > 0){
                    this.showNotifWarning(component, event, data.validations,"",true);
                    return;
                } else{
                    this.showNotifError(component, event,data.errors,"",true);
                    return;
                }
            } else {
                this.showNotifError(component, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    replaceProducts: function(component, event) {
        component.set('v.loaded', false);
        var products = component.get('v.productsToReplace');
        var recordId = component.get('v.recordId');
        var cmpsU = component.find("cbUpgrade");
        var cmpsT = component.find("cbtoggle");
        var replaceList = [];
        var cbUpgradeList = [].concat(cmpsU);
        var cbToogleList = [].concat(cmpsT);
        for(var i = 0; i < cbUpgradeList.length; i++){
            var cmp1 = cbUpgradeList[i];
            var cmp2 = cbToogleList[i];
            var val1 = cmp1.get("v.value");
            var val2 = cmp2.get("v.checked");
            if(val1){
                var info = val1 +'-'+val2
                replaceList.push(info);
            }
        }
        if(replaceList.length == 0){
            component.set('v.error','No Product Upgrade selected');
            return;
        } else {
            component.set('v.error',null);
        }
        
        var action = component.get("c.replaceProducts");
        action.setParams({quoteId : recordId , optionsList : JSON.stringify(replaceList)});
        action.setCallback(this, function(response) {
            component.set('v.loaded', true);
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if(data.valid){
                    this.showNotifSuccess(component, event,$A.get("$Label.c.Information_send_with_Success"),$A.get("$Label.c.Operation_Success"));
                    return;
                } else {
                    component.set('v.error', data.errors);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
                this.showNotifError(component, event,$A.get("$Label.c.Error_sending_information"),$A.get("$Label.c.Operation_failed"));
            }
        });
        $A.enqueueAction(action);
    },
    closeModal : function(component, event){
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },
    showNotifError : function(component, event, message, header, close) {
        component.find('notifLib').showToast ({
            "variant": "error",
            "header": header,
            "message": [].concat(message).join("\n")
        });
        if(close && close == true){
        	this.closeModal(component, event);
        }
    },
    showNotifWarning : function(component, event, message, header, close) {
        component.find('notifLib').showToast ({
            "variant": "warning",
            "header": header,
            "message": [].concat(message).join("\n"),
    //        "mode": 'pester' 
        });
        if(close && close == true){
        	this.closeModal(component, event);
        }
    },
    showNotifSuccess : function(component, event, message, header) {
        component.find('notifLib').showToast ({
            "variant": "success",
            "header": header,
            "message": [].concat(message).join("\n")
        });
        this.closeModal(component, event);
        $A.get('e.force:refreshView').fire();
    },
})