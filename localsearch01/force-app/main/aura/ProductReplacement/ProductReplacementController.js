({
    doInit : function(component, event, helper) {
		helper.getProducts(component, event);
	},
	handleCancelClick : function(component, event, helper) {
		helper.closeModal(component, event);
	},
    handleSaveClick : function(component, event, helper) {
		helper.replaceProducts(component, event); 
	},
    handleReplaceChange : function(component, event, helper) {
		var selectedOptionValue = event.getParam("value");
       // alert("Option selected with value: '" + selectedOptionValue + "'");
	}
})